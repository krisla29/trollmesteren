// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GradDiffuse"
{
	Properties
	{
		_TopColor("TopColor", Color) = (0.8679245,0.6384578,0.4380563,1)
		_MidColor("MidColor", Color) = (0.58,0.4277828,0.2939366,1)
		_BotColor("BotColor", Color) = (0.28,0.2074074,0.1431111,1)
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Height("Height", Float) = -2
		_TopMid("TopMid", Float) = -1
		_BotMid("BotMid", Float) = -0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TopColor;
		uniform float4 _MidColor;
		uniform float _Height;
		uniform float _TopMid;
		uniform float4 _BotColor;
		uniform float _BotMid;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float temp_output_48_0 = ( ( 1.0 - i.uv_texcoord.y ) * ( _Height * -1.0 ) );
			float4 lerpResult32 = lerp( _TopColor , _MidColor , ( temp_output_48_0 + _TopMid ));
			float4 lerpResult40 = lerp( _MidColor , _BotColor , ( temp_output_48_0 + _BotMid ));
			float4 lerpResult41 = lerp( lerpResult32 , lerpResult40 , ( temp_output_48_0 + 0.0 ));
			o.Albedo = ( tex2D( _TextureSample0, i.uv_texcoord ) * lerpResult41 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
414.4;518.4;1324;727;1643.91;440.3671;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;51;-1706.021,-13.77697;Inherit;False;Constant;_Float2;Float 2;7;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-1697.138,-89.78611;Inherit;False;Property;_Height;Height;4;0;Create;True;0;0;False;0;-2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;30;-1767.452,-266.4871;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;49;-1499.889,-278.6196;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;-1553.491,-86.04543;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-939.8111,-303.8073;Inherit;False;Property;_TopMid;TopMid;5;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-961.8704,-25.64592;Inherit;False;Property;_BotMid;BotMid;6;0;Create;True;0;0;False;0;-0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-1244.96,-201.7748;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;9;-593.9928,-607.4452;Inherit;False;Property;_TopColor;TopColor;0;0;Create;True;0;0;False;0;0.8679245,0.6384578,0.4380563,1;0.8679245,0.6384578,0.4380563,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;42;-734.5153,-279.5119;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;45;-731.5144,-69.06032;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;39;-593.0368,-238.7247;Inherit;False;Property;_MidColor;MidColor;1;0;Create;True;0;0;False;0;0.58,0.4277828,0.2939366,1;0.8679245,0.6384578,0.4380563,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;11;-584.8026,-10.83888;Inherit;False;Property;_BotColor;BotColor;2;0;Create;True;0;0;False;0;0.28,0.2074074,0.1431111,1;0.28,0.2074074,0.1431111,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;40;-254.865,-126.589;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-235.7442,-509.8625;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;32;-263.0765,-322.1218;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;52;-957.2249,-201.8377;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;24;17.83168,-441.3326;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;41;-39.72293,-241.5804;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;347.7996,-38.15973;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;519.0394,-29.90914;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;GradDiffuse;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;False;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;49;0;30;2
WireConnection;50;0;37;0
WireConnection;50;1;51;0
WireConnection;48;0;49;0
WireConnection;48;1;50;0
WireConnection;42;0;48;0
WireConnection;42;1;43;0
WireConnection;45;0;48;0
WireConnection;45;1;44;0
WireConnection;40;0;39;0
WireConnection;40;1;11;0
WireConnection;40;2;45;0
WireConnection;32;0;9;0
WireConnection;32;1;39;0
WireConnection;32;2;42;0
WireConnection;52;0;48;0
WireConnection;24;1;27;0
WireConnection;41;0;32;0
WireConnection;41;1;40;0
WireConnection;41;2;52;0
WireConnection;26;0;24;0
WireConnection;26;1;41;0
WireConnection;0;0;26;0
ASEEND*/
//CHKSM=00DD456DA38116AABD7C17022328D1A66F2401C3