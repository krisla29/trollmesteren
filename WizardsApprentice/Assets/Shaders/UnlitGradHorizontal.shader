// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UnlitGradHorizontal"
{
	Properties
	{
		_TopColor("TopColor", Color) = (0.9788539,1,0,1)
		_MidColor("MidColor", Color) = (1,0.05869111,0,1)
		_BotColor("BotColor", Color) = (0.08139104,0.5188679,0,1)
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Height("Height", Float) = -2
		_TopMid("TopMid", Float) = -1
		_BotMid("BotMid", Float) = -0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TopColor;
		uniform float4 _MidColor;
		uniform float _Height;
		uniform float _TopMid;
		uniform float4 _BotColor;
		uniform float _BotMid;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float temp_output_25_0 = ( ( 1.0 - i.uv_texcoord.y ) * ( _Height * -1.0 ) );
			float4 lerpResult31 = lerp( _TopColor , _MidColor , ( temp_output_25_0 + _TopMid ));
			float4 lerpResult32 = lerp( _MidColor , _BotColor , ( temp_output_25_0 + _BotMid ));
			float4 lerpResult35 = lerp( lerpResult31 , lerpResult32 , ( temp_output_25_0 + 0.0 ));
			o.Emission = ( tex2D( _TextureSample0, i.uv_texcoord ) * lerpResult35 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
87.2;524;1324;615;2158.967;544.988;1.94566;True;True
Node;AmplifyShaderEditor.RangedFloatNode;18;-2204.027,247.9463;Inherit;False;Constant;_Float2;Float 2;7;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-2195.144,171.9372;Inherit;False;Property;_Height;Height;4;0;Create;True;0;0;False;0;-2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;20;-2265.458,-4.763794;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;21;-1997.895,-16.8963;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-2051.497,175.6779;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-1437.817,-42.08401;Inherit;False;Property;_TopMid;TopMid;5;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-1459.876,236.0773;Inherit;False;Property;_BotMid;BotMid;6;0;Create;True;0;0;False;0;-0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1742.966,59.9485;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;30;-1091.999,-345.7219;Inherit;False;Property;_TopColor;TopColor;0;0;Create;True;0;0;False;0;0.9788539,1,0,1;0.8679245,0.6384578,0.4380563,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;29;-1232.521,-17.7886;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;27;-1082.808,250.8844;Inherit;False;Property;_BotColor;BotColor;2;0;Create;True;0;0;False;0;0.08139104,0.5188679,0,1;0.28,0.2074074,0.1431111,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;28;-1229.52,192.663;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;26;-1075.477,22.9986;Inherit;False;Property;_MidColor;MidColor;1;0;Create;True;0;0;False;0;1,0.05869111,0,1;0.8679245,0.6384578,0.4380563,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;33;-1455.231,59.88559;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;31;-761.0819,-60.3985;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;-733.7496,-248.1392;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;32;-752.8705,135.1343;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;36;-480.1738,-179.6093;Inherit;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;35;-537.7285,20.1429;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-199.2905,40.42051;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;2;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;UnlitGradHorizontal;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;21;0;20;2
WireConnection;22;0;19;0
WireConnection;22;1;18;0
WireConnection;25;0;21;0
WireConnection;25;1;22;0
WireConnection;29;0;25;0
WireConnection;29;1;24;0
WireConnection;28;0;25;0
WireConnection;28;1;23;0
WireConnection;33;0;25;0
WireConnection;31;0;30;0
WireConnection;31;1;26;0
WireConnection;31;2;29;0
WireConnection;32;0;26;0
WireConnection;32;1;27;0
WireConnection;32;2;28;0
WireConnection;36;1;34;0
WireConnection;35;0;31;0
WireConnection;35;1;32;0
WireConnection;35;2;33;0
WireConnection;17;0;36;0
WireConnection;17;1;35;0
WireConnection;2;2;17;0
ASEEND*/
//CHKSM=C96F5E0D017B8B2E7294D987C52F20C6C3CE9AEC