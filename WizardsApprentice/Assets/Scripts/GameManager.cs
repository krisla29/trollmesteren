﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public enum ControlsMode { Mobile, GamePad, PC };
    public ControlsMode controlsMode;
    public bool isEasyTargeting; //not implemented
    public bool isEasyCameraLooking; //not implemented
    public bool hasFirstPersonAim;
    public bool isNavMeshAgent;
    public bool isOnMountainTop;
    public Transform mountainTop;
    public CharacterManager characterManager;
    public FriendlyCharacterController wizardController;
    public FriendlyCharacterController underBossController;
    public FriendlyCharacterController capturedCharacterController;
    public float startZoom = 1;

    public float targetScreenHeight = 1080f;
    public float currentScreenHeight;
    //test:
    public bool isWebGl;
    public SaveObjectWebGl saveObjectWebGl;

    public bool isPlayerAiming;
    public bool isGameEvent;
    public bool isScene;
    public bool isSceneTouch;
    public bool isWorldMap;
    public bool isBattle;
    public bool isMapDemo;
    public BoxCollider rayBlocker;
    //public bool quitGame;
    public enum InteractionModes { Wand, Direct };
    public InteractionModes interactionMode;
    public InputManager inputManager;
    public GameEventsManager gameEventsManager;
    public TriggerEventsManager triggerEventsManager;
    public CameraSceneManager cameraSceneManager;
    public HudCanvas hudCanvas;
    public WorldMapController worldMapController;
    public SceneController currentScene;
    public GameEvent currentGameEvent;
    public PlayerReference playerReference;
    public PlayerController player;
    public PlayerStats playerStats;
    public CharacterSelector characterSelector; //on Player object
    public GameObject wandSpawner;
    public List<GameObject> interactables = new List<GameObject>();
    public List<GameObject> wandInteractables = new List<GameObject>();
    public ProximityManager proximityManager;
    public RectTransform uiEffectsContainer;
    public Transform worldEffectsContainer;   

    public AudioSource soundTrack;

    public Color colorEasy = Color.green;
    public Color colorNormal = Color.yellow;
    public Color colorHard = Color.red;

    private float interactablesDetectorScale;
    private float wandInteractableDetectorScale;

    public SceneController caveEntrance;
    public Transform mountainCollider;
    public Light worldLight;
    public Vector3 worldLightStartRotation;
    public float worldLightStartIntensity;
    public Material worldSkybox;
    public Material worldSkyboxClone;
    public float worldSkyboxStartExposure;
    public float fogDensity;
    public float fogStartDensity;
    public Color fogStartColor;
    public MeshRenderer starSky;
    public Material starskyMatClone;
    public float torchesDist = 30f;
    public AudioSource soundTrackOpening;
    public Transform startFader;
    public Transform saveGraphic;

    [Header("DEMOS AND EVENTS")]
    [Tooltip("Add the trigger and the triggered event in each list")]
    public List<Transform> openingSequence = new List<Transform>();
    public List<Transform> trophyEvent = new List<Transform>();
    public List<Transform> wellEvent = new List<Transform>();
    public List<Transform> meetUnderBossEvent = new List<Transform>();
    public List<Transform> transformUnderbossEvent = new List<Transform>();
    public List<Transform> hatTutorialEvent = new List<Transform>();
    public List<Transform> meetWizardEvent = new List<Transform>();
    public List<Transform> transformWizardEvent = new List<Transform>();
    public Cage cageScript; // ( Hat tutorial )    
    public HUD_Demo hudDemo;
    public HUD_Map_Demo mapDemoScript;
    public GameEvent underBossMeetEvent;
    public List<Transform> trophyDemoObjects = new List<Transform>();

    public delegate void GameSceneEvent(MonoBehaviour gameScript);
    public static event GameSceneEvent OnGameEnable;
    public static event GameSceneEvent OnGameDisable;

    public delegate void SwitchSceneEvent(SceneController scenController); // Sends to AwakenByScene
    public static event SwitchSceneEvent OnSwitchScene;
    public delegate void SwitchGameEvent(GameEvent gameEvent); // Sends to AwakenByScene
    public static event SwitchGameEvent OnSwitchEvent;
    public static event SwitchGameEvent OnAfterSwitchOnEvent;
    public static event SwitchGameEvent OnAfterSwitchOffEvent; //Sends To Cage ( Hat Demo ), AddCharacterEffect script

    private void Awake()
    {
        if(!hudCanvas.gameObject.activeSelf)
        {
            hudCanvas.gameObject.SetActive(true);
        }

        startFader.gameObject.SetActive(true);

        RenderSettings.fogDensity = fogStartDensity;
        RenderSettings.fogColor = fogStartColor;
        fogDensity = RenderSettings.fogDensity;
        worldLight.transform.localEulerAngles = worldLightStartRotation;
        worldLight.intensity = worldLightStartIntensity;
        worldSkyboxClone = Instantiate(worldSkybox);
        RenderSettings.skybox = worldSkyboxClone;
        worldSkyboxClone.SetFloat("_Exposure", worldSkyboxStartExposure);
        starskyMatClone = Instantiate(starSky.material);
        starSky.material = starskyMatClone;
    }

    void Start()
    {
        ChangeInteractionMode();
        hudCanvas.exitButton.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        CameraSceneManager.OnChangeToSceneCam += SwitchScene;
        CameraSceneManager.OnChangeToPlayerCam += SwitchScene;
        CameraSceneManager.OnChangeToEventCam += SwitchToEvent;
        CameraSceneManager.OnChangeFromEventCam += SwitchOffEvent;
        GameEvent.OnGameEventStart += SwitchToEvent;
        GameEvent.OnGameEventStop += SwitchOffEvent;
        //CameraEvent.OnCameraEventStop += SwitchOffEvent;
        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;
        Interactable.OnInteractableBirth += AddInteractable;
        Interactable.OnInteractableEnemyBirth += AddInteractable;
        Interactable.OnInteractableDeath += RemoveInteractable;
        Interactable.OnInteractableEnemyDeath += RemoveInteractable;
        WandInteractable.OnWandInteractableBirth += AddWandInteractable;
        WandInteractable.OnWandInteractableEnemyBirth += AddWandInteractable;
        WandInteractable.OnWandInteractableDeath += RemoveWandInteractable;
        WandInteractable.OnWandInteractableEnemyDeath += RemoveWandInteractable;
        WorldMapScene.OnWorldMapOn += TurnOffFog;
        WorldMapScene.OnWorldMapOff += TurnOnFog; 
    }

    private void OnDisable()
    {
        CameraSceneManager.OnChangeToSceneCam -= SwitchScene;
        CameraSceneManager.OnChangeToPlayerCam -= SwitchScene;
        CameraSceneManager.OnChangeToEventCam -= SwitchToEvent;
        CameraSceneManager.OnChangeFromEventCam -= SwitchOffEvent;
        GameEvent.OnGameEventStart -= SwitchToEvent;
        GameEvent.OnGameEventStop -= SwitchOffEvent;
        //CameraEvent.OnCameraEventStop -= SwitchOffEvent;
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
        Interactable.OnInteractableBirth -= AddInteractable;
        Interactable.OnInteractableEnemyBirth -= AddInteractable;
        Interactable.OnInteractableDeath -= RemoveInteractable;
        Interactable.OnInteractableEnemyDeath -= RemoveInteractable;
        WandInteractable.OnWandInteractableBirth -= AddWandInteractable;
        WandInteractable.OnWandInteractableEnemyBirth -= AddWandInteractable;
        WandInteractable.OnWandInteractableDeath -= RemoveWandInteractable;
        WandInteractable.OnWandInteractableEnemyDeath -= RemoveWandInteractable;
        WorldMapScene.OnWorldMapOn -= TurnOffFog;
        WorldMapScene.OnWorldMapOff -= TurnOnFog;
        StopAllCoroutines();
    }

    private void Update()
    {
        currentScreenHeight = Screen.height;
    }

    void AddInteractable(Transform objectTransform)
    {
        if(CheckIfNew(objectTransform.gameObject, interactables))
        {
            interactables.Add(objectTransform.gameObject);
            
            if(interactionMode == InteractionModes.Direct)
            {
                objectTransform.gameObject.SetActive(true);
            }
            else
            {
                objectTransform.gameObject.SetActive(false);
            }
            
        }
    }

    void RemoveInteractable(Transform objectTransform)
    {
        if (!CheckIfNew(objectTransform.gameObject, interactables))
        {
            interactables.Remove(objectTransform.gameObject);
        }
    }

    void AddWandInteractable(Transform objectTransform)
    {
        if (CheckIfNew(objectTransform.gameObject, wandInteractables))
        {
            wandInteractables.Add(objectTransform.gameObject);
            
            if (interactionMode == InteractionModes.Wand)
            {
                objectTransform.gameObject.SetActive(true);
            }
            else
            {
                objectTransform.gameObject.SetActive(false);
            }
            
        }
    }

    void RemoveWandInteractable(Transform objectTransform)
    {
        if (!CheckIfNew(objectTransform.gameObject, wandInteractables))
        {
            wandInteractables.Remove(objectTransform.gameObject);
        }
    }

    void SwitchScene(Transform currentTransform)
    {
        if (currentTransform.transform.CompareTag("MainCamera"))
        {
            isScene = false;

            if (currentScene)
            {
                if (currentScene.isBattleScene)
                    isBattle = false;
            }

            currentScene = null;
            proximityManager.isScene = false;
            hudCanvas.exitButton.gameObject.SetActive(false);            
            hudCanvas.zoomSlider.gameObject.SetActive(true);
            hudCanvas.rotateSlider.gameObject.SetActive(true);
            hudCanvas.lockRotationToggle.transform.parent.gameObject.SetActive(true);
            hudCanvas.gemsPanel.gameObject.SetActive(true);

            if (playerStats.hasUnlockedHat)
            {
                hudCanvas.toggleCharacterButton.gameObject.SetActive(true);
                hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(false);
            }
            else
            {
                hudCanvas.toggleCharacterButton.gameObject.SetActive(false);
                hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(true);
            }

            if (playerStats.hasUnlockedMap)
            {
                hudCanvas.worldMapButton.gameObject.SetActive(true);
                hudCanvas.worldMapBtnEmpty.gameObject.SetActive(false);
            }
            else
            {
                hudCanvas.worldMapButton.gameObject.SetActive(false);
                hudCanvas.worldMapBtnEmpty.gameObject.SetActive(true);
            }

            if(playerStats.hasSeenTrophyDemo)
            {
                hudCanvas.trophyButton.gameObject.SetActive(true);
                hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
            }
            else
            {
                hudCanvas.trophyButton.gameObject.SetActive(false);
                hudCanvas.trophyBtnEmpty.gameObject.SetActive(true);
            }

            mountainCollider.gameObject.SetActive(true);
            
            if (isMapDemo)
            {                
                hudCanvas.worldMapButton.gameObject.SetActive(true);
                hudCanvas.worldMapBtnEmpty.gameObject.SetActive(false);
                hudCanvas.StartMapBtnPulser();
                hudCanvas.uiPointer.SetParent(hudCanvas.mapBtnPulser.transform.parent);
                hudCanvas.uiPointer.anchoredPosition = new Vector2(hudCanvas.uiPointerBtnXpos, 0);
                hudCanvas.uiPointer.gameObject.SetActive(true);
            }                       
        }
        else
        {
            isScene = true;
            print(currentTransform);
            currentScene = currentTransform.GetComponent<SceneController>();
            proximityManager.isScene = true;           
            
            if (currentScene != hudCanvas.worldMapButton.worldMapScene)
            {
                hudCanvas.trophyButton.gameObject.SetActive(false);
                hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
                hudCanvas.worldMapButton.gameObject.SetActive(false);
                hudCanvas.worldMapBtnEmpty.gameObject.SetActive(false);
            }

            hudCanvas.zoomSlider.gameObject.SetActive(false);
            hudCanvas.rotateSlider.gameObject.SetActive(false);
            hudCanvas.lockRotationToggle.transform.parent.gameObject.SetActive(false);

            if (hudCanvas.trophyPanel.gameObject.activeSelf)
            {
                hudCanvas.trophyPanel.gameObject.SetActive(false);
                hudCanvas.trophyButton.isOn = false;
            }

            if (currentScene.isBattleScene)
            {
                isBattle = true;
                hudCanvas.gemsPanel.gameObject.SetActive(false);
            }

            if (currentScene == caveEntrance)
                mountainCollider.gameObject.SetActive(false);
            else
                mountainCollider.gameObject.SetActive(true);

            hudCanvas.toggleCharacterButton.gameObject.SetActive(false);
            hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(false);
            

            if (isMapDemo)
            {
                hudCanvas.ResetUiPointer();
                hudCanvas.ResetMapBtnPulser();

                if (currentScene.transform == worldMapController.transform)
                {
                    mapDemoScript.gameObject.SetActive(true);
                }
            }
            
        }

        if (OnSwitchScene != null)
            OnSwitchScene(currentScene);

        playerStats.SaveGame();
    }

    void SwitchToEvent(GameEvent passedEvent)
    {
        isGameEvent = true;
        currentGameEvent = passedEvent;
        hudCanvas.trophyButton.gameObject.SetActive(false);
        hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
        hudCanvas.worldMapButton.gameObject.SetActive(false);
        hudCanvas.worldMapBtnEmpty.gameObject.SetActive(false);
        hudCanvas.toggleCharacterButton.gameObject.SetActive(false);
        hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(false);
        hudCanvas.exitButton.gameObject.SetActive(false);

        if (hudCanvas.trophyPanel.gameObject.activeSelf)
        {
            hudCanvas.trophyPanel.gameObject.SetActive(false);
            hudCanvas.trophyButton.isOn = false;
        }

        if (passedEvent.lockPlayer)
            hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);

        hudCanvas.LockAll();

        if(passedEvent.hideUI)
        {
            hudCanvas.HideUI();
            hudCanvas.gemsPanel.gameObject.SetActive(false);
        }

        if (OnSwitchEvent != null)
            OnSwitchEvent(currentGameEvent);

        if (OnAfterSwitchOnEvent != null)
            OnAfterSwitchOnEvent(passedEvent);
        
    }

    void SwitchOffEvent(GameEvent passedEvent)
    {      
        isGameEvent = false;
        currentGameEvent = null;

        if(!isScene)
            hudCanvas.ShowUISlidersAndGems();
        
        if (!isScene)
        {
            if (playerStats.hasSeenTrophyDemo)
            {
                hudCanvas.trophyButton.gameObject.SetActive(true);
                hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
            }
            else
            {
                hudCanvas.trophyButton.gameObject.SetActive(false);
                hudCanvas.trophyBtnEmpty.gameObject.SetActive(true);
            }

            if (playerStats.hasUnlockedMap)
            {
                hudCanvas.worldMapButton.gameObject.SetActive(true);
                hudCanvas.worldMapBtnEmpty.gameObject.SetActive(false);
            }
            else
            {
                hudCanvas.worldMapButton.gameObject.SetActive(false);
                hudCanvas.worldMapBtnEmpty.gameObject.SetActive(true);
            }

            if(playerStats.hasUnlockedHat)
            {
                hudCanvas.toggleCharacterButton.gameObject.SetActive(true);
                hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(false);
            }
            else
            {
                hudCanvas.toggleCharacterButton.gameObject.SetActive(false);
                hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(true);
            }
        }
        else
        {
            hudCanvas.exitButton.gameObject.SetActive(true);

        }

        hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);

        hudCanvas.UnlockAll();

        if (OnSwitchEvent != null)
            OnSwitchEvent(currentGameEvent);

        if (OnAfterSwitchOffEvent != null)
            OnAfterSwitchOffEvent(passedEvent);

        playerStats.SaveGame();
    }

    public void ChangeInteractionMode()
    {
        if (interactionMode == InteractionModes.Wand)
        {
            foreach (var item in GameObject.FindGameObjectsWithTag("InteractableAim"))
            {
                Destroy(item.gameObject);
            }
            
            if (player)
            {
                player.wandInteractableDetector.gameObject.SetActive(true);
                player.interactablesDetector.gameObject.SetActive(false);
                
                for (int i = 0; i < wandInteractables.Count; i++)
                {
                    wandInteractables[i].SetActive(true);
                }

                for (int i = 0; i < interactables.Count; i++)
                {
                    interactables[i].SetActive(false);
                }

                if(player.wand)
                {
                    player.wand.gameObject.SetActive(true);
                }

                if (wandSpawner)
                {
                    if (!wandSpawner.activeSelf)
                    {
                        wandSpawner.SetActive(true);
                    }
                }

                player.interactablesDetector.interactables.Clear();

                player.wandInteractableDetector.transform.localScale = Vector3.zero;
                StartCoroutine("ScalePlayerWandDetector");
                
            }
        }
        if (interactionMode == InteractionModes.Direct)
        {
            var aimButtonScript = hudCanvas.aimButton.gameObject.GetComponent<AimButton>();
            var aimButtonToggle = hudCanvas.aimButton.gameObject.GetComponent<Toggle>();
            aimButtonScript.isOn = false;
            aimButtonToggle.isOn = false;
            hudCanvas.aimButton.gameObject.SetActive(false);

            foreach (var item in GameObject.FindGameObjectsWithTag("WandAim"))
            {
                item.SetActive(false);
            }           

            if (player)
            {
                player.interactablesDetector.gameObject.SetActive(true);
                player.wandInteractableDetector.gameObject.SetActive(false);

                if (player.toggleAim)
                {
                    player.toggleAim = false;
                }

                if (player.toggleAimThirdPerson)
                {
                    player.toggleAimThirdPerson = false;
                }

                if (player.wand)
                {
                    player.wand.gameObject.SetActive(false);
                }

                player.wandInteractableDetector.wandInteractables.Clear();
                player.interactablesDetector.transform.localScale = Vector3.zero;
                StartCoroutine("ScalePlayerDetector");
            }
            
            for (int i = 0; i < interactables.Count; i++)          
            {
                interactables[i].SetActive(true);               
            }

            for (int i = 0; i < wandInteractables.Count; i++)
            {
                wandInteractables[i].SetActive(false);
            }

            if (wandSpawner)
            {
                if (wandSpawner.activeSelf)
                {
                    wandSpawner.SetActive(false);
                }
            }
        }
    }

    private IEnumerator ScalePlayerWandDetector()
    {
        float time = 0;
        float nScale = wandInteractableDetectorScale;

        while (time < 1)
        {
            time += Time.deltaTime / 1;
            player.wandInteractableDetector.transform.localScale = Vector3.Lerp(Vector3.zero, new Vector3(nScale, nScale, nScale), time);           
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ScalePlayerDetector()
    {
        float time = 0;
        float nScale = interactablesDetectorScale;

        while (time < 1)
        {
            time += Time.deltaTime / 1;
            player.interactablesDetector.transform.localScale = Vector3.Lerp(Vector3.zero, new Vector3(nScale, nScale, nScale), time);
            yield return new WaitForEndOfFrame();
        }
    }

    void TurnOnFog()
    {
        StartCoroutine(FadeFog(false));
        isWorldMap = false;
        //RenderSettings.fog = true;
    }

    void TurnOffFog()
    {
        StartCoroutine(FadeFog(true));
        isWorldMap = true;
        //RenderSettings.fog = false;
    }

    void ReferenceNewPlayer(Transform transform)
    {
        player = transform.GetComponent<PlayerController>();
        interactablesDetectorScale = player.interactablesDetector.transform.localScale.x;
        wandInteractableDetectorScale = player.wandInteractableDetector.transform.localScale.x;
    }

    private IEnumerator FadeFog(bool isMap)
    {
        float t = 0;
        float cFogDensity = RenderSettings.fogDensity;
        float nFogDensity = RenderSettings.fogDensity;

        if (isMap)
        {
            nFogDensity = 0;
        }
        else
        {
            nFogDensity = fogDensity;
        }

        while(t < 1)
        {
            t += Time.deltaTime / 0.5f;
            RenderSettings.fogDensity = Mathf.Lerp(cFogDensity, nFogDensity, t);
            yield return new WaitForEndOfFrame();
        }
    }

    public void EnableRayBlocker()
    {
        var cam = Camera.main;
        rayBlocker.transform.position = cam.transform.position + (cam.transform.forward * 2);
        rayBlocker.transform.rotation = cam.transform.rotation;
        rayBlocker.gameObject.SetActive(true);
    }

    public void DisableRayBlocker()
    {
        rayBlocker.gameObject.SetActive(false);
        rayBlocker.transform.position = Vector3.zero;
        rayBlocker.transform.rotation = Quaternion.identity;        
    }

    public void CheckMountainTop()
    {
        if(player.transform.position.y >= mountainTop.position.y)
        {
            player.spriter.gameObject.SetActive(false);
            wizardController.gameObject.SetActive(true);

            isOnMountainTop = true;
        }
        else
        {
            player.spriter.gameObject.SetActive(true);
            wizardController.gameObject.SetActive(false);

            isOnMountainTop = false;
        }

        print(player.transform.position.y);
    }
    /*
    private bool CheckDistance(Transform objectToCheck) // Same distance check as in Wand
    {
        float distance = player.interactablesDetector.transform.localScale.x;
        Vector3 position = player.transform.position;

        if (objectToCheck != null)
        {
            Vector3 diff = objectToCheck.position - position;
            float curDistance = diff.sqrMagnitude;

            if (curDistance < distance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }
    }
    */
    private bool CheckIfNew(GameObject transformToCheck, List<GameObject> listToCompare)
    {
        bool isNew = true;

        foreach (var item in listToCompare)
        {
            if (item == transformToCheck)
            {
                isNew = false;
            }
        }

        if (isNew)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void OnDestroy()
    {        
        StopAllCoroutines();
    }

    private void OnApplicationQuit()
    {
        //playerStats.SaveGame();
    }    
}
