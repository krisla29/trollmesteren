﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BG_BattleManager : MonoBehaviour
{
    public GameManager gameManager;
    public BG_Battle bossGameBattleScript;
    public TileGame tileGameScript;
    public BossGame bossGameScript;
    public Follower mainCamFollower;
    public Transform mainCamTarget;
    public Transform wandIconParent;
    public WandUI wandUI;
    public Transform enemy;
    public Canvas bossBattleCanvas;
    public Transform rayCatcher;
    public int completedBattles;

    public Button testFinishBtn;

    public List<Collider> colliders = new List<Collider>();
    public List<Transform> transforms = new List<Transform>();
    public List<BG_GameSet> gameSets = new List<BG_GameSet>();

    public List<Transform> objectsToActivateOnStart = new List<Transform>();
    public List<Transform> objectsToDeactivateOnStart = new List<Transform>();

    public List<Transform> objectsToActivateOnFinish = new List<Transform>();
    public List<Transform> objectsToDeactivateOnFinish = new List<Transform>();

    public Transform currentScene;

    private Vector3 offsetPosition;
    private Quaternion startRotation;
    public Transform camStartTransform;

    public GameObject winEffectPrefab;

    public AudioSource winGameSound;

    private void OnEnable()
    {
        ResetAll();

        TileGame.OnBossGameReachGoal += PlayerArrivesGoalTile;
        TileGame.OnBossGameRestart += ResetAll;
        TileGame.OnBossGameInitialized += CheckObjectsStart;
        TileGame.OnBossGameWaitForReward += Victory;
        BossGame.OnBossGameComplete += ResetProgress;
        CheckObjectsStart();
    }

    private void OnDisable()
    {

        TileGame.OnBossGameReachGoal -= PlayerArrivesGoalTile;
        TileGame.OnBossGameRestart -= ResetAll;
        TileGame.OnBossGameInitialized -= CheckObjectsStart;
        TileGame.OnBossGameWaitForReward -= Victory;
        BossGame.OnBossGameComplete -= ResetProgress;

        if (bossGameScript.soundTrackBossDefeated)
        {
            if (bossGameScript.soundTrackBossDefeated.isPlaying)
                bossGameScript.soundTrackBossDefeated.Stop();
        }

        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void ResetProgress()
    {
        completedBattles = 0;
    }

    void CheckObjectsStart()
    {
        for (int i = 0; i < objectsToActivateOnStart.Count; i++)
        {
            objectsToActivateOnStart[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < objectsToDeactivateOnStart.Count; i++)
        {
            objectsToDeactivateOnStart[i].gameObject.SetActive(false);
        }

        print("Deactivated Tiles!");
    }

    public void EnterBattle(Transform passedCollider)
    {
        print("hello!");

        for (int i = 0; i < colliders.Count; i++)
        {
            if(colliders[i].transform == passedCollider)
            {
                SetCurrentScene(transforms[i]);
                mainCamFollower.target = transforms[i];               
            }
        }

        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        wandIconParent.gameObject.SetActive(true);
        bossGameBattleScript.gameObject.SetActive(true);
        LockPlayerAgent(true);
        rayCatcher.gameObject.SetActive(true);
        gameManager.player.isBattle = true;
        bossBattleCanvas.gameObject.SetActive(true);
        tileGameScript.currentCalculation.transform.parent.gameObject.SetActive(false);

        if (tileGameScript.currentGameSet.useHistory)
            tileGameScript.undoButton.gameObject.SetActive(false);
    }

    public void ExitBattle()
    {
        gameManager.player.isBattle = false; // Call earlier for smooth transition!!!
        LockPlayerAgent(false);
        rayCatcher.gameObject.SetActive(false);
        bossBattleCanvas.gameObject.SetActive(false);
        LockAvailableTiles(false);

        mainCamFollower.target = mainCamTarget;        
        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        wandIconParent.gameObject.SetActive(false);
        tileGameScript.currentCalculation.transform.parent.gameObject.SetActive(true);

        if (tileGameScript.currentGameSet.useHistory)
        {
            tileGameScript.undoButton.gameObject.SetActive(true);
            tileGameScript.ClearHistory();
        }

        DeactivateCurrentScene();

        if (completedBattles >= gameSets.Count)
            FinishGame();
    }
    
    void SetCurrentScene(Transform passedTransform)
    {
        currentScene = passedTransform;
    }

    void DeactivateCurrentScene()
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            if(currentScene == transforms[i])
            {
                currentScene.parent.gameObject.SetActive(false); //Deactivates Scene
            }
        }

        currentScene = null;
    }

    void ReactivateScenes()
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            transforms[i].parent.gameObject.SetActive(true);
        }
    }

    public void ResetAll()
    {
        for (int i = 0; i < colliders.Count; i++)
        {
            colliders[i].gameObject.SetActive(true);
        }

        currentScene = null;

        gameManager.player.isBattle = false; // Call earlier for smooth transition!!!
        LockPlayerAgent(false);
        rayCatcher.gameObject.SetActive(false);
        bossBattleCanvas.gameObject.SetActive(false);
        LockAvailableTiles(false);        
        bossGameBattleScript.gameObject.SetActive(false);
        tileGameScript.currentCalculation.transform.parent.gameObject.SetActive(true);

        if (tileGameScript.currentGameSet)
        {
            if (tileGameScript.currentGameSet.useHistory)
                tileGameScript.undoButton.gameObject.SetActive(true);
        }

        mainCamFollower.target = mainCamTarget;
        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        ReactivateScenes();
        ResetProgress();
        //CheckObjectsStart();
    }

    public void LockPlayerAgent(bool isLocked)
    {
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = isLocked;
    }

    void PlayerArrivesGoalTile()
    {
        LockAvailableTiles(true);
    }

    void LockAvailableTiles(bool isLocked)
    {
        foreach (var tile in tileGameScript.currentlyAvailable)
        {
            tile.tileCollider.enabled = !isLocked;
        }
    }

    void FinishGame()
    {       
        bossGameScript.FinishGame();
    }

    public void EndGameObjectActivation() // Called by BossGame
    {
        for (int i = 0; i < objectsToActivateOnFinish.Count; i++)
        {
            objectsToActivateOnFinish[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < objectsToDeactivateOnFinish.Count; i++)
        {
            objectsToDeactivateOnFinish[i].gameObject.SetActive(false);
        }
    }

    void Victory()
    {
        if(bossGameScript.soundTrackBossDefeated)
        {
            if (bossGameScript.soundTrackBossDefeated.isPlaying)
                bossGameScript.soundTrackBossDefeated.Stop();
        }

        var effect = Instantiate(winEffectPrefab, gameManager.player.transform.position, Quaternion.identity, bossGameScript.effectsContainer);

        if (winGameSound)
            winGameSound.Play();
    }

}
