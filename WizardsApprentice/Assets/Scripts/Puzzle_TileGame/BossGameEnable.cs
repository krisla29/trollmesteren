﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGameEnable : MonoBehaviour
{
    public BossGame bossGameScript;

    private void OnEnable()
    {
        if (!bossGameScript.enabled)
            bossGameScript.enabled = true;
    }
}
