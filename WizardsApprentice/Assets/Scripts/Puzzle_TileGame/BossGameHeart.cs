﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGameHeart : MonoBehaviour
{
    public BossGame bossGameScript;
    public int tileIndex;
    public float lerpInTime = 1;

    private void OnEnable()
    {
        StartCoroutine(AnimateIn());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator AnimateIn()
    {
        float t = 0;

        while( t < 1)
        {
            t += Time.deltaTime / lerpInTime;
            var h = LerpUtils.SmootherStep(t);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        float ti = 0;
        float animTime = 0.25f;
        float scaleAdd = 0.25f;

        while (ti < 1)
        {
            ti += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(ti);
            var scale = Mathf.Sin(h * Mathf.PI);
            scale *= scaleAdd;
            transform.localScale = Vector3.one + (Vector3.one * scale);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator AnimateOut()
    {
        float ti = 0;
        float animTime = 0.25f;
        float scaleAdd = 0.25f;

        while (ti < 1)
        {
            ti += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(ti);
            var scale = Mathf.Sin(h * Mathf.PI);
            scale *= scaleAdd;
            transform.localScale = Vector3.one + (Vector3.one * scale);
            yield return new WaitForEndOfFrame();
        }

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / lerpInTime;
            var h = LerpUtils.SmootherStep(t);
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        CallDestroy();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            bossGameScript.bgPlayerHealth.AddHeart();
            bossGameScript.heartTiles.Remove(tileIndex);

            if (bossGameScript.gainHeart)
                bossGameScript.gainHeart.Play();

            StartCoroutine(AnimateOut());

            if(bossGameScript.playerHealth < bossGameScript.bgPlayerHealth.hearts.Count)
                bossGameScript.PlayerHeal();
        }
    }

    void CallDestroy()
    {
        Destroy(transform.gameObject);
    }
}
