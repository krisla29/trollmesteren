﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TG_Key : MonoBehaviour
{
    public TG_Tile goal;
    public TG_Tile tile;
    public int value;
    public TextMeshPro keyText;
    public MeshRenderer meshRenderer;
    public SpriteRenderer spriteRenderer;
    public List<SpriteRenderer> spriteRenderersbg = new List<SpriteRenderer>();
    public SpriteRenderer keyTextBg;
}
