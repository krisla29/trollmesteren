﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGame : MonoBehaviour
{
    public GameManager gameManager;
    public TileGame tileGameScript;
    public Canvas tileGameCanvas;
    public BG_BattleManager battleManagerScript;
    public BG_Battle battleScript;
    public BG_LifeCounter bgPlayerHealth;
    public BossController bossControllerScript;
    public Transform itemContainer;
    public Transform projectilesContainer;
    public Follower camFollower;
    public Transform rayBlocker;
    public Transform portal;

    public GameObject enemyPrefab;
    public GameObject effectEnemyPrefab;
    public BossGameHeart heartPrefab;
    public GameObject effectHeartPrefab;
    public BossGameProjectile projectilePrefab;
    public GameObject effectProjectilePrefab;
    public BossGameUtility utilityPrefab;
    public GameObject effectUtilityPrefab;

    public GameObject effectCrackSpell;
    public GameObject effectCrackTile;
    public GameObject effectLockTile;
    public GameObject effectRestoreTile;
    public GameObject effectKillEnemy;
    public GameObject effectWizardStartCast;
    public GameObject effectWizardCast;
    public GameObject effectWizardCastSpawn;
    public GameObject effectPlayerCast;
    public GameObject effectPlayerStartCast;
    public GameObject effectPlayerStartCast2;
    public GameObject effectPlayerHeal;
    public GameObject effectEnemyDefeat;

    public List<int> heartTiles = new List<int>();
    public List<int> utilityTiles = new List<int>();
    public List<int> crackedTiles = new List<int>();
    public List<int> lockedTiles = new List<int>();
    public List<int> numericalTiles = new List<int>();
    public List<int> operatorTiles = new List<int>();

    public AudioSource soundEnemySpawn;
    public AudioSource soundSpawnHeart;
    public AudioSource gainHeart;
    public AudioSource soundSpawnProjectile;
    public AudioSource soundSpawnStar;
    public AudioSource soundCracking;
    public AudioSource soundResetCracked;
    public AudioSource soundLock;
    public AudioSource soundUnlock;
    public AudioSource gainUtility;
    public AudioSource soundEnemyDie;
    public AudioSource soundProjectileDie;
    public AudioSource soundTrackBossDefeated;
    public AudioSource soundEarthQuake;
    public AudioSource soundPlayerSpell;
    public AudioSource soundEnemySpell;

    public AudioSource soundWizardDie;

    public Transform effectsContainer;

    private Coroutine enemySpell;
    private Coroutine playerSpell;
    private bool isEnemySpell;
    private bool isPlayerSpell;

    private Vector3 bossStartScale;
    private Vector3 bossStartPos;
    private Quaternion bossStartRot;
    private Vector3 currentCamTarget;

    public int movesBeforeSpawn = 1;
    public int maxEnemies = 3;
    public int maxLocked = 10;
    public int maxCracked = 10;
    public int maxHeartTiles = 3;
    public int maxUtilityTiles = 5;
    public int maxNumerical = 10;
    public int maxOperator = 6;

    public int playerHealth = 5;
    public int playerMoveCount = 0;

    public bool isComplete;

    public delegate void BossGameEvent(); //Sends to this, BG_Battle, BG_BattleManager
    public static event BossGameEvent OnBossGameComplete;

    private void Awake()
    {
        bossStartScale = bossControllerScript.transform.localScale;
        bossStartPos = bossControllerScript.transform.localPosition;
        bossStartRot = bossControllerScript.transform.localRotation;
    }

    private void OnEnable()
    {
        //if(!isComplete)
        ResetGame();

        TileGame.OnBossGameMove += UpdateMoveCount;
        TileGame.OnBossGameRestart += ClearGame;
        TileGameEnemy.OnEnemyHitPlayer += EnemyHitPlayer;
        BossGameProjectile.OnProjectileExplode += PlayProjectileExplode;
        OnBossGameComplete += SetGameInactive;
    }

    private void OnDisable()
    {
        TileGame.OnBossGameMove -= UpdateMoveCount;
        TileGame.OnBossGameRestart -= ClearGame;
        TileGameEnemy.OnEnemyHitPlayer -= EnemyHitPlayer;
        BossGameProjectile.OnProjectileExplode -= PlayProjectileExplode;
        OnBossGameComplete -= SetGameInactive;
        ClearGame();
        StopAllCoroutines();
        enemySpell = null;
        playerSpell = null;
        CancelInvoke();
        EnablePause(false);
    }

    private void OnDestroy()
    {
        ClearGame();
        StopAllCoroutines();
        enemySpell = null;
        playerSpell = null;
        CancelInvoke();
    }

    void UpdateMoveCount()
    {
        playerMoveCount = tileGameScript.currentMoveCounter;

        if (playerMoveCount % movesBeforeSpawn == 0)
        {            
            enemySpell = StartCoroutine(RandomGameEvent());
        }
    }

    void PlayProjectileExplode()
    {
        if (soundProjectileDie)
            soundProjectileDie.Play();
    }

    private IEnumerator RandomGameEvent()
    {
        while (isPlayerSpell)
            yield return new WaitForEndOfFrame();

        EnablePause(true);
        isEnemySpell = true;

        if (tileGameScript.currentGameSet.useHistory)
            tileGameScript.ClearHistory();

        int index = Random.Range(0, 8);

        Instantiate(effectWizardStartCast, bossControllerScript.transform.position, Quaternion.identity, effectsContainer);        

        bossControllerScript.SetAttackAnim();

        yield return new WaitForSeconds(1.5f);

        if (soundEnemySpell)
            soundEnemySpell.Play();

        if (index <= 3)
            WizardSpawnSpell();
        else
            WizardCastSpell();

        yield return new WaitForSeconds(1.5f);

        switch (index)
        {
            case (0):                
                SpawnHeart();
                break;
            case (1):
                SpawnEnemy();
                break;
            case (2):
                SpawnProjectile();
                break;
            case (3):
                SpawnUtility();
                break;            
            case (4):
                SetCrackedTiles();
                break;
            case (5):
                SetLockedTiles();
                break;
            case (6):
                SetNumericalTiles();
                break;
            case (7):
                SetOperatorTiles();
                break;
            default:
                break;
        }

        yield return new WaitForSeconds(3);

        EnablePause(false);
        isEnemySpell = false;
    }

    void WizardCastSpell()
    {
        Instantiate(effectWizardCast, bossControllerScript.transform.position, Quaternion.identity, effectsContainer);
    }

    void WizardSpawnSpell()
    {
        Instantiate(effectWizardCastSpawn, bossControllerScript.transform.position + (bossControllerScript.transform.forward * 3), Quaternion.identity, effectsContainer);
    }

    void Dud()
    {
        print("SPELL - DUD!");

        bossControllerScript.SetFailAnim();
    }

    void Nothing()
    {

    }

    void SetCrackedTiles()
    {
        if (crackedTiles.Count < maxCracked)
        {
            print("SPELL - CRACKING TILES!");

            int randNum = Random.Range(1, Mathf.Clamp((maxCracked - crackedTiles.Count), 1, maxCracked));

            List<int> indexes = CheckAvailableTiles(randNum);

            Vector3[] positions = new Vector3[indexes.Count];

            for (int i = 0; i < indexes.Count; i++)
            {
                int randTime = Random.Range(playerMoveCount + 1, playerMoveCount + 11);
                var tile = tileGameScript.tiles[indexes[i]];
                StartCoroutine(CrackEffect(tile, 0.2f * i));
                tileGameScript.currentSinkerTiles.Add(tileGameScript.tiles[indexes[i]]);
                positions[i] = tileGameScript.tiles[indexes[i]].transform.position;
                tileGameScript.currentSinkTilesTimes.Add(randTime);
                tileGameScript.currentSinkTiles.Add(false);
                var tileTransform = tileGameScript.tiles[indexes[i]].transform;
                var crackedObject = Instantiate(tileGameScript.crackedPrefab, tileTransform.position, tileTransform.rotation, tileGameScript.tiles[indexes[i]].transform);
                tileGameScript.currentCrackedPrefabs.Add(crackedObject.transform);
                crackedTiles.Add(indexes[i]);
            }

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);
        }
        else
        {
            if (lockedTiles.Count < maxLocked)
                SetLockedTiles();
            else
                SpawnUtility();
        }
    }

    private IEnumerator CrackEffect(TG_Tile tile, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        var effect = Instantiate(effectCrackTile, tile.transform.position, tile.transform.rotation, effectsContainer);

        if (soundCracking)
            soundCracking.Play();
    }

    void ResetCrackedTiles()
    {
        if (crackedTiles.Count > 0)
        {
            print("SPELL - UN-CRACKING TILES!");

            Vector3[] positions = new Vector3[crackedTiles.Count];

            for (int i = crackedTiles.Count - 1; i > -1; i--)
            {
                var tile = tileGameScript.tiles[crackedTiles[i]];
                positions[i] = tile.transform.position;
                StartCoroutine(RestoreTileEffect(tile, 0.2f * i));
                int tgCrackedIndex = tileGameScript.currentSinkerTiles.IndexOf(tile);
                tileGameScript.currentSinkerTiles.Remove(tileGameScript.currentSinkerTiles[tgCrackedIndex]);
                tileGameScript.currentSinkTilesTimes.Remove(tileGameScript.currentSinkTilesTimes[tgCrackedIndex]);
                tileGameScript.currentSinkTiles.Remove(tileGameScript.currentSinkTiles[tgCrackedIndex]);
                tileGameScript.SetUnavailable(tile);
                var crackObject = tileGameScript.currentCrackedPrefabs[tgCrackedIndex].gameObject;
                tileGameScript.currentCrackedPrefabs.Remove(tileGameScript.currentCrackedPrefabs[tgCrackedIndex]);
                Destroy(crackObject);
            }

            crackedTiles.Clear();

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);

            tileGameScript.ResetTileHeight();

            bossControllerScript.SetFailAnim();
        }
        else
        {
            int index = Random.Range(0, 2);
            
            if(index == 0)
            {
                if (lockedTiles.Count > 0)
                    ResetLockedTiles();
                else if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else
                    Nothing();
            }
            else
            {
                if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else if (lockedTiles.Count > 0)
                    ResetLockedTiles();
                else
                    Nothing();
            }
        }
    }

    void SetLockedTiles()
    {
        if (lockedTiles.Count < maxLocked)
        {
            print("SPELL - LOCKING TILES!");

            int randNum = Random.Range(1, Mathf.Clamp((maxLocked - lockedTiles.Count), 1, maxLocked));

            List<int> indexes = CheckAvailableTiles(randNum);

            Vector3[] positions = new Vector3[indexes.Count];

            for (int i = 0; i < indexes.Count; i++)
            {
                var tile = tileGameScript.tiles[indexes[i]];
                lockedTiles.Add(indexes[i]);
                tileGameScript.SetLocked(tile, 0, TileGame.TileTypes.None, TileGame.OperatorTypes.None);
                positions[i] = tileGameScript.tiles[indexes[i]].transform.position;
                StartCoroutine(LockEffect(tile, 0.2f * i));
            }

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);
        }
        else
        {
            if (crackedTiles.Count < maxCracked)
                SetCrackedTiles();
            else
                SpawnUtility();
        }
    }

    private IEnumerator LockEffect(TG_Tile tile, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        var effect = Instantiate(effectLockTile, tile.transform.position, tile.transform.rotation, effectsContainer);

        if (soundLock)
            soundLock.Play();
    }

    private IEnumerator RestoreTileEffect(TG_Tile tile, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        var effect = Instantiate(effectRestoreTile, tile.transform.position, tile.transform.rotation, effectsContainer);

        if (soundUnlock)
            soundUnlock.Play();
    }


    void ResetLockedTiles()
    {
        if (lockedTiles.Count > 0)
        {
            print("SPELL - UN-LOCKING TILES!");

            Vector3[] positions = new Vector3[lockedTiles.Count];

            for (int i = 0; i < lockedTiles.Count; i++)
            {
                var tile = tileGameScript.tiles[lockedTiles[i]];
                StartCoroutine(RestoreTileEffect(tile, 0.2f * i));
                positions[i] = tile.transform.position;
                tileGameScript.SetUnavailable(tile);
            }

            lockedTiles.Clear();

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);

            bossControllerScript.SetFailAnim();
        }
        else
        {
            int index = Random.Range(0, 2);

            if(index == 0)
            {
                if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else
                    Nothing();
            }
            else
            {
                if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else
                    Nothing();
            }
        }
    }

    void SetNumericalTiles()
    {
        if (numericalTiles.Count < maxNumerical)
        {
            print("SPELL - SET NUMERICAL TILES!");

            int randNum = Random.Range(1, Mathf.Clamp((maxNumerical - numericalTiles.Count), 1, maxNumerical));

            List<int> indexes = CheckAvailableTiles(randNum);
            indexes = CheckAvailableStaticTiles(indexes, false);

            Vector3[] positions = new Vector3[indexes.Count];

            for (int i = 0; i < indexes.Count; i++)
            {
                int randomValue = Random.Range(-99, 100);
                var tile = tileGameScript.tiles[indexes[i]];
                numericalTiles.Add(indexes[i]);
                tileGameScript.SetLocked(tile, randomValue, TileGame.TileTypes.NumericalStatic, TileGame.OperatorTypes.None);
                positions[i] = tileGameScript.tiles[indexes[i]].transform.position;
                StartCoroutine(LockEffect(tile, 0.2f * i));
            }

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);
        }
        else
        {
            if (crackedTiles.Count < maxCracked)
                SetCrackedTiles();
            else
                SpawnUtility();
        }
    }

    void ResetNumericalTiles()
    {
        if (numericalTiles.Count > 0)
        {
            print("SPELL - UN-LOCKING NUMERICAL TILES!");

            Vector3[] positions = new Vector3[numericalTiles.Count];

            for (int i = 0; i < numericalTiles.Count; i++)
            {
                var tile = tileGameScript.tiles[numericalTiles[i]];
                StartCoroutine(RestoreTileEffect(tile, 0.2f * i));
                positions[i] = tile.transform.position;
                tileGameScript.SetUnavailable(tile);
            }

            numericalTiles.Clear();

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);

            bossControllerScript.SetFailAnim();
        }
        else
        {
            int index = Random.Range(0, 2);

            if (index == 0)
            {
                if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else
                    Nothing();
            }
            else
            {
                if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else
                    Nothing();
            }
        }
    }

    void SetOperatorTiles()
    {
        if (operatorTiles.Count < maxOperator)
        {
            print("SPELL - SET NUMERICAL TILES!");

            int randNum = Random.Range(1, Mathf.Clamp((maxOperator - operatorTiles.Count), 1, maxOperator));

            List<int> indexes = CheckAvailableTiles(randNum);
            indexes = CheckAvailableStaticTiles(indexes, true);

            Vector3[] positions = new Vector3[indexes.Count];

            for (int i = 0; i < indexes.Count; i++)
            {
                int randomValue = Random.Range(0, 4);
                var randomOperator = TileGame.OperatorTypes.Add;

                switch(randomValue)
                {
                    case (0):
                        randomOperator = TileGame.OperatorTypes.Add;
                        break;
                    case (1):
                        randomOperator = TileGame.OperatorTypes.Subtract;
                        break;
                    case (2):
                        randomOperator = TileGame.OperatorTypes.Multiply;
                        break;
                    case (3):
                        randomOperator = TileGame.OperatorTypes.Divide;
                        break;
                    default:
                        randomOperator = TileGame.OperatorTypes.Add;
                        break;
                }

                var tile = tileGameScript.tiles[indexes[i]];
                operatorTiles.Add(indexes[i]);
                tileGameScript.SetLocked(tile, 0, TileGame.TileTypes.OperatorStatic, randomOperator);
                positions[i] = tileGameScript.tiles[indexes[i]].transform.position;
                StartCoroutine(LockEffect(tile, 0.2f * i));
            }

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);
        }
        else
        {
            if (crackedTiles.Count < maxCracked)
                SetCrackedTiles();
            else
                SpawnUtility();
        }
    }

    void ResetOperatorTiles()
    {
        if (operatorTiles.Count > 0)
        {
            print("SPELL - UN-LOCKING OPERATOR TILES!");

            Vector3[] positions = new Vector3[operatorTiles.Count];

            for (int i = 0; i < operatorTiles.Count; i++)
            {
                var tile = tileGameScript.tiles[operatorTiles[i]];
                StartCoroutine(RestoreTileEffect(tile, 0.2f * i));
                positions[i] = tile.transform.position;
                tileGameScript.SetUnavailable(tile);
            }

            operatorTiles.Clear();

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);

            bossControllerScript.SetFailAnim();
        }
        else
        {
            int index = Random.Range(0, 2);

            if (index == 0)
            {
                if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else
                    Nothing();
            }
            else
            {
                if (tileGameScript.currentEnemies.Count > 0)
                    KillEnemies();
                else if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else
                    Nothing();
            }
        }
    }

    void SpawnHeart()
    {
        if (heartTiles.Count < maxHeartTiles)
        {
            print("SPELL - SPAWNS HEART!");

            List<int> index = CheckAvailableTiles(1);
            var tile = tileGameScript.tiles[index[0]];
            heartTiles.Add(index[0]);

            var newHeart = Instantiate(heartPrefab, tile.transform.position, Quaternion.identity, itemContainer);
            var effect = Instantiate(effectProjectilePrefab, tile.transform.position, Quaternion.identity, effectsContainer);
            newHeart.bossGameScript = this;
            newHeart.tileIndex = index[0];
            bossControllerScript.SetFailAnim();

            SetCamTarget(newHeart.transform.position);
        }
        else
        {
            if (utilityTiles.Count < maxUtilityTiles)
                SpawnUtility();
            else
                SpawnEnemy();
        }
    }

    void SpawnUtility()
    {
        if (utilityTiles.Count < maxUtilityTiles)
        {
            print("SPELL - SPAWNS UTILITY!");

            if (soundSpawnStar)
                soundSpawnStar.Play();

            List<int> index = CheckAvailableTiles(1);
            var tile = tileGameScript.tiles[index[0]];
            var effect = Instantiate(effectEnemyPrefab, tile.transform.position, Quaternion.identity, effectsContainer);
            utilityTiles.Add(index[0]);

            var newUtility = Instantiate(utilityPrefab, tile.transform.position, Quaternion.identity, itemContainer);
            newUtility.bossGameScript = this;
            newUtility.tileIndex = index[0];
            bossControllerScript.SetFailAnim();

            SetCamTarget(newUtility.transform.position);
        }
        else
        {
            if (heartTiles.Count < maxHeartTiles)
                SpawnHeart();
            else
                SpawnEnemy();
        }
    }

    void SpawnEnemy()
    {        
        if (tileGameScript.currentEnemies.Count < maxEnemies)
        {
            print("SPELL - SPAWNS ENEMY!");

            List<int> tileIndex = CheckAvailableTiles(1);

            var enemyStartTile = tileGameScript.tiles[tileIndex[0]];
            var effect = Instantiate(effectEnemyPrefab, enemyStartTile.transform.position, Quaternion.identity, effectsContainer);
            var enemy = Instantiate(enemyPrefab, enemyStartTile.transform.position, tileGameScript.gameTransform.rotation * Quaternion.Euler(0, 180, 0), tileGameScript.gameTransform);
            var enemyScript = enemy.GetComponent<TileGameEnemy>();
            enemyScript.tileGameScript = tileGameScript;
            enemyScript.enemyIndex = tileGameScript.currentEnemies.Count;
            enemyScript.startTile = enemyStartTile;
            enemyScript.enemyCurrentTile = enemyStartTile;

            tileGameScript.currentEnemies.Add(enemyScript);

            if (soundEnemySpawn)
                soundEnemySpawn.Play();

            SetCamTarget(enemy.transform.position);           
        }
        else
        {
            if (lockedTiles.Count < maxLocked)
                SetLockedTiles();
            else if (crackedTiles.Count < maxCracked)
                SetCrackedTiles();
            else
                SpawnProjectile();
        }
    }

    void KillEnemies()
    {       
        if (tileGameScript.currentEnemies.Count > 0)
        {
            print("SPELL - KILL ENEMIES!");

            Vector3[] positions = new Vector3[tileGameScript.currentEnemies.Count + projectilesContainer.childCount];

            for (int i = tileGameScript.currentEnemies.Count - 1; i > -1; i--)
            {
                var enemy = tileGameScript.currentEnemies[i].gameObject;
                positions[i] = enemy.transform.position;
                StartCoroutine(KillEnemiesEffect(enemy.transform.position, 0.2f, true));
                tileGameScript.currentEnemies.Remove(tileGameScript.currentEnemies[i]);
                Destroy(enemy);
            }

            for (int i = projectilesContainer.childCount - 1; i > -1; i--)
            {
                var projectile = projectilesContainer.GetChild(i).gameObject;
                positions[i] = projectile.transform.position;
                StartCoroutine(KillEnemiesEffect(projectile.transform.position, 0.2f, false));
                Destroy(projectile);
            }

            Vector3 pos = HelperFunctions.FindCenterPoint(positions);

            if (pos != Vector3.zero)
                SetCamTarget(pos);


            bossControllerScript.SetFailAnim();
        }
        else
        {
            int index = Random.Range(0, 2);

            if(index == 0)
            {
                if (crackedTiles.Count > 0)
                    ResetCrackedTiles();
                else
                    ResetLockedTiles();
            }
            else
            {
                if (lockedTiles.Count > 0)
                    ResetLockedTiles();
                else
                    ResetCrackedTiles();
            }
        }
    }

    private IEnumerator KillEnemiesEffect(Vector3 enemyPos, float waitTime, bool isEnemy)
    {
        yield return new WaitForSeconds(waitTime);

        var effect = Instantiate(effectKillEnemy, enemyPos, Quaternion.identity, effectsContainer);

        if (isEnemy)
        {
            if (soundEnemyDie)
                soundEnemyDie.Play();
        }
        else
        {
            if (soundProjectileDie)
                soundProjectileDie.Play();
        }
    }

    void SpawnProjectile()
    {
        print("SPELL - SPAWNS PROJECTILE!");

        var randomX = Random.Range(-1f, -0.1f);
        var randomZ = Random.Range(0.1f, 1f);

        var newDir = Vector3.zero;

        int rand = Random.Range(0, 2);

        if (rand == 0)
            newDir = Vector3.Normalize(new Vector3(randomX, 0, randomZ));
        else
            newDir = Vector3.Normalize(new Vector3(randomZ, 0, randomX));

        var startPoint = itemContainer.TransformPoint(newDir * 8f);
        var endPoint = itemContainer.TransformPoint(-newDir * 8f);

        var projectile = Instantiate(projectilePrefab, projectilesContainer);
        projectile.bossGameScript = this;
        projectile.startPos = startPoint;
        projectile.endPos = endPoint;
        projectile.transform.position = startPoint;
        var effect = Instantiate(effectProjectilePrefab, projectile.transform.position + (Vector3.up), Quaternion.identity, effectsContainer);

        if (soundSpawnProjectile)
            soundSpawnProjectile.Play();

        SetCamTarget(projectile.transform.position);
    }

    void ResetGame()
    {
        playerHealth = 5;
        playerMoveCount = 0;
        bgPlayerHealth.SetToAllFull();
        bossControllerScript.ResetFinalDefeat();
        bossControllerScript.gameObject.SetActive(true);
        bossControllerScript.transform.localScale = bossStartScale;
        bossControllerScript.transform.localPosition = bossStartPos;
        bossControllerScript.transform.localRotation = bossStartRot;
    }

    private List<int> CheckAvailableTiles(int tileCount)
    {
        List<int> indexes = new List<int>();

        var tempTileList = new List<int>();

        for (int i = 0; i < tileGameScript.tiles.Count; i++)
        {
            bool check = false;

            if (tileGameScript.tiles[i].gameObject.activeSelf)
            {
                if (!heartTiles.Contains(i) && !crackedTiles.Contains(i) && !lockedTiles.Contains(i) && !numericalTiles.Contains(i) && !operatorTiles.Contains(i) && !utilityTiles.Contains(i))
                {
                    if (tileGameScript.tiles[i].tileType == TileGame.TileTypes.None)
                    {
                        if (tileGameScript.tiles[i].tileState == TileGame.TileStates.Unavailable)
                        {
                            if (tileGameScript.tiles[i] != tileGameScript.currentTile)
                            {
                                bool distChecks = true;
                                float dist = 0.5f;

                                foreach (var item in tileGameScript.currentEnemies)
                                {
                                    if ((item.transform.position - tileGameScript.tiles[i].transform.position).sqrMagnitude < dist * dist)
                                        distChecks = false;
                                }

                                if(distChecks)
                                {
                                    if ((tileGameScript.tiles[i].transform.position - gameManager.player.transform.position).sqrMagnitude < dist * dist)
                                        distChecks = false;
                                }

                                if(distChecks)
                                    check = true;
                            }
                        }
                    }
                }
            }

            if (check)
                tempTileList.Add(i);
        }

        for (int i = 0; i < tileCount; i++)
        {
            if (tempTileList.Count > 0)
            {
                var index = Random.Range(0, tempTileList.Count);
                indexes.Add(tempTileList[index]);
                tempTileList.Remove(tempTileList[index]);
            }
        }
        
        return indexes;
    }

    private List<int> CheckAvailableStaticTiles(List<int> passedTileIndexes, bool isOperators)
    {
        List<int> tempList= new List<int>();
        
        for (int i = 0; i < passedTileIndexes.Count; i++)
        {
            tempList.Add(passedTileIndexes[i]);
        }
        /*
        for (int i = tempList.Count - 1; i > -1; i--)
        {
            bool check = true;

            for(int e = 0; e < tileGameScript.tiles[tempList[i]].connectedTiles.Count; e++)
            {
                if(tileGameScript.tiles[tempList[i]].connectedTiles[e].tileType != TileGame.TileTypes.None)
                {
                    check = false;
                }
            }

            if (!check)
                tempList.Remove(tempList[i]);
        }
        */
        return tempList;
    }

    private IEnumerator SpawnEnemyAnim(GameObject passedEnemy)
    {
        if (soundEnemySpawn)
            soundEnemySpawn.Play();

        float t = 0;
        float animTime = 1;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmoothStep(t);
            passedEnemy.transform.localScale = Vector3.Lerp(Vector3.one * 0.01f, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }

    void EnemyHitPlayer()
    {
        bgPlayerHealth.RemoveHeart();
    }

    public void ClearGame()
    {
        foreach (Transform child in itemContainer)
        {
            Destroy(child.gameObject);
        }

        foreach (Transform child in effectsContainer)
        {
            Destroy(child.gameObject);
        }

        foreach (Transform child in projectilesContainer)
        {
            Destroy(child.gameObject);
        }

        heartTiles.Clear();
        utilityTiles.Clear();
        crackedTiles.Clear();
        lockedTiles.Clear();
        numericalTiles.Clear();
        operatorTiles.Clear();
        bgPlayerHealth.transform.gameObject.SetActive(false);
        bgPlayerHealth.transform.gameObject.SetActive(true);
        //if(bgPlayerHealth.transform.gameObject.activeSelf)
        //bgPlayerHealth.SetToAllFull();

        ResetCamTarget();
    }

    public void CallGameOver()
    {
        print("GAME OVER, FOOL!!!");

        StartCoroutine(GameOver());
    }

    private IEnumerator GameOver()
    {
        float t = 0;
        float waitTime = 2;

        while(t < 1)
        {
            t += Time.deltaTime / waitTime;
            yield return new WaitForEndOfFrame();
        }

        ClearGame();
        ResetGame();
        tileGameScript.ResetCurrentGameSet();
    }

    private IEnumerator WaitBeforeResetCam()
    {
        yield return new WaitForSeconds(3);

        ResetCamTarget();
    }

    void ResetCamTarget()
    {
        StopCoroutine(LerpCamToTarget());
        StopCoroutine(WaitBeforeResetCam());
        currentCamTarget = Vector3.zero;
        camFollower.enabled = true;        
    }

    void SetCamTarget(Vector3 targetPos)
    {
        currentCamTarget = targetPos;
        StopCoroutine(LerpCamToTarget());
        StartCoroutine(LerpCamToTarget());

        StopCoroutine(WaitBeforeResetCam());
        StartCoroutine(WaitBeforeResetCam());
    }

    void SetCamTargetHard(Vector3 targetPos)
    {
        currentCamTarget = targetPos;
        StopCoroutine(LerpCamToTarget());
        StartCoroutine(LerpCamToTarget());
    }    
    
    private IEnumerator LerpCamToTarget()
    {
        camFollower.enabled = false;

        float t = 0;
        float animTime = 2f;
        Vector3 startPos = camFollower.transform.position;
        Vector3 planarForward = Vector3.Normalize(new Vector3(camFollower.transform.forward.x, 0, camFollower.transform.forward.z));
        Vector3 dist = -planarForward * 7f;
        Vector3 endPos = new Vector3(currentCamTarget.x, camFollower.transform.position.y, currentCamTarget.z) + dist;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            camFollower.transform.position = Vector3.Lerp(startPos, endPos, h);
            yield return new WaitForEndOfFrame();
        }
    }

    public void CastUtility()
    {      
        playerSpell = StartCoroutine(UtilityAnim());
    }

    private IEnumerator UtilityAnim()
    {
        while (isEnemySpell)
            yield return new WaitForEndOfFrame();

        isPlayerSpell = true;
        EnablePause(true);

        var effect = Instantiate(effectPlayerStartCast, effectsContainer);
        effect.transform.position = tileGameScript.currentTile.transform.position;

        StopCoroutine(RotatePlayer());
        StartCoroutine(RotatePlayer());

        gameManager.player.anim.SetTrigger("CastSpell");

        yield return new WaitForSeconds(0.5f);

        PlayerThrowStar();

        if (gainUtility)
            gainUtility.Play();

        int index = Random.Range(0, 5);

        yield return new WaitForSeconds(1.5f);

        if (soundPlayerSpell)
            soundPlayerSpell.Play();

        PlayerCastSpell();

        yield return new WaitForSeconds(1.5f);

        switch (index)
        {
            case (0):
                ResetCrackedTiles();
                break;
            case (1):
                ResetLockedTiles();
                break;
            case (2):
                ResetNumericalTiles();
                break;
            case (3):
                ResetOperatorTiles();
                break;
            case (4):
                KillEnemies();
                break;
            default:
                break;
        }

        yield return new WaitForSeconds(3f);

        EnablePause(false);
        isPlayerSpell = false;
    }

    private IEnumerator RotatePlayer()
    {
        float t = 0;
        float animTime = 0.5f;
        var dir = camFollower.transform.position - gameManager.player.transform.position;
        dir = new Vector3(dir.x, 0, dir.z);
        Quaternion look = Quaternion.LookRotation(dir);
        Quaternion startRot = gameManager.player.transform.rotation;

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            gameManager.player.transform.rotation = Quaternion.Slerp(startRot, look, h);
            yield return new WaitForEndOfFrame();
        }
    }

    void PlayerThrowStar()
    {
        var effect = Instantiate(effectPlayerStartCast2, gameManager.player.transform.position, Quaternion.identity, effectsContainer);
    }

    void PlayerCastSpell()
    {
        Instantiate(effectPlayerCast, gameManager.player.transform.position, Quaternion.identity, effectsContainer);
    }

    public void PlayerHeal()
    {
        Invoke("HealAnim", 1);
        Invoke("StartPlayerRotation", 0.5f);
    }

    void HealAnim()
    {
        var effect = Instantiate(effectPlayerHeal, gameManager.player.transform.position, Quaternion.identity, effectsContainer);
    }

    void StartPlayerRotation()
    {
        StopCoroutine(RotatePlayer());
        StartCoroutine(RotatePlayer());
    }

    void EnablePause(bool isPause)
    {
        if(isPause)
        {
            rayBlocker.gameObject.SetActive(true);
            if (gameManager.hudCanvas)
                gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);
            tileGameCanvas.enabled = false;
        }
        else
        {
            rayBlocker.gameObject.SetActive(false);
            if(gameManager.hudCanvas)
                gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);
            tileGameCanvas.enabled = true;
        }
    }

    public void FinishGame() // Called by BG_BattleManager
    {
        StartCoroutine(CompleteGame());
    }

    private IEnumerator CompleteGame()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        var scene = gameManager.currentScene;

        if (scene.soundTrack)
        {
            if (scene.soundTrack.isPlaying)
                scene.soundTrack.Stop();
        }

        EnablePause(true);
        
        yield return new WaitForSeconds(1);

        var effect = Instantiate(effectPlayerStartCast, effectsContainer);
        effect.transform.position = tileGameScript.currentTile.transform.position;

        StopCoroutine(RotatePlayer());
        StartCoroutine(RotatePlayer());

        gameManager.player.anim.SetTrigger("CastSpell");

        yield return new WaitForSeconds(0.5f);

        PlayerThrowStar();

        if (gainUtility)
            gainUtility.Play();

        int index = Random.Range(0, 5);

        yield return new WaitForSeconds(1.5f);

        if (soundPlayerSpell)
            soundPlayerSpell.Play();

        if (soundEarthQuake)
            soundEarthQuake.Play();

        PlayerCastSpell();

        yield return new WaitForSeconds(1.5f);

        ResetCrackedTiles();

        yield return new WaitForSeconds(2);

        ResetLockedTiles();

        yield return new WaitForSeconds(2);

        ResetNumericalTiles();

        yield return new WaitForSeconds(2);

        ResetOperatorTiles();

        yield return new WaitForSeconds(2);

        KillEnemies();       

        yield return new WaitForSeconds(2);

        SetCamTargetHard(bossControllerScript.transform.position);

        yield return new WaitForSeconds(2);

        GameObject defeatEffect = null;

        if (effectEnemyDefeat)
            defeatEffect = Instantiate(effectEnemyDefeat, bossControllerScript.transform.position, Quaternion.identity, effectsContainer);

        if (soundWizardDie)
            soundWizardDie.Play();

        bossControllerScript.SetFinalDefeat();
        /*
        float x = 0;
        float rotTime = 0.5f;
        Quaternion curRot = 

        while(x < 1)
        {
            x += Time.deltaTime / rotTime;
            var h = LerpUtils.SmootherStep(x);
            bossControllerScript.transform.rotation = 
        }
        */
        float t = 0;
        float animTime = 3;
        Vector3 bossCurrentSize = bossControllerScript.transform.localScale;

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            bossControllerScript.transform.localScale = Vector3.Lerp(bossCurrentSize, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        bossControllerScript.gameObject.SetActive(false);

        yield return new WaitForSeconds(1);

        ResetCamTarget();
        battleManagerScript.EndGameObjectActivation();

        if (soundTrackBossDefeated)
            soundTrackBossDefeated.Play();

        EnablePause(false);
        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);

        if (OnBossGameComplete != null)
            OnBossGameComplete();
    }

    void SetGameInactive()
    {
        isComplete = true;
        this.enabled = false;
    }
}
