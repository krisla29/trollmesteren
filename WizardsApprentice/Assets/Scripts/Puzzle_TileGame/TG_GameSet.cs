﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TG_GameSet : MonoBehaviour
{
    [Header("Description")]
    [TextArea(2, 10)]
    public string description;    
    [Header("Game Style")]
    [Tooltip("Whether or not to remove trail")]
    public TileGame.TileGameStyles gameStyle; // Whether or not to remove trail
    [Header("Set Tiles")]
    [Tooltip("List of ranges (inclusive min, inclusive max) of playable tiles in set. Please ensure that other tiles in set are within these ranges.")]
    public List<Vector2Int> setTiles = new List<Vector2Int>(); //List of ranges (inclusive min, inclusive max) of playable tiles in set. Please ensure that other tiles in set are within these ranges.
    [Header("Start Tiles")]
    [Tooltip("First selectable tiles in set")]
    public List<int> startTiles = new List<int>(); // First selectable tiles in set
    [Tooltip("Has startTile value to be set, Must be same size as startTiles!")]
    public List<bool> startIsNumerical = new List<bool>(); // Has startTile value to be set, Must be same size as startTiles!
    [Tooltip("Value of each startTile, Must be same size as startTiles!")]
    public List<int> startValues = new List<int>(); // Value of each startTile, Must be same size as startTiles!
    [Header("Goal Tiles")]
    [Tooltip("Tiles that must be visited to win set")]
    public List<int> goalTiles = new List<int>(); // Tiles that must be visited to win set
    [Tooltip("Value of each goal, Must be same size as goalTiles!")]
    public List<int> goalValues = new List<int>(); // Value of each goal, Must be same size as goalTiles!
    [Tooltip("Whether or not goalTiles locks value after completion, Must be same size as goalTiles!")]
    public List<bool> goalsLock = new List<bool>(); // Whether or not goalTiles locks value after completion, Must be same size as goalTiles!
    [Header("Goal Key Tiles")]
    [Tooltip("Tiles that must be visited for each goal, Must be same size as goalTiles!")]
    public List<TileGame.GoalKeyTile> goalKeyTiles = new List<TileGame.GoalKeyTile>(); // Tiles that must be visited for each goal, Must be same size as goalTiles! If goalKeyTile, all goals must have Keys
    [Tooltip("Uncheck to hide keytiles!")]
    public bool showKeyTiles = true;
    [Header("Locked Tiles")]
    [Tooltip("Various properties, depending on tileType. Set to None to lock tile completely")]
    public List<int> lockedTiles = new List<int>(); // Various properties, depending on tileType. Set to None to lock tile completely 
    [Tooltip("Must be same size as lockedTiles!")]
    public List<int> lockedTValues = new List<int>(); // Must be same size as lockedTiles! 
    [Tooltip("Must be same size as lockedTiles!")]
    public List<TileGame.TileTypes> lockedTTypes = new List<TileGame.TileTypes>(); // Must be same size as lockedTiles! 
    [Tooltip("Must be same size as lockedTiles!")]
    public List<TileGame.OperatorTypes> lockedOperatorTypes = new List<TileGame.OperatorTypes>(); // Must be same size as lockedTiles!
    [Header("Forbidden Input")]
    [Tooltip("Numbers that are skipped in Slider")]
    public List<int> forbiddenNumbers = new List<int>(); // Numbers that are skipped in Slider
    [Tooltip("Operators that are skipped in slider")]
    public List<TileGame.OperatorTypes> forbiddenOperators = new List<TileGame.OperatorTypes>(); // Operators that are skipped in slider
    [Header("Enemies")]
    [Tooltip("Number of enemies and starting tile indexes")]
    public List<int> enemies = new List<int>(); // Number of enemies and starting tile indexes
    [Header("SinkTiles")]
    [Tooltip("Number of tiles to sink")] // Number of tiles to sink
    public List<int> sinkTiles = new List<int>();
    [Tooltip("MoveCount to sink indexed sinkTile. Must be same size as sinkTiles!")] // MoveCount to sink indexed sinkTile. Must be same size as sinkTiles!
    public List<int> sinkTilesTime = new List<int>();
    [Header("Maximum Moves")]
    [Tooltip("Required maximum move count to win Game Set. If set to 0, then unlimited moves")]
    public int maximumMoves = 0; //Required maximum move count to win Game Set. If set to 0, then unlimited moves
    [Header("Target Moves")]
    [Tooltip("Sets the minimal target move count, where less than x gets lowest reward and less than z gets highest")]
    public Vector3Int targetMoves = new Vector3Int(3, 2 , 1); //Sets the minimal target move count, where less than x gets lowest reward and less than z gets highest
    [Header("History")]
    public bool useHistory = true;
}
