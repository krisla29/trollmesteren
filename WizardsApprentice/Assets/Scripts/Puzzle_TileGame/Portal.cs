﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    public GameManager gameManager;
    public Canvas portalCanvas;
    public string finishScene;
    public AudioSource soundMenuOpen;

    private void OnMouseDown()
    {
        if(!portalCanvas.gameObject.activeSelf)
            OpenDialogueBox();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!portalCanvas.gameObject.activeSelf)
                OpenDialogueBox();
        }
    }

    void OpenDialogueBox()
    {
        Time.timeScale = 0;
        portalCanvas.gameObject.SetActive(true);

        if (soundMenuOpen)
            soundMenuOpen.Play();
    }

    void CloseDialogueBox()
    {
        Time.timeScale = 1;
        portalCanvas.gameObject.SetActive(false);
    }

    public void Confirm()
    {
        Time.timeScale = 1;
        gameManager.playerStats.SaveGame();
        SceneManager.LoadScene(finishScene);
    }

    public void Cancel()
    {
        CloseDialogueBox();
    }

    public void OnEnable()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Time.timeScale = 1;

        if(gameManager && gameManager.hudCanvas && gameManager.hudCanvas.exitButton.gameObject)
        {
            gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        }        
    }
}
