﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossGameGoalsIndicator : MonoBehaviour
{
    public int goalLeftValue = 65;
    public Transform goalLeft;
    public Image goalLeftImg;
    public int goalRightValue = 58;
    public Transform goalRight;
    public Image goalRightImg;
    public int goalTopValue = 11;
    public Transform goalTop;
    public Image goalTopImg;

    public void OnEnable()
    {
        TileGame.OnBossGameReachSubGoal += CheckGoalTransform;
        TileGame.OnBossGameInitialized += ResetIndicator;
        TileGame.OnBossGameRestart += ResetIndicator;
        TileGame.OnBossTileGameEnable += ResetIndicator;
    }

    public void OnDisable()
    {
        TileGame.OnBossGameReachSubGoal -= CheckGoalTransform;
        TileGame.OnBossGameInitialized -= ResetIndicator;
        TileGame.OnBossGameRestart -= ResetIndicator;
        TileGame.OnBossTileGameEnable -= ResetIndicator;
    }

    public void CheckGoalTransform(int passedValue)
    {
        print("Check BossGoalValue");

        if(passedValue == goalLeftValue)
        {
            print("goal Left!");
            goalLeftImg.transform.GetChild(0).gameObject.SetActive(false);
            goalLeftImg.transform.GetChild(1).gameObject.SetActive(true);
        }
        else if(passedValue == goalRightValue)
        {
            print("goal Right!");
            goalRightImg.transform.GetChild(0).gameObject.SetActive(false);
            goalRightImg.transform.GetChild(1).gameObject.SetActive(true);
        }
        else if(passedValue == goalTopValue)
        {
            print("goal Top!");
            goalTopImg.transform.GetChild(0).gameObject.SetActive(false);
            goalTopImg.transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    void ResetIndicator()
    {
        print("Resetting BossGoal Indicator");

        goalLeftImg.transform.GetChild(1).gameObject.SetActive(false);
        goalRightImg.transform.GetChild(1).gameObject.SetActive(false);
        goalTopImg.transform.GetChild(1).gameObject.SetActive(false);

        goalLeftImg.transform.GetChild(0).gameObject.SetActive(true);
        goalRightImg.transform.GetChild(0).gameObject.SetActive(true);
        goalTopImg.transform.GetChild(0).gameObject.SetActive(true);
    }
}
