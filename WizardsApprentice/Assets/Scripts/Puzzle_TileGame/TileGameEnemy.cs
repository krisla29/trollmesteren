﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TileGameEnemy : MonoBehaviour
{
    public TileGame tileGameScript;
    public NavMeshAgent enemyAgent;
    public Collider enemyAgentCollider;
    public AudioSource stoneMoveAudio;
    public int enemyIndex;
    public int enemyMoveCounter;
    public TG_Tile startTile;
    public TG_Tile enemyCurrentTile;
    public TG_Tile enemyLastTile;
    public DiamondTrollController enemyController;

    public TG_Tile targetTile;

    public GameObject hitPlayerEffect;

    public delegate void TileGameEnemyEvent(int passedMoveCount, int thisIndex); //Sends to other current TileGameEnemy Scripts
    public static event TileGameEnemyEvent OnNextEnemyMove;

    public delegate void BossGameEnemyEvent(); //Sends To BossGame Script
    public static event BossGameEnemyEvent OnEnemyHitPlayer;

    private void Start()
    {
        enemyAgent.Warp(transform.position);
        //StartCoroutine(CountDownToDestroy());
    }

    private void OnEnable()
    {
        TileGame.OnMoveCountChange += CheckMoveTurn;
        OnNextEnemyMove += CheckMoveTurn;
    }

    private void OnDisable()
    {
        TileGame.OnMoveCountChange -= CheckMoveTurn;
        OnNextEnemyMove -= CheckMoveTurn;

        StopAllCoroutines();
    }

    void CheckMoveTurn(int passedMoveCount, int passedIndex)
    {
        if(passedIndex == enemyIndex)
        {
            if(passedMoveCount > enemyMoveCounter)
            {
                CheckMoveConditions(passedMoveCount);
            }
        }
    }

    void CheckMoveConditions(int passedMoveCount)
    {
        //stoneMoveAudio.Play();
        targetTile = enemyCurrentTile;
        float closestDist = 100f; //Arbitary big range
        int closestIndex = 0;
        var availableTiles = new List<TG_Tile>();
            
        for (int i = 1; i < enemyCurrentTile.connectedTiles.Count; i++) // CHECK CLOSEST AVAILABLE
        {
            if (enemyCurrentTile.connectedTiles[i].gameObject.activeSelf)
            {
                if (enemyCurrentTile.connectedTiles[i].tileState != TileGame.TileStates.Locked && enemyCurrentTile.connectedTiles[i].tileType != TileGame.TileTypes.Goal && 
                    enemyCurrentTile.connectedTiles[i].tileType != TileGame.TileTypes.GoalNeighbour)
                {
                    bool sinkTileCheck = true; // CHECK SUNKEN TILES

                    for (int e = 0; e < tileGameScript.currentSinkerTiles.Count; e++)
                    {
                        if (tileGameScript.tiles.IndexOf(tileGameScript.currentSinkerTiles[e]) == tileGameScript.tiles.IndexOf(enemyCurrentTile.connectedTiles[i]))
                        {
                            if (tileGameScript.currentSinkerTiles[e])
                                sinkTileCheck = false;
                        }
                    }

                    if(sinkTileCheck)                   
                    {
                        var curDist = Vector3.Distance(enemyCurrentTile.connectedTiles[i].transform.position, tileGameScript.gameManager.player.transform.position);
                        availableTiles.Add(enemyCurrentTile.connectedTiles[i]);

                        if (curDist < closestDist)
                        {
                            closestDist = curDist;
                            closestIndex = i;
                            HelperFunctions.SetNewIndex(availableTiles, availableTiles.IndexOf(enemyCurrentTile.connectedTiles[i]), 0);
                        }
                    }
                }
            }
        }

        if (availableTiles.Count > 0) // CHECK IF OTHER ENEMY IS TARGETING SAME TILE
        {
            for (int i = 0; i < availableTiles.Count; i++) 
            {
                bool check = true;

                foreach (var enemy in tileGameScript.currentEnemies)
                {
                    if (enemy)
                    {
                        if (enemy.targetTile == availableTiles[i])
                            check = false;
                    }
                }

                if (check)
                {
                    targetTile = availableTiles[i];
                    break;
                }

                else
                {
                    targetTile = enemyLastTile;
                }
            }
        }
        else
        {
            targetTile = enemyLastTile;
        }

        if (targetTile != null)
            MoveEnemy(targetTile, passedMoveCount);
        else
            MoveEnemy(enemyCurrentTile, passedMoveCount);
    }

    public void MoveEnemy(TG_Tile passedTargetTile, int passedMoveCount)
    {
        if (passedTargetTile != enemyCurrentTile)
        {
            if (enemyController)
                enemyController.SetStepAnim();
        }

        enemyLastTile = enemyCurrentTile;
        enemyAgent.SetDestination(passedTargetTile.transform.position);
        enemyCurrentTile = passedTargetTile;
        enemyMoveCounter++;

        if (enemyCurrentTile == tileGameScript.currentTile)
        {
            StartCoroutine(HitPlayer());
        }
        else
        {
            if (passedTargetTile.tileState == TileGame.TileStates.Available && passedTargetTile.tileType == TileGame.TileTypes.None)
            {
                tileGameScript.SetUnavailable(passedTargetTile);

                if(tileGameScript.currentlyAvailable.Contains(passedTargetTile))
                {
                    tileGameScript.currentlyAvailable.Remove(passedTargetTile);
                }
            }
        }

        StartCoroutine(NextEnemy(passedMoveCount));

        //yield return null;
        //yield return new WaitForEndOfFrame();
    }

    private IEnumerator NextEnemy(int passedMoveCount)
    {
        yield return new WaitForSeconds(0.25f);

        if (OnNextEnemyMove != null)
        {
            OnNextEnemyMove(passedMoveCount, enemyIndex + 1);
        }
    }

    public void UndoMove()
    {
        //stoneMoveAudio.Play();
        if (enemyController)
            enemyController.SetStepAnim();

        enemyAgent.SetDestination(enemyCurrentTile.transform.position);
    }

    private IEnumerator CountDownToDestroy()
    {
        yield return new WaitForSeconds(10);
        Destroy(transform.gameObject);
    }

    private IEnumerator HitPlayer()
    {
        if (enemyController)
            enemyController.SetAttackAnim();

        yield return new WaitForSeconds(0.75f);
        tileGameScript.SetFailSound.Play();
        var newEffect = Instantiate(hitPlayerEffect, enemyCurrentTile.transform.position, Quaternion.identity, tileGameScript.gameTransform);
        yield return new WaitForSeconds(0.2f);
        
        if (!tileGameScript.isFinalBossGame)
        {
            tileGameScript.SetPlayerDestination(tileGameScript.playerStartPos);
            yield return new WaitForSeconds(1);
            tileGameScript.ResetCurrentGameSet();
        }
        else
        {
            TG_Tile nextTile = CheckPlayerNextAvailable();
            TG_Tile lastTile = tileGameScript.currentTile;
            TileGame.TileTypes lastTileType = lastTile.tileType;
            TileGame.TileStates lastTileState = lastTile.tileState;

            if (nextTile != null)
            {
                tileGameScript.SetPlayerDestination(nextTile.transform);
                tileGameScript.SetSelected(nextTile);

                if((lastTileType == TileGame.TileTypes.None || lastTileType == TileGame.TileTypes.Operator || lastTileType == TileGame.TileTypes.Numerical)
                    && (lastTileState == TileGame.TileStates.Available || lastTileState == TileGame.TileStates.Selected))
                {
                    tileGameScript.SetUnavailable(lastTile);
                    tileGameScript.currentlyAvailable.Remove(lastTile);
                }
                //tileGameScript.SetAvailables();

                if (OnEnemyHitPlayer != null)
                    OnEnemyHitPlayer();
            }
            else
            {
                tileGameScript.SetPlayerDestination(tileGameScript.playerStartPos);
                yield return new WaitForSeconds(1);
                tileGameScript.ResetCurrentGameSet();
            }
        }
    }

    private TG_Tile CheckPlayerNextAvailable()
    {
        List<TG_Tile> candidateTiles = new List<TG_Tile>();
        TG_Tile nextTile = null;

        if (tileGameScript.currentlyAvailable.Count > 0)
        {
            for (int i = 0; i < tileGameScript.currentlyAvailable.Count; i++)
            {
                bool enemyCheck = true;

                foreach (var enemy in tileGameScript.currentEnemies)
                {
                    if (enemy.enemyCurrentTile == tileGameScript.currentlyAvailable[i])
                        enemyCheck = false;
                }

                if(enemyCheck)
                {
                    if(tileGameScript.currentlyAvailable[i].gameObject.activeSelf)
                        candidateTiles.Add(tileGameScript.currentlyAvailable[i]);
                }
            }
        }

        if(candidateTiles.Count > 0)
        {
            List<Transform> transforms = new List<Transform>();

            for (int i = 0; i < candidateTiles.Count; i++)
            {
                transforms.Add(candidateTiles[i].transform);
            }

            Transform pos = HelperFunctions.FindClosestItem(transforms, tileGameScript.gameManager.player.transform);
            int index = transforms.IndexOf(pos);
            nextTile = candidateTiles[index];
        }

        return nextTile;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

}
