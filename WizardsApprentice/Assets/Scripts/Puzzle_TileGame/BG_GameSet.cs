﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_GameSet : MonoBehaviour
{
    public List<BG_Task> easyTasks = new List<BG_Task>();
    public List<BG_Task> normalTasks = new List<BG_Task>();
    public List<BG_Task> hardTasks = new List<BG_Task>();
}
