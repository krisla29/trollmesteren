﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TG_GoalButton : MonoBehaviour
{
    public delegate void TG_GoalButtonEvent(); // Sends to TileGame script
    public static event TG_GoalButtonEvent OnGoalButtonClick;

    public void GoalButtonClick()
    {
        if (OnGoalButtonClick != null)
            OnGoalButtonClick();
    }
}
