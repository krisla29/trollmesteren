﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_BattleCollider : MonoBehaviour
{
    public BG_BattleManager battleManagerScript;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            battleManagerScript.EnterBattle(transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            battleManagerScript.ExitBattle();
        }
    }
}
