﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TG_Tile: MonoBehaviour
{
    public TileGame tileGameScript; //Static
    public bool startTile;
    public bool goalFinished;
    public List<TG_Tile> connectedTiles = new List<TG_Tile>(); //Static
    public TileGame.TileTypes tileType = TileGame.TileTypes.None;
    public TileGame.TileStates tileState = TileGame.TileStates.Unavailable;
    public TileGame.OperatorTypes tileOperatorType = TileGame.OperatorTypes.None;
    public int value;
    public TextMeshPro textMesh;
    public Collider tileCollider;
    public MeshRenderer meshRenderer;
    public SpriteRenderer sprite;
    public TG_Key key;
    //public SpriteRenderer key; //TODO: Make key spriteRenderer for Easy Color Change !!

    public delegate void TG_TileEvent(TG_Tile thisTG_Tile); // Sends to TileGame
    public static event TG_TileEvent OnTileClick;

    private void OnMouseDown()
    {
        if (tileCollider.enabled)
        {
            if (OnTileClick != null)
            {
                OnTileClick(this);
            }
        }
    }
}
