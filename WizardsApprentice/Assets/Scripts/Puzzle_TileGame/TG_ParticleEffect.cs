﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TG_ParticleEffect : MonoBehaviour
{
    public ParticleSystem pSystem;
    private ParticleSystem.ColorOverLifetimeModule pSystemColor;
    private ParticleSystemRenderer pSystemRenderer;
    public SpriteRenderer sprite;
    public Color effectColor;
    private Gradient gradient;
    private GradientColorKey[] colorKey;
    private GradientAlphaKey[] alphaKey;

    private void Start()
    {
        pSystemColor = pSystem.colorOverLifetime;

        gradient = new Gradient();

        // Populate the color keys at the relative time 0 and 1 (0 and 100%)
        colorKey = new GradientColorKey[2];
        colorKey[0].color = effectColor;
        colorKey[0].time = 0.0f;
        colorKey[1].color = effectColor;
        colorKey[1].time = 1.0f;

        // Populate the alpha  keys at relative time 0 and 1  (0 and 100%)
        alphaKey = new GradientAlphaKey[4];
        alphaKey[0].alpha = 0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1f;
        alphaKey[1].time = 0.3f;
        alphaKey[2].alpha = 1f;
        alphaKey[2].time = 0.7f;
        alphaKey[3].alpha = 0f;
        alphaKey[3].time = 1f;

        gradient.SetKeys(colorKey, alphaKey);
        pSystemColor.color = gradient;
        // What's the color at the relative time 0.25 (25 %) ?
        //Debug.Log(gradient.Evaluate(0.25f));
        sprite.color = new Vector4(effectColor.a, effectColor.g, effectColor.b, sprite.color.a);
    }

    private void Update()
    {
        colorKey[0].color = effectColor;
        colorKey[0].time = 0.0f;
        colorKey[1].color = effectColor;
        colorKey[1].time = 1.0f;

        alphaKey[0].alpha = 0f;
        alphaKey[0].time = 0.0f;
        alphaKey[1].alpha = 1f;
        alphaKey[1].time = 0.3f;
        alphaKey[2].alpha = 1f;
        alphaKey[2].time = 0.7f;
        alphaKey[3].alpha = 0f;
        alphaKey[3].time = 1f;

        pSystemColor.color = gradient;

        sprite.color = new Vector4(effectColor.a, effectColor.g, effectColor.b, sprite.color.a);
    }
}
