﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BG_Battle : MonoBehaviour
{
    public GameManager gameManager;
    public BG_BattleManager battleManagerScript;
    public BossGame bossGameScript;
    public List<Transform> tileGameGraphicsToRemove = new List<Transform>();
    public LineRenderer lineRenderer1;
    public LineRenderer lineRenderer2;
    public Camera cam;
    public Transform gameTransform;
    public Transform sceneTransform;
    public BossController enemy;
    public GameObject enemyCloudEffect;
    public LookAtObject2 enemyLookScript;
    private Vector3 enemyStartPos;
    public RectTransform enemyHealthBar;
    private Vector3 enemyHealthBarStartPos;
    public SpriteRenderer enemyTarget;
    public GameObject enemyHitEffect;
    public GameObject enemyHitEffectBig;
    public GameObject spellThrow;
    public Transform spellBuildupEffect;
    public SpriteRenderer wandHitEffect;
    public float maxDistanceEnemyTarget = 3f;
    public Image enemyHealthBarHealth;
    public float enemyStartHealth = 200;
    public float enemyCurrentHealth;

    public CanvasGroup playerCanvasHitEffect;
    public Image symbolOrder;

    public float MaxDamageBattle = 200;
    public float currentDamageBattle;
    public float maxAttackDamage = 50;
    public float currentAttackDamage;
    public float firePower = 7.5f;

    public GameObject[] spellEffects = new GameObject[4];
    public AudioSource[] spellSoundEffects = new AudioSource[4];
    public Transform effectsContainer;

    public GameObject attackerPrefab;
    private List<Transform> attackers = new List<Transform>();
    public Transform attackersContainer;
    public Transform attackersSpawnArea;
    public Vector2Int attackersRange = new Vector2Int(10, 20);
    public float minDistAttackers = 0.25f;
    public float attackTime = 10f;
    public GameObject aimEffect;

    public BG_Task currentTask;
    public Transform currentTaskBG;
    public Transform bgSky;
    public int taskCounter;
    private Vector3 bgStartPos;
    private Vector3 bgStartScale;
    private Vector3 bgStartRot;
    public List<BG_TaskObject> currentActiveObjects = new List<BG_TaskObject>();
    public float currentWaveTime = 40f;
    public float currentWaitTime = 10f;
    public float damageTime = 10f;
    private float damageCountdown;
    Vector3[] pointPositions1;
    Vector3[] pointPositions2;
    Ray ray;
    RaycastHit hit;
    public float rayDistance = 10f;
    public Vector3 comparePoint;
    public float compareRadius = 1f;

    private Vector3 startPos;

    private bool correct;
    private bool playerDamage;
    private bool attack;
    private bool hasClicked;

    private bool wait;

    public float minDistToPlayer = 0.5f;
    public float distToPlayer;

    //private bool waveReady;
    private Coroutine currentWave = null;
    private Coroutine attackPhase = null;
    private Coroutine finishOff = null;
    private Coroutine damageCountDown = null;
    private Coroutine spellBuildUp = null;
    private Coroutine hitPlayerCanvas = null;

    public AudioSource soundRay;
    public AudioSource soundHitObject;
    public AudioSource soundReflectObject;
    public AudioSource soundTaskComplete;
    public AudioSource soundTaskFail;
    public AudioSource soundPlayerHit;
    public AudioSource soundGhostHappy;
    public AudioSource soundGhostSad;
    public AudioSource soundGhostReady;
    public AudioSource soundGhostOps;
    public AudioSource soundWinBattle;
    public AudioSource soundHitEnemy;
    public AudioSource soundLevitate;
    public AudioSource soundGlassBreak;
    public AudioSource soundWhip;
    public AudioSource soundThrowDiamonds;

    private void Awake()
    {
        startPos = transform.localPosition;
        enemyCurrentHealth = enemyStartHealth;
        enemyStartPos = enemy.transform.localPosition;
        enemyHealthBarStartPos = enemyHealthBar.transform.position;

        bgStartPos = currentTaskBG.localPosition;
        bgStartScale = currentTaskBG.localScale;
        bgStartRot = currentTaskBG.localEulerAngles;
    }

    private void Update()
    {
        CheckWaitStatus();
    }

    void CheckWaitStatus()
    {
        if (!wait)
        {
            if (!battleManagerScript.wandUI.wandActive.gameObject.activeSelf)
            {
                battleManagerScript.wandUI.wandActive.gameObject.SetActive(true);
                battleManagerScript.wandUI.wandDeActive.gameObject.SetActive(false);
            }
        }
        else
        {
            if (battleManagerScript.wandUI.wandActive.gameObject.activeSelf)
            {
                battleManagerScript.wandUI.wandActive.gameObject.SetActive(false);
                battleManagerScript.wandUI.wandDeActive.gameObject.SetActive(true);
            }
        }
    }

    private void OnEnable()
    {
        InitializeBattle();        
        StartNewWave();
        BossGame.OnBossGameComplete += ResetTasks;
    }

    private void OnDisable()
    {       
        CancelBattle();
        
        StopAllCoroutines();
        NullifyAllCoroutines();

        BossGame.OnBossGameComplete -= ResetTasks;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        NullifyAllCoroutines();
    }

    void ResetTasks()
    {
        currentTask = null;
        taskCounter = 0;
    }

    private void NullifyAllCoroutines()
    {
        currentWave = null;
        attackPhase = null;
        finishOff = null;
        damageCountDown = null;
        spellBuildUp = null;
        hitPlayerCanvas = null;
    }

    public void CancelBattle()
    {
        if (soundRay)
        {
            if (soundRay.isPlaying)
                soundRay.Stop();
        }

        gameManager.player.toggleAimThirdPerson = false;
        gameManager.player.wandTarget = Vector3.zero;

        if (enemy.anim.GetBool("IsDefeated"))
            enemy.anim.SetBool("IsDefeated", false);

        enemy.SetFlying(false);

        ResetBools();

        if(currentTask)
        {
            Destroy(currentTask.gameObject);
        }
        currentTask = null;

        taskCounter = 0;

        foreach (Transform effect in effectsContainer)
        {
            Destroy(effect.gameObject);
        }

        foreach (var item in battleManagerScript.tileGameScript.currentEnemies)
        {
            item.gameObject.SetActive(true);
        }

        foreach (Transform child in bossGameScript.itemContainer)
        {
            child.gameObject.SetActive(true);
        }

        KillAttackers();

        enemyHealthBar.gameObject.SetActive(false);
        enemyTarget.gameObject.SetActive(false);
        enemyCloudEffect.SetActive(false);
        wandHitEffect.gameObject.SetActive(false);
        spellBuildupEffect.localScale = Vector3.zero;
        spellBuildupEffect.gameObject.SetActive(false);

        foreach (var item in tileGameGraphicsToRemove)
        {
            item.gameObject.SetActive(true);
        }

        if (symbolOrder.gameObject.activeSelf)
            symbolOrder.gameObject.SetActive(false);

        currentAttackDamage = 0;
        currentDamageBattle = 0;
        ResetLineRenderer2();
        ResetLineRenderer1();
    }

    void FinishBattle()
    {
        battleManagerScript.completedBattles++;
        KillAttackers();
        ResetBools();
        ResetCurrentTask();
        taskCounter = 0;
        currentAttackDamage = 0;
        currentDamageBattle = 0;
        battleManagerScript.ExitBattle();
        transform.gameObject.SetActive(false);
    }

    void CheckDistanceToPlayer()
    {
        distToPlayer = (transform.position - gameManager.player.transform.position).sqrMagnitude;

        if(distToPlayer < (minDistToPlayer * minDistToPlayer))
        {            
            playerDamage = true;
            StartCoroutine(PlayerDamage());           
            //waveReady = false;
            StopCoroutine(currentWave);
            MouseUp();
        }
    }

    void CheckRayTask()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 20f))
        {
            if (hit.transform == transform)
            {
                comparePoint = hit.point;
                Debug.DrawLine(ray.origin, ray.origin + (ray.direction * rayDistance), Color.red);

                for (int i = 0; i < currentTask.taskObjects.Count; i++)
                {
                    if ((currentTask.taskObjects[i].transform.position - comparePoint).sqrMagnitude < compareRadius * compareRadius)
                    {
                        if (!currentTask.taskObjects[i].isActive)
                        {
                            currentActiveObjects.Add(currentTask.taskObjects[i]);
                            currentTask.taskObjects[i].isActive = true;
                            StartCoroutine(PinGhost(i));                           

                            if (soundHitObject)
                                soundHitObject.Play();
                        }
                    }
                }
            }
        }

        if(currentActiveObjects.Count >= currentTask.taskObjects.Count)
        {
            MouseUp();
        }
    }

    void CheckRayAttack()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 20f) && finishOff == null)
        {
            if (hit.transform == transform)
            {
                comparePoint = hit.point;
                Debug.DrawLine(ray.origin, ray.origin + (ray.direction * rayDistance), Color.red);

                if (Vector3.Distance(hit.point, enemyTarget.transform.position) < maxDistanceEnemyTarget &&
                    Mathf.Abs(hit.point.y - enemyTarget.transform.position.y) < (maxDistanceEnemyTarget / 2))
                {
                    //wandHitEffect.gameObject.SetActive(true);
                    //wandHitEffect.transform.position = comparePoint;

                    if (currentAttackDamage < maxAttackDamage)
                    {
                        if (currentDamageBattle < MaxDamageBattle)
                        {
                            if (!enemy.anim.GetBool("IsDefeated"))
                                enemy.anim.SetBool("IsDefeated", true);

                            var value = Time.deltaTime * firePower;

                            enemyCurrentHealth -= value;
                            currentAttackDamage += value;
                            currentDamageBattle += value;

                            enemyHealthBarHealth.fillAmount = enemyCurrentHealth / enemyStartHealth;
                        }
                        else
                        {
                            finishOff = StartCoroutine(FinishOffEnemy(true));
                        }
                    }
                    else
                    {
                        if (currentDamageBattle < MaxDamageBattle)
                            finishOff = StartCoroutine(FinishOffEnemy(true));
                        else
                            finishOff = StartCoroutine(FinishOffEnemy(true));
                    }
                }
                else
                {
                    //wandHitEffect.gameObject.SetActive(false);

                    if (enemy.anim.GetBool("IsDefeated"))
                        enemy.anim.SetBool("IsDefeated", false);
                }
            }
        }        
    }

    void CheckRayDefend()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 20f))
        {            
            if (hit.transform == transform)
            {
                comparePoint = hit.point;

                for (int i = attackers.Count - 1; i > -1; i--)
                {
                    if ((attackers[i].transform.position - comparePoint).sqrMagnitude < minDistAttackers * minDistAttackers && attackPhase != null)
                    {
                        var attacker = attackers[i];

                        print(attacker);

                        Debug.DrawLine(ray.origin, hit.point, Color.green);

                        if (soundReflectObject)
                            soundReflectObject.Play();

                        StartCoroutine(RicochetAttacker(attacker));
                        attackers.Remove(attacker);
                    }
                    else
                    {
                        Debug.DrawLine(ray.origin, hit.point, Color.red);
                    }
                }
            }
        }
        else
        {
            comparePoint = ray.origin + (ray.direction * 20f);
            Debug.DrawLine(ray.origin, ray.origin + (ray.direction * rayDistance), Color.grey);
        }
    }

    void CheckDistanceDefend()
    {
        for (int i = 0; i < attackersContainer.childCount; i++)
        {
            if((attackersContainer.GetChild(i).transform.position - (gameManager.player.transform.position + Vector3.up)).sqrMagnitude < minDistAttackers * minDistAttackers)
            {
                StartCoroutine(PlayerDamage2());
            }
        }
    }

    void SpawnAttackers()
    {
        int randomCount = Random.Range(attackersRange.x, attackersRange.y + 1);

        for (int i = 0; i < randomCount; i++)
        {
            var spawnPoint = HelperFunctions.RandomPointInBox(attackersSpawnArea);
            var attacker = Instantiate(attackerPrefab, spawnPoint, Quaternion.identity, attackersContainer);
            attacker.transform.localRotation = Quaternion.AngleAxis(180, Vector3.up);
            attacker.name = "Attacker" + i;
            attackers.Add(attacker.transform);
            StartCoroutine(MoveAttackerToStart(spawnPoint, attacker.transform));
        }
    }

    private IEnumerator MoveAttackerToStart(Vector3 pos, Transform attacker)
    {
        float t = 0;
        float animTime = 0.5f;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            attacker.position = Vector3.Lerp(attackersSpawnArea.position, pos, h);
            attacker.GetChild(0).GetChild(0).localScale = Vector3.Lerp(Vector3.forward, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }

    void KillAttackers()
    {
        attackers.Clear();

        foreach (Transform attacker in attackersContainer)
        {
            Destroy(attacker.gameObject);
        }

    }

    private void OnMouseDown()
    {
        if (!wait)
            MouseDown();
    }

    void MouseDown()
    {
        print("PressedBattleArea!");
        gameManager.player.toggleAimThirdPerson = true;
        gameManager.player.wandTarget = hit.point;
        //gameManager.player.anim.SetBool("Aim", true);
        if (soundRay)
            soundRay.Play();

        StartLineRenderer2();
        hasClicked = true;
    }

    private void OnMouseDrag()
    {
        if (hasClicked && !wait)
        {
            if (!correct)
            {
                CheckRayTask();
                UpdateLineRenderer1();
            }
            else
            {   
                if(!attack)
                    CheckRayAttack();
                else
                {
                    CheckRayDefend();
                }
            }

            UpdateLineRenderer2();
            RotatePlayerWand();            
        }

        if (wait)
            ResetLineRenderer2();
    }

    private void OnMouseUp()
    {
        ResetLineRenderer2();

        if (!wait)
            MouseUp();       
    }

    private void MouseUp()
    {
        //print("ReleasedBattleArea!");        
        gameManager.player.toggleAimThirdPerson = false;
        gameManager.player.wandTarget = Vector3.zero;
        StartCoroutine(RotatePlayer());
        ResetLineRenderer2();

        if (enemy.anim.GetBool("IsDefeated"))
            enemy.anim.SetBool("IsDefeated", false);

        if (soundRay)
            soundRay.Stop();

        if (!correct && !playerDamage && !attack)
        {
            print("Hello");
            correct = CheckAnswer();
            
            if (correct)
            {
                if (soundTaskComplete)
                    soundTaskComplete.Play();

                //waveReady = false;
                StopCoroutine(currentWave);
                StartCoroutine(SpellSuccessful());
            }
            else
            {
                if (soundTaskFail)
                    soundTaskFail.Play();

                ResetCurrentTask();
            }
        }

        hasClicked = false;
    }

    bool CheckAnswer()
    {
        bool check = true;

        if (currentTask.taskObjects.Count == currentActiveObjects.Count)
        {
            for (int i = currentActiveObjects.Count - 1; i > -1; i--)
            {
                if (i > 0)
                {
                    if (currentActiveObjects[i].value <= currentActiveObjects[i - 1].value)
                    {
                        check = false;
                    }
                }
            }
        }
        else
        {
            check = false;
        }

        if(check)
        {
            print("Success!!");
            return true;
        }
        else
        {
            print("Fail!!");
            return false;
        }        
    }

    void ResetCurrentTask()
    {
        if (currentTask)
        {
            foreach (var taskObject in currentTask.taskObjects)
            {
                taskObject.isActive = false;
            }

            currentActiveObjects.Clear();

            StartLineRenderer1();
        }
    }

    void UpdateLineRenderer1()
    {
        lineRenderer1.positionCount = currentActiveObjects.Count + 1;
        pointPositions1 = new Vector3[currentActiveObjects.Count + 1];

        for (int i = 0; i < currentActiveObjects.Count; i++)
        {
            if (i < currentActiveObjects.Count)
            {
                if (currentActiveObjects[i])
                    pointPositions1[i] = currentActiveObjects[i].transform.position;                    
            }
        }        
        pointPositions1[pointPositions1.Length - 1] = comparePoint;
        lineRenderer1.SetPositions(pointPositions1);

        if (currentTask.lineIsClosed)
            lineRenderer1.loop = true;
        else
            lineRenderer1.loop = false;
    }

    void UpdateLineRenderer1End()
    {
        lineRenderer1.positionCount = currentActiveObjects.Count;
        pointPositions1 = new Vector3[currentActiveObjects.Count];

        for (int i = 0; i < currentActiveObjects.Count; i++)
        {
            if (i < currentActiveObjects.Count)
            {
                if (currentActiveObjects[i])
                    pointPositions1[i] = currentActiveObjects[i].transform.position;
            }
        }

        lineRenderer1.SetPositions(pointPositions1);

        if (currentTask.lineIsClosed)
        {
            if (pointPositions1.Length > 2)
                lineRenderer1.loop = true;
            else
                lineRenderer1.loop = false;
        }
        else
            lineRenderer1.loop = false;
    }

    void UpdateLineRenderer2()
    {
        lineRenderer2.positionCount = 3;
        pointPositions2 = new Vector3[3];

        pointPositions2[0] = gameManager.player.wandObject.star.position;
        pointPositions2[1] = gameManager.player.wandObject.star.position;
        pointPositions2[2] = comparePoint;

        lineRenderer2.SetPositions(pointPositions2);

        if (currentTask.lineIsClosed)
        {
            if (pointPositions1.Length > 2)
                lineRenderer1.loop = true;
            else
                lineRenderer1.loop = false;
        }
        else
            lineRenderer1.loop = false;

        if (!wandHitEffect.gameObject.activeSelf)
            wandHitEffect.gameObject.SetActive(true);

        wandHitEffect.transform.position = comparePoint;
    }

    void ResetLineRenderer1()
    {
        lineRenderer1.positionCount = 0;
        pointPositions1 = new Vector3[0];
        lineRenderer1.SetPositions(pointPositions1);
        lineRenderer1.enabled = false;
    }

    void ResetLineRenderer2()
    {
        lineRenderer2.positionCount = 0;
        pointPositions2 = new Vector3[0];
        lineRenderer2.SetPositions(pointPositions2);
        lineRenderer2.enabled = false;
        wandHitEffect.gameObject.SetActive(false);
    }

    void StartLineRenderer1()
    {
        lineRenderer1.positionCount = 0;
        pointPositions1 = new Vector3[0];
        lineRenderer1.SetPositions(pointPositions1);
        lineRenderer1.enabled = true;

        if (currentTask)
        {
            if (!currentTask.lineIsClosed)
                lineRenderer1.loop = false;
            else
                lineRenderer1.loop = true;
        }
    }

    void StartLineRenderer2()
    {
        lineRenderer2.positionCount = 0;
        pointPositions2 = new Vector3[0];
        lineRenderer2.SetPositions(pointPositions2);
        lineRenderer2.enabled = true;
    }

    void RotatePlayerWand()
    {
        Vector3 lookVector = new Vector3(ray.direction.x, 0, ray.direction.z);
        //lookVector = new Vector3(lookVector.x, 0, lookVector.y);
        Quaternion look = Quaternion.LookRotation(lookVector);
        gameManager.player.transform.localRotation = look;
    }

    private IEnumerator RotatePlayer()
    {
        float t = 0;
        Vector3 dir = new Vector3(enemy.transform.position.x, 0, enemy.transform.position.z) - new Vector3(gameManager.player.transform.position.x, 0, gameManager.player.transform.position.z);
        Quaternion endRot = Quaternion.LookRotation(dir, gameManager.player.transform.up);
        Quaternion startRot = gameManager.player.transform.rotation;

        while (t < 1)
        {
            t += Time.deltaTime / 0.5f;
            gameManager.player.transform.rotation = Quaternion.Slerp(startRot, endRot, t);
            yield return new WaitForEndOfFrame();
        }
    }

    void ResetBools()
    {
        correct = false;
        playerDamage = false;
        hasClicked = false;
        attack = false;
    }

    void StartNewWave()
    {
        wait = true;
        ResetBools();
        print("Started new set!");
        StopAllCoroutines();
        NullifyAllCoroutines();
        ResetTransforms();
        ResetCurrentTask();
        ResetLineRenderer1();
        StartLineRenderer1();
        ResetLineRenderer2();
        StartCoroutine(RotatePlayer());
        SpawnNewTask();
        currentWave = StartCoroutine(Wave());
    }

    private IEnumerator Wave()
    {        
        yield return new WaitForSeconds(2);

        int attackIndex = 0;

        attackIndex = enemy.SetAttackAnim();
        
        yield return new WaitForSeconds(currentWaitTime / 6);

        if(spellSoundEffects.Length == 4)
        {
            if (spellSoundEffects[attackIndex])
                spellSoundEffects[attackIndex].Play();
        }

        for (int i = 0; i < spellEffects.Length; i++)
        {
            var effect = Instantiate(spellEffects[attackIndex], effectsContainer);
            effect.transform.position = transform.position;
        }
                             
        float y = 0;
        float waitTime = currentWaitTime / 12;
        float waitTime2 = currentWaitTime / 8;

        for (int i = 0; i < currentTask.taskObjects.Count; i++)
        {
            StartCoroutine(GrowGhost(i, (waitTime + Random.Range(0.2f, 2f))));
        }

        currentTaskBG.gameObject.SetActive(true);        

        while (y < 1)
        {
            y += Time.deltaTime / waitTime2;
            float h = y;
            h = y * y * y * (y * (6f * y - 15f) + 10f);
            currentTaskBG.localScale = Vector3.Lerp(Vector3.zero, bgStartScale, h);
            currentTask.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(currentWaitTime / 4);

        float x = 0;
        float animTime3 = 2;
                  
        while (x < 1)
        {
            x += Time.deltaTime / animTime3;
            var scale = Mathf.Sin(x * (Mathf.PI * 1));

            for (int i = 0; i < currentTask.taskObjects.Count; i++)
            {
                currentTask.taskObjects[i].aim.transform.localScale = Vector3.one * scale;
            }

            yield return new WaitForEndOfFrame();                       
        }

        foreach (var item in currentTask.taskObjects)
        {
            item.aim.gameObject.SetActive(false);
        }

        symbolOrder.gameObject.SetActive(true);
        wait = false;

        yield return new WaitForSeconds(1);

        float t = 0;
        Vector3 endPos = new Vector3(0, 0, 5f);

        while (t < 1)
        {
            t += Time.deltaTime / currentWaveTime;
            transform.localPosition = Vector3.Lerp(startPos, endPos, t);
            CheckDistanceToPlayer();
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(currentWaitTime / 4);

        float ti = 0;
        Vector3 endPos2 = new Vector3(0, 0, 9f);

        while (ti < 1)
        {
            ti += Time.deltaTime / (currentWaveTime / 2);
            transform.localPosition = Vector3.Lerp(endPos, endPos2, ti);
            CheckDistanceToPlayer();
            yield return new WaitForEndOfFrame();
        }

        symbolOrder.gameObject.SetActive(false);
    }

    private IEnumerator SpellSuccessful()
    {
        wait = true;
        symbolOrder.gameObject.SetActive(false);
        Vector3 endPos = transform.localPosition;              
        transform.localPosition = endPos;

        float t = 0;
        float waitTime = 2;

        while(t < 1)
        {
            t += Time.deltaTime / waitTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            UpdateLineRenderer1End();
            yield return new WaitForEndOfFrame();
        }
               
        for (int i = 0; i < currentTask.taskObjects.Count; i++)
        {
            waitTime = Random.Range(0.2f, 1f);
            StartCoroutine(ShrinkGhost(i, waitTime));
        }

        float t2 = 0;
        float animTime = endPos.z / 3f;
        Vector3[] startPositions = new Vector3[currentTask.taskObjects.Count];

        for (int i = 0; i < currentTask.taskObjects.Count; i++)
        {
            startPositions[i] = currentTask.taskObjects[i].transform.position;
        }

        while (t2 < 1)
        {
            t2 += Time.deltaTime / animTime;
            float h = t2;
            h = t2 * t2 * t2 * (t2 * (6f * t2 - 15f) + 10f);
            
            for (int i = 0; i < currentTask.taskObjects.Count; i++)
            {
                currentTask.taskObjects[i].transform.position = Vector3.Lerp(startPositions[i], enemyTarget.transform.position + (enemy.transform.forward * 2), h);
            }

            UpdateLineRenderer1End();
            transform.localPosition = Vector3.Lerp(endPos, startPos, h);
            currentTaskBG.transform.localScale = Vector3.Lerp(bgStartScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        enemy.SetHitAnim();
        var effect = Instantiate(enemyHitEffectBig, effectsContainer);
        effect.transform.localPosition = Vector2.up * 2.5f;

        if (soundWhip)
            soundWhip.Play();
        
        yield return new WaitForSeconds(2);

        ResetCurrentTask();
        ResetLineRenderer1();
        attackPhase = StartCoroutine(AttackersPhase());
    }

    private IEnumerator AttackersPhase()
    {
        attack = true;

        enemy.SetCastSecondWaveAnim();
        spellBuildUp = StartCoroutine(BuildUpSpell());

        yield return new WaitForSeconds(6.5f);
        
        var spellGraphic = Instantiate(spellThrow, effectsContainer);
        spellGraphic.transform.localPosition = Vector3.up * 2;

        //currentTaskBG.transform.localPosition += new Vector3(0, 0, -0.5f);
        //StartCoroutine(ScaleBg(true));

        yield return new WaitForSeconds(1f);
        
        SpawnAttackers();

        yield return new WaitForSeconds(1f);

        float t = 0;
        Vector3 startPos = transform.localPosition;
        Vector3 endPos = new Vector3(0,0,8f);

        Vector3[] startPositions = new Vector3[attackersContainer.childCount];
        List<Transform> tmpAttackers = new List<Transform>();

        for (int i = 0; i < attackersContainer.childCount; i++)
        {
            startPositions[i] = attackersContainer.GetChild(i).transform.position;
            tmpAttackers.Add(attackersContainer.GetChild(i).transform);
            //var aim = Instantiate(aimEffect, effectsContainer);
            //aim.transform.parent = attackersContainer.GetChild(i);
            //aim.transform.localPosition = Vector3.zero;
        }

        Vector3 endPosition = gameManager.player.transform.position + Vector3.up;

        wait = false;

        if (attackersContainer.childCount > 0 && !playerDamage)
        {
            while (t < 1)
            {
                t += Time.deltaTime / attackTime;
                float h = t;
                h = t * t * t * (t * (6f * t - 15f) + 10f);

                CheckDistanceDefend();

                for (int i = 0; i < tmpAttackers.Count; i++)
                {
                    if (tmpAttackers[i] != null)
                    {
                        if (attackers.Contains(tmpAttackers[i]))
                        {
                            tmpAttackers[i].position = Vector3.Lerp(startPositions[i], endPosition, h);
                            //tmpAttackers[i].GetChild(0).transform.LookAt(gameManager.player.transform.position + Vector3.up);
                            var dir = tmpAttackers[i].position - gameManager.player.transform.position;
                            Quaternion look = Quaternion.LookRotation(dir);
                            var rot = tmpAttackers[i].GetChild(0).transform.rotation;
                            tmpAttackers[i].GetChild(0).transform.rotation = Quaternion.Slerp(rot, look, h);
                        }

                    }
                }

                transform.localPosition = Vector3.Lerp(startPos, endPos, h);

                if (attackersContainer.childCount == 0)
                    t = 1;

                yield return new WaitForEndOfFrame();
            }
        }

        tmpAttackers.Clear();

        if (attackersContainer.childCount <= 0)
        {
            enemy.SetFlying(true);
            yield return new WaitForSeconds(3);
            //StartCoroutine(ScaleBg(false));

            //yield return new WaitForSeconds(3f);            

            //currentTaskBG.gameObject.SetActive(false);

            damageCountDown = StartCoroutine(DamageCountDown());
        }

        attack = false;
    }

    private IEnumerator ScaleBg(bool up)
    {
        float t = 0;

        if(up)
            currentTaskBG.gameObject.SetActive(true);
      
        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);

            if(up)
                currentTaskBG.localScale = Vector3.Lerp(Vector3.zero, bgStartScale, h);
            else
                currentTaskBG.localScale = Vector3.Lerp(bgStartScale, Vector3.zero, h);

            yield return new WaitForEndOfFrame();
        }

        if(!up)
            currentTaskBG.gameObject.SetActive(false);
    }

    private IEnumerator RicochetAttacker(Transform passedAttacker)
    {
        float t = 0;
        float animTime = 0.25f;

        if (passedAttacker)
        {
            var startPos = passedAttacker.transform.position;

            Vector2 offset = new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f));

            var endPos = enemyTarget.transform.position + new Vector3(offset.x, offset.y, 0);

            while (t < 1)
            {
                t += Time.deltaTime / animTime;
                passedAttacker.position = Vector3.Lerp(startPos, endPos, t);
                yield return new WaitForEndOfFrame();
            }

            enemy.SetHitAnim();
            var hitEffect = Instantiate(enemyHitEffect, effectsContainer);
            hitEffect.transform.position = passedAttacker.position + enemy.transform.forward;

            if (soundHitEnemy)
                soundHitEnemy.Play();

            Destroy(passedAttacker.gameObject);
        }
    }

    private IEnumerator PlayerDamage()
    {
        wait = true;
        symbolOrder.gameObject.SetActive(false);
        Vector3 endPos = transform.localPosition;        
        transform.localPosition = endPos;
        ResetLineRenderer1();
        ResetLineRenderer2();

        if (soundTaskFail)
            soundTaskFail.Play();

        float t = 0;
        float waitTime = 1.5f;

        if (soundGhostHappy)
            soundGhostHappy.Play();

        while (t < 1)
        {
            t += Time.deltaTime / waitTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            currentTaskBG.transform.localScale = Vector3.Lerp(bgStartScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        gameManager.player.anim.SetTrigger("Defeat");

        if (soundPlayerHit)
            soundPlayerHit.Play();

        if(hitPlayerCanvas != null)
            StopCoroutine(hitPlayerCanvas);

        hitPlayerCanvas = null;
        hitPlayerCanvas = StartCoroutine(HitPlayerCanvasEffect());

        float t2 = 0;
        float animTime = 3;
        Vector3 startPos = currentTask.transform.localPosition;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / animTime;
            float h = t2;
            h = t2 * t2 * t2 * (t2 * (6f * t2 - 15f) + 10f);
            currentTask.transform.localPosition = Vector3.Lerp(startPos, startPos + (Vector3.up * 5f), h);
            yield return new WaitForEndOfFrame();
        }

        bossGameScript.bgPlayerHealth.RemoveHeart();

        yield return new WaitForSeconds(2);

        currentActiveObjects.Clear();
        StartNewWave();
    }

    private IEnumerator PlayerDamage2()
    {
        wait = true;
        transform.localPosition = startPos;
        StopCoroutine(attackPhase);
        attackPhase = null;
        gameManager.player.anim.SetTrigger("Defeat");
        
        if (soundTaskFail)
            soundTaskFail.Play();

        yield return new WaitForSeconds(1);

        if (soundGhostHappy)
            soundGhostHappy.Play();

        float t = 0;
        float animTime = 3f;

        Vector3[] startPoses = new Vector3[attackersContainer.childCount];

        if (hitPlayerCanvas != null)
            StopCoroutine(hitPlayerCanvas);

        hitPlayerCanvas = null;
        hitPlayerCanvas = StartCoroutine(HitPlayerCanvasEffect());

        for (int i = 0; i < attackersContainer.childCount; i++)
        {
            startPoses[i] = attackersContainer.GetChild(i).transform.position;
        }

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);

            for (int i = 0; i < attackersContainer.childCount; i++)
            {
                attackersContainer.GetChild(i).transform.position = Vector3.Lerp(startPoses[i], startPoses[i] + (Vector3.up * 5), t);
            }

            yield return new WaitForEndOfFrame();
        }

        bossGameScript.bgPlayerHealth.RemoveHeart();

        yield return new WaitForSeconds(2);

        KillAttackers();
        currentActiveObjects.Clear();
        StartNewWave();
    }

    private IEnumerator ShrinkGhost(int index, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        float t = 0;
        float shrinkTime = 0.5f;
        Vector3 startScale = currentTask.taskObjects[index].transform.localScale;

        if (soundGhostSad)
            soundGhostSad.Play();

        while (t < 1)
        {
            t += Time.deltaTime / shrinkTime;
            float h = t;            
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            currentTask.taskObjects[index].transform.localScale = Vector3.Lerp(startScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator GrowGhost(int index, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        float t = 0;
        float growthTime = 1;

        if (soundGhostReady)
            soundGhostReady.Play();

        while (t < 1)
        {
            t += Time.deltaTime / growthTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            currentTask.taskObjects[index].transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }        
    }

    private IEnumerator PinGhost(int index)
    {
        float t = 0;
        float animTime = 0.25f;
        Vector3 startScale = currentTask.taskObjects[index].transform.localScale;
        float maxAddScale = 0.3f;

        if (soundGhostOps)
            soundGhostOps.Play();

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            var scale = Mathf.Sin(h * Mathf.PI);
            scale *= maxAddScale;
            currentTask.taskObjects[index].transform.localScale = startScale + (Vector3.one * scale);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator SpellFaliure()
    {
        yield return null;
    }

    private IEnumerator DamageCountDown()
    {
        transform.localPosition = Vector3.zero;
        enemyHealthBar.gameObject.SetActive(true);
        MoveHealthBar();
        enemyTarget.gameObject.SetActive(true);
        enemyLookScript.enabled = false;
        enemyCloudEffect.SetActive(true);

        if (soundLevitate)
            soundLevitate.Play();

        float x = 0;
        Vector3 pos1 = enemy.transform.localPosition;

        while (x < 1)
        {
            x += Time.deltaTime / 0.75f;
            float h = x;
            h = x * x * x * (x * (6f * x - 15f) + 10f);
            enemy.transform.localPosition = Vector3.Lerp(pos1, pos1 + (Vector3.up * 0.5f), h);
            MoveHealthBar();
            yield return new WaitForEndOfFrame();
        }

        //yield return new WaitForSeconds(damageTime);
        float horizontalMove;
        float verticalMove;
        Vector3 pos = enemy.transform.localPosition;
        Vector3 dir = sceneTransform.InverseTransformDirection(enemy.transform.right);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / damageTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            horizontalMove = Mathf.Sin(t * (Mathf.PI * 4));
            horizontalMove *= 5f;
            verticalMove = Mathf.Sin(t * (Mathf.PI * 8));
            verticalMove *= 0.5f;
            enemy.transform.localPosition = pos + (dir * horizontalMove) + (Vector3.up * verticalMove) + (Vector3.up * 0.5f);
            MoveHealthBar();
            //enemy.transform.Translate(dir * horizontalMove);
            yield return new WaitForEndOfFrame();
        }
        
        if(finishOff == null)
            finishOff = StartCoroutine(FinishOffEnemy(false));        
    }

    void MoveHealthBar()
    {
        enemyHealthBar.transform.position = enemy.transform.position + (enemy.transform.forward * 2);
    }

    private IEnumerator FinishOffEnemy(bool dealtMaxDamage)
    {
        StopCoroutine(damageCountDown);
        damageCountDown = null;

        MouseUp();
       
        wait = true;
        attack = false;

        if (dealtMaxDamage)
            enemy.SetMajorHitAnim();

        enemy.SetFlying(false);
        enemy.SetDefeat(false);

        float x = 0;
        Vector3 pos = enemy.transform.localPosition;

        while(x < 1)
        {
            x += Time.deltaTime / 0.75f;
            float h = x;
            h = x * x * x * (x * (6f * x - 15f) + 10f);
            enemy.transform.localPosition = Vector3.Lerp(pos, enemyStartPos, h);
            MoveHealthBar();
            yield return new WaitForEndOfFrame();
        }
       
        enemy.transform.localPosition = enemyStartPos;
        enemyHealthBar.transform.position = enemyHealthBarStartPos;
        enemyLookScript.enabled = true;
        enemyHealthBar.gameObject.SetActive(false);
        enemyTarget.gameObject.SetActive(false);
        enemyCloudEffect.SetActive(false);

        if (currentDamageBattle < MaxDamageBattle)
        {
            currentActiveObjects.Clear();
            currentAttackDamage = 0;
            yield return new WaitForSeconds(3);
            StartNewWave();
        }
        else
        {
            yield return new WaitForSeconds(2);

            if (soundWinBattle)
                soundWinBattle.Play();

            yield return new WaitForSeconds(1);

            enemy.SetFailAnim();

            yield return new WaitForSeconds(2);

            gameManager.player.toggleAimThirdPerson = false;
            gameManager.player.isBattle = false;
            //yield return new WaitForSeconds(2);
            FinishBattle();
        }
    }

    private IEnumerator BuildUpSpell()
    {
        yield return new WaitForSeconds(0.5f);

        spellBuildupEffect.gameObject.SetActive(true);

        float maxSize = 1.5f;
        float x = 0;
        float buildUpTime = 5.5f;

        bgSky.gameObject.SetActive(true);

        while (x < 1)
        {
            x += Time.deltaTime / buildUpTime;
            float h = x;
            h = x * x * x * (x * (6f * x - 15f) + 10f);
            spellBuildupEffect.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * maxSize, x);

            if(x > 0.8f && soundThrowDiamonds)
            {
                if (!soundThrowDiamonds.isPlaying)
                    soundThrowDiamonds.Play();
            }

            yield return new WaitForEndOfFrame();
        }

        float t = 0;
        float disappearTime = 1f;

        while(t < 1)
        {
            t += Time.deltaTime / disappearTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            spellBuildupEffect.localScale = Vector3.Lerp(Vector3.one * maxSize, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        spellBuildupEffect.gameObject.SetActive(false);
    }

    private IEnumerator HitPlayerCanvasEffect()
    {       
        playerCanvasHitEffect.transform.localScale = Vector3.zero;
        playerCanvasHitEffect.alpha = 1;
        playerCanvasHitEffect.gameObject.SetActive(true);

        float t = 0;
        float animTime1 = 1f;

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            playerCanvasHitEffect.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            playerCanvasHitEffect.transform.localEulerAngles = Vector3.Lerp(Vector3.zero, new Vector3(0,0, 360), h);
            yield return new WaitForEndOfFrame();
        }

        playerCanvasHitEffect.transform.localEulerAngles = Vector3.zero;

        if (soundGlassBreak)
            soundGlassBreak.Play();

        float ti = 0;
        float animTime2 = 0.5f;
        float addScale = 0.3f;

        while (ti < 1)
        {
            ti += Time.deltaTime / animTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            var scale = Mathf.Sin(h * (Mathf.PI));
            scale *= addScale;
            playerCanvasHitEffect.transform.localScale = Vector3.one + (Vector3.one * scale);
            yield return new WaitForEndOfFrame();
        }

        float tm = 0;
        float animTime3 = 0.5f;

        while (tm < 1)
        {
            tm += Time.deltaTime / animTime3;
            float h = tm;
            h = tm * tm * tm * (tm * (6f * tm - 15f) + 10f);
            playerCanvasHitEffect.alpha = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }

        playerCanvasHitEffect.gameObject.SetActive(true);
        playerCanvasHitEffect.transform.localScale = Vector3.zero;
        playerCanvasHitEffect.alpha = 1;
    }
    /*
    private IEnumerator WinBattle()
    {
        StopCoroutine(damageCountDown);
        damageCountDown = null;

        MouseUp();

        wait = true;
        attack = false;

        enemy.SetMajorHitAnim();
        enemy.SetFlying(false);
        enemy.SetDefeat(false);

        float x = 0;
        Vector3 pos = enemy.transform.localPosition;

        while (x < 1)
        {
            x += Time.deltaTime / 0.75f;
            float h = x;
            h = x * x * x * (x * (6f * x - 15f) + 10f);
            enemy.transform.localPosition = Vector3.Lerp(pos, enemyStartPos, h);
            yield return new WaitForEndOfFrame();
        }

        enemyHealthBar.gameObject.SetActive(false);
        currentActiveObjects.Clear();
        enemy.transform.localPosition = enemyStartPos;
        enemyLookScript.enabled = true;
        enemyCloudEffect.SetActive(false);

        yield return new WaitForSeconds(5);

        gameManager.player.toggleAimThirdPerson = false;
        gameManager.player.isBattle = false;

        yield return new WaitForSeconds(2);

        FinishBattle();
    }
    */
    void SpawnNewTask()
    {
        if(currentTask)
            Destroy(currentTask.gameObject);

        BG_Task newTask = null;

        //for (int i = 0; i < battleManagerScript.gameSets.Count; i++)
        //{
        int i = battleManagerScript.completedBattles;

        if(battleManagerScript.gameSets.Count > i)
        {
            if (battleManagerScript.tileGameScript.tileGameDifficulty == PlayerStats.Difficulty.Easy)
            {
                if (battleManagerScript.gameSets[i].easyTasks.Count <= taskCounter)
                    taskCounter = 0;

                newTask = Instantiate(battleManagerScript.gameSets[i].easyTasks[taskCounter], transform);
            }
            else if (battleManagerScript.tileGameScript.tileGameDifficulty == PlayerStats.Difficulty.Normal)
            {
                if (battleManagerScript.gameSets[i].normalTasks.Count <= taskCounter)
                    taskCounter = 0;

                newTask = Instantiate(battleManagerScript.gameSets[i].normalTasks[taskCounter], transform);
            }
            else if (battleManagerScript.tileGameScript.tileGameDifficulty == PlayerStats.Difficulty.Hard)
            {
                if (battleManagerScript.gameSets[i].hardTasks.Count <= taskCounter)
                    taskCounter = 0;

                newTask = Instantiate(battleManagerScript.gameSets[i].hardTasks[taskCounter], transform);
            }
        }
        //}

        currentTask = newTask;
        taskCounter++;
        currentTask.transform.localScale = Vector3.zero;

        for (int e = 0; e < currentTask.taskObjects.Count; e++)
        {
            currentTask.taskObjects[e].transform.localScale = Vector3.zero;
        }
    }

    void InitializeBattle()
    {
        ResetTransforms();
        ResetBools();

        if (currentTask)
            Destroy(currentTask.gameObject);

        enemyHealthBar.gameObject.SetActive(false);
        enemyCurrentHealth = enemyStartHealth;
        enemyHealthBarHealth.fillAmount = 1;
        enemyTarget.gameObject.SetActive(false);
        currentTaskBG.localPosition = bgStartPos;
        currentTaskBG.localScale = Vector3.zero;
        currentTaskBG.localEulerAngles = bgStartRot;
        currentDamageBattle = 0;
        currentAttackDamage = 0;

        foreach (var item in tileGameGraphicsToRemove)
        {
            item.gameObject.SetActive(false);
        }

        foreach (var item in battleManagerScript.tileGameScript.currentEnemies)
        {
            item.gameObject.SetActive(false);
        }

        foreach (Transform child in bossGameScript.itemContainer)
        {
            child.gameObject.SetActive(false);
        }
    }

    void ResetTransforms()
    {
        transform.localPosition = startPos;
        currentTaskBG.localPosition = bgStartPos;
        currentTaskBG.localScale = Vector3.zero;
        currentTaskBG.localEulerAngles = bgStartRot;
        enemy.transform.localPosition = enemyStartPos;
        enemyHealthBar.transform.position = enemyHealthBarStartPos;
    }
}
