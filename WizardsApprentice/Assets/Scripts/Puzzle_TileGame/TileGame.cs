﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TileGame : MonoBehaviour, IDifficulty, ISetDifficulty, IAchievementPanel
{
    //Global Fields
    public bool isFinalBossGame;
    public PlayerStats.Difficulty tileGameDifficulty = PlayerStats.Difficulty.Normal;
    private int correctStreakCounter;
    private int wrongStreakCounter;
    public int numberOfCorrects = 5;
    public int numberOfWrongs = 3;
    public GameManager gameManager;
    public TileGameCanvas tileGameCanvas;
    public AchievementPanel achievementPanel;
    public Transform worldTransform;
    public Transform gameTransform;
    public Transform playerStartPos;
    public enum TileTypes { Numerical, Operator, None, Goal, GoalNeighbour, NumericalStatic, OperatorStatic }
    public enum OperatorTypes { Add, Subtract, Multiply, Divide, None }
    public enum TileStates { Available, Selected, Defined, Locked, Unavailable}
    public enum TileGameStyles { Path, PathHard, TrailBlaze}
    public LineRenderer lineRenderer;
    public Vector3[] lrPositions;
    public RectTransform buttonPanel;
    public ValueSlider numericalSlider;
    public ValueSlider operatorSlider;
    public Button numericalButton;
    public Button operatorButton;
    public Button undoButton;
    public Button ResetButton;
    public Button GoalButton;
    public Image goalButtonImage;
    public Image goalGlowImage;
    public TextMeshProUGUI goalButtonText;
    public Image buttonsPanel;
    public Image SliderBg;
    public TextMeshProUGUI currentCalculation;
    public RectTransform moveCounterGroup;
    public Image moveCounterImage;
    public TextMeshProUGUI MoveCounterText;
    public TextMeshProUGUI MaxMoveCountText;
    public Image maxMoveCountImage;
    public RectTransform maxMoveCountGroup;
    public Transform numericalButtonObject;
    public Transform operatorButtonObject;
    public RectTransform setCountPanel;
    public TextMeshProUGUI setCountPanelText;
    public Image setCountImage;

    public List<TG_Tile> tiles = new List<TG_Tile>();    
    public Material defaultMat;
    public Material selectedOperatorMat;
    public Material selectedNumericalMat;
    public Material selectedNoneMat;
    public Material definedOperatorMat;
    public Material definedNumericalMat;
    public Material highlightMat;
    public Material lockedMat;
    public Material startMat;
    public Material goalMat;
    public Material goalActiveMat;
    public Material trailMat;
    public Material keyMat;
    public Material goalEffectMat;
    public Color lineColor;
    public Color operatorColor;
    public Color numericalColor;
    public Color nullColor;
    public List<Color> goalColors = new List<Color>();
    private List<float> tilesStartPosY = new List<float>();
    private const float selectedYdist = -0.05f;
    public const int undoSize = 30;
    public GameObject goalGlowPrefab;
    public GameObject correctEffect;
    public GameObject operateEffect;
    public GameObject failSetEffect;
    public GameObject keyPrefab;
    public GameObject lockPrefab;
    public GameObject enemyPrefab;
    public GameObject gemPhysicalPrefab;
    //public Trophy trophy;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.TileGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;
    public RectTransform winGamePanel;
    public GameObject crackedPrefab;
    public GameObject crackedTileAnim;
    //---------AUDIO-----------
    public AudioSource clickTileSound;
    public AudioSource clickSliderSound;
    public AudioSource releaseSliderSound;
    public AudioSource sliderBlockedSound;
    public AudioSource undoButtonClickSound;
    public AudioSource restartButtonClickSound;
    public AudioSource calculateSound;
    public AudioSource updateMoveCountSound;
    public AudioSource unlockGoalSound;
    public AudioSource unlockKeySound;
    public AudioSource lockGoalSound;
    public AudioSource reachGoalSound;
    public AudioSource SetCompleteSound;
    public AudioSource SetFailSound;
    public AudioSource MoveStoneSound;
    public AudioSource MoveStoneQuicklySound;
    public AudioSource LiquidSound;
    public AudioSource winSound1;
    public AudioSource winSound2;

    [System.Serializable]
    public class GoalKeyTile
    {
        public List<int> keyTiles = new List<int>();
        public List<int> keyTilesValues = new List<int>();
    }
    
    [Header("GAME SETS")]
    public List<TG_GameSet> gameSets = new List<TG_GameSet>();
    public List<TG_GameSet> gameSetsEasy = new List<TG_GameSet>();
    public List<TG_GameSet> gameSetsHard = new List<TG_GameSet>();

    public List<TG_GameSet> completedSets = new List<TG_GameSet>();
    public List<int> completedSetsAchievements = new List<int>();
    //---------------------------------------------------------------------READONLY--------------------------------------------------------------------------//

    public class TileProps
    {
        public bool goalFinish;
        public bool key; // experimental NEW
        public TileGame.TileTypes tileType;
        public TileGame.TileStates tileState;
        public TileGame.OperatorTypes tileOperatorType;
        public int value;
        public bool startTile;
        public bool showSprite;
        public bool colliderEnabled;
        public Material mat;
        public bool showText;
        public string txt;
        public float curYpos;
    }

    public class EnemyProps
    {
        public int enemyIndex;
        public TG_Tile startTile;
        public TG_Tile currentTile;
        public TG_Tile lastTile;
        public int enemyMoveCount;
    }

    [System.Serializable]
    public class TG_History
    {
        public List<TileProps> curTileProps = new List<TileProps>();
        public List<EnemyProps> curEnemyProps = new List<EnemyProps>();

        public TG_GameSet curGameSet;

        public int curMoveCount;
        public int curVal;
        public bool hasCalc;
        public TG_Tile curTile;
        public TG_Tile laTile;
        public TG_Tile laPaTile;
        public TG_Tile curGoalAvailable;
        public bool curGoalButnActive;
        public bool curGoalEffect;
        public bool hasMoFoStart;
        public bool hasAddStaToPath;
        public int curGoalCounter;
        public string curCalcText;
        public TileTypes curTileType;
        public List<TG_Tile> curAvail = new List<TG_Tile>();
        public List<TG_Tile> curPath = new List<TG_Tile>();
        public List<GameObject> curEffects = new List<GameObject>();
        public List<bool> curLocks = new List<bool>();
        public List<TG_Tile> curSinkerTiles = new List<TG_Tile>();
        public List<int> curSinkerTilesTimes = new List<int>();
        public List<bool> curSinkTiles = new List<bool>();
    }

    public List<TG_History> history = new List<TG_History>();
    public int setCounter = 0;
    public TG_GameSet currentGameSet;
    public TileGame.TileGameStyles currentGamestyle;
    public List<TG_Tile> currentTiles = new List<TG_Tile>();
    
    public int currentMoveCounter;
    public int currentValue;
    public bool hasCalculated;
    public int currentGoalCounter;
    public TG_Tile currentTile;
    public TG_Tile lastTile;
    public TG_Tile lastPathTile;
    public TG_Tile currentGoalAvailable;
    public GameObject currentGoalEffect;
    public bool hasMovedFromStart;
    public bool hasAddedStartToPath;
    public bool hasWonGame;
    //public int completedSets = 0;
    public TileTypes currentTileType;
    public List<TG_Tile> currentlyAvailable = new List<TG_Tile>();
    public List<TG_Tile> currentPath = new List<TG_Tile>();
    public List<GameObject> currentEffects = new List<GameObject>();
    public List<int> currentNumbers = new List<int>();
    public List<int> currentOperators = new List<int>();
    public List<Material> currentGoalMaterials = new List<Material>();
    public List<Material> currentActiveGoalMats = new List<Material>();
    public List<Material> currentGoalEffectMats = new List<Material>();
    public List<GoalKeyTile> currentGoalKeyTiles = new List<GoalKeyTile>();
    public List<Material> currentKeyMaterials = new List<Material>();
    public List<Transform> currentLocks = new List<Transform>();
    public List<TileGameEnemy> currentEnemies = new List<TileGameEnemy>();
    public List<TG_Tile> currentSinkerTiles = new List<TG_Tile>();
    public List<int> currentSinkTilesTimes = new List<int>();
    public List<bool> currentSinkTiles = new List<bool>();
    public List<Transform> currentCrackedPrefabs = new List<Transform>();
    public TG_Tile lastOperatorTile = null;
    public TG_Tile lastValueTile = null;

    public int numericalLastKey;
    public int operatorLastKey;

    public List<Transform> wakeUpOnComplete = new List<Transform>();
    public List<Transform> sleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    public delegate void TileGameMoveEvent(int moveCount, int enemyIndex); // Sends to TileGameEnemy
    public static event TileGameMoveEvent OnMoveCountChange;

    public delegate void TileGameEvent(); //Sends to PlayerStats
    public static event TileGameEvent OnTileGameEnable;
    public static event TileGameEvent OnBossTileGameEnable;
    public static event TileGameEvent OnTileGameDisable;

    public delegate void TileBossGameEvent(); // Sends to BossGame_Battle, BossGame script
    public static event TileBossGameEvent OnBossGameReachGoal;
    public static event TileBossGameEvent OnBossGameMove;
    public static event TileBossGameEvent OnBossGameRestart;
    public static event TileBossGameEvent OnBossGameInitialized; //Sends to BG_BattleManager
    public static event TileBossGameEvent OnBossGameWaitForReward; //Sends to BG_BattleManager

    public delegate void TileBossGameUIEvent(int passedGoalValue); //Sends to BossGameGoalsIndicator
    public static event TileBossGameUIEvent OnBossGameReachSubGoal;

    private void Awake()
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            tilesStartPosY.Add(tiles[i].transform.localPosition.y);
        }
    }

    private void OnEnable()
    {        
        TransferPlayerStats();
        //SetPlayerStatsCurrentCompletedSets(); //---FiX 03.03.21---

        InitializeGame();
        StartNewGameSet(setCounter, tileGameDifficulty);

        TG_Tile.OnTileClick += CheckClickedTile;
        TG_GoalButton.OnGoalButtonClick += ClickGoalButton;
        ValueSlider.OnValueSliderUp += CheckSliderInput;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel += StartSelectedGameSet;

        if (!isFinalBossGame)
        {
            if (OnTileGameEnable != null)
                OnTileGameEnable();
        }
        else
        {
            if (OnBossTileGameEnable != null)
                OnBossTileGameEnable();
        }

        OpenDifficultyPanelStart();
    }

    private void OnDisable()
    {
        TG_Tile.OnTileClick -= CheckClickedTile;
        TG_GoalButton.OnGoalButtonClick -= ClickGoalButton;
        ValueSlider.OnValueSliderUp -= CheckSliderInput;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel -= StartSelectedGameSet;

        if (winGamePanel.gameObject.activeSelf)
            winGamePanel.gameObject.SetActive(false);

        if (setCountPanel.gameObject.activeSelf)
            setCountPanel.gameObject.SetActive(false);

        if (OnTileGameDisable != null)
            OnTileGameDisable();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            CheckMousePress();

        UpdateLineRenderer();
    }

    public void ClickGoalButton()
    {
        clickSliderSound.Play();
        print("Clicked goal button");
        CheckClickedTile(currentGoalAvailable);        
    }
    
    private IEnumerator ScaleGoalButton()
    {
        var easyScale = GoalButton.transform.parent.GetComponent<EasyScalePulse>();
        easyScale.enabled = false;

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime/ 0.25f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            GoalButton.transform.parent.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        GoalButton.transform.parent.gameObject.SetActive(false);
        easyScale.enabled = true;
        GoalButton.transform.parent.localScale = Vector3.one;
    }

    void CheckClickedTile(TG_Tile passedTileScript) //MAIN CHECKS, RUNS IF CLICKED ON TILE COLLIDER
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            if (tiles[i] == passedTileScript)
            {
                if (clickTileSound)
                    clickTileSound.Play();


                if (!currentGameSet.goalTiles.Contains(tiles.IndexOf(passedTileScript)) || (currentGameSet.goalTiles.Contains(tiles.IndexOf(passedTileScript)) && tiles[i].goalFinished))
                {
                    if (currentlyAvailable.Contains(passedTileScript) || currentGameSet.startTiles.Contains(tiles.IndexOf(passedTileScript)))
                    {
                        if (currentGameSet.useHistory)
                        {
                            if (history.Count < undoSize)
                                AddHistory();
                            else
                            {
                                history.RemoveAt(0);
                                AddHistory();
                            }
                        }


                        hasCalculated = false;
                        ResetTileHeight();

                        if (currentMoveCounter > 0)
                            hasAddedStartToPath = true;

                        if (currentTile)
                        {
                            lastTile = currentTile;

                            if (currentGameSet.startTiles.Contains(tiles.IndexOf(lastTile)) && !hasAddedStartToPath)
                                lastPathTile = lastTile;

                            if (lastTile.tileState == TileStates.Selected)
                                lastTile.meshRenderer.material = highlightMat;
                        }

                        SetSelected(passedTileScript);
                        SetCurrentTileHeight();
                        ToggleActiveSlider();

                        if (!hasMovedFromStart)
                            CheckNewAvailablesStart();

                        SetPlayerDestination(currentTile.transform);
                        //-------
                        bool calcCheck = false;

                        if (currentPath.Count >= 3)
                        {
                            if (currentTile != lastPathTile)
                            {
                                if ((currentPath[currentPath.Count - 1].tileType == TileTypes.Numerical || currentPath[currentPath.Count - 1].tileType == TileTypes.NumericalStatic) &&
                                    (currentPath[currentPath.Count - 2].tileType == TileTypes.Operator || currentPath[currentPath.Count - 2].tileType == TileTypes.OperatorStatic) &&
                                    (currentPath[currentPath.Count - 3].tileType == TileTypes.Numerical || currentPath[currentPath.Count - 3].tileType == TileTypes.NumericalStatic))                                
                                {
                                    calcCheck = true;
                                }
                            }
                            else if (currentTile.tileType == TileTypes.OperatorStatic && currentPath.Count > 3)
                            {
                                if ((currentPath[currentPath.Count - 1].tileType == TileTypes.Operator || currentPath[currentPath.Count - 1].tileType == TileTypes.OperatorStatic) &&
                                    (currentPath[currentPath.Count - 2].tileType == TileTypes.Numerical || currentPath[currentPath.Count - 2].tileType == TileTypes.NumericalStatic) &&
                                    (currentPath[currentPath.Count - 3].tileType == TileTypes.Operator|| currentPath[currentPath.Count - 3].tileType == TileTypes.OperatorStatic))
                                {
                                    calcCheck = true;
                                }
                            }
                        }
                        //-----
                        //if (currentTile != lastPathTile || (currentTile.tileType == TileTypes.OperatorStatic))
                        if (calcCheck)
                        {
                            Calculation(passedTileScript);

                            if (hasCalculated)
                                SetNewTiles();

                            if(lastPathTile && currentTile.tileType != TileTypes.OperatorStatic)
                                CheckKey(lastPathTile);
                            else if(lastTile && currentTile.tileType == TileTypes.OperatorStatic)
                                CheckKey(lastTile);

                            for (int e = 0; e < currentGameSet.goalTiles.Count; e++)
                            {
                                if (tiles[currentGameSet.goalTiles[e]].connectedTiles.Contains(passedTileScript))
                                    CheckAnswer(passedTileScript);
                            }
                        }                        

                        if (!CheckCurrentGoal())
                        {
                            if (currentGoalAvailable)
                            {
                                ReverseGoalCompletion();
                            }

                            GoalButton.transform.parent.gameObject.SetActive(false);
                        }
                        else
                        {
                            int index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(currentGoalAvailable));
                            GoalButton.transform.parent.gameObject.SetActive(true);
                            goalButtonText.text = currentGoalAvailable.value.ToString();
                            goalButtonImage.color = goalColors[index];
                            goalGlowImage.color = new Vector4(goalColors[index].r, goalColors[index].g, goalColors[index].b, goalGlowImage.color.a);
                        }
                    }
                }
                else
                {
                    if (!passedTileScript.goalFinished)
                    {
                        StartCoroutine(CheckGoal(passedTileScript));
                        SetPlayerDestination(passedTileScript.transform);
                        StartCoroutine(ScaleGoalButton());
                    }
                }
            }
        }
    }

    private IEnumerator UpdateMoveCounter() //"Animated"
    {
        float t = 0;
        var startColor = moveCounterImage.color;
        var newColor = moveCounterImage.color * 1.3f;
        var startSize = moveCounterGroup.localScale;
        var newSize = moveCounterGroup.localScale * 1.3f;

        while(t < 1)
        {
            t += Time.deltaTime * 8f;
            moveCounterGroup.localScale = Vector3.Lerp(startSize, newSize, t);
            moveCounterImage.color = Vector4.Lerp(startColor, newColor, t);
            yield return new WaitForEndOfFrame();
        }

        t = 0;

        while (t < 1)
        {
            t += Time.deltaTime * 8f;
            moveCounterGroup.localScale = Vector3.Lerp(newSize, startSize, t);
            moveCounterImage.color = Vector4.Lerp(newColor, startColor, t);
            yield return new WaitForEndOfFrame();
        }

        if (updateMoveCountSound)
            updateMoveCountSound.Play();

        currentMoveCounter++;

        if (OnBossGameMove != null)
            OnBossGameMove();

        if (currentGameSet.maximumMoves > 0 && currentGameSet.maximumMoves < currentMoveCounter)
        {
            StartCoroutine(SetFail(false));
        }

        MoveCounterText.text = currentMoveCounter.ToString();

        if(currentSinkTiles.Count > 0) //Check SinkTiles
        {
            for (int i = 0; i < currentSinkTiles.Count; i++)
            {
                if(currentSinkTilesTimes[i] <= currentMoveCounter && !currentSinkTiles[i])
                {
                    var tile = tiles[tiles.IndexOf(currentSinkerTiles[i])];

                    if (tile.tileType != TileTypes.Goal && tile.key == null)
                    {
                        StartCoroutine(SinkTile(tile, true));
                        currentSinkTiles[i] = true;

                        if (tile == currentTile)
                            StartCoroutine(SetFail(true));
                    }
                }
            }
        }

        if(OnMoveCountChange != null)
        {
            OnMoveCountChange(currentMoveCounter, 0);
        }
    }

    private IEnumerator SetFail(bool characterFalls)
    {
        if(characterFalls)
            gameManager.player.anim.SetTrigger("Fall");

        var failEffect = Instantiate(failSetEffect, currentTile.transform.position + (Vector3.up * 2), Quaternion.identity, gameTransform);

        yield return new WaitForSeconds(0.5f);

        SetFailSound.Play();
        
        if(!characterFalls)
            yield return new WaitForSeconds(0.5f);
        else
        {
            float t = 0;            

            while(t < 1)
            {
                t += Time.deltaTime / 0.5f;
                gameManager.player.navMeshAgent.baseOffset = Mathf.Lerp(0, -1, t);
                yield return new WaitForEndOfFrame();
            }
        }

        ResetCurrentGameSet();
    }

    private void CheckNewAvailablesStart()
    {
        if (currentGameSet.startTiles.Contains(tiles.IndexOf(currentTile)))
        {
            foreach (var item in currentTile.connectedTiles)
            {
                if (item.tileType == TileTypes.None)
                {
                    if (item.tileState != TileStates.Locked) // New TestCondition
                    {
                        SetAvailable(item);

                        if (currentTile.tileType == TileTypes.None)
                            item.tileType = TileTypes.Numerical;
                        if (currentTile.tileType == TileTypes.Numerical)
                            item.tileType = TileTypes.Operator;
                    }
                }
                if(currentTileType == TileTypes.Numerical && item.tileType == TileTypes.OperatorStatic)
                {
                    SetAvailable(item);
                }
            }

            if(currentTileType == TileTypes.None)
                SetUnavailable(currentTile);
            if (currentTileType == TileTypes.Numerical)
            {
                SetDefined(currentTile);
                currentTile.tileCollider.enabled = false;
                currentTile.textMesh.text = currentTile.value.ToString();
                numericalButtonObject.gameObject.SetActive(false);
                operatorButtonObject.gameObject.SetActive(false);
                SetCalculationText(currentTile.textMesh.text);
            }

            foreach (var item in currentGameSet.startTiles)
            {
                if (item != tiles.IndexOf(currentTile))
                {
                    if(!(tiles[item].tileType == TileTypes.None && tiles[item].tileState == TileStates.Locked)) // New Test Condition
                        SetUnavailable(tiles[item]);                    
                }

                tiles[item].startTile = false;
            }

            hasMovedFromStart = true;
        }
    }

    void ToggleActiveSlider()
    {
        if (currentTileType == TileTypes.Numerical)
        {
            numericalButtonObject.gameObject.SetActive(true);
            operatorButtonObject.gameObject.SetActive(false);
            buttonsPanel.color = numericalColor;
            SliderBg.color = numericalColor;
        }
        if (currentTileType == TileTypes.Operator)
        {
            numericalButtonObject.gameObject.SetActive(false);
            operatorButtonObject.gameObject.SetActive(true);
            buttonsPanel.color = operatorColor;
            SliderBg.color = operatorColor;
        }
        if (currentTileType != TileTypes.Numerical && currentTileType != TileTypes.Operator)
        {
            numericalButtonObject.gameObject.SetActive(false);
            operatorButtonObject.gameObject.SetActive(false);
            buttonsPanel.color = nullColor;
            SliderBg.color = nullColor;
        }
    }

    void CheckSliderInput(ValueSlider passedSlider)
    {               
        if(passedSlider == operatorSlider)
        {
            CheckPathTiles();

            if (currentGameSet.forbiddenOperators.Count > 0)
            {
                BlockOperatorSliderInputs();
            }

            if (currentPath.Count >= 2)
                BlockDivisonOperatorIfZero();

            if(passedSlider.currentKey == 0)
                DefineCurrentTile(TileTypes.Operator, operatorSlider.currentKey, OperatorTypes.Add);
            else if (passedSlider.currentKey == 1)
                DefineCurrentTile(TileTypes.Operator, operatorSlider.currentKey, OperatorTypes.Subtract);
            else if (passedSlider.currentKey == 2)
                DefineCurrentTile(TileTypes.Operator, operatorSlider.currentKey, OperatorTypes.Multiply);
            else if (passedSlider.currentKey == 3)
                DefineCurrentTile(TileTypes.Operator, operatorSlider.currentKey, OperatorTypes.Divide);

            if (releaseSliderSound)
                releaseSliderSound.Play();
            //print("operatorSlider");
        }
        else if(passedSlider == numericalSlider)
        {
            CheckPathTiles();

            if (currentGameSet.forbiddenNumbers.Count > 0)
            {
                BlockNumericalSliderInputs();
            }

            if (currentPath.Count >= 2 && lastValueTile != null && lastOperatorTile != null)
            {
                BlockNumericalOnDivision();
                BlockIfResultExceedsLimit(numericalSlider.currentKey); //Defaults to minimum values if limit is reached
            }

            DefineCurrentTile(TileTypes.Numerical, numericalSlider.currentKey, OperatorTypes.None);

            if (releaseSliderSound)
                releaseSliderSound.Play();
        }

        numericalLastKey = numericalSlider.currentKey;
        operatorLastKey = operatorSlider.currentKey;
    }
    
    int SetNewKey(List<int> AvailableKeys, ValueSlider slider) // Copy this to other sliders with blocked values
    {
        int sliderLastKey = 0;

        if (slider == numericalSlider)
            sliderLastKey = numericalLastKey;
        else if (slider == operatorSlider)
            sliderLastKey = operatorLastKey;

        var NewKey = HelperFunctions.RoundToList(slider.currentKey, AvailableKeys);
        var newKeyIndex = AvailableKeys.IndexOf(NewKey);
        var firstAvailable = AvailableKeys[0];
        var lastAvailable = AvailableKeys[AvailableKeys.Count - 1];

        var nextKey = 0;
        if (newKeyIndex < AvailableKeys.Count - 1)
            nextKey = AvailableKeys[newKeyIndex + 1];
        else
            nextKey = firstAvailable;

        var prevKey = 0;
        if (newKeyIndex > 0)
            prevKey = AvailableKeys[newKeyIndex - 1];
        else
            prevKey = lastAvailable;
 
        
        if (NewKey != slider.currentKey)
        {
            if (sliderBlockedSound)
                sliderBlockedSound.Play();
        }

        if (sliderLastKey < slider.currentKey || (sliderLastKey == lastAvailable && (slider.currentKey == 0 || slider.currentKey > lastAvailable))) //Attempts to go up
        {
            if (NewKey < slider.currentKey || (NewKey == lastAvailable && (slider.currentKey == 0 || slider.currentKey > lastAvailable))) // but new key is set below
            {
                // Skips to next available key
                if (AvailableKeys.Count - 1 > newKeyIndex)
                    //NewKey = nextKey;
                    NewKey = AvailableKeys[newKeyIndex + 1];
                else if (AvailableKeys.Count - 1 == newKeyIndex)
                    //NewKey = firstAvailable;
                    NewKey = AvailableKeys[0];
            }
        }
        else if (sliderLastKey > slider.currentKey || (sliderLastKey == firstAvailable && (slider.currentKey == slider.key - 1 || slider.currentKey < firstAvailable))) // Attempts to go down
        {
            if (NewKey > slider.currentKey || (NewKey == firstAvailable && (slider.currentKey == slider.key - 1 || slider.currentKey < firstAvailable))) // but new key is set above                              
            {
                // Skips to previous available key
                if (newKeyIndex > 0)
                    //NewKey = prevKey;
                    NewKey = AvailableKeys[newKeyIndex - 1];
                else if (newKeyIndex == 0)
                    //NewKey = lastAvailable;
                    NewKey = AvailableKeys[AvailableKeys.Count - 1];
            }
        }     
        
        return NewKey;
    }
    /*
    int SetBlockedKey(int CurrentKey, List<int> AvailableKeys, ValueSlider slider)
    {
        var newKey = HelperFunctions.RoundToList(slider.currentKey, AvailableKeys);
        var newKeyIndex = AvailableKeys.IndexOf(newKey);

        if(newKey < slider.currentKey)
        { }
        else if (newKey < slider.currentKey)
        { }

        return newKey;
    }
    */
    void BlockNumericalSliderInputs()
    {
        //for (int i = 0; i < currentGameSet.forbiddenNumbers.Count; i++)
        //{
        if (!currentNumbers.Contains(numericalSlider.currentKey))
        {          
            numericalSlider.SetKey(SetNewKey(currentNumbers, numericalSlider));
        }
        //}
    }
        
    void BlockOperatorSliderInputs()
    {
        //for (int i = 0; i < currentGameSet.forbiddenOperators.Count; i++)
        //{
        if (!currentOperators.Contains(operatorSlider.currentKey))
        {            
            operatorSlider.SetKey(SetNewKey(currentOperators, operatorSlider));
        }
        //}
    }
    void BlockDivisonOperatorIfZero()
    {
        if (lastValueTile)
        {
            if (lastValueTile.value == 0)
            {
                if (currentOperators.Contains(3))
                {
                    if (operatorSlider.currentKey == 3) // 3 = division
                    {
                        if (sliderBlockedSound)
                            sliderBlockedSound.Play();

                        var tempList = new List<int>();

                        foreach (var item in currentOperators)
                        {
                            tempList.Add(item);
                        }

                        tempList.Remove(3);

                        operatorSlider.SetKey(SetNewKey(tempList, operatorSlider));
                    }
                }
            }
        }
    }

    void CheckPathTiles()
    {
        if (currentPath.Count >= 1)
        {
            if(currentTile)
            {
                if(currentTile.tileType == TileTypes.Operator || currentTile.tileType == TileTypes.OperatorStatic)
                {
                    lastOperatorTile = currentTile;
                        
                    if(currentTile == lastPathTile)
                    {
                        lastValueTile = currentPath[currentPath.Count - 2];
                    }
                    else
                    {
                        lastValueTile = currentPath[currentPath.Count - 1];
                    }
                }
                else if((currentTile.tileType == TileTypes.Numerical || currentTile.tileType == TileTypes.NumericalStatic) && currentPath.Count >= 2)
                {
                    if(currentTile == lastPathTile)
                    {
                        lastOperatorTile = currentPath[currentPath.Count - 2];

                        if (currentPath.Count > 2)                                      ////????????????
                            lastValueTile = currentPath[currentPath.Count - 3];
                        else
                            lastValueTile = currentPath[currentPath.Count - 2];
                    }
                    else
                    {
                        lastOperatorTile = currentPath[currentPath.Count - 1];
                        lastValueTile = currentPath[currentPath.Count - 2];
                    }                    
                }                
            }
        }    
    }
    void BlockNumericalOnDivision()
    {                
        if (lastOperatorTile.tileType == TileTypes.Operator || lastOperatorTile.tileType == TileTypes.OperatorStatic)
        {
            if (lastOperatorTile.tileOperatorType == OperatorTypes.Divide)
            {
                var newSuggestions = new List<int>();

                for (int i = 1; i < 10; i++) // skips zero 
                {
                    if (lastValueTile.value % i == 0)
                    {
                        newSuggestions.Add(i);
                    }
                }

                bool check = false;

                for (int i = newSuggestions.Count - 1; i >= 0; i--)
                {
                    if (currentNumbers.Contains(newSuggestions[i]))
                        check = true;
                    else
                        newSuggestions.Remove(newSuggestions[i]);
                }

                if (check)
                {                
                    numericalSlider.SetKey(SetNewKey(newSuggestions, numericalSlider));
                }
                else
                {
                    if (1 != numericalSlider.currentKey)
                    {
                        if (sliderBlockedSound)
                            sliderBlockedSound.Play();
                    }

                    numericalSlider.SetKey(1); //Attention!!: Sets number to 1 even if 1 is forbidden. Workaround to avoid further complications with division rule (to ensure that results are ints)
                }
            }
        }
    }

    void SetNewNumericalKey(int highestNumber)
    {        
        var newList = new List<int>();

        for (int i = 0; i < currentNumbers.Count; i++)
        {
            if (i <= highestNumber)
                newList.Add(currentNumbers[i]);
        }

        numericalSlider.SetKey(SetNewKey(newList, numericalSlider));
    }

    void BlockIfResultExceedsLimit(int currentKey) //Blocks user input if result exceeds 99 or -99 ------ TODO: Change blocking to new method (SetNewKey()) !!!!
    {
        if (lastOperatorTile.tileType == TileTypes.Operator || lastPathTile.tileType == TileTypes.OperatorStatic)
        {
            if(lastOperatorTile.tileOperatorType == OperatorTypes.Add)
            {
                if (lastValueTile.value + currentKey > 99 || lastValueTile.value + currentKey < -99)
                {
                    var highestNumber = Mathf.Abs(99 - (lastValueTile.value));

                    SetNewNumericalKey(highestNumber);
                }
            }
            if (lastOperatorTile.tileOperatorType == OperatorTypes.Subtract)
            {
                if (lastValueTile.value - currentKey < -99 || lastValueTile.value - currentKey > 99)
                {
                    var highestNumber = ((lastValueTile.value) + 99);

                    SetNewNumericalKey(highestNumber);
                    /*
                    if (sliderBlockedSound)
                        sliderBlockedSound.Play();

                    numericalSlider.SetKey(0);
                    */
                }
            }
            if (lastOperatorTile.tileOperatorType == OperatorTypes.Multiply)
            {
                if (lastValueTile.value * currentKey > 99 || lastValueTile.value * currentKey < -99)
                {
                    var highestNumber = Mathf.FloorToInt(99 / lastValueTile.value);

                    SetNewNumericalKey(highestNumber);
                }
            }            
        }
    }

    void DefineCurrentTile(TileTypes passedTileType, int passedValue, OperatorTypes passedOperator)
    {
        if (currentTile)
        {
            if (passedTileType == TileTypes.Operator || passedTileType == TileTypes.OperatorStatic)
            {
                if (passedOperator == OperatorTypes.Add)
                {
                    currentTile.textMesh.text = "+";
                    currentTile.tileOperatorType = OperatorTypes.Add;
                }
                else if (passedOperator == OperatorTypes.Subtract)
                {
                    currentTile.textMesh.text = "-";
                    currentTile.tileOperatorType = OperatorTypes.Subtract;
                }
                else if (passedOperator == OperatorTypes.Multiply)
                {
                    currentTile.textMesh.text = "•";
                    currentTile.tileOperatorType = OperatorTypes.Multiply;
                }
                else if (passedOperator == OperatorTypes.Divide)
                {
                    currentTile.textMesh.text = ":";
                    currentTile.tileOperatorType = OperatorTypes.Divide;
                }

                currentTile.value = 0;
            }
            else if (passedTileType == TileTypes.Numerical || passedTileType == TileTypes.NumericalStatic)
            {
                currentTile.tileOperatorType = OperatorTypes.None;
                currentTile.value = passedValue;
                currentTile.textMesh.text = passedValue.ToString();
            }

            if (lastPathTile && !hasAddedStartToPath) // Adds start tile to path if start tile is Type None
            {
                if (currentGameSet.startTiles.Contains(tiles.IndexOf(lastPathTile)))
                {
                    if (lastPathTile.tileType == TileTypes.None)
                    {
                        print("Adds start tile to path");
                        currentPath.Add(lastPathTile);
                        hasAddedStartToPath = true;
                    }
                }
            }

            SetDefined(currentTile);

            SetAvailables();

            ChangeCalculationText();
        }
    }

    void ChangeCalculationText()
    {
        if (currentGameSet.gameStyle == TileGameStyles.TrailBlaze)
        {
            if (currentPath.Count > 0)
            {
                if (currentPath[0].tileType == TileTypes.Numerical || currentPath[0].tileType == TileTypes.NumericalStatic)
                {
                    if (currentPath.Count == 1)
                        currentCalculation.text = currentPath[0].textMesh.text;
                    else if (currentPath.Count == 2)
                        currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text;
                    else if (currentPath.Count == 3)
                        currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + "=";
                }
                else if(currentPath[0].tileType == TileTypes.None)
                {
                    if (currentPath.Count == 2)
                        currentCalculation.text = currentPath[1].textMesh.text;
                    else if (currentPath.Count == 3)
                        currentCalculation.text = currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text;
                    else if (currentPath.Count == 4)
                        currentCalculation.text = currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + currentPath[3].textMesh.text + " " + "=";
                }
            }
        }
        else if(currentGameSet.gameStyle == TileGameStyles.Path)
        {
            if (currentPath.Count > 0)
            {
                if (currentPath[0].tileType == TileTypes.Numerical || currentPath[0].tileType == TileTypes.NumericalStatic)
                {
                    if (currentPath.Count == 1)
                        currentCalculation.text = currentPath[0].textMesh.text;
                    else if (currentPath.Count == 2)
                        currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text;
                    else if (currentPath.Count == 3)
                        currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + "=";
                    else if (currentPath.Count > 3)
                    {
                        if(currentPath[currentPath.Count - 1].tileType != TileTypes.OperatorStatic)
                            currentCalculation.text = currentPath[currentPath.Count - 3].textMesh.text + " " + currentPath[currentPath.Count - 2].textMesh.text + " " + currentPath[currentPath.Count - 1].textMesh.text;
                        else
                            currentCalculation.text = currentPath[currentPath.Count - 2].textMesh.text + " " + currentPath[currentPath.Count - 1].textMesh.text;

                        if (currentPath[currentPath.Count - 1].tileType == TileTypes.Numerical || currentPath[currentPath.Count - 1].tileType == TileTypes.NumericalStatic)
                            currentCalculation.text += " " + "=";
                    }
                }
                else if (currentPath[0].tileType == TileTypes.None)
                {
                    if (currentPath.Count == 2)
                        currentCalculation.text = currentPath[1].textMesh.text;
                    else if (currentPath.Count == 3)
                        currentCalculation.text = currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text;
                    else if (currentPath.Count == 4)
                        currentCalculation.text = currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + currentPath[3].textMesh.text + " " + "=";
                    else if (currentPath.Count > 4)
                    {
                        if (currentPath[currentPath.Count - 1].tileType != TileTypes.OperatorStatic)
                            currentCalculation.text = currentPath[currentPath.Count - 3].textMesh.text + " " + currentPath[currentPath.Count - 2].textMesh.text + " " + currentPath[currentPath.Count - 1].textMesh.text;
                        else
                            currentCalculation.text = currentPath[currentPath.Count - 2].textMesh.text + " " + currentPath[currentPath.Count - 1].textMesh.text;

                        if (currentPath[currentPath.Count - 1].tileType == TileTypes.Numerical || currentPath[currentPath.Count - 1].tileType == TileTypes.NumericalStatic)
                            currentCalculation.text += " " + "=";
                    }
                }
            }
        }
        else if(currentGameSet.gameStyle == TileGameStyles.PathHard)
        {
            if (currentPath.Count > 0)
            {
                string newText = "";

                for (int i = 0; i < currentPath.Count; i++)
                {
                    if (i == 0 && (currentPath[0].tileType == TileTypes.Numerical || currentPath[0].tileType == TileTypes.NumericalStatic))
                        newText += currentPath[i].textMesh.text + " ";
                    else if (i == 0 && currentPath[0].tileType == TileTypes.None)
                    { }
                    if (i > 0)
                        newText += currentPath[i].textMesh.text + " ";
                }
                
                currentCalculation.text = "";
                currentCalculation.text = newText;
            }
        }
    }

    void FinishCalculationText(int result)
    {
        if (currentGameSet.gameStyle == TileGameStyles.TrailBlaze)
        {
            if (currentPath[0].tileType == TileTypes.Numerical || currentPath[0].tileType == TileTypes.NumericalStatic)
                currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + "=" + " " + result.ToString();
            else if (currentPath[0].tileType == TileTypes.None)
            {
                if(currentPath.Count < 4) // NEW TEST CONDITION TO FIX OUT OF RANGE EXCEPTION !!!
                    currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + "=" + " " + result.ToString(); 
                else
                    currentCalculation.text = currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + currentPath[3].textMesh.text + " " + "=" + " " + result.ToString(); //----------------------------- WAS OUT OF RANGE EXCEPTION HERE!!!!!!!!!!!!
            }
        }
        else if(currentGameSet.gameStyle == TileGameStyles.Path)
        {
            if (currentPath[0].tileType == TileTypes.Numerical || currentPath[0].tileType == TileTypes.NumericalStatic)
            {
                if (currentPath.Count < 4)
                    currentCalculation.text = currentPath[0].textMesh.text + " " + currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + "=" + " " + result.ToString();
                else
                {
                    if(currentTile.tileType != TileTypes.OperatorStatic)
                        currentCalculation.text = currentPath[currentPath.Count - 3].textMesh.text + " " + currentPath[currentPath.Count - 2].textMesh.text + " " + currentPath[currentPath.Count - 1].textMesh.text + " " + "=" + " " + result.ToString();
                    else
                        currentCalculation.text = currentPath[currentPath.Count - 4].textMesh.text + " " + currentPath[currentPath.Count - 3].textMesh.text + " " + currentPath[currentPath.Count - 2].textMesh.text + " " + "=" + " " + result.ToString();
                }
            }
            else if (currentPath[0].tileType == TileTypes.None)
            {
                if (currentPath.Count < 5)
                    currentCalculation.text = currentPath[1].textMesh.text + " " + currentPath[2].textMesh.text + " " + currentPath[3].textMesh.text + " " + "=" + " " + result.ToString();
                else
                {
                    if (currentTile.tileType != TileTypes.OperatorStatic)
                        currentCalculation.text = currentPath[currentPath.Count - 3].textMesh.text + " " + currentPath[currentPath.Count - 2].textMesh.text + " " + currentPath[currentPath.Count - 1].textMesh.text + " " + "=" + " " + result.ToString();
                    else
                        currentCalculation.text = currentPath[currentPath.Count - 4].textMesh.text + " " + currentPath[currentPath.Count - 3].textMesh.text + " " + currentPath[currentPath.Count - 2].textMesh.text + " " + "=" + " " + result.ToString();
                }
            }
        }
        else if(currentGameSet.gameStyle == TileGameStyles.PathHard)
        {
            string newText = "";

            for (int i = 0; i < currentPath.Count; i++)
            {
                if (i == 0 && (currentPath[0].tileType == TileTypes.Numerical || currentPath[0].tileType == TileTypes.NumericalStatic))
                    newText += currentPath[i].textMesh.text + " ";
                else if (i == 0 && currentPath[0].tileType == TileTypes.None)
                { }
                if (i > 0)
                    newText += currentPath[i].textMesh.text + " ";
            }
            
            newText += "=" + " " + result.ToString();
            currentCalculation.text = "";
            currentCalculation.text = newText;
        }
    }

    void WipeCalculationText()
    {
        currentCalculation.text = "";
    }

    void SetCalculationText(string newText)
    {
        currentCalculation.text = newText;
    }

    public void SetAvailables()
    {
        foreach (var item in currentlyAvailable)
        {
            if (item != currentTile)
            {
                CheckUnavailable(item);
            }
        }

        currentlyAvailable.Clear();

        foreach (var item in currentTile.connectedTiles) //Availibility checks:
        {
            if (!currentPath.Contains(item))
            {
                CheckAvailable(item);
            }
        }
    }
    
    void CheckAvailable(TG_Tile tile)
    {
        if (tile.tileState != TileStates.Defined)
        {
            bool enemyCheck = true;

            if (tile.tileType == TileTypes.None)
            {                
                foreach (var enemy in currentEnemies)
                {
                    if (enemy.enemyCurrentTile == tile)
                        enemyCheck = false;
                }
            }

            if (enemyCheck)
            {
                if (tile.tileType == TileTypes.GoalNeighbour && (currentTile.tileType == TileTypes.Numerical || currentTile.tileType == TileTypes.NumericalStatic))
                {
                    SetAvailable(tile);
                }
                if (tile.tileType == TileTypes.Goal)
                {
                    SetAvailable(tile);
                }
                if (currentTile.tileType == TileTypes.GoalNeighbour && (lastPathTile.tileType == TileTypes.Numerical || lastPathTile.tileType == TileTypes.NumericalStatic)
                    && currentPath.Count > 2)
                {
                    SetAvailable(tile);
                }
                if (tile.tileType == TileTypes.NumericalStatic && (currentTile.tileType == TileTypes.Operator || currentTile.tileType == TileTypes.OperatorStatic))
                {
                    if (!CheckIllegalOperations(tile))
                        SetAvailable(tile);
                }
                if (tile.tileType == TileTypes.OperatorStatic && (currentTile.tileType == TileTypes.Numerical || currentTile.tileType == TileTypes.NumericalStatic))
                {
                    SetAvailable(tile);
                }
                if ((tile.tileType == TileTypes.None && tile.tileState != TileStates.Locked) || tile.tileState != TileStates.Locked)
                {
                    SetAvailable(tile);
                }
            }
        }            
    }

    void CheckUnavailable(TG_Tile tile)
    {
        if (tile.tileType != TileTypes.GoalNeighbour && tile.tileType != TileTypes.Goal &&
            tile.tileType != TileTypes.NumericalStatic && tile.tileType != TileTypes.OperatorStatic && !currentPath.Contains(tile))
        {
            if (tile.tileType == TileTypes.None && tile.tileState == TileStates.Locked)
                SetLocked(tile, tile.value, tile.tileType, tile.tileOperatorType);
            else
                SetUnavailable(tile);
        }
        if (tile.tileType == TileTypes.GoalNeighbour || tile.tileType == TileTypes.Goal ||
            tile.tileType == TileTypes.NumericalStatic || tile.tileType == TileTypes.OperatorStatic)
        {
            SetLocked(tile, tile.value, tile.tileType, tile.tileOperatorType);
        }
        if (tile.tileType == TileTypes.GoalNeighbour && (currentTile.tileType != TileTypes.Numerical && currentTile.tileType != TileTypes.NumericalStatic)) //Resets GoalNeighbour
        {
            SetLocked(tile, tile.value, tile.tileType, tile.tileOperatorType);
        }
        
        if (tile.tileState == TileStates.Available && tile.tileType == TileTypes.None)
        {
            bool enemyCheck = true;

            foreach (var enemy in currentEnemies)
            {
                if (enemy.enemyCurrentTile == tile)
                    enemyCheck = false;
            }

            if (!enemyCheck)
            {
                SetUnavailable(tile);
            }
        }
    }

    bool CheckIllegalOperations(TG_Tile tile)
    {
        bool divisionCheck = false;
        bool multiplicationCheck = false;
        bool additionCheck = false;
        bool subtractionCheck = false;

        if (currentPath.Count >= 2 && currentTile == currentPath[currentPath.Count - 1])
        {
            if (currentTile.tileOperatorType == OperatorTypes.Divide) //Division
            {
                if (currentPath[currentPath.Count - 2].value % tile.value != 0 || tile.value == 0)
                {
                    divisionCheck = true;
                }
            }
            if(currentTile.tileOperatorType == OperatorTypes.Multiply) //Multiplication
            {
                if (currentPath[currentPath.Count - 2].value * tile.value > 99 || currentPath[currentPath.Count - 2].value * tile.value < -99)
                {
                    multiplicationCheck = true;
                }
            }
            if (currentTile.tileOperatorType == OperatorTypes.Add) //Addition
            {
                if (currentPath[currentPath.Count - 2].value + tile.value > 99 || currentPath[currentPath.Count - 2].value + tile.value < -99)
                {
                    additionCheck = true;
                }
            }
            if (currentTile.tileOperatorType == OperatorTypes.Subtract) //Subtraction
            {
                if (currentPath[currentPath.Count - 2].value - tile.value > 99 || currentPath[currentPath.Count - 2].value - tile.value < -99)
                {
                    subtractionCheck = true;
                }
            }
        }

        if (!divisionCheck && !multiplicationCheck && !additionCheck && !subtractionCheck)
            return false;
        else
            return true;
    }
    //SET STATES:---------------------------------------------------------------------------------------------

    public void SetAvailable(TG_Tile passedTile)
    {        
        passedTile.tileState = TileStates.Available;
        passedTile.tileCollider.enabled = true;
        passedTile.meshRenderer.material = highlightMat;
        passedTile.sprite.gameObject.SetActive(false);

        if (passedTile.tileType != TileTypes.Goal && passedTile.tileType != TileTypes.GoalNeighbour &&
            passedTile.tileType != TileTypes.NumericalStatic && passedTile.tileType != TileTypes.OperatorStatic)
        {
            passedTile.textMesh.text = "";
        }

        currentlyAvailable.Add(passedTile);
    }

    public void SetSelected(TG_Tile passedTile)
    {
        currentTile = passedTile;
        passedTile.tileState = TileStates.Selected;
        passedTile.sprite.gameObject.SetActive(false);

        if (passedTile.tileType != TileTypes.Goal && passedTile.tileType != TileTypes.GoalNeighbour && 
            passedTile.tileType != TileTypes.NumericalStatic && passedTile.tileType != TileTypes.OperatorStatic)
        {
            passedTile.textMesh.text = "";

            if (lastPathTile)
            {
                if (lastPathTile.tileType == TileTypes.None || lastPathTile.tileType == TileTypes.Operator || lastPathTile.tileType == TileTypes.OperatorStatic)
                {
                    passedTile.meshRenderer.material = selectedNumericalMat;
                    passedTile.tileType = TileTypes.Numerical;
                    currentTileType = TileTypes.Numerical;
                }
                else
                {
                    passedTile.meshRenderer.material = selectedOperatorMat;
                    passedTile.tileType = TileTypes.Operator;
                    currentTileType = TileTypes.Operator;
                }                
            }
            else
            {
                currentTileType = passedTile.tileType;
            }
        }
        else
        {
            currentTileType = passedTile.tileType;

            if(passedTile.tileType == TileTypes.GoalNeighbour)
            {
                //passedTile.tileCollider.enabled = false;
            }
        }
        
        if(passedTile.tileType == TileTypes.NumericalStatic || passedTile.tileType == TileTypes.OperatorStatic)
        {
            if (passedTile.tileType == TileTypes.NumericalStatic)
            {
                DefineCurrentTile(TileTypes.NumericalStatic, passedTile.value, passedTile.tileOperatorType);
                passedTile.meshRenderer.material = definedNumericalMat;
                currentTileType = TileTypes.NumericalStatic;
            }
            else
            {
                DefineCurrentTile(TileTypes.OperatorStatic, passedTile.value, passedTile.tileOperatorType);
                passedTile.meshRenderer.material = definedOperatorMat;
                currentTileType = TileTypes.OperatorStatic;
            }
        }
        
    }

    public void SetDefined(TG_Tile passedTile)
    {
        passedTile.tileState = TileStates.Defined;

        if (currentPath.Count > 0)
        {
            if (currentPath[currentPath.Count - 1] != passedTile)
                currentPath.Add(passedTile);            
        }
        else
        {
            currentPath.Add(passedTile);
        }

        if (lastPathTile)
        {
            if (lastPathTile != passedTile)
                lastPathTile = passedTile;
        }
        else
        {
            lastPathTile = passedTile;
        }

        passedTile.sprite.gameObject.SetActive(true);

        if (passedTile.tileType != TileTypes.Goal && passedTile.tileType != TileTypes.GoalNeighbour)
        {
            if (passedTile.tileType == TileTypes.Numerical || passedTile.tileType == TileTypes.NumericalStatic)
                passedTile.meshRenderer.material = definedNumericalMat;
            else if (passedTile.tileType == TileTypes.Operator || passedTile.tileType == TileTypes.OperatorStatic)
                passedTile.meshRenderer.material = definedOperatorMat;           
        }
    }

    public void SetLocked(TG_Tile passedTile, int value, TileTypes tileType, OperatorTypes operatorType)
    {
        passedTile.tileState = TileStates.Locked;
        passedTile.tileOperatorType = operatorType;
        passedTile.tileCollider.enabled = false;

        if (tileType != TileTypes.Goal)
            passedTile.meshRenderer.material = lockedMat;
        else
        {
            AssignGoalMat(passedTile);
        } 
        
        passedTile.value = value;
        passedTile.tileType = tileType;

        if (tileType == TileTypes.Numerical || tileType == TileTypes.NumericalStatic)
            passedTile.textMesh.text = value.ToString();
        else if(tileType == TileTypes.Operator || tileType == TileTypes.OperatorStatic)
        {
            if (operatorType == OperatorTypes.Add)
                passedTile.textMesh.text = "+";
            else if (operatorType == OperatorTypes.Subtract)
                passedTile.textMesh.text = "-";
            else if (operatorType == OperatorTypes.Multiply)
                passedTile.textMesh.text = "•";
            else if (operatorType == OperatorTypes.Divide)
                passedTile.textMesh.text = ":";
        }

        passedTile.sprite.gameObject.SetActive(false);
    }

    public void SetUnavailable(TG_Tile passedTile)
    {
        passedTile.tileState = TileStates.Unavailable;
        passedTile.tileType = TileTypes.None;
        passedTile.value = 0;
        passedTile.textMesh.text = "";
        passedTile.tileCollider.enabled = false;

        passedTile.meshRenderer.material = defaultMat;

        passedTile.sprite.gameObject.SetActive(false);
    }

    //SET TYPES: ----------------------------------------------------------------------------------------------------

    void SetGoal(TG_Tile passedTile, int value)
    {
        passedTile.tileState = TileStates.Locked;
        passedTile.tileType = TileTypes.Goal;
        AssignGoalMat(passedTile);
        //passedTile.meshRenderer.material = goalMat;
        passedTile.value = value;
        passedTile.tileCollider.enabled = false;
        passedTile.textMesh.gameObject.SetActive(true);
        passedTile.textMesh.text = value.ToString();
    }

    void SetGoalNeighbour(TG_Tile passedTile)
    {
        passedTile.tileState = TileStates.Locked;
        passedTile.tileType = TileTypes.GoalNeighbour;
        passedTile.meshRenderer.material = lockedMat;
        passedTile.value = 0;
        passedTile.tileCollider.enabled = false;
        passedTile.textMesh.gameObject.SetActive(true);
        passedTile.textMesh.text = "=";
    }

    void SetStart(TG_Tile passedTile, int value, bool startIsNumerical)
    {
        passedTile.tileState = TileStates.Available;
        passedTile.meshRenderer.material = startMat;
        passedTile.startTile = true;

        if (startIsNumerical)
            passedTile.tileType = TileTypes.Numerical;
        else
            passedTile.tileType = TileTypes.None;

        passedTile.value = value;
        passedTile.tileCollider.enabled = true;

        if(passedTile.tileType == TileTypes.Numerical)
            passedTile.textMesh.text = value.ToString();
    }

    public void SetNone(TG_Tile passedTile)
    {
        passedTile.tileType = TileTypes.None;
        passedTile.value = 0;
        passedTile.textMesh.text = "";
    }

    public void AssignGoalMat(TG_Tile passedTile)
    {
        var index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(passedTile));
        if (currentGoalMaterials.Count - 1 >= index)
            passedTile.meshRenderer.material = currentGoalMaterials[index];
        else
            passedTile.meshRenderer.material = goalMat;
    }

    public void AssignActiveGoalMat(TG_Tile passedTile)
    {
        var index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(passedTile));
        if (currentActiveGoalMats.Count - 1 >= index)
            passedTile.meshRenderer.material = currentActiveGoalMats[index];
        else
            passedTile.meshRenderer.material = goalActiveMat;
    }
    //----------------------------------------------CALCULATION----------------------------------------------------------

    void Calculation(TG_Tile passedTile)
    {        
        if (currentGameSet.gameStyle == TileGameStyles.Path || currentGameSet.gameStyle == TileGameStyles.TrailBlaze)
        {
            if (lastTile && lastPathTile && currentTile)
            {
                if (currentTile != lastPathTile || currentTile.tileType == TileTypes.OperatorStatic)
                {
                    print("CALCULATING...");

                    if (currentTile != lastPathTile)
                    {
                        if ((lastTile.tileType == TileTypes.Numerical || lastTile.tileType == TileTypes.NumericalStatic) && currentPath.Count >= 3)
                        {

                            currentValue = OperateValues(currentPath[currentPath.Count - 3], currentPath[currentPath.Count - 2], currentPath[currentPath.Count - 1]);

                            if (currentGameSet.gameStyle == TileGameStyles.Path)
                            {}
                            else
                            {}

                            if (calculateSound)
                                calculateSound.Play();

                            FinishCalculationText(currentValue);
                            StartCoroutine(UpdateMoveCounter());
                            hasCalculated = true;
                        }
                    }
                    if (currentTile.tileType == TileTypes.OperatorStatic)
                    {
                        if ((lastTile.tileType == TileTypes.Numerical || lastTile.tileType == TileTypes.NumericalStatic) && currentPath.Count >= 3)
                        {
                            bool check = true;
                            
                            if (currentPath.Count <= 3) //Checks if current path is inital path and skips a calc if startTile is Type of None
                            {
                                foreach (var item in currentPath)
                                {
                                    if (currentGameSet.startTiles.Contains(tiles.IndexOf(item)))
                                    {
                                        if (item.tileType == TileTypes.None)
                                            check = false;
                                    }
                                }
                            }
                            
                            if (check)
                            {
                                if ((lastTile.tileType == TileTypes.Numerical || lastTile.tileType == TileTypes.NumericalStatic) && currentPath.Count < 4)
                                    currentValue = OperateValues(currentPath[currentPath.Count - 3], currentPath[currentPath.Count - 2], currentPath[currentPath.Count - 1]);
                                else if ((lastTile.tileType == TileTypes.Numerical || lastTile.tileType == TileTypes.NumericalStatic) && currentPath.Count >= 4)
                                    currentValue = OperateValues(currentPath[currentPath.Count - 4], currentPath[currentPath.Count - 3], currentPath[currentPath.Count - 2]);
                                
                                if (currentGameSet.gameStyle == TileGameStyles.Path)
                                {}
                                else
                                {}

                                if (calculateSound)
                                    calculateSound.Play();

                                FinishCalculationText(currentValue);
                                StartCoroutine(UpdateMoveCounter());
                                hasCalculated = true;
                            }
                            else
                            {

                            }

                        }
                    }
                }
            }
        }
        else
        {
            bool check = false;

            for (int i = 0; i < currentGameSet.goalTiles.Count; i++)
            {
                if (tiles[currentGameSet.goalTiles[i]].connectedTiles.Contains(passedTile))
                {
                    check = true;
                }
            }

            if (check)
            {
                print("CALCULATING ALL...");

                List<TG_Tile> nums = new List<TG_Tile>();
                List<TG_Tile> ops = new List<TG_Tile>();

                for (int e = 0; e < currentPath.Count; e++)
                {
                    if (currentPath[e].tileType == TileTypes.Numerical || currentPath[e].tileType == TileTypes.NumericalStatic)
                    {
                        nums.Add(currentPath[e]);
                    }
                    if (currentPath[e].tileType == TileTypes.Operator || currentPath[e].tileType == TileTypes.OperatorStatic)
                    {
                        ops.Add(currentPath[e]);
                    }
                }

                if (calculateSound)
                    calculateSound.Play();

                currentValue = OperateAllValues(nums, ops);
                FinishCalculationText(currentValue);
                StartCoroutine(UpdateMoveCounter());
                hasCalculated = true;
            }
        }
    }

    int OperateValues(TG_Tile firstNumTile, TG_Tile operatorTile, TG_Tile lastNumTile)
    {
        switch(operatorTile.tileOperatorType)
        {
            case (OperatorTypes.Add):
                return (firstNumTile.value + lastNumTile.value);
            case (OperatorTypes.Subtract):
                return (firstNumTile.value - lastNumTile.value);
            case (OperatorTypes.Multiply):
                return (firstNumTile.value * lastNumTile.value);
            case (OperatorTypes.Divide):
                return Mathf.RoundToInt(firstNumTile.value / lastNumTile.value);
            default:
                return 0;
        }
    }

    int OperateAllValues(List<TG_Tile> numTiles, List<TG_Tile> operatorTiles)
    {
        int curVal = 0;

        for (int i = 0; i < operatorTiles.Count; i++)
        {
            int sum = 0;

            if(i == 0)
            {
                curVal = numTiles[0].value;               
            }

            if (numTiles.Count >= i + 1)
            {
                switch (operatorTiles[i].tileOperatorType)
                {
                    case (OperatorTypes.Add):
                        sum += numTiles[i + 1].value;
                        break;
                    case (OperatorTypes.Subtract):
                        sum -= numTiles[i + 1].value;
                        break;
                    case (OperatorTypes.Multiply):
                        sum = curVal * numTiles[i + 1].value;
                        break;
                    case (OperatorTypes.Divide):
                        sum = curVal / numTiles[i + 1].value;
                        break;
                    default:
                        break;
                }
            }

            curVal += sum;
        }

        return curVal;
    }
    //----------------------------Set New Tiles After Calculation
    void SetNewTiles() 
    {
        print("SETTING NEW TILES.....");

        Vector3 effectPosition = Vector3.zero;

        if (currentTile && lastPathTile)
        {
            if (currentTile != lastPathTile || currentTile.tileType == TileTypes.OperatorStatic)
            {
                switch (currentGameSet.gameStyle)
                {
                    case (TileGameStyles.TrailBlaze):

                        if(currentTile != lastPathTile)
                        {
                            if (lastPathTile.tileType == TileTypes.Numerical || lastPathTile.tileType == TileTypes.NumericalStatic)
                            { 
                                foreach (var item in currentPath)
                                {
                                    if (item != lastPathTile)
                                    {
                                        if (item.tileType != TileTypes.NumericalStatic && item.tileType != TileTypes.OperatorStatic)
                                            SetUnavailable(item);
                                        if(item.tileType == TileTypes.NumericalStatic || item.tileType == TileTypes.OperatorStatic)
                                            SetLocked(item, item.value, item.tileType, item.tileOperatorType);
                                        if (lastPathTile.connectedTiles.Contains(item))
                                        {
                                            if (item.tileType != TileTypes.NumericalStatic && item.tileType != TileTypes.OperatorStatic)
                                                CheckAvailable(item);
                                            if (item.tileType == TileTypes.NumericalStatic || item.tileType == TileTypes.OperatorStatic)
                                            {
                                                if(item.tileType == TileTypes.NumericalStatic && (lastPathTile.tileType == TileTypes.Operator || lastPathTile.tileType == TileTypes.OperatorStatic))
                                                    CheckAvailable(item);
                                                if (item.tileType == TileTypes.OperatorStatic && (lastPathTile.tileType == TileTypes.Numerical || lastPathTile.tileType == TileTypes.NumericalStatic))
                                                    CheckAvailable(item);
                                            }
                                        }
                                    }
                                }

                                currentPath.Clear();

                                lastPathTile.value = currentValue;
                                lastPathTile.textMesh.text = currentValue.ToString();
                                currentPath.Add(lastPathTile);
                                effectPosition = lastPathTile.transform.position;
                            }                            
                        }
                        else if (currentTile.tileType == TileTypes.OperatorStatic)
                        {
                            TG_Tile nextLastPathTile = null;
                            foreach (var item in currentPath)
                            {
                                if (item != lastPathTile && item != currentPath[currentPath.Count - 2]) // Excludes Numerical before operator from alteration
                                {
                                    if (item.tileType != TileTypes.NumericalStatic && item.tileType != TileTypes.OperatorStatic)
                                        SetUnavailable(item);
                                    if (item.tileType == TileTypes.NumericalStatic || item.tileType == TileTypes.OperatorStatic)
                                        SetLocked(item, item.value, item.tileType, item.tileOperatorType);
                                    if (lastPathTile.connectedTiles.Contains(item))
                                    {
                                        if (item.tileType != TileTypes.NumericalStatic && item.tileType != TileTypes.OperatorStatic)
                                            CheckAvailable(item);
                                        if (item.tileType == TileTypes.NumericalStatic || item.tileType == TileTypes.OperatorStatic)
                                        {
                                            if (item.tileType == TileTypes.NumericalStatic && (lastPathTile.tileType == TileTypes.Operator || lastPathTile.tileType == TileTypes.OperatorStatic))
                                                CheckAvailable(item);
                                            if (item.tileType == TileTypes.OperatorStatic && (lastPathTile.tileType == TileTypes.Numerical || lastPathTile.tileType == TileTypes.NumericalStatic))
                                                CheckAvailable(item);
                                        }
                                    }
                                }
                                if(item == currentPath[currentPath.Count - 2])
                                {
                                    nextLastPathTile = item;
                                }
                            }

                            currentPath.Clear();

                            nextLastPathTile.value = currentValue;
                            nextLastPathTile.textMesh.text = currentValue.ToString();
                            currentPath.Add(nextLastPathTile);
                            currentPath.Add(lastPathTile);
                            effectPosition = nextLastPathTile.transform.position;
                        }

                        break;

                    case (TileGameStyles.Path):
                        if (currentTile != lastPathTile)
                        {
                            if (lastPathTile.tileType == TileTypes.Numerical || lastPathTile.tileType == TileTypes.NumericalStatic)

                            {
                                foreach (var item in currentPath)
                                {
                                    if (item != lastPathTile)
                                    {
                                        if (currentGameSet.startTiles.Contains(tiles.IndexOf(item)))
                                        {
                                            item.sprite.gameObject.SetActive(true);
                                        }

                                        item.textMesh.text = "";
                                        item.meshRenderer.material = lockedMat;
                                    }
                                }

                                lastPathTile.value = currentValue;
                                lastPathTile.textMesh.text = currentValue.ToString();
                                effectPosition = lastPathTile.transform.position;
                            }
                        }
                        else if (currentTile.tileType == TileTypes.OperatorStatic)
                        {
                            TG_Tile nextLastPathTile = null;

                            foreach (var item in currentPath)
                            {
                                if (item != lastPathTile && item != currentPath[currentPath.Count - 2]) // Excludes Numerical before operator from alteration
                                {
                                    if (currentGameSet.startTiles.Contains(tiles.IndexOf(item)))
                                    {
                                        item.sprite.gameObject.SetActive(true);
                                    }

                                    item.textMesh.text = "";
                                    item.meshRenderer.material = lockedMat;
                                }
                                if (item == currentPath[currentPath.Count - 2])
                                {
                                    nextLastPathTile = item;
                                }
                            }

                            nextLastPathTile.value = currentValue;
                            nextLastPathTile.textMesh.text = currentValue.ToString();
                            effectPosition = nextLastPathTile.transform.position;
                        }

                        break;

                    case (TileGameStyles.PathHard):

                        break;

                    default:

                        break;

                }
            }

            if (operateEffect)
            {
                print("EffectPosition = " + effectPosition.ToString());

                Instantiate(operateEffect, effectPosition, worldTransform.rotation, gameTransform);
            }
        }
    }
    //-----------------------------------------------KEYS----------------------------------------------------------------

    void CheckKey(TG_Tile passedTile)
    {
        if (passedTile.tileType == TileTypes.Numerical || passedTile.tileType == TileTypes.NumericalStatic)
        {
            if (passedTile.key)
            {
                if (passedTile.key.gameObject.activeSelf)
                {
                    print("keyCheck");

                    if(passedTile.key.value == currentValue)
                    {
                        //Some Response....
                        if (unlockKeySound)
                            unlockKeySound.Play();

                        passedTile.key.gameObject.SetActive(false);

                        for (int i = 0; i < currentGoalKeyTiles.Count; i++) //-------------------------CHECKS IF THIS KEY IS LAST KEY OF SET AND UNLOCKS GOAL
                        {
                            if (currentGoalKeyTiles[i].keyTiles.Contains(tiles.IndexOf(passedTile)))
                            {
                                bool check = true;

                                foreach (var key in currentGoalKeyTiles[i].keyTiles)
                                {
                                    if (tiles[key].key.gameObject.activeSelf)
                                        check = false;
                                }

                                if (check)
                                {
                                    if (currentLocks[i])
                                        StartCoroutine(DisableLock(currentLocks[i]));
                                }
                            }                            
                        }
                    }
                }
            }
        }
    }
    //----------------------------------------------COMPLETION-----------------------------------------------------------
    void CheckAnswer(TG_Tile passedTile)
    {
        for (int i = 0; i < currentGameSet.goalTiles.Count; i++)
        {
            if (!tiles[currentGameSet.goalTiles[i]].goalFinished)
            {
                if (tiles[currentGameSet.goalTiles[i]].connectedTiles.Contains(passedTile))
                {
                    if (tiles[currentGameSet.goalTiles[i]].value == currentValue)
                    {
                        bool keyCheck = true;

                        if(currentGoalKeyTiles.Count - 1 >= i)
                        {
                            if (currentGoalKeyTiles[i].keyTiles.Count > 0)
                            {
                                for (int e = 0; e < currentGoalKeyTiles[i].keyTiles.Count; e++)
                                {
                                    if(tiles[currentGoalKeyTiles[i].keyTiles[e]].key.gameObject.activeSelf)
                                    {
                                        keyCheck = false;
                                        //Some Response;
                                    }
                                }                                
                            }
                        }

                        if (keyCheck)
                        {
                            currentGoalAvailable = tiles[currentGameSet.goalTiles[i]];
                            StartCoroutine(InstantiateEffects(tiles[currentGameSet.goalTiles[i]]));                            
                        }
                    }
                }
            }
        }
    }

    bool CheckCurrentGoal()
    {
        if (currentGoalAvailable)
        {
            if (currentGoalAvailable.value == currentValue)
                return true;
            else
                return false;
        }
        else
            return false;
    }

    void ReverseGoalCompletion()
    {
        if (lockGoalSound)
            lockGoalSound.PlayDelayed(0.5f);

        currentGoalAvailable.tileCollider.enabled = false;
        AssignGoalMat(currentGoalAvailable);
        Destroy(currentGoalEffect);
        StartCoroutine(ScaleGoal(currentGoalAvailable, false));
    }

    private IEnumerator InstantiateEffects(TG_Tile passedTile)
    {
        if (unlockGoalSound)
            unlockGoalSound.PlayDelayed(0.75f);

        yield return new WaitForSeconds(0.75f);

        int index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(passedTile));

        if (goalGlowPrefab)
        {
            if (currentGoalEffect)
                Destroy(currentGoalEffect);
            var glow = Instantiate(goalGlowPrefab, passedTile.transform.position, worldTransform.rotation, gameTransform);
            glow.transform.GetChild(0).GetComponent<MeshRenderer>().material = currentGoalEffectMats[index];
            currentGoalEffect = glow;
        }
        if (correctEffect)
        {
            var effect = Instantiate(correctEffect, passedTile.transform.position, worldTransform.rotation, gameTransform);
            var effectScript = effect.GetComponent<TG_ParticleEffect>();
            effectScript.effectColor = goalColors[index];
        }

        passedTile.tileCollider.enabled = true;
        AssignActiveGoalMat(passedTile);
        StartCoroutine(ScaleGoal(currentGoalAvailable, true));
    }

    private IEnumerator CheckGoal(TG_Tile passedGoalTile)
    {        
        yield return new WaitForSeconds(1f);

        var index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(passedGoalTile));

        currentGoalCounter++;

        if (reachGoalSound)
            reachGoalSound.Play();

        if (currentGoalCounter >= currentGameSet.goalTiles.Count && !passedGoalTile.goalFinished)
        {           
            print("GAMESET COMPLETE!");
            foreach (var item in currentTiles)
            {
                SetLocked(item, item.value, item.tileType, item.tileOperatorType);
            }
            StartCoroutine(CheckRewards());
            //CompleteGameSet();
        }
        else
        {
            print("GOAL COMPLETE!");
            GoalComplete(tiles[currentGameSet.goalTiles[index]], tiles[currentGameSet.goalTiles[index]].value, currentGameSet.goalsLock[index]);
        }

        yield return null;
    }

    void GoalComplete(TG_Tile goalTile, int goalValue, bool isGoalLocked)
    {
        //play some victory anim        
        currentTile = goalTile;
        lastTile = goalTile;
        currentPath.Clear();
        currentPath.Add(goalTile);
        goalTile.goalFinished = true;
        currentGoalAvailable = null;
        goalTile.tileType = TileTypes.Numerical;
        SetDefined(goalTile);
        Destroy(currentGoalEffect);
        currentGoalEffect = null;

        foreach (var item in tiles)
        {
            if(item.tileType != TileTypes.Goal)
                CheckUnavailable(item);
        }
        foreach (var item in goalTile.connectedTiles)
        {
            SetNone(item);
            SetAvailable(item);
        }

        if(isFinalBossGame)
        {
            if (OnBossGameReachGoal != null)
                OnBossGameReachGoal();

            if (OnBossGameReachSubGoal != null)
            {               
                OnBossGameReachSubGoal(goalValue);                
            }

            print("Reached Goal Boss");
        }        
    }

    private IEnumerator CheckRewards()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        buttonPanel.gameObject.SetActive(false);

        currentCalculation.transform.parent.gameObject.SetActive(false);
        var player = gameManager.player;
        player.navMeshAgent.updateRotation = false;

        Quaternion curRot = gameManager.player.transform.rotation;
        Quaternion targetRot = Quaternion.LookRotation(-gameTransform.forward, Vector3.up);
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 0.25f;
            player.transform.rotation = Quaternion.Slerp(curRot, targetRot, t);
            yield return new WaitForEndOfFrame();
        }

        gameManager.player.navMeshAgent.updateRotation = true;

        yield return new WaitForSeconds(0.25f);
        
        int achievement = 0;

        if(completedSets.Contains(currentGameSet))
        {
            achievement = completedSetsAchievements[completedSets.IndexOf(currentGameSet)];
        }

        int oldAchievement = achievement;
        int levelIndex = CheckLevelIndex(tileGameDifficulty);
        int currentAchievement = CheckCurrentAchievement();

        if(!isFinalBossGame)
            OpenAchievementPanel(achievementPanel, tileGameDifficulty, levelIndex, gameSets.Count, oldAchievement, currentAchievement);
        else
        {
            if (OnBossGameWaitForReward != null)
                OnBossGameWaitForReward();
        }

        yield return new WaitForSeconds(7);

        if (SetCompleteSound)
            SetCompleteSound.Play();

        gameManager.player.anim.SetTrigger("Success");

        if (currentMoveCounter <= currentGameSet.targetMoves.z)
        {                        
            if (achievement < 3)
            {
                achievement = 3;
            }
        }
        else if (currentMoveCounter <= currentGameSet.targetMoves.y)
        {
            if (achievement < 2)
            {
                achievement = 2;
            }
        }
        else if (currentMoveCounter <= currentGameSet.targetMoves.x)
        {
            if (achievement < 1)
            {
                achievement = 1;
            }            
        }

        var reward = (achievement - oldAchievement) * 3;

        if (reward <= 0)
            reward = 1;

        for (int i = 0; i < reward; i++)
        {
            Invoke("InstantiateGem", 0.2f * i);
        }
        
        yield return new WaitForSeconds((achievement - oldAchievement) + 1);

        CompleteGameSet(achievement);
    }

    private IEnumerator WinRoutine()
    {
        yield return new WaitForSeconds(1);

        foreach (var item in wakeUpOnComplete)
        {
            item.gameObject.SetActive(true);
        }

        foreach (var item in sleepOnComplete)
        {
            item.gameObject.SetActive(false);
        }
        
        trophyPanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        if (winSound1)
            winSound1.Play();
        trophyGraphic.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(2);
        gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
        trophyGraphic.gameObject.SetActive(false);
        trophyPanel.gameObject.SetActive(false);
        yield return new WaitForSeconds(3);
        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);

        gameManager.playerStats.trophies.Add(trophy);

        if (!isFinalBossGame)
        {
            gameManager.playerStats.hasWonTileGame = true;
        }
        else
        {
            gameManager.playerStats.hasWonBossTileGame = true;
        }

        hasWonGame = true;
        gameManager.playerStats.SaveGame();

        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();        
        setCounter = 0;

        ResetTileHeight();
        yield return new WaitForSeconds(3f);
    }

    private IEnumerator WinAgain()
    {
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
        setCounter = 0;
        ResetTileHeight();
        yield return new WaitForSeconds(3f);
    }

    void InstantiateGem()
    {
        if (gameManager.player)
            Instantiate(gemPhysicalPrefab, gameManager.player.transform.position + Vector3.up * 3f, Quaternion.identity, gameTransform);
    }

    void CompleteGameSet(int passedAchievement)
    {        

        if (setCounter < gameSets.Count - 1)
        {            
            setCounter++;

            if (!completedSets.Contains(currentGameSet))
            {   
                completedSets.Add(currentGameSet);
                completedSetsAchievements.Add(passedAchievement);                
            }

            SetCurrentCompletedSet(passedAchievement);
            /*
            if (!isFinalBossGame)
                gameManager.playerStats.TileGameSetCount = setCounter;
            else
                gameManager.playerStats.BossTileGameSetCount = setCounter;
            */
            InitializeGame();            
            StartNewGameSet(setCounter, tileGameDifficulty);
        }
        else
        {
            if (!completedSets.Contains(currentGameSet))
            {
                completedSets.Add(currentGameSet);
                completedSetsAchievements.Add(passedAchievement);                
            }

            SetCurrentCompletedSet(passedAchievement);

            CorrectTicker();

            FinishGame();
        }        
    }

    void FinishGame()
    {       
        if (!hasWonGame)
        {
            print("Won TileGame first time");
            StartCoroutine(WinRoutine());
        }
        else
        {
            StartCoroutine(WinAgain());
            //setCounter = 0;
            //InitializeGame();
            //StartNewGameSet(setCounter);
        }
    }
    //--------------------------------------------------MOVEOBJECTS------------------------------------------------------------------------

    public void SetPlayerDestination(Transform tile)
    {
        if(gameManager.player)
        {
            if(gameManager.player.isNavMeshAgent)
            {
                if (tile != playerStartPos)
                    gameManager.player.playerMovementController.playerMovementAgent.SetNewDestination(tile.position);
                else
                    gameManager.player.navMeshAgent.Warp(playerStartPos.position);
            }
        }
    }
       
    private void SetCurrentTileHeight()
    {
        if (currentTile)
            currentTile.transform.localPosition = new Vector3(currentTile.transform.localPosition.x, currentTile.transform.localPosition.y + selectedYdist, currentTile.transform.localPosition.z);
    }

    public void ResetTileHeight()
    {
        for (int i = 0; i < tiles.Count; i++)
        {
            bool sinkCheck = true;

            if (currentSinkerTiles.Count > 0)
            {
                for (int e = 0; e < currentSinkerTiles.Count; e++)
                {
                    if (tiles.IndexOf(currentSinkerTiles[e]) == i)
                    {
                        if (currentSinkTiles[e])
                            sinkCheck = false;
                    }
                }
            }

            if(sinkCheck)
                tiles[i].transform.localPosition = new Vector3(tiles[i].transform.localPosition.x, tilesStartPosY[i], tiles[i].transform.localPosition.z);
        } 
        //if(currentTile)
            //currentTile.transform.localPosition = new Vector3(currentTile.transform.localPosition.x, tilesStartPosY[tiles.IndexOf(currentTile)], currentTile.transform.localPosition.z);

    }
    
    //---------------------------------------------------INITIALIZING----------------------------------------------------------------------

    public void ResetCurrentGameSet()
    {       
        buttonPanel.gameObject.SetActive(false);

        if (restartButtonClickSound)
            restartButtonClickSound.Play();

        WrongTicker();
       
        InitializeGame();        
        StartNewGameSet(setCounter, tileGameDifficulty);
    }
    
    public void InitializeGame()
    {
        if (isFinalBossGame)
        {
            if (OnBossGameRestart != null)
                OnBossGameRestart();
        }

        StopAllCoroutines();
        gameManager.player.navMeshAgent.baseOffset = 0;

        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].goalFinished = false;
            if (tiles[i].key)
                Destroy(tiles[i].key.gameObject);
            tiles[i].key = null;
            tiles[i].tileType = TileTypes.None;
            tiles[i].tileState = TileStates.Unavailable;
            tiles[i].value = 0;
            tiles[i].textMesh.text = " ";
            tiles[i].meshRenderer.material = defaultMat;
            tiles[i].tileCollider.enabled = false;
            tiles[i].sprite.color = lineColor;
            tiles[i].sprite.gameObject.SetActive(false);
            tiles[i].transform.localScale = Vector3.one;
            tiles[i].transform.localPosition = new Vector3(tiles[i].transform.localPosition.x, tilesStartPosY[i], tiles[i].transform.localPosition.z);
        }

        GoalButton.transform.parent.gameObject.SetActive(false);
        undoButton.gameObject.SetActive(true);
        ResetButton.gameObject.SetActive(true);
        currentTiles.Clear();
        currentPath.Clear();
        currentMoveCounter = 0;
        MoveCounterText.text = currentMoveCounter.ToString();
        currentValue = 0;
        hasCalculated = false;
        currentTile = null;
        lastTile = null;
        lastPathTile = null;
        currentGoalAvailable = null;
        if (currentGoalEffect)
            Destroy(currentGoalEffect);
        currentGoalEffect = null;
        currentGoalCounter = 0;
        hasMovedFromStart = false;
        hasAddedStartToPath = false;
        currentTileType = TileTypes.None;
        currentlyAvailable.Clear();
        currentPath.Clear();
        currentNumbers.Clear();
        currentOperators.Clear();
        currentCalculation.text = "";
        lastOperatorTile = null;
        lastValueTile = null;
        maxMoveCountGroup.gameObject.SetActive(false);
        setCountPanel.transform.localScale = Vector3.zero;

        currentSinkerTiles.Clear();
        currentSinkTilesTimes.Clear();
        currentSinkTiles.Clear();

        foreach (var item in currentCrackedPrefabs)
        {
            Destroy(item.gameObject);
        }
        currentCrackedPrefabs.Clear();

        foreach (var item in currentEnemies)
        {
            Destroy(item.gameObject);
        }
        currentEnemies.Clear();

        foreach (var item in currentGoalMaterials)
        {
            Destroy(item);
        }
        currentGoalMaterials.Clear();

        foreach (var item in currentActiveGoalMats)
        {
            Destroy(item);
        }
        currentActiveGoalMats.Clear();

        foreach (var item in currentGoalEffectMats)
        {
            if (item)
                Destroy(item);
        }
        currentGoalEffectMats.Clear();
        
        foreach (var item in currentKeyMaterials)
        {
            Destroy(item);
        }
        currentKeyMaterials.Clear();

        foreach (var item in currentLocks)
        {
            Destroy(item.gameObject);
        }
        currentLocks.Clear();
        
        currentGoalKeyTiles.Clear();

        foreach (var item in currentEffects)
        {
            if (item)
                Destroy(item.gameObject);
        }
        currentEffects.Clear();

        if (OnMoveCountChange != null)
        {
            OnMoveCountChange(currentMoveCounter, 0);
        }

        history.Clear();
        Resources.UnloadUnusedAssets();
        //ResetTileHeight();
    }

//---------------------------------StartNewSet-------------------------------------------------
   
    public void StartNewGameSet(int counter, PlayerStats.Difficulty difficulty)
    {
        SetGameSetWithDifficulty(counter);

        if (!isFinalBossGame)
            gameManager.playerStats.TileGameSetCount = setCounter;
        else
            gameManager.playerStats.BossTileGameSetCount = setCounter;

        SetPlayerDestination(playerStartPos);
        numericalButtonObject.gameObject.SetActive(false);
        operatorButtonObject.gameObject.SetActive(false);
        currentGamestyle = currentGameSet.gameStyle;

        if(currentGamestyle != TileGameStyles.TrailBlaze)
        {
            if(!currentGameSet.forbiddenOperators.Contains(OperatorTypes.Multiply))
                currentGameSet.forbiddenOperators.Add(OperatorTypes.Multiply);
            if (!currentGameSet.forbiddenOperators.Contains(OperatorTypes.Divide))
                currentGameSet.forbiddenOperators.Add(OperatorTypes.Divide);
        }

        for (int i = 0; i < currentGameSet.setTiles.Count; i++)
        {
            for (int e = currentGameSet.setTiles[i].x; e <= currentGameSet.setTiles[i].y; e++)
            {
                currentTiles.Add(tiles[e]);
            }
        }

        for (int i = 0; i < tiles.Count; i++)
        {
            if (currentTiles.Contains(tiles[i]))
                tiles[i].gameObject.SetActive(true);
            else
                tiles[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < currentGameSet.startTiles.Count; i++)
        {
            var startTile = tiles[currentGameSet.startTiles[i]];

            currentlyAvailable.Add(startTile);

            SetStart(startTile, currentGameSet.startValues[i], currentGameSet.startIsNumerical[i]);
            startTile.startTile = true;
        }

        for (int i = 0; i < currentGameSet.goalTiles.Count; i++)
        {
            var goal = tiles[currentGameSet.goalTiles[i]];

            SetGoal(goal, currentGameSet.goalValues[i]);

            foreach (var item in goal.connectedTiles)
            {
                SetGoalNeighbour(item);
            }

            var newGoalMat = Instantiate(goalMat);
            currentGoalMaterials.Add(newGoalMat);
            var newActiveGoalMat = Instantiate(goalActiveMat);
            currentActiveGoalMats.Add(newActiveGoalMat);
            var newGoalEffectMat = Instantiate(goalEffectMat);
            currentGoalEffectMats.Add(newGoalEffectMat);
            var newKeyMat = Instantiate(keyMat);           
            currentKeyMaterials.Add(newKeyMat);

            if(goalColors.Count - 1 >= i)
            {
                newGoalMat.SetColor("_Color0", goalColors[i]);
                newActiveGoalMat.SetColor("_Color", goalColors[i]);
                newKeyMat.SetColor("_TintColor", goalColors[i]);
                newGoalEffectMat.SetColor("_TintColor", new Vector4(goalColors[i].r, goalColors[i].g, goalColors[i].b, 0.1f));
            }
            else
            {
                newGoalMat.SetColor("_Color0", Color.magenta);
                newActiveGoalMat.SetColor("_Color", Color.magenta);
                newKeyMat.SetColor("_TintColor", Color.magenta);
                newGoalEffectMat.SetColor("_TintColor", Color.magenta);
            }

            goal.meshRenderer.material = newGoalMat;

            if (currentGameSet.goalKeyTiles.Count > 0)
            {
                //TEST
                if (currentGameSet.goalKeyTiles[i].keyTiles.Count > 0)
                {
                    var newLock = Instantiate(lockPrefab, goal.transform.position, goal.transform.rotation, gameTransform);
                    var lockRenderer = newLock.transform.GetChild(0).GetComponent<MeshRenderer>();
                    lockRenderer.material = newGoalMat;
                    currentLocks.Add(newLock.transform);

                    StartCoroutine(SetLock(newLock.transform, goal.transform, newGoalMat));
                }
                //TEST END  

                for (int e = 0; e < currentGameSet.goalKeyTiles[i].keyTiles.Count; e++)
                {
                    //bool lockCreated = false;

                    if (currentGameSet.goalKeyTiles[i].keyTiles.Count > 0)
                    {
                        //TEST
                        /*
                        if (!lockCreated)
                        {
                            var newLock = Instantiate(lockPrefab, goal.transform.position, goal.transform.rotation, gameTransform);
                            var lockRenderer = newLock.transform.GetChild(0).GetComponent<MeshRenderer>();
                            lockRenderer.material = newGoalMat;
                            currentLocks.Add(newLock.transform);

                            StartCoroutine(SetLock(newLock.transform, goal.transform, newGoalMat));
                            lockCreated = true;
                        }
                        */
                        //TEST END
                        var curKeyTile = tiles[currentGameSet.goalKeyTiles[i].keyTiles[e]];
                        var newKey = Instantiate(keyPrefab, curKeyTile.transform.position, curKeyTile.transform.rotation, curKeyTile.transform);
                        newKey.gameObject.SetActive(true);
                        var newKeyScript = newKey.GetComponent<TG_Key>();
                        curKeyTile.key = newKeyScript;
                        newKeyScript.goal = goal;                        

                        newKeyScript.tile = tiles[currentGameSet.goalKeyTiles[i].keyTiles[e]];
                        newKeyScript.value = currentGameSet.goalKeyTiles[i].keyTilesValues[e];

                        if (currentGameSet.showKeyTiles)
                        {
                            newKeyScript.keyText.text = currentGameSet.goalKeyTiles[i].keyTilesValues[e].ToString();
                            newKeyScript.meshRenderer.material = newKeyMat;

                            if (goalColors.Count - 1 >= i)
                            {
                                newKeyScript.keyText.color = goalColors[i];
                                newKeyScript.spriteRenderer.color = goalColors[i];
                                newKeyScript.keyTextBg.color = HelperFunctions.ModifyColor(goalColors[i], 1, 0.25f, 1);
                            }
                            else
                            {
                                newKeyScript.keyText.color = Color.magenta;
                                newKeyScript.spriteRenderer.color = Color.magenta;
                                newKeyScript.keyTextBg.color = HelperFunctions.ModifyColor(Color.magenta, 1, 0.25f, 1);
                            }
                        }
                        else
                        {
                            newKeyScript.keyText.gameObject.SetActive(false);
                            newKeyScript.spriteRenderer.gameObject.SetActive(false);
                            newKeyScript.keyTextBg.gameObject.SetActive(false);

                            foreach (var sprite in newKeyScript.spriteRenderersbg)
                            {
                                sprite.gameObject.SetActive(false);
                            }
                        }
                    }
                }

                currentGoalKeyTiles.Add(currentGameSet.goalKeyTiles[i]);
            }            
        }

        for (int i = 0; i < currentGameSet.lockedTiles.Count; i++)
        {
            var lockedTile = tiles[currentGameSet.lockedTiles[i]];

            SetLocked(lockedTile, currentGameSet.lockedTValues[i], currentGameSet.lockedTTypes[i], currentGameSet.lockedOperatorTypes[i]);

            if (lockedTile.tileType == TileTypes.NumericalStatic)
                lockedTile.textMesh.text = lockedTile.value.ToString();
            if (lockedTile.tileType == TileTypes.OperatorStatic)
            {
                if(lockedTile.tileOperatorType == OperatorTypes.Add)
                    lockedTile.textMesh.text = "+";
                if (lockedTile.tileOperatorType == OperatorTypes.Subtract)
                    lockedTile.textMesh.text = "-";
                if (lockedTile.tileOperatorType == OperatorTypes.Multiply)
                    lockedTile.textMesh.text = "•";
                if (lockedTile.tileOperatorType == OperatorTypes.Divide)
                    lockedTile.textMesh.text = ":";
            }
        }

        for (int i = 0; i < 10; i++) //NumericalSlider
        {
            if (!currentGameSet.forbiddenNumbers.Contains(i))
                currentNumbers.Add(i);
        }

        //OperatorSlider
        {
            if (!currentGameSet.forbiddenOperators.Contains(OperatorTypes.Add))
                currentOperators.Add(0);
            if (!currentGameSet.forbiddenOperators.Contains(OperatorTypes.Subtract))
                currentOperators.Add(1);
            if (!currentGameSet.forbiddenOperators.Contains(OperatorTypes.Multiply))
                currentOperators.Add(2);
            if (!currentGameSet.forbiddenOperators.Contains(OperatorTypes.Divide))
                currentOperators.Add(3);
        }

        for (int i = 0; i < currentGameSet.enemies.Count; i++)
        {
            var enemyStartTile = tiles[currentGameSet.enemies[i]];
            var enemy = Instantiate(enemyPrefab, enemyStartTile.transform.position, gameTransform.rotation * Quaternion.Euler(0, 180, 0), gameTransform);
            var enemyScript = enemy.GetComponent<TileGameEnemy>();
            enemyScript.tileGameScript = this;
            enemyScript.enemyIndex = i;
            enemyScript.startTile = enemyStartTile;
            enemyScript.enemyCurrentTile = enemyStartTile;

            currentEnemies.Add(enemyScript);
        }

        if(currentGameSet.maximumMoves > 0)
        {
            maxMoveCountGroup.gameObject.SetActive(true);
            maxMoveCountImage.color = Color.gray;
            MaxMoveCountText.text = currentGameSet.maximumMoves.ToString();
        }

        if(currentGameSet.sinkTiles.Count > 0)
        {
            for (int i = 0; i < currentGameSet.sinkTiles.Count; i++)
            {
                currentSinkerTiles.Add(tiles[currentGameSet.sinkTiles[i]]);
                currentSinkTilesTimes.Add(currentGameSet.sinkTilesTime[i]);
                currentSinkTiles.Add(false);
                var tileTransform = tiles[currentGameSet.sinkTiles[i]].transform;
                var crackedObject = Instantiate(crackedPrefab, tileTransform.position, tileTransform.rotation, tiles[currentGameSet.sinkTiles[i]].transform);
                currentCrackedPrefabs.Add(crackedObject.transform);
            }
        }

        foreach (var item in tiles)
        {
            if (!currentlyAvailable.Contains(item))
            {
                if (!currentGameSet.lockedTiles.Contains(tiles.IndexOf(item)))
                {
                    if (!currentGameSet.goalTiles.Contains(tiles.IndexOf(item)))
                    {
                        bool check = true;

                        foreach (var goalTile in currentGameSet.goalTiles)
                        {
                            if (tiles[goalTile].connectedTiles.Contains(item))
                                check = false;
                        }

                        if (check)
                        {
                            SetUnavailable(item);
                        }
                    }
                }
            }
        }

        for (int i = 0; i < currentGameSet.goalTiles.Count; i++)
        {
            StartCoroutine(ScaleGoal(tiles[currentGameSet.goalTiles[i]], false));
        }

        if(currentGameSet == gameSets[0] && !isFinalBossGame)
        {
            undoButton.gameObject.SetActive(false);
            ResetButton.gameObject.SetActive(false);
        }

        if(!currentGameSet.useHistory)
            undoButton.gameObject.SetActive(false);

        buttonPanel.gameObject.SetActive(true);

        currentCalculation.transform.parent.gameObject.SetActive(true);

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);

        StartCoroutine(ShowSetCountPanel(setCounter));

        if(isFinalBossGame)
        {
            if (OnBossGameInitialized != null)
                OnBossGameInitialized();
        }

        gameManager.playerStats.SaveGame();
    }

    private IEnumerator SetLock(Transform lockObject, Transform connectedGoal, Material connectedGoalMaterial)
    {        
        float t = 0;
        var startPos = connectedGoal.position - Vector3.up;
        var endPos = connectedGoal.position;

        lockObject.gameObject.SetActive(true);
        MoveStoneSound.Play();

        while (t < 1)
        {
            t += Time.deltaTime / 2f;
            lockObject.transform.position = Vector3.Lerp(startPos, endPos, t);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator DisableLock(Transform lockObject)
    {
        float t = 0;
        var startPos = lockObject.transform.position;
        var endPos = startPos - Vector3.up;

        yield return new WaitForSeconds(1);
        MoveStoneSound.Play();

        while (t < 1)
        {
            t += Time.deltaTime / 2f;
            lockObject.transform.position = Vector3.Lerp(startPos, endPos, t);
            yield return new WaitForEndOfFrame();
        }

        lockObject.gameObject.SetActive(false);
    }

    private IEnumerator ScaleGoal(TG_Tile goalTile, bool setAvailable)
    {
        float t = 0;
        float maxHeight = 5f;

        MoveStoneQuicklySound.Play();

        while (t < 1)
        {
            t += Time.deltaTime / 0.5f;
            if (!setAvailable)
                goalTile.transform.localScale = Vector3.Lerp(Vector3.one, new Vector3(1f, maxHeight, 1f), t);
            else
                goalTile.transform.localScale = Vector3.Lerp(new Vector3(1f, maxHeight, 1f), Vector3.one, t);

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator SinkTile(TG_Tile tileToSink, bool sink)
    {
        GameObject crackedInstance = null;
        Vector3 startPos = tileToSink.transform.localPosition;
        Vector3 endPos = Vector3.one;

        if (sink)
            crackedInstance = Instantiate(crackedTileAnim, tileToSink.transform.position, tileToSink.transform.rotation, gameTransform);

        if (sink)
            endPos = new Vector3(tileToSink.transform.localPosition.x, tileToSink.transform.localPosition.y - 2f, tileToSink.transform.localPosition.z);
        else
            endPos = new Vector3(tileToSink.transform.localPosition.x, tilesStartPosY[tiles.IndexOf(tileToSink)], tileToSink.transform.localPosition.z);

        tileToSink.transform.localPosition = endPos;

        

        LiquidSound.Play();

        yield return null;
        //while(t < 1)
        //{
        //t += Time.deltaTime / 2f;
        //tileToSink.transform.localPosition = Vector3.Lerp(startPos, endPos, t);
        //yield return new WaitForEndOfFrame();
        //}
    }

    private IEnumerator ShowSetCountPanel(int setCount)
    {
        if (tileGameDifficulty == PlayerStats.Difficulty.Easy)
            setCountImage.color = gameManager.colorEasy;
        else if (tileGameDifficulty == PlayerStats.Difficulty.Normal)
            setCountImage.color = gameManager.colorNormal;
        else if (tileGameDifficulty == PlayerStats.Difficulty.Hard)
            setCountImage.color = gameManager.colorHard;
        else
            setCountImage.color = Color.grey;

        setCountPanelText.text = (setCount + 1).ToString() + "/" + gameSets.Count.ToString();

        if (setCountPanelText.text.Length < 4)
            setCountPanelText.fontSize = 220;
        else if (setCountPanelText.text.Length == 4)
            setCountPanelText.fontSize = 200;
        else if (setCountPanelText.text.Length > 4)
            setCountPanelText.fontSize = 160;

        setCountPanel.gameObject.SetActive(true);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        setCountPanel.gameObject.SetActive(false);
    }
    //---------------------------------------------------CLICKCHECKS----------------------------------------------------------------------

    private void CancelSelection() //Not currently in use
    {
        print("cancel");

        foreach (var item in tiles)
        {
            if (item.tileState == TileStates.Available ||
                item.tileState == TileStates.Selected)
            {
                if (currentGameSet.startTiles.Contains(tiles.IndexOf(item)) &&
                   (item.tileState == TileStates.Selected || item.tileState == TileStates.Available))
                {
                    item.meshRenderer.material = startMat;
                }
                else if (!currentGameSet.startTiles.Contains(tiles.IndexOf(item)) &&
                       item.tileState != TileStates.Selected && item.tileState != TileStates.Available)
                {
                    item.meshRenderer.material = defaultMat;
                }
            }
        }

        ResetTileHeight();
        currentTile = null;
    }

    void CheckMousePress()
    {        
        if (!IsMouseOverUIelements())
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.parent != gameTransform)
                {
                    //CancelSelection();                
                }
            }  
            else
            {
                //CancelSelection();
            }
        }
    }

    public bool IsMouseOverUIelements()
    {
        var mousePosition = Input.mousePosition;
        int testInt = 0;

        for (int i = 0; i < tileGameCanvas.uiRects.Count; i++)
        {
            if (tileGameCanvas.uiRects[i])
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(tileGameCanvas.uiRects[i], Input.mousePosition, Camera.main))
                {
                    testInt += 1;
                }
            }
        }

        if (testInt == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
   
    //---------------------------------------------------UNDO/HISTORY----------------------------------------------------------------------

    public void UndoLast()
    {
        if (undoButtonClickSound)
            undoButtonClickSound.Play();

        if(history.Count > 0)
            RemoveHistory(history[history.Count - 1]);
        if (currentTile)
            SetPlayerDestination(currentTile.transform);
        else
            SetPlayerDestination(playerStartPos);
    }

    void AddHistory()
    {
        var historyEntry = new TG_History();

        for (int i = 0; i < tiles.Count; i++)
        {
            var historyTileEntry = new TileProps();
            historyEntry.curTileProps.Add(historyTileEntry);

            historyTileEntry.tileType = tiles[i].tileType;
            historyTileEntry.tileState = tiles[i].tileState;
            historyTileEntry.tileOperatorType = tiles[i].tileOperatorType;
            historyTileEntry.goalFinish = tiles[i].goalFinished;
            //key
            if (tiles[i].key)
            {
                if (tiles[i].key.gameObject.activeSelf)
                    historyTileEntry.key = true; // experimental NEW
                else
                    historyTileEntry.key = false;
            }
            else
                historyTileEntry.key = false;

            historyTileEntry.value = tiles[i].value;
            historyTileEntry.mat = tiles[i].meshRenderer.material;
            historyTileEntry.startTile = tiles[i].startTile;
            historyTileEntry.colliderEnabled = tiles[i].tileCollider.enabled;
            historyTileEntry.showSprite = tiles[i].sprite.gameObject.activeSelf;
            historyTileEntry.txt = tiles[i].textMesh.text;
            historyTileEntry.showText = tiles[i].textMesh.gameObject.activeSelf;
            historyTileEntry.curYpos = tiles[i].transform.position.y;
        }

        if (currentGameSet.enemies.Count > 0)
        {
            for (int i = 0; i < currentGameSet.enemies.Count; i++)
            {
                if (currentEnemies[i])
                {
                    var historyEnemyEntry = new EnemyProps();
                    historyEntry.curEnemyProps.Add(historyEnemyEntry);

                    historyEnemyEntry.enemyIndex = currentEnemies[i].enemyIndex;
                    historyEnemyEntry.startTile = currentEnemies[i].startTile;
                    historyEnemyEntry.currentTile = currentEnemies[i].enemyCurrentTile;
                    historyEnemyEntry.lastTile = currentEnemies[i].enemyLastTile;
                    historyEnemyEntry.enemyMoveCount = currentEnemies[i].enemyMoveCounter;
                }
            }
        }
        
        historyEntry.curGameSet = currentGameSet;        
        historyEntry.curMoveCount = currentMoveCounter;
        historyEntry.curVal = currentValue;
        historyEntry.hasCalc = hasCalculated;
        historyEntry.curTile = currentTile;
        historyEntry.laTile = lastTile;
        historyEntry.laPaTile = lastPathTile;
        historyEntry.hasMoFoStart = hasMovedFromStart;
        historyEntry.hasAddStaToPath = hasAddedStartToPath;
        historyEntry.curTileType = currentTileType;
        historyEntry.curGoalButnActive = GoalButton.transform.parent.gameObject.activeSelf;
        historyEntry.curGoalCounter = currentGoalCounter;
        historyEntry.curCalcText = currentCalculation.text;
        historyEntry.curGoalAvailable = currentGoalAvailable;
        historyEntry.curGoalEffect = currentGoalEffect;

        for (int i = 0; i < currentSinkerTiles.Count; i++)
        {
            historyEntry.curSinkerTiles.Add(currentSinkerTiles[i]);
        }

        for (int i = 0; i < currentSinkTilesTimes.Count; i++)
        {
            historyEntry.curSinkerTilesTimes.Add(currentSinkTilesTimes[i]);
        }

        for (int i = 0; i < currentSinkTiles.Count; i++)
        {
            historyEntry.curSinkTiles.Add(currentSinkTiles[i]);
        }        

        for (int i = 0; i < currentlyAvailable.Count; i++)
        {
            historyEntry.curAvail.Add(currentlyAvailable[i]);
        }

        for (int i = 0; i < currentPath.Count; i++)
        {
            historyEntry.curPath.Add(currentPath[i]);
        }

        for (int i = 0; i < currentEffects.Count; i++)
        {
            historyEntry.curEffects.Add(currentEffects[i]);
        }

        for (int i = 0; i < currentLocks.Count; i++)
        {
            historyEntry.curLocks.Add(currentLocks[i].gameObject.activeSelf);
        }
        
        history.Add(historyEntry);
    }

    void RemoveHistory(TG_History historyEntry)
    {
        for (int i = 0; i < historyEntry.curTileProps.Count; i++)
        {
            tiles[i].tileType = historyEntry.curTileProps[i].tileType;
            tiles[i].tileState = historyEntry.curTileProps[i].tileState;
            tiles[i].tileOperatorType = historyEntry.curTileProps[i].tileOperatorType;
            tiles[i].goalFinished = historyEntry.curTileProps[i].goalFinish;
            //Key:
            if(tiles[i].key)
            {
                if (!tiles[i].key.gameObject.activeSelf)
                {
                    if (historyEntry.curTileProps[i].key)
                        tiles[i].key.gameObject.SetActive(true);
                }
                else
                {
                    if(!historyEntry.curTileProps[i].key)
                        tiles[i].key.gameObject.SetActive(false);
                }
            }
            tiles[i].value = historyEntry.curTileProps[i].value;
            tiles[i].meshRenderer.material = historyEntry.curTileProps[i].mat;
            tiles[i].startTile = historyEntry.curTileProps[i].startTile;
            tiles[i].tileCollider.enabled = historyEntry.curTileProps[i].colliderEnabled;
            tiles[i].sprite.gameObject.SetActive(historyEntry.curTileProps[i].showSprite);
            tiles[i].textMesh.text = historyEntry.curTileProps[i].txt;
            tiles[i].textMesh.gameObject.SetActive(historyEntry.curTileProps[i].showText);
            tiles[i].transform.position = new Vector3(tiles[i].transform.position.x, historyEntry.curTileProps[i].curYpos, tiles[i].transform.position.z);
        }
        
        currentGameSet = historyEntry.curGameSet;
        
        if(currentGameSet.enemies.Count > 0)
        {
            for (int i = 0; i < historyEntry.curEnemyProps.Count; i++)
            {
                if (currentEnemies[i] == null)
                {
                    var newEnemy = Instantiate(enemyPrefab, historyEntry.curEnemyProps[i].currentTile.transform.position, Quaternion.identity, gameTransform);
                    var newEnemyScript = newEnemy.GetComponent<TileGameEnemy>();
                    newEnemyScript.tileGameScript = this;
                    currentEnemies[i] = newEnemyScript;
                }

                if (currentEnemies[i] != null)
                {
                    currentEnemies[i].enemyIndex = historyEntry.curEnemyProps[i].enemyIndex;
                    currentEnemies[i].startTile = historyEntry.curEnemyProps[i].startTile;
                    var lastTile = currentEnemies[i].enemyCurrentTile;
                    currentEnemies[i].enemyCurrentTile = historyEntry.curEnemyProps[i].currentTile;
                    currentEnemies[i].enemyLastTile = historyEntry.curEnemyProps[i].lastTile;
                    currentEnemies[i].enemyMoveCounter = historyEntry.curEnemyProps[i].enemyMoveCount;

                    if (currentEnemies[i].enemyCurrentTile != lastTile)
                        currentEnemies[i].UndoMove();
                }                
            }
        }

        currentMoveCounter = historyEntry.curMoveCount;
        MoveCounterText.text = currentMoveCounter.ToString();
        currentValue = historyEntry.curVal;
        hasCalculated = historyEntry.hasCalc;
        currentTile = historyEntry.curTile;
        lastTile = historyEntry.laTile;
        lastPathTile = historyEntry.laPaTile;
        var tempGoal = currentGoalAvailable;
        currentGoalAvailable = historyEntry.curGoalAvailable;
        hasMovedFromStart = historyEntry.hasMoFoStart;
        hasAddedStartToPath = historyEntry.hasAddStaToPath;
        currentTileType = historyEntry.curTileType;
        currentlyAvailable = historyEntry.curAvail;
        currentPath = historyEntry.curPath;
        currentGoalCounter = historyEntry.curGoalCounter;
        currentCalculation.text = historyEntry.curCalcText;
        GoalButton.transform.parent.gameObject.SetActive(historyEntry.curGoalButnActive);

        if(historyEntry.curGoalButnActive)
        {
            int index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(currentGoalAvailable));
            goalButtonText.text = currentGoalAvailable.value.ToString();
            goalButtonImage.color = goalColors[index];
            goalGlowImage.color = new Vector4(goalColors[index].r, goalColors[index].g, goalColors[index].b, goalGlowImage.color.a);
        }

        if (currentGoalEffect && !historyEntry.curGoalEffect)
        {
            Destroy(currentGoalEffect);
            StartCoroutine(ScaleGoal(tempGoal, false));
        }

        if (!currentGoalEffect && historyEntry.curGoalEffect)
        {
            int index = currentGameSet.goalTiles.IndexOf(tiles.IndexOf(historyEntry.curGoalAvailable));
            var glow = Instantiate(goalGlowPrefab, historyEntry.curGoalAvailable.transform.position, worldTransform.rotation, gameTransform);
            glow.transform.GetChild(0).GetComponent<MeshRenderer>().material = currentGoalEffectMats[index];
            currentGoalEffect = glow;
            StartCoroutine(ScaleGoal(historyEntry.curGoalAvailable, true));
        }

        if(historyEntry.curEffects.Count < currentEffects.Count)
        {
            for (int i = 0; i < currentEffects.Count; i++)
            {
                if(i > historyEntry.curEffects.Count - 1)
                {
                    Destroy(currentEffects[i].gameObject);
                }
            }
        }

        currentEffects = historyEntry.curEffects;
        
        if(currentLocks.Count > 0)
        {
            for (int i = 0; i < currentLocks.Count; i++)
            {
                if(historyEntry.curLocks[i] != currentLocks[i].gameObject.activeSelf)
                {
                    if (historyEntry.curLocks[i])
                        StartCoroutine(SetLock(currentLocks[i], tiles[currentGameSet.goalTiles[i]].transform, currentGoalMaterials[i]));
                    else
                        StartCoroutine(DisableLock(currentLocks[i].transform));
                }
            }
        }

        if (currentSinkTiles.Count > 0)
        {
            for (int i = currentSinkTiles.Count - 1; i > -1; i--)
            {
                if (historyEntry.curSinkTiles.Count > i)
                {
                    if (historyEntry.curSinkTiles[i] != currentSinkTiles[i])
                    {
                        if (historyEntry.curSinkTiles[i])
                        {
                            StartCoroutine(SinkTile(tiles[tiles.IndexOf(currentSinkerTiles[i])], true));
                            currentSinkTiles[i] = true;
                        }
                        else
                        {
                            StartCoroutine(SinkTile(tiles[tiles.IndexOf(currentSinkerTiles[i])], false));
                            currentSinkTiles[i] = false;
                        }
                    }
                }
                else
                {
                    currentSinkTiles.Remove(currentSinkTiles[i]);
                    currentSinkerTiles.Remove(currentSinkerTiles[i]);
                    currentSinkTilesTimes.Remove(currentSinkTilesTimes[i]);
                    var crackPrefab = currentCrackedPrefabs[i].gameObject;
                    currentCrackedPrefabs.Remove(currentCrackedPrefabs[i]);
                    Destroy(crackPrefab);
                }
            }
        }

        ToggleActiveSlider();
        history.Remove(historyEntry);
        historyEntry = null;
    }
    
    public void ClearHistory()
    {        
        history.Clear();
        Resources.UnloadUnusedAssets();
    }
    //-------------------------------------------------------------------------------------------------------------------------------------

    private void UpdateLineRenderer()
    {
        lrPositions = new Vector3[currentPath.Count];

        for (int i = 0; i < currentPath.Count; i++)
        {
            lrPositions[i] = currentPath[i].transform.position + (Vector3.up * 0.2f);
        }

        lineRenderer.positionCount = lrPositions.Length;
        lineRenderer.SetPositions(lrPositions);
        lineRenderer.startColor = lineColor;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        foreach (var item in tiles)
        {
            if (item.tileCollider.enabled)
                Gizmos.DrawWireSphere(item.transform.position + Vector3.up, 0.25f);
        }
    }

    //---------------------------------------------------IDifficulty/ISetDifficulty------------------------------------------------------------------

    public void CheckDifficulty()
    {
        if (!hasWonGame)
        {
            if ((correctStreakCounter >= numberOfCorrects && tileGameDifficulty != PlayerStats.Difficulty.Hard) || (wrongStreakCounter >= numberOfWrongs && tileGameDifficulty != PlayerStats.Difficulty.Easy))
            {
                if (correctStreakCounter >= numberOfCorrects && tileGameDifficulty == PlayerStats.Difficulty.Easy)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;
                else if (correctStreakCounter >= numberOfCorrects && tileGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Hard;
                else if (wrongStreakCounter >= numberOfWrongs && tileGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Easy;
                else if (wrongStreakCounter >= numberOfWrongs && tileGameDifficulty == PlayerStats.Difficulty.Hard)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;

                if(!isFinalBossGame)
                    gameManager.hudCanvas.difficultyPanel.OpenPanel();
                correctStreakCounter = 0;
                wrongStreakCounter = 0;
            }
        }
    }

    public void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        tileGameDifficulty = requestedDifficulty;
        //InitializeGame();
        //StartNewGameSet(setCounter, tileGameDifficulty);
    }
    public void SetCurrentDifficultyStartSet(PlayerStats.Difficulty requestedDifficulty)
    {
        tileGameDifficulty = requestedDifficulty;
        InitializeGame();
        StartNewGameSet(setCounter, tileGameDifficulty);
    }

    public void SetGameSetWithDifficulty(int index)
    {
        switch (tileGameDifficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                currentGameSet = gameSetsEasy[index];
                break;
            case (PlayerStats.Difficulty.Normal):
                currentGameSet = gameSets[index];
                break;
            case (PlayerStats.Difficulty.Hard):
                currentGameSet = gameSetsHard[index];
                break;
            default:
                currentGameSet = gameSets[index];
                break;
        }
    }

    public void WrongTicker()
    {
        wrongStreakCounter++;
        correctStreakCounter = 0;
        CheckDifficulty();
    }


    public void CorrectTicker()
    {
        wrongStreakCounter = 0;
        correctStreakCounter++;
        CheckDifficulty();
    }

    public void OpenDifficultyPanelStart()
    {
        gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.None;
        gameManager.hudCanvas.difficultyPanel.startNewLevel = true;

        if ((!isFinalBossGame && gameManager.playerStats.hasWonTileGame) || (isFinalBossGame && gameManager.playerStats.hasWonBossTileGame))
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = true;
            gameManager.hudCanvas.difficultyPanel.currentLevelCount = gameSets.Count;
        }
        else
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = false;
        }

        gameManager.hudCanvas.difficultyPanel.gameObject.SetActive(true);
    }

    public void SetCurrentCompletedSet(int passedAchievement)
    {
        int setIndex = 0;

        if (gameSets.Contains(currentGameSet))
            setIndex = gameSets.IndexOf(currentGameSet) + gameSets.Count;
        else if (gameSetsEasy.Contains(currentGameSet))
            setIndex = gameSetsEasy.IndexOf(currentGameSet);
        else if (gameSetsHard.Contains(currentGameSet))
            setIndex = gameSetsHard.IndexOf(currentGameSet) + (gameSets.Count * 2);

        if (!isFinalBossGame)
        {
            if (!gameManager.playerStats.tileGameCompletedSets.Contains(setIndex))
            {
                gameManager.playerStats.tileGameCompletedSets.Add(setIndex);
                gameManager.playerStats.tileGameSetAchievements.Add(setIndex, passedAchievement);
            }
            else
            {
                gameManager.playerStats.tileGameSetAchievements[setIndex] = passedAchievement;
            }
        }
        else
        {
            if (!gameManager.playerStats.bossTileGameCompletedSets.Contains(setIndex))
            {
                gameManager.playerStats.bossTileGameCompletedSets.Add(setIndex);
                gameManager.playerStats.bossTileGameSetAchievements.Add(setIndex, passedAchievement);
            }
            else
            {
                gameManager.playerStats.bossTileGameSetAchievements[setIndex] = passedAchievement;
            }
        }

        //SetPlayerStatsCurrentCompletedSets();
    }
    
    public void SetPlayerStatsCurrentCompletedSets()
    {
        gameManager.playerStats.currentCompletedSets.Clear();
        gameManager.playerStats.currentAchievements.Clear();

        if (!isFinalBossGame)
        {
            for (int i = 0; i < gameManager.playerStats.tileGameCompletedSets.Count; i++)
            {
                var index = gameManager.playerStats.tileGameCompletedSets[i];
                gameManager.playerStats.currentCompletedSets.Add(index);
                gameManager.playerStats.currentAchievements.Add(index, gameManager.playerStats.tileGameSetAchievements[index]);
            }
        }
        else
        {
            for (int i = 0; i < gameManager.playerStats.bossTileGameCompletedSets.Count; i++)
            {
                var index = gameManager.playerStats.bossTileGameCompletedSets[i];
                gameManager.playerStats.currentCompletedSets.Add(index);
                gameManager.playerStats.currentAchievements.Add(index, gameManager.playerStats.bossTileGameSetAchievements[index]);
            }
        }
    }

    public void TransferPlayerStats()
    {
        //
        if (!isFinalBossGame)
        {
            hasWonGame = gameManager.playerStats.hasWonTileGame;
            setCounter = gameManager.playerStats.TileGameSetCount;
        }
        else
        {
            hasWonGame = gameManager.playerStats.hasWonBossTileGame;
            setCounter = gameManager.playerStats.BossTileGameSetCount;
        }
        //

        completedSets.Clear();
        completedSetsAchievements.Clear();

        if(!isFinalBossGame)
        {
            
            for (int i = 0; i < gameManager.playerStats.tileGameCompletedSets.Count; i++)
            {
                var setIndex = gameManager.playerStats.tileGameCompletedSets[i];

                if (setIndex < gameSets.Count)
                {
                    completedSets.Add(gameSetsEasy[setIndex]);                    
                }
                else if(setIndex >= gameSets.Count && setIndex < (gameSets.Count * 2))
                {
                    completedSets.Add(gameSets[setIndex - gameSets.Count]);
                }
                else if (setIndex >= (gameSets.Count * 2) && setIndex < (gameSets.Count * 3))
                {
                    completedSets.Add(gameSetsHard[setIndex - (gameSets.Count * 2)]);
                }

                completedSetsAchievements.Add(gameManager.playerStats.tileGameSetAchievements[setIndex]);
            }
            
        }
        else
        {
            for (int i = 0; i < gameManager.playerStats.bossTileGameCompletedSets.Count; i++)
            {
                var setIndex = gameManager.playerStats.bossTileGameCompletedSets[i];

                if (setIndex < gameSets.Count)
                {
                    completedSets.Add(gameSetsEasy[setIndex]);
                }
                else if (setIndex >= gameSets.Count && setIndex < (gameSets.Count * 2))
                {
                    completedSets.Add(gameSets[setIndex - gameSets.Count]);
                }
                else if (setIndex >= (gameSets.Count * 2) && setIndex < (gameSets.Count * 3))
                {
                    completedSets.Add(gameSetsHard[setIndex - (gameSets.Count * 2)]);
                }

                completedSetsAchievements.Add(gameManager.playerStats.bossTileGameSetAchievements[setIndex]);
            }
        }
    }

    public void StartSelectedGameSet(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        tileGameDifficulty = requestedDifficulty;

        if (requestedLevel < gameSets.Count)
            setCounter = requestedLevel;
        else if (requestedLevel >= gameSets.Count && requestedLevel < (gameSets.Count * 2))
            setCounter = requestedLevel - gameSets.Count;
        else if (requestedLevel >= gameSets.Count * 2)
            setCounter = requestedLevel - (gameSets.Count * 2);

        //StopAllCoroutines();
        InitializeGame();
        StartNewGameSet(setCounter, tileGameDifficulty);
    }

    //IAchievementPanel:------------------------------------

    public void OpenAchievementPanel(AchievementPanel achievementPanel, PlayerStats.Difficulty difficulty, int levelIndex, int totalLevels, int prevAchievement, int achievement)
    {
        achievementPanel.difficulty = difficulty;
        achievementPanel.levelIndex = levelIndex;
        achievementPanel.prevAchievment = prevAchievement;
        achievementPanel.achievement = achievement;
        achievementPanel.totalLevels = totalLevels;

        achievementPanel.gameObject.SetActive(true);
    }

    public int CheckLevelIndex(PlayerStats.Difficulty difficulty)
    {
        switch (difficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                return gameSetsEasy.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Normal):
                return gameSets.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Hard):
                return gameSetsHard.IndexOf(currentGameSet) + 1;
            default:
                return 0;
        }
    }

    public int CheckCurrentAchievement()
    {
        int testAchievement;

        if (currentMoveCounter <= currentGameSet.targetMoves.z)
        {
            testAchievement = 3;          
        }
        else if (currentMoveCounter <= currentGameSet.targetMoves.y)
        {
            testAchievement = 2;           
        }
        else if (currentMoveCounter <= currentGameSet.targetMoves.x)
        {
            testAchievement = 1;           
        }
        else
        {
            testAchievement = 0;
        }

        return testAchievement;
    }

    public int CheckPrevAchievement()
    {
        int prevAchievement = 0;
        return prevAchievement;
    }    
}
