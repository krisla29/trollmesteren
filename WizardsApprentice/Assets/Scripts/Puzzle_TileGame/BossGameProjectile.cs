﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossGameProjectile : MonoBehaviour
{
    public BossGame bossGameScript;
    public GameObject deathEffect;
    public Vector3 startPos;
    public Vector3 endPos;
    public float lerpTime = 15f;
    private bool hasInflictedDamage;

    public delegate void BG_ProjectileEvent(); //Sends to BossGame
    public static event BG_ProjectileEvent OnProjectileExplode;

    private void OnEnable()
    {
        StartCoroutine(Birth());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void Destroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator Birth()
    {
        float t = 0;
        float birthTime = 1;

        while( t < 1)
        {
            t += Time.deltaTime / birthTime;
            var h = LerpUtils.SmoothStep(t);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            RotateProjectile();
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(Travel());
    }
    /*
    private IEnumerator Death()
    {

        float t = 0;
        float deathTime = 0.15f;

        while (t < 1)
        {
            t += Time.deltaTime / deathTime;
            var h = LerpUtils.SmoothStep(t);
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        Explode();
    }
    */
    private IEnumerator Travel()
    {
        float t = 0;
        //Vector3 end = transform.forward * 20f;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            var h = LerpUtils.SmoothStep(t);
            transform.position = Vector3.Lerp(startPos, endPos, h);
            RotateProjectile();
            yield return new WaitForEndOfFrame();
        }

        //StartCoroutine(Death());
        Explode();
    }

    void Explode()
    {
        if (OnProjectileExplode != null)
            OnProjectileExplode();

        var effect = Instantiate(deathEffect, bossGameScript.effectsContainer);
        effect.transform.position = transform.position;
        Destroy(transform.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (!hasInflictedDamage)
            {
                StopCoroutine(Travel());
                //StartCoroutine(Death());
                bossGameScript.bgPlayerHealth.RemoveHeart();
                hasInflictedDamage = true;
                Explode();
            }
        }
    }

    void RotateProjectile()
    {
        Vector3 lookVector = endPos - transform.position;
        Quaternion look = Quaternion.LookRotation(lookVector);
        transform.rotation = Quaternion.Slerp(transform.rotation, look, Time.deltaTime * 50f);
    }
}
