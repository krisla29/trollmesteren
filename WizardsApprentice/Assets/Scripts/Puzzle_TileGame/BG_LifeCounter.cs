﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BG_LifeCounter : MonoBehaviour
{
    public BossGame bossGameScript;
    public Canvas playerHealthCanvas;
    public List<Image> hearts = new List<Image>();
    public RectTransform glow;
    public Color fullColor = Color.red;
    public Color emptyColor = Color.white;
    public float animTime = 0.5f;
    public AudioSource soundGainHeart;
    public AudioSource soundLoseHeart;
    private Coroutine animIn;
    //public int currentHearts = 5;

    private void OnEnable()
    {
        SetToAllFull();
        ResetGlow();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        animIn = null;
        ResetGlow();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        animIn = null;
    }

    void ResetGlow()
    {
        glow.transform.localScale = Vector3.zero;
        glow.gameObject.SetActive(false);
    }

    public void SetToAllFull()
    {
        StopAllCoroutines();
        animIn = null;
        animIn = StartCoroutine(AnimateIn());

        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].color = fullColor;
        }

        bossGameScript.playerHealth = hearts.Count;
    }

    public void SetToAllEmpty()
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].color = emptyColor;
        }

        bossGameScript.playerHealth = 0;
    }

    public void AddHeart()
    {
        if (bossGameScript.playerHealth < hearts.Count)
        {
            bossGameScript.playerHealth++;
            StartCoroutine(AddHeartAnim());

            if (!bossGameScript.battleManagerScript.currentScene)
                bossGameScript.gameManager.player.anim.SetTrigger("Success");
        }
    }

    public void RemoveHeart()
    {
        if (bossGameScript.playerHealth > 0)
        {
            bossGameScript.playerHealth--;
            StartCoroutine(RemoveHeartAnim());

            if(bossGameScript.playerHealth == 0)
            {
                bossGameScript.CallGameOver();
            }

            if (!bossGameScript.battleManagerScript.currentScene)
                bossGameScript.gameManager.player.anim.SetTrigger("Defeat");
        }        
    }

    private IEnumerator RemoveHeartAnim()
    {
        float t = 0;
        float scaleToSubtract = -0.2f;
        glow.anchoredPosition = hearts[bossGameScript.playerHealth].GetComponent<RectTransform>().anchoredPosition;
        glow.gameObject.SetActive(true);

        if (soundLoseHeart)
            soundLoseHeart.Play();

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            hearts[bossGameScript.playerHealth].color = Vector4.Lerp(fullColor, emptyColor, h);
            var scale = Mathf.Sin(h * (Mathf.PI));
            glow.transform.localScale = Vector3.one * scale;
            scale *= scaleToSubtract;
            hearts[bossGameScript.playerHealth].transform.localScale = Vector3.one + (Vector3.one * scale);
            yield return new WaitForEndOfFrame();
        }

        glow.gameObject.SetActive(false);
    }

    private IEnumerator AddHeartAnim()
    {
        float t = 0;
        float scaleToAdd = 0.2f;
        glow.anchoredPosition = hearts[bossGameScript.playerHealth - 1].GetComponent<RectTransform>().anchoredPosition;
        glow.gameObject.SetActive(true);

        if (soundGainHeart)
            soundGainHeart.Play();
        
        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            hearts[bossGameScript.playerHealth - 1].color = Vector4.Lerp(emptyColor, fullColor, h);
            var scale = Mathf.Sin(h * (Mathf.PI));
            glow.transform.localScale = Vector3.one * scale;
            scale *= scaleToAdd;
            hearts[bossGameScript.playerHealth - 1].transform.localScale = Vector3.one + (Vector3.one * scale);            
            yield return new WaitForEndOfFrame();
        }

        glow.gameObject.SetActive(false);
    }

    private IEnumerator AnimateIn()
    {
        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].gameObject.SetActive(false);
        }

        yield return new WaitForSeconds(2);

        for (int i = 0; i < hearts.Count; i++)
        {
            hearts[i].gameObject.SetActive(true);

            if (soundGainHeart)
                soundGainHeart.Play();

            float t = 0;
            float animTime = 0.25f;

            while (t < 1)
            {
                t += Time.deltaTime / animTime;
                var h = LerpUtils.SmoothStep(t);
                hearts[i].transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
                yield return new WaitForEndOfFrame();
            }

            float ti = 0;
            float animTime2 = 0.2f;
            float scaleToAdd = 0.3f;

            while (ti < 1)
            {
                ti += Time.deltaTime / animTime2;
                var h = LerpUtils.SmoothStep(ti);
                var scale = Mathf.Sin(h * Mathf.PI);
                scale *= scaleToAdd;
                hearts[i].transform.localScale = Vector3.one + (Vector3.one * scale);
                yield return new WaitForEndOfFrame();
            }
        }        
    }
}
