﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataTypes
{
   public enum DataType { Int, Float, Vector3, Color, Gradient }
}
