﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAV_Object : MonoBehaviour
{
    public AreaVolumeObject av_Object;

    private void OnMouseDrag()
    {
        av_Object.ScaleBoundryObject();
    }
}
