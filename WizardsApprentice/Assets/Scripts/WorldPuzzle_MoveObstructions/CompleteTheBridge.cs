﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CompleteTheBridge : MonoBehaviour
{
    public GameManager gameManager;
    public Collider thisCollider;
    public Transform playerPos;
    public NavMeshObstacle obstacle;
    public AudioSource soundMove;
    public AudioSource soundClick;
    public bool isComplete;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(!isComplete)
                StartCoroutine(Unlock());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if(!isComplete)
                SetToComplete();
        }       
    }

    void SetToComplete()
    {
        obstacle.transform.position += (Vector3.up * -3);
        obstacle.gameObject.SetActive(false);
        transform.position += (Vector3.up * - 0.2f);
        transform.gameObject.SetActive(false);
        isComplete = true;
    }

    private IEnumerator Unlock()
    {
        float nh = 0.2f;
        gameManager.player.navMeshAgent.height = nh;
        gameManager.player.navMeshAgent.SetDestination(playerPos.position);
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;

        float x = 0;
        float t = 0;
        float animTime = 3;

        Vector3 startPosO = obstacle.transform.position;
        Vector3 endPosO = obstacle.transform.position + (Vector3.up * -3);
        Vector3 startPosT = transform.position;
        Vector3 endPosT = transform.position + (Vector3.up * -0.2f);

        if (soundClick)
            soundClick.Play();

        while(x < 1)
        {
            x += Time.deltaTime / 1.5f;
            transform.position = Vector3.Lerp(startPosT, endPosT, x);
            gameManager.player.navMeshAgent.height = Mathf.Lerp(nh, 0, x);
            yield return new WaitForEndOfFrame();
        }

        gameManager.player.navMeshAgent.height = 0;
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;

        if (soundMove)
            soundMove.Play();

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            obstacle.transform.position = Vector3.Lerp(startPosO, endPosO, t);
            
            yield return new WaitForEndOfFrame();
        }

        isComplete = true;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
