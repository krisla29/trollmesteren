﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpIndicator : MonoBehaviour
{
    public GameManager gameManager;
    public Transform startIndicator;
    public Transform endIndicator;
    public Transform pathIndicator;
    public Transform grp;
    public enum Direction { Both, Down, Up}
    public Direction direction;

    public float startEndPulseTime = 2f;
    public float startEndScaleMult = 1f;
    public float pathTravelTime = 4f;
    public float pathMaxHeight = 3f;
    public float pathRotMult = -180f;

    private Vector3 startAnglesPath;
    private Vector3 startScalePath;
    private Vector3 startScaleStart;
    private Vector3 startScaleEnd;
    private bool isLowerThanPlayer;
    private bool reverseDirection;

    private void Awake()
    {
        startIndicator.gameObject.SetActive(false);
        endIndicator.gameObject.SetActive(false);
        pathIndicator.gameObject.SetActive(false);        
        startAnglesPath = pathIndicator.localEulerAngles;
        startScalePath = pathIndicator.localScale;
        startScaleStart = startIndicator.localScale;
        startScaleEnd = endIndicator.localScale;
    }

    private void OnEnable()
    {
        StopCoroutine(MainSequence());
        ResetAll();
        StartCoroutine(MainSequence());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator MainSequence()
    {
        ResetAll();
        Vector3 startSc = new Vector3(startScalePath.x, 0, startScalePath.z);
        Vector3 startPs = new Vector3(startIndicator.localPosition.x, startIndicator.localPosition.y + (Mathf.Abs(startScalePath.y) * 0.5f), startIndicator.localPosition.z);

        if(reverseDirection)
            startPs = new Vector3(endIndicator.localPosition.x, endIndicator.localPosition.y + (Mathf.Abs(startScalePath.y) * 0.5f), endIndicator.localPosition.z);

        pathIndicator.localScale = startSc;

        if (!reverseDirection)
            startIndicator.gameObject.SetActive(true);
        else
            endIndicator.gameObject.SetActive(true);

        pathIndicator.gameObject.SetActive(true);

        float t = 0;
        float animTime1 = startEndPulseTime;
        

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            var h = LerpUtils.SmootherStep(t);
            var s = Mathf.Sin(t * (Mathf.PI));
            s *= startEndScaleMult;

            if(!reverseDirection)
                startIndicator.localScale = startScaleStart * s;
            else
                endIndicator.localScale = startScaleEnd * s;

            pathIndicator.localScale = Vector3.Lerp(startSc, startScalePath, h);
            yield return new WaitForEndOfFrame();
        }

        if (!reverseDirection)
            startIndicator.gameObject.SetActive(false);
        else
            endIndicator.gameObject.SetActive(false);

        float t2 = 0;
        float animTime2 = pathTravelTime;
        float heightMult = pathMaxHeight;
        var sp = startIndicator.localPosition;
        var ep = endIndicator.localPosition;
        var a = new Vector3(startAnglesPath.x, startAnglesPath.y, startAnglesPath.z + pathRotMult);

        if (reverseDirection)
        {
            sp = endIndicator.localPosition;
            ep = startIndicator.localPosition;
            a = new Vector3(startAnglesPath.x, startAnglesPath.y, startAnglesPath.z + (-pathRotMult));
        }

        while (t2 < 1)
        {
            t2 += Time.deltaTime / animTime2;
            var h = LerpUtils.SmootherStep(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI)); // half round 0 - 1 - 0
            var y = new Vector3(0, s * heightMult, 0);
            pathIndicator.localPosition = Vector3.Lerp(sp + y, ep + y, h);
            pathIndicator.localEulerAngles = Vector3.Lerp(startAnglesPath, a, h);
            
            yield return new WaitForEndOfFrame();
        }

        if (!reverseDirection)
            endIndicator.gameObject.SetActive(true);
        else
            startIndicator.gameObject.SetActive(true);

        float t3 = 0;
        float animTime3 = startEndPulseTime;

        while (t3 < 1)
        {
            t3 += Time.deltaTime / animTime3;
            var h = LerpUtils.SmootherStep(t3);
            var s = Mathf.Sin(t3 * (Mathf.PI));
            s *= startEndScaleMult;

            if (!reverseDirection)
                endIndicator.localScale = startScaleEnd * s;
            else
                startIndicator.localScale = startScaleStart * s;

            pathIndicator.localScale = Vector3.Lerp(startScalePath, startSc, h);
            yield return new WaitForEndOfFrame();
        }

        pathIndicator.gameObject.SetActive(false);

        if (!reverseDirection)
            endIndicator.gameObject.SetActive(false);
        else
            startIndicator.gameObject.SetActive(false);

        PlayerPosCheck();
    }

    void ResetAll()
    {
        startIndicator.gameObject.SetActive(false);
        endIndicator.gameObject.SetActive(false);
        pathIndicator.gameObject.SetActive(false);

        if (!reverseDirection)
            pathIndicator.localPosition = startIndicator.localPosition;
        else
            pathIndicator.localPosition = endIndicator.localPosition;

        pathIndicator.localEulerAngles = startAnglesPath;
        pathIndicator.localScale = startScalePath;
        startIndicator.localScale = startScaleStart;
        endIndicator.localScale = startScaleEnd;
    }

    void PlayerPosCheck()
    {
        if (gameManager.player.transform.position.y > transform.position.y)
            isLowerThanPlayer = true;
        else
            isLowerThanPlayer = false; 

        if(direction == Direction.Both)
        {
            if(isLowerThanPlayer)
            {
                reverseDirection = true;
            }
            else
            {
                reverseDirection = false;
            }
        }
        else
        {
            reverseDirection = false;
        }

        StartCoroutine(MainSequence());
    }
    /*
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (!grp.gameObject.activeSelf)
                grp.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (grp.gameObject.activeSelf)
                grp.gameObject.SetActive(false);
        }
    }
    */
}
