﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignPost : MonoBehaviour
{
    public CanvasGroup signCanvas;
    public SignPostCanvas signPostCanvasScript;
    private float startTime;
    public float showTime = 5f;
    private bool isVisible;
    public WandInteractable wandInteractable;
    public Interactable interactable;
    private bool isPlayerTrigger;
    public AudioSource audioSource;
    public AudioSource soundCloseBtn;
    private float fadeInTime = 0.5f;
    private float fadeOutTime = 2f;
    private float displayTime = 6f;
    private bool isPressed;

    private void Awake()
    {
        signCanvas.gameObject.SetActive(false);
    }
    
    private void ShowCanvas()
    {
        audioSource.Play();
        signCanvas.gameObject.SetActive(true);
        isVisible = true;
        isPlayerTrigger = false;
        StopAllCoroutines();
        StartCoroutine(FadeIn());
    }
    private void OnEnable()
    {
        Interactable.OnInteractablePressed += RegisterTrigger;
        SignPostCanvas.OnPressedCanvas += RegisterTrigger;
    }
    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= RegisterTrigger;
        SignPostCanvas.OnPressedCanvas -= RegisterTrigger;
        signPostCanvasScript.HideCanvasFromHud();
        signCanvas.gameObject.SetActive(false);
        CancelInvoke();
        StopAllCoroutines();
    }
    private void RegisterTrigger(Transform transform)
    {
        if(transform == interactable.transform || transform == signPostCanvasScript.transform)
        {
            StopAllCoroutines();
            StartCoroutine(FadeIn());
        }
    }

    private IEnumerator FadeIn()
    {
        audioSource.Play();
        signCanvas.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            signCanvas.alpha = Mathf.Lerp(0, 1, t);
            t += Time.deltaTime / fadeInTime;
            yield return new WaitForEndOfFrame();
        }
            
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        if (signCanvas.gameObject.activeSelf)
        {
            yield return new WaitForSeconds(displayTime);

            float t = 0;

            while (t < 1)
            {
                signCanvas.alpha = Mathf.Lerp(1, 0, t);
                t += Time.deltaTime / fadeOutTime;
                yield return new WaitForEndOfFrame();
            }

            signPostCanvasScript.HideCanvasFromHud();
            signCanvas.gameObject.SetActive(false);
        }
    }

    public void CloseCanvas()
    {
        if (soundCloseBtn)
            soundCloseBtn.Play();

        StopAllCoroutines();
        signPostCanvasScript.HideCanvasFromHud();
        signCanvas.alpha = 0;
        //signCanvas.gameObject.SetActive(false);
        Invoke("SetInactive", 0);
    }

    void SetInactive()
    {
        signCanvas.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        CancelInvoke();
        StopAllCoroutines();
    }
}
