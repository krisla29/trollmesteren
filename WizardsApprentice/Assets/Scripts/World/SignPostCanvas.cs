﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignPostCanvas : MonoBehaviour
{
    public Canvas canvas;
    public ScaleWithScreenHeight scaleHeightScript;
    public RectTransform canvasTransform;
    public RectTransform contentScaler;
    public RectTransform infoText;
    public Collider canvasCollider;
    public Button closeCanvasButton;
    public LookAtCameraUI lookAtCameraScript;
    public Collider rayBlocker;
    public Transform closeGraphic;
    public AudioSource soundCloseBtn;

    private Vector2 startSize;
    private Vector3 startPos;
    private Vector3 startScale;

    public delegate void SignPostCanvasEvent(Transform thisTransform); //Sends to SignPost script, SignPostCanvas scripts
    public static event SignPostCanvasEvent OnPressedCanvas;
    public static event SignPostCanvasEvent OnCloseCanvas;
    public delegate void SignPostCanvasGlobalEvent(); // Sends to PlayerController
    public static event SignPostCanvasGlobalEvent OnHudCanvasEnable;
    public static event SignPostCanvasGlobalEvent OnHudCanvasDisable;

    private void Start()
    {
        startSize = canvasTransform.sizeDelta;
        startPos = transform.position;
        startScale = transform.localScale;

        if (scaleHeightScript)
            scaleHeightScript.disableScaling = true;
    }

    private void OnEnable()
    {
        SignPostCanvas.OnPressedCanvas += CheckIfOtherOpened;
    }

    private void OnDisable()
    {
        SignPostCanvas.OnPressedCanvas -= CheckIfOtherOpened;
    }

    void CheckIfOtherOpened(Transform passedCanvasTransform)
    {
        if(passedCanvasTransform != transform)
        {
            HideCanvasFromHud();
        }
    }

    private void DisplayCanvasInHud()
    {
        closeGraphic.gameObject.SetActive(false);
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvasCollider.enabled = false;
        closeCanvasButton.gameObject.SetActive(true);
        lookAtCameraScript.enabled = false;

        if (contentScaler)
            contentScaler.localScale = Vector3.one;

        if (infoText)
            infoText.localScale = Vector3.one * 4f;

        if (scaleHeightScript)
            scaleHeightScript.disableScaling = false;

        if (OnPressedCanvas != null)
            OnPressedCanvas(transform);

        if (OnHudCanvasEnable != null)
            OnHudCanvasEnable();
    }

    public void HideCanvasByClick()
    {
        if (soundCloseBtn)
            soundCloseBtn.Play();

        HideCanvasFromHud();
    }

    public void HideCanvasFromHud()
    {
        closeGraphic.gameObject.SetActive(true);
        canvas.renderMode = RenderMode.WorldSpace;
        canvasCollider.enabled = true;
        closeCanvasButton.gameObject.SetActive(false);
        lookAtCameraScript.enabled = true;
        transform.position = startPos;
        transform.localScale = startScale;
        canvasTransform.sizeDelta = startSize;       

        if (contentScaler)
            contentScaler.localScale = Vector3.one * 0.25f;

        if (infoText)
            infoText.localScale = Vector3.one;

        if (scaleHeightScript)
            scaleHeightScript.disableScaling = true;

        if (OnCloseCanvas != null)
            OnCloseCanvas(transform);

        if (OnHudCanvasDisable != null)
            OnHudCanvasDisable();
    }

    private void OnMouseDown()
    {
        DisplayCanvasInHud();
    }
}
