﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject fire;
    public Transform scaleTransform;
    public GameObject ExtinguishEffect;
    private Vector3 startSize;
    private Coroutine startRoutine;
    private Coroutine stopRoutine;

    private void Awake()
    {
        startSize = scaleTransform.localScale;
    }

    private void Start()
    {
        Initialize();
    }

    private void OnEnable()
    {
        TimeGame.OnMorning += PutOutFire;
        TimeGame.OnNight += CheckDistanceToPlayer;
    }

    private void OnDisable()
    {
        TimeGame.OnMorning -= PutOutFire;
        TimeGame.OnNight -= CheckDistanceToPlayer;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            CheckTime();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (fire.gameObject.activeSelf)
            {
                stopRoutine = StartCoroutine(StopFire());

                if (ExtinguishEffect)
                {
                    var effect = Instantiate(ExtinguishEffect, fire.transform.position, Quaternion.identity, gameManager.worldEffectsContainer);
                }
            }
        }
    }

    private void Initialize()
    {
        if (gameManager.playerStats.timeOfDay[0] >= 21 || gameManager.playerStats.timeOfDay[0] < 6)
        {
            fire.gameObject.SetActive(true);
            scaleTransform.localScale = startSize;
        }
        else
        {
            fire.gameObject.SetActive(false);
            scaleTransform.localScale = Vector3.zero;
        }
    }

    void CheckTime()
    {
        if (gameManager.playerStats.timeOfDay[0] >= 21 || gameManager.playerStats.timeOfDay[0] < 6)
            startRoutine = StartCoroutine(StartFire());
    } 

    void CheckDistanceToPlayer()
    {
        if((transform.position - gameManager.player.transform.position).sqrMagnitude < (gameManager.torchesDist * gameManager.torchesDist))
        {
            if (!fire.gameObject.activeSelf)
                startRoutine = StartCoroutine(StartFire());
        }
    }

    void PutOutFire()
    {
        if (fire.gameObject.activeSelf)
        {
            stopRoutine = StartCoroutine(StopFire());

            if (ExtinguishEffect)
            {
                var effect = Instantiate(ExtinguishEffect, fire.transform.position, Quaternion.identity, gameManager.worldEffectsContainer);
            }
        }
    }

    private IEnumerator StartFire()
    {
        fire.gameObject.SetActive(true);
        scaleTransform.localScale = Vector3.zero;

        float t = 0;
        float growTime = 2;

        while(t < 1)
        {
            t += Time.deltaTime / growTime;
            var h = LerpUtils.SmootherStep(t);
            scaleTransform.localScale = Vector3.Lerp(Vector3.zero, startSize, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator StopFire()
    {
        scaleTransform.localScale = startSize;

        float t = 0;
        float shrinkTime = 1;

        while (t < 1)
        {
            t += Time.deltaTime / shrinkTime;
            var h = LerpUtils.SmootherStep(t);
            scaleTransform.localScale = Vector3.Lerp(startSize, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        fire.gameObject.SetActive(false);
    }
}
