﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignPostClose : MonoBehaviour
{
    public SignPost signPostScript;

    private void OnMouseDown()
    {
        signPostScript.CloseCanvas();
    }
}
