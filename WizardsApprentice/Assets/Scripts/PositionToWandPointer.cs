﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionToWandPointer : MonoBehaviour
{
    //private Transform lastParent;
    public WandInteractable wandInteractable;
    //private Transform wandPointer;

    void Start()
    {
        //lastParent = transform.parent;
        //wandPointer = GameObject.FindGameObjectWithTag("WandPointer").transform;
    }

    void Update()
    {
        if (wandInteractable.isInteracting)
        {
            if (wandInteractable.wandPointer)
            {
                transform.position = wandInteractable.wandPointer.position;
            }
            else
            {
                //transform.parent = lastParent;
            }
        }
        else
        {
            //transform.parent = lastParent;
        }
    }
}
