﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectCharacterMenu : MonoBehaviour
{
    public MainMenu mainMenuScript;
    public Button boyButton;
    public Button girlButton;

    public Animator boyAnim;
    public Animator girlAnim;

    private void Awake()
    {
        boyAnim.Play("Idle", 0, 0.5f);
    }

    public void BackToMainMenu()
    {
        mainMenuScript.mainMenu.transform.gameObject.SetActive(true);
        transform.gameObject.SetActive(false);
    }
}
