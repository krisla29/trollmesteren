﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.IO;
//using System.Runtime.Serialization.Formatters.Binary;

public class MainMenuController : MonoBehaviour
{
    public MainMenu mainMenuScript;
    public SelectCharacterMenu selectCharacterScript;

    string path;
   
    private void Awake()
    {
        Debug.Log(Application.productName);
        //Check to set save game methods;

        if (Application.platform == RuntimePlatform.WebGLPlayer)
            GameProperties.isWebGl = true;
        else
            GameProperties.isWebGl = false;

        Debug.Log("GameProperties.isWebGl is set to " + GameProperties.isWebGl);
    }

    private void OnEnable()
    {
        RestManager2.OnPreloadDone += RecieveWebSaveInfo;
        path = GameProperties.saveDirectory;
        InitializeGame();
    }

    private void OnDisable()
    {
        RestManager2.OnPreloadDone -= RecieveWebSaveInfo;
    }

    bool CheckForSaveFile()
    {       
        if (File.Exists(path))
        {
            Debug.Log("saveGame found in " + path);
            return true;
        }
        else
        {
            Debug.LogWarning("saveGame file not found in " + path);
            return false;
        }
    }

    private void InitializeGame()
    {
        if (!GameProperties.isWebGl)
        {
            if (CheckForSaveFile())
                SetSaveState(true);
            else
                SetSaveState(false);
        }
        else
        {
            if (!GameProperties.gp_initialized)
            {
                SaveSystemWeb.InitWebGl();
                Debug.Log("Initializing WebGl from MainMenuController...");
            }
            else
            {
                RestManager2.instance.GetStaticUris();
            }
        }
    }

    private void SetSaveState(bool hasData)
    {
        if (hasData)
        {
            mainMenuScript.SetToContinueGame();
            mainMenuScript.hasSaveData = true;
        }
        else
        {
            mainMenuScript.SetToStartNewGame();
            mainMenuScript.hasSaveData = false;
        }
    }

    private void RecieveWebSaveInfo(bool hasData)
    {
        if (hasData)
            SetSaveState(true);
        else
            SetSaveState(false);
    }

    public void DeleteSavedGame()
    {
        if (File.Exists(path))
        {
            File.Delete(path);
            Debug.Log("Deleted saveGame in " + path);
            mainMenuScript.hasSaveData = false;
        }
    }
}
