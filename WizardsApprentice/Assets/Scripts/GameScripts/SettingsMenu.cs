﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenu : MonoBehaviour
{
    public MainMenu mainMenuScript;

    public void BackToMainMenu()
    {
        mainMenuScript.mainMenu.transform.gameObject.SetActive(true);
        transform.gameObject.SetActive(false);
    }
}
