﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameProperties
{
    public static string saveDirectory = Application.persistentDataPath + "/saveGame.Wa";
    public static string gp_saveGameUri;
    public static string gp_ugcUri;
    public static string gp_createItemUri;
    public static bool gp_initialized;
    public static bool isNewGame;
    public static bool isBoy;
    public static bool isWebGl;
}
