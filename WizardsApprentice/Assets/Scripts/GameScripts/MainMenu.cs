﻿using System;
//using System.Net;
//using System.Text;
using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Networking;
using UnityEngine.SceneManagement;
//using System.IO;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour
{
    public MainMenuController mainMenuController;
    public RectTransform mainMenu;

    public Button startButton1;
    public Image button1Image;
    public RectTransform button1Rect;
    public RectTransform mainRect;
    public TextMeshProUGUI button1Text;
    public RectTransform minorRect;
    public Button startButton2;
    public Image button2Image;
    public RectTransform button2Rect;
    public TextMeshProUGUI button2Text;

    public Sprite buttonImageMain;
    public Sprite buttonImageMinor;
    public Color TextColorMain;
    public Color TextColorMinor;
    public Color TextColorUnavailable;
    public Color buttonColorMain;
    public Color buttonColorMinor;
    public Color buttonColorUnavailable;
    public float fontSizeMain;
    public float fontSizeMinor;

    public bool hasSaveData;

    string urlAddress = "https://skole.salaby.no/5-7/norsk";

    public Canvas loadingCanvas;
    public CanvasGroup loadingCG;
    public Slider loadingSlider;
    AsyncOperation loadingOperation;
    public bool isLoadingGame;

    public RectTransform selectCharacterPanel;
    public RectTransform soundMenuPanel;
    public RectTransform confirmationDBox;
    
    private void OnEnable()
    {
        loadingCanvas.gameObject.SetActive(false);
        loadingCG.alpha = 0;
        isLoadingGame = false;
        Time.timeScale = 1;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void ContinueGame()
    {
        GameProperties.isNewGame = false;
        loadingOperation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        StartLoading();
    }

    public void StartNewGame()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    void StartLoading()
    {
        loadingCanvas.gameObject.SetActive(true);
        isLoadingGame = true;
        StartCoroutine(FadeInLoadingScreen());
    }

    public void Settings()
    {
        //Application.OpenURL("https://skole.salaby.no/5-7/norsk");
        //bool check = false;
        //GetRequestHeader();

        //StartCoroutine(GetRequest(urlAddress));

        //check = CheckSalabyLogin();
        //Debug.Log(check);
        //CheckSalabyLogin();
        soundMenuPanel.gameObject.SetActive(true);
        mainMenu.transform.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(isLoadingGame)
        {
            loadingSlider.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
        }
    }

    public void OpenConfirmationDB()
    {
        confirmationDBox.gameObject.SetActive(true);
    }

    public void CloseConfirmationDB()
    {
        confirmationDBox.gameObject.SetActive(false);
    }

    public void CheckIfSaveData()
    {
        if (hasSaveData)
            OpenConfirmationDB();
        else
            OpenSelectCharacterPanel();
    }

    public void OpenSelectCharacterPanel()
    {
        selectCharacterPanel.gameObject.SetActive(true);
        mainMenu.transform.gameObject.SetActive(false);
    }

    public void StartNewGameBoy()
    {
        GameProperties.isNewGame = true;
        GameProperties.isBoy = true;
        mainMenuController.DeleteSavedGame();
        OpenGameScene();
    }

    public void StartNewGameGirl()
    {
        GameProperties.isNewGame = true;
        GameProperties.isBoy = false;
        mainMenuController.DeleteSavedGame();
        OpenGameScene();
    }

    private void OpenGameScene()
    {
        loadingOperation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        StartLoading();
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game");
        Application.Quit();
    }

    public void SetToContinueGame()
    {
        startButton1.transform.SetParent(mainRect);
        button1Image.sprite = buttonImageMain;
        button1Image.color = buttonColorMain;
        button1Text.color = TextColorMain;
        button1Text.fontSize = fontSizeMain;
        button1Rect.anchoredPosition = Vector2.zero;
        button1Rect.sizeDelta = Vector2.one;
        startButton2.transform.SetParent(minorRect);
        button2Image.sprite = buttonImageMinor;
        button2Image.color = buttonColorMinor;
        button2Text.color = TextColorMinor;
        button2Text.fontSize = fontSizeMinor;
        button2Rect.anchoredPosition = Vector2.zero;
        button2Rect.sizeDelta = Vector2.one;
    }

    public void SetToStartNewGame()
    {
        startButton1.transform.SetParent(minorRect);
        startButton1.enabled = false;
        button1Image.sprite = buttonImageMinor;
        button1Image.color = buttonColorUnavailable;
        button1Text.color = TextColorUnavailable;
        button1Text.fontSize = fontSizeMinor;
        button1Rect.anchoredPosition = Vector2.zero;
        button1Rect.sizeDelta = Vector2.one;
        startButton2.transform.SetParent(mainRect);
        button2Image.sprite = buttonImageMain;
        button2Image.color = buttonColorMain;
        button2Text.color = TextColorMain;
        button2Text.fontSize = fontSizeMain;
        button2Rect.anchoredPosition = Vector2.zero;
        button2Rect.sizeDelta = Vector2.one;
    }

    private IEnumerator FadeInLoadingScreen()
    {
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime;
            loadingCG.alpha = Mathf.Lerp(0, 1, t);
            yield return new WaitForEndOfFrame();
        }

        loadingCG.alpha = 1;
    }
    /*
    public bool CheckSalabyLogin()
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(urlAddress);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

        string dataCheck = "";

        if (response.StatusCode == HttpStatusCode.OK)
        {
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = null;

            if (String.IsNullOrWhiteSpace(response.CharacterSet))
            {
                readStream = new StreamReader(receiveStream);
            }
            else
            {
                readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
            }

            string data = readStream.ReadToEnd();
            dataCheck = data;

            Debug.Log(data);

            response.Close();
            readStream.Close();
        }

        if (dataCheck.Contains(urlAddress))
            return true;
        else
            return false;
    }

    public bool CheckSalabyLogin2()
    {
        using (WebClient client = new WebClient())
        {
            string htmlCode = client.DownloadString(urlAddress);

            Debug.Log(htmlCode);

            if (htmlCode.Contains(urlAddress))
                return true;
            else
                return false;
        }
    }

    public void GetRequestHeader()
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(urlAddress);

        string fetchedRequest = webRequest.GetRequestHeader(urlAddress);

        Debug.Log(fetchedRequest);
    }
    
    private IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            //webRequest.redirectLimit = 0;
            // Request and wait for the desired page.
            string testRequest = webRequest.url;

            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;          

            if (webRequest.isHttpError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
                Debug.Log("FALSE");
            }
            else
            {
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                Debug.Log("TRUE");
            }

            Debug.Log("CHECK:::::");

            Debug.Log(testRequest);
            Debug.Log(webRequest.url);

            string fetchedRequest = webRequest.GetResponseHeader(testRequest);

            Debug.Log(fetchedRequest);
        }
    }
    */
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
