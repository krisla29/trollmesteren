﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSpeed : MonoBehaviour
{
    public Vector3 mouseDelta = Vector3.zero;
    private Vector3 lastPos = Vector3.zero;
    private float counter = 1;

    void Start()
    {
        
    }
    void Update()
    {
        
    }
    public void FixedUpdate()
    {
        mouseDelta = Input.mousePosition - lastPos;
        //print(mouseDelta);
        mouseDelta.x = mouseDelta.x / Screen.width;
        mouseDelta.y = mouseDelta.y / Screen.height;
        if(counter > Time.deltaTime)
        lastPos = Input.mousePosition;
    }
}
