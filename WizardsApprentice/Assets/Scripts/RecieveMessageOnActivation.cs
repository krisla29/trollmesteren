﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecieveMessageOnActivation : MonoBehaviour
{
    public Transform linkedObject;
    [SerializeField]private bool isActivated;
    public MonoBehaviour linkedBehaviour;

    private void OnEnable()
    {
        SendMessageOnActivation.OnSendActivation += RecieveMessage;
    }
    private void OnDisable()
    {
        SendMessageOnActivation.OnSendActivation -= RecieveMessage;
    }
    private void RecieveMessage(Transform sentObject)
    {
        if(sentObject == linkedObject && !isActivated)
        {
            linkedBehaviour.enabled = true;
            isActivated = true;
        }       
    }
}
