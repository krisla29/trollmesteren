﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerEnterTag : MonoBehaviour
{
    public string tag;
    public bool isActivated;
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == (tag))
        {
            isActivated = true;
        }
    }
}
