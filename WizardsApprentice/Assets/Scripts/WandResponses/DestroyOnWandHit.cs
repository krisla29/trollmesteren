﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnWandHit : MonoBehaviour
{
    public WandInteractable wandInteractable;
    public GameObject objectToDestroy;
    public float hitTime = 0;

    private void OnEnable()
    {
        Wand.OnWandInteractableStart += TimerStart;
    }

    private void OnDisable()
    {
        Wand.OnWandInteractableStart -= TimerStart;
        StopAllCoroutines();
    }

    private void TimerStart(Transform recievedTransform)
    {       
        if(recievedTransform == wandInteractable.transform)
        {
            //print("heellooo");
            StartCoroutine(DestroyObject());
        }
    }

    private IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(hitTime);
        CheckIfStillInteracting();
    }
    private void CheckIfStillInteracting()
    {
        if(wandInteractable.isInteracting)
        {
            Destroy(objectToDestroy);
        }
    }

    private void OnDestroy()
    {
        print("destroyed " + objectToDestroy);
        StopAllCoroutines();
    }
}
