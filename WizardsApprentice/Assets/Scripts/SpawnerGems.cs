﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerGems : MonoBehaviour
{
    public GameObject gemPrefab;
    public Transform releaseButton;
    public PlayerStats playerStats;
    public AudioSource playerReleavesGems;
    //public TheWell theWellScript;

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += ReleaseCurrentGems;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= ReleaseCurrentGems;
        StopAllCoroutines();
    }

    void ReleaseCurrentGems(Transform interactable)
    {
        if (interactable == releaseButton)
        {
            if (playerStats.gems > 0)
            {
                for (int i = 0; i < playerStats.gems; i++)
                {
                    Invoke("ReleaseGem", Random.Range(0.3f, 1f));
                }               
            }
        }
    }

    void ReleaseGem()
    {
        playerReleavesGems.Play();
        var randomOffset = new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
        var randomRot = Quaternion.Euler(0, Random.Range(-179f, 179f), 0);

        var gemInstance = Instantiate(gemPrefab, transform.position + randomOffset, randomRot);
        //playerStats.gems -= 1;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
