﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeshCreator : MonoBehaviour
{
    public Button addPointButton;
    public Transform gameTransform;
    public List<Transform> pointObjects = new List<Transform>();
    public Vector3[] pointVertices;
    public int[] triangles;
    public Transform pointsParent;
    public string pointTitleSuffix;
    public GameObject pointPrefab;
    public GameObject meshPrefab;
    public GameObject meshObject;
    public Mesh mesh;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public Material meshMaterial;
    public LineRenderer lineRenderer;

    private void Start()
    {
        mesh = new Mesh();
        meshObject = Instantiate(meshPrefab, gameTransform);
        meshFilter = meshObject.GetComponent<MeshFilter>();
        meshRenderer = meshObject.GetComponent<MeshRenderer>();
        meshMaterial = meshRenderer.material;
        meshFilter.mesh = mesh;
    }

    void AddPoint()
    {
        var newPoint = Instantiate(pointPrefab, pointsParent);
        pointObjects.Add(newPoint.transform);
        newPoint.name = pointTitleSuffix + "_" + pointObjects.Count;
    }

    private void Update()
    {
        UpdateMeshData();
        UpdateMesh();
        UpdateLineRenderer();
    }

    void UpdateMeshData()
    {
        if (pointObjects.Count <= 3)
        {
            pointVertices = new Vector3[pointObjects.Count];
            triangles = new int[pointObjects.Count];
        }
        else
        {
            pointVertices = new Vector3[((pointObjects.Count - 1) * 3) - 3];
            triangles = new int[((pointObjects.Count - 1) * 3) - 3];
        }

        for (int i = 0; i < pointObjects.Count; i++) //Creates a new triangle for each point after first three points:
        {
            if (i < 3)
            {
                pointVertices[i] = pointObjects[i].localPosition;
                triangles[i] = i;
            }
            else
            {

                pointVertices[((i - 1) * 3) - 3] = pointObjects[i - 2].localPosition;
                pointVertices[((i - 1) * 3) - 2] = pointObjects[i].localPosition;
                pointVertices[((i - 1) * 3) - 1] = pointObjects[i - 1].localPosition;

                if (i % 2 != 0)
                {
                    triangles[((i - 1) * 3) - 3] = ((i - 1) * 3) - 1;
                    triangles[((i - 1) * 3) - 2] = ((i - 1) * 3) - 3;
                    triangles[((i - 1) * 3) - 1] = ((i - 1) * 3) - 2;
                }
                else
                {
                    triangles[((i - 1) * 3) - 3] = ((i - 1) * 3) - 2;
                    triangles[((i - 1) * 3) - 2] = ((i - 1) * 3) - 3;
                    triangles[((i - 1) * 3) - 1] = ((i - 1) * 3) - 1;
                }
            }
        }
    }

    void UpdateLineRenderer()
    {
        lineRenderer.positionCount = pointVertices.Length;
        lineRenderer.SetPositions(pointVertices);
    }

    void UpdateMesh()
    {
        mesh.Clear();
        mesh.vertices = pointVertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();
    }

    void Undo()
    {

    }

    void Redo()
    {

    }

    void MovePoint()
    {

    }
}
