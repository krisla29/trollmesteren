﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAfterSeconds : MonoBehaviour
{
    public float lifeTime;
    private float timeStamp;

    void Start()
    {
        timeStamp = Time.time;
    }

    void Update()
    {
        if(lifeTime < Time.time - timeStamp)
        {
            Destroy(transform.gameObject);
        }
    }
}
