﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeGamePanel : MonoBehaviour
{
    public TimeGame timeGame;
    public RectTransform timeGamePanel;
    public RectTransform tasksPanel;
    public RectTransform currentTaskPanel;
    public RectTransform closButton;
    public AudioSource soundOpen;

    private void OnMouseDown()
    {
        OpenPanel();
    }

    public void OpenPanel()
    {
        if (soundOpen)
            soundOpen.Play();

        timeGamePanel.gameObject.SetActive(true);
        timeGame.gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
    }

    public void ClosePanel()
    {
        timeGame.gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        tasksPanel.transform.localScale = Vector3.zero;
        currentTaskPanel.transform.localScale = Vector3.zero;
    }
}
