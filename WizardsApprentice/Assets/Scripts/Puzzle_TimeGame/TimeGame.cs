﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimeGame : MonoBehaviour
{
    public GameManager gameManager;
    public SceneController sceneController;
    public enum TimeOfDay { Night, Day, Evening}
    public TimeOfDay timeOfDay = TimeOfDay.Day;
    private TimeOfDay lastTimeOfDay;
    private float distToStart = 0.1f;
    public InteractableBehaviourRotate2 minuteHand;
    public InteractableBehaviourRotate2 hourHand;
    public InteractableBehaviourRotate2 digit1;
    public InteractableBehaviourRotate2 digit2;
    public InteractableBehaviourRotate2 digit3;
    public InteractableBehaviourRotate2 digit4;
    public InteractableBehaviourRotate2 shadow;
    public InteractableBehaviourRotate2 earth;
    private List<InteractableBehaviourRotate2> handles = new List<InteractableBehaviourRotate2>();
    public Transform moon;
    public SpriteRenderer moonGraphic;
    public SpriteRenderer moonGlow;

    public int[] time = new int[2];
    public float normalizedTime;
    public float radialTime;
    public float offsetNormalizedTime;
    private int[] lastTime = new int[2];

    public enum Operation { Add, Subtract}
   
    public List<TimeGameTask> timeTasks = new List<TimeGameTask>();
    public TimeGameTask currentTimeTask; //ReadOnly
    public int taskCounter;
    public Color colorAdd = Color.red;
    public Color colorSubtract = Color.blue;

    public Transform ui_minuteHand;
    public Transform ui_hourHand;
    public TextMeshProUGUI ui_operationText;
    public Image operationImage;
    public TextMeshProUGUI ui_taskText;
    public TextMeshProUGUI ui_digiClockText;

    public Transform canvasRayBlocker;

    public TimeGamePanel timeGamePanel;
    public CanvasGroup correctTextPanel;
    public TextMeshProUGUI correctText;

    public PlayerStats.Trophy trophy = PlayerStats.Trophy.ClockGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;

    public GameObject gemPrefab;

    public GameObject correctEffect;
    public Transform correctEffectSpawnPoint;
    public Transform effectsContainer;
    //public RectTransform trophyHint;
    public bool someHandlePressed;

    private bool handlesClockPressed;
    private bool handlesDclockPressed;
    private bool handleEarthPressed;
    private bool handleSclockPressed;

    public bool hasWonGame;

    public AudioSource soundCorrect;
    public AudioSource hitTrophyTime;
    public AudioSource soundWinGame;
    public AudioSource soundReleaseGems;
    public AudioSource musicDay;
    public AudioSource musicNight;

    public AudioSource soundLoopClock;
    public AudioSource soundLoopDigiClock;
    public AudioSource soundLoopEarth;
    public AudioSource soundLoopSunClock;

    public List<Transform> ObjectsToActivateOnComplete = new List<Transform>();
    public List<Transform> ObjectsToSleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    public delegate void TimeGameEvent(); //Sends To PlayerStats
    public static event TimeGameEvent OnTimeGameEnable;
    public static event TimeGameEvent OnTimeGameDisable;

    public delegate void TimeEvent(); //Sends to Torch script
    public static event TimeEvent OnMorning;
    public static event TimeEvent OnEvening;
    public static event TimeEvent OnNight;

    public int[] nightTime = new int[2] { 18, 0 };
    public int[] morningTime = new int[2] { 6, 0 };

    public AudioSource soundOwl;
    public AudioSource soundRooster;

    void Awake()
    {
        handles.Add(minuteHand);
        handles.Add(hourHand);
        handles.Add(digit1);
        handles.Add(digit2);
        handles.Add(digit3);
        handles.Add(digit4);
        handles.Add(shadow);
        handles.Add(earth);        
    }

    private void Start()
    {
        for (int i = 0; i < taskCounter; i++)
        {
            timeTasks[i].canvasGroup.alpha = 1;
        }
    }

    private void OnEnable()
    {
        hasWonGame = gameManager.playerStats.hasWonClockGame;
        taskCounter = gameManager.playerStats.clockGameCompletedTasks;

        for (int i = 0; i < timeTasks.Count; i++)
        {
            if (i < taskCounter)
                SetIconToComplete(timeTasks[i]);
            else
                SetIconToIncomplete(timeTasks[i]);
        }
        /*
        if(taskCounter > 0)
        {
            for (int i = 0; i < taskCounter; i++)
            {
                SetIconToComplete(timeTasks[i]);
            }
        }
        */
        if (taskCounter < timeTasks.Count)
            NewTask();

        if (OnTimeGameEnable != null)
            OnTimeGameEnable();

        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        ResetCorrectPanel();
        StopAllCoroutines();
        EmptyEffects();
        ResetInteractionBools();
        DisableSounds();
        currentTimeTask = null;

        if (OnTimeGameDisable != null)
            OnTimeGameDisable();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void UpdateTimeFromClock()
    {
        if (minuteHand.interactable.isPressed)
            UpdateHourHandFromMinutehand();
        else if (hourHand.interactable.isPressed)
            UpdateMinuteHandFromHourHand();

        float t1 = Mathf.FloorToInt(hourHand.stepsFromZero);
        float t2 = Mathf.FloorToInt(minuteHand.stepsFromZero);
        
        if (t2 > 59.9f)
        {
            t2 = 59;            
        }

        if (minuteHand.interactable.isPressed)
        {
            time[0] = minuteHand.cycleCounter;
            time[1] = (int)(t2);
        }
        else if (hourHand.interactable.isPressed)
        {
            if (hourHand.currentAngle > 359 && Input.GetAxis("Mouse X") > 0)
            {

            }
            else if (hourHand.currentAngle > 359 && Input.GetAxis("Mouse X") < 0)
            {

            }
            else
            {
                time[0] = Mathf.FloorToInt(Mathf.Clamp(t1, 0, 55) / 5) + Mathf.FloorToInt(hourHand.cycleCounter * 12);
                time[1] = (int)(t2);
            }
        }        
    }

    void UpdateTimeFromDigiClock()
    {
        float t1 = digit1.stepsFromZeroRound;
        float t2 = digit2.stepsFromZeroRound;

        if (t1 > 2)
            t1 = 0;

        if(t1 > 1)
        {
            digit2.hasMaxSteps = true;
            digit2.maxStepsFromZero = 3;

            if (t2 > 3)
            {
                if(digit1.interactable.isPressed)
                    digit2.UpdateRotationFromAngle(108);

                t2 = 3;
            }
        }
        else
        {
            digit2.hasMaxSteps = false;
            digit2.maxStepsFromZero = 0;

            if (t2 > 9)
                t2 = 0;
        }               

        time[0] = Mathf.FloorToInt(t1 * 10) + Mathf.FloorToInt(t2);

        float t3 = digit3.stepsFromZeroRound;
        float t4 = digit4.stepsFromZeroRound;

        if (t3 > 5)
            t3 = 0;
        if (t4 > 9)
            t4 = 0;

        time[1] = Mathf.FloorToInt(t3 * 10) + Mathf.FloorToInt(t4);
    }

    void UpdateTimeFromSunClock()
    {
        float t = shadow.stepsFromZeroRound;

        if (t > 23)
            t = 0;

        time[0] = Mathf.RoundToInt(t);
    }

    void UpdateTimeFromEarth()
    {
        float t = earth.stepsFromZeroRound;

        if (t > 23)
            t = 0;

        time[0] = Mathf.RoundToInt(t);
    }

    private void UpdateHourHandFromMinutehand()
    {
        float angle = (minuteHand.cycleCounter * 30) + (minuteHand.currentAngle / 12);
        hourHand.cycleCounter = Mathf.FloorToInt(time[0] / 12);
        hourHand.UpdateRotationFromAngle(angle);
    }

    private void UpdateMinuteHandFromHourHand()
    {
        float angle = (hourHand.currentAngle * 12);
        minuteHand.cycleCounter = time[0];
        minuteHand.UpdateRotationFromAngle(angle);
    }

    void UpdateClockFromTime()
    {
        float minuteAngle = time[1] * 6;
        minuteHand.UpdateRotationFromAngle(minuteAngle);
        minuteHand.cycleCounter = time[0];

        UpdateHourHandFromMinutehand();
    }

    void UpdateDigiClockFromTime()
    {
        float angle1 = (Mathf.FloorToInt(time[0] / 10)) * 120;
        digit1.UpdateRotationFromAngle(angle1);
        float angle2 = time[0] * 36;
        digit2.UpdateRotationFromAngle(angle2);
        float angle3 = (Mathf.FloorToInt(time[1] / 10)) * 60;
        digit3.UpdateRotationFromAngle(angle3);
        float angle4 = time[1] * 36;
        digit4.UpdateRotationFromAngle(angle4);
    }

    void UpdateSunClockFromTime()
    {
        float angle = time[0] * 15;
        shadow.UpdateRotationFromAngle(angle);
    }

    void UpdateEarthFromTime()
    {
        float angle = time[0] * 15;
        earth.UpdateRotationFromAngle(angle);
    }

    private void Update()
    {
        if(minuteHand.interactable.isPressed || hourHand.interactable.isPressed)
        {
            UpdateTimeFromClock();

            UpdateDigiClockFromTime();
            UpdateEarthFromTime();
            UpdateSunClockFromTime();
        }
        else if(digit1.interactable.isPressed || digit2.interactable.isPressed || digit3.interactable.isPressed || digit4.interactable.isPressed)
        {
            UpdateTimeFromDigiClock();

            UpdateClockFromTime();
            UpdateEarthFromTime();
            UpdateSunClockFromTime();
        }
        else if(shadow.interactable.isPressed)
        {
            UpdateTimeFromSunClock();

            UpdateClockFromTime();
            UpdateDigiClockFromTime();
            UpdateEarthFromTime();
        }
        else if(earth.interactable.isPressed)
        {
            UpdateTimeFromEarth();

            UpdateClockFromTime();
            UpdateDigiClockFromTime();
            UpdateSunClockFromTime();
        }

        CheckIfHandlesPressed();
        UpdateNormalizedTime();

        UpdateLight();
        UpdateSky();
        UpdateFog();
        UpdateMoon();
        UpdateTimeOfDay();
        UpdateWorldGems();
        UpdateSounds();

        if (!hasWonGame)
            CheckCurrentTask();       

        if (time[0] == nightTime[0] && time[1] < 5 && someHandlePressed)
        {
            if (soundOwl)
            {
                if(!soundOwl.isPlaying)
                    soundOwl.Play();
            }
        }

        if (time[0] == morningTime[0] && time[1] < 5 && someHandlePressed)
        {
            if (soundRooster)
            {
                if(!soundRooster.isPlaying)
                    soundRooster.Play();
            }
        }

        lastTime = time;
        lastTimeOfDay = timeOfDay;
        //CheckLastDestination();
    }

    void UpdateNormalizedTime()
    {
        normalizedTime = (time[0] / 24f) + ((time[1] / 60f) / 24);
        radialTime = normalizedTime * 360;

        float tempOffset;

        if (normalizedTime <= 0.5f)
            tempOffset = normalizedTime;
        else
            tempOffset = 1 - normalizedTime;

        offsetNormalizedTime = tempOffset * 2;
    }

    void CheckIfHandlesPressed()
    {
        bool check = false;

        for (int i = 0; i < handles.Count; i++)
        {
            if (handles[i].interactable.isPressed)
            {
                if (handles[i] == minuteHand || handles[i] == hourHand)
                    handlesClockPressed = true;                

                if (handles[i] == digit1 || handles[i] == digit2 || handles[i] == digit3 || handles[i] == digit4)
                    handlesDclockPressed = true;
               
                if (handles[i] == shadow)
                    handleSclockPressed = true;              

                if (handles[i] == earth)
                    handleEarthPressed = true;
              
                check = true;
            }            
        }

        if (check)
            someHandlePressed = true;
        else
        {
            someHandlePressed = false;
            handlesClockPressed = false;
            handlesDclockPressed = false;
            handleSclockPressed = false;
            handleEarthPressed = false;
        }
    }

    void ResetInteractionBools()
    {
        someHandlePressed = false;
        handlesClockPressed = false;
        handlesDclockPressed = false;
        handleSclockPressed = false;
        handleEarthPressed = false;
    }

    void CheckSleepAnim()
    {
        if(timeOfDay == TimeOfDay.Night)
        {
            if((gameManager.player.transform.position - sceneController.startPos.position).sqrMagnitude < distToStart)
            {
                if (!gameManager.player.anim.GetBool("Sleep"))
                    gameManager.player.anim.SetBool("Sleep", true);
            }
            else
            {
                if (gameManager.player.anim.GetBool("Sleep"))
                    gameManager.player.anim.SetBool("Sleep", false);

                print("Setting to false");
            }
        }
        else
        {
            if(gameManager.player.anim.GetBool("Sleep"))
                gameManager.player.anim.SetBool("Sleep", false);
        }
    }

    void CheckReturnToStartPos()
    {
        if(timeOfDay == TimeOfDay.Night)
        {
            if (gameManager.player.navMeshAgent.remainingDistance < 0.1f && (gameManager.player.transform.position - sceneController.startPos.position).sqrMagnitude > distToStart)
            {
                StartCoroutine(WaitAndReturnToStartPos());
            }
        }
    }

    private IEnumerator WaitAndReturnToStartPos()
    {
        yield return new WaitForSeconds(3);

        if (gameManager.player.navMeshAgent.remainingDistance < 0.1f && (gameManager.player.transform.position - sceneController.startPos.position).sqrMagnitude > distToStart)
        {
            gameManager.player.navMeshAgent.SetDestination(sceneController.startPos.position);
        }
    }
    /*
    void CheckLastDestination()
    {
        if (gameManager.player.navMeshAgent.hasPath)
            lastDestination = gameManager.player.navMeshAgent.destination;
    }
    */
    void FixedUpdate()
    {
        CheckSleepAnim();
        CheckReturnToStartPos();
    }

    void UpdateLight()
    {
        var rotOffset = gameManager.worldLightStartRotation;

        gameManager.worldLight.transform.localEulerAngles = rotOffset + new Vector3(0, radialTime - 180, 0);

        var intensity = gameManager.worldLightStartIntensity * offsetNormalizedTime;

        gameManager.worldLight.intensity = intensity;
    }

    void UpdateSky()
    {
        float intensity;

        if(normalizedTime >= 0.5f)
            intensity = gameManager.worldSkyboxStartExposure * Mathf.Clamp(offsetNormalizedTime + 0.3f, 0.2f, 1);
        else
            intensity = gameManager.worldSkyboxStartExposure * Mathf.Clamp(offsetNormalizedTime, 0.3f, 1);

        gameManager.worldSkyboxClone.SetFloat("_Exposure", intensity);
    }

    void UpdateFog()
    {
        Color fogColor;

        if (normalizedTime >= 0.5f)
            fogColor = gameManager.fogStartColor * Mathf.Clamp(offsetNormalizedTime + 0.3f, 0.2f, 1);
        else
            fogColor = gameManager.fogStartColor * Mathf.Clamp(offsetNormalizedTime, 0.3f, 1);

        fogColor.a = 1;

        RenderSettings.fogColor = fogColor;
    }

    void UpdateMoon()
    {
        float rotY;
        rotY = radialTime * 0.5f;

        if (radialTime > 180)
            rotY = (radialTime + 360) * 0.5f;

        float rotX = (90f * (1 - offsetNormalizedTime)) / 2;

        if (normalizedTime > 0.70f || normalizedTime < 0.3f)
            moon.gameObject.SetActive(true);
        else
            moon.gameObject.SetActive(false);

        moon.parent.localEulerAngles = new Vector3(0, rotY, 0);
        moon.parent.parent.localPosition = new Vector3(0, -offsetNormalizedTime * 30, 0);

        if (normalizedTime > 0.7f)
        {
            moonGraphic.color = new Color(moonGraphic.color.r, moonGraphic.color.g, moonGraphic.color.b, -1.4f + (normalizedTime * 2));
            moonGlow.color = new Color(moonGlow.color.r, moonGlow.color.g, moonGlow.color.b, -0.35f + (normalizedTime * 0.5f) - 0.05f);           
        }
        else if(normalizedTime < 0.3f)
        {
            moonGraphic.color = new Color(moonGraphic.color.r, moonGraphic.color.g, moonGraphic.color.b, 0.6f - (normalizedTime * 2));
            moonGlow.color = new Color(moonGlow.color.r, moonGlow.color.g, moonGlow.color.b, 0.1f - (normalizedTime * 0.5f));
        }

        if (gameManager.starskyMatClone)
        {
            float t = 0.05f - (offsetNormalizedTime * 0.1f);
            gameManager.starskyMatClone.SetColor("_TintColor", new Color(1, 1, 1, Mathf.Clamp(t, 0f, 0.1f)));
        }
    }

    void UpdateTimeOfDay()
    {
        if ((time[0] < 6 || time[0] >= 18))
        {
            if(gameManager.soundTrack == musicDay)
            {
                gameManager.soundTrack.Stop();
                gameManager.soundTrack = musicNight;
                gameManager.soundTrack.Play();
            }
            else
            {
                if (!gameManager.soundTrack.isPlaying)
                    gameManager.soundTrack.Play();
            }

            if (time[0] >= 18 && time[0] < 21)
            {
                if (timeOfDay != TimeOfDay.Evening)
                {
                    if (OnEvening != null)
                        OnEvening();
                }

                timeOfDay = TimeOfDay.Evening;
            }
            if(time[0] >= 21 || time[0] < 6)
            {
                if (timeOfDay != TimeOfDay.Night)
                {
                    if (OnNight != null)
                        OnNight();
                }

                timeOfDay = TimeOfDay.Night;
            }
        }
        else
        {
            if (gameManager.soundTrack == musicNight)
            {
                gameManager.soundTrack.Stop();
                gameManager.soundTrack = musicDay;
                gameManager.soundTrack.Play();               
            }
            else
            {
                if (!gameManager.soundTrack.isPlaying)
                    gameManager.soundTrack.Play();
            }

            if(timeOfDay != TimeOfDay.Day)
            {
                if (OnMorning != null)
                    OnMorning();
            }

            timeOfDay = TimeOfDay.Day;
        }
    }

    void UpdateWorldGems()
    {
        if (lastTimeOfDay != timeOfDay)
        {
            SetGems();
        }
    }

    void SetGems()
    {
        if (timeOfDay != TimeOfDay.Night)
        {
            if (!gameManager.playerStats.worldGemsParent.gameObject.activeSelf)
                gameManager.playerStats.worldGemsParent.gameObject.SetActive(true);
            if (gameManager.playerStats.nightGemsParent.gameObject.activeSelf)
                gameManager.playerStats.nightGemsParent.gameObject.SetActive(false);
        }
        else
        {
            if (gameManager.playerStats.worldGemsParent.gameObject.activeSelf)
                gameManager.playerStats.worldGemsParent.gameObject.SetActive(false);
            if (!gameManager.playerStats.nightGemsParent.gameObject.activeSelf)
                gameManager.playerStats.nightGemsParent.gameObject.SetActive(true);
        }
    }

    public void SetTimeAndUpdateAll(int[] passedTime)
    {
        time = passedTime;
        UpdateNormalizedTime();
        UpdateClockFromTime();
        UpdateDigiClockFromTime();
        UpdateEarthFromTime();
        UpdateSunClockFromTime();
        UpdateLight();
        UpdateSky();
        UpdateFog();
        UpdateMoon();

        if(gameManager.playerStats.hasSeenOpening)
            UpdateTimeOfDay();

        SetGems();
        SendTimeEvent();
    }

    void SendTimeEvent()
    {
        if(time[0] >= 6 && time[0] < 18)
        {
            if (OnMorning != null)
                OnMorning();
        }
        if(time[0] >= 18 && time[0] < 21)
        {
            if (OnEvening != null)
                OnEvening();
        }
        if(time[0] >= 21 || time[0] < 6)
        {
            if (OnNight != null)
                OnNight();
        }
    }

    void CheckCurrentTask()
    {
        if (time[0] == currentTimeTask.correctTime.x && time[1] == currentTimeTask.correctTime.y && !someHandlePressed)
        {
            if (soundCorrect)
            {
                if (!soundCorrect.isPlaying)
                    soundCorrect.Play();

                taskCounter++;
                gameManager.playerStats.clockGameCompletedTasks++;

                if (taskCounter < timeTasks.Count)
                {
                    StartCoroutine(CorrectAnswer(currentTimeTask.correctTime));
                    NewTask();
                }
                else
                    StartWinGame();
            }
        }
    }

    private IEnumerator CorrectAnswer(Vector2Int time)
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        canvasRayBlocker.gameObject.SetActive(true);

        var effect = Instantiate(correctEffect, correctEffectSpawnPoint.position, correctEffectSpawnPoint.rotation, effectsContainer);

        yield return new WaitForSeconds(1);      

        string h = time.x.ToString();

        if (h.Length < 2)
            h = "0" + h;

        string m = time.y.ToString();

        if (m.Length < 2)
            m = "0" + m;

        correctText.text = h + ":" + m;
        correctTextPanel.gameObject.SetActive(true);

        yield return new WaitForSeconds(1);

        gameManager.player.anim.SetTrigger("Success");

        yield return new WaitForSeconds(1);

        if (soundReleaseGems)
            soundReleaseGems.Play();

        for (int i = 0; i < 3; i++)
        {
            var gem = Instantiate(gemPrefab, correctEffectSpawnPoint.position, Quaternion.identity, null);
            yield return new WaitForSeconds(0.3f);
            StartCoroutine(MoveGem(gem));
        }

        yield return new WaitForSeconds(4);

        float t = 0;
        float fadeTime = 1;

        while(t < 1)
        {
            t += Time.deltaTime / fadeTime;
            correctTextPanel.alpha = Mathf.Lerp(1, 0, t);
            yield return new WaitForEndOfFrame();
        }

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        canvasRayBlocker.gameObject.SetActive(false);

        ResetCorrectPanel();
        timeGamePanel.OpenPanel();
    }

    private IEnumerator MoveGem(GameObject passedGem)
    {
        float x = 0;
        float gemTravelTime = 3;

        while (x < 1)
        {
            x += Time.deltaTime / gemTravelTime;
            var u = LerpUtils.SmootherStep(x);
            passedGem.transform.position = Vector3.Lerp(correctEffectSpawnPoint.position, gameManager.player.transform.position + Vector3.up, u);
            var y = Mathf.Sin(u * (Mathf.PI * 4));
            passedGem.transform.position += new Vector3(0, y, 0);
            yield return new WaitForEndOfFrame();
        }

        gameManager.playerStats.AddGem(1);
        Destroy(passedGem);
    }

    void ResetCorrectPanel()
    {
        correctTextPanel.transform.localScale = Vector3.zero;
        correctTextPanel.gameObject.SetActive(false);
        correctTextPanel.alpha = 1;
    }

    void NewTask()
    {
        if(currentTimeTask)
        {
            SetIconToComplete(currentTimeTask);
        }

        currentTimeTask = timeTasks[taskCounter];

        currentTimeTask.canvasGroup.alpha = 1;
        currentTimeTask.scalePulser.enabled = true;

        if (currentTimeTask.overrideRotation)
        {
            ui_minuteHand.localEulerAngles = new Vector3(0, 0, currentTimeTask.minuteRot);
            ui_hourHand.localEulerAngles = new Vector3(0, 0, currentTimeTask.hourRot);
        }
        else
            SetClockHandRots();

        if (currentTimeTask.operation == Operation.Add)
        {
            ui_operationText.text = "+";
            //ui_operationText.rectTransform.rect.yMin = new Rect(0, 0, 0, 0);
            ui_operationText.color = colorAdd;
            operationImage.color = Color.white + colorAdd;
        }
        else
        {
            ui_operationText.text = "-";
            ui_operationText.color = colorSubtract;
            operationImage.color = Color.white + colorAdd;
        }

        ui_taskText.text = currentTimeTask.taskText;
        SetUiDigiClockText();
        gameManager.playerStats.SaveGame();
    }

    void SetClockHandRots()
    {
        /*
        if(currentTimeTask.clockTime.y <= 30)
            ui_minuteHand.localEulerAngles = new Vector3(0, 0, -currentTimeTask.clockTime.y * 6);
        else
            ui_minuteHand.localEulerAngles = new Vector3(0, 0, (currentTimeTask.clockTime.y * 6) - 180);

        if(currentTimeTask.clockTime.x <= 6 ||
            currentTimeTask.clockTime.x > 12 && currentTimeTask.clockTime.x <= 18)
            ui_hourHand.localEulerAngles = new Vector3(0, 0, (-currentTimeTask.clockTime.x * 30f) + (-currentTimeTask.clockTime.y * 0.5f));
        else
            ui_hourHand.localEulerAngles = new Vector3(0, 0, (currentTimeTask.clockTime.x * 30f) + (currentTimeTask.clockTime.y * 0.5f) - 180);
        */

        ui_minuteHand.localEulerAngles = new Vector3(0, 0, 360 - (currentTimeTask.clockTime.y * 6));

        if(currentTimeTask.clockTime.x <= 12)
            ui_hourHand.localEulerAngles = new Vector3(0, 0, 360 - ((currentTimeTask.clockTime.x * 30f) + (currentTimeTask.clockTime.y * 0.5f)));
        else
            ui_hourHand.localEulerAngles = new Vector3(0, 0, 360 - (((currentTimeTask.clockTime.x - 12) * 30f) + (currentTimeTask.clockTime.y * 0.5f)));
    }

    void SetUiDigiClockText()
    {
        string h = currentTimeTask.clockTime.x.ToString();

        if (h.Length < 2)
            h = "0" + h;

        string m = currentTimeTask.clockTime.y.ToString();

        if (m.Length < 2)
            m = "0" + m;

        //currentTimeTask.digiClockText = h + ":" + m;
        ui_digiClockText.text = h + ":" + m; 
    }

    void EmptyEffects()
    {
        foreach (Transform child in effectsContainer)
        {
            Destroy(child.gameObject);
        }
    }

    void DisableSounds()
    {
        if(soundLoopClock)
        {
            if (soundLoopClock.isPlaying)
                soundLoopClock.Stop();
        }
        if (soundLoopDigiClock)
        {
            if (soundLoopDigiClock.isPlaying)
                soundLoopDigiClock.Stop();
        }
        if (soundLoopEarth)
        {
            if (soundLoopEarth.isPlaying)
                soundLoopEarth.Stop();
        }
        if (soundLoopSunClock)
        {
            if (soundLoopSunClock.isPlaying)
                soundLoopSunClock.Stop();
        }
    }

    void UpdateSounds()
    {
        if(handlesClockPressed)
        {
            if (!soundLoopClock.isPlaying)
                soundLoopClock.Play();
        }
        else
        {
            if (soundLoopClock.isPlaying)
                soundLoopClock.Stop();
        }

        if (handlesDclockPressed)
        {
            if (!soundLoopDigiClock.isPlaying)
                soundLoopDigiClock.Play();
        }
        else
        {
            if (soundLoopDigiClock.isPlaying)
                soundLoopDigiClock.Stop();
        }

        if (handleSclockPressed)
        {
            if (!soundLoopSunClock.isPlaying)
                soundLoopSunClock.Play();
        }
        else
        {
            if (soundLoopSunClock.isPlaying)
                soundLoopSunClock.Stop();
        }

        if (handleEarthPressed)
        {
            if (!soundLoopEarth.isPlaying)
                soundLoopEarth.Play();
        }
        else
        {
            if (soundLoopEarth.isPlaying)
                soundLoopEarth.Stop();
        }
    }

    void SetIconToComplete(TimeGameTask task)
    {
        task.correctBg.gameObject.SetActive(true);
        task.checkMarkGrp.gameObject.SetActive(true);
        task.scalePulser.enabled = false;
        task.scalePulser.transform.localScale = Vector3.one;
        task.canvasGroup.alpha = 1f;
    }

    void SetIconToIncomplete(TimeGameTask task)
    {
        task.correctBg.gameObject.SetActive(false);
        task.checkMarkGrp.gameObject.SetActive(false);
        task.scalePulser.enabled = false;
        task.scalePulser.transform.localScale = Vector3.one;
        task.canvasGroup.alpha = 0.2f;
    }

    void StartWinGame()
    {
        if (hitTrophyTime)
            hitTrophyTime.Play();

        StartCoroutine(WinGame());

        foreach (var item in ObjectsToActivateOnComplete)
        {
            item.gameObject.SetActive(true);
        }

        foreach (var item in ObjectsToSleepOnComplete)
        {
            item.gameObject.SetActive(false);
        }

        hasWonGame = true;
        gameManager.playerStats.hasWonClockGame = true;
        gameManager.playerStats.SaveGame();
    }

    private IEnumerator WinGame()
    {
        yield return new WaitForSeconds(1);
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        trophyPanel.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        if (soundWinGame)
            soundWinGame.Play();
        trophyGraphic.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);        
        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(2);
        gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
        trophyGraphic.gameObject.SetActive(false);
        trophyPanel.gameObject.SetActive(false);
        yield return new WaitForSeconds(3);
        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
        gameManager.playerStats.trophies.Add(trophy);       
        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        gameManager.playerStats.SaveGame();
    }    
}
