﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeGameTask : MonoBehaviour
{
    public CanvasGroup canvasGroup;
    public RectTransform correctBg;
    public RectTransform checkMarkGrp;
    public EasyScalePulse scalePulser;
    public Vector2Int clockTime = new Vector2Int();
    //public string digiClockText;
    public Vector2Int correctTime = new Vector2Int();
    public bool overrideRotation;
    public float minuteRot = 0;
    public float hourRot = 0;
    public TimeGame.Operation operation = TimeGame.Operation.Add;
    public string taskText;
    public Transform icon_minuteHand;
    public Transform icon_hourHand;
    public TextMeshProUGUI icon_operationText;
    public TextMeshProUGUI icon_taskText;
}
