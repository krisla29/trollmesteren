﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMessageOnActivation : MonoBehaviour
{
    private bool isFinished = false;
    public delegate void SendActivation(Transform linkedObject);
    public static event SendActivation OnSendActivation;

    public bool triggerOnce;
    public List<Transform> linkedObjects = new List<Transform>();
    public AudioSource audioSource;
    public Collider triggerObject;
    private bool isTriggered;

    private void OnEnable()
    {
        PlayerController.OnPlayerTrigger += TriggerActivation;
    }
    private void OnDisable()
    {
        PlayerController.OnPlayerTrigger -= TriggerActivation;
    }
    private void TriggerActivation(Transform transform)
    {
        if (transform == triggerObject.transform && !isFinished)
        {
            isTriggered = true;

            if (linkedObjects.Count > 0)
            {
                foreach (var item in linkedObjects)
                {
                    if (!item.gameObject.activeSelf)
                    {
                        item.gameObject.SetActive(true);
                    }
                    else
                    {
                        item.gameObject.SetActive(false);
                    }
                }
            }
            if (audioSource)
            {
                audioSource.Play();
            }

            if (triggerOnce)
            {
                isFinished = true;
            }

            isTriggered = false;
        }
    }
}
