﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGameExit : MonoBehaviour
{
    public MazeGame mazeGameScript;

    private void OnMouseDown()
    {
        if (mazeGameScript.gameObject.activeSelf)
            mazeGameScript.ExitScene();
    }
}
