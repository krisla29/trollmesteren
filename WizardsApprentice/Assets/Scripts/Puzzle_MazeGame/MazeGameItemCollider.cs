﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGameItemCollider : MonoBehaviour
{
    public MazeGameItem mgItem;
    public Collider thisCollider;
    private bool isFirstClick;

    private void OnMouseDown()
    {
        if (!mgItem.interactableDragToPoints.lockBehaviour)
        {
            if (!isFirstClick)
            {
                bool startParentCheck = false;

                if (mgItem.transform.parent == mgItem.mazeGameScript.startSlotsParent.transform)
                {
                    startParentCheck = true;
                    mgItem.LeaveStartParent();                    
                }

                if (startParentCheck)
                {
                    mgItem.InitiatlizeItem();
                }

                isFirstClick = true;
            }

            //mgItem.mazeGameScript.DisableTargets();
            
            if (mgItem.mazeGameScript.currentGameItem && mgItem.mazeGameScript.currentGameItem != mgItem)
            {
                mgItem.mazeGameScript.currentGameItem.DeselectCurrentGameItem();
            }
            
            mgItem.mazeGameScript.currentGameItem = mgItem;

            if (!mgItem.itemOnTop)
            {
                mgItem.SetSelected(true);
            }            
        }
    }

    private void OnMouseUp()
    {
        if (!mgItem.interactableDragToPoints.lockBehaviour)
        {
            mgItem.UpdateNavMesh();
            mgItem.mazeGameScript.CheckReachable();
        }
    }    
}
