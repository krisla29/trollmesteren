﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MazeGameButtons : MonoBehaviour
{
    public delegate void MazeGameButtonEvents(); //Sends to AreaVolumeGame
    public static event MazeGameButtonEvents OnAddButtonClick;
    public static event MazeGameButtonEvents OnSubtractButtonClick;
    public static event MazeGameButtonEvents OnDestroyButtonClick;

    public void AddClick()
    {
        if (OnAddButtonClick != null)
        {
            OnAddButtonClick();
        }
    }

    public void SubtractClick()
    {
        if (OnSubtractButtonClick != null)
        {
            OnSubtractButtonClick();
        }
    }

    public void DestroyVoxels()
    {
        if (OnDestroyButtonClick != null)
        {
            OnDestroyButtonClick();
        }
    }
}
