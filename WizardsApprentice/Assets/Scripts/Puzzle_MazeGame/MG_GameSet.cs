﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MG_GameSet : MonoBehaviour
{
    public bool showAddItems = true;
    public bool showDelete = true;
    public bool showRestart = true;

    public int resources;
    public bool freezeItems = true;
    [System.Serializable]
    public struct AvailableItem
    {
        public MazeGameItem item;
        public int cost;
    }
    public List<AvailableItem> availableItems = new List<AvailableItem>();
    [System.Serializable]
    public struct LevelItem
    {
        public MazeGameItem item;
        public List<Vector3> coordinates;
        public List<MazeGame.ItemRotation> rotations;
        public bool isDestroyable;
        public bool isStatic;
        public bool isBlocker;
    }
    public List<LevelItem> levelItems = new List<LevelItem>();
    [System.Serializable]
    public struct Goal
    {
        public int reward;
        public Vector3 position;
    }
    public List<Goal> goals = new List<Goal>();
    [System.Serializable]
    public struct Treasure
    {
        public int reward;
        public Vector3 position;
    }
    public List<Treasure> treasures = new List<Treasure>();

    public Vector3Int bonusLimits = new Vector3Int(100, 75, 50);

}
