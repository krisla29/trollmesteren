﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MazeGameCamera : MonoBehaviour
{
    public PlayerReference playerReferenceScript;
    public MazeGame MazeGame;
    public Transform gamePivot;
    public float speed = 3f;
    public Transform originalParent;
    public GameObject tempCamParentPrefab;
    public GameObject tempParent;
    public Transform tempParentChild;
    public Transform player;
    public RotationSlider rotationSlider;
    public Slider zoomSlider;
    public Follower followerScript;
    private float startHeight;
    private Vector3 playerStartPos;
    private float camOffset = -5f;
    private float startPosZ;
    private float startPosY;
    public bool isResetting;
    private Coroutine resetRot;
    
    private void Awake()
    {
        startPosZ = transform.localPosition.z;
        startPosY = transform.localPosition.y;
    }

    private void OnEnable()
    {
        if (playerReferenceScript.player)
            player = playerReferenceScript.player;

        originalParent = transform.parent;

        if (!tempParent)
            //tempParent = new GameObject("tempSceneCamParent");
            tempParent = Instantiate(tempCamParentPrefab);

        followerScript = tempParent.GetComponent<Follower>();
        tempParent.transform.SetParent(originalParent);
        tempParent.transform.position = gamePivot.position;
        tempParent.transform.rotation = gamePivot.rotation;
        tempParentChild = tempParent.transform.GetChild(0);
        followerScript.enabled = true;
        
        startHeight = tempParent.transform.position.y;       
        transform.SetParent(tempParentChild.transform);

        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;

        if (player)
        {
            playerStartPos = player.transform.position;
        }

        rotationSlider.rotateSlider.interactable = true;
        zoomSlider.interactable = true;
    }

    private void OnDisable()
    {
        Invoke("ReparentAndDestroy", 0);
        StopAllCoroutines();   
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void ReparentAndDestroy()
    {
        if (tempParent)
        {
            if (transform.parent.parent == tempParent.transform)
                transform.SetParent(originalParent);

            Destroy(tempParent);
        }
    }

    private void Update()
    {
        //LookAtPlayerReverse();
        Zoom();
        //TranslateHeight();
    }

    private void LateUpdate()
    {
        if (!isResetting)
            RotateParent();
    }

    void LookAtPlayerReverse()
    {
        if (tempParent && player)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            //var newRot = Quaternion.Slerp(tempParent.transform.rotation, Quaternion.LookRotation(gamePivot.position - player.position), smoothLerp);
            var newRot = Quaternion.LookRotation(player.position - gamePivot.position, Vector3.up);
            var oldRot = tempParent.transform.rotation;
            var newRotEuler = newRot.eulerAngles;
            var oldRotEuler = oldRot.eulerAngles;
            var rotYLerp = Mathf.Lerp(oldRotEuler.y, newRotEuler.y, smoothLerp);
            tempParent.transform.rotation = Quaternion.Euler(0, rotYLerp, 0);

        }
    }
    
    void TranslateHeight()
    {
        if (player)
        {
            var newPos = new Vector3(tempParent.transform.localPosition.x, (player.transform.position.y - playerStartPos.y), tempParent.transform.localPosition.z);
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var pos = Vector3.Lerp(tempParent.transform.localPosition, newPos, smoothLerp);
            tempParent.transform.localPosition = pos;
        }
    }

    void TranslatePlane()
    {
        if(player)
        {
            var newPos = new Vector3(tempParent.transform.localPosition.x, (player.transform.position.y - playerStartPos.y), tempParent.transform.localPosition.z);
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var pos = Vector3.Lerp(tempParent.transform.localPosition, newPos, smoothLerp);
            tempParent.transform.localPosition = pos;
        }
    }
    
    void ReferenceNewPlayer(Transform newPlayer)
    {
        player = newPlayer;
        playerStartPos = player.transform.position;
    }

    void RotateParent()
    {       
        if (rotationSlider.rotateSlider.gameObject.activeSelf)
        {            
            if (rotationSlider.rotateSlider)
            {               
                if (rotationSlider.GetRotateHandle())
                {
                    transform.parent.localEulerAngles += new Vector3(0, (-rotationSlider.Rotate() * 2f), 0);                    
                } 
                
                else if (rotationSlider.isRotatingBack)
                {
                    var locRot = transform.parent.localEulerAngles;
                    //transform.parent.localEulerAngles = new Vector3(0, Mathf.Lerp(rot.y, 0, Time.deltaTime), 0);
                    transform.parent.localRotation = Quaternion.Lerp(transform.parent.localRotation, Quaternion.Euler(0,0,0), Time.deltaTime);
                }
                
            }
        }
    }
    void Zoom()
    {
        var posZ = startPosZ * zoomSlider.value;
        var posY = startPosY * zoomSlider.value;
        transform.localPosition = new Vector3(transform.localPosition.x, posY, posZ);
        //transform.
    }
    
    public void RotateBack()
    {
        isResetting = true;

        if (resetRot != null)
            StopCoroutine(resetRot);

        resetRot = StartCoroutine(ResetRotation());
    }

    private IEnumerator ResetRotation()
    {
        rotationSlider.rotateSlider.interactable = false;
        zoomSlider.interactable = false;

        float t = 0;
        float animTime = 3f;
        float rotSliderVal = rotationSlider.rotateSlider.value;
        float zoomSliderVal = zoomSlider.value;

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            transform.parent.localRotation = Quaternion.Slerp(transform.parent.localRotation, Quaternion.Euler(0, 0, 0), h);
            rotationSlider.rotateSlider.value = Mathf.Lerp(rotSliderVal, 0.5f, h);
            zoomSlider.value = Mathf.Lerp(zoomSliderVal, 1, h);
            yield return new WaitForEndOfFrame();
        }

        rotationSlider.rotateSlider.interactable = true;
        zoomSlider.interactable = true;
        isResetting = false;
    }
}
