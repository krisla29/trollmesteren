﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MG_Goal : MonoBehaviour
{
    public int reward = 100;
    public TextMeshPro rewardText;

    public delegate void MGGoalEvent(MG_Goal thisGoal); //Sends To MazeGame
    public static event MGGoalEvent OnHitPlayer;

    private void Start()
    {
        rewardText.text = reward.ToString();    
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (OnHitPlayer != null)
                OnHitPlayer(this);
        }
    }
}
