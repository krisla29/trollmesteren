﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class MazeGame : MonoBehaviour, IDifficulty, ISetDifficulty, IAchievementPanel
{
    public GameManager gameManager;
    public PlayerStats.Difficulty mazeGameDifficulty = PlayerStats.Difficulty.Normal;
    private int correctStreakCounter;
    private int wrongStreakCounter;
    public int numberOfCorrects = 5;
    public int numberOfWrongs = 3;
    public AchievementPanel achievementPanel;
    public Transform startDestination;
    public Camera cam;
    public MazeGameCamera mazeGameCamera;
    public Toggle lockPlayerToggle;
    public ToggleSwapIcons lockPlayerToggleSwap;
    public Image modeVignette;
    public MG_ModeIndicator modeIndicator;
    public CanvasGroup addButtonLock;
    public CanvasGroup deleteButtonLock;
    public NavMeshSurface gameNavMesh;
    public Transform worldTransform;
    public PlayerReference playerReference;
    public NavMeshAgent playerAgent;
    public MazeGameCanvas mazeGameCanvas;
    public Canvas extraCanvas;
    public CanvasGroup animatedNumberParent;
    public Text animatedNumberText;
    public Image animatedNumberTextBg;
    public Transform resourcesTarget;
    public RectTransform setCountPanel;
    public Image setCountImage;
    public TextMeshProUGUI setCountPanelText;
    public GameObject gemPhysicalPrefab;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.MazeGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;
    public Transform freezer;
    public Transform freezerFollower;
    public SpriteRenderer freezerLight;
    public float freezerHeight = -1f;
    public bool drawGizmos;
    public GameObject birthEffectPrefab;
    public GameObject deathEffectPrefab;
    public List<Transform> newItemsStartSlots = new List<Transform>();
    public Follower startSlotsParent;
    public GameObject targetPrefab;
    public MG_PlayerLock playerLockPrefab;
    public MG_PlayerLock currentPlayerLock;
    public bool lockPlayer;
    private float playerSpeed;
    private PlayerMovementAgent playerMovementAgentScript;
    public List<Transform> targets = new List<Transform>();
    public List<GameObject> mazeGamePrefabs = new List<GameObject>();
    public List<Transform> snapPoints = new List<Transform>();

    private const int agentNavMeshID = -1372625422;

    public Transform gridPointsContainer;
    public List<MazeGameItem> currentGameItems = new List<MazeGameItem>();
    public Transform itemsContainer;
    public MazeGameItem currentGameItem;
    public MazeGameItem lastGameItem;
    public MazeGameItem nextGameItem;
    public MazeGameItem playerGameItem;
    public int itemLimit;

    public Material currentMaterial;
    public Material origMaterial;

    public Material materialStatic;
    public Material materialStaticWalkable;
    public Material materialBlocker;

    public bool isInitialized;
    public bool playerIsAtGoal;

    public delegate void MazeGameEvent(Transform currentItem);
    public static event MazeGameEvent OnPlayerReachesGoal; //Sends to MazeGameItemScript
    public delegate void MazeGameResourcesEvent(int amaountToAdd); //Sends To MazeGameCanvas
    public static event MazeGameResourcesEvent OnChangeResources;
    public delegate void MazeGameWorldEvent(); //Sends to PlayerStats
    public static event MazeGameWorldEvent OnMazeGameEnable;
    public static event MazeGameWorldEvent OnMazeGameDisable;

    public MG_Goal GoalPrefab;
    public MG_Treasure TreasurePrefab;
    public AudioSource clickSound;
    public AudioSource rotateSound;
    public AudioSource addItemSound;
    public AudioSource deleteItemSound;
    public AudioSource errorSound;
    public AudioSource buildItemSound;
    public AudioSource selectObjectSound;
    public AudioSource bonusGoodSound;
    public AudioSource goalSound;
    public AudioSource selectLockedSound;
    public AudioSource newSetSound;
    public AudioSource counterRewindSound;
    public AudioSource freezeObjectsSound;

    public enum ItemRotation { North, East, South, West}
    public List<MG_GameSet> gameSets = new List<MG_GameSet>();
    public List<MG_GameSet> gameSetsEasy = new List<MG_GameSet>();
    public List<MG_GameSet> gameSetsHard = new List<MG_GameSet>();
    public List<MG_GameSet> completedSets = new List<MG_GameSet>();
    public List<int> completedSetsAchievements = new List<int>();

    public MG_GameSet currentGameSet;
    public List<MazeGameItem> currentAvailableItems = new List<MazeGameItem>();
    public List<MG_Goal> currentGoals = new List<MG_Goal>();
    public int currentReachedGoals;
    public List<MG_Treasure> currentTreasures = new List<MG_Treasure>();
    public int currentCost;
    public Vector3Int currentBonusLimits;
    public int setCounter = 0;
    public int currentResources;
    public bool hasOpenedFirstTime;

    public bool hasWonGame;

    public List<Transform> ObjectsToActivateOnComplete = new List<Transform>();
    public List<Transform> ObjectsToSleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    private void OnEnable()
    {        
        extraCanvas.gameObject.SetActive(true);
        StartCoroutine(FadeOut(1));
        cam = Camera.main;

        TransferPlayerStats();
        ResetModeIndicator();    
        ResetFreezer();
        InitializePlayer();

        if (!hasOpenedFirstTime)
        {
            ClearAndStartNew();
            hasOpenedFirstTime = true;
        }
        OpenDifficultyPanelStart();

        Interactable.OnInteractablePressed += SetAgentDestination;
        Interactable.OnInteractablePressed += CheckPlayerLock;
        InteractableBehaviourDTPFree.OnDTPFreeRotation += CheckPath;
        InteractableBehaviourDTPFree.OnDTPFreeRotation += PlayRotationSound;
        InteractableBehaviourDTPFree.OnDTPFreeRotationError += PlayErrorSound;
        InteractableBehaviourDTPFree.OnDTPFreeDrop += ItemPlaced;
        InteractableBehaviourDTPFree.OnDTPFreePickUp += ItemPickedUp;
        InteractableBehaviourDTPFree.OnDTPFreeError += PlayLockedSelect;
        MG_Goal.OnHitPlayer += CheckProgress;
        MG_Treasure.OnHitPlayer += CheckTreasure;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel += StartSelectedGameSet;
              
        if (OnMazeGameEnable != null)
            OnMazeGameEnable();

        gameManager.player.navMeshAgent.agentTypeID = agentNavMeshID;
        //gameManager.hudCanvas.HideUI();
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= SetAgentDestination;
        Interactable.OnInteractablePressed -= CheckPlayerLock;
        InteractableBehaviourDTPFree.OnDTPFreeRotation -= CheckPath;
        InteractableBehaviourDTPFree.OnDTPFreeRotation -= PlayRotationSound;
        InteractableBehaviourDTPFree.OnDTPFreeRotationError -= PlayErrorSound;
        InteractableBehaviourDTPFree.OnDTPFreeDrop -= ItemPlaced;
        InteractableBehaviourDTPFree.OnDTPFreePickUp -= ItemPickedUp;
        InteractableBehaviourDTPFree.OnDTPFreeError -= PlayLockedSelect;
        MG_Goal.OnHitPlayer -= CheckProgress;
        MG_Treasure.OnHitPlayer -= CheckTreasure;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel -= StartSelectedGameSet;

        if (currentPlayerLock)
            Destroy(currentPlayerLock.gameObject);

        if(gameManager.player.navMeshAgent)
            gameManager.player.navMeshAgent.speed = playerSpeed;

        if (playerMovementAgentScript)
            playerMovementAgentScript.lockAgent = false;

        if (setCountPanel.gameObject.activeSelf)
            setCountPanel.gameObject.SetActive(false);

        if (counterRewindSound)
            counterRewindSound.Stop();

        StopAllCoroutines();
        CancelInvoke();

        ResetFreezer();
        ResetAnimatedNumber();
        ResetExtraCanvas();
        extraCanvas.gameObject.SetActive(false);

        if (OnMazeGameDisable != null)
            OnMazeGameDisable();

        //gameManager.player.navMeshAgent.agentTypeID = 0;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CheckIfCurrentItem();           
        }

        CheckPlayerPathStatus();
    }    

    public void ExitScene()
    {
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
    }

    void InitializePlayer()
    {
        if (playerReference.player)
            playerAgent = playerReference.player.GetComponent<NavMeshAgent>();

        playerSpeed = gameManager.player.navMeshAgent.speed;
        playerMovementAgentScript = gameManager.player.GetComponent<PlayerMovementAgent>();

        CreatePlayerLock();
    }

    void CreatePlayerLock()
    {
        if (!currentPlayerLock)
        {
            currentPlayerLock = Instantiate(playerLockPrefab);
            currentPlayerLock.transform.parent = playerReference.player;
            currentPlayerLock.transform.localPosition = Vector3.zero;
        }
    }

    void CheckTargets()
    {
        if(lockPlayer)
        {
            DisableTargets();
        }
        else
        {
            EnableTargets();
        }
    }

    void CheckProgress(MG_Goal passedGoal)
    {
        bool check = false;

        if(currentGoals.Contains(passedGoal))
        {
            check = true;
        }

        if(check)
        {
            currentReachedGoals++;

            if (goalSound)
                goalSound.Play();

            StartAnimatedNumber(passedGoal.transform, resourcesTarget, passedGoal.reward, 0.75f, 2.5f, 0.65f);
            gameManager.player.anim.SetTrigger("Success");

            UpdateResources(currentResources + passedGoal.reward);
            var goal = passedGoal.gameObject;
            currentGoals.Remove(passedGoal);
            Destroy(goal);
        }

        if (currentGameSet.freezeItems)
            StartCoroutine(FreezeCurrentItems());

        if (currentGoals.Count <= 0)
            StartCoroutine(WinSet());           
    }

    void CheckTreasure(MG_Treasure passedTreasure)
    {
        var treasure = passedTreasure.gameObject;

        if(passedTreasure.reward > 0)
        {
            if (bonusGoodSound)
                bonusGoodSound.Play();

            gameManager.player.anim.SetTrigger("Success");

            StartAnimatedNumber(passedTreasure.transform, resourcesTarget, passedTreasure.reward, 0.75f, 2.5f, 0.65f);
        }

        UpdateResources(currentResources + passedTreasure.reward);
        currentTreasures.Remove(passedTreasure);
        Destroy(treasure);

        if (currentGameSet.freezeItems)
            StartCoroutine(FreezeCurrentItems());
    }

    void CheckPlayerLock(Transform passedInteractable)
    {       
        if(passedInteractable == currentPlayerLock.interactable.transform)
        {
            lockPlayerToggle.isOn = !lockPlayerToggle.isOn;
        }
    }

    void ResetPlayerLock()
    {
        lockPlayerToggle.isOn = false;
        lockPlayerToggleSwap.SetIcon();
        LockPlayer();
    }

    void SetToBuildMode()
    {
        lockPlayerToggle.isOn = true;
        lockPlayerToggleSwap.SetIcon();
        LockPlayer();
    }

    public void ButtonError()
    {
        StopCoroutine(RattleToggle());
        StartCoroutine(RattleToggle());
    }

    void HideButtonsBuildMode()
    {
        mazeGameCanvas.addItemPanel.gameObject.SetActive(false);
        HideDeleteBtn();
    }

    void ShowButtonsBuildMode()
    {
        if(currentGameSet.showAddItems)
        {
            mazeGameCanvas.addItemPanel.gameObject.SetActive(true);
        }
        if(currentGameSet.showDelete)
        {
            if (currentGameItem != null)
                ShowDeleteBtn();
        }
    }

    public void LockPlayer()
    {
        lockPlayer = lockPlayerToggle.isOn;

        if (lockPlayer)
        {
            if (playerMovementAgentScript)
                playerMovementAgentScript.lockAgent = true;

            mazeGameCamera.followerScript.targetIsPlayer = false;
            startSlotsParent.targetIsPlayer = false;

            if (currentGameItem)
            {
                mazeGameCamera.followerScript.target = currentGameItem.transform;
                startSlotsParent.target = currentGameItem.transform;
            }
            else
            {
                mazeGameCamera.followerScript.target = worldTransform;
                startSlotsParent.target = worldTransform;
            }

            mazeGameCamera.followerScript.followProperty01.speed = 2.5f;
            startSlotsParent.followProperty01.speed = 4;

            playerAgent.speed = 0;

            if (gameManager.player.isNavMeshAgent)
            {
                currentPlayerLock.sprite.sprite = currentPlayerLock.lockedImage;
                currentPlayerLock.spriteGlow.gameObject.SetActive(true);
            }

            DisableTargets();
            ShowButtonsBuildMode();
            modeVignette.color = lockPlayerToggleSwap.colorOn;
        }
        else
        {
            if (playerMovementAgentScript)
                playerMovementAgentScript.lockAgent = false;

            mazeGameCamera.followerScript.targetIsPlayer = true;
            startSlotsParent.targetIsPlayer = true;

            if (gameManager.player)
            {
                mazeGameCamera.followerScript.target = gameManager.player.transform;
                startSlotsParent.target = gameManager.player.transform;
            }

            mazeGameCamera.followerScript.followProperty01.speed = 4;
            startSlotsParent.followProperty01.speed = 3;

            playerAgent.speed = playerSpeed;

            currentPlayerLock.sprite.sprite = currentPlayerLock.unlockedImage;
            currentPlayerLock.spriteGlow.gameObject.SetActive(false);

            EnableTargets();
            HideButtonsBuildMode();
            modeVignette.color = lockPlayerToggleSwap.colorOn * 0;
        }
    }

    void PlayRotationSound(Transform passedBehaviour)
    {
        if(rotateSound)
            rotateSound.Play();
    }

    void ItemPlaced(Transform passedBehaviour)
    {
        if(buildItemSound)
            buildItemSound.Play();
    }

    void ItemPickedUp(Transform passedBehaviour)
    {

    }

    void PlayErrorSound(Transform passedBehaviour)
    {
        if (errorSound)
            errorSound.Play();
    }

    void PlayLockedSelect(Transform passedBehaviour)
    {
        if(selectLockedSound)
            selectLockedSound.Play();
    }

    void CheckPath(Transform nullTransform)
    {
        gameNavMesh.BuildNavMesh();
        //DisableTargets();
        CheckReachable();
    }

    void UpdateNavigation()
    {
        CheckPath(null);
    }
    
    void CheckPlayerPathStatus()
    {
        if(playerAgent)
        {
            if(playerAgent.hasPath && !playerIsAtGoal)
            {
                if(playerAgent.remainingDistance < 0.1f)
                {
                    playerIsAtGoal = true;

                    if (currentGameItem)
                    {
                        if (OnPlayerReachesGoal != null)
                        {
                            OnPlayerReachesGoal(currentGameItem.transform); ////CONTINUE HERE!!!!!!
                        }
                    }
                }
            }
            if(!playerAgent.hasPath)
            {
                playerIsAtGoal = false;
            }
        }
    }
    
    public void CheckReachable() // called by each itemCollider OnMouseUp
    {
        if (playerAgent != null)
        {
            for (int i = 0; i < currentGameItems.Count; i++)
            {
                bool check = false;

                for (int e = 0; e < currentGameItems[i].walkTargets.Count; e++)
                {
                    if(playerAgent.isOnNavMesh)
                    { 
                        NavMeshPath path = new NavMeshPath();
                        playerAgent.CalculatePath(currentGameItems[i].walkTargets[e].transform.position, path);

                        if (path.status == NavMeshPathStatus.PathComplete)
                        {
                            if (!lockPlayer)
                            {
                                currentGameItems[i].walkTargets[e].gameObject.SetActive(true);
                            }

                            currentGameItems[i].isWalkable = true;

                            if (currentGameItems[i].currentMaterial == currentGameItems[i].materialSel || currentGameItems[i].currentMaterial == currentGameItems[i].materialWalkableSelect)
                            {
                                currentGameItems[i].ChangeToMaterial(currentGameItems[i].materialWalkableSelect);
                            }
                            else
                            {
                                if (!currentGameItems[i].isLocked && !currentGameItems[i].isBlocker)
                                {
                                    if (!currentGameItems[i].itemOnTop)
                                        currentGameItems[i].ChangeToMaterial(currentGameItems[i].materialWalkable);
                                    else
                                        currentGameItems[i].ChangeToMaterial(currentGameItems[i].materialWalkableLocked);
                                }
                                else if (currentGameItems[i].isLocked && !currentGameItems[i].isBlocker)
                                {
                                    currentGameItems[i].ChangeToMaterial(materialStaticWalkable);
                                }
                            }

                            check = true;
                        }                        
                    }
                }

                if(!check)
                {
                    currentGameItems[i].isWalkable = false;

                    if (currentGameItems[i].currentMaterial != currentGameItems[i].materialSel &&
                        currentGameItems[i].currentMaterial != currentGameItems[i].materialWalkableSelect)
                    {
                        if (!currentGameItems[i].isLocked && !currentGameItems[i].isBlocker)
                        {
                            if (currentGameItems[i].itemOnTop)
                                currentGameItems[i].ChangeToMaterial(currentGameItems[i].materialLock);
                            else
                                currentGameItems[i].ChangeToMaterial(currentGameItems[i].material);
                        }
                        else if (currentGameItems[i].isLocked && !currentGameItems[i].isBlocker)
                        {
                            currentGameItems[i].ChangeToMaterial(materialStatic);
                        }
                    }

                    if (currentGameItems[i].currentMaterial == currentGameItems[i].materialWalkableSelect ||
                        currentGameItems[i].currentMaterial == currentGameItems[i].materialSel)
                    {
                        currentGameItems[i].ChangeToMaterial(currentGameItems[i].materialSel);
                    }
                }
            }
        }

        if (currentGameItem)
            currentGameItem.SetSelected(false);
    }

    public void DisableTargets()// called by each itemCollider OnMouseDown
    {
        for (int i = 0; i < currentGameItems.Count; i++)
        {
            for (int e = 0; e < currentGameItems[i].walkTargets.Count; e++)
            {
                currentGameItems[i].walkTargets[e].gameObject.SetActive(false);
            }

            currentGameItems[i].mgCollider.thisCollider.enabled = true;
            print("enabled colliders on " + currentGameItems[i].gameObject.name);
        }        
    }

    public void EnableTargets()
    {
        for (int i = 0; i < currentGameItems.Count; i++)
        {
            for (int e = 0; e < currentGameItems[i].walkTargets.Count; e++)
            {
                currentGameItems[i].walkTargets[e].gameObject.SetActive(true);
            }

            currentGameItems[i].mgCollider.thisCollider.enabled = false;
            print("disabled colliders on " + currentGameItems[i].gameObject.name);
        }        
    }

    public void SetAgentDestination(Transform interactable) //OBSOLETE?
    {
        if(playerAgent)
        {
            if (!gameManager.player.isLocked)
            {
                if (targets.Contains(interactable))
                    playerAgent.SetDestination(interactable.transform.position);// - Vector3.up * 1.1f); //Negative height to initialisation (in CheckReachable() above)
            }
        }
    }

    void CheckIfCurrentItem()
    {
        if (!IsMouseOverUIelements())
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (!hit.collider.CompareTag("Interactable") || hit.collider == null)
                {
                    if (currentGameItem)
                    {
                        if (currentGameItem.itemOnTop)
                        {
                            if (!currentGameItem.isWalkable)
                            {
                                if(!currentGameItem.isLocked && !currentGameItem.isBlocker)
                                    currentGameItem.ChangeToMaterial(currentGameItem.materialLock);
                            }
                            else
                            {
                                if (!currentGameItem.isLocked && !currentGameItem.isBlocker)
                                {
                                    currentGameItem.ChangeToMaterial(currentGameItem.materialWalkableLocked);
                                }
                                else if (currentGameItem.isLocked && !currentGameItem.isBlocker)
                                {
                                    currentGameItem.ChangeToMaterial(materialStaticWalkable);
                                }
                            }
                        }
                        else
                        {
                            if (!currentGameItem.isWalkable)
                            {
                                if (!currentGameItem.isLocked && !currentGameItem.isBlocker)
                                    currentGameItem.ChangeToMaterial(currentGameItem.material);
                            }
                            else
                            {
                                if (!currentGameItem.isLocked && !currentGameItem.isBlocker)
                                {
                                    currentGameItem.ChangeToMaterial(currentGameItem.materialWalkable);
                                }
                                else if (currentGameItem.isLocked && !currentGameItem.isBlocker)
                                {
                                    currentGameItem.ChangeToMaterial(materialStaticWalkable);
                                }
                            }
                        }

                        currentGameItem.rotationHandle.gameObject.SetActive(false);
                        currentGameItem = null;
                        HideDeleteBtn();
                    }
                }
            }
            else
            {
                if (currentGameItem)
                {
                    if (currentGameItem.itemOnTop)
                    {
                        if(!currentGameItem.isWalkable)
                            currentGameItem.ChangeToMaterial(currentGameItem.materialLock);
                        else
                            currentGameItem.ChangeToMaterial(currentGameItem.materialWalkableLocked);
                    }
                    else
                    {
                        if(!currentGameItem.isWalkable)
                            currentGameItem.ChangeToMaterial(currentGameItem.material);
                        else
                            currentGameItem.ChangeToMaterial(currentGameItem.materialWalkable);
                    }

                    currentGameItem.rotationHandle.gameObject.SetActive(false);
                    currentGameItem = null;
                    HideDeleteBtn();
                }
            }
        }

        UpdateCameraTarget();

    }

    public void HideDeleteBtn()
    {
        if(mazeGameCanvas.deleteButtonTransform.parent.gameObject.activeSelf)
            mazeGameCanvas.deleteButtonTransform.parent.gameObject.SetActive(false);
    }

    public void ShowDeleteBtn()
    {
        if (!mazeGameCanvas.deleteButtonTransform.parent.gameObject.activeSelf)
            mazeGameCanvas.deleteButtonTransform.parent.gameObject.SetActive(true);
    }
    
    void UpdateCameraTarget()
    {
        if (lockPlayer)
        {
            if (currentGameItem)
            {
                mazeGameCamera.followerScript.target = currentGameItem.transform;
                startSlotsParent.target = currentGameItem.transform;
            }
            else
            {
                mazeGameCamera.followerScript.target = worldTransform;
                startSlotsParent.target = worldTransform;
            }
        }
    }
    /*
    void DestroyGameItem(MazeGameItem itemToDestroy) //Not in use
    {
        foreach(var nestedPoint in itemToDestroy.interactableDragToPoints.nestedPoints)
        {
            foreach (var item in currentGameItems)
            {
                item.interactableDragToPoints.points.Remove(nestedPoint);    
            }
        }        
    }
    */
    public void AddMazeGameItem(MazeGameItem itemPrefab)
    {
        bool freeSlotCheck = true;

        if (!startSlotsParent.gameObject.activeSelf && playerAgent)
            freeSlotCheck = true;
        else
            freeSlotCheck = false;
        
        if (freeSlotCheck)
        {
            startSlotsParent.gameObject.SetActive(true);
            startSlotsParent.transform.position = playerAgent.transform.position;

            foreach (var item in newItemsStartSlots)
            {
                item.gameObject.SetActive(true);   
            }

            var newItem = Instantiate(itemPrefab, startSlotsParent.transform);
            var newItemScript = newItem.GetComponent<MazeGameItem>();
            nextGameItem = newItemScript;

            newItemScript.mazeGameScript = this;
            newItemScript.mazeGameNavMesh = this.gameNavMesh;
            newItemScript.interactableDragToPoints.startPoint = newItemsStartSlots[4]; //midPoint

            Instantiate(birthEffectPrefab, newItemsStartSlots[4].position + Vector3.up, Quaternion.identity);

            currentGameItems.Add(newItemScript);

            DeliverItemPoints(newItemScript);
            ItemGetPoints(newItemScript, snapPoints);

            //Resources:
            var resourcesToSubtract = CheckItemCost(newItemScript);

            StartAnimatedNumber(resourcesTarget, newItem.transform, -resourcesToSubtract, 0.75f, 0.65f, 4);

            UpdateResources(currentResources - resourcesToSubtract);

            Invoke("UpdateNavigation", 0);
        }
        else
        {
            print("Error - Not enough space to spawn..."); //indicate error
        }
    }

    public void DeleteMazeGameItem()
    {
        bool check = false;
        MazeGameItem itemToDestroy = null;

        if (nextGameItem)
            itemToDestroy = nextGameItem;
        else if (currentGameItem && !nextGameItem)
            itemToDestroy = currentGameItem;

        if(currentGameItem || nextGameItem)
        {
            if (itemToDestroy.isDestroyable || nextGameItem)
            {
                if(!nextGameItem && deleteItemSound)
                    deleteItemSound.Play();

                if (itemToDestroy.interactableDragToPoints.lastPoint)
                {
                    itemToDestroy.interactableDragToPoints.lastPoint.gameObject.SetActive(true);
                }

                for (int i = 0; i < itemToDestroy.interactableDragToPoints.volumeTempPoints.Count; i++)
                {
                    if (itemToDestroy.interactableDragToPoints.volumeTempPoints[i])
                    {
                        itemToDestroy.interactableDragToPoints.volumeTempPoints[i].gameObject.SetActive(true);
                    }
                }

                RemoveItemPoints(itemToDestroy);
                
                if(currentGameItems.Contains(itemToDestroy))
                    currentGameItems.Remove(itemToDestroy);

                check = true;
            }
        }

        if (check)
        {
            if(itemToDestroy.interactableDragToPoints.mainObjectClone)
            {
                Destroy(itemToDestroy.interactableDragToPoints.mainObjectClone);
            }

            print(itemToDestroy.gameObject);
            Instantiate(deathEffectPrefab, itemToDestroy.transform.position + Vector3.up, Quaternion.identity);

            if (currentGameItem && !nextGameItem)
            {
                if(itemToDestroy == currentGameItem)
                    currentGameItem = null;
            }

            if(nextGameItem)
            {
                if(itemToDestroy == nextGameItem)
                {
                    startSlotsParent.gameObject.SetActive(false);
                }

                nextGameItem = null;
            }
            //Resources:
            var resourcesToAdd = CheckItemCost(itemToDestroy);

            StartAnimatedNumber(itemToDestroy.transform, resourcesTarget, resourcesToAdd, 0.75f, 4, 0.65f);

            UpdateResources(currentResources + resourcesToAdd);

            Destroy(itemToDestroy.gameObject);

            Invoke("UpdateNavigation", 0);
        }
        else
        {
            print("Nothing to delete!");
        }

        HideDeleteBtn();
    }

    public bool IsMouseOverUIelements()
    {
        var mousePosition = Input.mousePosition;
        int testInt = 0;

        for (int i = 0; i < mazeGameCanvas.uiRects.Count; i++)
        {
            if (mazeGameCanvas.uiRects[i])
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(mazeGameCanvas.uiRects[i], Input.mousePosition, Camera.main))
                {
                    testInt += 1;
                }
            }
        }

        if (testInt == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool CheckDistanceToPlayer()
    {
        if(currentGameItem)
        {
            bool check = false;
            
            for (int i = 0; i < currentGameItem.walkTargets.Count; i++)
            {
                if(Vector3.Distance(playerReference.player.position, currentGameItem.walkTargets[i].transform.position) < 0.25f)
                {
                    check = true;
                }
            }

            if (check)
                return true;
            else
                return false;
        }
        else
        {
            return false;
        }
    }

    private void Initialize()
    {
        startSlotsParent.gameObject.SetActive(false);

        var gamePoints = new List<Transform>();

        foreach (Transform child in gridPointsContainer)
        {
            snapPoints.Add(child);
            child.gameObject.SetActive(true);
            gamePoints.Add(child);
        }

        for (int i = 0; i < newItemsStartSlots.Count; i++)
        {
            snapPoints.Add(newItemsStartSlots[i]);
            gamePoints.Add(newItemsStartSlots[i]);
        }

        for (int i = 0; i < currentGameItems.Count; i++)
        {
            ItemGetPoints(currentGameItems[i], gamePoints);
        }

        gamePoints.Clear();

        for (int i = 0; i < currentGameItems.Count; i++)
        {
            currentGameItems[i].enabled = true;
        }

        isInitialized = true;
    }

    private void ClearLevel()
    {
        snapPoints.Clear();

        for (int i = currentGameItems.Count - 1; i > -1; i--)
        {
            var item = currentGameItems[i].gameObject;
            currentGameItems.Remove(currentGameItems[i]);
            Destroy(item);
        }

        currentAvailableItems.Clear();

        for (int i = currentGoals.Count - 1; i > -1; i--)
        {
            var goal = currentGoals[i].gameObject;
            Destroy(goal);
        }
        currentGoals.Clear();
        currentReachedGoals = 0;

        for (int i = currentTreasures.Count - 1; i > -1; i--)
        {
            var treasure = currentTreasures[i].gameObject;
            Destroy(treasure);
        }
        currentTreasures.Clear();
        currentResources = 0;
    }

    private void StartNewSet(int setIndex, PlayerStats.Difficulty difficulty)
    {
        StopCoroutine(AnimateModeIndicator());
        modeIndicator.canvasGrp.alpha = 0;

        StartCoroutine(FadeOut(1));

        SetGameSetWithDifficulty(setIndex);

        gameManager.playerStats.MazeGameSetCount = setCounter;

        currentResources = currentGameSet.resources;
        
        currentBonusLimits = currentGameSet.bonusLimits;

        SetAvialableItems();

        InstantiateLevelItems();

        InstantiateGoals();

        InstantiateTreasures();

        mazeGameCanvas.UpdateAvailable(currentGameSet.availableItems);

        UpdateResources(currentResources);

        Invoke("UpdateNavigation", 0);

        ToggleGameButtons();

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        mazeGameCanvas.buttonsPanel.gameObject.SetActive(true);
        mazeGameCanvas.inputBlocker.gameObject.SetActive(false);
        mazeGameCanvas.resourcesPanel.gameObject.SetActive(true);
        StartCoroutine(ShowSetCountPanel(setCounter));

        if (newSetSound)
            newSetSound.Play();

        if (!currentGameSet.showAddItems)
            ResetPlayerLock();
        else
            SetToBuildMode();
        
        StartCoroutine(AnimateModeIndicator());

        gameManager.playerStats.SaveGame();
    }

    void ResetPlayer()
    {
        Invoke("UpdateNavigation", 0);

        if (playerAgent)
        {
            playerAgent.Warp(startDestination.position);
            playerAgent.transform.rotation = startDestination.rotation;
        }
    }

    public void ClearAndStartNew()
    {
        ClearLevel();
        ResetPlayer();
        Resources.UnloadUnusedAssets();
        Initialize();
        mazeGameCamera.RotateBack();
        StartNewSet(setCounter, mazeGameDifficulty);        
    }

    public void ClearAndInitialize()
    {
        ClearLevel();
        ResetPlayer();
        Resources.UnloadUnusedAssets();
        Initialize();
        mazeGameCamera.RotateBack();
    }

    private void ToggleGameButtons()
    {
        if (currentGameSet.showAddItems)
            mazeGameCanvas.addItemPanel.gameObject.SetActive(true);
        else
            mazeGameCanvas.addItemPanel.gameObject.SetActive(false);

        HideDeleteBtn();
        /*
        if (currentGameSet.showDelete)
            mazeGameCanvas.deleteButtonTransform.gameObject.SetActive(true);
        else
            mazeGameCanvas.deleteButtonTransform.gameObject.SetActive(false);
        */

        if (currentGameSet.showRestart)
            mazeGameCanvas.restartButtonTransform.gameObject.SetActive(true);
        else
            mazeGameCanvas.restartButtonTransform.gameObject.SetActive(false);
    }
    
    private void InstantiateLevelItems()
    {
        for (int i = 0; i < currentGameSet.levelItems.Count; i++)
        {
            for (int e = 0; e < currentGameSet.levelItems[i].coordinates.Count; e++)
            {
                var newLevelItem = Instantiate(currentGameSet.levelItems[i].item);
                newLevelItem.isInitialized = true;
                newLevelItem.transform.parent = itemsContainer;
                newLevelItem.transform.localPosition = currentGameSet.levelItems[i].coordinates[e];
                var rot = CheckItemRotation(currentGameSet.levelItems[i].rotations[e]);
                newLevelItem.transform.localEulerAngles = rot;
                newLevelItem.mazeGameScript = this;
                DeliverItemPoints(newLevelItem);
                ItemGetPoints(newLevelItem, snapPoints);

                newLevelItem.interactableDragToPoints.Initialize();

                if (!currentGameSet.levelItems[i].isDestroyable)
                    newLevelItem.isDestroyable = false;
                else
                    newLevelItem.isDestroyable = true;

                if (currentGameSet.levelItems[i].isStatic && !currentGameSet.levelItems[i].isBlocker)
                {
                    newLevelItem.isLocked = true;
                    newLevelItem.ChangeToMaterial(materialStatic);
                    newLevelItem.interactableDragToPoints.lockBehaviour = true;
                }

                if (currentGameSet.levelItems[i].isBlocker)
                {
                    newLevelItem.isLocked = true;
                    newLevelItem.isBlocker = true;
                    newLevelItem.interactableDragToPoints.lockBehaviour = true;
                    newLevelItem.ChangeToMaterial(materialBlocker);
                }

                //newLevelItem.mgCollider.GetComponent<MazeGameItemCollider>().is
                currentGameItems.Add(newLevelItem);
            }
        }
    }

    void InstantiateGoals()
    {
        for (int i = 0; i < currentGameSet.goals.Count; i++)
        {
            var goal = Instantiate(GoalPrefab);
            goal.transform.parent = worldTransform;
            goal.transform.localPosition = currentGameSet.goals[i].position;
            goal.reward = currentGameSet.goals[i].reward;
            goal.rewardText.text = goal.reward.ToString();
            currentGoals.Add(goal);
        }
    }

    void InstantiateTreasures()
    {
        for (int i = 0; i < currentGameSet.treasures.Count; i++)
        {
            var treasure = Instantiate(TreasurePrefab);
            treasure.transform.parent = worldTransform;
            treasure.transform.localPosition = currentGameSet.treasures[i].position;
            treasure.reward = currentGameSet.treasures[i].reward;
            treasure.rewardText.text = treasure.reward.ToString();
            currentTreasures.Add(treasure);
        }
    }

    void SetAvialableItems()
    {
        for (int i = 0; i < currentGameSet.availableItems.Count; i++)
        {
            currentAvailableItems.Add(currentGameSet.availableItems[i].item);
        }
    }

    int CheckItemCost(MazeGameItem item)
    {
        int cost = 0;

        for (int i = 0; i < currentGameSet.availableItems.Count; i++)
        {
            if (currentGameSet.availableItems[i].item.itemIndex == item.itemIndex)
                cost = currentGameSet.availableItems[i].cost;
        }

        return cost;
    }

    void UpdateResources(int newAmount)
    {
        currentResources = newAmount;

        if (OnChangeResources != null)
            OnChangeResources(newAmount);

    }

    private IEnumerator FreezeCurrentItems()
    {
        yield return new WaitForSeconds(0.5f);

        freezer.gameObject.SetActive(true);
        freezerFollower.gameObject.SetActive(true);
        freezerLight.gameObject.SetActive(true);

        Vector3 startPos = new Vector3(0, freezerHeight * -1, 0);
        Vector3 endPos = new Vector3(0, freezerHeight, 0);
        Vector3 midPos = new Vector3(0, 0, 0);
        Vector3 startScale = new Vector3(0.51f, 0.4f, 1);
        Vector3 endScale = new Vector3(0.51f, 0.4f, 1);
        Color startColor = new Color(freezerLight.color.r, freezerLight.color.g, freezerLight.color.b, 0);
        Color endColor= new Color(freezerLight.color.r, freezerLight.color.g, freezerLight.color.b, 1);

        float t = 0;
        float animTime = 1f;

        if (freezeObjectsSound)
            freezeObjectsSound.Play();

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            freezer.transform.localPosition = Vector3.Lerp(startPos, midPos, t);
            freezer.transform.localScale = Vector3.Lerp(startScale, endScale, h);
            freezerLight.color = Color.Lerp(startColor, endColor, h);
            freezerFollower.transform.position = freezer.position;
            yield return new WaitForEndOfFrame();
        }        

        for (int i = 0; i < currentGameItems.Count; i++)
        {
            if (!currentGameItems[i].isBlocker)
            {
                currentGameItems[i].isLocked = true;

                if (!currentGameItems[i].isWalkable)
                    currentGameItems[i].ChangeToMaterial(materialStatic);
                else
                    currentGameItems[i].ChangeToMaterial(materialStaticWalkable);

                currentGameItems[i].interactableDragToPoints.lockBehaviour = true;
            }
        }
        
        float ti = 0;
        float animTime2 = 1f;

        while (ti < 1)
        {
            ti += Time.deltaTime / animTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            freezer.transform.localPosition = Vector3.Lerp(midPos, endPos, ti);
            freezer.transform.localScale = Vector3.Lerp(endScale, startScale, h);
            freezerLight.color = Color.Lerp(endColor, startColor, h);
            freezerFollower.transform.position = freezer.position;
            yield return new WaitForEndOfFrame();
        }        

        freezer.gameObject.SetActive(false);
        freezerFollower.gameObject.SetActive(false);
        freezerLight.gameObject.SetActive(false);
    }

    void ResetFreezer()
    {
        freezer.transform.localScale = new Vector3(0.51f, 0.4f, 1);
        freezer.transform.localPosition = new Vector3(0, freezerHeight, 0);
        freezer.gameObject.SetActive(false);
        freezerFollower.gameObject.SetActive(false);
        freezerLight.color = new Color(freezerLight.color.r, freezerLight.color.g, freezerLight.color.b, 0);
        freezerLight.gameObject.SetActive(false);
    }

    Vector3Int CheckItemRotation(ItemRotation rotation)
    {
        int rot = 0;

        switch(rotation)
        {
            case (ItemRotation.North):
                rot = 0;
                break;
            case (ItemRotation.East):
                rot = 90;
                break;
            case (ItemRotation.South):
                rot = 180;
                break;
            case (ItemRotation.West):
                rot = -90;
                break;
            default:
                rot = 0;
                break;
        }

        return new Vector3Int(0, rot, 0);
    }

    private void DeliverItemPoints(MazeGameItem item)
    {
        for (int e = 0; e < currentGameItems.Count; e++)
        {
            if (currentGameItems[e] != item)
            {
                for (int i = 0; i < item.snapPoints.Count; i++)
                {
                    var point = item.snapPoints[i];

                    if (!currentGameItems[e].interactableDragToPoints.points.Contains(point))
                        currentGameItems[e].interactableDragToPoints.points.Add(point);
                }
            }
        }

        for (int i = 0; i < item.snapPoints.Count; i++)
        {
            var point = item.snapPoints[i];

            if(!snapPoints.Contains(point))
                snapPoints.Add(point);            
        }
    }

    private void RemoveItemPoints(MazeGameItem item)
    {
        for (int e = 0; e < currentGameItems.Count; e++)
        {
            if (currentGameItems[e] != item)
            {
                for (int i = 0; i < item.snapPoints.Count; i++)
                {
                    var point = item.snapPoints[i];

                    if (currentGameItems[e].interactableDragToPoints.points.Contains(point))
                    {
                        currentGameItems[e].interactableDragToPoints.points.Remove(point);
                        print("deleted " + point.name + " on " + currentGameItems[e].name);
                    }
                }               
            }
        }

        for (int i = 0; i < item.snapPoints.Count; i++)
        {
            var point = item.snapPoints[i];

            if (snapPoints.Contains(point))
                snapPoints.Remove(point);            
        }
    }

    private void ItemGetPoints(MazeGameItem item, List<Transform> pointList)
    {
        for (int i = 0; i < pointList.Count; i++)
        {
            var point = pointList[i];

            if (!item.snapPoints.Contains(point) && !item.interactableDragToPoints.nestedPoints.Contains(point))
                item.interactableDragToPoints.points.Add(pointList[i]);
        }
    }

    private IEnumerator WinSet()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        mazeGameCanvas.buttonsPanel.gameObject.SetActive(false);
        //mazeGameCanvas.resourcesPanel.gameObject.SetActive(false);
        mazeGameCanvas.inputBlocker.gameObject.SetActive(true);
        LockSpriter(true);

        CorrectTicker();

        int achievement = CheckCurrentAchievement();
        int prevAchievement = CheckPrevAchievement();

        AddCompletedSetAndAchievement(achievement);
        SetCurrentCompletedSet(achievement);

        yield return new WaitForSeconds(2);

        OpenAchievementPanel(achievementPanel, mazeGameDifficulty, (setCounter + 1), gameSets.Count, prevAchievement, achievement);

        yield return new WaitForSeconds(7);

        StartCoroutine(RewardPlayer(achievement, prevAchievement));

        yield return new WaitForSeconds((achievement - prevAchievement) + 2);

        mazeGameCamera.RotateBack();

        if (setCounter >= gameSets.Count - 1)
        {
            setCounter = 0;
            //gameManager.playerStats.MazeGameSetCount = 0;

            if (!hasWonGame)
            {
                trophyPanel.gameObject.SetActive(true);
                yield return new WaitForSeconds(2);
                if (bonusGoodSound)
                    bonusGoodSound.Play();
                trophyGraphic.gameObject.SetActive(true);
                yield return new WaitForSeconds(3);
                gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
                gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
                yield return new WaitForSeconds(2);
                gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
                trophyGraphic.gameObject.SetActive(false);
                trophyPanel.gameObject.SetActive(false);
                yield return new WaitForSeconds(3);
                gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
                gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
                gameManager.playerStats.trophies.Add(trophy);
                gameManager.playerStats.hasWonMazeGame = true;
                StartCoroutine(WinGame());

                foreach (var item in ObjectsToActivateOnComplete)
                {
                    item.gameObject.SetActive(true);
                }

                foreach (var item in ObjectsToSleepOnComplete)
                {
                    item.gameObject.SetActive(false);
                }
            }
            else
            {
                OpenDifficultyPanelStart();
                Invoke("ClearAndStartNew", 0);
                StartCoroutine(FadeIn(1));
                yield return new WaitForSeconds(1);
            }

            hasWonGame = true;
        }
        else
        {
            setCounter++;
            //gameManager.playerStats.MazeGameSetCount = setCounter;
            Invoke("ClearAndStartNew", 0);
            StartCoroutine(FadeIn(1));
            yield return new WaitForSeconds(1);
        }

        gameManager.playerStats.SaveGame();
        LockSpriter(false);

        yield return null;
    }

    void LockSpriter(bool setLock)
    {
        if (gameManager.player.spriter)
        {
            if (setLock)
            {
                gameManager.player.spriter.isLocked = true;
            }
            else
            {
                gameManager.player.spriter.isLocked = false;
            }
        }
    }

    private IEnumerator RewindTimer(float time, int step)
    {
        float t = 0;
        int tempResources = currentResources;
        int startResources = currentResources;
        counterRewindSound.Play();


        while (t < 1)
        {
            t += Time.deltaTime / time;

            if (tempResources > 0 && t >= (((startResources / tempResources) / startResources) / time))
            {
                tempResources -= 2;
                mazeGameCanvas.resourcesText.text = tempResources.ToString();
            }

            if (tempResources <= 0)
            {
                tempResources = 0;

                if (counterRewindSound.isPlaying)
                    counterRewindSound.Stop();

                break;
            }

            yield return new WaitForEndOfFrame();
        }


        tempResources = 0;
        mazeGameCanvas.resourcesText.text = tempResources.ToString();

        if (counterRewindSound.isPlaying)
            counterRewindSound.Stop();
    }

    private IEnumerator WinGame()
    {
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
        hasOpenedFirstTime = false;
        yield return null;
    }

    private IEnumerator RewardPlayer(int passedAchievement, int passedOldAchievement)
    {
        float timeInterval = 0.2f;

        int reward = (passedAchievement - passedOldAchievement) * 5;

        if (reward <= 0)
            reward = 1;

        if(reward > 1)
        {
            gameManager.player.anim.SetTrigger("Success");

            if (bonusGoodSound)
                bonusGoodSound.Play();
        }

        StartCoroutine(RewindTimer((reward * timeInterval) + 3, Mathf.RoundToInt(reward/2)));

        for (int i = 0; i < reward; i++)
        {
            Invoke("InstantiateGem", timeInterval * i);
        }

        yield return new WaitForSeconds((passedAchievement - passedOldAchievement) + 1);
    }

    void InstantiateGem()
    {
        if (gameManager.player)
            Instantiate(gemPhysicalPrefab, gameManager.player.transform.position + Vector3.up * 4f, Quaternion.identity, worldTransform);
    }

    private IEnumerator ShowSetCountPanel(int setCount)
    {
        if (mazeGameDifficulty == PlayerStats.Difficulty.Easy)
            setCountImage.color = gameManager.colorEasy;
        else if(mazeGameDifficulty == PlayerStats.Difficulty.Normal)
            setCountImage.color = gameManager.colorNormal;
        else if (mazeGameDifficulty == PlayerStats.Difficulty.Hard)
            setCountImage.color = gameManager.colorHard;
        else
            setCountImage.color = Color.grey;

        setCountPanelText.text = (setCount + 1).ToString() + "/" + gameSets.Count.ToString();

        if (setCountPanelText.text.Length < 4)
            setCountPanelText.fontSize = 220;
        else if (setCountPanelText.text.Length == 4)
            setCountPanelText.fontSize = 200;
        else if (setCountPanelText.text.Length > 4)
            setCountPanelText.fontSize = 160;

        setCountPanel.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        setCountPanel.gameObject.SetActive(false);
    }

    void AddCompletedSetAndAchievement(int passedAchievement)
    {
        if (!completedSets.Contains(currentGameSet))
        {
            completedSets.Add(currentGameSet);
            completedSetsAchievements.Add(passedAchievement);
        }
    }

    public void RestartLevel()
    {
        StartCoroutine(RestartRoutine());
        mazeGameCamera.RotateBack();
    }

    void ResetExtraCanvas()
    {
        mazeGameCanvas.fader.alpha = 0;
        setCountPanel.localScale = Vector3.zero;
    }

    private IEnumerator FadeIn(float time)
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / time;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            mazeGameCanvas.fader.alpha = Mathf.Lerp(0, 1, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FadeOut(float time)
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / time;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            mazeGameCanvas.fader.alpha = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator RestartRoutine()
    {
        StartCoroutine(FadeIn(1));

        yield return new WaitForSeconds(1);

        ClearAndStartNew();
    }

    private void ResetAnimatedNumber()
    {
        animatedNumberParent.transform.position = Vector3.zero;
        animatedNumberText.color = Color.white;
        animatedNumberText.text = "";
        animatedNumberTextBg.color = Color.black;
        animatedNumberParent.alpha = 0;
        animatedNumberParent.gameObject.SetActive(false);
    }

    public void StartAnimatedNumber(Transform startTransform, Transform endTransform, int value, float totalTime, float startSize, float endSize)
    {
        StartCoroutine(AnimateNumber(startTransform, endTransform, value, totalTime, startSize, endSize));
    }

    private IEnumerator AnimateNumber(Transform startTransform, Transform endTransform, int value, float totalTime, float startSize, float endSize)
    {
        ResetAnimatedNumber();

        Vector3 curStartPos = startTransform.position;
        Vector3 curEndPos = endTransform.position;
        Vector3 lastStartPos = curStartPos;
        Vector3 lastEndPos = curEndPos;

        animatedNumberParent.transform.position = startTransform.position;
        animatedNumberText.color = Color.white;

        if (value >= 0)
        {
            animatedNumberText.text = "+" + value.ToString();
            animatedNumberTextBg.color = new Color(1, 0.8f, 0, 1);
        }
        else
        {
            animatedNumberText.text = value.ToString();
            animatedNumberTextBg.color = new Color(1, 0, 0, 1);
        }
        
        animatedNumberParent.alpha = 1;
        animatedNumberParent.gameObject.SetActive(true);

        //float size = Vector3.Distance(startTransform.position, endTransform.position) * sizeMultiplier;
        //float size = startSize;

        float t = 0;
        float lerpTime = totalTime / 3f;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            animatedNumberParent.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * startSize, h);
            //animatedNumberParent.alpha = Mathf.Lerp(0, 1, h);
            yield return new WaitForEndOfFrame();
        }

        float ti = 0;
        float lerpTime2 = (totalTime / 2f) + (totalTime / 3f);

        //Color bgStartCol = new Color(animatedNumberTextBg.color.r, animatedNumberTextBg.color.g, animatedNumberTextBg.color.b, 1);
        //Color bgEndCol = new Color(bgStartCol.r, bgStartCol.g, bgStartCol.b, 0);
        //Color startCol = Color.white;
        //Color endCol = new Color(startCol.r, startCol.g, startCol.b, 0);        

        while (ti < 1)
        {
            ti += Time.deltaTime / lerpTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            animatedNumberParent.transform.localScale = Vector3.Lerp(Vector3.one * startSize, Vector3.one * endSize, h);

            if (startTransform)
                curStartPos = startTransform.position;
            else
                curStartPos = lastStartPos;

            if (endTransform)
                curEndPos = endTransform.position;
            else
                curEndPos = lastEndPos;

            animatedNumberParent.transform.position = Vector3.Lerp(curStartPos, curEndPos, h);

            if(ti >= 0.5f)
                animatedNumberParent.alpha = Mathf.Lerp(1, 0, h * 0.5f);

            yield return new WaitForEndOfFrame();
        }

        ResetAnimatedNumber();
    }

    private IEnumerator RattleToggle()
    {
        float t = 0;
        float animTime = 1f;
        lockPlayerToggle.transform.localPosition = Vector3.zero;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.EaseIn(t);
            var s = Mathf.Sin(h * (Mathf.PI * 4));
            s *= 10f;
            lockPlayerToggle.transform.localPosition = new Vector3(s, 0, 0);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator AnimateModeIndicator()
    {
        modeIndicator.canvasGrp.alpha = 0;
        modeIndicator.transform.localScale = Vector3.one;
        modeIndicator.transform.localPosition = Vector3.zero;

        if(lockPlayerToggle.isOn)
        {
            modeIndicator.bg.color = lockPlayerToggleSwap.colorOn;
            modeIndicator.walkIcon.gameObject.SetActive(false);
            modeIndicator.buildIcon.gameObject.SetActive(true);
        }
        else
        {
            modeIndicator.bg.color = lockPlayerToggleSwap.colorOff;
            modeIndicator.walkIcon.gameObject.SetActive(true);
            modeIndicator.buildIcon.gameObject.SetActive(false);
        }

        yield return new WaitForSeconds(3f);

        Vector3 endPos = lockPlayerToggle.transform.position;

        float t = 0;
        float animTime = 3;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            modeIndicator.canvasGrp.alpha = Mathf.Lerp(1, 0.1f, h);
            modeIndicator.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 0.5f, h);
            modeIndicator.transform.localPosition = Vector3.Lerp(Vector3.zero, lockPlayerToggle.transform.parent.localPosition, h);
            yield return new WaitForEndOfFrame();
        }

        StopCoroutine(RattleToggle());
        StartCoroutine(RattleToggle());

        modeIndicator.canvasGrp.alpha = 0;
    }

    void ResetModeIndicator()
    {
        modeIndicator.canvasGrp.alpha = 0;
        modeIndicator.transform.localScale = Vector3.one;
        modeIndicator.transform.localPosition = Vector3.zero;
    }

    private void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            foreach (var item in snapPoints)
            {
                if (item.gameObject.activeSelf)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawWireSphere(item.position, 0.25f);
                }
                else
                {
                    Gizmos.color = Color.gray;
                    Gizmos.DrawWireSphere(item.position, 0.25f);
                }
            }
        }
    }

    //---------------------------------------------------IDifficulty/ISetDifficulty------------------------------------------------------------------

    public void CheckDifficulty()
    {
        if (!hasWonGame)
        {
            if ((correctStreakCounter >= numberOfCorrects && mazeGameDifficulty != PlayerStats.Difficulty.Hard) || (wrongStreakCounter >= numberOfWrongs && mazeGameDifficulty != PlayerStats.Difficulty.Easy))
            {
                if (correctStreakCounter >= numberOfCorrects && mazeGameDifficulty == PlayerStats.Difficulty.Easy)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;
                else if (correctStreakCounter >= numberOfCorrects && mazeGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Hard;
                else if (wrongStreakCounter >= numberOfWrongs && mazeGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Easy;
                else if (wrongStreakCounter >= numberOfWrongs && mazeGameDifficulty == PlayerStats.Difficulty.Hard)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;

                gameManager.hudCanvas.difficultyPanel.OpenPanel();
                correctStreakCounter = 0;
                wrongStreakCounter = 0;
            }
        }
    }

    public void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        mazeGameDifficulty = requestedDifficulty;        
    }
    public void SetCurrentDifficultyStartSet(PlayerStats.Difficulty requestedDifficulty)
    {
        mazeGameDifficulty = requestedDifficulty;
        Invoke("ClearAndStartNew", 0);
    }

    public void SetGameSetWithDifficulty(int index)
    {
        switch (mazeGameDifficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                currentGameSet = gameSetsEasy[index];
                break;
            case (PlayerStats.Difficulty.Normal):
                currentGameSet = gameSets[index];
                break;
            case (PlayerStats.Difficulty.Hard):
                currentGameSet = gameSetsHard[index];
                break;
            default:
                currentGameSet = gameSets[index];
                break;
        }
    }

    public void WrongTicker()
    {
        wrongStreakCounter++;
        correctStreakCounter = 0;
        CheckDifficulty();
    }


    public void CorrectTicker()
    {
        wrongStreakCounter = 0;
        correctStreakCounter++;
        CheckDifficulty();
    }

    public void OpenDifficultyPanelStart()
    {
        gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.None;
        gameManager.hudCanvas.difficultyPanel.startNewLevel = true;

        if (gameManager.playerStats.hasWonMazeGame)
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = true;
            gameManager.hudCanvas.difficultyPanel.currentLevelCount = gameSets.Count;
        }
        else
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = false;
        }

        gameManager.hudCanvas.difficultyPanel.gameObject.SetActive(true);
    }

    public void SetCurrentCompletedSet(int passedAchievement)
    {
        int setIndex = 0;

        if (gameSets.Contains(currentGameSet))
            setIndex = gameSets.IndexOf(currentGameSet) + gameSets.Count;
        else if (gameSetsEasy.Contains(currentGameSet))
            setIndex = gameSetsEasy.IndexOf(currentGameSet);
        else if (gameSetsHard.Contains(currentGameSet))
            setIndex = gameSetsHard.IndexOf(currentGameSet) + (gameSets.Count * 2);

        if (!gameManager.playerStats.mazeGameCompletedSets.Contains(setIndex))
        {
            gameManager.playerStats.mazeGameCompletedSets.Add(setIndex);
            gameManager.playerStats.mazeGameSetAchievements.Add(setIndex, passedAchievement);
        }
        else
        {
            gameManager.playerStats.mazeGameSetAchievements[setIndex] = passedAchievement;
        }        

        //SetPlayerStatsCurrentCompletedSets();
    }

    public void SetPlayerStatsCurrentCompletedSets()
    {
        gameManager.playerStats.currentCompletedSets.Clear();
        gameManager.playerStats.currentAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.mazeGameCompletedSets.Count; i++)
        {
            var index = gameManager.playerStats.mazeGameCompletedSets[i];
            gameManager.playerStats.currentCompletedSets.Add(index);
            gameManager.playerStats.currentAchievements.Add(index, gameManager.playerStats.mazeGameSetAchievements[index]);
        }      
    }

    public void TransferPlayerStats()
    {

        hasWonGame = gameManager.playerStats.hasWonMazeGame;
        setCounter = gameManager.playerStats.MazeGameSetCount;

        completedSets.Clear();
        completedSetsAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.mazeGameCompletedSets.Count; i++)
        {
            var setIndex = gameManager.playerStats.mazeGameCompletedSets[i];

            if (setIndex < gameSets.Count)
            {
                completedSets.Add(gameSetsEasy[setIndex]);
            }
            else if (setIndex >= gameSets.Count && setIndex < (gameSets.Count * 2))
            {
                completedSets.Add(gameSets[setIndex - gameSets.Count]);
            }
            else if (setIndex >= (gameSets.Count * 2) && setIndex < (gameSets.Count * 3))
            {
                completedSets.Add(gameSetsHard[setIndex - (gameSets.Count * 2)]);
            }

            completedSetsAchievements.Add(gameManager.playerStats.mazeGameSetAchievements[setIndex]);
        }
    }

    public void StartSelectedGameSet(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        mazeGameDifficulty = requestedDifficulty;

        if (requestedLevel < gameSets.Count)
            setCounter = requestedLevel;
        else if (requestedLevel >= gameSets.Count && requestedLevel < (gameSets.Count * 2))
            setCounter = requestedLevel - gameSets.Count;
        else if (requestedLevel >= gameSets.Count * 2)
            setCounter = requestedLevel - (gameSets.Count * 2);

        Invoke("ClearAndStartNew", 0);
    }

    //IAchievementPanel:------------------------------------

    public void OpenAchievementPanel(AchievementPanel achievementPanel, PlayerStats.Difficulty difficulty, int levelIndex, int totalLevels, int prevAchievement, int achievement)
    {
        achievementPanel.difficulty = difficulty;
        achievementPanel.levelIndex = levelIndex;
        achievementPanel.prevAchievment = prevAchievement;
        achievementPanel.achievement = achievement;
        achievementPanel.totalLevels = totalLevels;

        achievementPanel.gameObject.SetActive(true);
    }

    public int CheckLevelIndex(PlayerStats.Difficulty difficulty)
    {
        switch (difficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                return gameSetsEasy.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Normal):
                return gameSets.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Hard):
                return gameSetsHard.IndexOf(currentGameSet) + 1;
            default:
                return 0;
        }
    }

    public int CheckCurrentAchievement()
    {
        int testAchievement;

        if (currentResources >= 300)
        {
            testAchievement = 3;
        }
        else if (currentResources >= 200 && currentResources < 300)
        {
            testAchievement = 2;
        }
        else if (currentResources >= 100 && currentResources < 200)
        {
            testAchievement = 1;
        }
        else
        {
            testAchievement = 0;
        }

        return testAchievement;
    }

    public int CheckPrevAchievement()
    {
        int passedPrevAchievement = 0;

        if (completedSets.Contains(currentGameSet))
        {
            passedPrevAchievement = completedSetsAchievements[completedSets.IndexOf(currentGameSet)];
        }
        
        return passedPrevAchievement;
    }    
}
