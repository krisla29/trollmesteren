﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MG_ModeIndicator : MonoBehaviour
{
    public CanvasGroup canvasGrp;
    public Image bg;
    public Image walkIcon;
    public Image buildIcon;
}
