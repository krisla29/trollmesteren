﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MG_Treasure : MonoBehaviour
{
    public int reward = 100;
    public TextMeshPro rewardText;

    public delegate void MGTreasureEvent(MG_Treasure thisTreasure); //Sends To MazeGame
    public static event MGTreasureEvent OnHitPlayer;

    private void Start()
    {
        rewardText.text = reward.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (OnHitPlayer != null)
                OnHitPlayer(this);
        }
    }
}
