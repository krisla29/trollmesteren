﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MazeGameCanvas : MonoBehaviour
{
    public MazeGame mazeGame;
    public Transform inputBlocker;
    public RectTransform buttonsPanel;
    public Button addItemButton;
    public RectTransform deleteButtonTransform;
    public RectTransform restartButtonTransform;
    public RectTransform addItemPanel;
    public Button toggleUpButton;
    public Button toggleDownButton;
    public MeshFilter addItemButtonMeshFilter;
    public Transform iconScaler;
    public TextMeshProUGUI itemCostText;
    public RectTransform resourcesPanel;
    public TextMeshProUGUI resourcesText;
    public CanvasGroup fader;
    public int currentItemIndex;
    public RectTransform trophyPanel;
    public RectTransform trophy;
    public AudioSource soundSpendMoney;

    [System.Serializable]
    public class Icon
    {
        public Mesh mesh;
        public float size = 1;
        public Vector3 offset;
    }
    public List<Icon> icons = new List<Icon>();
    [System.Serializable]
    public struct CurrentIcon
    {
        public Mesh mesh;
        public int cost;
        public float size;
        public Vector3 offset;
    }

    public List<CurrentIcon> currentIcons = new List<CurrentIcon>();

    public List<RectTransform> uiRects = new List<RectTransform>();
    
    private void OnEnable()
    {
        addItemPanel.anchoredPosition = Vector2.zero;
        deleteButtonTransform.anchoredPosition = Vector2.zero;
        resourcesText.transform.localScale = Vector3.one;
        MazeGame.OnChangeResources += UpdateResources;
    }

    private void OnDisable()
    {
        MazeGame.OnChangeResources -= UpdateResources;
        resourcesText.transform.localScale = Vector3.one;
        addItemPanel.anchoredPosition = Vector2.zero;
        deleteButtonTransform.anchoredPosition = Vector2.zero;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void UpdateResources(int newAmount)
    {
        StartCoroutine(ChangeResources(newAmount));
    }

    private IEnumerator ChangeResources(int newAmount)
    {
        float t = 0;
        float animTime1 = 0.25f;
        
        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            resourcesText.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.5f, h);
            yield return new WaitForEndOfFrame();
        }

        resourcesText.text = newAmount.ToString();

        float ti = 0;
        float animTime2 = 0.25f;

        while (ti < 1)
        {
            ti += Time.deltaTime / animTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            resourcesText.transform.localScale = Vector3.Lerp(Vector3.one * 1.5f, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }

    public void UpdateAvailable(List<MG_GameSet.AvailableItem> passedAvailables)
    {
        currentIcons.Clear();

        for (int i = 0; i < passedAvailables.Count; i++)
        {
            var icon = new CurrentIcon();
            icon.mesh = passedAvailables[i].item.iconMesh;
            icon.cost = passedAvailables[i].cost;
            var size = 1f;
            var offset = Vector3.zero;

            for (int e = 0; e < icons.Count; e++)
            {
                if (icons[e].mesh == icon.mesh)
                {
                    size = icons[e].size;
                    offset = icons[e].offset;
                }                    
            }

            icon.size = size;
            icon.offset = offset;

            currentIcons.Add(icon);
        }

        currentItemIndex = currentIcons.Count - 1;

        UpdateAddItemButton();
    }

    public void UpdateAddItemButton()
    {
        if (currentIcons.Count > 0)
        {
            addItemButtonMeshFilter.sharedMesh = currentIcons[currentItemIndex].mesh;
            iconScaler.transform.localScale = Vector3.one * currentIcons[currentItemIndex].size;
            addItemButtonMeshFilter.transform.localPosition = currentIcons[currentItemIndex].offset;
            itemCostText.text = "$ " + currentIcons[currentItemIndex].cost.ToString();
        }
    }
    
    public void AddItem()
    {
        if (mazeGame.currentResources - currentIcons[currentItemIndex].cost >= 0)
        {
            mazeGame.AddMazeGameItem(mazeGame.currentGameSet.availableItems[currentItemIndex].item);
            mazeGame.addItemSound.Play();

            if (soundSpendMoney)
                soundSpendMoney.Play();
        }
        else
        {
            if(mazeGame.errorSound)
                mazeGame.errorSound.Play();

            StartCoroutine(NotEnoughResources());
        }
    }

    public void DeleteItem()
    {
        if (mazeGame.currentGameItem)
        {
            if (!mazeGame.currentGameItem.itemOnTop && !mazeGame.currentGameItem.playerOnTop)
            {
                mazeGame.DeleteMazeGameItem();
            }
            else
            {
                if (mazeGame.errorSound)
                    mazeGame.errorSound.Play();

                StartCoroutine(CantDeleteItem());
            }
        }
        else if(!mazeGame.currentGameItem && mazeGame.nextGameItem)
        {
            mazeGame.DeleteMazeGameItem();
        }
    }

    public void ToggleUp()
    {
        if(mazeGame.nextGameItem)
        {
            mazeGame.DeleteMazeGameItem();
        }

        if (currentItemIndex < currentIcons.Count - 1)
            currentItemIndex++;
        else
            currentItemIndex = 0;

        mazeGame.clickSound.Play();
        UpdateAddItemButton();
    }

    public void ToggleDown()
    {
        if (mazeGame.nextGameItem)
        {
            mazeGame.DeleteMazeGameItem();
        }

        if (currentItemIndex > 0)
            currentItemIndex--;
        else
            currentItemIndex = currentIcons.Count - 1;

        UpdateAddItemButton();
        mazeGame.clickSound.Play();
    }

    private IEnumerator NotEnoughResources()
    {
        float t = 0;
        float anim1 = 1f;

        while(t < 1)
        {
            t += Time.deltaTime / anim1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            var move = Mathf.Sin(t * (Mathf.PI * 8));
            move *= 15f; //(pixels)
            addItemPanel.anchoredPosition = new Vector2(move, 0);
            yield return new WaitForEndOfFrame();
        }

        addItemPanel.anchoredPosition = Vector2.zero;
    }

    private IEnumerator CantDeleteItem()
    {
        float t = 0;
        float anim1 = 1f;

        while (t < 1)
        {
            t += Time.deltaTime / anim1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            var move = Mathf.Sin(t * (Mathf.PI * 8));
            move *= 15f; //(pixels)
            deleteButtonTransform.anchoredPosition = new Vector2(move, 0);
            yield return new WaitForEndOfFrame();
        }

        deleteButtonTransform.anchoredPosition = Vector2.zero;
    }
}
