﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MazeGameItem : MonoBehaviour
{
    public int itemIndex;
    public MazeGame mazeGameScript;
    public bool isDestroyable = true;
    public MeshRenderer initialMeshrenderer;
    public MeshFilter initialMesh;
    public Mesh iconMesh;
    public List<MeshRenderer> meshRenderers = new List<MeshRenderer>();
    public InteractableBehaviourDTPFree interactableDragToPoints;
    public List<Transform> snapPoints = new List<Transform>();
    public List<Collider> walkTargets = new List<Collider>();
    public Interactable rotationHandle;
    public Material materialLock;
    public Material material;
    public Material materialInit;
    public Material materialSel;
    public Material materialWalkable;
    public Material materialWalkableLocked;
    public Material materialWalkableSelect;
    public Material currentMaterial;
    public MazeGameItemCollider mgCollider;
    public NavMeshSurface mazeGameNavMesh;
    public bool isBlocker;
    public bool isLocked;
    public bool isInitialized;
    public bool isOnTopOfOther; //readOnly
    public bool itemOnTop; //readOnly
    public bool playerOnTop;
    public bool isWalkable; //readOnly
    private float interactableStartSize;
    public EasyRotate easyRotateScript;

    private float addTime = 0;

    private void Start()
    {
        if (isInitialized)
        {

        }
        else
        {
            for (int i = 0; i < meshRenderers.Count; i++)
            {
                meshRenderers[i].gameObject.SetActive(false);
            }

            initialMeshrenderer.gameObject.SetActive(true);
            initialMeshrenderer.material = materialInit;

            if (mazeGameScript.currentGameSet.showDelete)
            {
                if (isDestroyable)
                    mazeGameScript.ShowDeleteBtn();
            }
        }

        interactableStartSize = interactableDragToPoints.interactable.iconSize;

        if (isInitialized)
        {
            if(easyRotateScript)
                easyRotateScript.enabled = false;
        }
    }

    public void InitiatlizeItem() //called by mgCollider
    {
        if (easyRotateScript)
            easyRotateScript.enabled = false;

        transform.localRotation = Quaternion.identity;
        isInitialized = true;

        initialMeshrenderer.gameObject.SetActive(false);

        for (int i = 0; i < meshRenderers.Count; i++)
        {
            meshRenderers[i].gameObject.SetActive(true);
        }
    }

    public void UpdateNavMesh() //called by mgCollider
    {
        mazeGameScript.gameNavMesh.BuildNavMesh();
    }

    public void DeselectCurrentGameItem()
    {
        if (mazeGameScript.currentGameItem.itemOnTop)
        {
            if (!isWalkable)
                mazeGameScript.currentGameItem.ChangeToMaterial(materialLock);
            else
                mazeGameScript.currentGameItem.ChangeToMaterial(materialWalkableLocked);
        }
        else
        {
            if(!isWalkable)
                mazeGameScript.currentGameItem.ChangeToMaterial(material);
            else
                mazeGameScript.currentGameItem.ChangeToMaterial(materialWalkable);
        }

        if (mazeGameScript.currentGameItem.rotationHandle.gameObject.activeSelf)
        {
            mazeGameScript.currentGameItem.rotationHandle.gameObject.SetActive(false);
        }
    }

    public void SetSelected(bool playSound)
    {
        if ((mazeGameScript.currentGameItem.currentMaterial != mazeGameScript.currentGameItem.materialSel) &&
            (mazeGameScript.currentGameItem.currentMaterial != mazeGameScript.currentGameItem.materialWalkableSelect))
        {
            if(playSound)
                mazeGameScript.selectObjectSound.Play();
        }

        if(!mazeGameScript.currentGameItem.isWalkable)
            mazeGameScript.currentGameItem.ChangeToMaterial(materialSel);
        else
            mazeGameScript.currentGameItem.ChangeToMaterial(materialWalkableSelect);

        if (!itemOnTop && !playerOnTop)
            rotationHandle.gameObject.SetActive(true);
        else
            rotationHandle.gameObject.SetActive(false);
       
        mazeGameScript.lastGameItem = this;

        if (mazeGameScript.currentGameSet.showDelete)
        {
            if(isDestroyable)
                mazeGameScript.ShowDeleteBtn();
        }
    }

    public void LeaveStartParent()
    {
        transform.SetParent(mazeGameScript.itemsContainer);

        if (interactableDragToPoints.mainObjectClone)
            interactableDragToPoints.mainObjectClone.transform.SetParent(mazeGameScript.itemsContainer);

        foreach (var item in mazeGameScript.newItemsStartSlots)
        {
            item.gameObject.SetActive(false);
        }

        mazeGameScript.startSlotsParent.gameObject.SetActive(false);
        mazeGameScript.nextGameItem = null;
    }

    private void Update()
    {
        if (mazeGameScript.playerAgent)
            interactableDragToPoints.player = mazeGameScript.playerAgent.transform;

        if (isBlocker)
        {
            addTime += Time.deltaTime;
            currentMaterial.SetTextureOffset("_texcoord", new Vector2(addTime * 0.1f, addTime * 0.05f));
        }

        playerOnTop = interactableDragToPoints.hasPlayerOnTop;
    }

    private void OnEnable()
    {        
        InteractableBehaviourDTPFree.OnDTPFreeItemOnTop += ItemIsOnTop;
        InteractableBehaviourDTPFree.OnDTPFreeNoItem += ItemNotOnTop;
        MazeGame.OnPlayerReachesGoal += CheckPlayerOnTop;
    }

    private void OnDisable()
    {
        InteractableBehaviourDTPFree.OnDTPFreeItemOnTop -= ItemIsOnTop;
        InteractableBehaviourDTPFree.OnDTPFreeNoItem -= ItemNotOnTop;
        MazeGame.OnPlayerReachesGoal -= CheckPlayerOnTop;
    }

    void ItemIsOnTop(Transform passedBehaviour)
    {
        if (passedBehaviour == interactableDragToPoints.transform)
        {
            itemOnTop = true;
            foreach (Transform child in interactableDragToPoints.transform)
            {
                child.transform.localScale = Vector3.zero;
            }

            if (!isLocked && !isBlocker)
            {
                if (!isWalkable)
                    ChangeToMaterial(materialLock);
                else
                    ChangeToMaterial(materialWalkableLocked);
            }
        }
    }

    void ItemNotOnTop(Transform passedBehaviour)
    {
        if (passedBehaviour == interactableDragToPoints.transform)
        {
            itemOnTop = false;
            foreach (Transform child in interactableDragToPoints.transform)
            {
                child.transform.localScale = Vector3.one * interactableStartSize;
            }

            if (!isLocked && !isBlocker)
            {
                if (!isWalkable)
                    ChangeToMaterial(material);
                else
                    ChangeToMaterial(materialWalkableLocked);
            }
        }
    }

    void CheckPlayerOnTop(Transform passedMazeGameItem)
    {
        if (mazeGameScript.currentGameItem)
        {
            if (mazeGameScript.currentGameItem.transform == transform)
            {
                if (rotationHandle.gameObject.activeSelf)
                {
                    bool playerDistCheck = true;

                    foreach (var item in interactableDragToPoints.nestedPoints)
                    {
                        if (!interactableDragToPoints.CheckDistanceToPlayer(item.position))
                            playerDistCheck = false;
                    }

                    print("checked nested points");

                    if (!playerDistCheck)
                        rotationHandle.gameObject.SetActive(false);
                }
            }
        }
    }

    public void ChangeToMaterial(Material newMaterial)
    {
        for (int i = 0; i < meshRenderers.Count; i++)
        {
            meshRenderers[i].material = newMaterial;
        }

        currentMaterial = newMaterial;
    }
}
