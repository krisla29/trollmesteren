﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MG_PlayerLock : MonoBehaviour
{
    public Sprite lockedImage;
    public Sprite unlockedImage;
    public SpriteRenderer spriteGlow;
    public SpriteRenderer sprite;
    public Interactable interactable;
    public NavMeshAgent dummyAgent;
}
