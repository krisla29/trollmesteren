﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MG_RotationHandle : MonoBehaviour
{
    public Transform interactable;
    public float animTime = 0.25f;
    public float scaleMult = 1.5f;


    private void OnEnable()
    {
        Interactable.OnInteractablePressed += CheckClick;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= CheckClick;
        StopAllCoroutines();
    }

    void CheckClick(Transform passedInteractable)
    {
        if (interactable == passedInteractable)
        {
            StopCoroutine(ClickEffect());
            StartCoroutine(ClickEffect());
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator ClickEffect()
    {
        float t = 0;
        float animTimeSplit = animTime * 0.5f;

        while(t < 1)
        {
            t += Time.deltaTime / animTimeSplit;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * scaleMult, h);
            yield return new WaitForEndOfFrame();
        }

        float x = 0;

        while (x < 1)
        {
            x += Time.deltaTime / animTimeSplit;
            float h = x;
            h = x * x * x * (x * (6f * x - 15f) + 10f);
            transform.localScale = Vector3.Lerp(Vector3.one * scaleMult, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        transform.localScale = Vector3.one;
    }
}
