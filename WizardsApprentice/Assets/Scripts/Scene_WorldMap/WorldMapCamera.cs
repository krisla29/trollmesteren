﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WorldMapCamera : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Slider zoomSlider;
    public Camera thisCam;
    public WorldMapCanvas worldMapCanvas;
    private float startFOV;
    public Vector3 startPos;
    public InputManager InputManager;
    private float lastPosX;
    private float lastPosZ;
    public List<RectTransform> uiRects = new List<RectTransform>();

    public bool blockInput; // Controlled by HUD_Map_Demo
    public bool lockPan; // Controlled by HUD_Map_Demo

    public float lastZoom;
    public Vector3 lastCamPos;

    private void Awake()
    {
        startFOV = thisCam.fieldOfView;
        startPos = transform.position;
        lastCamPos = startPos;
        lastZoom = zoomSlider.value;
    }

    private void OnEnable()
    {
        InputManager.OnInputMouseDown += SetLastValues;
        transform.position = startPos;
    }

    void OnDisable()
    {
        InputManager.OnInputMouseDown -= SetLastValues;
        transform.position = startPos;
    }

    void SetLastValues()
    {
        lastZoom = zoomSlider.value;
        lastCamPos = transform.position;
    }

    private void Update()
    {
        if(!blockInput)
            Zoom();

        if(InputManager.isPressing && !blockInput)
            Pan();
    }

    void Zoom()
    {
        float zoom = (zoomSlider.value - 1) * 20f;
        thisCam.fieldOfView = startFOV + zoom;
    }

    public void RemoteZoom() // Controlled by MapDemo
    {
        float zoom = (zoomSlider.value - 1) * 20f;
        thisCam.fieldOfView = startFOV + zoom;
    }

    void Pan()
    {
        var mousePosition = thisCam.ScreenToViewportPoint(Input.mousePosition);
        mousePosition -= Vector3.one * 0.5f;

        var mouseSpeed = new Vector3(-mousePosition.y, 0, mousePosition.x);

        //mouseSpeed *= (Time.deltaTime * 50f);

        if(InteractionCheck() && !lockPan)
            transform.position = Vector3.Lerp(transform.position, startPos + (mouseSpeed * 150f), Time.deltaTime);

        lastPosX = mousePosition.x;
        lastPosZ = mousePosition.z;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Hello");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("GoodBye");
    }

    bool InteractionCheck()
    {
        var pos = Input.mousePosition;
        bool check = true;

        for (int i = 0; i < uiRects.Count; i++)
        {
            Vector2 inversePos = uiRects[i].InverseTransformPoint(pos);

            if (uiRects[i].rect.Contains(inversePos))
                check = false;
        }

        return check;
    }
}
