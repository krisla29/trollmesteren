﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLocationButtonDemo : MonoBehaviour
{
    public WorldMapCanvas worldMapCanvasScript;
    public MapLocationButton startLocationButton;
    public MapDemo mapDemoScript;
    //public Transform scene;

    //public delegate void WorldMapEvent(int index); //Sends to PlayerStats
    //public static event WorldMapEvent OnTriggerLocation;

    private void OnMouseDown()
    {
        mapDemoScript.FinishDemo();
    }

    public void TravelToStart() // Called by MapDemo
    {
        worldMapCanvasScript.TravelToLocation(startLocationButton.transform.parent, null);
        transform.gameObject.SetActive(false);
    }
}
