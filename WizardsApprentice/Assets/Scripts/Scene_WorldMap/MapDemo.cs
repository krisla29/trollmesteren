﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapDemo : MonoBehaviour
{
    public GameManager gameManager;
    public MapObject mapObjectScript;
    public GraphicRaycaster mapSceneRaycaster;
    public WorldMapCamera worldMapCamera;
    public Slider mapSceneZoomSlider;
    public Transform mapCamTransform;
    public Transform mapLocations;
    public Animator anim;
    public Image pointer;
    public Collider RayBlocker;
    public Canvas spriterCanvas;
    public RectTransform spriterPos1;
    public RectTransform spriterPos2;
    public RectTransform spriterPos3;
    public SpriterController spriterController;
    public float waitTime = 4f;

    public bool hasPanned;
    private bool isZooming;
    public bool hasZoomed;
    private bool isClicking;
    public bool hasClickedTravelPoint;
    private bool isClosing;

    private int panHash;
    private int zoomHash;
    private int travelHash;
    private int closeHash;

    private int currentHash;

    private void Awake()
    {
        panHash = Animator.StringToHash("PanDemo");
        zoomHash = Animator.StringToHash("ZoomDemo");
        travelHash = Animator.StringToHash("TravelPointDemo");
        closeHash = Animator.StringToHash("CloseDemo");
        currentHash = panHash;
    }

    private void Start()
    {
        StartDemo();
        //StartCoroutine(Animation());
    }

    private void Update()
    {
        if (hasPanned && !isZooming)
        {
            StartCoroutine(Step(zoomHash));
            currentHash = zoomHash;
            isZooming = true;
        }
        if (hasPanned && isZooming && hasZoomed && !isClicking)
        {
            StartCoroutine(Step(travelHash));
            currentHash = travelHash;
            isClicking = true;
        }
        if (hasPanned && isZooming && hasZoomed && isClicking && hasClickedTravelPoint && !isClosing)
        {
            StartCoroutine(Step(closeHash));
            currentHash = closeHash;
            isClosing = true;
        }
    }

    private void OnEnable()
    {
        InputManager.OnInputMouseUp += CheckLastInteraction;
        //MapLocationButton.OnTriggerLocation += ExitDemo;
    }

    private void OnDisable()
    {
        InputManager.OnInputMouseUp -= CheckLastInteraction;
        //MapLocationButton.OnTriggerLocation -= ExitDemo;

        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void CheckLastInteraction()
    {
        if(!hasPanned)
        {
            if (Vector3.Distance(worldMapCamera.transform.position, worldMapCamera.lastCamPos) > 1f)
                hasPanned = true;
        }
        if(!hasZoomed)
        {
            if (Mathf.Abs(worldMapCamera.zoomSlider.value - worldMapCamera.lastZoom) > 0.1f)
                hasZoomed = true;
        }
    }

    public void TravelPointClick()
    {
        if (hasZoomed && !hasClickedTravelPoint)
            hasClickedTravelPoint = true;
    }

    private IEnumerator Step(int hash)
    {
        //StopCoroutine(Animation());
        pointer.gameObject.SetActive(false);


        Vector3 startScale = spriterController.transform.localScale;
        float x = 0;
        float fadeTime = 2f;

        while(x < 1)
        {
            x += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(x);
            //pointer.color = Color.Lerp(Color.white, new Vector4(1,1,1,0), h);
            spriterController.transform.localScale = Vector3.Lerp(startScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1f);
       
        float t = 0;
        float animTime1 = 2;

        Vector3 startPos = spriterController.transform.localPosition;
        
        Vector3 endPos = startPos;
        float curZoom = worldMapCamera.zoomSlider.value;
        Vector3 camCurPos = worldMapCamera.transform.position;
        bool scaleSpriter = false;

        if (hash == zoomHash)
            endPos = spriterPos2.localPosition;
        else if (hash == travelHash)
        {
            endPos = spriterPos3.localPosition;
            mapLocations.gameObject.SetActive(true);
            worldMapCamera.blockInput = true;
            worldMapCamera.zoomSlider.gameObject.SetActive(false);
        }
        else if (hash == closeHash)
        {
            scaleSpriter = true;
            gameManager.hudCanvas.worldMapButton.gameObject.SetActive(true);
        }

        //anim.SetTrigger(hash);
        //StartCoroutine(Animation());
        anim.SetBool(currentHash, true);
        pointer.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / animTime1;
            var h = LerpUtils.SmootherStep(t);

            if (!scaleSpriter)
            {
                spriterController.transform.localPosition = Vector3.Lerp(startPos, endPos, h);
                spriterController.transform.localScale = Vector3.Lerp(Vector3.zero, startScale, h);

                if (hash == travelHash)
                {
                    worldMapCamera.zoomSlider.value = Mathf.Lerp(curZoom, 1, h);
                    worldMapCamera.transform.position = Vector3.Lerp(camCurPos, worldMapCamera.startPos, h);
                    worldMapCamera.RemoteZoom();
                }
            }
            else
            {
                spriterController.transform.localScale = Vector3.Lerp(startScale, Vector3.zero, h);
                gameManager.hudCanvas.worldMapButton.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            }

            //pointer.color = Color.Lerp(new Vector4(1,1,1,0), Color.white, h);

            yield return new WaitForEndOfFrame();
        }

        //yield return new WaitForSeconds(waitTime);
        
        //StartCoroutine(Animation());
    }
    /*
    private IEnumerator Animation()
    {
        anim.SetTrigger(currentHash);

        pointer.gameObject.SetActive(true);

        //while (anim.GetCurrentAnimatorStateInfo(0).normalizedTime < 1)
        //{
            //yield return new WaitForEndOfFrame();
        //}

        //pointer.gameObject.SetActive(false);
        
        yield return new WaitForSeconds(waitTime * 2);
        
        StartCoroutine(Animation());
    }
    */
    public void FinishDemo() //Called by MapLocationButtonDemo
    {
        //StopAllCoroutines();

        if (gameManager)
        {
            gameManager.isMapDemo = false;

            if(gameManager.playerStats)
                gameManager.playerStats.hasUnlockedMap = true;

            
            
            if (gameManager.hudCanvas)
            {
                if(gameManager.hudCanvas.worldMapButton.gameObject)
                    gameManager.hudCanvas.worldMapButton.gameObject.SetActive(true);

                if(gameManager.hudCanvas.mapBtnPulser.gameObject)
                    gameManager.hudCanvas.mapBtnPulser.enabled = true;
            }
            
        }

        if (spriterCanvas)
            spriterCanvas.gameObject.SetActive(false);

        if (worldMapCamera)
        {
            worldMapCamera.zoomSlider.gameObject.SetActive(true);
            worldMapCamera.blockInput = false;
        }

        if (mapObjectScript)
            mapObjectScript.sceneCollider.transform.localScale = mapObjectScript.sceneColliderStartScale;

        transform.gameObject.SetActive(false);
    }
    /*
    void ExitDemo(int n)
    {
        if (spriterCanvas)
            spriterCanvas.gameObject.SetActive(false);        

        if (worldMapCamera)
            worldMapCamera.blockInput = false;

        transform.gameObject.SetActive(false);
    }
    */
    void StartDemo()
    {
        gameManager.hudCanvas.trophyButton.gameObject.SetActive(false);
        gameManager.hudCanvas.worldMapButton.gameObject.SetActive(false);
        mapLocations.gameObject.SetActive(false);
        spriterCanvas.gameObject.SetActive(true);
        pointer.gameObject.SetActive(true);
        spriterController.transform.localPosition = spriterPos1.localPosition;
    }
}
