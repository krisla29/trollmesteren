﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMapButton : MonoBehaviour
{
    public SceneController worldMapScene;
    public GameManager gameManager;
    public Image mapImage;
    public bool mapIsOn;
    private float lerpTime = 0.1f;

    public delegate void WorldMapButtonEvent(); //Sends To MapObject script
    public static event WorldMapButtonEvent OnPressWMButton;

    public Color on = Color.red;
    public Color off = Color.yellow;

    public void ToggleMap()
    {
        //if (!mapIsOn && !gameManager.isScene) //  && !gameManager.isGameEvent
        if (!mapIsOn) //  && !gameManager.isGameEvent
        {
            mapIsOn = true;
            StartCoroutine(LerpCollider(gameManager.player.transform.position));
            mapImage.color = on;
        }
        else
        {
            mapIsOn = false;
            StartCoroutine(LerpCollider(gameManager.player.transform.position + Vector3.up * 100f));
            mapImage.color = off;
        }

        if (OnPressWMButton != null)
            OnPressWMButton();
    }

    public void SetToOff()
    {
        mapIsOn = false;
        StartCoroutine(LerpCollider(gameManager.player.transform.position + Vector3.up * 100f));
        mapImage.color = off;
    }

    private IEnumerator LerpCollider(Vector3 destination)
    {
        float t = 0;
        Vector3 currentPos = worldMapScene.sceneCollider.transform.position; 

        while(t < 1)
        {
            t += Time.deltaTime / lerpTime;
            worldMapScene.sceneCollider.transform.position = Vector3.Lerp(currentPos, destination, t);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
