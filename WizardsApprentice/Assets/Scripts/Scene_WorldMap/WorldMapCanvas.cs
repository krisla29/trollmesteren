﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMapCanvas : MonoBehaviour
{
    public GameManager gameManager;
    public CameraSceneManager cameraSceneManager;
    public ProximityManager proximityManager;
    public SpriteRenderer playerIndicator;
    public WorldMapController worldMapController;
    public WorldMapButton worldMapButton;
    public AudioSource soundClick;

    public HUD_Map_Demo mapDemoScript;
    public Canvas spriterCanvas;
    
    public List<Transform> locations = new List<Transform>();
    public List<int> visitedLocations = new List<int>();
    public Transform bossLocation;


    private void OnEnable()
    {
        if (gameManager.player)
        {
            playerIndicator.gameObject.SetActive(true);
            playerIndicator.transform.position = gameManager.player.transform.position;
        }

        SetLayers(true);

        foreach (var item in locations)
        {
            if (item)
            {
                if (visitedLocations.Contains(locations.IndexOf(item)))
                    item.GetChild(0).GetChild(0).gameObject.SetActive(true);
            }
        }

        if(gameManager.isMapDemo)
        {
            if(!mapDemoScript.gameObject.activeSelf)
                mapDemoScript.gameObject.SetActive(true);
            if(!spriterCanvas.gameObject.activeSelf)
                spriterCanvas.gameObject.SetActive(true);
        }
        else
        {
            mapDemoScript.gameObject.SetActive(false);
            spriterCanvas.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        DisableMapObjects();
    }

    public void DisableMapObjects()
    {
        SetLayers(false);

        foreach (var item in locations)
        {
            if (item)
            {
                if (item.childCount > 0)
                {
                    if (item.GetChild(0).childCount > 0)
                    {
                        item.GetChild(0).GetChild(0).gameObject.SetActive(false);
                    }
                }
            }
        }

        playerIndicator.gameObject.SetActive(false);
    }

    public void TravelToLocation(Transform passedLocation, Transform passedScene)
    {
        if (visitedLocations.Contains(locations.IndexOf(passedLocation)))
        {
            var location = locations[locations.IndexOf(passedLocation)];

            gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;

            if (soundClick)
                soundClick.Play();

            if (passedScene != null)
            {
                cameraSceneManager.MapEnterScene(passedScene);
                gameManager.player.navMeshAgent.Warp(location.position);
            }
            else
            {
                gameManager.player.navMeshAgent.Warp(location.position);
                gameManager.player.transform.rotation = location.rotation;
            }

            worldMapButton.worldMapScene.sceneCollider.transform.position = gameManager.player.transform.position + (Vector3.up * 100f);
            proximityManager.CheckPulse();
            worldMapButton.mapIsOn = false;
            worldMapButton.mapImage.color = worldMapButton.off;
        }
    }

    public void SetLayers(bool isMap)
    {
        foreach (var item in locations)
        {
            if (item)
            {
                if (isMap)
                    item.GetChild(0).gameObject.layer = 9;
                else
                    item.GetChild(0).gameObject.layer = 2;
            }
        }
    }    
}
