﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMapController : MonoBehaviour
{
    public GameManager gameManager;
    public WorldMapCanvas worldMapCanvas;
    public RectTransform worldMapButton;
    public RectTransform worldMapButtonParent;
    public Image worldMapButtonImage;
    private Color startColor;
    public AudioSource soundNewLocation;
    public AudioSource soundMapButton;
    public GameObject newLocationWorldEffect;
    public GameObject newLocationUIEffect;

    private Coroutine newLocation;

    public void InitializeLocations()
    {
        startColor = worldMapButtonImage.color;

        for (int i = 0; i < gameManager.playerStats.unlockedLocations.Count; i++)
        {
            if (gameManager.playerStats.unlockedLocations[i] >= 0 && gameManager.playerStats.unlockedLocations[i] < worldMapCanvas.locations.Count)
                worldMapCanvas.visitedLocations.Add(gameManager.playerStats.unlockedLocations[i]);
        }

        worldMapCanvas.DisableMapObjects();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        newLocation = null;

        if (worldMapButton)
        {
            worldMapButton.transform.localScale = Vector3.one;
            worldMapButton.transform.localEulerAngles = Vector3.zero;
        }

        if(worldMapButtonImage)
            worldMapButtonImage.color = startColor;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        newLocation = null;
    }

    public void NewLocation(Vector3 position)
    {
        if (newLocation != null)
            StopCoroutine(newLocation);

        newLocation = StartCoroutine(AddNewLocation(position));
    }

    private IEnumerator AddNewLocation(Vector3 position)
    {
        GameObject worldEffect = null;
        GameObject uiEffect = null;

        if (newLocationWorldEffect)
        {
            worldEffect = Instantiate(newLocationWorldEffect, gameManager.worldEffectsContainer);
            worldEffect.transform.position = position;
        }

        if (soundNewLocation)
            soundNewLocation.Play();

        yield return new WaitForSeconds(2);

        if (newLocationUIEffect)
        {
            uiEffect = Instantiate(newLocationUIEffect, gameManager.uiEffectsContainer);
            var trans = uiEffect.GetComponent<RectTransform>();
            trans.position = worldMapButton.position;
        }

        if (soundMapButton)
            soundMapButton.Play();

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1f;
            var h = LerpUtils.SmootherStep(t);
            var s = Mathf.Sin(h * Mathf.PI);
            worldMapButton.transform.localScale = Vector3.one + (Vector3.one * s * 0.25f);
            worldMapButton.transform.localEulerAngles = new Vector3(0, 0, s * 45f);
            worldMapButtonImage.color = startColor + new Color(s * 0.25f, 0, 0, 0);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(3);
    }
}
