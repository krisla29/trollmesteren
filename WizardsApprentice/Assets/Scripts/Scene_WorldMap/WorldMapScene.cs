﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapScene : MonoBehaviour
{
    public delegate void WorldMapEvent(); //Sends to GameManager
    public static event WorldMapEvent OnWorldMapOn;
    public static event WorldMapEvent OnWorldMapOff;

    private void OnEnable()
    {
        if(OnWorldMapOn != null)
        {
            OnWorldMapOn();
        }
    }

    private void OnDisable()
    {
        if (OnWorldMapOff != null)
        {
            OnWorldMapOff();
        }
    }    
}
