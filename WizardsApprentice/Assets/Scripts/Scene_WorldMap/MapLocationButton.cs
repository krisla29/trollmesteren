﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapLocationButton : MonoBehaviour
{
    public WorldMapCanvas worldMapCanvasScript;
    public Transform scene;

    public delegate void WorldMapEvent(int index); //Sends to PlayerStats
    public static event WorldMapEvent OnTriggerLocation;

    private void OnMouseDown()
    {
        worldMapCanvasScript.TravelToLocation(transform.parent, scene);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(!worldMapCanvasScript.visitedLocations.Contains(worldMapCanvasScript.locations.IndexOf(transform.parent)))
            {
                int index = worldMapCanvasScript.locations.IndexOf(transform.parent);

                worldMapCanvasScript.visitedLocations.Add(index);    
                
                if(index != 0)
                    worldMapCanvasScript.worldMapController.NewLocation(transform.parent.position);

                if (OnTriggerLocation != null)
                    OnTriggerLocation(worldMapCanvasScript.locations.IndexOf(transform.parent));
            }

            worldMapCanvasScript.gameManager.playerStats.lastLocation = worldMapCanvasScript.locations.IndexOf(transform.parent);
            worldMapCanvasScript.gameManager.playerStats.SaveGame();
        }
    }
}
