﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleChoiceHolder : MonoBehaviour
{
    public List<MultipleChoiceTask> easyTasks = new List<MultipleChoiceTask>();
    public List<MultipleChoiceTask> normalTasks = new List<MultipleChoiceTask>();
    public List<MultipleChoiceTask> hardTasks = new List<MultipleChoiceTask>();
}
