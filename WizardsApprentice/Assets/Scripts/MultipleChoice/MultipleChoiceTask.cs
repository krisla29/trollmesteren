﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleChoiceTask : MonoBehaviour
{
    public string question;
    public string rightAnswer;
    public List<string> wrongAnswers = new List<string>();
}
