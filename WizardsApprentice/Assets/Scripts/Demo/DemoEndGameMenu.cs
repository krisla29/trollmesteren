﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DemoEndGameMenu : MonoBehaviour
{
    public Transform interactable;
    public RectTransform panel;
    public Button closeButton;
    public Button exitGameButton;
    public AudioSource openPanelSound;

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += CheckInteractable;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= CheckInteractable;
        StopAllCoroutines();
    }

    void CheckInteractable(Transform passedInteractable)
    {
        if(passedInteractable == interactable)
        {
            OpenPanel();
        }
    }

    void OpenPanel()
    {
        if (openPanelSound)
            openPanelSound.Play();

        panel.gameObject.SetActive(true);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
