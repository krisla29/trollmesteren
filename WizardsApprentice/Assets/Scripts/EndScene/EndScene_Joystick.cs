﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EndScene_Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Camera cam;
    public Transform objectToMove;
    public List<RectTransform> uiRects = new List<RectTransform>();
    public float speedMult = 150f;
    public float lerpBackTime = 1f;
    public bool isPressed;
    private float lastPosX;
    private float lastPosY;
    private Vector3 startPos;
    private Coroutine lerpBack;

    private void OnMouseDown()
    {
        isPressed = true;
        startPos = objectToMove.localPosition;
    }

    private void OnMouseDrag()
    {
        
    }

    private void OnMouseUp()
    {
        isPressed = false;
        startPos = objectToMove.localPosition;

        if (lerpBack != null)
            StopCoroutine(lerpBack);

        lerpBack = StartCoroutine(MoveObjectBack());
    }

    private void Update()
    {
        if (isPressed)
            Pan();
    }

    void Pan()
    {
        var mousePosition = cam.ScreenToViewportPoint(Input.mousePosition);
        mousePosition -= Vector3.one * 0.5f;

        var mouseSpeed = new Vector3(mousePosition.x, mousePosition.y, 0);

        //mouseSpeed *= (Time.deltaTime * 50f);

        if (InteractionCheck())
            //objectToMove.localPosition = Vector3.Lerp(objectToMove.localPosition, objectToMove.localPosition + (mouseSpeed * speedMult), Time.deltaTime);
            objectToMove.Translate((mouseSpeed * speedMult) * Time.deltaTime);

        lastPosX = mousePosition.x;
        lastPosY = mousePosition.y;
    }

    bool InteractionCheck()
    {
        var pos = Input.mousePosition;
        bool check = true;

        for (int i = 0; i < uiRects.Count; i++)
        {
            Vector2 inversePos = uiRects[i].InverseTransformPoint(pos);

            if (uiRects[i].rect.Contains(inversePos))
                check = false;
        }

        return check;
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }

    private IEnumerator MoveObjectBack()
    {
        float t = 0;

        Vector3 startPosLoc = objectToMove.localPosition;

        while(t < 1)
        {
            t += Time.deltaTime / lerpBackTime;
            var h = LerpUtils.SmootherStep(t);
            objectToMove.localPosition = Vector3.Lerp(startPosLoc, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
