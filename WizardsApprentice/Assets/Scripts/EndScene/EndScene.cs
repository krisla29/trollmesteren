﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScene : MonoBehaviour
{
    public Camera introCam;
    public Camera mainCam;
    public EndSceneCharacterSwitcher endSceneCharacterSwitcherScript;
    public CanvasGroup canvasGrp;

    public GameObject effectPrefab;
    public Transform effectSpawner;
    public Transform effectsContainer;
    private int maxEffects = 5;
    public List<Transform> currentEffects = new List<Transform>();
    public float timeToSpawn = 10f;

    public Transform fireWorksParent;
    public List<GameObject> fireWorksPrefabs = new List<GameObject>();
    public AudioSource soundGetStar;
    public AudioSource soundCharYay;
    public List<AudioSource> soundsLaunch = new List<AudioSource>();
    public List<AudioSource> soundsBoom = new List<AudioSource>();
    public List<AudioSource> soundsCelebrate = new List<AudioSource>();

    private void Awake()
    {
        introCam.gameObject.SetActive(true);
        mainCam.gameObject.SetActive(false);
        endSceneCharacterSwitcherScript.gameObject.SetActive(false);
    }

    private void Start()
    {
        StartCoroutine(Intro());
        StartCoroutine(SpawnEffects());
    }

    private IEnumerator Intro()
    {
        float t = 0;
        float animTime1 = 5f;
        Vector3 startPos = introCam.transform.position;
        Vector3 startRot = introCam.transform.localEulerAngles;

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            var h = LerpUtils.SmootherStep(t);
            introCam.transform.position = Vector3.Lerp(startPos, startPos + (-Vector3.up * 40), h);
            introCam.transform.localEulerAngles = Vector3.Lerp(startRot, startRot + (Vector3.up * 180), t);
            canvasGrp.alpha = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float animTime2 = 5f;
        Vector3 startPos2 = introCam.transform.position;
        Quaternion startRot2 = introCam.transform.rotation;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / animTime2;
            var h = LerpUtils.SmootherStep(t2);
            introCam.transform.position = Vector3.Lerp(startPos2, mainCam.transform.position, h);
            introCam.transform.rotation = Quaternion.Slerp(startRot2, mainCam.transform.rotation, h);
            yield return new WaitForEndOfFrame();
        }

        introCam.gameObject.SetActive(false);
        mainCam.gameObject.SetActive(true);
        endSceneCharacterSwitcherScript.gameObject.SetActive(true);
    }

    private IEnumerator SpawnEffects()
    {
        yield return new WaitForSeconds(timeToSpawn);


        if (currentEffects.Count < maxEffects)
        {
            int randNum = Random.Range(-2, 10);
            var effect = Instantiate(effectPrefab, effectSpawner.transform.position + (Vector3.up * randNum), Quaternion.identity, effectsContainer);
            var effectScript = effect.AddComponent<EndSceneEffect>();
            effectScript.player = endSceneCharacterSwitcherScript.follower;
            currentEffects.Add(effect.transform);
        }


        if (currentEffects.Count > 0)
        {
            while (currentEffects[currentEffects.Count - 1] == null)
            {
                currentEffects.Remove(currentEffects[currentEffects.Count - 1]);
            }

            while (currentEffects.Count > maxEffects)
            {
                var effectToKill = currentEffects[currentEffects.Count - 1].gameObject;
                currentEffects.Remove(currentEffects[currentEffects.Count - 1]);
                Destroy(effectToKill);
            }
        }

        StartCoroutine(SpawnEffects());
    }

    private void OnEnable()
    {
        EndSceneEffect.OnPlayerHit += TriggerNewEffect;
    }

    private void OnDisable()
    {
        EndSceneEffect.OnPlayerHit -= TriggerNewEffect;
        StopAllCoroutines();
    }

    void TriggerNewEffect(Transform passedTransform)
    {
        var effect = passedTransform.gameObject;
        currentEffects.Remove(passedTransform);
        Vector3 pos = passedTransform.position;
        print("killed " + effect.name);
        Destroy(effect);

        if (soundGetStar)
            soundGetStar.Play();

        if (soundCharYay)
            soundCharYay.Play();

        StartCoroutine(SpawnFireWorks());
    }

    private IEnumerator SpawnFireWorks()
    {
        for (int i = 0; i < 4; i++)
        {
            float randWait = Random.Range(0.05f, 0.2f);

            yield return new WaitForSeconds(randWait);

            int randPrefab = Random.Range(0, fireWorksPrefabs.Count);

            var fireWorks = Instantiate(fireWorksPrefabs[randPrefab], fireWorksParent.position, Quaternion.identity, effectsContainer);

            int randLaunch = Random.Range(0, soundsLaunch.Count);

            soundsLaunch[randLaunch].Play();
            StartCoroutine(Boom());

            float randX = Random.Range(-10, 10);
            float randZ = Random.Range(-10, 10);
            float randY = Random.Range(10, 20);

            var fw_rb = fireWorks.GetComponent<Rigidbody>();
            Vector3 dir = new Vector3(randX, randY, randZ);

            fw_rb.AddForce(dir, ForceMode.Impulse);
        }

        StartCoroutine(Celebrate());
    }

    private IEnumerator Boom()
    {
        yield return new WaitForSeconds(1.25f);

        int randBoom = Random.Range(0, soundsBoom.Count);

        soundsBoom[randBoom].Play();
    }

    private IEnumerator Celebrate()
    {
        yield return new WaitForSeconds(1.25f);

        int randCel = Random.Range(0, soundsCelebrate.Count);

        soundsCelebrate[randCel].Play();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
