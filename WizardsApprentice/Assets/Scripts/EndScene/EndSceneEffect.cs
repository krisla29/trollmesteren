﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneEffect : MonoBehaviour
{
    public Transform player;
    private bool isHit;

    public delegate void EffectEvent(Transform thisTransform); // Sends to EndScene script
    static public event EffectEvent OnPlayerHit;
   
    private void Update()
    {
        if (!isHit && player)
        {
            Vector3 offset = player.position - transform.position;
            float sqrLen = offset.sqrMagnitude;

            if (sqrLen < 4f)
            {
                if (OnPlayerHit != null)
                    OnPlayerHit(transform);

                isHit = true;
            }
        }
    }
}
