﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EndScene_Interactable : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Camera cam;
    public Transform objectToMove;
    public List<RectTransform> uiRects = new List<RectTransform>();
    public float speedMult = 150f;
    public float lerpBackTime = 1f;
    public bool isPressed;
    private float lastPosX;
    private float lastPosY;
    private Vector3 startPos;
    private Coroutine lerpBack;
    private Coroutine lerpBackCam;

    public Transform pointer;

    private Ray ray;
    private RaycastHit hit;
    private Vector3 comparePoint;

    void Awake()
    {
        pointer.gameObject.SetActive(false);
    }

    private void OnMouseDown()
    {
        isPressed = true;

        if (lerpBack != null)
            StopCoroutine(lerpBack);

        if (lerpBackCam != null)
            StopCoroutine(lerpBackCam);

        startPos = objectToMove.localPosition;

        pointer.gameObject.SetActive(true);
    }

    private void OnMouseDrag()
    {
        pointer.position = comparePoint;
    }

    private void OnMouseUp()
    {
        isPressed = false;
        startPos = objectToMove.localPosition;

        if (lerpBack != null)
            StopCoroutine(lerpBack);

        lerpBack = StartCoroutine(MoveObjectBack());

        if (lerpBackCam != null)
            StopCoroutine(lerpBackCam);

        lerpBackCam = StartCoroutine(LerpBackCam());

        pointer.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (isPressed)
        {
            Pan();
        }
    }

    private void LateUpdate()
    {
        if (isPressed)
        {
            PanCam();
        }
    }
    /*
    void Pan2()
    {
        var mousePosition = cam.ScreenToViewportPoint(Input.mousePosition);
        mousePosition -= Vector3.one * 0.5f;

        var mouseSpeed = new Vector3(mousePosition.x, mousePosition.y, 0);

        //mouseSpeed *= (Time.deltaTime * 50f);

        if (InteractionCheck())
            //objectToMove.localPosition = Vector3.Lerp(objectToMove.localPosition, objectToMove.localPosition + (mouseSpeed * speedMult), Time.deltaTime);
            objectToMove.Translate((mouseSpeed * speedMult) * Time.deltaTime);

        lastPosX = mousePosition.x;
        lastPosY = mousePosition.y;
    }
    */
    void Pan()
    {  
        ray = ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == transform)
            {
                comparePoint = hit.point;

                if (InteractionCheck())
                    objectToMove.position = Vector3.Lerp(objectToMove.position, comparePoint, Time.deltaTime * 10f);
            }
        }
    }

    void PanCam()
    {
        Vector3 camPos = new Vector3(cam.transform.InverseTransformPoint(comparePoint).x, cam.transform.InverseTransformPoint(comparePoint).y, cam.transform.localPosition.z);

        if (camPos.y > 0)
            camPos = new Vector3(camPos.x, camPos.y * 1.5f, camPos.z);

        cam.transform.localPosition = Vector3.Lerp(cam.transform.localPosition, camPos, Time.deltaTime * 0.5f);
    }

    bool InteractionCheck()
    {
        var pos = Input.mousePosition;
        bool check = true;

        for (int i = 0; i < uiRects.Count; i++)
        {
            Vector2 inversePos = uiRects[i].InverseTransformPoint(pos);

            if (uiRects[i].rect.Contains(inversePos))
                check = false;
        }

        return check;
    }

    public void OnPointerDown(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }

    private IEnumerator MoveObjectBack()
    {
        float t = 0;

        Vector3 startPosLoc = objectToMove.localPosition;

        while(t < 1)
        {
            t += Time.deltaTime / lerpBackTime;
            var h = LerpUtils.SmootherStep(t);
            objectToMove.localPosition = Vector3.Lerp(startPosLoc, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator LerpBackCam()
    {
        float t = 0;

        Vector3 startPosCam = cam.transform.localPosition;

        while (t < 1)
        {
            t += Time.deltaTime / (lerpBackTime * 3f);
            var h = LerpUtils.SmootherStep(t);
            cam.transform.localPosition = Vector3.Lerp(startPosCam, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
