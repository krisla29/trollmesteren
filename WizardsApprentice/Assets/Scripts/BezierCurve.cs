﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierCurve : MonoBehaviour
{    
    [System.Serializable]
    public class SlideObject
    {
        public Transform slideObject;
        [Range(0f, 100f)] public float pathProgression;
        public int currentControlPoint; //ReadOnly - Refers to last control Point passed
        public bool rotateWithPath;
    }
    [Header("SLIDE OBJECTS")]
    public List<SlideObject> slideObjects = new List<SlideObject>();
    public List<Vector3> lastPositions = new List<Vector3>();
    public bool updateSlideObjects = true;
    private bool slideObjectsInitialized;
    [Header("PATH")]
    public LineRenderer lineRenderer;
    public bool lRUpdateInScene;
    public bool lRUpdateInPlayMode;
    private bool lrInitialized;
    public bool showManipulators = true;
    public bool lerpBetweenPoints;
    public Transform controlPointsParent;
    public bool updatePath;
    private bool pathInitialized;
    public enum CurveTypes { Linear, Quadratic, Cubic }
    public CurveTypes curveType;
    public List<Transform> controlPoints = new List<Transform>();
    public List<Transform> handles = new List<Transform>();
    public int curvePoints = 50;
    public bool[] lockTangents = new bool[3];

    public int pointsPerSegment;
    public int difference;
    [HideInInspector] public Vector3[] pointPositions = new Vector3[50];
    
    private int lastCurvePoints;
    private CurveTypes lastCurveType;

    private void OnEnable()
    {
        lastPositions.Clear();

        for (int i = 0; i < slideObjects.Count; i++)
        {
            lastPositions.Add(slideObjects[i].slideObject.transform.position);
        }
    }

    void UpdateCurve()
    {
        if (curveType != lastCurveType)
        {
            InitializeHandles(curveType);
        }
           
        pointsPerSegment = Mathf.RoundToInt(curvePoints / (controlPoints.Count - 1));
        difference = curvePoints - (pointsPerSegment * (controlPoints.Count - 1));

        if (curvePoints != lastCurvePoints)
        {
            pointPositions = new Vector3[curvePoints];                
        }

        if (lockTangents.Length != controlPoints.Count - 2)
        {
            lockTangents = new bool[controlPoints.Count - 2];
        }

        DrawCurve();
        CheckTangents();
        /*
        if (lerpBetweenPoints)
            UpdateLastPositions();

        PositionSliderObject();        
        */
        lastCurvePoints = curvePoints;
        lastCurveType = curveType;

    }

    void UpdateSlideObjects()
    {
        if (lerpBetweenPoints)
            UpdateLastPositions();

        PositionSliderObject();        
    }

    private void Update()
    {
        if (updatePath)
            UpdateCurve();
        else
        {
            if (!pathInitialized)
            {
                UpdateCurve();
                pathInitialized = true;
            }
        }

        if (updateSlideObjects)
            UpdateSlideObjects();
        else
        {
            if(!slideObjectsInitialized)
            {
                UpdateSlideObjects();
                slideObjectsInitialized = true;
            }
        }

        if (lineRenderer)
        {
            if(lRUpdateInPlayMode)
                UpdateLineRenderer();
            else
            {
                if (!lrInitialized)
                {
                    UpdateLineRenderer();
                    lrInitialized = true;
                }
            }
        }
    }

    void UpdateLineRenderer()
    {
        lineRenderer.enabled = true;

        lineRenderer.positionCount = curvePoints;

        for (int i = 0; i < difference; i++)
        {
            pointPositions[pointPositions.Length - i - 1] = controlPoints[0].position;
            //pointPositions[pointPositions.Length - i - 1] = controlPoints[controlPoints.Count - 1].position;
        }

        lineRenderer.SetPositions(pointPositions);
    }
    
    void InitializeHandles(CurveTypes newCurveType)
    {
        if(newCurveType == CurveTypes.Linear)
        {
            SetHandlesVisibility(CurveTypes.Linear);
        }
        if(newCurveType == CurveTypes.Quadratic)
        {
            SetHandlesVisibility(CurveTypes.Quadratic);

            for (int i = 0; i < controlPoints.Count - 1; i++)
            {
                if (i % 2 == 0)
                {
                    var handle = handles[i];
                }
            }               
        }
        if (newCurveType == CurveTypes.Cubic)
        {
            SetHandlesVisibility(CurveTypes.Cubic);

            for (int i = 0; i < controlPoints.Count - 1; i++)
            {
                var handle1 = handles[i];
                var handle2 = handles[i + 1];
            }
        }
    }

    void SetHandlesVisibility(CurveTypes nCurveType)
    {
        switch(nCurveType)
        {
            case (CurveTypes.Linear):
                for (int i = 0; i < handles.Count; i++)
                {
                    handles[i].gameObject.SetActive(false);
                }
                break;
            case (CurveTypes.Quadratic):
                for (int i = 0; i < handles.Count; i++)
                {
                    handles[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < handles.Count; i++)
                {
                    if(i%2 == 0)
                        handles[i].gameObject.SetActive(true);
                }
                break;
            case (CurveTypes.Cubic):
                for (int i = 0; i < handles.Count; i++)
                {
                    handles[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < handles.Count; i++)
                {
                        handles[i].gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }
    }

    void CheckTangents()
    {
        if (curveType == CurveTypes.Cubic)
        {
            int lockCounter = -1;

            for (int i = 0; i < handles.Count; i++)
            {
                if (i > 0 && i < handles.Count - 1)
                {
                    if (i % 2 == 1)
                    {
                        lockCounter++;

                        if(lockTangents[lockCounter])
                        {
                            handles[i].transform.localPosition = -handles[i + 1].transform.localPosition;
                            handles[i + 1].transform.localPosition = -handles[i].transform.localPosition;
                        }
                    }
                }
            }
        }
    }

    void DrawCurve()
    {
        switch (curveType)
        {
            case (CurveTypes.Linear):
                DrawLinearCurve();
                break;
            case (CurveTypes.Quadratic):
                DrawQuadraticCurve();
                break;
            case (CurveTypes.Cubic):
                DrawCubicCurve();
                break;
            default:
                break;
        }
    }

    void DrawLinearCurve()
    {
        for (int x = 1; x < controlPoints.Count; x++)
        {
            for (int i = 1; i < pointsPerSegment + 1; i++)
            {
                float t = i / (float)pointsPerSegment;
                int numerator = ((x - 1) * pointsPerSegment) + i - 1;

                if(x < controlPoints.Count)
                    pointPositions[numerator] = CalculateLinearBezierPoints(t, controlPoints[x - 1].position, controlPoints[x].position);
                else
                    pointPositions[numerator] = CalculateLinearBezierPoints(t, controlPoints[x - 2].position, controlPoints[x - 1].position);
            }
        }
    }

    void DrawQuadraticCurve()
    {
        for (int x = 1; x < controlPoints.Count; x++)
        {
            for (int i = 1; i < pointsPerSegment + 1; i++)
            {
                float t = i / (float)pointsPerSegment;
                int numerator = ((x - 1) * pointsPerSegment) + i - 1;

                if (x == 1)
                    pointPositions[numerator] = CalculateQuadraticBezierPoints(t, controlPoints[x - 1].position, handles[0].position, controlPoints[x].position);
                else if (x > 1 && x < controlPoints.Count)
                    pointPositions[numerator] = CalculateQuadraticBezierPoints(t, controlPoints[x - 1].position, handles[(x * 2) - 2].position, controlPoints[x].position);
                else if (x == controlPoints.Count)
                    pointPositions[numerator] = CalculateQuadraticBezierPoints(t, controlPoints[x - 2].position, handles[handles.Count - 1].position, controlPoints[x - 1].position);
            }
        }
    }

    void DrawCubicCurve()
    {
        for (int x = 1; x < controlPoints.Count; x++)
        {
            for (int i = 1; i < pointsPerSegment + 1; i++)
            {
                float t = i / (float)pointsPerSegment;
                int numerator = ((x - 1) * pointsPerSegment) + i - 1;

                if(x == 1)
                    pointPositions[numerator] = CalculateCubicBezierPoints(t, controlPoints[x - 1].position, handles[0].position, handles[1].position, controlPoints[x].position);
                else if (x > 1 && x < controlPoints.Count)
                    pointPositions[numerator] = CalculateCubicBezierPoints(t, controlPoints[x - 1].position, handles[(x * 2) - 2].position, handles[(x * 2) - 1].position, controlPoints[x].position);
                else if (x == controlPoints.Count)
                    pointPositions[numerator] = CalculateCubicBezierPoints(t, controlPoints[x - 2].position, handles[handles.Count - 1].position, handles[handles.Count - 1].position, controlPoints[x - 1].position);
            }
        }
    }

    private Vector3 CalculateLinearBezierPoints(float t, Vector3 p0, Vector3 p1)
    {
        return p0 + (t * (p1 - p0));
    }

    private Vector3 CalculateQuadraticBezierPoints(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;
        return p;
    }

    private Vector3 CalculateCubicBezierPoints(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;
        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;
        return p;
    }

    void PositionSliderObject()
    {
        for (int i = 0; i < slideObjects.Count; i++)
        {
            if (slideObjects[i] != null)
            {
                float fac1 = pointsPerSegment / (100f / (controlPoints.Count - 1));
                float fac2 = slideObjects[i].pathProgression / (controlPoints.Count - 1);
                float deltaProg = slideObjects[i].pathProgression - Mathf.FloorToInt(slideObjects[i].pathProgression);
                int currentPoint = Mathf.RoundToInt(fac1 * fac2 * (controlPoints.Count - 1)) - 1;
                
                if (slideObjects[i].slideObject)
                {
                    if (currentPoint >= 0 && currentPoint <= curvePoints)
                    {
                        if (lerpBetweenPoints)
                            slideObjects[i].slideObject.transform.position = Vector3.Lerp(slideObjects[i].slideObject.transform.position, pointPositions[currentPoint], Time.deltaTime);
                            //slideObjects[i].slideObject.transform.position = Vector3.Lerp(lastPositions[i], pointPositions[currentPoint], Time.deltaTime);                                                        
                        else
                            slideObjects[i].slideObject.transform.position = pointPositions[currentPoint];

                        if (currentPoint < pointsPerSegment)
                            slideObjects[i].currentControlPoint = 0;
                        else
                            slideObjects[i].currentControlPoint = Mathf.FloorToInt(currentPoint / pointsPerSegment);                        
                    }
                }

                if(slideObjects[i].rotateWithPath)
                {
                
                    var lookDirection = Vector3.zero;

                    if (currentPoint > 0 && currentPoint < pointPositions.Length - 1)
                        lookDirection = pointPositions[currentPoint + 1] - pointPositions[currentPoint];
                    else if (currentPoint == 0)
                        lookDirection = pointPositions[1] - pointPositions[0];
                    else if (currentPoint == pointPositions.Length - 1)
                        lookDirection = pointPositions[1] - pointPositions[0];

                    if (lookDirection == Vector3.zero)
                        lookDirection = pointPositions[1] - pointPositions[0];

                    slideObjects[i].slideObject.transform.rotation = Quaternion.LookRotation((lookDirection), Vector3.up);
                }
            }
        }
    }

    void UpdateLastPositions()
    {
        if(lastPositions.Count != slideObjects.Count)
        {
            lastPositions.Clear();

            for (int i = 0; i < slideObjects.Count; i++)
            {
                lastPositions.Add(slideObjects[i].slideObject.transform.position);
            }
        }
        else
        {
            for (int i = 0; i < lastPositions.Count; i++)
            {
                lastPositions[i] = slideObjects[i].slideObject.transform.position;
            }
        }
    }

    void OnDrawGizmos()
    {
        if (controlPoints.Count >= 2 && handles.Count == (controlPoints.Count * 2) - 2)
        {
            UpdateCurve();
        }
        else if(handles.Count != (controlPoints.Count * 2) - 2)
        {
            print("You need to set handles count to: (controlPoints.Count * 2) - 2 !");
        }
        else if(controlPoints.Count < 2)
        {
            print("You need to set number of controlPoints to two or more!");
        }

        if(lRUpdateInScene)
        {
            if(lineRenderer)
            {
                UpdateLineRenderer();
            }
        }
        else
        {
            lineRenderer.enabled = false;
        }
        if (showManipulators)
        {
            foreach (Transform item in handles)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawSphere(item.position, 0.25f);
            }
            for (int i = 0; i < controlPoints.Count; i++)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(controlPoints[i].position, Vector3.one / 2);
            }
            for (int x = 1; x < controlPoints.Count; x++) //Skips the last controlPoint
            {
                for (int i = 1; i < pointsPerSegment + 1; i++)
                {
                    int numerator = ((x - 1) * pointsPerSegment) + i - 1;
                    Gizmos.color = Color.gray;

                    if (numerator < (((controlPoints.Count - 1) * pointsPerSegment) - 1))
                        Gizmos.DrawLine(pointPositions[numerator], pointPositions[numerator + 1]);
                }
            }
            if (curveType != CurveTypes.Linear)
            {
                Gizmos.color = Color.yellow;
                int counter = 0;

                for (int i = 0; i < handles.Count; i++)
                {
                    if (i == 0)
                    {
                        if (curveType == CurveTypes.Quadratic)
                        {
                            Gizmos.DrawLine(handles[i].position, controlPoints[counter].transform.position);
                            Gizmos.DrawLine(handles[i].position, controlPoints[counter + 1].transform.position);
                        }
                        if (curveType == CurveTypes.Cubic)
                        {
                            Gizmos.DrawLine(handles[i].position, controlPoints[counter].transform.position);
                        }
                    }

                    if (i > 0 && i < handles.Count - 1)
                    {
                        if (i % 2 == 1)
                        {
                            counter++;

                            if (curveType == CurveTypes.Quadratic)
                            {
                                Gizmos.DrawLine(handles[i + 1].position, controlPoints[counter].transform.position);
                                Gizmos.DrawLine(handles[i + 1].position, controlPoints[counter + 1].transform.position);
                            }
                            if (curveType == CurveTypes.Cubic)
                            {
                                Gizmos.DrawLine(handles[i].position, controlPoints[counter].transform.position);
                                Gizmos.DrawLine(handles[i + 1].position, controlPoints[counter].transform.position);
                            }
                        }
                    }

                    if (i == handles.Count - 1)
                    {
                        if (curveType == CurveTypes.Cubic)
                        {
                            Gizmos.DrawLine(handles[handles.Count - 1].position, controlPoints[controlPoints.Count - 1].transform.position);
                        }
                    }
                }
            }
        }
    }
}


