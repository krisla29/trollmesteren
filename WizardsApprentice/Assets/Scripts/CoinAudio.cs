﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAudio : MonoBehaviour
{
    public AudioSource coinSound;
    public AudioSource coinLoseSound;

    public void OnEnable()
    {
        CoinCollector.OnAddCoin += PlayCoinSound;
        CoinCollector.OnLoseCoin += PlayLoseCoinSound;
    }

    public void OnDisable()
    {
        CoinCollector.OnAddCoin -= PlayCoinSound;
        CoinCollector.OnLoseCoin -= PlayLoseCoinSound;
    }

    public void PlayCoinSound()
    {
        coinSound.Play();
    }
    public void PlayLoseCoinSound()
    {
        coinLoseSound.Play();
    }
}
