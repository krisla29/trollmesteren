﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainTop : MonoBehaviour
{
    public GameManager gameManager;
    public AudioSource portalAudio;
    public SceneController bossGameScene;

    private void Start()
    {
        gameManager.isOnMountainTop = false;

        if (portalAudio.transform.parent.gameObject.activeSelf)
            portalAudio.Stop();
        //gameManager.player.spriter.gameObject.SetActive(true);
        gameManager.wizardController.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        CharacterSelector.OnChangeCharacter += CheckShowWizard;
    }

    private void OnDisable()
    {
        CharacterSelector.OnChangeCharacter -= CheckShowWizard;
    }

    void CheckShowWizard(CharacterSelector.PlayerCharacter passedCharacter)
    {
        if(gameManager.isOnMountainTop)
        {
            if(passedCharacter != CharacterSelector.PlayerCharacter.Wizard)
            {
                if(!gameManager.wizardController.gameObject.activeSelf)
                    gameManager.wizardController.gameObject.SetActive(true);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            print("Checked MountainTop!");
            //gameManager.CheckMountainTop();
            
            if (!gameManager.isOnMountainTop)
            {
                gameManager.player.spriter.gameObject.SetActive(false);

                if (gameManager.currentScene != bossGameScene)
                {
                    if (gameManager.player.characterSelector.playerCharacter != CharacterSelector.PlayerCharacter.Wizard)
                        gameManager.wizardController.gameObject.SetActive(true);
                }
            }

            gameManager.isOnMountainTop = true;

            if (portalAudio.transform.parent.gameObject.activeSelf)
                portalAudio.Play();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            print("Checked MountainTop!");
            //gameManager.CheckMountainTop();
            
            if (gameManager.isOnMountainTop)
            {
                if (gameManager.currentScene != bossGameScene)
                {
                    if (gameManager.player.characterSelector.playerCharacter != CharacterSelector.PlayerCharacter.Wizard)
                        gameManager.player.spriter.gameObject.SetActive(true);                   
                }

                gameManager.wizardController.gameObject.SetActive(false);
            }

            gameManager.isOnMountainTop = false;

            if (portalAudio.transform.parent.gameObject.activeSelf)
                portalAudio.Stop();
        }
    }
}
