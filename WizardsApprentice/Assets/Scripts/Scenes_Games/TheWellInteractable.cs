﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheWellInteractable : MonoBehaviour
{
    public TheWell theWellScript;
    public Transform interactable;

    private void OnEnable()
    {
        if (!theWellScript.isFinished)
        {
            if(interactable)
                interactable.gameObject.SetActive(true);
        }
        else
        {
            if (interactable)
                interactable.gameObject.SetActive(false);
        }

        TheWell.OnStepUnlock += CheckIfFinished;
    }

    private void OnDisable()
    {
        if(interactable)
            interactable.gameObject.SetActive(false);

        TheWell.OnStepUnlock -= CheckIfFinished;
    }

    void CheckIfFinished()
    {
        if (theWellScript.isFinished)
        {
            if(interactable)
                interactable.gameObject.SetActive(false);
        }
    }
}
