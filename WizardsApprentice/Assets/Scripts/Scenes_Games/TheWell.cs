﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using System.Linq;

public class TheWell : MonoBehaviour
{
    public GameManager gameManager;
    public SceneController sceneController;
    public Interactable activationArea;
    public List<Transform> steps = new List<Transform>();
    public List<Transform> obstacles = new List<Transform>();
    private List<bool> unlockedSteps = new List<bool>();
    public int stepCounter;
    public int currentStep;
    public float stepDepth = 2.5f;
    //public List<Transform> tokens = new List<Transform>();
    public Spawner gemSpawner;
    public SpawnerTrophy trophySpawner;
    public AudioSource wellGainsGems;

    public int gemsRequired1 = 400;
    public int gemsRequired2 = 600;
    public int stepGemCost = 40;
    public int gemCounter;
    public int currentGemsCounter;

    public bool gemsRequired1Reached = false;
    public bool gemsRequired2Reached = false;
    //public Transform tokensParent;
    //public GameObject tokenBirthEffect;
    //public GameObject tokenKillEffect;
    public Transform mainCamKey;
    public Transform firstStepCamKey;
    public Transform camKeyInbetween;
    public Camera sceneCam;
    public Follower2 camFollower;
    private Vector3 camStartPos;
    private Quaternion camStartRot;
    public Canvas canvas;
    public TextMeshProUGUI canvasText;
    public ParticleSystem wellParticles;
    public AudioSource stonePush;
    public Color startColor = Color.white;
    public Color nextColor = Color.yellow;
    public Color successColor = Color.green;
    public Color finishedColor = Color.magenta;
    public Color errorColor = Color.red;
    public Color currentColor;
    public float colorFadeTime = 2;
    public float stepMoveTime = 2;
    //public Collider killer;
    //public int tokenIndex;   
    public bool isFinished;
    public bool isDemo;
    public bool isReversed;
    public bool isFirstStep = true;
    private bool hasSeenFirstStepAnim;

    private Coroutine firstTimeAnimation;
    private Coroutine fader;
    private List<Coroutine> lerps = new List<Coroutine>();
    //public int matchCounter;
    public delegate void TheWellEvent(); //Sends to TheWellInteractable
    public static event TheWellEvent OnStepUnlock;    

    private void Awake()
    {
        camStartPos = sceneCam.transform.position;
        camStartRot = sceneCam.transform.rotation;

        for (int i = 0; i < steps.Count; i++)
        {
            lerps.Add(null);
        }        
    }

    void Start()
    {
        InitializeSteps();
        canvasText.text = gemCounter.ToString() + "\n" + gemsRequired1.ToString();
        stepCounter = 0;
       
        ChangeParticleColor(startColor, false);
        //TransferSavedProgress();
    }

    private void OnEnable()
    {
        //TheWellKiller.OnAddGem += AddGem;
    }
    private void OnDisable()
    {
        //TheWellKiller.OnAddGem -= AddGem;       

        StopAllCoroutines();
        firstTimeAnimation = null;
        fader = null;
        for (int i = 0; i < lerps.Count; i++)
        {
            lerps[i] = null;
        }

        CheckUnlocked();
        sceneCam.transform.position = camStartPos;
        sceneCam.transform.rotation = camStartRot;
        camFollower.target = mainCamKey;

        if(gameManager.hudCanvas.exitButton)
            gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
    }

    private void SuccessGem()
    {       
        stepCounter += 1;

        if (!isFinished)
        {
            if (!isFirstStep)
            {
                if (lerps[stepCounter - 1] != null)
                    StopCoroutine(lerps[stepCounter - 1]);

                lerps[stepCounter - 1] = StartCoroutine(UnlockStep(steps[stepCounter - 1]));
            }
            else
            {
                {
                    if (firstTimeAnimation != null)
                        StopCoroutine(firstTimeAnimation);

                    firstTimeAnimation = StartCoroutine(UnlockFirstStep());
                    //gameManager.playerStats.hasSeenTheWellFirstStep = true;
                }
            }
        }

        if(isReversed && !isFinished)
            obstacles[stepCounter - 1].gameObject.SetActive(false);
        
        if (stepCounter == steps.Count)
        {
            isFinished = true;
        }

        if (OnStepUnlock != null)
        {
            OnStepUnlock();
        }
    }

    public void AddGem()
    {
        gemCounter += 1;
        currentGemsCounter += 1;
        //gameManager.playerStats.wellGems++;

        if(!wellGainsGems.isPlaying)
            wellGainsGems.Play();

        if ((gemsRequired1Reached && !gemsRequired2Reached && gemCounter <= gemsRequired2)
            || (!gemsRequired1Reached && !gemsRequired2Reached && gemCounter <= gemsRequired2 + gemsRequired1))
        {
            if (currentGemsCounter >= stepGemCost)
            {
                SuccessGem();
                currentGemsCounter = 0;
            }
        }

        if (!gemsRequired1Reached)
        {
            canvasText.text = gemCounter.ToString() + "\n" + gemsRequired1.ToString();

            if (gemCounter >= gemsRequired1)
            {
                gemCounter -= gemsRequired1;
                gemsRequired1Reached = true;
            }
        }

        //if(gemsRequired1Reached && !gemsRequired2Reached && !isDemo)
        if (gemsRequired1Reached && !gemsRequired2Reached && !isDemo)
        {
            canvasText.text = gemCounter.ToString() + "\n" + gemsRequired2.ToString();

            if (gemCounter >= gemsRequired2)
            {
                gemCounter -= gemsRequired2;
                gemsRequired2Reached = true;
            }
        }
        /*
        if(gemsRequired1Reached && gemsRequired2Reached && !isDemo)
        {
            canvasText.text = gemCounter.ToString();
        }
        */
        if (gemsRequired1Reached && isDemo)
        {
            //canvasText.text = gemsRequired1.ToString() + "\n" + gemsRequired1.ToString();
        }
        
    }

    void AddSavedGems()
    {
        gemCounter += 1;
        currentGemsCounter += 1;

        if ((gemsRequired1Reached && !gemsRequired2Reached && gemCounter <= gemsRequired2) 
            || (!gemsRequired1Reached && !gemsRequired2Reached && gemCounter <= gemsRequired2 + gemsRequired1))
        {
            if (currentGemsCounter >= stepGemCost)
            {
                stepCounter += 1;
                //print(stepCounter);
                //print(gemCounter);
                var startPos = steps[stepCounter - 1].localPosition;
                steps[stepCounter - 1].localPosition += (-steps[stepCounter - 1].right * stepDepth);
                steps[stepCounter - 1].gameObject.SetActive(false);
                currentGemsCounter = 0;
            }
        }

        if (!gemsRequired1Reached)
        {
            canvasText.text = gemCounter.ToString() + "\n" + gemsRequired1.ToString();

            if (gemCounter >= gemsRequired1)
            {
                gemCounter -= gemsRequired1;
                gemsRequired1Reached = true;
            }
        }

        if (gemsRequired1Reached && !gemsRequired2Reached && !isDemo)
        {
            canvasText.text = gemCounter.ToString() + "\n" + gemsRequired2.ToString();

            if (gemCounter >= gemsRequired2)
            {
                gemCounter -= gemsRequired2;
                gemsRequired2Reached = true;
            }
        }        
    }

    void InitializeSteps()
    {
        if (isReversed)
        {
            foreach (var item in steps)
            {
                item.position += (-item.right * stepDepth);
                item.gameObject.SetActive(false);
            }
            foreach (var item in obstacles)
            {
                item.position += (-item.right * stepDepth);
            }
        }

        for (int i = 0; i < steps.Count; i++)
        {
            unlockedSteps.Add(false);
        }
    }

    public void ChangeParticleColor(Color color, bool isFading)
    {
        var col = wellParticles.colorOverLifetime;
        col.enabled = true;
        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(0.5f, 0.5f), new GradientAlphaKey(0.0f, 1f) });

        col.color = grad;
        currentColor = color;

        if (isFading)
        {
            if (fader != null)
                StopCoroutine(fader);

            fader = StartCoroutine(FadeParticleColor(color));
        }
    }

    private IEnumerator FadeParticleColor(Color color)
    {
        print("FADING COROUTINE");
        var col = wellParticles.colorOverLifetime;
        col.enabled = true;

        float elapsedTime = 0;
        Gradient currentGrad = new Gradient();

        while (elapsedTime < colorFadeTime)
        {
            currentColor = Color.Lerp(currentColor, startColor, (elapsedTime / colorFadeTime * 0.1f));
            currentGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(currentColor, 0.0f), new GradientColorKey(currentColor, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(0.5f, 0.5f), new GradientAlphaKey(0.0f, 1f) });
            col.color = currentGrad;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    private IEnumerator UnlockStep(Transform step)
    {
        while (!hasSeenFirstStepAnim)
            yield return new WaitForEndOfFrame();

        step.gameObject.SetActive(true);

        unlockedSteps[steps.IndexOf(step)] = true;
        
        if(stonePush)
            stonePush.Play();

        print("MOVING COROUTINE");

        float t = 0;
        var startPos = step.localPosition;
        var newPos = step.localPosition + (-step.right * stepDepth);

        if (isReversed)
            newPos = step.localPosition + (step.right * stepDepth);

        while (t < 1)
        {
            t += Time.deltaTime / stepMoveTime;
            step.localPosition = Vector3.Lerp(startPos, newPos, t);            
            yield return new WaitForEndOfFrame();
        }

        if (!isReversed)
            step.gameObject.SetActive(false);
    }

    private IEnumerator UnlockFirstStep()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;

        var step = steps[0];
        step.gameObject.SetActive(true);

        unlockedSteps[0] = true;
        isFirstStep = false;
        
        print("MOVING FIRST COROUTINE");

        yield return new WaitForSeconds(1);

        camFollower.target = camKeyInbetween;

        yield return new WaitForSeconds(2f);

        camFollower.target = firstStepCamKey;

        yield return new WaitForSeconds(4);
        /*
        float x = 0;
        float anim1 = 4f;

        while(x < 1)
        {
            x += Time.deltaTime / anim1;
            var h = LerpUtils.SmootherStep(x);
            sceneCam.transform.position = Vector3.Lerp(camStartPos, camKey.position, h);
            sceneCam.transform.rotation = Quaternion.Slerp(camStartRot, camKey.rotation, h);
        }
        */

        if(stonePush)
            stonePush.Play();

        hasSeenFirstStepAnim = true;

        float t = 0;
        var startPos = step.localPosition;
        var newPos = step.localPosition + (-step.right * stepDepth);

        if (isReversed)
            newPos = step.localPosition + (step.right * stepDepth);

        while (t < 1)
        {
            t += Time.deltaTime / stepMoveTime;
            step.localPosition = Vector3.Lerp(startPos, newPos, t);
            yield return new WaitForEndOfFrame();
        }

        if (!isReversed)
            step.gameObject.SetActive(false);

        camFollower.target = camKeyInbetween;

        yield return new WaitForSeconds(2f);

        camFollower.target = mainCamKey;

        yield return new WaitForSeconds(2f);
        /*
        if (!isReversed)
            step.gameObject.SetActive(false);

        float y = 0;
        float anim2 = 3f;

        while (y < 1)
        {
            y += Time.deltaTime / anim2;
            var h = LerpUtils.SmootherStep(y);
            sceneCam.transform.position = Vector3.Lerp(camKey.position, camStartPos, h);
            sceneCam.transform.rotation = Quaternion.Slerp(camKey.rotation, camStartRot, h);
        }
        */
        gameManager.playerStats.hasSeenTheWellFirstStep = true;
        //gameManager.playerStats.SaveGame();
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        if (gameManager.currentScene == sceneController)
            gameManager.hudCanvas.exitButton.gameObject.SetActive(true);        
    }    

    void CheckUnlocked()
    {
        if (isReversed)
        {
            for (int i = 0; i < unlockedSteps.Count; i++)
            {
                if(unlockedSteps[i] == true)
                {
                    steps[i].gameObject.SetActive(true);
                }
            }
        }
        else
        {
            for (int i = 0; i < unlockedSteps.Count; i++)
            {
                if (unlockedSteps[i] == true)
                {
                    steps[i].gameObject.SetActive(false);
                }
            }
        }
    }

    public void TransferSavedProgress()
    {
        hasSeenFirstStepAnim = gameManager.playerStats.hasSeenTheWellFirstStep;
        isFirstStep = !hasSeenFirstStepAnim;

        for (int i = 0; i < gameManager.playerStats.wellGems; i++)
        {
            AddSavedGems();
        }

        if (gameManager.playerStats.wellGems >= gemsRequired1 + gemsRequired2)
            isFinished = true;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
