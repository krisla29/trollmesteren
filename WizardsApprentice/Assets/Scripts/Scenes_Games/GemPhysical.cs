﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemPhysical : MonoBehaviour
{
    public Rigidbody rb;

    void Start()
    {
        rb.AddForce(Vector3.up * 5f, ForceMode.Impulse);
    }    
}
