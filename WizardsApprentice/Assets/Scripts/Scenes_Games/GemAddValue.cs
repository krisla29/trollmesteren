﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemAddValue : MonoBehaviour
{
    public int value = 49; // 50 minus default 1
    private GameManager gameManager;
    private bool hasAddedValue;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Spriter"))
        {
            if (!hasAddedValue)
            {
                //gameManager.playerStats.collectedGems += value;
                gameManager.playerStats.gems += value;
                hasAddedValue = true;
            }
        }
    }
}
