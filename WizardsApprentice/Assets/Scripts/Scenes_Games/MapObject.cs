﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapObject : MonoBehaviour
{
    public GameManager gameManager;
    public Camera sceneCam;
    public Collider sceneCollider;
    [HideInInspector]public Vector3 sceneColliderStartScale;
    public RectTransform mapButton;
    public RectTransform hudButnParent;
    public SpriteRenderer mapTransferImage;
    public SpriteRenderer mapBeforeImage;
    public SpriteRenderer mapAfterImage;
    public SpriteRenderer glow;
    public SpriteRenderer bgSprite;
    public SpriteRenderer interactableIcon;
    public Transform beforeObject;
    public Transform afterObject;
    public SkinnedMeshRenderer map3dMesh;
    public Collider interactionCollider;
    public Collider playerTrigger;
    public bool isUnlocked;
    public bool hasOpenedMap;

    public Color mapStartColor;
    public Color mapGlowColor;
    public Color mapEndColor;
    public AudioSource soundStonePush;
    public AudioSource soundMagic;
    public AudioSource taskComplete;
    public AudioSource soundGainMap;
    public AudioSource soundButton;

    private void Awake()
    {
        mapBeforeImage.color = mapStartColor;
        mapAfterImage.color = mapEndColor;
        sceneColliderStartScale = sceneCollider.transform.localScale;
    }

    private void OnEnable()
    {
        CheckState();
        WorldMapButton.OnPressWMButton += SetHasOpenedMap;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        WorldMapButton.OnPressWMButton -= SetHasOpenedMap;
    }

    void SetHasOpenedMap()
    {
        hasOpenedMap = true;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void CheckState()
    {
        if (gameManager.playerStats.hasUnlockedMap)
            isUnlocked = true;

        if(isUnlocked)
        {
            beforeObject.gameObject.SetActive(false);
            afterObject.gameObject.SetActive(true);
        }
        else
        {
            beforeObject.gameObject.SetActive(true);
            afterObject.gameObject.SetActive(false);
        }
    }

    public void ActivateMap()
    {
        StartCoroutine(ActivationAnimation());
    }

    private void OnMouseDown()
    {
        if(!isUnlocked)
        {            
            ActivateMap();
            interactionCollider.enabled = false;
        }
    }

    private IEnumerator ActivationAnimation()
    {
        gameManager.player.navMeshAgent.SetDestination(gameManager.player.transform.position);
        gameManager.player.isLocked = true;
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        float t1 = 0;
        float anim1 = 4f;
        float dist1 = 0.3f;
        Vector3 startPos1 = beforeObject.position;
        Vector3 map3dInitialPos = map3dMesh.transform.parent.position;

        if (soundStonePush)
            soundStonePush.Play();

        if (soundMagic)
            soundMagic.Play();

        if (taskComplete)
            taskComplete.Play();

        interactableIcon.gameObject.SetActive(false);
        glow.gameObject.SetActive(true);

        while(t1 < 1)
        {
            t1 += Time.deltaTime / anim1;
            var h = LerpUtils.SmootherStep(t1);
            beforeObject.position = Vector3.Lerp(startPos1, startPos1 + (-beforeObject.transform.right * dist1), h);
            map3dMesh.transform.parent.position = Vector3.Lerp(map3dInitialPos, map3dInitialPos + (-map3dMesh.transform.parent.transform.right * dist1), h);

            if (t1 < 0.5f)
                mapBeforeImage.color = Color.Lerp(mapStartColor, mapGlowColor, t1 * 2f);
            else
                mapBeforeImage.color = Color.Lerp(mapGlowColor, mapEndColor, (t1 - 0.5f) * 2f);

            yield return new WaitForEndOfFrame();
        }

        glow.gameObject.SetActive(false);        
        bgSprite.gameObject.SetActive(true);
        gameManager.player.anim.SetTrigger("Success");

        float t2 = 0;
        float anim2 = 2f;

        Vector3 tiStartPos = mapTransferImage.transform.position;
        Quaternion tiStartRot = mapTransferImage.transform.rotation;
        Vector3 map3dStartScale = map3dMesh.transform.parent.localScale;
        Vector3 map3dStartPos = map3dMesh.transform.parent.position;
        Quaternion map3dStartRot = map3dMesh.transform.parent.rotation;
        Color startCol = mapTransferImage.color;
        float camDist = 4f;

        //mapTransferImage.gameObject.SetActive(true);        

        while(t2 < 1)
        {
            t2 += Time.deltaTime / anim2;
            var h = LerpUtils.SmootherStep(t2);
            mapTransferImage.transform.position = Vector3.Lerp(tiStartPos, sceneCam.transform.position + (sceneCam.transform.forward * camDist), h);
            mapTransferImage.transform.rotation = Quaternion.Slerp(tiStartRot, sceneCam.transform.rotation, h);
            map3dMesh.transform.parent.position = Vector3.Lerp(map3dStartPos, sceneCam.transform.position + (sceneCam.transform.forward * camDist) + (sceneCam.transform.right * 0.25f), h);
            map3dMesh.transform.parent.localScale = Vector3.Lerp(map3dStartScale, map3dStartScale * 0.75f, h);
            map3dMesh.transform.parent.rotation = Quaternion.Slerp(map3dStartRot, sceneCam.transform.rotation * Quaternion.AngleAxis(90, Vector3.up) * Quaternion.AngleAxis(20, Vector3.forward), h);            
            mapTransferImage.color = Color.Lerp(startCol * 0, startCol, h);
            yield return new WaitForEndOfFrame();
        }

        if (soundGainMap)
            soundGainMap.Play();        

        float x = 0;
        float animX = 2f;
        Vector3 map3dMidPos = map3dMesh.transform.parent.position;
        Quaternion map3dMidRot = map3dMesh.transform.parent.rotation;

        while (x < 1)
        {
            x += Time.deltaTime / animX;
            var h = LerpUtils.SmootherStep(x);
            map3dMesh.SetBlendShapeWeight(0, h * 99f);
            var s = Mathf.Sin(h * Mathf.PI);
            map3dMesh.transform.parent.position = map3dMidPos + Vector3.up * (s * 0.5f);
            s *= -20f;           
            map3dMesh.transform.parent.rotation = map3dMidRot * Quaternion.AngleAxis(s, Vector3.right) * Quaternion.AngleAxis(360 * h, Vector3.up);
            yield return new WaitForEndOfFrame();
        }

        float t3 = 0;
        float anim3 = 2f;
        
        Vector3 tiCurPos = mapTransferImage.transform.position;
        Vector3 tiCurScale = mapTransferImage.transform.localScale;
        Quaternion tiCurRot = mapTransferImage.transform.rotation;
        Vector3 map3dCurPos = map3dMesh.transform.parent.position;
        Vector3 map3dCurScale = map3dMesh.transform.parent.localScale;
        Quaternion map3dCurRot = map3dMesh.transform.parent.rotation;
        
        float endScale = 0.25f;
        mapButton.gameObject.SetActive(true);

        while (t3 < 1)
        {
            t3 += Time.deltaTime / anim3;
            var h = LerpUtils.SmootherStep(t3);
            mapTransferImage.transform.position = Vector3.Lerp(tiCurPos, mapButton.transform.position, h);
            mapTransferImage.transform.localScale = Vector3.Lerp(tiCurScale, (Vector3.one * endScale), h);
            map3dMesh.transform.parent.position = Vector3.Lerp(map3dCurPos, mapButton.transform.position, h);
            //map3dMesh.transform.parent.rotation = map3dCurRot * Quaternion.AngleAxis((-20 * h), Vector3.right);
            map3dMesh.transform.parent.localScale = Vector3.Lerp(map3dCurScale, Vector3.zero, h);
            //map3dMesh.SetBlendShapeWeight(0, 50 + (h * 49.5f));
            yield return new WaitForEndOfFrame();
        }        

        mapTransferImage.gameObject.SetActive(false);
        map3dMesh.transform.parent.gameObject.SetActive(false);

        isUnlocked = true;
        beforeObject.gameObject.SetActive(false);
        afterObject.gameObject.SetActive(true);

        gameManager.isMapDemo = true;
        gameManager.player.isLocked = false;
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);

        float t4 = 0;
        float anim4 = 0.5f;
        Vector3 sceneCollScale = sceneCollider.transform.localScale;

        while (t4 < 1)
        {
            t4 += Time.deltaTime / anim4;
            sceneCollider.transform.localScale = Vector3.Lerp(sceneCollScale, Vector3.zero, t4);
            yield return new WaitForEndOfFrame();
        }
    }
}
