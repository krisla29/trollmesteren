﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class MapLock : MonoBehaviour
{
    public GameManager gameManager;
    public AudioSource soundError;
    public AudioSource soundComplete;
    public AudioSource soundUIclick;

    public SpriteRenderer lockBow;
    public SpriteRenderer lockPad;
    public SpriteRenderer blob;
    public Color colorUnlock;
    public NavMeshObstacle navMeshObstacle;

    private bool hasStartedCorrectSeq;

    private void OnEnable()
    {
        if (gameManager.playerStats.hasUnlockedMap)
            transform.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void Unlock() //Called by Hud_Map_Demo
    {
        blob.color = colorUnlock;
        lockBow.transform.localPosition = new Vector3(0, 0.4f, 0);
        navMeshObstacle.enabled = false;
    }

    private void OnMouseDown()
    {
        if (gameManager.playerStats.hasUnlockedMap)
        {
            if (!hasStartedCorrectSeq)
            {
                StartCoroutine(UnlockSequence());
                hasStartedCorrectSeq = true;
            }
        }
        else
            StartCoroutine(ErrorSequence());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(gameManager.playerStats.hasUnlockedMap)
            {
                if(!hasStartedCorrectSeq)
                {
                    StartCoroutine(UnlockSequence());
                    hasStartedCorrectSeq = true;
                }
            }
        }
    }

    private IEnumerator UnlockSequence()
    {
        if (soundUIclick)
            soundUIclick.Play();

        Button mapButton = null;

        if (gameManager.hudCanvas.worldMapButton.gameObject.activeSelf)
            mapButton = gameManager.hudCanvas.worldMapButton.GetComponent<Button>();
                    
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;

        if(!gameManager.isScene)
        {
            if (mapButton != null)
                mapButton.enabled = false;

            if (gameManager.hudCanvas.trophyButton.gameObject.activeSelf)
                gameManager.hudCanvas.trophyButton.enabled = false;
        }

        if (soundComplete)
            soundComplete.Play();

        yield return new WaitForSeconds(1);

        lockBow.transform.localPosition = new Vector3(0, 0.4f, 0);
        
        float t = 0;
        float animTime = 1;
        Color locksC = lockPad.color;
        Color blobC = blob.color;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            lockBow.color = Color.Lerp(locksC, new Color(locksC.r, locksC.g, locksC.b, 0), h);
            lockPad.color = Color.Lerp(locksC, new Color(locksC.r, locksC.g, locksC.b, 0), h);
            blob.color = Color.Lerp(blobC, new Color(blobC.r, blobC.g, blobC.b, 0), h);
            yield return new WaitForEndOfFrame();
        }

        //yield return new WaitForSeconds(1);

        if (!gameManager.isScene)
        {
            if (mapButton != null)
                mapButton.enabled = true;

            gameManager.hudCanvas.trophyButton.enabled = true;
        }

        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        transform.gameObject.SetActive(false);
    }

    private IEnumerator ErrorSequence()
    {
        if (soundError)
            soundError.Play();

        float t = 0;
        float animTime = 1f;
        Vector3 pos = lockPad.transform.parent.localPosition;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var s = Mathf.Sin(t * (Mathf.PI * 4));
            s *= 0.4f;
            lockPad.transform.parent.localPosition = new Vector3(s, pos.y, pos.z);
            yield return new WaitForEndOfFrame();
        }
    }
}
