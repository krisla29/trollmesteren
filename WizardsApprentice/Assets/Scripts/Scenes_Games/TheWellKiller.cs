﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheWellKiller : MonoBehaviour
{
    public TheWell theWell;
    public delegate void TheWellGemEvent(); // Sends to The Well & PlayerStats
    public static event TheWellGemEvent OnAddGem;

    public void OnTriggerEnter(Collider other)
    {
        //print(other.gameObject);

        //if (other.gameObject.CompareTag("Player"))
        //{
            //Destroy(other.gameObject);
        //}

        if (!theWell.isFinished)
        {
            if(other.gameObject.CompareTag("Gem"))
            {
                
                /*
                if(OnAddGem != null)
                {
                    OnAddGem();
                }
                */
                Destroy(other.gameObject);
            }                       
        }      
    }
}
