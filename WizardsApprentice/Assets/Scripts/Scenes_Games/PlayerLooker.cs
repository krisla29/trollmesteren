﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLooker : MonoBehaviour
{
    public GameManager gameManager;

    private void Update()
    {
        if(gameManager)
        {
            if(gameManager.player)
            {
                LookAtPlayerHorizontal();
                FollowPlayerVertical();
            }
        }
    }

    void LookAtPlayerHorizontal()
    {
        var dir = gameManager.player.transform.position - transform.position;
        Vector3 lookVector = new Vector3(dir.x, 0, dir.z);
        Quaternion look = Quaternion.LookRotation(lookVector);
        transform.localRotation = look;
    }

    void FollowPlayerVertical()
    {
        var pos = transform.position;
        transform.position = new Vector3(pos.x, gameManager.player.transform.position.y, pos.z);
    }
}
