﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerStats : MonoBehaviour
{
    public bool debugMode;
    public GameManager gameManager;
    public CheatListPanel cheatListScript;
    public bool isNewGame = true;
    public Transform startPosition;
    public SpawnerGemsWell wellGemsSpawner;
    public Transform portal;

    public OrderGame orderGameScript;
    public TileGame tileGameScript;
    public TileGame tileBossGameScript;
    public Shooter shooterGameScript;
    public BattleSort battleSortScript;
    public MazeGame mazeGameScript;
    public AreaVolumeGame areaVolumeGameScript;
    public StarMaker starMakerScript;
    public FiguresGame figuresGameScript;
    public TimeGame timeGameScript;
    public RacingGame racingGameScript;
    public MOG_BossDoor bossDoorScript;
    public TowerScanner towerScannerScript;
    public BossDoorManager bossDoorManager; // Final Boss Door
    public TextMeshProUGUI gemsHudText;
    public Transform gemsHudParticles;
    public GameObject playerPortalStart;

    [Header("CURRENT GAME")]
    public MonoBehaviour currentGameScript;
    public Trophy currentTrophy;
    public int currentLevel;
    public int currentMaxLevel;
    public bool currentHasCompleted;
    public List<int> currentCompletedSets = new List<int>(); //Should be transposed according to difficulty?   
    public bool isCurrentAchievements;
    public Dictionary<int, int> currentAchievements = new Dictionary<int, int>();

    public enum Trophy { OrderGame = 0, ShooterGame = 1, MazeGame = 2, FiguresGame = 3, AreaVolumeGame = 4, ClockGame = 5, TileGame = 6, BattleSort = 7, BossTileGame = 8, StarMakerGame = 9, RacingGame = 10 }
    [Header("PLAYER STATS")]
    public List<Trophy> trophies = new List<Trophy>();
    //public int trophyCounter;
    public enum Difficulty { Normal = 2, Hard = 3, Easy = 1, None = 0}
    public Difficulty difficulty = Difficulty.Normal;
    public int gems;
    public int wellGems;
    public Transform worldGemsParent;
    public List<int> worldGemsTaken = new List<int>();
    public Transform nightGemsParent;
    public List<int> nightGemsTaken = new List<int>();
    public int[] timeOfDay = new int[2] { 12, 00 };
    public int lastLocation;
    public List<int> unlockedLocations = new List<int>();
    public TheWell theWellScript;
    public AudioSource gainGem;
    public AudioSource gainNightGem;
    public AudioSource loseGem;   
    public List<int> playedEvents = new List<int>();
    public List<int> unlockedCharacters = new List<int>();
    public int currentCharacter;
    public CharacterSelector.PlayerCharacter startCharacter;

    private Coroutine addGemsWell;
    private Coroutine portalEffect;
    private int gemsLeft;

    [Header("WORLD PUZZLES PROGRESSION")]
    
    public bool hasSeenTheWellFirstStep;
    public bool hasMetWizard;
    public bool hasCompletedWizard;
    public bool hasMetUnderboss;

    public bool hasSeenDifficultyPanelDemo;
    public bool hasSeenTheWellDemo;

    public bool hasSeenOpening;
    public bool hasSeenInteractionDemo;
    public bool hasSeenTrophyDemo;
    public bool hasUnlockedMap;
    public bool hasUnlockedHat;

    public bool hasOpenedCaveDoor;
    public bool hasOpenedTowerScanner;
    public bool hasOpenedBossDoor;

    [Header("PUZZLES PROGRESSION")]
    //OrderGame
    public bool hasWonOrderGame;
    [SerializeField]
    private int orderGameSetCount;
    public int OrderGameSetCount
    {
        get
        {
            return orderGameSetCount;
        }
        set
        {
            orderGameSetCount = Mathf.Clamp(value, 0, orderGameScript.gameSets.Count - 1);
        }
    }
    public List<int> orderGameCompletedSets = new List<int>();
    //TileGame
    public bool hasWonTileGame;
    [SerializeField]
    private int tileGameSetCount;
    public int TileGameSetCount
    {
        get
        {
            return tileGameSetCount;
        }
        set
        {
            tileGameSetCount = Mathf.Clamp(value, 0, tileGameScript.gameSets.Count - 1);
        }
    }
    public List<int> tileGameCompletedSets = new List<int>();
    public Dictionary<int, int> tileGameSetAchievements = new Dictionary<int, int>();
    //BossTileGame
    public bool hasWonBossTileGame;
    [SerializeField]
    private int bossTileGameSetCount;
    public int BossTileGameSetCount
    {
        get
        {
            return bossTileGameSetCount;
        }
        set
        {
            bossTileGameSetCount = Mathf.Clamp(value, 0, tileGameScript.gameSets.Count - 1);
        }
    }
    public List<int> bossTileGameCompletedSets = new List<int>();
    public Dictionary<int, int> bossTileGameSetAchievements = new Dictionary<int, int>();
    //Shooter
    public bool hasWonShooterGame;
    [SerializeField]
    private int shooterSetCount;
    public int ShooterSetCount
    {
        get
        {
            return shooterSetCount;
        }
        set
        {
            shooterSetCount = Mathf.Clamp(value, 0, shooterGameScript.shooterGameSets.Count - 1);
        }
    }
    public List<int> shooterGameCompletedSets = new List<int>();
    public Dictionary<int, int> shooterGameSetAchievements = new Dictionary<int, int>();
    //MazeGame
    public bool hasWonMazeGame;
    [SerializeField]
    private int mazeGameSetCount;
    public int MazeGameSetCount
    {
        get
        {
            return mazeGameSetCount;
        }
        set
        {
            mazeGameSetCount = Mathf.Clamp(value, 0, mazeGameScript.gameSets.Count - 1);
        }
    }
    public List<int> mazeGameCompletedSets = new List<int>();
    public Dictionary<int, int> mazeGameSetAchievements = new Dictionary<int, int>();
    //AreaVolume Game
    public bool hasWonAreaVolumeGame;
    [SerializeField]
    private int areaVolumeGameSetCount;
    public int AreaVolumeGameSetCount
    {
        get
        {
            return areaVolumeGameSetCount;
        }
        set
        {
            areaVolumeGameSetCount = Mathf.Clamp(value, 0, areaVolumeGameScript.gameSets.Count - 1);
        }
    }
    public List<int> areaVolumeGameCompletedSets = new List<int>();
    public Dictionary<int, int> areaVolumeGameSetAchievements = new Dictionary<int, int>();
    //StarMaker Game
    public bool hasWonStarMakerGame;
    [SerializeField]
    private int starMakerGameSetCount;
    public int StarMakerGameSetCount
    {
        get
        {
            return starMakerGameSetCount;
        }
        set
        {
            starMakerGameSetCount = Mathf.Clamp(value, 0, starMakerScript.gameSets.Count - 1);
        }
    }
    public List<int> starMakerGameCompletedSets = new List<int>();
    //public Dictionary<int, int> starMakerGameSetAchievements = new Dictionary<int, int>();
    //FiguresGame
    public bool hasWonFiguresGame;
    [SerializeField]
    private int figuresGameSetCount;
    public int FiguresGameSetCount
    {
        get
        {
            //return figuresGameSetCount;
            return figuresGameFiguresCompleted.Count;
        }
        set
        {
            figuresGameSetCount = Mathf.Clamp(value, 0, figuresGameScript.figures.Count);

            figuresGameFiguresCompleted.Clear();

            for (int i = 0; i < figuresGameSetCount; i++)
            {
                figuresGameFiguresCompleted.Add(i);
            }
        }
    }
    public List<int> figuresGameFiguresCompleted = new List<int>();
    //ClockGame
    public bool hasWonClockGame;
    public int clockGameCompletedTasks;
    [SerializeField]
    private int clockGameSetCount;
    public int ClockGameSetCount
    {
        get
        {
            return clockGameCompletedTasks;
        }
        set
        {
            clockGameSetCount = Mathf.Clamp(value, 0, timeGameScript.timeTasks.Count - 1);
            clockGameCompletedTasks = clockGameSetCount;
        }
    }
    //RacingGame
    public bool hasWonRacingGame;
    //BattleSort
    public bool hasWonBattleSort; // Checked by BattleSort script
    public bool hasPlayedBattleDemoDesc;
    public bool hasPlayedBattleDemoAsc;
    public float battleSortEnemyHealth = 0.5f;

    //DRILL GAME;
    public int drillGameRight = 1;
    public int drillGameWrong = 0;
    public int orderGameRight = 5;
    
    
    //EVENTS;
    public delegate void CoinEvents();
    public static event CoinEvents addCoin;
    public delegate void TrophyEvents(Trophy trophyToPass);
    public static event TrophyEvents OnGainTrophy;

    public delegate void ActivateGameEvent(); // Sends to GameProgressionDisplay and DifficultyPanel
    public static event ActivateGameEvent OnActivateGame;
    public static event ActivateGameEvent OnDeactivateGame;

    public delegate void CharacterEvent(); //Sends to FriendlyCharacterController
    public static event CharacterEvent OnInitializeCharacter;
    public static event CharacterEvent OnCheckCharacterEvents;

    private void Awake()
    {   
        gameManager.isWebGl = GameProperties.isWebGl;

        Debug.Log("gameManager.isWebGl is set to " + gameManager.isWebGl);
        Debug.Log("static GameProperties.isWebGl is set to " + GameProperties.isWebGl);

        isNewGame = GameProperties.isNewGame;
        Debug.Log("isNewGame = " + isNewGame);

        RestManager2.instance.GetStaticUris();

        if (isNewGame && !debugMode)
        {
            Debug.Log("Loading New game from PlayerStats...");

            if (GameProperties.isBoy)
                startCharacter = CharacterSelector.PlayerCharacter.Boy;
            else
                startCharacter = CharacterSelector.PlayerCharacter.Girl;

            LoadNewGame(startCharacter);
        }
        else
        {
            if (!debugMode)
            {
                Debug.Log("Loading game from PlayerStats...");
                LoadGame();
            }
            else
            {
                Debug.Log("Loading New Debug game from PlayerStats...");
                isNewGame = false;
                LoadNewGame(gameManager.player.characterSelector.playerCharacter);
            }
        }           
    }

    private void Start()
    {
        gameManager.player.anim.gameObject.SetActive(false);
        gameManager.player.spriter.gameObject.SetActive(false);
    }

    private IEnumerator HidePlayer()
    {
        yield return new WaitForSeconds(1);

        gameManager.player.anim.gameObject.SetActive(false);
        gameManager.player.spriter.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        GemCollector.OnCollectGem += GainGem;
        TheWellKiller.OnAddGem += LoseGemSingle;
        DrillGame.OnRightAnswer += DrillGameCorrect;
        DrillGame.OnWrongAnswer += DrillGameWrong;
        OrderGame.OnOrderGameCorrect += OrderGameCorrect;
        OrderGame.OnOrderGameEnable += SetToOrderGame;
        OrderGame.OnOrderGameDisable += SetToNull;
        TileGame.OnTileGameEnable += SetToTileGame;
        TileGame.OnBossTileGameEnable += SetToBossTileGame;
        TileGame.OnTileGameDisable += SetToNull;
        Shooter.OnShooterEnable += SetToShooterGame;
        Shooter.OnShooterDisable += SetToNull;
        MazeGame.OnMazeGameEnable += SetToMazeGame;
        MazeGame.OnMazeGameDisable += SetToNull;
        AreaVolumeGame.OnAreaVolumeGameEnable += SetToAreaVolumeGame;
        AreaVolumeGame.OnAreaVolumeGameDisable += SetToNull;
        StarMaker.OnStarMakerGameEnable += SetToStarMakerGame;
        StarMaker.OnStarMakerGameDisable += SetToNull;
        TimeGame.OnTimeGameEnable += SetToTimeGame;
        TimeGame.OnTimeGameDisable += SetToNull;
        FiguresGame.OnFiguresGameEnable += SetToFiguresGame;
        FiguresGame.OnFiguresGameDisable += SetToNull;
        RacingGame.OnRacingGameEnable += SetToRacingGame;
        RacingGame.OnRacingGameDisable += SetToNull;
        BattleSort.OnBattleSortEnable += SetToBattleSort;
        BattleSort.OnBattleSortDisable += SetToNull;
        MOG_BossDoor.OnBossDoorEnable += SetToBossDoor;
        MOG_BossDoor.OnBossDoorDisable += SetToNull;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndLevel += SetDifficultyAndLevel;
        MapLocationButton.OnTriggerLocation += CheckUnlockedLocations;
        CharacterSelector.OnChangeCharacter += SetCurrentCharacter;
        CheatListPanel.OnChangePuzzleCompletion += CheckCharacterEvents;
        GameManager.OnAfterSwitchOffEvent += CheckHasMetUnderBoss;
        SaveSystemWeb.OnLoadWebData += ReturnWebLoad;
        RestManager2.OnSaveDone += DisableSaveGraphic;
    }

    private void OnDisable()
    {
        GemCollector.OnCollectGem -= GainGem;
        TheWellKiller.OnAddGem -= LoseGemSingle;
        DrillGame.OnRightAnswer -= DrillGameCorrect;
        DrillGame.OnWrongAnswer -= DrillGameWrong;
        OrderGame.OnOrderGameCorrect -= OrderGameCorrect;
        OrderGame.OnOrderGameEnable -= SetToOrderGame;
        OrderGame.OnOrderGameDisable -= SetToNull;
        TileGame.OnTileGameEnable -= SetToTileGame;
        TileGame.OnBossTileGameEnable -= SetToBossTileGame;
        TileGame.OnTileGameDisable -= SetToNull;
        Shooter.OnShooterEnable -= SetToShooterGame;
        Shooter.OnShooterDisable -= SetToNull;
        MazeGame.OnMazeGameEnable -= SetToMazeGame;
        MazeGame.OnMazeGameDisable -= SetToNull;
        AreaVolumeGame.OnAreaVolumeGameEnable -= SetToAreaVolumeGame;
        AreaVolumeGame.OnAreaVolumeGameDisable -= SetToNull;
        StarMaker.OnStarMakerGameEnable -= SetToStarMakerGame;
        StarMaker.OnStarMakerGameDisable -= SetToNull;
        TimeGame.OnTimeGameEnable -= SetToTimeGame;
        TimeGame.OnTimeGameDisable -= SetToNull;
        FiguresGame.OnFiguresGameEnable -= SetToFiguresGame;
        FiguresGame.OnFiguresGameDisable -= SetToNull;
        RacingGame.OnRacingGameEnable -= SetToRacingGame;
        RacingGame.OnRacingGameDisable -= SetToNull;
        BattleSort.OnBattleSortEnable -= SetToBattleSort;
        BattleSort.OnBattleSortDisable -= SetToNull;
        MOG_BossDoor.OnBossDoorEnable -= SetToBossDoor;
        MOG_BossDoor.OnBossDoorDisable -= SetToNull;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndLevel -= SetDifficultyAndLevel;
        MapLocationButton.OnTriggerLocation -= CheckUnlockedLocations;
        CharacterSelector.OnChangeCharacter -= SetCurrentCharacter;
        CheatListPanel.OnChangePuzzleCompletion -= CheckCharacterEvents;
        GameManager.OnAfterSwitchOffEvent -= CheckHasMetUnderBoss;
        SaveSystemWeb.OnLoadWebData -= ReturnWebLoad;
        RestManager2.OnSaveDone -= DisableSaveGraphic;

        CancelInvoke();

        if (addGemsWell != null)
        {
            StopCoroutine(addGemsWell);

            for (int i = 0; i < gemsLeft; i++)
            {
                theWellScript.AddGem();

                if (gems > 0)
                    gems--;
            }

            SetHudText();
            wellGemsSpawner.isReady = true;
            gemsLeft = 0;
        }

        addGemsWell = null;

        if (gemsHudParticles)
            StopGemParticles();
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        CancelInvoke();

        if (addGemsWell != null)
        {
            StopCoroutine(addGemsWell);

            for (int i = 0; i < gemsLeft; i++)
            {
                theWellScript.AddGem();

                if (gems > 0)
                    gems--;
            }

            SetHudText();
            wellGemsSpawner.isReady = true;
            gemsLeft = 0;
        }
        
        addGemsWell = null;

        if (gemsHudParticles)
            StopGemParticles();
        StopAllCoroutines();
    }
    private void FixedUpdate()
    {
        CheckCurrentValues();
    }

    void DisableSaveGraphic()
    {
        gameManager.saveGraphic.gameObject.SetActive(false);
    }

    void CheckCharacterEvents()
    {
        if (OnCheckCharacterEvents != null)
            OnCheckCharacterEvents();
    }
    
    void SetDifficultyAndLevel(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        difficulty = requestedDifficulty;

        if (requestedLevel < currentMaxLevel)
            currentLevel = requestedLevel;
        else if (requestedLevel >= currentMaxLevel && requestedLevel < (currentMaxLevel * 2))
            currentLevel = requestedLevel - currentMaxLevel;
        else if (requestedLevel >= currentMaxLevel * 2)
            currentLevel = requestedLevel - (currentMaxLevel * 2);
    }

    void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        difficulty = requestedDifficulty;
    }

    void SetCurrentCharacter(CharacterSelector.PlayerCharacter passedCharacter)
    {
        //currentCharacter = Array.IndexOf(Enum.GetValues(passedCharacter.GetType()), passedCharacter);
        int index = 0;
        //index = passedCharacter.GetHashCode();

        for (int i = 0; i < gameManager.characterSelector.characters.Count; i++)
        {
            if (gameManager.characterSelector.characters[i].characterType == passedCharacter)
                index = i;
        }

        if (!hasSeenOpening)
            gameManager.player.anim.transform.gameObject.SetActive(false);

        currentCharacter = index;
    }

    void SetWorldGems()
    {
        for (int i = 0; i < worldGemsParent.childCount; i++)
        {
            if (worldGemsTaken.Count > i)
            {
                if (worldGemsTaken[i] == 0)
                    worldGemsParent.GetChild(i).gameObject.SetActive(false);
                else
                    worldGemsParent.GetChild(i).gameObject.SetActive(true);
            }
        }

        for (int i = 0; i < nightGemsParent.childCount; i++)
        {
            if (nightGemsTaken.Count > i)
            {
                if (nightGemsTaken[i] == 0)
                    nightGemsParent.GetChild(i).gameObject.SetActive(false);
                else
                    nightGemsParent.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

    void SetWellProgression()
    {
        theWellScript.TransferSavedProgress();
    }

    void SetTime()
    {
        if(timeGameScript)
        {
            timeGameScript.SetTimeAndUpdateAll(timeOfDay);
        }
    }

    void InitializeWorldGems()
    {

        for (int i = 0; i < worldGemsParent.childCount; i++)
        {
            worldGemsTaken.Add(1);
        }

        for (int i = 0; i < nightGemsParent.childCount; i++)
        {
            nightGemsTaken.Add(1);
        }
        
    }

    void CheckCurrentValues()
    {
        if(currentGameScript == orderGameScript)
        {
            currentTrophy = Trophy.OrderGame;
            currentLevel = orderGameSetCount;
            currentMaxLevel = orderGameScript.gameSets.Count -1;
            currentHasCompleted = hasWonOrderGame;
            currentCompletedSets = orderGameCompletedSets;
            isCurrentAchievements = false;
        }
        else if (currentGameScript == shooterGameScript)
        {
            currentTrophy = Trophy.ShooterGame;
            currentLevel = shooterSetCount;
            currentMaxLevel = shooterGameScript.shooterGameSets.Count - 1;
            currentHasCompleted = hasWonShooterGame;
            currentCompletedSets = shooterGameCompletedSets;
            isCurrentAchievements = true;
            currentAchievements = shooterGameSetAchievements;
        }
        else if (currentGameScript == tileGameScript)
        {
            currentTrophy = Trophy.TileGame;
            currentLevel = tileGameSetCount;
            currentMaxLevel = tileGameScript.gameSets.Count - 1;
            currentHasCompleted = hasWonTileGame;
            currentCompletedSets = tileGameCompletedSets;
            isCurrentAchievements = true;
            currentAchievements = tileGameSetAchievements;
        }
        else if (currentGameScript == tileBossGameScript)
        {
            currentTrophy = Trophy.BossTileGame;
            currentLevel = bossTileGameSetCount;
            currentMaxLevel = tileBossGameScript.gameSets.Count - 1;
            currentHasCompleted = hasWonBossTileGame;
            currentCompletedSets = bossTileGameCompletedSets;
            isCurrentAchievements = false;
            currentAchievements = bossTileGameSetAchievements;
        }
        else if (currentGameScript == mazeGameScript)
        {
            currentTrophy = Trophy.MazeGame;
            currentLevel = mazeGameSetCount;
            currentMaxLevel = mazeGameScript.gameSets.Count - 1;
            currentHasCompleted = hasWonMazeGame;
            currentCompletedSets = mazeGameCompletedSets;
            isCurrentAchievements = true;
            currentAchievements = mazeGameSetAchievements;
        }
        else if (currentGameScript == areaVolumeGameScript)
        {
            currentTrophy = Trophy.AreaVolumeGame;
            currentLevel = areaVolumeGameSetCount;
            currentMaxLevel = areaVolumeGameScript.gameSets.Count - 1;
            currentHasCompleted = hasWonAreaVolumeGame;
            currentCompletedSets = areaVolumeGameCompletedSets;
            isCurrentAchievements = true;
            currentAchievements = areaVolumeGameSetAchievements;
        }
        else if (currentGameScript == starMakerScript)
        {
            currentTrophy = Trophy.StarMakerGame;
            currentLevel = starMakerGameSetCount;
            currentMaxLevel = starMakerScript.gameSets.Count - 1;
            currentHasCompleted = hasWonStarMakerGame;
            currentCompletedSets = starMakerGameCompletedSets;
            isCurrentAchievements = false;
        }
        else if (currentGameScript == figuresGameScript)
        {
            currentTrophy = Trophy.FiguresGame;
            currentLevel = figuresGameSetCount;
            currentMaxLevel = figuresGameScript.figures.Count - 1;
            currentHasCompleted = hasWonFiguresGame;
            currentCompletedSets = figuresGameFiguresCompleted;
            isCurrentAchievements = false;
        }
        else if (currentGameScript == timeGameScript)
        {
            currentTrophy = Trophy.ClockGame;
            currentLevel = clockGameSetCount; //NEW!
            currentMaxLevel = timeGameScript.timeTasks.Count - 1; //NEW!
            currentHasCompleted = hasWonClockGame;
            //currentCompletedSets = clockGameCompletedTasks;
            isCurrentAchievements = false;
        }
        else if (currentGameScript == racingGameScript)
        {
            currentTrophy = Trophy.RacingGame;
            //currentLevel = starMakerGameSetCount;
            //currentMaxLevel = starMakerScript.gameSets.Count - 1;
            currentHasCompleted = hasWonClockGame;
            isCurrentAchievements = false;
        }
        else if (currentGameScript == battleSortScript)
        {
            currentTrophy = Trophy.BattleSort;
            currentLevel = 0;
            currentMaxLevel = 0;
            currentHasCompleted = hasWonBattleSort;
            isCurrentAchievements = false;
        }
        else
        {
            currentGameScript = null;
            currentTrophy = Trophy.OrderGame;
            currentLevel = 0;
            currentMaxLevel = 0;
            currentHasCompleted = false;
            currentCompletedSets = null;
            isCurrentAchievements = true;
            currentAchievements = null;
        }
    }

    private void GainTrophy(Trophy trophyToPass)
    {
        if(OnGainTrophy != null)
        {
            OnGainTrophy(trophyToPass);
        }
    }

    void DrillGameCorrect()
    {
        AddGem(drillGameRight);
    }

    void DrillGameWrong()
    {

    }

    void OrderGameCorrect()
    {
        AddGem(orderGameRight);
    }

    public void racingGameCorrect(int passedReward)
    {
        AddGem(passedReward);
    }

    private void GainGem(int gainedGems, Transform gem)
    {
        gems += gainedGems;
        //collectedGems += gainedGems;
        
        SetHudText();

        if (gem.parent == worldGemsParent)
            worldGemsTaken[gem.GetSiblingIndex()] = 0;

        if (gem.parent == nightGemsParent)
        {
            nightGemsTaken[gem.GetSiblingIndex()] = 0;
            gainNightGem.Play();
        }
        else
        {
            gainGem.Play();
        }

        SaveGame();
    }

    public void AddGem(int passedGems)
    {
        gems += passedGems;
        //collectedGems += passedGems;
        gainGem.Play();
        SetHudText();      
    }
    private void LoseGem(int passedGems)
    {
        gems -= passedGems;
        loseGem.Play();
        SetHudText();
    }
    private void LoseGemSingle()
    {
        gems -= 1;
        //loseGem.Play();
        SetHudText();
    }

    void SetHudText()
    {
        /*
        if(!theWellScript.gemsRequired1Reached)
            gemsHudText.text = collectedGems.ToString() + " / " + theWellScript.gemsRequired1.ToString();
        else
            gemsHudText.text = collectedGems.ToString() + " / " + (theWellScript.gemsRequired1 + theWellScript.gemsRequired2).ToString();
        */
        if (gemsHudParticles)
        {
            CancelInvoke("StopGemParticles");
            gemsHudParticles.gameObject.SetActive(true);
            Invoke("StopGemParticles", 0.4f);
        }

        gemsHudText.text = gems.ToString();
    }

    void StopGemParticles()
    {
        gemsHudParticles.gameObject.SetActive(false);
    }

    void SetToOrderGame()
    {
        currentGameScript = orderGameScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToTileGame()
    {
        currentGameScript = tileGameScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToBossTileGame()
    {
        currentGameScript = tileBossGameScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToShooterGame()
    {
        currentGameScript = shooterGameScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToMazeGame()
    {
        currentGameScript = mazeGameScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToAreaVolumeGame()
    {
        currentGameScript = areaVolumeGameScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToStarMakerGame()
    {
        currentGameScript = starMakerScript;
        CheckCurrentValues();

        if (OnActivateGame != null)
            OnActivateGame();
    }

    void SetToTimeGame()
    {
        currentGameScript = timeGameScript;
        CheckCurrentValues();
    }

    void SetToFiguresGame()
    {
        currentGameScript = figuresGameScript;
        CheckCurrentValues();
    }

    void SetToRacingGame()
    {
        currentGameScript = racingGameScript;
        CheckCurrentValues();
    }

    void SetToBattleSort()
    {
        currentGameScript = battleSortScript;
        CheckCurrentValues();
        /*
        if (OnActivateGame != null)
            OnActivateGame();
        */
    }

    void SetToBossDoor()
    {
        currentGameScript = bossDoorScript;
    }

    void SetToNull()
    {
        currentGameScript = null;
        currentTrophy = Trophy.OrderGame;
        currentLevel = 0;
        currentMaxLevel = 0;
        currentHasCompleted = false;
        currentCompletedSets = null;
        isCurrentAchievements = true;
        currentAchievements = null;

        if (OnDeactivateGame != null)
            OnDeactivateGame();
    }

    void CheckUnlockedLocations(int passedIndex)
    {
        if (!unlockedLocations.Contains(passedIndex))
            unlockedLocations.Add(passedIndex);
    }

    void CheckDoors()
    {
        bossDoorScript.CheckEvents();
        towerScannerScript.CheckIfUnlocked();
        bossDoorManager.InitializeBossDoor();
    }

    void InitializeCharacters()
    {
        if (OnInitializeCharacter != null)
            OnInitializeCharacter();

        for (int i = 0; i < gameManager.characterManager.characters.Count; i++)
        {
            gameManager.characterManager.characters[i].InitializeCharacter();
        }
    }

    void UnlockCharacters()
    {
        CheckCharacterEvents();
    }

    void CheckMapButton()
    {
        if (hasUnlockedMap)
        {
            gameManager.hudCanvas.worldMapButton.gameObject.SetActive(true);
            gameManager.hudCanvas.worldMapBtnEmpty.gameObject.SetActive(false);
            gameManager.hudCanvas.mapBtnPulser.enabled = false;
        }
        else
        {
            gameManager.hudCanvas.worldMapButton.gameObject.SetActive(false);
            gameManager.hudCanvas.worldMapBtnEmpty.gameObject.SetActive(true);
        }
    }

    void SetLocations()
    {
        gameManager.worldMapController.InitializeLocations();
    }

    void CheckHatButton()
    {
        if (hasUnlockedHat)
        {
            gameManager.hudCanvas.toggleCharacterButton.gameObject.SetActive(true);
            gameManager.hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(false);
        }
        else
        {
            gameManager.hudCanvas.toggleCharacterButton.gameObject.SetActive(false);
            gameManager.hudCanvas.toggleCharacterBtnEmpty.gameObject.SetActive(true);
        }
    }

    void CheckTrophyButton()
    {
        if(hasSeenTrophyDemo)
        {
            gameManager.hudCanvas.trophyButton.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
        }
        else
        {
            gameManager.hudCanvas.trophyButton.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyBtnEmpty.gameObject.SetActive(true);
            /*
            if(hasSeenInteractionDemo)
            {
                foreach (var item in gameManager.trophyDemoObjects)
                {
                    item.gameObject.SetActive(true);
                }
            }
            */
        }
    }

    void SetCharacter()
    {
        if (!debugMode)
        {
            if (isNewGame)
                gameManager.characterSelector.playerCharacter = startCharacter;
            else
                gameManager.characterSelector.playerCharacter = gameManager.characterSelector.characters[currentCharacter].characterType;

            var index = gameManager.characterSelector.GetCharacterIndex(startCharacter);

            if (!unlockedCharacters.Contains(index))
                unlockedCharacters.Add(index);
        }
        else
        {
            gameManager.characterSelector.playerCharacter = startCharacter;

            var index = gameManager.characterSelector.GetCharacterIndex(startCharacter);

            if (!unlockedCharacters.Contains(index))
                unlockedCharacters.Add(index);
        }
    }

    void PlacePlayer()
    {
        if(isNewGame)
        {
            gameManager.player.transform.position = startPosition.position;
            gameManager.player.transform.rotation = startPosition.rotation;
        }
        else
        {
            gameManager.player.transform.position = gameManager.worldMapController.worldMapCanvas.locations[lastLocation].position;
            gameManager.player.transform.rotation = gameManager.worldMapController.worldMapCanvas.locations[lastLocation].rotation;
        }         

        if (playerPortalStart && gameManager.player)
        {
            if (hasSeenOpening && hasSeenInteractionDemo)
            {
                PortalEffect();
            }
            else
            {
                
            }
        }
    }

    public void PortalEffect()
    {
        if (portalEffect != null)
            StopCoroutine(portalEffect);

        portalEffect = StartCoroutine(Portal());
    }

    private IEnumerator Portal()
    {
        GameObject newPortalEffect = Instantiate(playerPortalStart, gameManager.player.transform.position + Vector3.up, Quaternion.identity, gameManager.worldEffectsContainer);

        yield return new WaitForSeconds(0.5f);

        gameManager.player.anim.gameObject.SetActive(true);

        //if(lastLocation != gameManager.worldMapController.worldMapCanvas.locations.IndexOf(gameManager.worldMapController.worldMapCanvas.bossLocation))
        if(gameManager.player.characterSelector.playerCharacter != CharacterSelector.PlayerCharacter.Wizard)
            gameManager.player.spriter.gameObject.SetActive(true);
    }

    public void SaveGame()
    {
        Debug.Log("Saving progress...");

        gameManager.saveGraphic.gameObject.SetActive(true);

        if (!gameManager.isWebGl)
            SaveSystem.SaveGame(this);
        else
            SaveSystemWeb.SaveWebGl(this);
    }

    public void SaveAndExitGame()
    {
        SaveGame();
        Application.Quit();
    }

    public void SaveAndExitToMain()
    {
        SaveGame();
        SceneManager.LoadSceneAsync(0);
    }

    public void LoadGame()
    {
        Debug.Log("Loading progress...");

        gameManager.saveGraphic.gameObject.SetActive(true);

        if (!gameManager.isWebGl)
        {
            PlayerData data = SaveSystem.LoadGame();
            Load(data);
        }
        else
        {
            SaveSystemWeb.LoadWebGl();
        }       
    }

    private void ReturnWebLoad(PlayerDataWeb playerDataWeb)
    {
        Debug.Log("implementing player web data...");
        LoadWeb(playerDataWeb);
    }
    
    private void Load(PlayerData data)
    {
        trophies.Clear(); //testing:

        for (int i = 0; i < data.trophies.Length; i++)
        {
            trophies.Add((Trophy)data.trophies[i]);
        }

        difficulty = (Difficulty)data.difficulty;
        battleSortEnemyHealth = data.battleSortEnemyHealth;

        gems = data.gems;
        wellGems = data.wellGems;
        worldGemsTaken.Clear();
        worldGemsTaken = new List<int>(data.worldGemsTaken);
        Debug.Log("loaded worldGemsTaken list count is " + worldGemsTaken.Count);
        nightGemsTaken.Clear();
        nightGemsTaken = new List<int>(data.nightGemsTaken);

        timeOfDay = data.timeOfDay;

        lastLocation = data.lastLocation;
        unlockedLocations.Clear();
        unlockedLocations = new List<int>(data.unlockedLocations);

        playedEvents.Clear();
        playedEvents = new List<int>(data.playedEvents);

        if (data.startCharacter == 0)
            startCharacter = CharacterSelector.PlayerCharacter.Boy;
        else
            startCharacter = CharacterSelector.PlayerCharacter.Girl;

        currentCharacter = data.currentCharacter;
        gameManager.characterSelector.playerCharacter = (CharacterSelector.PlayerCharacter)currentCharacter;
        unlockedCharacters.Clear();
        unlockedCharacters = new List<int>(data.unlockedCharacters);

        hasSeenTheWellFirstStep = data.hasSeenTheWellFirstStep;
        hasMetWizard = data.hasMetWizard;
        hasCompletedWizard = data.hasCompletedWizard;
        hasMetUnderboss = data.hasMetUnderboss;
        hasSeenDifficultyPanelDemo = data.hasSeenDifficultyPanelDemo;
        hasSeenTheWellDemo = data.hasSeenTheWellDemo;
        hasSeenOpening = data.hasSeenOpening;
        hasSeenInteractionDemo = data.hasSeenInteractionDemo;
        hasSeenTrophyDemo = data.hasSeenTrophyDemo;
        hasUnlockedMap = data.hasUnlockedMap;
        hasUnlockedHat = data.hasUnlockedHat;
        hasOpenedCaveDoor = data.hasOpenedCaveDoor;
        hasOpenedTowerScanner = data.hasOpenedTowerScanner;
        hasOpenedBossDoor = data.hasOpenedBossDoor;

        //Minigames:
        orderGameSetCount = data.OrderGameSetCount;
        hasWonOrderGame = data.hasWonOrderGame;
        orderGameCompletedSets.Clear();
        orderGameCompletedSets = new List<int>(data.orderGameCompletedSets);
        TileGameSetCount = data.TileGameSetCount;
        hasWonTileGame = data.hasWonTileGame;
        tileGameCompletedSets.Clear();
        tileGameCompletedSets = new List<int>(data.tileGameCompletedSets);
        tileGameSetAchievements.Clear();
        tileGameSetAchievements = new Dictionary<int, int>();
        int counter1 = 0;
        foreach (var item in data.tileGameSetAchievementsKeys)
        {
            tileGameSetAchievements.Add(data.tileGameSetAchievementsKeys[counter1], data.tileGameSetAchievementsValues[counter1]);
            counter1++;
        }
        BossTileGameSetCount = data.BossTileGameSetCount;
        hasWonBossTileGame = data.hasWonBossTileGame;
        bossTileGameCompletedSets.Clear();
        bossTileGameCompletedSets = new List<int>(data.bossTileGameCompletedSets);
        bossTileGameSetAchievements.Clear();
        bossTileGameSetAchievements = new Dictionary<int, int>();
        int counter2 = 0;
        foreach (var item in data.bossTileGameSetAchievementsKeys)
        {
            bossTileGameSetAchievements.Add(data.bossTileGameSetAchievementsKeys[counter2], data.bossTileGameSetAchievementsValues[counter2]);
            counter2++;
        }
        ShooterSetCount = data.ShooterSetCount;
        hasWonShooterGame = data.hasWonShooterGame;
        shooterGameCompletedSets.Clear();
        shooterGameCompletedSets = new List<int>(data.shooterGameCompletedSets);
        shooterGameSetAchievements.Clear();
        shooterGameSetAchievements = new Dictionary<int, int>();
        int counter3 = 0;
        foreach (var item in data.shooterGameSetAchievementsKeys)
        {
            shooterGameSetAchievements.Add(data.shooterGameSetAchievementsKeys[counter3], data.shooterGameSetAchievementsValues[counter3]);
            counter3++;
        }
        MazeGameSetCount = data.MazeGameSetCount;
        hasWonMazeGame = data.hasWonMazeGame;
        mazeGameCompletedSets.Clear();
        mazeGameCompletedSets = new List<int>(data.mazeGameCompletedSets);
        int counter4 = 0;
        foreach (var item in data.mazeGameSetAchievementsKeys)
        {
            mazeGameSetAchievements.Add(data.mazeGameSetAchievementsKeys[counter4], data.mazeGameSetAchievementsValues[counter4]);
            counter4++;
        }
        AreaVolumeGameSetCount = data.AreaVolumeGameSetCount;
        hasWonAreaVolumeGame = data.hasWonAreaVolumeGame;
        areaVolumeGameCompletedSets.Clear();
        areaVolumeGameCompletedSets = new List<int>(data.areaVolumeGameCompletedSets);
        areaVolumeGameSetAchievements.Clear();
        areaVolumeGameSetAchievements = new Dictionary<int, int>();
        int counter5 = 0;
        foreach (var item in data.areaVolumeGameSetAchievementsKeys)
        {
            areaVolumeGameSetAchievements.Add(data.areaVolumeGameSetAchievementsKeys[counter5], data.areaVolumeGameSetAchievementsValues[counter5]);
            counter5++;
        }
        StarMakerGameSetCount = data.StarMakerGameSetCount;
        hasWonStarMakerGame = data.hasWonStarMakerGame;
        starMakerGameCompletedSets.Clear();
        starMakerGameCompletedSets = new List<int>(data.starMakerGameCompletedSets);
        hasWonFiguresGame = data.hasWonFiguresGame;
        FiguresGameSetCount = data.FiguresGameSetCount;
        figuresGameFiguresCompleted.Clear();
        figuresGameFiguresCompleted = new List<int>(data.figuresGameFiguresCompleted);
        hasWonClockGame = data.hasWonClockGame;
        clockGameCompletedTasks = data.clockGameCompletedTasks;
        ClockGameSetCount = data.ClockGameSetCount;
        hasWonRacingGame = data.hasWonRacingGame;
        hasWonBattleSort = data.hasWonBattleSort;
        hasPlayedBattleDemoDesc = data.hasPlayedBattleDemoDesc;
        hasPlayedBattleDemoAsc = data.hasPlayedBattleDemoAsc;

        SetCheats(data);

        Debug.Log("GameLoaded");

        Initialize();
    }

    private void LoadWeb(PlayerDataWeb data)
    {
        trophies.Clear(); //testing:

        for (int i = 0; i < data.trophies.Length; i++)
        {
            trophies.Add((Trophy)data.trophies[i]);
        }

        difficulty = (Difficulty)data.difficulty;
        battleSortEnemyHealth = data.battleSortEnemyHealth;

        gems = data.gems;
        wellGems = data.wellGems;
        worldGemsTaken.Clear();
        worldGemsTaken = new List<int>(data.worldGemsTaken);        
        nightGemsTaken.Clear();
        nightGemsTaken = new List<int>(data.nightGemsTaken);

        timeOfDay = data.timeOfDay;

        lastLocation = data.lastLocation;
        unlockedLocations.Clear();
        unlockedLocations = new List<int>(data.unlockedLocations);

        playedEvents.Clear();
        playedEvents = new List<int>(data.playedEvents);

        if (data.startCharacter == 0)
            startCharacter = CharacterSelector.PlayerCharacter.Boy;
        else
            startCharacter = CharacterSelector.PlayerCharacter.Girl;

        currentCharacter = data.currentCharacter;
        gameManager.characterSelector.playerCharacter = (CharacterSelector.PlayerCharacter)currentCharacter;
        unlockedCharacters.Clear();
        unlockedCharacters = new List<int>(data.unlockedCharacters);

        hasSeenTheWellFirstStep = data.hasSeenTheWellFirstStep;
        hasMetWizard = data.hasMetWizard;
        hasCompletedWizard = data.hasCompletedWizard;
        hasMetUnderboss = data.hasMetUnderboss;
        hasSeenDifficultyPanelDemo = data.hasSeenDifficultyPanelDemo;
        hasSeenTheWellDemo = data.hasSeenTheWellDemo;
        hasSeenOpening = data.hasSeenOpening;
        hasSeenInteractionDemo = data.hasSeenInteractionDemo;
        hasSeenTrophyDemo = data.hasSeenTrophyDemo;
        hasUnlockedMap = data.hasUnlockedMap;
        hasUnlockedHat = data.hasUnlockedHat;
        hasOpenedCaveDoor = data.hasOpenedCaveDoor;
        hasOpenedTowerScanner = data.hasOpenedTowerScanner;
        hasOpenedBossDoor = data.hasOpenedBossDoor;

        //Minigames:
        orderGameSetCount = data.OrderGameSetCount;
        hasWonOrderGame = data.hasWonOrderGame;
        orderGameCompletedSets.Clear();
        orderGameCompletedSets = new List<int>(data.orderGameCompletedSets);
        TileGameSetCount = data.TileGameSetCount;
        hasWonTileGame = data.hasWonTileGame;
        tileGameCompletedSets.Clear();
        tileGameCompletedSets = new List<int>(data.tileGameCompletedSets);
        tileGameSetAchievements.Clear();
        tileGameSetAchievements = new Dictionary<int, int>();
        int counter1 = 0;
        foreach (var item in data.tileGameSetAchievementsKeys)
        {
            tileGameSetAchievements.Add(data.tileGameSetAchievementsKeys[counter1], data.tileGameSetAchievementsValues[counter1]);
            counter1++;
        }
        BossTileGameSetCount = data.BossTileGameSetCount;
        hasWonBossTileGame = data.hasWonBossTileGame;
        bossTileGameCompletedSets.Clear();
        bossTileGameCompletedSets = new List<int>(data.bossTileGameCompletedSets);
        bossTileGameSetAchievements.Clear();
        bossTileGameSetAchievements = new Dictionary<int, int>();
        int counter2 = 0;
        foreach (var item in data.bossTileGameSetAchievementsKeys)
        {
            bossTileGameSetAchievements.Add(data.bossTileGameSetAchievementsKeys[counter2], data.bossTileGameSetAchievementsValues[counter2]);
            counter2++;
        }
        ShooterSetCount = data.ShooterSetCount;
        hasWonShooterGame = data.hasWonShooterGame;
        shooterGameCompletedSets.Clear();
        shooterGameCompletedSets = new List<int>(data.shooterGameCompletedSets);
        shooterGameSetAchievements.Clear();
        shooterGameSetAchievements = new Dictionary<int, int>();
        int counter3 = 0;
        foreach (var item in data.shooterGameSetAchievementsKeys)
        {
            shooterGameSetAchievements.Add(data.shooterGameSetAchievementsKeys[counter3], data.shooterGameSetAchievementsValues[counter3]);
            counter3++;
        }
        MazeGameSetCount = data.MazeGameSetCount;
        hasWonMazeGame = data.hasWonMazeGame;
        mazeGameCompletedSets.Clear();
        mazeGameCompletedSets = new List<int>(data.mazeGameCompletedSets);
        int counter4 = 0;
        foreach (var item in data.mazeGameSetAchievementsKeys)
        {
            mazeGameSetAchievements.Add(data.mazeGameSetAchievementsKeys[counter4], data.mazeGameSetAchievementsValues[counter4]);
            counter4++;
        }
        AreaVolumeGameSetCount = data.AreaVolumeGameSetCount;
        hasWonAreaVolumeGame = data.hasWonAreaVolumeGame;
        areaVolumeGameCompletedSets.Clear();
        areaVolumeGameCompletedSets = new List<int>(data.areaVolumeGameCompletedSets);
        areaVolumeGameSetAchievements.Clear();
        areaVolumeGameSetAchievements = new Dictionary<int, int>();
        int counter5 = 0;
        foreach (var item in data.areaVolumeGameSetAchievementsKeys)
        {
            areaVolumeGameSetAchievements.Add(data.areaVolumeGameSetAchievementsKeys[counter5], data.areaVolumeGameSetAchievementsValues[counter5]);
            counter5++;
        }
        StarMakerGameSetCount = data.StarMakerGameSetCount;
        hasWonStarMakerGame = data.hasWonStarMakerGame;
        starMakerGameCompletedSets.Clear();
        starMakerGameCompletedSets = new List<int>(data.starMakerGameCompletedSets);
        hasWonFiguresGame = data.hasWonFiguresGame;
        FiguresGameSetCount = data.FiguresGameSetCount;
        figuresGameFiguresCompleted.Clear();
        figuresGameFiguresCompleted = new List<int>(data.figuresGameFiguresCompleted);
        hasWonClockGame = data.hasWonClockGame;
        clockGameCompletedTasks = data.clockGameCompletedTasks;
        ClockGameSetCount = data.ClockGameSetCount;
        hasWonRacingGame = data.hasWonRacingGame;
        hasWonBattleSort = data.hasWonBattleSort;
        hasPlayedBattleDemoDesc = data.hasPlayedBattleDemoDesc;
        hasPlayedBattleDemoAsc = data.hasPlayedBattleDemoAsc;

        SetCheatsWeb(data);

        Debug.Log("GameLoaded");

        Initialize();
    }

    public void StartAddWellGems(int passedGems, int initalWait)
    {
        
        if(addGemsWell != null)
        {
            StopCoroutine(addGemsWell);

            for (int i = 0; i < gemsLeft; i++)
            {
                theWellScript.AddGem();
                wellGems++;

                if (gems > 0)
                    gems--;                
            }

            SetHudText();
            wellGemsSpawner.isReady = true;
            gemsLeft = 0;
        }

        addGemsWell = null;
        
        gemsLeft = passedGems;
        addGemsWell = StartCoroutine(AddWellGems(passedGems, initalWait));
    }

    private IEnumerator AddWellGems(int passedGems, int initialWait)
    {
        yield return new WaitForSeconds(initialWait);

        for (int i = 0; i < passedGems; i++)
        {
            theWellScript.AddGem();
            wellGems++;

            if(gemsLeft > 0)
                gemsLeft--;
            if (gems > 0)
                gems--;

            SetHudText();

            yield return new WaitForEndOfFrame();
        }

        wellGemsSpawner.isReady = true;
    }

    private void LoadNewGame(CharacterSelector.PlayerCharacter startCharacter)
    {
        PlayerStats newPlayerStats = new PlayerStats();
        newPlayerStats.startCharacter = startCharacter;
        PlayerData newData = new PlayerData(newPlayerStats);

        Load(newData);
    }

    private void Initialize()
    {
        if(isNewGame)
            InitializeWorldGems();

        SetHudText();
        GetEvents();       
        ValidateWorldGemsCount();
        SetWorldGems();
        SetWellProgression();
        SetTime();
        CheckDoors();
        InitializeCharacters();
        UnlockCharacters();
        CheckMapButton();
        SetLocations();
        CheckHatButton();
        CheckTrophyButton();
        SetActiveEventsAndTriggers();
        SetCharacter();
        PlacePlayer();

        gameManager.saveGraphic.gameObject.SetActive(false);
        Debug.Log("PlayerStats initialization done...");
    }

    void GetEvents()
    {
        gameManager.gameEventsManager.GetAllEvents();
    }

    void SetActiveEventsAndTriggers()
    {
        if(hasSeenOpening) //Tested
        {
            foreach(var item in gameManager.openingSequence)
            {
                item.gameObject.SetActive(false);
            }

            if(!hasSeenInteractionDemo)
                gameManager.hudDemo.gameObject.SetActive(true);
        }
        else
        {
            foreach (var item in gameManager.openingSequence)
            {
                item.gameObject.SetActive(true);
            }

            gameManager.soundTrackOpening.Play();
        }

        if(hasSeenInteractionDemo && !hasSeenTrophyDemo) // Tested
        {
            foreach(var item in gameManager.trophyEvent)
            {
                item.gameObject.SetActive(true);
            }

            gameManager.hudDemo.gameObject.SetActive(false);
        }

        if(hasSeenTrophyDemo)
        {
            foreach (var item in gameManager.trophyEvent)
            {
                item.gameObject.SetActive(false);
            }
        }

        if(hasSeenTrophyDemo && !hasUnlockedMap)
        {
            gameManager.mapDemoScript.mapObjectScript.playerTrigger.gameObject.SetActive(true);
        }

        if(hasUnlockedMap)
        {
            gameManager.mapDemoScript.mapLockScript.gameObject.SetActive(false);
            gameManager.mapDemoScript.mapObjectScript.map3dMesh.transform.parent.gameObject.SetActive(false);
        }

        if(hasSeenDifficultyPanelDemo)
        {
            gameManager.hudCanvas.difficultyPanel.hasOpened = true;
        }

        if(!hasSeenTheWellDemo) //Tested
        {
            foreach (var item in gameManager.wellEvent)
            {
                item.gameObject.SetActive(true);
            }
        }

        if(hasOpenedCaveDoor) //Tested
        {
            bossDoorScript.gameComplete = true;
            bossDoorScript.bossDoorOpenScript.isOpen = true;
            bossDoorScript.bossDoorOpenScript.transform.position = bossDoorScript.bossDoorOpenScript.endPos;
        }

        if(!hasMetUnderboss && !hasUnlockedHat) //Tested
        {
            foreach (var item in gameManager.meetUnderBossEvent)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in gameManager.transformUnderbossEvent)
            {
                item.gameObject.SetActive(false);
            }

            gameManager.capturedCharacterController.isComplete = false;
        }

        else if(hasMetUnderboss && !hasUnlockedHat) //Tested
        {           
            foreach (var item in gameManager.meetUnderBossEvent)
            {
                item.gameObject.SetActive(false);
            }

            foreach (var item in gameManager.transformUnderbossEvent)
            {
                if(hasWonRacingGame)
                    item.gameObject.SetActive(true);
            }

            gameManager.capturedCharacterController.isComplete = false;
            gameManager.underBossController.animatorTrue.SetBool("hasLostUnderboss", true);
            gameManager.underBossController.animatorTransformed.SetBool("hasLostUnderboss", true);
            gameManager.underBossController.currentAnimator.SetBool("hasLostUnderboss", true);
        }

        if (hasUnlockedHat)
        {
            foreach (var item in gameManager.meetUnderBossEvent)
            {
                item.gameObject.SetActive(false);
            }

            foreach (var item in gameManager.transformUnderbossEvent)
            {
                item.gameObject.SetActive(false);
            }

            foreach (var item in gameManager.hatTutorialEvent)
            {
                item.gameObject.SetActive(false);
            }

            gameManager.cageScript.SetToComplete();

            if(!playedEvents.Contains(gameManager.gameEventsManager.gameEvents.IndexOf(gameManager.underBossController.victoryEvent)))
                playedEvents.Add(gameManager.gameEventsManager.gameEvents.IndexOf(gameManager.underBossController.victoryEvent));

            if(!playedEvents.Contains(gameManager.gameEventsManager.gameEvents.IndexOf(gameManager.capturedCharacterController.victoryEvent)))
                playedEvents.Add(gameManager.gameEventsManager.gameEvents.IndexOf(gameManager.capturedCharacterController.victoryEvent));

            gameManager.underBossController.isComplete = true;            
            //gameManager.underBossController.characterStage = FriendlyCharacterController.CharacterStages.IsSolved;
            gameManager.underBossController.animatorTrue.SetBool("hasLostUnderboss", true);
            gameManager.underBossController.animatorTransformed.SetBool("hasLostUnderboss", true);
            gameManager.underBossController.currentAnimator.SetBool("hasLostUnderboss", true);
            gameManager.capturedCharacterController.isComplete = true;
            gameManager.capturedCharacterController.InitializeCharacter();
            //gameManager.capturedCharacterController.characterStage = FriendlyCharacterController.CharacterStages.IsSolved;
        }

        /*
        if (hasOpenedTowerScanner)
            towerScannerScript.sceneCollider.gameObject.SetActive(false);
        else
            towerScannerScript.sceneCollider.gameObject.SetActive(true);
        */

        CheckWizardEvents();

        if (trophies.Contains(Trophy.BossTileGame))
            portal.gameObject.SetActive(true);
    }

    public void CheckWizardEvents()
    {
        if (!hasMetWizard && !hasCompletedWizard)
        {
            foreach (var item in gameManager.meetWizardEvent)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in gameManager.transformWizardEvent)
            {
                item.gameObject.SetActive(false);
            }
        }
        else if (hasMetWizard && !hasCompletedWizard)
        {
            foreach (var item in gameManager.meetWizardEvent)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in gameManager.transformWizardEvent)
            {
                item.gameObject.SetActive(false);
            }
        }
        else if (hasMetWizard && hasCompletedWizard)
        {
            foreach (var item in gameManager.meetWizardEvent)
            {
                item.gameObject.SetActive(false);
            }

            foreach (var item in gameManager.transformWizardEvent)
            {
                item.gameObject.SetActive(false);
            }

            if (!playedEvents.Contains(gameManager.gameEventsManager.gameEvents.IndexOf(gameManager.wizardController.victoryEvent)))
                playedEvents.Add(gameManager.gameEventsManager.gameEvents.IndexOf(gameManager.wizardController.victoryEvent));

            gameManager.wizardController.isComplete = true;
            //gameManager.wizardController.characterStage = FriendlyCharacterController.CharacterStages.IsSolved;
        }
    }

    private void OnApplicationPause(bool pause)
    {
        SaveGame();
    }

    void CheckHasMetUnderBoss(GameEvent passedEvent)
    {
        if(passedEvent == gameManager.underBossMeetEvent)
        {
            hasMetUnderboss = true;
        }
    }

    public void SetToHatDemoEvents() //Called by cheatList script
    {
        foreach (var item in gameManager.meetUnderBossEvent)
        {
            item.gameObject.SetActive(false);
        }

        foreach (var item in gameManager.transformUnderbossEvent)
        {
            item.gameObject.SetActive(true);
        }
    }

    public void SetToMeetUnderbossEvents() //Called by cheatList script
    {
        foreach (var item in gameManager.meetUnderBossEvent)
        {
            item.gameObject.SetActive(true);
        }

        foreach (var item in gameManager.transformUnderbossEvent)
        {
            item.gameObject.SetActive(false);
        }      
    }

    void SetCheats(PlayerData passedData)
    {
        cheatListScript.areaVolumeGameComplete.isOn = passedData.hasWonAreaVolumeGame;
        cheatListScript.areaVolumeGameSetSlider.value = passedData.AreaVolumeGameSetCount;
        cheatListScript.battlePlayedDemoAsc.isOn = passedData.hasPlayedBattleDemoAsc;
        cheatListScript.battlePlayedDemoDesc.isOn = passedData.hasPlayedBattleDemoDesc;
        cheatListScript.battleSortComplete.isOn = passedData.hasWonBattleSort;
        cheatListScript.battleSortEnemyHealthSlider.value = passedData.battleSortEnemyHealth;
        cheatListScript.bossGameComplete.isOn = passedData.hasWonBossTileGame;
        cheatListScript.clockGameComplete.isOn = passedData.hasWonClockGame;
        cheatListScript.clockGameTaskSlider.value = passedData.ClockGameSetCount;
        cheatListScript.difficultySlider.value = Mathf.Clamp(passedData.difficulty, 1, 4);
        cheatListScript.figuresGameComplete.isOn = passedData.hasWonFiguresGame;
        cheatListScript.figuresGameSetSlider.value = passedData.FiguresGameSetCount;
        cheatListScript.gemSlider.value = Mathf.Clamp(passedData.gems, 0, 1001);
        cheatListScript.mazeGameComplete.isOn = passedData.hasWonMazeGame;
        cheatListScript.mazeGameSetSlider.value = passedData.MazeGameSetCount;
        cheatListScript.orderGameComplete.isOn = passedData.hasWonOrderGame;
        cheatListScript.orderGameSetSlider.value = passedData.OrderGameSetCount;
        cheatListScript.racingGameComplete.isOn = passedData.hasWonRacingGame;
        cheatListScript.shooterGameComplete.isOn = passedData.hasWonShooterGame;
        cheatListScript.shooterGameSetSlider.value = passedData.ShooterSetCount;
        cheatListScript.starMakerGameComplete.isOn = passedData.hasWonStarMakerGame;
        cheatListScript.starMakerGameSetSlider.value = passedData.StarMakerGameSetCount;
        cheatListScript.tileGameComplete.isOn = passedData.hasWonTileGame;
        cheatListScript.tileGameSetSlider.value = passedData.TileGameSetCount;
    }

    void SetCheatsWeb(PlayerDataWeb passedData)
    {
        cheatListScript.areaVolumeGameComplete.isOn = passedData.hasWonAreaVolumeGame;
        cheatListScript.areaVolumeGameSetSlider.value = passedData.AreaVolumeGameSetCount;
        cheatListScript.battlePlayedDemoAsc.isOn = passedData.hasPlayedBattleDemoAsc;
        cheatListScript.battlePlayedDemoDesc.isOn = passedData.hasPlayedBattleDemoDesc;
        cheatListScript.battleSortComplete.isOn = passedData.hasWonBattleSort;
        cheatListScript.battleSortEnemyHealthSlider.value = passedData.battleSortEnemyHealth;
        cheatListScript.bossGameComplete.isOn = passedData.hasWonBossTileGame;
        cheatListScript.clockGameComplete.isOn = passedData.hasWonClockGame;
        cheatListScript.clockGameTaskSlider.value = passedData.ClockGameSetCount;
        cheatListScript.difficultySlider.value = Mathf.Clamp(passedData.difficulty, 1, 4);
        cheatListScript.figuresGameComplete.isOn = passedData.hasWonFiguresGame;
        cheatListScript.figuresGameSetSlider.value = passedData.FiguresGameSetCount;
        cheatListScript.gemSlider.value = Mathf.Clamp(passedData.gems, 0, 1001);
        cheatListScript.mazeGameComplete.isOn = passedData.hasWonMazeGame;
        cheatListScript.mazeGameSetSlider.value = passedData.MazeGameSetCount;
        cheatListScript.orderGameComplete.isOn = passedData.hasWonOrderGame;
        cheatListScript.orderGameSetSlider.value = passedData.OrderGameSetCount;
        cheatListScript.racingGameComplete.isOn = passedData.hasWonRacingGame;
        cheatListScript.shooterGameComplete.isOn = passedData.hasWonShooterGame;
        cheatListScript.shooterGameSetSlider.value = passedData.ShooterSetCount;
        cheatListScript.starMakerGameComplete.isOn = passedData.hasWonStarMakerGame;
        cheatListScript.starMakerGameSetSlider.value = passedData.StarMakerGameSetCount;
        cheatListScript.tileGameComplete.isOn = passedData.hasWonTileGame;
        cheatListScript.tileGameSetSlider.value = passedData.TileGameSetCount;
    }

    void ValidateWorldGemsCount()
    {
        CheckGemsTakenList(worldGemsTaken, worldGemsParent);
        CheckGemsTakenList(nightGemsTaken, nightGemsParent);
    }

    void CheckGemsTakenList(List<int> listOfGems, Transform parentOfObjects) // method to correct error in previous saves
    {
        int correctCount = parentOfObjects.childCount;

        if (listOfGems.Count != correctCount)
        {
            if(listOfGems.Count < correctCount)
            {
                Debug.Log("Adding to " + listOfGems);

                for (int i = listOfGems.Count; i < correctCount; i++)
                {
                    listOfGems.Add(1);
                }
            }
            else if(listOfGems.Count > correctCount)
            {
                Debug.Log("Subtracting from " + listOfGems);

                for (int i = listOfGems.Count; i-- > correctCount;)
                {
                    listOfGems.Remove(listOfGems[i]);
                }
            }
        }

        Debug.Log(listOfGems + " new count is " + listOfGems.Count);
        Debug.Log("Correct count should be " + correctCount);
    }    
}
