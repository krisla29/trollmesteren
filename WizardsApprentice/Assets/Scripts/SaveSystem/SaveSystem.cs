﻿
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveGame(PlayerStats playerStats)
    {
        if (!playerStats.gameManager.isWebGl) //Hack to ensure that everything is saved in webGl if some scripts call this method directly;
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = GameProperties.saveDirectory;
            FileStream stream = new FileStream(path, FileMode.Create);

            PlayerData data = new PlayerData(playerStats);

            formatter.Serialize(stream, data);
            stream.Close();

            playerStats.gameManager.saveGraphic.gameObject.SetActive(false);
        }
        else
        {
            playerStats.SaveGame();
        }
    }

    public static PlayerData LoadGame()
    {
        string path = GameProperties.saveDirectory;

        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            stream.Position = 0;
            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogError("saveGame file not found in " + path);
            return null;
        }
    }    
}
