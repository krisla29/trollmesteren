﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SavePlayerDataWeb
{
    public static PlayerDataWeb SaveData(PlayerStats playerStats)
    {
        PlayerDataWeb playerData = new PlayerDataWeb();

        PlayerStats.Trophy[] trophiesArray = playerStats.trophies.ToArray();
        playerData.trophies = new int[trophiesArray.Length];

        for (int i = 0; i < trophiesArray.Length; i++)
        {
            playerData.trophies[i] = Array.IndexOf(Enum.GetValues(trophiesArray[i].GetType()), trophiesArray[i]);
        }

        int index = Array.IndexOf(Enum.GetValues(playerStats.difficulty.GetType()), playerStats.difficulty);
        playerData.difficulty = index;
        playerData.battleSortEnemyHealth = playerStats.battleSortEnemyHealth;

        playerData.gems = playerStats.gems;
        playerData.wellGems = playerStats.wellGems;
        playerData.worldGemsTaken = playerStats.worldGemsTaken.ToArray();
        playerData.nightGemsTaken = playerStats.nightGemsTaken.ToArray();

        playerData.timeOfDay = playerStats.timeOfDay;

        playerData.lastLocation = playerStats.lastLocation;
        playerData.unlockedLocations = playerStats.unlockedLocations.ToArray();

        playerData.playedEvents = playerStats.playedEvents.ToArray();

        if (playerStats.startCharacter == CharacterSelector.PlayerCharacter.Boy)
            playerData.startCharacter = 0;
        else
            playerData.startCharacter = 1;

        playerData.currentCharacter = playerStats.currentCharacter;
        playerData.unlockedCharacters = playerStats.unlockedCharacters.ToArray();


        playerData.hasSeenTheWellFirstStep = playerStats.hasSeenTheWellFirstStep;
        playerData.hasMetWizard = playerStats.hasMetWizard;
        playerData.hasCompletedWizard = playerStats.hasCompletedWizard;
        playerData.hasMetUnderboss = playerStats.hasMetUnderboss;
        playerData.hasSeenDifficultyPanelDemo = playerStats.hasSeenDifficultyPanelDemo;
        playerData.hasSeenTheWellDemo = playerStats.hasSeenTheWellDemo;
        playerData.hasSeenOpening = playerStats.hasSeenOpening;
        playerData.hasSeenInteractionDemo = playerStats.hasSeenInteractionDemo;
        playerData.hasSeenTrophyDemo = playerStats.hasSeenTrophyDemo;
        playerData.hasUnlockedMap = playerStats.hasUnlockedMap;
        playerData.hasUnlockedHat = playerStats.hasUnlockedHat;
        playerData.hasOpenedCaveDoor = playerStats.hasOpenedCaveDoor;
        playerData.hasOpenedTowerScanner = playerStats.hasOpenedTowerScanner;
        playerData.hasOpenedBossDoor = playerStats.hasOpenedBossDoor;

        //Minigames:
        playerData.OrderGameSetCount = playerStats.OrderGameSetCount;
        playerData.hasWonOrderGame = playerStats.hasWonOrderGame;
        playerData.orderGameCompletedSets = playerStats.orderGameCompletedSets.ToArray();
        playerData.TileGameSetCount = playerStats.TileGameSetCount;
        playerData.hasWonTileGame = playerStats.hasWonTileGame;
        playerData.tileGameCompletedSets = playerStats.tileGameCompletedSets.ToArray();
        playerData.tileGameSetAchievementsKeys = new int[playerStats.tileGameSetAchievements.Count];
        playerData.tileGameSetAchievementsValues = new int[playerStats.tileGameSetAchievements.Count];
        int counter1 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.tileGameSetAchievements)
        {
            playerData.tileGameSetAchievementsKeys[counter1] = entry.Key;
            playerData.tileGameSetAchievementsValues[counter1] = entry.Value;
            counter1++;
        }
        playerData.BossTileGameSetCount = playerStats.BossTileGameSetCount;
        playerData.hasWonBossTileGame = playerStats.hasWonBossTileGame;
        playerData.bossTileGameCompletedSets = playerStats.bossTileGameCompletedSets.ToArray();
        playerData.bossTileGameSetAchievementsKeys = new int[playerStats.bossTileGameSetAchievements.Count];
        playerData.bossTileGameSetAchievementsValues = new int[playerStats.bossTileGameSetAchievements.Count];
        int counter2 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.bossTileGameSetAchievements)
        {
            playerData.bossTileGameSetAchievementsKeys[counter2] = entry.Key;
            playerData.bossTileGameSetAchievementsValues[counter2] = entry.Value;
            counter2++;
        }
        playerData.ShooterSetCount = playerStats.ShooterSetCount;
        playerData.hasWonShooterGame = playerStats.hasWonShooterGame;
        playerData.shooterGameCompletedSets = playerStats.shooterGameCompletedSets.ToArray();
        playerData.shooterGameSetAchievementsKeys = new int[playerStats.shooterGameSetAchievements.Count];
        playerData.shooterGameSetAchievementsValues = new int[playerStats.shooterGameSetAchievements.Count];
        int counter3 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.shooterGameSetAchievements)
        {
            playerData.shooterGameSetAchievementsKeys[counter3] = entry.Key;
            playerData.shooterGameSetAchievementsValues[counter3] = entry.Value;
            counter3++;
        }
        playerData.MazeGameSetCount = playerStats.MazeGameSetCount;
        playerData.hasWonMazeGame = playerStats.hasWonMazeGame;
        playerData.mazeGameCompletedSets = playerStats.mazeGameCompletedSets.ToArray();
        playerData.mazeGameSetAchievementsKeys = new int[playerStats.mazeGameSetAchievements.Count];
        playerData.mazeGameSetAchievementsValues = new int[playerStats.mazeGameSetAchievements.Count];
        int counter4 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.mazeGameSetAchievements)
        {
            playerData.mazeGameSetAchievementsKeys[counter4] = entry.Key;
            playerData.mazeGameSetAchievementsValues[counter4] = entry.Value;
            counter4++;
        }
        playerData.AreaVolumeGameSetCount = playerStats.AreaVolumeGameSetCount;
        playerData.hasWonAreaVolumeGame = playerStats.hasWonAreaVolumeGame;
        playerData.areaVolumeGameCompletedSets = playerStats.areaVolumeGameCompletedSets.ToArray();
        playerData.areaVolumeGameSetAchievementsKeys = new int[playerStats.areaVolumeGameSetAchievements.Count];
        playerData.areaVolumeGameSetAchievementsValues = new int[playerStats.areaVolumeGameSetAchievements.Count];
        int counter5 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.areaVolumeGameSetAchievements)
        {
            playerData.areaVolumeGameSetAchievementsKeys[counter5] = entry.Key;
            playerData.areaVolumeGameSetAchievementsValues[counter5] = entry.Value;
            counter5++;
        }
        playerData.StarMakerGameSetCount = playerStats.StarMakerGameSetCount;
        playerData.hasWonStarMakerGame = playerStats.hasWonStarMakerGame;
        playerData.starMakerGameCompletedSets = playerStats.starMakerGameCompletedSets.ToArray();
        playerData.hasWonFiguresGame = playerStats.hasWonFiguresGame;
        playerData.FiguresGameSetCount = playerStats.FiguresGameSetCount;
        playerData.figuresGameFiguresCompleted = playerStats.figuresGameFiguresCompleted.ToArray();
        playerData.hasWonClockGame = playerStats.hasWonClockGame;
        playerData.clockGameCompletedTasks = playerStats.clockGameCompletedTasks;
        playerData.ClockGameSetCount = playerStats.ClockGameSetCount;
        playerData.hasWonRacingGame = playerStats.hasWonRacingGame;
        playerData.hasWonBattleSort = playerStats.hasWonBattleSort;
        playerData.hasPlayedBattleDemoDesc = playerStats.hasPlayedBattleDemoDesc;
        playerData.hasPlayedBattleDemoAsc = playerStats.hasPlayedBattleDemoAsc;

        Debug.Log("Saved Game");


        return playerData;
    }
}
