﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int[] trophies;
    public int difficulty;
    public float battleSortEnemyHealth;

    public int gems;
    public int wellGems;
    public int[] worldGemsTaken;
    public int[] nightGemsTaken;

    public int[] timeOfDay; 

    public int lastLocation;
    public int[] unlockedLocations;

    public int[] playedEvents;

    public int currentCharacter;
    public int[] unlockedCharacters;
    public int startCharacter;

    public bool hasSeenTheWellFirstStep;
    public bool hasMetWizard;
    public bool hasCompletedWizard;
    public bool hasMetUnderboss;
    public bool hasSeenDifficultyPanelDemo;
    public bool hasSeenTheWellDemo;
    public bool hasSeenOpening;
    public bool hasSeenInteractionDemo;
    public bool hasSeenTrophyDemo;
    public bool hasUnlockedMap;
    public bool hasUnlockedHat;
    public bool hasOpenedCaveDoor;
    public bool hasOpenedTowerScanner;
    public bool hasOpenedBossDoor;

    public int OrderGameSetCount;
    public bool hasWonOrderGame;
    public int[] orderGameCompletedSets;
    public int TileGameSetCount;
    public bool hasWonTileGame;
    public int[] tileGameCompletedSets;
    public int[] tileGameSetAchievementsKeys;
    public int[] tileGameSetAchievementsValues;
    public int BossTileGameSetCount;
    public bool hasWonBossTileGame;
    public int[] bossTileGameCompletedSets;
    public int[] bossTileGameSetAchievementsKeys;
    public int[] bossTileGameSetAchievementsValues;
    public int ShooterSetCount;
    public bool hasWonShooterGame;
    public int[] shooterGameCompletedSets;
    public int[] shooterGameSetAchievementsKeys;
    public int[] shooterGameSetAchievementsValues;
    public int MazeGameSetCount;
    public bool hasWonMazeGame;
    public int[] mazeGameCompletedSets;
    public int[] mazeGameSetAchievementsKeys;
    public int[] mazeGameSetAchievementsValues;
    public bool hasWonAreaVolumeGame;
    public int AreaVolumeGameSetCount;
    public int[] areaVolumeGameCompletedSets;
    public int[] areaVolumeGameSetAchievementsKeys;
    public int[] areaVolumeGameSetAchievementsValues;
    public int StarMakerGameSetCount;
    public bool hasWonStarMakerGame;
    public int[] starMakerGameCompletedSets;
    public int FiguresGameSetCount;
    public bool hasWonFiguresGame;
    public int[] figuresGameFiguresCompleted;
    public bool hasWonClockGame;
    public int clockGameCompletedTasks;
    public int ClockGameSetCount;
    public bool hasWonRacingGame;
    public bool hasWonBattleSort;
    public bool hasPlayedBattleDemoDesc;
    public bool hasPlayedBattleDemoAsc;

    public PlayerData (PlayerStats playerStats)
    {
        PlayerStats.Trophy[] trophiesArray = playerStats.trophies.ToArray();
        trophies = new int[trophiesArray.Length];

        for (int i = 0; i < trophiesArray.Length; i++)
        {
            trophies[i] = Array.IndexOf(Enum.GetValues(trophiesArray[i].GetType()), trophiesArray[i]);
        }

        int index = Array.IndexOf(Enum.GetValues(playerStats.difficulty.GetType()), playerStats.difficulty);
        difficulty = index;
        battleSortEnemyHealth = playerStats.battleSortEnemyHealth;

        gems = playerStats.gems;
        wellGems = playerStats.wellGems;
        worldGemsTaken = playerStats.worldGemsTaken.ToArray();
        nightGemsTaken = playerStats.nightGemsTaken.ToArray();

        timeOfDay = playerStats.timeOfDay;

        lastLocation = playerStats.lastLocation;
        unlockedLocations = playerStats.unlockedLocations.ToArray();

        playedEvents = playerStats.playedEvents.ToArray();

        if (playerStats.startCharacter == CharacterSelector.PlayerCharacter.Boy)
            startCharacter = 0;
        else
            startCharacter = 1;

        currentCharacter = playerStats.currentCharacter;
        unlockedCharacters = playerStats.unlockedCharacters.ToArray();


        hasSeenTheWellFirstStep = playerStats.hasSeenTheWellFirstStep;
        hasMetWizard = playerStats.hasMetWizard;
        hasCompletedWizard = playerStats.hasCompletedWizard;
        hasMetUnderboss = playerStats.hasMetUnderboss;
        hasSeenDifficultyPanelDemo = playerStats.hasSeenDifficultyPanelDemo;
        hasSeenTheWellDemo = playerStats.hasSeenTheWellDemo;
        hasSeenOpening = playerStats.hasSeenOpening;
        hasSeenInteractionDemo = playerStats.hasSeenInteractionDemo;
        hasSeenTrophyDemo = playerStats.hasSeenTrophyDemo;
        hasUnlockedMap = playerStats.hasUnlockedMap;
        hasUnlockedHat = playerStats.hasUnlockedHat;
        hasOpenedCaveDoor = playerStats.hasOpenedCaveDoor;
        hasOpenedTowerScanner = playerStats.hasOpenedTowerScanner;
        hasOpenedBossDoor = playerStats.hasOpenedBossDoor;

        //Minigames:
        OrderGameSetCount = playerStats.OrderGameSetCount;
        hasWonOrderGame = playerStats.hasWonOrderGame;
        orderGameCompletedSets = playerStats.orderGameCompletedSets.ToArray();
        TileGameSetCount = playerStats.TileGameSetCount;
        hasWonTileGame = playerStats.hasWonTileGame;
        tileGameCompletedSets = playerStats.tileGameCompletedSets.ToArray();
        tileGameSetAchievementsKeys = new int[playerStats.tileGameSetAchievements.Count];
        tileGameSetAchievementsValues = new int[playerStats.tileGameSetAchievements.Count];
        int counter1 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.tileGameSetAchievements)
        {
            tileGameSetAchievementsKeys[counter1] = entry.Key;
            tileGameSetAchievementsValues[counter1] = entry.Value;
            counter1++;
        }
        BossTileGameSetCount = playerStats.BossTileGameSetCount;
        hasWonBossTileGame = playerStats.hasWonBossTileGame;
        bossTileGameCompletedSets = playerStats.bossTileGameCompletedSets.ToArray();
        bossTileGameSetAchievementsKeys = new int[playerStats.bossTileGameSetAchievements.Count];
        bossTileGameSetAchievementsValues = new int[playerStats.bossTileGameSetAchievements.Count];
        int counter2 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.bossTileGameSetAchievements)
        {
            bossTileGameSetAchievementsKeys[counter2] = entry.Key;
            bossTileGameSetAchievementsValues[counter2] = entry.Value;
            counter2++;
        }
        ShooterSetCount = playerStats.ShooterSetCount;
        hasWonShooterGame = playerStats.hasWonShooterGame;
        shooterGameCompletedSets = playerStats.shooterGameCompletedSets.ToArray();
        shooterGameSetAchievementsKeys = new int[playerStats.shooterGameSetAchievements.Count];
        shooterGameSetAchievementsValues = new int[playerStats.shooterGameSetAchievements.Count];
        int counter3 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.shooterGameSetAchievements)
        {
            shooterGameSetAchievementsKeys[counter3] = entry.Key;
            shooterGameSetAchievementsValues[counter3] = entry.Value;
            counter3++;
        }
        MazeGameSetCount = playerStats.MazeGameSetCount;
        hasWonMazeGame = playerStats.hasWonMazeGame;
        mazeGameCompletedSets = playerStats.mazeGameCompletedSets.ToArray();
        mazeGameSetAchievementsKeys = new int[playerStats.mazeGameSetAchievements.Count];
        mazeGameSetAchievementsValues = new int[playerStats.mazeGameSetAchievements.Count];
        int counter4 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.mazeGameSetAchievements)
        {
            mazeGameSetAchievementsKeys[counter4] = entry.Key;
            mazeGameSetAchievementsValues[counter4] = entry.Value;
            counter4++;
        }
        AreaVolumeGameSetCount = playerStats.AreaVolumeGameSetCount;
        hasWonAreaVolumeGame = playerStats.hasWonAreaVolumeGame;
        areaVolumeGameCompletedSets = playerStats.areaVolumeGameCompletedSets.ToArray();
        areaVolumeGameSetAchievementsKeys = new int[playerStats.areaVolumeGameSetAchievements.Count];
        areaVolumeGameSetAchievementsValues = new int[playerStats.areaVolumeGameSetAchievements.Count];
        int counter5 = 0;
        foreach (KeyValuePair<int, int> entry in playerStats.areaVolumeGameSetAchievements)
        {
            areaVolumeGameSetAchievementsKeys[counter5] = entry.Key;
            areaVolumeGameSetAchievementsValues[counter5] = entry.Value;
            counter5++;
        }
        StarMakerGameSetCount = playerStats.StarMakerGameSetCount;
        hasWonStarMakerGame = playerStats.hasWonStarMakerGame;
        starMakerGameCompletedSets = playerStats.starMakerGameCompletedSets.ToArray();
        hasWonFiguresGame = playerStats.hasWonFiguresGame;
        FiguresGameSetCount = playerStats.FiguresGameSetCount;
        figuresGameFiguresCompleted = playerStats.figuresGameFiguresCompleted.ToArray();
        hasWonClockGame = playerStats.hasWonClockGame;
        clockGameCompletedTasks = playerStats.clockGameCompletedTasks;
        ClockGameSetCount = playerStats.ClockGameSetCount;
        hasWonRacingGame = playerStats.hasWonRacingGame;
        hasWonBattleSort = playerStats.hasWonBattleSort;
        hasPlayedBattleDemoDesc = playerStats.hasPlayedBattleDemoDesc;
        hasPlayedBattleDemoAsc = playerStats.hasPlayedBattleDemoAsc;
    }
}
