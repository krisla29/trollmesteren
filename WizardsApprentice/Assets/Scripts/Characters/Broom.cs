﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Broom : MonoBehaviour
{
    public CharacterSelector playerCharacterSelector;
    public Transform broomGraphic;

    private void OnEnable()
    {
        if (playerCharacterSelector)
        {
            CharacterSelector.OnChangeCharacter += CheckCharacter;
            CheckCharacter(playerCharacterSelector.playerCharacter);
        }
    }

    private void OnDisable()
    {
        if (playerCharacterSelector)
        {
            CharacterSelector.OnChangeCharacter -= CheckCharacter;
        }
    }

    void CheckCharacter(CharacterSelector.PlayerCharacter passedCharacter)
    {
        if (passedCharacter == CharacterSelector.PlayerCharacter.Wizard)
            broomGraphic.gameObject.SetActive(false);
        else
            broomGraphic.gameObject.SetActive(true);
    }

    public void ChangeIfWizard(bool isWizard)
    {
        if (isWizard)
            broomGraphic.gameObject.SetActive(false);
        else
            broomGraphic.gameObject.SetActive(true);
    }
}
