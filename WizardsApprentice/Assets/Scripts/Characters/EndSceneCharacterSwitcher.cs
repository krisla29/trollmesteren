﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneCharacterSwitcher : MonoBehaviour
{
    public CharacterSelectorOthers characterSelectorOthers;
    public Broom broom;
    public Vector2 timeInterval = new Vector2(5f, 15f);
    private Character currentCharacter;
    private Character lastCharacter;

    public GameObject portalEffect;
    public Transform effectsContainer;

    private Coroutine scaleRoutine;
    public Transform follower;
    private Vector3 followerStartScale;

    public List<AudioSource> laughs = new List<AudioSource>();
    public AudioSource wizardLaugh;

    private void Awake()
    {
        followerStartScale = transform.localScale;
    }

    private void OnEnable()
    {
        StartCoroutine(SwitchRandomly());

        SpawnPortal();

        if (scaleRoutine != null)
            StopCoroutine(scaleRoutine);

        scaleRoutine = StartCoroutine(ScaleTransform());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator SwitchRandomly()
    {
        if(currentCharacter)
            lastCharacter = currentCharacter;

        float randTime = Random.Range(timeInterval.x, timeInterval.y);

        yield return new WaitForSeconds(randTime);

        SpawnPortal();

        if (scaleRoutine != null)
            StopCoroutine(scaleRoutine);

        scaleRoutine = StartCoroutine(ScaleTransform());

        int i = characterSelectorOthers.SetRandomCharacter();

        currentCharacter = characterSelectorOthers.characters[i];

        if (currentCharacter.character == CharacterSelector.PlayerCharacter.Wizard)
        {
            broom.ChangeIfWizard(true);

            if (wizardLaugh)
                wizardLaugh.Play();
        }
        else
        {
            int randNum = Random.Range(0, laughs.Count);

            if (laughs[randNum])
                laughs[randNum].Play();
        }

        if (lastCharacter && currentCharacter)
        {
            if (lastCharacter.character == CharacterSelector.PlayerCharacter.Wizard && currentCharacter.character != CharacterSelector.PlayerCharacter.Wizard)
                broom.ChangeIfWizard(false);
        }

        StartCoroutine(SwitchRandomly());
    }

    void SpawnPortal()
    {
        if (portalEffect)
        {
            GameObject pEffect = null;

            if (lastCharacter)
                pEffect = Instantiate(portalEffect, lastCharacter.transform.position, Quaternion.identity, effectsContainer);
            else
                pEffect = Instantiate(portalEffect, follower.position, Quaternion.identity, effectsContainer);
        }
    }

    private IEnumerator ScaleTransform()
    {
        float t = 0;
        float animTime = 0.5f;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            follower.localScale = Vector3.Lerp(Vector3.zero, followerStartScale, h);
            yield return new WaitForEndOfFrame();
        }
    }
}
