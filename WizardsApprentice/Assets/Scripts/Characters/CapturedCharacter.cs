﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapturedCharacter : MonoBehaviour
{
    public GameManager gameManager;
    public FriendlyCharacterController characterController;
    public SceneController caveInsideScene;
    public Animator boyAnim;
    public Animator girlAnim;

    private void OnEnable()
    {
        CharacterSelector.OnChangeCharacter += CheckCapturedCharacter;
        GameManager.OnSwitchScene += CheckScene;
    }

    private void OnDisable()
    {
        CharacterSelector.OnChangeCharacter -= CheckCapturedCharacter;
        GameManager.OnSwitchScene -= CheckScene;
    }

    private void Awake()
    {
        
    }

    void CheckScene(SceneController passedScene)
    {
        if(passedScene == caveInsideScene)
        {
            CheckIfPlayerIsCapturedCharacter();
            print("Checked scene caveInside");
        }
    }

    private void Start()
    {
        CheckStartCharacter();
        CheckIfPlayerIsCapturedCharacter();
    }

    void SetCharacter(Animator characterAnim)
    {
        characterController.animatorTrue = characterAnim;
        characterController.trueCharacter = characterAnim.gameObject;
    }

    void CheckStartCharacter()
    {
        if(gameManager.playerStats.startCharacter == CharacterSelector.PlayerCharacter.Boy)
        {
            SetCharacter(girlAnim);
            characterController.character = CharacterSelector.PlayerCharacter.Girl;
        }
        else if(gameManager.playerStats.startCharacter == CharacterSelector.PlayerCharacter.Girl)
        {
            SetCharacter(boyAnim);
            characterController.character = CharacterSelector.PlayerCharacter.Boy;
        }
    }

    void CheckIfPlayerIsCapturedCharacter()
    {
        if (gameManager.playerStats.hasUnlockedHat)
        {
            if (gameManager.playerStats.startCharacter == CharacterSelector.PlayerCharacter.Boy && gameManager.player.characterSelector.playerCharacter == CharacterSelector.PlayerCharacter.Girl
                || gameManager.playerStats.startCharacter == CharacterSelector.PlayerCharacter.Girl && gameManager.player.characterSelector.playerCharacter == CharacterSelector.PlayerCharacter.Boy)
            {
                HideCapturedCharacter();               
            }
            else
            {
                ShowCapturedCharacter();
            }
        }
    }

    void HideCapturedCharacter()
    {
        characterController.animatorTrue.gameObject.SetActive(false);        
    }

    void ShowCapturedCharacter()
    {
        characterController.animatorTrue.gameObject.SetActive(true);
    }

    void CheckCapturedCharacter(CharacterSelector.PlayerCharacter passedCharacter)
    {
        if (gameManager.playerStats.hasUnlockedHat)
        {
            if (gameManager.playerStats.startCharacter == CharacterSelector.PlayerCharacter.Boy && passedCharacter == CharacterSelector.PlayerCharacter.Girl
                || gameManager.playerStats.startCharacter == CharacterSelector.PlayerCharacter.Girl && passedCharacter == CharacterSelector.PlayerCharacter.Boy)
            {
                print("Requested character " + passedCharacter);
                HideCapturedCharacter();
            }
        }
    }
}
