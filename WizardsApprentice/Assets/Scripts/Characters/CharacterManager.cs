﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour
{
    public List<FriendlyCharacterController> characters = new List<FriendlyCharacterController>();

    private void OnEnable()
    {
        GameManager.OnSwitchScene += CheckVisibility;
    }

    private void OnDisable()
    {
        GameManager.OnSwitchScene -= CheckVisibility;
    }

    void CheckVisibility(SceneController passedSceneController)
    {
        if(passedSceneController == null)
        {
            for (int i = 0; i < characters.Count; i++)
            {
                if(characters[i].showInWorld)
                {
                    if (!characters[i].gameObject.activeSelf)
                        characters[i].gameObject.SetActive(true);
                }
            }
        }
        else
        {
            for (int i = 0; i < characters.Count; i++)
            {
                bool check = false;

                for (int e = 0; e < characters[i].showInScenes.Count; e++)
                {                    
                    if(passedSceneController == characters[i].showInScenes[e])
                    {
                        if (!characters[i].gameObject.activeSelf)
                            characters[i].gameObject.SetActive(true);

                        check = true;
                    }
                }

                if(!check)
                {
                    if (characters[i].gameObject.activeSelf)
                        characters[i].gameObject.SetActive(false);
                }
            }
        }
    }
}
