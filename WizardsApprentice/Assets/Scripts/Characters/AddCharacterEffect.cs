﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddCharacterEffect : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject nc_worldEffect;
    public GameObject nc_uiEffect;
    public GameObject changeCharacterEffect;
    public AudioSource soundNewCharacter;
    public AudioSource soundNewCharacter2;
    public AudioSource soundNewCharacterUI;
    public AudioSource soundError;
    public RectTransform buttonRect;
    public Image buttonImage;

    private Color startColor;
    private Coroutine newCharacter;

    [SerializeField] private FriendlyCharacterController currentCharacterController;

    private void Awake()
    {
        startColor = buttonImage.color;       
    }

    private void OnEnable()
    {
        FriendlyCharacterController.OnAddCharacter += SetCurrentCharacter;
        GameManager.OnAfterSwitchOffEvent += CheckEvent;
    }

    private void OnDisable()
    {
        FriendlyCharacterController.OnAddCharacter -= SetCurrentCharacter;
        GameManager.OnAfterSwitchOffEvent -= CheckEvent;

        StopAllCoroutines();
        newCharacter = null;

        if (buttonRect)
        {
            buttonRect.transform.localScale = Vector3.one;
            buttonRect.transform.localEulerAngles = Vector3.zero;
        }

        if (buttonImage)
            buttonImage.color = startColor;
    }

    private void SetCurrentCharacter(FriendlyCharacterController passedCharacterController)
    {
        currentCharacterController = passedCharacterController;
    }

    private void CheckEvent(GameEvent passedEvent)
    {
        if (currentCharacterController)
        {
            if (currentCharacterController.victoryEvent)
            {
                if (passedEvent.transform == currentCharacterController.victoryEvent)
                {
                    NewCharacter();
                }
            }
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        newCharacter = null;
    }

    public void NewCharacter()
    {
        if (newCharacter != null)
            StopCoroutine(newCharacter);

        newCharacter = StartCoroutine(AddNewCharacter());
    }

    private IEnumerator AddNewCharacter()
    {
        yield return new WaitForSeconds(0.3f);

        GameObject worldEffect = null;
        GameObject uiEffect = null;
        Vector3 characterPosition = currentCharacterController.transform.position + Vector3.up;
        Vector3 playerPosition = gameManager.player.transform.position + Vector3.up;
        Vector3 dir = characterPosition - playerPosition;
        Quaternion lookDir = Quaternion.LookRotation(dir);

        if (nc_worldEffect)
        {
            worldEffect = Instantiate(nc_worldEffect, characterPosition, lookDir, gameManager.worldEffectsContainer);
            //worldEffect.transform.position = characterPosition;
        }

        if (soundNewCharacter2)
            soundNewCharacter2.Play();

        float x = 0;
        float animTime = 1f;
       

        while (x < 1)
        {
            x += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(x);
            playerPosition = gameManager.player.transform.position + Vector3.up;
            dir = characterPosition - playerPosition;
            worldEffect.transform.position = Vector3.Lerp(characterPosition, playerPosition, h);
            worldEffect.transform.rotation = Quaternion.Slerp(lookDir, Quaternion.LookRotation(dir), h);
            yield return new WaitForEndOfFrame();
        }

        if (soundNewCharacter)
            soundNewCharacter.Play();
        
        yield return new WaitForSeconds(1);

        if (nc_uiEffect)
        {
            uiEffect = Instantiate(nc_uiEffect, gameManager.uiEffectsContainer);
            var trans = uiEffect.GetComponent<RectTransform>();
            trans.position = buttonRect.position;
        }

        if (soundNewCharacterUI)
            soundNewCharacterUI.Play();

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1f;
            var h = LerpUtils.SmootherStep(t);
            var s = Mathf.Sin(h * Mathf.PI);
            buttonRect.transform.localScale = Vector3.one + (Vector3.one * s * 0.25f);
            buttonRect.transform.localEulerAngles = new Vector3(0, 0, s * 45f);
            buttonImage.color = startColor + new Color(s * 0.25f, 0, 0, 0);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(3);
    }

    public void ChangeCharacterEffect() // Used by Players's CharacteSelector script
    {
        GameObject effect = null;

        if (soundNewCharacter)
            soundNewCharacter.Play();

        if (changeCharacterEffect)
            effect = Instantiate(changeCharacterEffect, gameManager.player.transform.position + Vector3.up, Quaternion.identity, gameManager.worldEffectsContainer);
    }

    public void NoCharacterToChange() // Used by Players's CharacteSelector script
    {
        if (soundError)
            soundError.Play();
    }
}
