﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectorOthers : MonoBehaviour
{
    public CharacterSelector playerCharacterSelector;
    public List<Character> characters = new List<Character>();

    private void OnEnable()
    {
        if (playerCharacterSelector)
        {
            CharacterSelector.OnChangeCharacter += CheckCharacter;
            CheckCharacter(playerCharacterSelector.playerCharacter);
        }
    }

    private void OnDisable()
    {
        if (playerCharacterSelector)
        {
            CharacterSelector.OnChangeCharacter -= CheckCharacter;
        }
    }

    public void CheckCharacter(CharacterSelector.PlayerCharacter passedCharacter)
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if(passedCharacter == characters[i].character)
                characters[i].gameObject.SetActive(true);
            else
                characters[i].gameObject.SetActive(false);
        }        
    }

    public int SetRandomCharacter()
    {
        int randIndex = Random.Range(0, characters.Count);

        for (int i = 0; i < characters.Count; i++)
        {
            if (i == randIndex)
                characters[i].gameObject.SetActive(true);
            else
                characters[i].gameObject.SetActive(false);
        }

        return randIndex;
    }
}
