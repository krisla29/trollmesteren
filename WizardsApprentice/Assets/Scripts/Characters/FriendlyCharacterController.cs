﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendlyCharacterController : MonoBehaviour, ICharacter
{   
    public GameManager gameManager;
    public GameEventsManager gameEventsManager;
    public CharacterSelector.PlayerCharacter character;
    public PlayerStats.Trophy linkedTrophy;
    public bool showCharacter = true;
    public List<SceneController> showInScenes = new List<SceneController>();
    public bool showInWorld = true;
    public bool isCapturedCharacter;
    public bool isWizard;
    public bool isUnderBoss;
    public bool muteCharacter;
    public bool isComplete;
    public bool isTransformedHumanoid = true;
    public bool isInteractable = true;
    public float victoryDelay = 3f;
    public float graphicDisplayTime = 14f;
    public Animator animatorTrue;
    public Animator animatorTransformed;
    public Animator currentAnimator;
    public GameObject trueCharacter;
    public GameObject transformedCharacter;
    public GameObject talkBubbleProblem;
    public GameObject talkBubbleSolved;
    private PlayerController player;
    public bool orientTowardsPlayer;
    public GameObject birthEffect;
    public GameObject transformEffect;
    //public enum CharacterStages { HasNotMetPlayer, WaitForPlayerToGame, WhilePlayerGames, WaitForPlayerDebrief, IsSolved}
    //public CharacterStages characterStage;

    public Transform victoryTrigger;
    public Transform victoryEvent;
    public Transform meetCharacterTrigger;
    public Transform meetCharacterEvent;

    public AudioSource waveAudio;
    public AudioSource anxiousTalk;
    public AudioSource anxiousTalk2;
    public AudioSource celebrate;
    public AudioSource transformEffectSound;
    public AudioSource happyTalk;
    public AudioSource sob;
    public AudioSource whistle;

    private Coroutine spinCharacter;
    private Coroutine jumpCharacter;

    public List<Transform> wakeUpOnMeetEventStart = new List<Transform>();
    public List<Transform> sleepOnMeetEventStart = new List<Transform>();

    public List<Transform> wakeUpOnVictoryEventStart = new List<Transform>();
    public List<Transform> sleepOnVictoryEventStart = new List<Transform>();

    public delegate void CharacterEvent(FriendlyCharacterController thisController);
    public static event CharacterEvent OnAddCharacter; //Sends to AddCharacterEffect script (in HudCanvas)
    public static event CharacterEvent OnInitialized; // Sends to CapturedCharacter script

    public void InitializeCharacter()
    {
        Debug.Log("Initializing character " + character);
        spinCharacter = null;
        jumpCharacter = null;

        if (gameManager.playerStats.playedEvents.Contains(gameEventsManager.gameEvents.IndexOf(victoryEvent)))
        {
            if (!isUnderBoss && !isCapturedCharacter)
                isComplete = true;
        }
        else
        {
            if(!isUnderBoss && !isCapturedCharacter)
                isComplete = false;
        }

        if (!isComplete)
        {

            var randTime = Random.Range(0, 5f);

            if (!isWizard && !isUnderBoss)
                animatorTransformed.SetBool("Sad", true);
            else if (isWizard)
            { }
            else if(isUnderBoss)
            { }
 

            trueCharacter.SetActive(false);
            transformedCharacter.SetActive(true);
            currentAnimator = animatorTransformed;
            Debug.Log("Set character " + character + " to transformed");
        }
        else
        {
            if(!isCapturedCharacter)
                trueCharacter.SetActive(true);

            transformedCharacter.SetActive(false);
            currentAnimator = animatorTrue;
            currentAnimator.SetBool("isHappyIdle", true);

            if(isUnderBoss)
            {
                currentAnimator.SetBool("hasLostUnderboss", true);
                animatorTransformed.SetBool("hasLostUnderboss", true);
                animatorTrue.SetBool("hasLostUnderboss", true);
            }

            Debug.Log("Set character " + character + " to human");
        }

        CheckCharacterEvent();   
    }

    void SetToVictoryEvent()
    {
        if (gameManager.playerStats.trophies.Contains(linkedTrophy) && !gameManager.playerStats.playedEvents.Contains(gameEventsManager.gameEvents.IndexOf(victoryEvent)) && !isWizard && !isCapturedCharacter && !isUnderBoss)
        {
            if (meetCharacterTrigger)
                meetCharacterTrigger.gameObject.SetActive(false);
            if (meetCharacterEvent)
                meetCharacterEvent.gameObject.SetActive(false);

            if (victoryTrigger)
                victoryTrigger.gameObject.SetActive(true);
            if (victoryEvent)
                victoryEvent.gameObject.SetActive(true);
        }
    }

    void SetToStartEvent()
    {
        if (!gameManager.playerStats.trophies.Contains(linkedTrophy) && !gameManager.playerStats.playedEvents.Contains(gameEventsManager.gameEvents.IndexOf(meetCharacterEvent)) && !isWizard && !isCapturedCharacter && !isUnderBoss)
        {
            if (meetCharacterTrigger)
                meetCharacterTrigger.gameObject.SetActive(true);
            if (meetCharacterEvent)
                meetCharacterEvent.gameObject.SetActive(true);

            if (victoryTrigger)
                victoryTrigger.gameObject.SetActive(false);
            if (victoryEvent)
                victoryEvent.gameObject.SetActive(false);
        }
        else if(!gameManager.playerStats.trophies.Contains(linkedTrophy) && gameManager.playerStats.playedEvents.Contains(gameEventsManager.gameEvents.IndexOf(meetCharacterEvent)) && !isWizard && !isCapturedCharacter && !isUnderBoss)
        {
            if (meetCharacterTrigger)
                meetCharacterTrigger.gameObject.SetActive(false);
            if (meetCharacterEvent)
                meetCharacterEvent.gameObject.SetActive(false);

            if (victoryTrigger)
                victoryTrigger.gameObject.SetActive(false);
            if (victoryEvent)
                victoryEvent.gameObject.SetActive(false);
        }
    }

    void CheckCharacterEvent()
    {
        if (gameManager.playerStats.trophies.Contains(linkedTrophy))
            SetToVictoryEvent();
        else
            SetToStartEvent();
    }

    private void OnEnable()
    {
        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>(); //Error, missing tag?

        if(gameManager.player)
        {
            player = gameManager.player;
        }

        SpawnPlayer.OnSpawnNewPlayer += ReferencePlayer;
        GameEvent.OnGameEventStart += CheckVictoryEvent;
        GameEvent.OnGameEventStart += CheckMeetEvent;
        CharacterSelector.OnChangeCharacter += CheckPlayerCharacter;
        PlayerStats.OnCheckCharacterEvents += CheckCharacterEvent;
        PlayerStats.OnInitializeCharacter += InitializeCharacter;

        talkBubbleProblem.gameObject.SetActive(false);
        talkBubbleSolved.gameObject.SetActive(false);

        if(isUnderBoss)
        {
            if (isComplete)
            {
                currentAnimator.SetBool("hasLostUnderboss", true);
                animatorTransformed.SetBool("hasLostUnderboss", true);
                animatorTrue.SetBool("hasLostUnderboss", true);
            }
        }
    }

    private void OnDisable()
    {
        SpawnPlayer.OnSpawnNewPlayer -= ReferencePlayer;
        GameEvent.OnGameEventStart -= CheckVictoryEvent;
        GameEvent.OnGameEventStart -= CheckMeetEvent;
        CharacterSelector.OnChangeCharacter -= CheckPlayerCharacter;
        PlayerStats.OnCheckCharacterEvents -= CheckCharacterEvent;
        PlayerStats.OnInitializeCharacter -= InitializeCharacter;

        StopAllCoroutines();
        spinCharacter = null;
        jumpCharacter = null;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        spinCharacter = null;
        jumpCharacter = null;
        CancelInvoke();
    }

    void CheckVictoryEvent(GameEvent passedEvent)
    {
        if(passedEvent.transform == victoryEvent)
        {
            if (isWizard)
            {
                gameManager.playerStats.hasCompletedWizard = true;
            }
            //print("Boy got message!");
            if (!isTransformedHumanoid && !isUnderBoss)
            {
                animatorTransformed.SetBool("Sad", false);
                animatorTransformed.SetBool("Happy", true);
                StartCoroutine(ScaleTransformed());
            }

            if(isUnderBoss)
            {
                animatorTransformed.SetBool("hasLostUnderboss", true);
                animatorTrue.SetBool("hasLostUnderboss", true);
            }

            Invoke("SetVictory", victoryDelay);

            foreach (var item in wakeUpOnVictoryEventStart)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in sleepOnVictoryEventStart)
            {
                item.gameObject.SetActive(false);
            }

            if(!isUnderBoss && !isUnderBoss)
            {
                UnlockCharacter(character);
            }
        }
    }

    void CheckMeetEvent(GameEvent passedEvent)
    {        
        if (passedEvent.transform == meetCharacterEvent)
        {
            if (isWizard)
                gameManager.playerStats.hasMetWizard = true;

            TalkToPlayerAnxiously();

            foreach (var item in wakeUpOnMeetEventStart)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in sleepOnMeetEventStart)
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    void TalkToPlayerAnxiously()
    {
        CancelInvoke("PlayAnxiousSound");
        StopAllCoroutines();

        if (!isTransformedHumanoid)
        {
            if (!isWizard && !isUnderBoss)
                animatorTransformed.SetTrigger("SadTalk");
            else if (isWizard)
                animatorTransformed.SetTrigger("TriggerTalk");
            else if (isUnderBoss)
                animatorTransformed.SetTrigger("HappyTalk");
        }

        if (talkBubbleProblem)
            StartCoroutine(ShowGraphic(talkBubbleProblem));

        if (!isWizard && !isUnderBoss)
        {
            Invoke("PlayAnxiousSound", 1.5f);
            Invoke("PlayAnxiousSound", 4f);
            Invoke("PlayAnxiousSound", 5.5f);
            Invoke("PlayAnxiousSound", 8f);
        }
        else if(isWizard)
        {
            Invoke("PlayAnxiousSound", 1.5f);
            Invoke("PlayAnxiousSound2", 5.5f);
        }
        else if(isUnderBoss)
        {
            Invoke("PlayAnxiousSound", 1.5f);
            Invoke("PlayAnxiousSound2", 3.5f);
            Invoke("PlayAnxiousSound", 6.5f);
        }
    }

    void PlayAnxiousSound()
    {
        if (anxiousTalk)
            anxiousTalk.Play();
    }

    void PlayAnxiousSound2()
    {
        if (anxiousTalk2)
            anxiousTalk2.Play();
    }

    void TalkToPlayerHappily()
    {
        CancelInvoke("PlayHappySound");
        StopAllCoroutines();

        if (talkBubbleSolved)
            StartCoroutine(ShowGraphic(talkBubbleSolved));

        if (!isWizard && !isUnderBoss)
            Invoke("PlayHappySound", 1f);
        else if (isWizard)
        {
            if (!isComplete)
            {
                Invoke("PlayHappySound", 3.5f);
            }
            else
            {
                Invoke("PlayHappySound", 0.5f);
                animatorTrue.SetTrigger("TriggerTalk");
            }
        }
        else if (isUnderBoss)
        {
            Invoke("PlayHappySound", 1f);
            currentAnimator.SetTrigger("SadTalk");
            Invoke("PlayHappySound", 3f);
        }
    }

    void PlayHappySound()
    {
        if (happyTalk)
            happyTalk.Play();
    }

    private IEnumerator ShowGraphic(GameObject graphicObject)
    {
        graphicObject.SetActive(true);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            graphicObject.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(graphicDisplayTime);

        float ti = 0;

        while(ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            graphicObject.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        graphicObject.SetActive(false);
    }

    void SetVictory()
    {
        //characterStage = CharacterStages.IsSolved;
        transformedCharacter.SetActive(false);
        trueCharacter.SetActive(true);
        currentAnimator = animatorTrue;

        GameObject effect = null;

        if (!isUnderBoss)
        {
            effect = Instantiate(transformEffect, transform.position + Vector3.up, Quaternion.identity);

            if (transformEffectSound)
                transformEffectSound.Play();

            currentAnimator.SetBool("isVictory", true);
        }
        else
        {
            animatorTransformed.SetBool("hasLostUnderboss", true);
            animatorTrue.SetBool("hasLostUnderboss", true);
            currentAnimator.SetTrigger("SadTalk");
        }

        if(!muteCharacter)
            celebrate.Play();

        if(isWizard)
        {
            //spinCharacter = StartCoroutine(Spin(1f, 477.5f, false, false, trueCharacter.transform));
            jumpCharacter = StartCoroutine(Jump(1f, 1, trueCharacter.transform, 0));
            Invoke("JumpAgain", 1.1f);
        }

        Invoke("ReturnToNewIdle", 3f);
        print("Was set to Victory");
    }

    void JumpAgain()
    {
        if (jumpCharacter != null)
            StopCoroutine(jumpCharacter);

        jumpCharacter = StartCoroutine(Jump(1f, 0.5f, trueCharacter.transform, 0));
    }

    void ReturnToNewIdle()
    {
        if (!isUnderBoss)
        {
            currentAnimator.SetBool("isVictory", false);
            currentAnimator.SetBool("isHappyIdle", true);
        }
        else
        {
            animatorTransformed.SetBool("hasLostUnderboss", true);
            animatorTrue.SetBool("hasLostUnderboss", true);
        }

        TalkToPlayerHappily();
        isComplete = true;        
    }
    /*
    void StartWaveAnim()
    {
        if (characterStage == CharacterStages.HasNotMetPlayer)
        {
            currentAnimator.SetBool("isWaving", true);
            if(!muteCharacter)
                waveAudio.Play();
        }

        var randTime = Random.Range(2f, 4f);
        Invoke("StopWaveAnim", randTime);
        
    }
    void StopWaveAnim()
    {
        currentAnimator.SetBool("isWaving", false);

        if (characterStage == CharacterStages.HasNotMetPlayer)
        {
            var randTime = Random.Range(7f, 15f);
            Invoke("StartWaveAnim", randTime);
        }
    }
    */
    void ReferencePlayer(Transform playerTransform)
    {
        player = playerTransform.GetComponent<PlayerController>();
    }

    private IEnumerator ScaleTransformed()
    {
        float t = 0;
        Vector3 startScale = transformedCharacter.transform.localScale;

        while(t < 1)
        {
            t += Time.deltaTime / victoryDelay;

            if (transformedCharacter)
                transformedCharacter.transform.localScale = Vector3.Lerp(startScale, startScale + Vector3.one, t);

            yield return new WaitForEndOfFrame();
        }
    }

    private void OnMouseDown()
    {
        CheckActiveEvents();
    }

    void CheckActiveEvents()
    {
        if (!isComplete)
        {
            if (meetCharacterEvent && !victoryEvent)
            {
                if (!meetCharacterEvent.gameObject.activeSelf)
                {
                    if (meetCharacterTrigger)
                    {
                        if (!meetCharacterTrigger.gameObject.activeSelf)
                            CheckTalkState();
                    }
                    else
                    {
                        CheckTalkState();
                    }
                }
            }
            else if (victoryEvent && !meetCharacterEvent && !isCapturedCharacter)
            {
                if (!victoryEvent.gameObject.activeSelf)
                {
                    if (victoryTrigger)
                    {
                        if (!victoryTrigger.gameObject.activeSelf)
                            CheckTalkState();
                    }
                    else
                    {
                        CheckTalkState();
                    }
                }
            }
            else if (meetCharacterEvent && victoryEvent)
            {
                if (!victoryEvent.gameObject.activeSelf && !meetCharacterEvent.gameObject.activeSelf)
                {
                    if (meetCharacterTrigger && !victoryTrigger)
                    {
                        if (!meetCharacterTrigger.gameObject.activeSelf)
                            CheckTalkState();
                    }
                    else if (victoryTrigger && !meetCharacterTrigger)
                    {
                        if (!victoryTrigger.gameObject.activeSelf)
                            CheckTalkState();
                    }
                    else if (victoryTrigger && meetCharacterTrigger)
                    {
                        if (!victoryTrigger.gameObject.activeSelf && !meetCharacterTrigger.gameObject.activeSelf)
                            CheckTalkState();
                    }
                    else
                    {
                        CheckTalkState();
                    }
                }
            }
            else
            {
                CheckTalkState();
            }
        }
        else
        {
            CheckTalkState();
        }
    }

    void CheckTalkState()
    {
        if (!isTransformedHumanoid)
        {
            if (!isComplete)
            {
                TalkToPlayerAnxiously();
            }
            else
            {
                if(!isUnderBoss)
                    animatorTransformed.SetTrigger("HappyTalk");

                TalkToPlayerHappily();
            }
        }
        else
        {
            if (!isComplete) { }
            //TalkToPlayerAnxiously();
            else
            {
                TalkToPlayerHappily();
            }
        }
    }

    private IEnumerator Spin(float spinTime, float angles, bool startFromCurrent, bool returnToCurrent, Transform obj)
    {
        float t = 0;

        float startAngle = 0;
        float endAngle = angles;

        if (startFromCurrent)
            startAngle = obj.transform.localRotation.y;
            

        while(t < 1)
        {
            t += Time.deltaTime / spinTime;
            var h = LerpUtils.SmootherStep(t);
            float spin = 0;

            if (!returnToCurrent)
                spin = Mathf.Lerp(0, 1, h);
            else
                spin = Mathf.Sin(h * Mathf.PI);

            spin *= (angles + startAngle);

            obj.transform.rotation = Quaternion.AngleAxis(spin, Vector3.up);

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator Jump(float jumpTime, float jumpHeight, Transform obj, float delay)
    {
        yield return new WaitForSeconds(delay);

        float t = 0;
        var startPos = obj.transform.localPosition;

        while(t < 1)
        {
            t += Time.deltaTime / jumpTime;
            var h = LerpUtils.SmootherStep(t);
            var jump = Mathf.Sin(h * Mathf.PI);
            jump *= jumpHeight;
            obj.transform.localPosition = startPos + Vector3.up * jump;
            yield return new WaitForEndOfFrame();
        }
    }

    void CheckPlayerCharacter(CharacterSelector.PlayerCharacter passedCharacter)
    {
        if(passedCharacter == character)
        {
            if (isComplete && trueCharacter.gameObject.activeSelf)
                trueCharacter.gameObject.SetActive(false);
        }

        if (passedCharacter != character && showCharacter)
        {
            if (isComplete && !trueCharacter.gameObject.activeSelf)
                trueCharacter.gameObject.SetActive(true);
        }
    }

    //---------------------------------------------------------ICHARACTER-----------------------------------------------

    public void UnlockCharacter(CharacterSelector.PlayerCharacter character)
    {
        Debug.Log("Unlocking character " + character);

        var characterIndex = gameManager.characterSelector.GetCharacterIndex(character);

        if (!gameManager.playerStats.unlockedCharacters.Contains(characterIndex))
        {
            gameManager.playerStats.unlockedCharacters.Add(characterIndex);
            gameManager.playerStats.SaveGame();

            if (OnAddCharacter != null)
                OnAddCharacter(this);
        }
    }
}
