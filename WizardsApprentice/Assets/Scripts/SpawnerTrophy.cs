﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerTrophy : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public GameObject birthEffect;
    public float waitForNewSpawn = 10f;
    public Transform parent;
    public bool spawnAtStart;
    public AudioSource spawnerBirthSound;
    private GameObject currentObject;
    private float timeStamp;
    private bool timeSet = false;

    private void Start()
    {
        if (spawnAtStart)
        {
            SpawnObject();
        }
    }

    private void Update()
    {
        if (!currentObject && !timeSet)
        {
            timeStamp = Time.time;
            timeSet = true;
        }
        if (!currentObject && timeSet)
        {
            if (waitForNewSpawn <= Time.time - timeStamp)
            {
                SpawnObject();
            }
        }
    }

    void SpawnObject()
    {
        if (spawnerBirthSound)
        {
            spawnerBirthSound.Play();
        }

        currentObject = Instantiate(prefabToSpawn);

        if (parent)
        {
            currentObject.transform.parent = parent;
        }
        if (birthEffect)
        {
            Instantiate(birthEffect, transform);
        }

        currentObject.transform.position = transform.position;

        timeSet = false;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
