﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerExitTag : MonoBehaviour
{
    public string tag;
    public bool isActivated = true;
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == (tag))
        {
            isActivated = false;
        }
    }
}
