﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathFunctions
{
    public static float AdditionOfTwo(float numberA, float numberB)
    {
        return numberA + numberB;
    }

    public static float SubtractionOfTwo(float numberA, float numberB)
    {
        return numberA - numberB;
    }

    public static float AdditionOfThree(float numberA, float numberB, float numberC)
    {
        return numberA + numberB + numberC;
    }

    public static float SubtractionOfThree(float numberA, float numberB, float numberC)
    {
        return numberA - numberB - numberC;
    }

    public static float UnknownAddTwo(float numberA, float answerB)
    {
        return answerB - numberA;
    }

    public static float UnknownSubtractTwo(float numberA, float answerB)
    {
        return answerB + numberA;
    }
}
