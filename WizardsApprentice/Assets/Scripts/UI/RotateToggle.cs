﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateToggle : MonoBehaviour
{
    public Toggle thisToggle;

    public void CheckRotateState()
    {
        if (thisToggle.isOn)
            thisToggle.interactable = false;
        else
            thisToggle.interactable = true;
    }
}
