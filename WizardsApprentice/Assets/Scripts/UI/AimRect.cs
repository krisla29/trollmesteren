﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AimRect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public bool isPressed;

    void Start()
    {
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPressed = false;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        isPressed = true;
    }
}
