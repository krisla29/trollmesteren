﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyPanelDemo : MonoBehaviour
{
    public GameManager gameManager;
    public Canvas spriterCanvas;
    public Canvas uiCanvas;
    public CanvasGroup graphicEasy;
    public CanvasGroup graphicNormal;
    public CanvasGroup graphicHard;
    public DifficultyPanel difficultyPanelScript;

    public float holdTime = 4f;
    
    private IEnumerator DemoSequence()
    {
        graphicEasy.alpha = 0;
        graphicNormal.alpha = 0;
        graphicHard.alpha = 0;

        yield return new WaitForSecondsRealtime(1f);

        float t = 0;
        float a = 2f;

        while(t < 1)
        {
            t += Time.unscaledDeltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            graphicEasy.alpha = Mathf.Lerp(0, 1, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(holdTime);

        float t2 = 0;
        float a2 = 2f;

        while (t2 < 1)
        {
            t2 += Time.unscaledDeltaTime / a2;
            var h = LerpUtils.SmootherStep(t2);
            graphicEasy.alpha = Mathf.Lerp(1, 0, h);
            graphicNormal.alpha = Mathf.Lerp(0, 1, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(holdTime);

        float t3 = 0;
        float a3 = 2f;

        while (t3 < 1)
        {
            t3 += Time.unscaledDeltaTime / a3;
            var h = LerpUtils.SmootherStep(t3);
            graphicNormal.alpha = Mathf.Lerp(1, 0, h);
            graphicHard.alpha = Mathf.Lerp(0, 1, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSecondsRealtime(holdTime);

        float t4 = 0;
        float a4 = 2f;

        while (t4 < 1)
        {
            t4 += Time.unscaledDeltaTime / a4;
            var h = LerpUtils.SmootherStep(t4);
            graphicHard.alpha = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }

        //yield return new WaitForSecondsRealtime(holdTime);

        StartCoroutine(DemoSequence());
    }

    private void OnEnable()
    {
        GameManager.OnSwitchScene += CheckScene;
        StartCoroutine(DemoSequence());
    }

    private void OnDisable()
    {
        GameManager.OnSwitchScene -= CheckScene;
        StopAllCoroutines();
    }

    private void Update()
    {
        if (!difficultyPanelScript.difficultyPanel.gameObject.activeSelf)
            transform.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void CheckScene(SceneController passedSceneController)
    {
        if(passedSceneController != null)
        {
            spriterCanvas.worldCamera = gameManager.currentScene.sceneCamera;
            spriterCanvas.planeDistance = gameManager.currentScene.sceneCanvas.planeDistance;
        }
    }

    public void EndDemo()
    {
        if (transform.gameObject.activeSelf)
        {
            StopAllCoroutines();
            difficultyPanelScript.hasOpened = true;
            gameManager.playerStats.hasSeenDifficultyPanelDemo = true;
            gameManager.playerStats.SaveGame();
            transform.gameObject.SetActive(false);
        }
    }
}
