﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClampRectSize : MonoBehaviour
{
    public RectTransform rectTransform;
    [Range(0, 100)] public float percentageToRemoveWidth = 10;
    [Range(0, 100)] public float percentageToRemoveHeight = 10;

    void Update()
    {
        //var sDelta = rectTransform.sizeDelta;
        var rectWidth = rectTransform.sizeDelta.x;

        if (percentageToRemoveWidth > 0)
        {           
            rectWidth = Screen.width - ((Screen.width / 100) * percentageToRemoveWidth);
        }
        else
        {
            rectWidth = Screen.width;
        }

        var rectHeight = rectTransform.sizeDelta.y;

        if (percentageToRemoveHeight > 0)
        {
            rectHeight = Screen.height - ((Screen.height / 100) * percentageToRemoveHeight);
        }
        else
        {
            rectHeight = Screen.height;
        }

        rectTransform.sizeDelta = new Vector2(rectWidth, rectHeight);
    }
}
