﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_Map_Demo : MonoBehaviour
{
    public GameManager gameManager;
    //public Animator demoAnim;
    //public Transform masterObject;
    public MapObject mapObjectScript;
    public MapLock mapLockScript;
    public Transform mapLocations;
    public CanvasGroup uiCanvasGrp;
    public CanvasGroup spriterCanvasGrp;
    public SpriterController spriterController;
    public RectTransform spriterRect;
    public RectTransform uiPointer;
    public Image uiPointerImage;
    public RectTransform uiPointerImageRect;
    public Interactable travelPoint;
    //public SpriteRenderer travelPointSprite
    public Transform travelPointT;
    //public SpriteRenderer arrowPointerImage;
    //public Transform navigationPointer;

    //public RectTransform rotSliderRect;
    //public RectTransform rotToggleRect;
    public GraphicRaycaster mapSceneRaycaster;
    public WorldMapCamera worldMapCamera;
    public Transform startLoc;
    public WorldMapCanvas worldMapCanvas;
    public RectTransform zoomSliderRect;

    public AudioSource soundSuccess;
    public AudioSource soundDemoComplete;

    private Coroutine fadeOut;
    private Coroutine fadeIn;
    private Coroutine fadeOutFast;
    private Coroutine fadeInFast;

    private Coroutine clickTutorial;
    private Coroutine panTutorial;
    private Coroutine travelTutorial;
    private Coroutine zoomTutorial;
    private Coroutine finishTutorial;


    public float waitTime = 2;
    public float animPause = 4f;
    public float longFade = 2;
    public float fastFade = 0.5f;
    public float soundDelay = 0.5f;

    private Vector2 pointerSize;
    public Vector3 spriterScale = Vector3.one * 150f;

    //InteractionValues
    private float lastZoomValue;
    private Vector3 lastCamPos;

    private bool hasPanned;
    private bool hasZoomed;
    private bool hasClickedTravelPoint;

    private bool isAnimating;

    private void Awake()
    {
        pointerSize = uiPointerImageRect.sizeDelta;
    }

    private void CheckStatus()
    {
        if (!gameManager.playerStats.hasUnlockedMap)
        {
            LockPanning();
            LockZoomSlider();            
            spriterController.transform.localScale = Vector3.zero;
            StartDemo();
        }
        else
        {
            transform.gameObject.SetActive(false);
        }
    }
    
    void StartDemo()
    {
        gameManager.hudCanvas.trophyButton.gameObject.SetActive(false);
        gameManager.hudCanvas.worldMapButton.gameObject.SetActive(false);
        mapLocations.gameObject.SetActive(false);
        spriterCanvasGrp.gameObject.SetActive(true);
        uiPointer.gameObject.SetActive(true);
        spriterRect.anchoredPosition = new Vector2(Screen.width * 0.2f, Screen.height * 0.2f);
        InitializePanDemo();
    }

    private void OnEnable()
    {
        CheckStatus();
        InputManager.OnInputMouseDown += SetCurrentInteraction;
        InputManager.OnInputMouseUp += CheckLastInteraction;
        Interactable.OnInteractablePressed += TravelPointClick;
    }

    private void OnDisable()
    {
        InputManager.OnInputMouseDown -= SetCurrentInteraction;
        InputManager.OnInputMouseUp -= CheckLastInteraction;
        Interactable.OnInteractablePressed -= TravelPointClick;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
    
    void LockZoomSlider()
    {
        worldMapCamera.zoomSlider.interactable = false;
    }

    void UnlockZoomSlider()
    {
        worldMapCamera.zoomSlider.interactable = true;
    }

    void LockPanning()
    {
        worldMapCamera.lockPan = true;
    }

    void UnlockPanning()
    {
        worldMapCamera.lockPan = false;
    }

    void LockAll()
    {
        worldMapCamera.blockInput = true;
        LockPanning();
        LockZoomSlider();
    }

    void UnlockAll()
    {
        worldMapCamera.blockInput = false;
        UnlockPanning();
        UnlockZoomSlider();
    }
    
    public void TravelPointClick(Transform passedInteractable)
    {
        if (passedInteractable == travelPoint.transform)
        {
            if (hasZoomed && !hasClickedTravelPoint)
            {
                StartCoroutine(DemoTravelFinish());
                hasClickedTravelPoint = true;
            }
        }
    }

    void SetCurrentInteraction()
    {
        lastZoomValue = worldMapCamera.zoomSlider.value;
        lastCamPos = worldMapCamera.transform.position;

        if(isAnimating)
            FadeFastOut();
    }

    void CheckLastInteraction()
    {        
        if (!hasPanned)
        {
            if (Vector3.Distance(worldMapCamera.transform.position, worldMapCamera.lastCamPos) > 1f)
                StartCoroutine(DemoPanFinish());
        }
        
        if (hasPanned && !hasZoomed)
        {
            if (Mathf.Abs(worldMapCamera.zoomSlider.value - lastZoomValue) > 0.1f)
                StartCoroutine(DemoZoomFinish());
        }
        
        if (isAnimating)
            FadeFastIn();
    }
   
    void InitializePanDemo()
    {
        HideCanvasGrps();
        UnlockPanning();
        panTutorial = StartCoroutine(PanTutorial());
    }

    private IEnumerator DemoPanFinish()
    {
        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;
        hasPanned = true;
        //LockRotationSlider();

        if (panTutorial != null)
            StopCoroutine(panTutorial);

        FadeOutLong();

        yield return new WaitForSeconds(waitTime);

        InitializeZoomDemo();
    }
    
    void InitializeZoomDemo()
    {
        HideCanvasGrps();
        UnlockZoomSlider();
        zoomTutorial = StartCoroutine(ZoomTutorial());
    }

    private IEnumerator DemoZoomFinish()
    {
        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;
        hasZoomed = true;

        if (zoomTutorial != null)
            StopCoroutine(zoomTutorial);       

        yield return new WaitForSeconds(0.5f);

        LockZoomSlider();
        LockPanning();

        float t = 0;
        float lerpTime = 1.5f;
        float curZoom = worldMapCamera.zoomSlider.value;
        Vector3 curCamPos = worldMapCamera.transform.position;
        travelPointT.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            var h = LerpUtils.SmootherStep(t);
            worldMapCamera.zoomSlider.value = Mathf.Lerp(curZoom, 1, h);
            worldMapCamera.transform.position = Vector3.Lerp(curCamPos, new Vector3(travelPointT.position.x, curCamPos.y, travelPointT.transform.position.z), h);
            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        yield return new WaitForSeconds(waitTime);

        InitializeTravelDemo();
    }

    void InitializeTravelDemo()
    {
        HideCanvasGrps();        
        travelTutorial = StartCoroutine(TravelTutorial());
    }

    private IEnumerator DemoTravelFinish()
    {
        FadeOutLong();
        uiPointer.gameObject.SetActive(false);
        travelPointT.gameObject.SetActive(false);

        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;

        LockAll();
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);        

        if (travelTutorial != null)
            StopCoroutine(travelTutorial);

        yield return new WaitForSeconds(longFade);

        FadeInLong();

        spriterRect.anchoredPosition3D = new Vector3(0, 0, -150f);

        yield return new WaitForSeconds(waitTime);

        float curZoom = worldMapCamera.zoomSlider.value;
        Vector3 camPos = worldMapCamera.transform.position;
        float t = 0;
        float time = 1f;

        while(t < 1)
        {
            t += Time.deltaTime / time;
            var h = LerpUtils.SmootherStep(t);
            worldMapCamera.zoomSlider.value = Mathf.Lerp(curZoom, 1, h);
            //worldMapCamera.transform.position = Vector3.Lerp(camPos, worldMapCamera.startPos, h);
            yield return new WaitForEndOfFrame();
        }       

        yield return new WaitForSeconds(waitTime);

        if (soundDemoComplete)
            soundDemoComplete.Play();

        FadeOutLong();

        yield return new WaitForSeconds(longFade);

        FinishDemo();
    }

    public void FinishDemo()
    {       
        ResetCanvasGrps();
        UnlockAll();
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);
        
        if (spriterCanvasGrp)
            spriterCanvasGrp.gameObject.SetActive(false);

        if (worldMapCamera)
        {
            worldMapCamera.zoomSlider.gameObject.SetActive(true);
            worldMapCamera.blockInput = false;
            worldMapCamera.transform.position = worldMapCamera.startPos;
        }

        if (mapObjectScript)
        {
            mapObjectScript.sceneCollider.transform.localScale = mapObjectScript.sceneColliderStartScale;
            mapObjectScript.map3dMesh.transform.parent.gameObject.SetActive(false);
            mapObjectScript.playerTrigger.gameObject.SetActive(false);
        }

        gameManager.isMapDemo = false;
        mapLockScript.Unlock();
        mapLocations.gameObject.SetActive(true);
        gameManager.hudCanvas.ResetMapBtnPulser();
        gameManager.playerStats.hasUnlockedMap = true;
        gameManager.playerStats.SaveGame();
        worldMapCanvas.TravelToLocation(startLoc, null);
        worldMapCamera.transform.position = worldMapCamera.startPos;
        transform.gameObject.SetActive(false);
    }

    private IEnumerator FadeOut()
    {
        float t = 0;
        float fadeTime = longFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 0, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 0, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        //spriterController.gameObject.SetActive(false);
    }

    private IEnumerator FadeIn()
    {
        float t = 0;
        float fadeTime = longFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;
        //spriterController.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 1, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 1, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, spriterScale, h);
            yield return new WaitForEndOfFrame();
        }

        spriterController.animator.SetTrigger("TriggerIdleTalk");
    }

    void ResetCanvasGrps()
    {
        uiCanvasGrp.alpha = 1;
        spriterCanvasGrp.alpha = 1;
    }

    void HideCanvasGrps()
    {
        uiCanvasGrp.alpha = 0;
        spriterCanvasGrp.alpha = 0;
    }

    private IEnumerator FadeOutFast()
    {
        float t = 0;
        float fadeTime = fastFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 0, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 0, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        //spriterController.gameObject.SetActive(false);
    }

    private IEnumerator FadeInFast()
    {
        float t = 0;
        float fadeTime = fastFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;
        //spriterController.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 1, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 1, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, spriterScale, h);
            yield return new WaitForEndOfFrame();
        }

        spriterController.animator.SetTrigger("TriggerIdleTalk");
    }

    void FadeInLong()
    {
        StopFades();
        fadeIn = StartCoroutine(FadeIn());
    }

    void FadeOutLong()
    {
        StopFades();
        fadeOut = StartCoroutine(FadeOut());
    }

    void FadeFastIn()
    {
        StopFades();
        fadeInFast = StartCoroutine(FadeInFast());
    }

    void FadeFastOut()
    {
        StopFades();
        fadeOutFast = StartCoroutine(FadeOutFast());
    }

    void StopFades()
    {
        if (fadeInFast != null)
            StopCoroutine(fadeInFast);
        if (fadeOutFast != null)
            StopCoroutine(fadeOutFast);
        if (fadeIn != null)
            StopCoroutine(fadeIn);
        if (fadeOut != null)
            StopCoroutine(fadeOut);
    }    

    private IEnumerator PanTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(uiCanvasGrp.transform);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 180f);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(-Screen.width * 0.15f, 0, -150f);

        float t = 0;
        float a = 6f;
        Vector2 startAnchPos = Vector2.zero;
        Vector2 endAnchPos = Vector2.zero;
        float magn = 0.15f;

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.EaseOut(t);
            var s = Mathf.Sin(h * (Mathf.PI * 2));
            s *= Screen.height * magn;
            uiPointer.anchoredPosition = new Vector2(0, s);

            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 8f;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(h * (Mathf.PI * 2));
            s *= Screen.width * magn;
            uiPointer.anchoredPosition = new Vector2(s, 0);

            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        panTutorial = StartCoroutine(PanTutorial());
    }

    private IEnumerator TravelTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(uiCanvasGrp.transform);        
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 180);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(Screen.width * 0.15f, Screen.height * 0.1f, -150f);
        Vector2 startPos = worldMapCamera.thisCam.WorldToViewportPoint(travelPointT.transform.position);

        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(startPos.x, startPos.y + Screen.height * 0.4f);
        Vector2 endAnchPos = startPos;

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 0.75f;
        Vector2 startScale = uiPointerImageRect.sizeDelta;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI * 1));
            uiPointer.anchoredPosition = new Vector2(startPos.x, startPos.y + (-Screen.height * 0.03f) * s);
            uiPointerImageRect.sizeDelta = new Vector2(startScale.x + (s * (startScale.x * 0.15f)), startScale.y - (s * (startScale.y * 0.15f)));
            yield return new WaitForEndOfFrame();
        }

        float t3 = 0;
        float a3 = 1.5f;
        Vector2 startAnchPos2 = endAnchPos;
        Vector2 endAnchPos2 = startAnchPos;

        while (t3 < 1)
        {
            t3 += Time.deltaTime / a3;
            var h = LerpUtils.SmootherStep(t3);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(new Vector2(startAnchPos2.x * 0.5f, startAnchPos2.y * 0.5f), new Vector2(endAnchPos2.x * 0.5f, endAnchPos2.y * 0.5f), h);
            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        travelTutorial = StartCoroutine(TravelTutorial());
    }

    private IEnumerator ZoomTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(zoomSliderRect);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 45f);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(-Screen.width * 0.15f, Screen.height * 0.1f, -150f);


        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(Screen.width * 0.3f, 0);
        //Vector2 endAnchPos = new Vector2(0, Screen.height * 0.25f);
        Vector2 endAnchPos = new Vector2(0, zoomSliderRect.rect.yMax);

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 3f;
        float e = zoomSliderRect.rect.yMin * 0.5f;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            var h = LerpUtils.SmootherStep(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI));
            s *= -Screen.height * 0.4f;
            //uiPointer.anchoredPosition = new Vector2(0,endAnchPos.y + s);
            uiPointer.anchoredPosition = Vector2.Lerp(endAnchPos, new Vector2(endAnchPos.x, e), h);

            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        zoomTutorial = StartCoroutine(ZoomTutorial());
    }
    /*
    private IEnumerator FinishTutorial()
    {
        isAnimating = true;
        uiPointer.gameObject.SetActive(false);
        uiPointer.SetParent(uiCanvasGrp.transform);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 0);

        FadeFastIn();

        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(Screen.width * 0.3f, 0);
        Vector2 endAnchPos = new Vector2(0, Screen.height * 0.35f);

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.EaseOut(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 3f;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI));
            s *= -Screen.height * 0.4f;
            uiPointer.anchoredPosition = new Vector2(0, endAnchPos.y + s);

            yield return new WaitForEndOfFrame();
        }

        FadeFastOut();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        finishTutorial = StartCoroutine(FinishTutorial());
    }
    */
    /*
    float t = 0;
    float a = 1f;

    while(t < 1)
    {
        t += Time.deltaTime / a;
        var h = LerpUtils.SmootherStep(t);
        var s = Mathf.Sin(t * (Mathf.PI * 2));
        yield return new WaitForEndOfFrame();
    }
    */

}
