﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyPanel : MonoBehaviour
{
    public GameManager gameManager;
    public AudioSource openPanelSound;
    public AudioSource closePanelSound;
    public BgFaders bg;
    //public EasyScalePulse easyScalePulseScript;
    
    public EasyScalePulse easyButton;
    public EasyScalePulse normalButton;
    public EasyScalePulse hardButton;

    public RectTransform difficultyPanel;
    public RectTransform chooseLevelPanel;

    public List<Button> levelsEasyButtons = new List<Button>();
    public List<Button> levelsNormalButtons = new List<Button>();
    public List<Button> levelsHardButtons = new List<Button>();

    public PlayerStats.Difficulty suggestedDifficulty = PlayerStats.Difficulty.Normal;
    public delegate void DifficultyPanelEvent(PlayerStats.Difficulty requestedDifficulty); //Sends to game scripts, PlayerStats && GameProgressionDisplay
    public static event DifficultyPanelEvent OnSetDifficulty;
    public static event DifficultyPanelEvent OnSetDifficultyAndRestartLevel;
    public delegate void DifficultyPanelLevelEvent(PlayerStats.Difficulty requestedDifficulty, int requestedLevel); //Sends to game scripts, PlayerStats && GameProgressionDisplay
    public static event DifficultyPanelLevelEvent OnSetDifficultyAndLevel;

    public bool startNewLevel;
    public int currentLevelCount;
    public bool isCurrentAchievements;
    public bool hasTrophy;
    public bool hasOpened;

    public DifficultyPanelDemo difficultyPanelDemo;
    //public GameEvent tutorialEvent;
    //public PosZToPlaneDist PosZToPlaneDistScript;

    private void Start()
    {
        //hasOpened = gameManager.playerStats.hasSeenDifficultyPanelDemo;
    }

    private void OnEnable()
    {
        OpenPanel();

        PlayerStats.OnDeactivateGame += ClosePanel;
        PlayerStats.OnActivateGame += OpenPanel;
    }

    public void OpenPanel()
    {
        if (!gameManager.isBattle)
        {
            isCurrentAchievements = gameManager.playerStats.isCurrentAchievements;
            hasTrophy = gameManager.playerStats.currentHasCompleted;

            if (!hasTrophy)
                StartCoroutine(OpenDifficultyPanel());
            else
                StartCoroutine(OpenChooserPanel());

            bg.FT_FadeSingle(false, true);
            gameManager.hudCanvas.gameProgressionDisplay.currentDifficultyIcon.interactable = false;
            gameManager.hudCanvas.exitButton.interactable = false;
                       
            //hasOpened = true;
        }
    }

    public void SetStartLevelTrue()
    {
        startNewLevel = true;

        OpenPanel();
    }

    public void SetEasy()
    {
        if (startNewLevel)
        {
            if (OnSetDifficulty != null)
                OnSetDifficultyAndRestartLevel(PlayerStats.Difficulty.Easy);
        }
        else
        {
            if (OnSetDifficulty != null)
                OnSetDifficulty(PlayerStats.Difficulty.Easy);
        }

        ClosePanel();
    }

    public void SetNormal()
    {
        if (startNewLevel)
        {
            if (OnSetDifficulty != null)
                OnSetDifficultyAndRestartLevel(PlayerStats.Difficulty.Normal);
        }
        else
        {
            if (OnSetDifficulty != null)
                OnSetDifficulty(PlayerStats.Difficulty.Normal);
        }

        ClosePanel();
    }

    public void SetHard()
    {
        if (startNewLevel)
        {
            if (OnSetDifficulty != null)
                OnSetDifficultyAndRestartLevel(PlayerStats.Difficulty.Hard);
        }
        else
        {
            if (OnSetDifficulty != null)
                OnSetDifficulty(PlayerStats.Difficulty.Hard);
        }

        ClosePanel();
    }

    private void OnDisable()
    {        
        PlayerStats.OnDeactivateGame -= ClosePanel;
        PlayerStats.OnActivateGame -= OpenPanel;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void ClosePanel()
    {
        transform.localScale = Vector3.zero;
        //difficultyPanel.transform.localScale = Vector3.zero;
        //chooseLevelPanel.transform.localScale = Vector3.zero;
        //easyScalePulseScript.enabled = false;

        easyButton.enabled = false;
        normalButton.enabled = false;
        hardButton.enabled = false;

        easyButton.transform.localScale = Vector3.one;
        normalButton.transform.localScale = Vector3.one;
        hardButton.transform.localScale = Vector3.one;

        if (closePanelSound)
            closePanelSound.Play();

        gameManager.player.isLocked = false;

        if (chooseLevelPanel.gameObject.activeSelf)
            ResetChooserIcons();

        difficultyPanel.gameObject.SetActive(false);
        chooseLevelPanel.gameObject.SetActive(false);

        Time.timeScale = 1;
        StopAllCoroutines();
        transform.localScale = Vector3.zero;
        
        bg.FT_FadeSingle(true, true);
        gameManager.hudCanvas.gameProgressionDisplay.currentDifficultyIcon.interactable = true;
        gameManager.hudCanvas.exitButton.interactable = true;
        startNewLevel = false;                       
    }

    void CheckSuggestedDifficulty()
    {
        if (suggestedDifficulty == PlayerStats.Difficulty.Easy)
        {
            easyButton.enabled = true;
            normalButton.enabled = false;
            hardButton.enabled = false;
            normalButton.transform.localScale = Vector3.one;
            hardButton.transform.localScale = Vector3.one;
        }
        else if (suggestedDifficulty == PlayerStats.Difficulty.Normal)
        {
            normalButton.enabled = true;
            easyButton.enabled = false;
            hardButton.enabled = false;
            easyButton.transform.localScale = Vector3.one;
            hardButton.transform.localScale = Vector3.one;
        }
        else if (suggestedDifficulty == PlayerStats.Difficulty.Hard)
        {
            hardButton.enabled = true;
            easyButton.enabled = false;
            normalButton.enabled = false;
            easyButton.transform.localScale = Vector3.one;
            normalButton.transform.localScale = Vector3.one;
        }
        if (suggestedDifficulty == PlayerStats.Difficulty.None)
        {
            easyButton.enabled = false;
            normalButton.enabled = false;
            hardButton.enabled = false;
            easyButton.transform.localScale = Vector3.one;
            normalButton.transform.localScale = Vector3.one;
            hardButton.transform.localScale = Vector3.one;
        }
    }

    private IEnumerator OpenDifficultyPanel()
    {
        float t = 0;
        float timeToOpenPanel = 1f;
        difficultyPanel.gameObject.SetActive(true);
        chooseLevelPanel.gameObject.SetActive(false);
        
        while (t < 1)
        {
            t += Time.deltaTime / timeToOpenPanel;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
        
        if (openPanelSound)
            openPanelSound.Play();

        //easyScalePulseScript.enabled = true;
        CheckSuggestedDifficulty();

        gameManager.player.isLocked = true;
        Time.timeScale = 0;

        if (!hasOpened)
        {
            if (difficultyPanelDemo)
            {            
                difficultyPanelDemo.gameObject.SetActive(true);
            }
        }
    }

    private IEnumerator OpenChooserPanel()
    {
        
        float t = 0;
        float timeToOpenPanel = 1f;
        chooseLevelPanel.gameObject.SetActive(true);
        difficultyPanel.gameObject.SetActive(false);

        InitializeLevelsMap();
        
        while (t < 1)
        {
            t += Time.deltaTime / timeToOpenPanel;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
        
        if (openPanelSound)
            openPanelSound.Play();

        //easyScalePulseScript.enabled = true;

        gameManager.player.isLocked = true;
        Time.timeScale = 0;
    }

    void InitializeLevelsMap()
    {
        currentLevelCount = gameManager.playerStats.currentMaxLevel + 1;

        for (int i = 0; i < levelsNormalButtons.Count; i++)
        {
            if (i < currentLevelCount)
            {
                levelsEasyButtons[i].gameObject.SetActive(true);
                levelsNormalButtons[i].gameObject.SetActive(true);
                levelsHardButtons[i].gameObject.SetActive(true);
            }
            else
            {
                levelsEasyButtons[i].gameObject.SetActive(false);
                levelsNormalButtons[i].gameObject.SetActive(false);
                levelsHardButtons[i].gameObject.SetActive(false);
            }
        }

        CheckCompletedLevels();
    }

    void CheckCompletedLevels()
    {

        for (int i = 0; i < levelsEasyButtons.Count; i++)
        {
            if(gameManager.playerStats.currentCompletedSets.Contains(i))
            {
                levelsEasyButtons[i].transform.GetChild(0).gameObject.SetActive(true);

                if(isCurrentAchievements)
                {
                    SetAchievementLevel(i, i, levelsEasyButtons);
                }                
            }
            else
            {
                levelsEasyButtons[i].transform.GetChild(0).gameObject.SetActive(false);
            }

            if (isCurrentAchievements)
            {
                for (int e = 0; e < 3; e++)
                {
                    levelsEasyButtons[i].transform.GetChild(e + 1).gameObject.SetActive(true);
                }
            }
            else
            {
                for (int e = 0; e < 3; e++)
                {
                    levelsEasyButtons[i].transform.GetChild(e + 1).gameObject.SetActive(false);
                    //print("Setting star " + (e + 1).ToString() + " false");
                }
            }
        }
        for (int i = 0; i < levelsNormalButtons.Count; i++)
        {
            int index = i + currentLevelCount;

            if (gameManager.playerStats.currentCompletedSets.Contains(index))
            {
                levelsNormalButtons[i].transform.GetChild(0).gameObject.SetActive(true);

                if (isCurrentAchievements)
                {
                    SetAchievementLevel(index, i, levelsNormalButtons);
                }
            }
            else
            {
                levelsNormalButtons[i].transform.GetChild(0).gameObject.SetActive(false);
            }

            if (isCurrentAchievements)
            {
                for (int e = 0; e < 3; e++)
                {
                    levelsNormalButtons[i].transform.GetChild(e + 1).gameObject.SetActive(true);
                }
            }
            else
            {
                for (int e = 0; e < 3; e++)
                {
                    levelsNormalButtons[i].transform.GetChild(e + 1).gameObject.SetActive(false);
                    //print("Setting star " + (e + 1).ToString() + " false");
                }
            }
        }
        for (int i = 0; i < levelsHardButtons.Count; i++)
        {
            int index = i + (currentLevelCount * 2);

            if (gameManager.playerStats.currentCompletedSets.Contains(index))
            {
                levelsHardButtons[i].transform.GetChild(0).gameObject.SetActive(true);

                if (isCurrentAchievements)
                {                
                    SetAchievementLevel(index, i, levelsHardButtons);
                }               
            }
            else
            {
                levelsHardButtons[i].transform.GetChild(0).gameObject.SetActive(false);
            }

            if (isCurrentAchievements)
            {
                for (int e = 0; e < 3; e++)
                {
                    levelsHardButtons[i].transform.GetChild(e + 1).gameObject.SetActive(true);
                }
            }
            else
            {
                for (int e = 0; e < 3; e++)
                {
                    levelsHardButtons[i].transform.GetChild(e + 1).gameObject.SetActive(false);
                    //print("Setting star " + (e + 1).ToString() + " false");
                }
            }
        }        
    }

    public void LevelButtonSelect(Button pushedButton)
    {
        int desiredLevel = 0;
        PlayerStats.Difficulty desiredDifficulty = PlayerStats.Difficulty.Easy;

        if(levelsEasyButtons.Contains(pushedButton))
        {
            desiredLevel = levelsEasyButtons.IndexOf(pushedButton);
            desiredDifficulty = PlayerStats.Difficulty.Easy;
        }
        else if(levelsNormalButtons.Contains(pushedButton))
        {
            desiredLevel = levelsNormalButtons.IndexOf(pushedButton) + currentLevelCount;
            desiredDifficulty = PlayerStats.Difficulty.Normal;
        }
        else if (levelsHardButtons.Contains(pushedButton))
        {
            desiredLevel = levelsHardButtons.IndexOf(pushedButton) + (currentLevelCount * 2);
            desiredDifficulty = PlayerStats.Difficulty.Hard;
        }

        if (OnSetDifficultyAndLevel != null)
            OnSetDifficultyAndLevel(desiredDifficulty, desiredLevel);

        ClosePanel();
    }   
    
    void SetAchievementLevel(int index, int buttonIndex, List<Button> buttonList)
    {
        if (gameManager.playerStats.currentAchievements[index] == 1)
        {
            buttonList[buttonIndex].transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
        }
        else if (gameManager.playerStats.currentAchievements[index] == 2)
        {
            buttonList[buttonIndex].transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
            buttonList[buttonIndex].transform.GetChild(2).GetChild(0).gameObject.SetActive(true);
        }
        else if (gameManager.playerStats.currentAchievements[index] == 3)
        {
            buttonList[buttonIndex].transform.GetChild(1).GetChild(0).gameObject.SetActive(true);
            buttonList[buttonIndex].transform.GetChild(2).GetChild(0).gameObject.SetActive(true);
            buttonList[buttonIndex].transform.GetChild(3).GetChild(0).gameObject.SetActive(true);
        }
    }

    void ResetChooserIcons()
    {
        for (int i = 0; i < levelsEasyButtons.Count; i++)
        {
            levelsEasyButtons[i].transform.GetChild(0).gameObject.SetActive(false);

            for (int e = 0; e < 3; e++)
            {
                levelsEasyButtons[i].transform.GetChild(e + 1).GetChild(0).gameObject.SetActive(false);
            }
        }
                
        for (int i = 0; i < levelsNormalButtons.Count; i++)
        {
            levelsNormalButtons[i].transform.GetChild(0).gameObject.SetActive(false);

            for (int e = 0; e < 3; e++)
            {
                levelsNormalButtons[i].transform.GetChild(e + 1).GetChild(0).gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < levelsHardButtons.Count; i++)
        {
            levelsHardButtons[i].transform.GetChild(0).gameObject.SetActive(false);

            for (int e = 0; e < 3; e++)
            {
                levelsHardButtons[i].transform.GetChild(e + 1).GetChild(0).gameObject.SetActive(false);
            }
        }
    }
    /*
    private IEnumerator FadeInBg()
    {
        yield return new WaitForSecondsRealtime(1);

        float t = 0;

        while(t < 1)
        {
            t += Time.fixedUnscaledDeltaTime;
            bg.alpha = Mathf.Lerp(0, 1, t);
            yield return new WaitForEndOfFrame();
        }
    }
    */
}
