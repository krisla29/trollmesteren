﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public class CheatListPanel : MonoBehaviour
{
    public PlayerStats playerStats;
    public HudCanvas hudCanvas;

    public Slider gemSlider;
    public TextMeshProUGUI gemText;
    public Slider difficultySlider;
    public TextMeshProUGUI difficultyText;
    public Toggle orderGameComplete;
    public Slider orderGameSetSlider;
    public TextMeshProUGUI orderGameSetText;
    public Toggle tileGameComplete;
    public Slider tileGameSetSlider;
    public TextMeshProUGUI tileGameSetText;
    public Toggle shooterGameComplete;
    public Slider shooterGameSetSlider;
    public TextMeshProUGUI shooterGameSetText;
    public Toggle battlePlayedDemoDesc;
    public Toggle battlePlayedDemoAsc;
    public Toggle battleSortComplete;
    public Slider battleSortEnemyHealthSlider;
    public TextMeshProUGUI battleSortEnemyHealthText;
    public Toggle mazeGameComplete;
    public Slider mazeGameSetSlider;
    public TextMeshProUGUI mazeGameSetText;
    public Toggle starMakerGameComplete;
    public Slider starMakerGameSetSlider;
    public TextMeshProUGUI starMakerGameSetText;
    public Toggle areaVolumeGameComplete;
    public Slider areaVolumeGameSetSlider;
    public TextMeshProUGUI areaVolumeGameSetText;
    public Toggle figuresGameComplete;
    public Slider figuresGameSetSlider;
    public TextMeshProUGUI figuresGameSetText;
    public Toggle clockGameComplete;
    public Slider clockGameTaskSlider;
    public TextMeshProUGUI clockGameSetText;
    public Toggle racingGameComplete;
    public Toggle bossGameComplete;
    public Slider bossGameSetSlider;
    public TextMeshProUGUI bossGameSetText;
    private string path;

    public delegate void CheatEvent(); //Sends to PlayerStats
    public static event CheatEvent OnChangePuzzleCompletion;

    private void Awake()
    {
        path = GameProperties.saveDirectory;
    }

    public void ChangePlayerGems()
    {
        playerStats.gems = (int)gemSlider.value;
        //playerStats.gems = playerStats.gems;
        gemText.text = playerStats.gems.ToString();
        hudCanvas.gemsText.text = gemText.text;
    }

    public void SetPlayerDifficulty()
    {
        switch(difficultySlider.value)
        {
            case (1):
                playerStats.difficulty = PlayerStats.Difficulty.Easy;
                break;
            case (2):
                playerStats.difficulty = PlayerStats.Difficulty.Normal;
                break;
            case (3):
                playerStats.difficulty = PlayerStats.Difficulty.Hard;
                break;
            default:
                break;
        }

        difficultyText.text = difficultySlider.value.ToString();
    }

    public void ToggleOrderGameComplete()
    {
        if(orderGameComplete.isOn)
        {
            if(!CheckTrophys(PlayerStats.Trophy.OrderGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.OrderGame);
            }

            playerStats.hasWonOrderGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.OrderGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.OrderGame);
            }

            playerStats.hasWonOrderGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeOrderGameSet()
    {
        playerStats.OrderGameSetCount = ((int)orderGameSetSlider.value);
        orderGameSetText.text = (playerStats.OrderGameSetCount + 1).ToString();
    }

    public void ToggleShooterGameComplete()
    {
        if (shooterGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.ShooterGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.ShooterGame);
            }

            playerStats.hasWonShooterGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.ShooterGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.ShooterGame);
            }

            playerStats.hasWonShooterGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeShooterGameSet()
    {
        playerStats.ShooterSetCount = ((int)shooterGameSetSlider.value);
        shooterGameSetText.text = (playerStats.ShooterSetCount + 1).ToString();
    }

    public void ToggleTileGameComplete()
    {
        if (tileGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.TileGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.TileGame);
            }

            playerStats.hasWonTileGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.TileGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.TileGame);
            }

            playerStats.hasWonTileGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeTileGameSet()
    {
        playerStats.TileGameSetCount = ((int)tileGameSetSlider.value);
        tileGameSetText.text = (playerStats.TileGameSetCount + 1).ToString();
    }

    public void ToggleBattleSortComplete()
    {
        if (battleSortComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.BattleSort))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.BattleSort);
            }

            playerStats.hasWonBattleSort = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.BattleSort))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.BattleSort);
            }

            playerStats.hasWonBattleSort = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ToggleBattleDemoDescPlayed()
    {
        if(battlePlayedDemoDesc.isOn)
        {
            playerStats.hasPlayedBattleDemoDesc = true;
        }
        else
        {
            playerStats.hasPlayedBattleDemoDesc = false;
        }
    }

    public void ToggleBattleDemoAscPlayed()
    {
        if (battlePlayedDemoAsc.isOn)
        {
            playerStats.hasPlayedBattleDemoDesc = true;
        }
        else
        {
            playerStats.hasPlayedBattleDemoDesc = false;
        }
    }

    public void ChangeBattleSortEnemyHealth()
    {
        playerStats.battleSortEnemyHealth = battleSortEnemyHealthSlider.value;
        battleSortEnemyHealthText.text = ((int)(playerStats.battleSortEnemyHealth * 50)).ToString();
    }

    public void ToggleMazeGameComplete()
    {
        if (mazeGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.MazeGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.MazeGame);
            }

            playerStats.hasWonMazeGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.MazeGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.MazeGame);
            }

            playerStats.hasWonMazeGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeMazeGameSet()
    {
        playerStats.MazeGameSetCount = ((int)mazeGameSetSlider.value);
        mazeGameSetText.text = (playerStats.MazeGameSetCount + 1).ToString();
    }

    public void ToggleStarMakerGameComplete()
    {
        if (starMakerGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.StarMakerGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.StarMakerGame);
            }

            playerStats.hasWonStarMakerGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.StarMakerGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.StarMakerGame);
            }

            playerStats.hasWonStarMakerGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeStarMakerGameSet()
    {
        playerStats.StarMakerGameSetCount = ((int)starMakerGameSetSlider.value);
        starMakerGameSetText.text = (playerStats.StarMakerGameSetCount + 1).ToString();
    }

    public void ToggleAreaVolumeGameComplete()
    {
        if (areaVolumeGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.AreaVolumeGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.AreaVolumeGame);
            }

            playerStats.hasWonAreaVolumeGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.AreaVolumeGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.AreaVolumeGame);
            }

            playerStats.hasWonAreaVolumeGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeAreaVolumeGameSet()
    {
        playerStats.AreaVolumeGameSetCount = ((int)areaVolumeGameSetSlider.value);
        areaVolumeGameSetText.text = (playerStats.AreaVolumeGameSetCount + 1).ToString();
    }

    public void ToggleFiguresGameComplete()
    {
        if (figuresGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.FiguresGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.FiguresGame);
            }

            playerStats.hasWonFiguresGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.FiguresGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.FiguresGame);
            }

            playerStats.hasWonFiguresGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeFiguresGameSet()
    {
        playerStats.FiguresGameSetCount = ((int)figuresGameSetSlider.value);
        figuresGameSetText.text = (playerStats.FiguresGameSetCount).ToString();
    }

    public void ToggleClockGameComplete()
    {
        if (clockGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.ClockGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.ClockGame);
            }

            playerStats.hasWonClockGame = true;
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.ClockGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.ClockGame);
            }

            playerStats.hasWonClockGame = false;
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeClockGameTasksComplete()
    {
        playerStats.ClockGameSetCount = ((int)clockGameTaskSlider.value);
        clockGameSetText.text = playerStats.ClockGameSetCount.ToString();
    }

    public void ToggleRacingGameComplete()
    {
        if (racingGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.RacingGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.RacingGame);
            }

            playerStats.hasWonRacingGame = true;
            playerStats.hasMetUnderboss = true;
            playerStats.SetToHatDemoEvents();
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.RacingGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.RacingGame);
            }

            playerStats.hasWonRacingGame = false;
            playerStats.hasMetUnderboss = false;
            playerStats.SetToMeetUnderbossEvents();
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ToggleBossGameComplete()
    {
        if (bossGameComplete.isOn)
        {
            if (!CheckTrophys(PlayerStats.Trophy.BossTileGame))
            {
                playerStats.trophies.Add(PlayerStats.Trophy.BossTileGame);
            }

            playerStats.hasWonBossTileGame = true;
            playerStats.portal.gameObject.SetActive(true);
        }
        else
        {
            if (CheckTrophys(PlayerStats.Trophy.BossTileGame))
            {
                playerStats.trophies.Remove(PlayerStats.Trophy.BossTileGame);
            }

            playerStats.hasWonBossTileGame = false;
            playerStats.portal.gameObject.SetActive(false);
        }

        if (OnChangePuzzleCompletion != null)
            OnChangePuzzleCompletion();
    }

    public void ChangeBossGameSet()
    {
        playerStats.BossTileGameSetCount = ((int)bossGameSetSlider.value);
        bossGameSetText.text = (playerStats.BossTileGameSetCount + 1).ToString();
    }

    private bool CheckTrophys(PlayerStats.Trophy trophyToFind)
    {
        bool check = false;

        for (int i = 0; i < playerStats.trophies.Count; i++)
        {
            if (playerStats.trophies[i] == trophyToFind)
                check = true;
        }

        if (check)
            return true;
        else
            return false;
    }

    public void DeleteSaveFile()
    {
        if (File.Exists(path))
        {
            File.Delete(path);
            Debug.Log("Deleted saveGame in " + path);
        }
    }
}
