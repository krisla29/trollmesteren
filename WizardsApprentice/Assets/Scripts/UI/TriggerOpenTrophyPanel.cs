﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerOpenTrophyPanel : MonoBehaviour
{
    public GameManager gameManager;
    public Toggle trophyPanelButton;
    public float startShowDelay = 2;
    public float showPanelTime = 4;

    private void OnEnable()
    {
        StartCoroutine(ShowTrophyPanel());
        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
        gameManager.hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
        gameManager.hudCanvas.trophyButton.gameObject.SetActive(true);
        trophyPanelButton.interactable = false;
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator HideTrophyPanel()
    {
        yield return new WaitForSeconds(showPanelTime);
        
        gameManager.playerStats.hasSeenTrophyDemo = true;
        gameManager.playerStats.SaveGame();

        if (trophyPanelButton.isOn)
            trophyPanelButton.isOn = false;

        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        trophyPanelButton.interactable = true;
        transform.gameObject.SetActive(false);
    }

    private IEnumerator ShowTrophyPanel()
    {
        yield return new WaitForSeconds(startShowDelay);

        if(!trophyPanelButton.isOn)
            trophyPanelButton.isOn = true;

        StartCoroutine(HideTrophyPanel());
    }
}
