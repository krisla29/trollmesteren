﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSwapIcons : MonoBehaviour
{
    public Toggle toggle;
    public List<Image> onImages = new List<Image>();
    public List<Image> offImages = new List<Image>();
    public bool changeBgColor;
    public Image bg;
    public Color colorOn = Color.green;
    public Color colorOff = Color.grey;

    public void SetIcon()
    {
        if (toggle.isOn)
        {
            foreach (var onImage in onImages)
            {
                onImage.gameObject.SetActive(true);
            }
            foreach (var offImage in offImages)
            {
                offImage.gameObject.SetActive(false);
            } 
            
            if(changeBgColor)
            {
                bg.color = colorOn;
            }
        }
        else
        {
            foreach (var onImage in onImages)
            {
                onImage.gameObject.SetActive(false);
            }
            foreach (var offImage in offImages)
            {
                offImage.gameObject.SetActive(true);
            }

            if (changeBgColor)
            {
                bg.color = colorOff;
            }
        }
    }
}
