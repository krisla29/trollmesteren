﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ValueSlider : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    public bool lockValueSlider;
    public enum SliderType { Vertical, Horizontal}
    public SliderType sliderType;
    public Mask mask;
    public Image valuesImage;
    public bool useDynamicValues;
    public List<int> values = new List<int>();
    [Tooltip("Must be same size as values List")]
    public List<TextMeshProUGUI> textObjects = new List<TextMeshProUGUI>();
    public Color textColor = Color.white;
    public float textFontSize = 90f;
    public RectTransform textParent;
    public RectTransform rectTransform;
    public int key; //ReadOnly
    public int currentKey; //ReadOnly
    public int startKey = 0;
    public float speed = 20f;
    public bool isOn; //ReadOnly
    public bool isPressed; //ReadOnly

    private Vector2 res; //ReadOnly   

    public delegate void ValueSliderEvent(ValueSlider thisSlider); //Sends to StarMaker script, TileGame script
    public static event ValueSliderEvent OnValueSliderUp;

    public void Awake()
    {
        res = rectTransform.sizeDelta;

        if(sliderType == SliderType.Vertical)
        {
            key = Mathf.FloorToInt(res.y / res.x);
        }
        else if (sliderType == SliderType.Vertical)
        {
            key = Mathf.FloorToInt(res.x / res.y);
        }

        if(useDynamicValues)
        {
            SetValues();
        }
    }

    private void Start()
    {
        SetKey(startKey);
        currentKey = startKey;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!lockValueSlider)
        {
            SnapToKey();

            isPressed = true;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!lockValueSlider)
        {
            if (sliderType == SliderType.Vertical)
                VerticalMovement();
            else if (sliderType == SliderType.Horizontal)
                HorizontalMovement();

            isPressed = true;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!lockValueSlider)
        {
            SnapToKey();

            if(OnValueSliderUp != null)
            {
                OnValueSliderUp(this);
            }

            isPressed = false;
        }
    }

    void VerticalMovement()
    {
        var newPos = (Input.GetAxis("Mouse Y") * speed);
        rectTransform.anchoredPosition += new Vector2(0, newPos);

        if (rectTransform.anchoredPosition.y > (res.x * (key - 1)) + (res.x / 2)) //Continues to beginning
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, - (res.x / 2));
        else if(rectTransform.anchoredPosition.y < -(res.x / 2)) //Continues to end
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, (res.x * (key - 1)) + (res.x / 2));
    }

    void HorizontalMovement()
    {
        var newPos = (Input.GetAxis("Mouse X") * speed);
        rectTransform.anchoredPosition += new Vector2(newPos, 0);

        if (rectTransform.anchoredPosition.x > (res.y * (key - 1)) + (res.y / 2)) //Continues to beginning
            rectTransform.anchoredPosition = new Vector2(-(res.y / 2), rectTransform.anchoredPosition.y);
        else if(rectTransform.anchoredPosition.x < -(res.y / 2)) //Continues to end
            rectTransform.anchoredPosition = new Vector2((res.y * (key - 1)) + (res.y / 2), rectTransform.anchoredPosition.y);
    }

    void SnapToKey()
    {
        var curPos = rectTransform.anchoredPosition;

        if (sliderType == SliderType.Vertical)
        {
            currentKey = Mathf.Clamp(Mathf.RoundToInt(curPos.y / res.x), 0, key - 1);
            var newPos = res.x * currentKey;
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, newPos);
        }
        else if (sliderType == SliderType.Horizontal)
        {
            currentKey = Mathf.Clamp(Mathf.RoundToInt(curPos.x / res.y),0 , key - 1);
            var newPos = res.y * currentKey;
            rectTransform.anchoredPosition = new Vector2(newPos, rectTransform.anchoredPosition.y);
        }
    }

    public void IncreaseKey()
    {
        if (sliderType == SliderType.Vertical)
        {
            var newPos = res.x * (currentKey + 1);

            if (currentKey + 1 > (key - 1))
                newPos = 0;

            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, newPos);
        }
        else if (sliderType == SliderType.Horizontal)
        {
            var newPos = res.y * (currentKey + 1);

            if (currentKey + 1 > (key - 1))
                newPos = 0;
            rectTransform.anchoredPosition = new Vector2(newPos, rectTransform.anchoredPosition.y);
        }

        if (currentKey + 1 > (key - 1))
            currentKey = 0;
        else
            currentKey++;

        if (OnValueSliderUp != null)
        {
            OnValueSliderUp(this);
        }
    }

    public void DecreaseKey()
    {
        if (sliderType == SliderType.Vertical)
        {
            var newPos = res.x * (currentKey - 1);

            if (currentKey - 1 < 0)
                newPos = res.x * (key - 1);

            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, newPos);
        }
        else if (sliderType == SliderType.Horizontal)
        {
            var newPos = res.y * (currentKey - 1);

            if (currentKey - 1 < 0)
                newPos = res.y * (key - 1);

            rectTransform.anchoredPosition = new Vector2(newPos, rectTransform.anchoredPosition.y);
        }

        if (currentKey - 1 < 0)
            currentKey = (key - 1);
        else
            currentKey--;

        if (OnValueSliderUp != null)
        {
            OnValueSliderUp(this);
        }
    }

    public void SetKey(int passedKey)
    {
        var clampedKey = Mathf.Clamp(passedKey, 0, key - 1);

        if (sliderType == SliderType.Vertical)
        {
            var newPos = res.x * clampedKey;
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, newPos);
        }
        else if (sliderType == SliderType.Horizontal)
        {
            var newPos = res.y * clampedKey;
            rectTransform.anchoredPosition = new Vector2(newPos, rectTransform.anchoredPosition.y);
        }

        currentKey = clampedKey;
    }

    public void SetValues()
    {        
        for (int i = 0; i < textObjects.Count; i++)
        {
            textObjects[i].text = values[i].ToString();
            textObjects[i].color = textColor;
            textObjects[i].fontSize = textFontSize;
        }

        valuesImage.enabled = false;
        textParent.gameObject.SetActive(true);
    }
}
