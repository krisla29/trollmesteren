﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophyDemoKey : MonoBehaviour
{
    public GameManager gameManager;
    public Canvas trophyEventCanvas;
    public Camera cam;
    public RectTransform keySlot;
    public Transform keyPos;
    public Transform key;
    public Transform keyParent;
    public EasyRotate keyParentRotateScript;
    public Image keyUiImage;
    public Transform eventCamBgSprite;
    public AudioSource soundGainKey;

    private void Start()
    {
        StartCoroutine(KeySequence());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator KeySequence()
    {
        gameManager.player.anim.SetTrigger("Success");

        float t = 0;
        float animTime1 = 2f;

        eventCamBgSprite.gameObject.SetActive(true);
        keyParentRotateScript.enabled = false;
        Vector3 keyStartPos = key.transform.position;
        Vector3 keyStartRot = key.transform.eulerAngles;

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            var h = LerpUtils.SmootherStep(t);
            key.transform.position = Vector3.Lerp(keyStartPos, keyStartPos + (Vector3.up * 2f), h);
            key.transform.eulerAngles = Vector3.Lerp(keyStartRot, new Vector3(90, 0, 760), h);
            yield return new WaitForEndOfFrame();
        }

        if (soundGainKey)
            soundGainKey.Play();

        float x = 0;
        float ax = 2f;

        Quaternion startRot = key.transform.rotation;
        Vector3 startScale = key.transform.localScale;
        Vector3 startPos = key.transform.position;

        while(x < 1)
        {
            x += Time.deltaTime / ax;
            var h = LerpUtils.SmootherStep(x);
            var s = Mathf.Sin(h * (Mathf.PI));
            key.transform.rotation = startRot * Quaternion.AngleAxis(360 * h, Vector3.forward);
            key.transform.localScale = startScale + (Vector3.one * (s * 0.015f));
            key.transform.position = startPos + (Vector3.up * (s * 0.5f));
            yield return new WaitForEndOfFrame();
        }

        key.transform.SetParent(keySlot.transform);

        float t2 = 0;
        float animTime2 = 2f;

        Vector3 keyCurPos = key.transform.localPosition;
        Vector3 keyCurRot = key.transform.localEulerAngles;
        Vector3 keyCureScale = key.transform.localScale;
        keyUiImage.transform.parent.gameObject.SetActive(true);
        keyUiImage.gameObject.SetActive(true);        
        Quaternion curRot = key.transform.localRotation;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / animTime2;
            var h = LerpUtils.SmootherStep(t2);
            key.transform.localPosition = Vector3.Lerp(keyCurPos, Vector3.zero, h);
            key.transform.localScale = Vector3.Lerp(keyCureScale, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        key.transform.gameObject.SetActive(false);
        Vector3 curLocPos = key.transform.localPosition;
        Vector3 keyUiScale = key.transform.localScale;
       

        float t3 = 0;
        float animTime3 = 1f;        

        while (t3 < 1)
        {
            t3 += Time.deltaTime / animTime3;
            var h = LerpUtils.SmootherStep(t3);            
           
            key.transform.localScale = Vector3.Lerp(keyUiScale, Vector3.zero, h);
            key.transform.localPosition = Vector3.Lerp(curLocPos, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
    }

    void Finish()
    {
        keyParent.gameObject.SetActive(false);
    }
}
