﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundVolumeControl : MonoBehaviour
{
    public AudioMixer audioMixer;
    public AudioSource effectsTestSound;

    public void SetEffectsLevel(float effectsLevel)
    {
        audioMixer.SetFloat("effectsVol", effectsLevel);

        if(effectsTestSound)
        {
            if (!effectsTestSound.isPlaying)
                effectsTestSound.Play();
        }
    }

    public void SetMusicLevel(float musicLevel)
    {
        audioMixer.SetFloat("musicVol", musicLevel);
    }
}
