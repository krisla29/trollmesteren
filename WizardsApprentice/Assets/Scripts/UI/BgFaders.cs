﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgFaders : MonoBehaviour
{
    public CanvasGroup faderOpaque;
    public CanvasGroup faderTransparent;

    private Coroutine fo_FadeSingle;
    private Coroutine fo_FadeBoth;

    private Coroutine ft_FadeSingle;
    private Coroutine ft_FadeBoth;

    public float fadeTime = 1f;

    private void Awake()
    {
        faderOpaque.alpha = 0;
        faderTransparent.alpha = 0;
    }

    public void FO_FadeSingle(bool fadeOut, bool unscaledTime)
    {
        StopAllCoroutines();
        fo_FadeSingle = StartCoroutine(FaderOpaqueFadeSingle(fadeOut, unscaledTime));
    }

    public void FO_FadeBoth(bool unscaledTime)
    {
        StopAllCoroutines();
        fo_FadeBoth = StartCoroutine(FaderOpaqueFadeBoth(unscaledTime));
    }

    public void FT_FadeSingle(bool fadeOut, bool unscaledTime)
    {
        StopAllCoroutines();
        ft_FadeSingle = StartCoroutine(FaderTransparentFadeSingle(fadeOut, unscaledTime));
    }

    public void FT_FadeBoth(bool unscaledTime)
    {
        StopAllCoroutines();
        ft_FadeBoth = StartCoroutine(FaderTransparentFadeBoth(unscaledTime));
    }

    private IEnumerator FaderOpaqueFadeSingle(bool fadeOut, bool unscaledTime)
    {
        if (!fadeOut)
            faderOpaque.gameObject.SetActive(true);

        float t = 0;
        float curAlpha = faderOpaque.alpha;

        while(t < 1)
        {
            if (!unscaledTime)
                t += Time.deltaTime / fadeTime;
            else
                t += Time.unscaledDeltaTime / fadeTime;

            if (!fadeOut)
                faderOpaque.alpha = Mathf.Lerp(curAlpha, 1, t);
            else
                faderOpaque.alpha = Mathf.Lerp(curAlpha, 0, t);

            yield return new WaitForEndOfFrame();
        }

        if (fadeOut)
            faderOpaque.gameObject.SetActive(false);

        yield return null;
    }

    private IEnumerator FaderOpaqueFadeBoth(bool unscaledTime)
    {
        faderOpaque.gameObject.SetActive(true);

        float t = 0;
        float curAlpha = faderOpaque.alpha;

        while (t < 1)
        {
            if (!unscaledTime)
                t += Time.deltaTime / fadeTime;
            else
                t += Time.unscaledDeltaTime / fadeTime;

            faderOpaque.alpha = Mathf.Lerp(curAlpha, 1, t);

            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;

        while (t2 < 1)
        {
            if (!unscaledTime)
                t2 += Time.deltaTime / fadeTime;
            else
                t2 += Time.unscaledDeltaTime / fadeTime;

            faderOpaque.alpha = Mathf.Lerp(1, 0, t2);

            yield return new WaitForEndOfFrame();
        }

        faderOpaque.gameObject.SetActive(false);

        yield return null;
    }

    private IEnumerator FaderTransparentFadeSingle(bool fadeOut, bool unscaledTime)
    {
        if (!fadeOut)
            faderTransparent.gameObject.SetActive(true);

        float t = 0;
        float curAlpha = faderTransparent.alpha;

        while (t < 1)
        {
            if (!unscaledTime)
                t += Time.deltaTime / fadeTime;
            else
                t += Time.unscaledDeltaTime / fadeTime;

            if (!fadeOut)
                faderTransparent.alpha = Mathf.Lerp(curAlpha, 1, t);
            else
                faderTransparent.alpha = Mathf.Lerp(curAlpha, 0, t);

            yield return new WaitForEndOfFrame();
        }

        if (fadeOut)
            faderTransparent.gameObject.SetActive(false);

        yield return null;
    }

    private IEnumerator FaderTransparentFadeBoth(bool unscaledTime)
    {
        faderTransparent.gameObject.SetActive(true);

        float t = 0;
        float curAlpha = faderTransparent.alpha;

        while (t < 1)
        {
            if (!unscaledTime)
                t += Time.deltaTime / fadeTime;
            else
                t += Time.unscaledDeltaTime / fadeTime;

            faderTransparent.alpha = Mathf.Lerp(curAlpha, 1, t);

            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;

        while (t2 < 1)
        {
            if (!unscaledTime)
                t2 += Time.deltaTime / fadeTime;
            else
                t2 += Time.unscaledDeltaTime / fadeTime;

            faderTransparent.alpha = Mathf.Lerp(1, 0, t2);

            yield return new WaitForEndOfFrame();
        }

        faderTransparent.gameObject.SetActive(false);

        yield return null;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
