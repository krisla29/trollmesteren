﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotationSlider : MonoBehaviour
{
    public GameManager gameManager;
    public Slider rotateSlider;
    public RotationSliderHandle rotateSliderHandle;
    public bool rotationHandlePressed; //ReadOnly
    public float rotationLevel;
    public bool rotateBack = true;
    public float timeCounter = 10f;
    public float zeroMargin = 0.005f;
    private float timeStamp;
    public bool isRotatingBack;
    
    private void Update()
    {
        if (rotateBack)
        {
            rotationHandlePressed = rotateSliderHandle.isPressed;

            if (!rotationHandlePressed)
            {
                if(timeStamp == 0)
                    timeStamp = Time.time;

                if (Time.time > timeStamp + timeCounter)
                {
                    isRotatingBack = true;

                    if (rotateSlider.value - 0.500f != 0)
                        rotateSlider.value = Mathf.Lerp(rotateSlider.value, 0.500f, Time.deltaTime);

                    if(rotateSlider.value > (0.5f - zeroMargin) && rotateSlider.value < (0.5f + zeroMargin))
                    {
                        timeStamp = 0;
                        isRotatingBack = false;
                    }
                }
            }
            else
            {
                timeStamp = 0;
                isRotatingBack = false;
            }
        }
    }

    public float Rotate()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            if (gameManager.controlsMode == GameManager.ControlsMode.PC)
            {
                return Mathf.Clamp(rotationLevel, -0.500f, 0.500f);
            }
            else if (gameManager.controlsMode == GameManager.ControlsMode.GamePad)
            {
                return Mathf.Clamp(rotationLevel, -0.500f, 0.500f);
            }
            else
            {
                return Mathf.Clamp(rotationLevel, -0.500f, 0.500f);
            }
        }
        else
        {
            return Mathf.Clamp(rotateSlider.value - 0.500f, -0.500f, 0.500f);
        }
    }
    public void RotationBuilder()
    {
        if (Input.GetKey("e"))
        {
            if (rotationLevel < 1)
            {
                rotationLevel += 0.005f;
            }
        }
        else if (Input.GetKey("q"))
        {
            if (rotationLevel > 0)
            {
                rotationLevel -= 0.005f;
            }
        }

        var sliderValueTransposed = Mathf.Clamp(rotateSlider.value - 0.500f, -0.500f, 0.500f);
        sliderValueTransposed = rotationLevel;
    }

    public bool GetRotateHandle()
    {
        return rotateSliderHandle.isPressed;
    }
}
