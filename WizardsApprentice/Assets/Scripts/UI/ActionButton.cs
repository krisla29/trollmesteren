﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ActionButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public AimButton aimButton;
    public Sprite jumpIcon;
    public Color jumpColor;
    public Sprite interactIcon;
    public Color interactionColor;
    public Sprite fireIcon;
    public Color fireColor;
    public Sprite pickupIcon;
    public Color pickupColor;
    public Image currentImage;
    public Image buttonFill;
    public Image buttonOutline;

    public Button button;
    public bool isPressed;
    public bool isRelease;

    public bool ActionButtonUp()
    {
        return true;
    }

    public bool ActionButtonDown()
    {
        return true;
    }
    
    public void OnPointerUp(PointerEventData eventData)
    {
        isPressed = false;
        isRelease = true;
        ActionButtonUp();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        isPressed = true;
        ActionButtonDown();
        isRelease = false;
        /*
        if (aimButton.isOn)
        {
            if (isRelease)
            {
                isRelease = false;
            }
            else
            {
                isRelease = true;
            }
        }
        else
        {
            isRelease = false;
        }
        */
    }
}
