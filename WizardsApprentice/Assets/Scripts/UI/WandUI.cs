﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WandUI : MonoBehaviour
{
    public Image wandActive;
    public Image wandDeActive;
    public Image wandGlow;
    public Image wandShadow;
}
