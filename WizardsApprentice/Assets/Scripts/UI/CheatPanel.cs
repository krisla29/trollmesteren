﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CheatPanel : MonoBehaviour
{
    public List<string> cheatCodes = new List<string>();
    public List<int> intCode = new List<int>();
    public List<ValueSlider> valueSliders = new List<ValueSlider>();
    public TMP_InputField inputText;
    public Button checkAnswerButton;
    public AudioSource correctSound;
    public AudioSource wrongSound;
    public Transform cheatListPanel;
    public bool useString;

    public void CheckCode()
    {
        if (useString)
            StringCheck();
        else
            IntCheck();
    } 
    
    private void StringCheck()
    {
        bool test = false;

        for (int i = 0; i < cheatCodes.Count; i++)
        {
            if (cheatCodes[i] == inputText.text)
                test = true;
        }

        Response(test);
    }

    private void IntCheck()
    {
        bool test = true;

        for (int i = 0; i < intCode.Count; i++)
        {
            if (intCode[i] != valueSliders[i].currentKey)
                test = false;
        }

        Response(test);
    }

    private void Response(bool correct)
    {
        if (correct)
        {
            if (correctSound)
                correctSound.Play();

            if (cheatListPanel)
                cheatListPanel.gameObject.SetActive(true);

            inputText.text = "";

            transform.gameObject.SetActive(false);
        }
        else
        {
            if (wrongSound)
                wrongSound.Play();

            inputText.text = "";
        }
    }
}
