﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    public GameManager gameManager;
    public static bool gamePaused = false;
    public Button pauseButton;
    public InputManager InputManager;
    public GameObject pauseMenu;
    public GameObject controlsMenu;
    private AudioSource pausedMusic;

    private void Start()
    {
        pauseMenu.SetActive(false);
        pauseButton.onClick.AddListener(PauseGame);
    }

    void Update()
    {
        PauseInput();
    }

    public void PauseInput()
    {
        if(InputManager.CancelDown())
        {
            if(gamePaused)
            {
                ResumeGame();                
            }
            else
            {
                PauseGame();
            }
        }       
    }

    public void PauseGame()
    {
        gamePaused = true;
        print("pausing game");
        Time.timeScale = 0f;
        pauseMenu.SetActive(true);
        gameManager.EnableRayBlocker();

        if (!gameManager.playerStats.hasSeenOpening)
        {
            if (gameManager.soundTrackOpening.isPlaying)
            {
                pausedMusic = gameManager.soundTrackOpening;
                gameManager.soundTrackOpening.Pause();
            }
        }
    }

    public void ResumeGame()
    {
        gamePaused = false;
        print("resuming game");
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);

        /*
        if (gameManager.isScene)
        {
            if (gameManager.currentScene)
            {
                if (gameManager.currentScene.hasBackButton)
                    gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
            }
        }
        */
        gameManager.DisableRayBlocker();

        if (!gameManager.playerStats.hasSeenOpening)
        {
            if (pausedMusic != null)
                pausedMusic.Play();
        }
    }

    public void OpenControlsMenu()
    {
        controlsMenu.SetActive(true);
        pauseMenu.SetActive(false);
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
