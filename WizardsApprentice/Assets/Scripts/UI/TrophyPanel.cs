﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophyPanel : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject gainTrophyEffect;
    public Button closePanelButton;
    public AudioSource gainTrophySound;
   
    public List<Trophy> trophys;
    public List<Transform> emptyGraphics;

    private void OnEnable()
    {
        CheckTrophys();
    }

    private void OnDisable()
    {
        
    }

    public void GainTrophy(PlayerStats.Trophy passedTrophy)
    {
        for (int i = 0; i < trophys.Count; i++)
        {
            if(trophys[i].trophy == passedTrophy)
            {
                trophys[i].gameObject.SetActive(true);
                emptyGraphics[i].gameObject.SetActive(false);
                var effect = Instantiate(gainTrophyEffect, trophys[i].transform.parent.position, Quaternion.identity, trophys[i].transform.parent);
                if (gainTrophySound)
                    gainTrophySound.Play();
            }
        }
    }

    void CheckTrophys()
    {
        for (int i = 0; i < trophys.Count; i++)
        {
            if (gameManager.playerStats.trophies.Contains(trophys[i].trophy))
            {
                trophys[i].gameObject.SetActive(true);
                emptyGraphics[i].gameObject.SetActive(false);
            }
            else
            {
                trophys[i].gameObject.SetActive(false);
                emptyGraphics[i].gameObject.SetActive(true);
            }
        }
    }
}
