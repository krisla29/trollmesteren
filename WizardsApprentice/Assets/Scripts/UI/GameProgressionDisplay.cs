﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameProgressionDisplay : MonoBehaviour
{
    public PlayerStats playerStatsScript;    
    public TextMeshProUGUI levelText;
    public Image currentTrophyIcon;
    public Button currentDifficultyIcon;
    //Prefabs:
    public Image difficultyEasyIcon;
    public Image difficultyNormalIcon;
    public Image difficultyHardIcon;
    public Color easyColor;
    public Color normalColor;
    public Color hardColor;
    public Image orderGameIcon;
    public Color orderGameColor;
    public Image shooterGameIcon;
    public Color shooterGameColor;
    public Image tileGameIcon;
    public Color tileGameColor;
    public Image battleSortIcon;
    public Color battleSortColor;
    public Image raceGameIcon;
    public Color raceGameColor;
    public Image mazeGameIcon;
    public Color mazeGameColor;
    public Image figuresGameIcon;
    public Color figuresGameColor;
    public Image starMakerGameIcon;
    public Color starMakerGameColor;
    public Image areaVolumeGameIcon;
    public Color areaVolumeGameColor;
    public Image clockGameIcon;
    public Color clockGameColor;
    public Image bossGameIcon;
    public Color bossGameColor;

    public Color defaultColor = Color.grey;

    private void OnEnable()
    {
        if (playerStatsScript.currentGameScript != playerStatsScript.battleSortScript)
        {
            levelText.gameObject.SetActive(true);
            currentTrophyIcon.gameObject.SetActive(true);
            currentDifficultyIcon.gameObject.SetActive(true);
        }
        else
        {
            levelText.gameObject.SetActive(false);
            currentTrophyIcon.gameObject.SetActive(true);
            currentDifficultyIcon.gameObject.SetActive(false);
        }

        CheckTrophyIcon();

        DifficultyPanel.OnSetDifficulty += SetDifficultyIcon;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetDifficultyIcon;
        DifficultyPanel.OnSetDifficultyAndLevel += SetDifficultyIcon2;
    }
    private void OnDisable()
    {
        DifficultyPanel.OnSetDifficulty -= SetDifficultyIcon;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetDifficultyIcon;
        DifficultyPanel.OnSetDifficultyAndLevel -= SetDifficultyIcon2;
    }

    void FixedUpdate()
    {
        if (!playerStatsScript.currentHasCompleted)
            levelText.text = (playerStatsScript.currentLevel + 1).ToString() + "/" + (playerStatsScript.currentMaxLevel + 1).ToString();
        else
            levelText.text = playerStatsScript.currentCompletedSets.Count.ToString() + "/" + ((playerStatsScript.currentMaxLevel + 1) * 3).ToString();
    }

    void SetDifficultyIcon(PlayerStats.Difficulty requestedDifficulty)
    {
        var buttonImage = currentDifficultyIcon.GetComponent<Image>();

        if (requestedDifficulty == PlayerStats.Difficulty.Easy)
        {
            buttonImage.sprite = difficultyEasyIcon.sprite;
            buttonImage.color = easyColor;
        }
        else if (requestedDifficulty == PlayerStats.Difficulty.Normal)
        {
            buttonImage.sprite = difficultyNormalIcon.sprite;
            buttonImage.color = normalColor;
        }
        else if (requestedDifficulty == PlayerStats.Difficulty.Hard)
        {
            buttonImage.sprite = difficultyHardIcon.sprite;
            buttonImage.color = hardColor;
        }
    }
    void SetDifficultyIcon2(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        SetDifficultyIcon(requestedDifficulty);
    }

    void DeactivateDisplay()
    {

    }

    void CheckTrophyIcon()
    {
        switch(playerStatsScript.currentTrophy)
        {
            case (PlayerStats.Trophy.OrderGame):
                currentTrophyIcon.sprite = orderGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = orderGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.ShooterGame):
                currentTrophyIcon.sprite = shooterGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = shooterGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.TileGame):
                currentTrophyIcon.sprite = tileGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = tileGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.BattleSort):
                currentTrophyIcon.sprite = battleSortIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = battleSortColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.RacingGame):
                currentTrophyIcon.sprite = raceGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = raceGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.MazeGame):
                currentTrophyIcon.sprite = mazeGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = mazeGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.FiguresGame):
                currentTrophyIcon.sprite = figuresGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = figuresGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.StarMakerGame):
                currentTrophyIcon.sprite = starMakerGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = starMakerGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.AreaVolumeGame):
                currentTrophyIcon.sprite = areaVolumeGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = areaVolumeGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.ClockGame):
                currentTrophyIcon.sprite = clockGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = clockGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            case (PlayerStats.Trophy.BossTileGame):
                currentTrophyIcon.sprite = bossGameIcon.sprite;
                if (playerStatsScript.currentHasCompleted)
                    currentTrophyIcon.color = bossGameColor;
                else
                    currentTrophyIcon.color = defaultColor;
                break;
            default:
                break;
        }
    }
}
