﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_Demo : MonoBehaviour
{
    public GameManager gameManager;
    //public Animator demoAnim;
    public Transform masterObject;
    public CanvasGroup uiCanvasGrp;
    public CanvasGroup spriterCanvasGrp;
    public SpriterController spriterController;
    public RectTransform spriterRect;
    public RectTransform uiPointer;
    public Image uiPointerImage;
    public RectTransform uiPointerImageRect;
    public Interactable arrowPointer;
    public SpriteRenderer arrowPointerImage;
    public Transform navigationPointer;

    public RectTransform rotSliderRect;
    public RectTransform rotToggleRect;
    public RectTransform zoomSliderRect;

    public AudioSource soundSuccess;
    public AudioSource soundDemoComplete;

    private Coroutine fadeOut;
    private Coroutine fadeIn;
    private Coroutine fadeOutFast;
    private Coroutine fadeInFast;

    private Coroutine clickTutorial;
    private Coroutine rotateTutorial;
    private Coroutine rotateLockTutorial;
    private Coroutine zoomTutorial;
    private Coroutine finishTutorial;


    public float waitTime = 2;
    public float animPause = 4f;
    public float longFade = 2;
    public float fastFade = 0.5f;
    public float soundDelay = 0.5f;

    private Vector2 pointerSize;
    public Vector3 spriterScale = Vector3.one * 150f;

    //InteractionValues
    private float lastZoomValue;
    private float lastRotateValue;
    private bool lastRotationLockState;

    private bool didDemoClick;
    private bool didDemoRotate;
    private bool didDemoRotateLock;
    private bool didDemoZoom;

    private bool isAnimating;

    public List<Transform> objectsToWakeUp = new List<Transform>();

    private void Awake()
    {
        pointerSize = uiPointerImageRect.sizeDelta;
        arrowPointer.gameObject.SetActive(false);
        navigationPointer.gameObject.SetActive(false);
        uiCanvasGrp.alpha = 0;
        spriterCanvasGrp.alpha = 0;
    }

    private void Start()
    {
        if (!gameManager.playerStats.hasSeenInteractionDemo)
        {
            gameManager.playerStats.hasSeenOpening = true;
            gameManager.playerStats.timeGameScript.SetTimeAndUpdateAll(gameManager.playerStats.timeOfDay);
            LockAgentMovement();
            LockRotationSlider();
            LockZoomSlider();
            LockRotationToggle();
            gameManager.hudCanvas.HideButtons();
            //gameManager.hudCanvas.trophyButton.gameObject.SetActive(false);
            spriterController.transform.localScale = Vector3.zero;
            StartCoroutine(StartAnim());
            gameManager.playerStats.SaveGame();
        }
        else
        {
            transform.gameObject.SetActive(false);
        }
    }

    private IEnumerator StartAnim()
    {
        yield return new WaitForSeconds(0.5f);
        
        gameManager.playerStats.PortalEffect();

        yield return new WaitForSeconds(0.5f);

        //gameManager.player.anim.gameObject.SetActive(true);
        //gameManager.player.spriter.gameObject.SetActive(true);

        yield return new WaitForSeconds(2f);

        StartDemo();

        yield return null;
        /*
        float t = 0;
        float fadeTime = 2f;
        Color startCol = arrowPointerImage.color;

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            arrowPointerImage.color = Color.Lerp(new Color(startCol.r, startCol.g, startCol.b, 0), startCol, h);
            yield return new WaitForEndOfFrame();
        }
        */
    }

    void StartDemo()
    {
        //demoAnim.gameObject.SetActive(true);
       
        masterObject.gameObject.SetActive(true);
        arrowPointer.gameObject.SetActive(true);
        clickTutorial = StartCoroutine(ClickTutorial());
    }

    private void OnEnable()
    {
        InputManager.OnInputMouseDown += SetCurrentInteraction;
        InputManager.OnInputMouseUp += CheckLastInteraction;
        Interactable.OnInteractablePressed += CheckInteractable;
    }

    private void OnDisable()
    {
        InputManager.OnInputMouseDown -= SetCurrentInteraction;
        InputManager.OnInputMouseUp -= CheckLastInteraction;
        Interactable.OnInteractablePressed -= CheckInteractable;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void LockAgentMovement()
    {
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
    }

    void UnlockAgentMovement()
    {
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
    }

    void LockZoomSlider()
    {
        gameManager.hudCanvas.zoomSlider.interactable = false;
    }

    void UnlockZoomSlider()
    {
        gameManager.hudCanvas.zoomSlider.interactable = true;
    }

    void LockRotationSlider()
    {
        gameManager.hudCanvas.rotateSlider.interactable = false;
        gameManager.player.cameraMovementController.rotationLock = true;
    }

    void UnlockRotationSlider()
    {
        gameManager.hudCanvas.rotateSlider.interactable = true;
        gameManager.player.cameraMovementController.rotationLock = false;
    }

    void LockRotationToggle()
    {
        gameManager.hudCanvas.lockRotationToggle.enabled = false;
    }

    void UnlockRotationToggle()
    {
        gameManager.hudCanvas.lockRotationToggle.enabled = true;
    }

    void LockAll()
    {
        LockAgentMovement();
        LockRotationSlider();
        LockRotationToggle();
        LockZoomSlider();
    }

    void UnlockAll()
    {
        UnlockAgentMovement();
        UnlockRotationSlider();
        UnlockRotationToggle();
        UnlockZoomSlider();
    }
    
    void CheckInteractable(Transform passedTransform)
    {
        if (!didDemoClick)
        {
            if (passedTransform == arrowPointer.transform)
            {
                StartCoroutine(DemoClickFinish());
            }
        }
    }

    void SetCurrentInteraction()
    {
        lastZoomValue = gameManager.hudCanvas.zoomSlider.value;
        lastRotateValue = gameManager.hudCanvas.rotateSlider.value;
        lastRotationLockState = gameManager.hudCanvas.lockRotationToggle.isOn;

        if(isAnimating)
            FadeFastOut();
    }

    void CheckLastInteraction()
    {
        if(didDemoClick && !didDemoRotate)
        {
            if(gameManager.hudCanvas.rotateSlider.value != lastRotateValue)
            {
                StartCoroutine(DemoRotationFinish());
            }
        }

        if(didDemoRotate && !didDemoRotateLock)
        {
            if(gameManager.hudCanvas.lockRotationToggle.isOn)
            {
                StartCoroutine(DemoRotateLockFinish());
            }
        }

        if(didDemoRotateLock && !didDemoZoom)
        {
            if(gameManager.hudCanvas.zoomSlider.value != lastZoomValue)
            {
                StartCoroutine(DemoZoomFinish());
            }
        }

        if(isAnimating)
            FadeFastIn();
    }

    private IEnumerator DemoClickFinish()
    {
        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;
        didDemoClick = true;
        arrowPointer.transform.GetComponent<Collider>().enabled = false;
        navigationPointer.gameObject.SetActive(true);
        gameManager.player.navMeshAgent.SetDestination(navigationPointer.position);

        if (clickTutorial != null)
            StopCoroutine(clickTutorial);

        FadeOutLong();

        float t = 0;
        float fadeTime = 2f;
        Color startCol = arrowPointerImage.color;

        while(t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            arrowPointerImage.color = Color.Lerp(startCol, new Color(startCol.r, startCol.g, startCol.b, 0), h);
            yield return new WaitForEndOfFrame();
        }

        arrowPointer.gameObject.SetActive(false);      

        yield return new WaitForSeconds(waitTime);

        InitializeRotationDemo();
    }

    void InitializeRotationDemo()
    {
        HideCanvasGrps();
        //demoAnim.SetBool("hasClicked", true);
        //demoAnim.Play("HUD_demo_rotate", 0, 0);
        UnlockRotationSlider();
        rotateTutorial = StartCoroutine(RotateTutorial());
    }

    private IEnumerator DemoRotationFinish()
    {
        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;
        didDemoRotate = true;
        //LockRotationSlider();

        if (rotateTutorial != null)
            StopCoroutine(rotateTutorial);

        FadeOutLong();

        yield return new WaitForSeconds(waitTime);        

        InitializeRotationLockDemo();
    }

    void InitializeRotationLockDemo()
    {
        HideCanvasGrps();
        //demoAnim.SetBool("hasRotated", true);
        //demoAnim.Play("HUD_demo_rotate_lock", 0, 0);
        UnlockRotationToggle();
        rotateLockTutorial = StartCoroutine(RotateLockTutorial());
    }

    private IEnumerator DemoRotateLockFinish()
    {
        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;
        didDemoRotateLock = true;
        //LockRotationToggle();

        if (rotateLockTutorial != null)
            StopCoroutine(rotateLockTutorial);

        FadeOutLong();

        yield return new WaitForSeconds(waitTime);

        InitializeZoomDemo();
    }

    void InitializeZoomDemo()
    {
        HideCanvasGrps();
        //demoAnim.SetBool("hasLocked", true);
        //demoAnim.Play("HUD_demo_zoom", 0, 0);
        UnlockZoomSlider();
        zoomTutorial = StartCoroutine(ZoomTutorial());
    }

    private IEnumerator DemoZoomFinish()
    {
        uiPointer.gameObject.SetActive(false);

        if (soundSuccess)
            soundSuccess.PlayDelayed(soundDelay);

        isAnimating = false;
        didDemoZoom = true;

        LockAll();
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);
        spriterRect.anchoredPosition3D = new Vector3(0, 0, -150f);

        if (zoomTutorial != null)
            StopCoroutine(zoomTutorial);

        FadeInLong();

        yield return new WaitForSeconds(waitTime * 0.5f);

        float curZoom = gameManager.hudCanvas.zoomSlider.value;
        float t = 0;
        float zoomBack = 1f;

        while(t < 1)
        {
            t += Time.deltaTime / zoomBack;
            var h = LerpUtils.SmootherStep(t);
            gameManager.hudCanvas.zoomSlider.value = Mathf.Lerp(curZoom, 1, h);
            yield return new WaitForEndOfFrame();
        }       

        yield return new WaitForSeconds(waitTime * 0.5f);

        if (soundDemoComplete)
            soundDemoComplete.Play();

        FadeOutLong();

        yield return new WaitForSeconds(longFade);

        FinishDemo();
    }

    void FinishDemo()
    {       
        ResetCanvasGrps();
        UnlockAll();
        gameManager.hudCanvas.lockRotationToggle.isOn = true;
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);
        //gameManager.hudCanvas.trophyButton.gameObject.SetActive(true);
        gameManager.playerStats.hasSeenInteractionDemo = true;
        gameManager.playerStats.SaveGame();

        foreach (Transform child in objectsToWakeUp)
        {
            child.gameObject.SetActive(true);
        }

        transform.gameObject.SetActive(false);
    }

    private IEnumerator FadeOut()
    {
        float t = 0;
        float fadeTime = longFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 0, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 0, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        //spriterController.gameObject.SetActive(false);
    }

    private IEnumerator FadeIn()
    {
        float t = 0;
        float fadeTime = longFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;
        //spriterController.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 1, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 1, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, spriterScale, h);
            yield return new WaitForEndOfFrame();
        }

        spriterController.animator.SetTrigger("TriggerIdleTalk");
    }

    void ResetCanvasGrps()
    {
        uiCanvasGrp.alpha = 1;
        spriterCanvasGrp.alpha = 1;
    }

    void HideCanvasGrps()
    {
        uiCanvasGrp.alpha = 0;
        spriterCanvasGrp.alpha = 0;
    }

    private IEnumerator FadeOutFast()
    {
        float t = 0;
        float fadeTime = fastFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 0, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 0, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        //spriterController.gameObject.SetActive(false);
    }

    private IEnumerator FadeInFast()
    {
        float t = 0;
        float fadeTime = fastFade;
        float curFade1 = uiCanvasGrp.alpha;
        float curFade2 = spriterCanvasGrp.alpha;
        Vector3 spriterCurScale = spriterController.transform.localScale;
        //spriterController.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            uiCanvasGrp.alpha = Mathf.Lerp(curFade1, 1, h);
            spriterCanvasGrp.alpha = Mathf.Lerp(curFade2, 1, h);
            spriterController.transform.localScale = Vector3.Lerp(spriterCurScale, spriterScale, h);
            yield return new WaitForEndOfFrame();
        }

        spriterController.animator.SetTrigger("TriggerIdleTalk");
    }

    void FadeInLong()
    {
        StopFades();
        fadeIn = StartCoroutine(FadeIn());
    }

    void FadeOutLong()
    {
        StopFades();
        fadeOut = StartCoroutine(FadeOut());
    }

    void FadeFastIn()
    {
        StopFades();
        fadeInFast = StartCoroutine(FadeInFast());
    }

    void FadeFastOut()
    {
        StopFades();
        fadeOutFast = StartCoroutine(FadeOutFast());
    }

    void StopFades()
    {
        if (fadeInFast != null)
            StopCoroutine(fadeInFast);
        if (fadeOutFast != null)
            StopCoroutine(fadeOutFast);
        if (fadeIn != null)
            StopCoroutine(fadeIn);
        if (fadeOut != null)
            StopCoroutine(fadeOut);
    }

    private IEnumerator ClickTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(uiCanvasGrp.transform);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 35f);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(Screen.width * 0.2f, Screen.height * 0.1f, -150f);

        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(Screen.width * 0.2f, -Screen.height * 0.4f);
        Vector2 endAnchPos = Vector2.zero;

        while(t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 0.75f;
        Vector2 startScale = uiPointerImageRect.sizeDelta;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI * 1));
            uiPointer.anchoredPosition = new Vector2((-Screen.width * 0.015f) * s, (Screen.height * 0.03f) * s);
            uiPointerImageRect.sizeDelta = new Vector2(startScale.x + (s * (startScale.x * 0.15f)), startScale.y - (s * (startScale.y * 0.15f)));
            yield return new WaitForEndOfFrame();
        }

        float t3 = 0;
        float a3 = 1.5f;
        Vector2 startAnchPos2 = endAnchPos;
        Vector2 endAnchPos2 = startAnchPos;

        while (t3 < 1)
        {
            t3 += Time.deltaTime / a3;
            var h = LerpUtils.SmootherStep(t3);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(new Vector2(startAnchPos2.x * 0.5f, startAnchPos2.y * 0.5f), new Vector2(endAnchPos2.x * 0.5f, endAnchPos2.y * 0.5f), h);
            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        clickTutorial = StartCoroutine(ClickTutorial());
    }

    private IEnumerator RotateTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(rotSliderRect);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 180f);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(-Screen.width * 0.15f, 0, -150f);

        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(0, Screen.height * 0.5f);
        Vector2 endAnchPos = Vector2.zero;

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 4f;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI * 2));
            s *= Screen.width * 0.25f;
            uiPointer.anchoredPosition = new Vector2(s, 0);

            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        rotateTutorial = StartCoroutine(RotateTutorial());
    }

    private IEnumerator RotateLockTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(rotToggleRect.transform);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 180);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(Screen.width * 0.15f, Screen.height * 0.1f, -150f);

        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(0, Screen.height * 0.4f);
        Vector2 endAnchPos = Vector2.zero;

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 0.75f;
        Vector2 startScale = uiPointerImageRect.sizeDelta;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI * 1));
            uiPointer.anchoredPosition = new Vector2(0, (-Screen.height * 0.03f) * s);
            uiPointerImageRect.sizeDelta = new Vector2(startScale.x + (s * (startScale.x * 0.15f)), startScale.y - (s * (startScale.y * 0.15f)));
            yield return new WaitForEndOfFrame();
        }

        float t3 = 0;
        float a3 = 1.5f;
        Vector2 startAnchPos2 = endAnchPos;
        Vector2 endAnchPos2 = startAnchPos;

        while (t3 < 1)
        {
            t3 += Time.deltaTime / a3;
            var h = LerpUtils.SmootherStep(t3);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(new Vector2(startAnchPos2.x * 0.5f, startAnchPos2.y * 0.5f), new Vector2(endAnchPos2.x * 0.5f, endAnchPos2.y * 0.5f), h);
            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        rotateLockTutorial = StartCoroutine(RotateLockTutorial());
    }

    private IEnumerator ZoomTutorial()
    {
        isAnimating = true;
        uiPointer.SetParent(zoomSliderRect);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 45f);
        uiPointerImageRect.sizeDelta = pointerSize;

        FadeInLong();
        //spriterController.animator.SetTrigger("TriggerTalk");
        spriterRect.anchoredPosition3D = new Vector3(-Screen.width * 0.15f, Screen.height * 0.1f, -150f);


        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(Screen.width * 0.3f, 0);
        Vector2 endAnchPos = new Vector2(0, Screen.height * 0.35f);

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.SmootherStep(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 3f;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI));
            s *= -Screen.height * 0.4f;
            uiPointer.anchoredPosition = new Vector2(0,endAnchPos.y + s);

            yield return new WaitForEndOfFrame();
        }

        FadeOutLong();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        while (gameManager.inputManager.isPressing)
            yield return new WaitForEndOfFrame();

        zoomTutorial = StartCoroutine(ZoomTutorial());
    }
    /*
    private IEnumerator FinishTutorial()
    {
        isAnimating = true;
        uiPointer.gameObject.SetActive(false);
        uiPointer.SetParent(uiCanvasGrp.transform);
        uiPointer.transform.localEulerAngles = new Vector3(0, 0, 0);

        FadeFastIn();

        float t = 0;
        float a = 2f;
        Vector2 startAnchPos = new Vector2(Screen.width * 0.3f, 0);
        Vector2 endAnchPos = new Vector2(0, Screen.height * 0.35f);

        while (t < 1)
        {
            t += Time.deltaTime / a;
            var h = LerpUtils.EaseOut(t);
            //var s = Mathf.Sin(t * (Mathf.PI * 2));
            uiPointer.anchoredPosition = Vector2.Lerp(startAnchPos, endAnchPos, h);
            yield return new WaitForEndOfFrame();
        }

        float t2 = 0;
        float a2 = 3f;

        while (t2 < 1)
        {
            t2 += Time.deltaTime / a2;
            //var h = LerpUtils.EaseOut(t2);
            var s = Mathf.Sin(t2 * (Mathf.PI));
            s *= -Screen.height * 0.4f;
            uiPointer.anchoredPosition = new Vector2(0, endAnchPos.y + s);

            yield return new WaitForEndOfFrame();
        }

        FadeFastOut();

        isAnimating = false;

        yield return new WaitForSeconds(animPause);

        finishTutorial = StartCoroutine(FinishTutorial());
    }
    */
    /*
    float t = 0;
    float a = 1f;

    while(t < 1)
    {
        t += Time.deltaTime / a;
        var h = LerpUtils.SmootherStep(t);
        var s = Mathf.Sin(t * (Mathf.PI * 2));
        yield return new WaitForEndOfFrame();
    }
    */

}
