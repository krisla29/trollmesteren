﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuControls : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject pauseMenu;
    public Text controlsModeText;
    public Text aimModeText;
    public Text agentModeText;
    public Text interactionModeText;

    public void Start()
    {
        if (gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            controlsModeText.text = "Mobile Mode";
        }
        else
        {
            controlsModeText.text = "PC Mode";
        }

        if (gameManager.hasFirstPersonAim)
        {
            aimModeText.text = "First Person Aim";
        }
        else
        {
            aimModeText.text = "Auto Aim";
        }

        if (gameManager.isNavMeshAgent)
        {
            agentModeText.text = "PointAndClick ON";
        }
        else
        {
            agentModeText.text = "PointAndClick OFF";
        }

        if(gameManager.interactionMode == GameManager.InteractionModes.Direct)
        {
            interactionModeText.text = "Direct Mode";
        }
        else
        {
            interactionModeText.text = "Wand Mode";
        }
    }
    public void ToggleControlsMode()
    {
        if(gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            controlsModeText.text = "PC Mode";
            gameManager.controlsMode = GameManager.ControlsMode.PC;
        }
        else
        {
            controlsModeText.text = "Mobile Mode";
            gameManager.controlsMode = GameManager.ControlsMode.Mobile;
        }
    }

    public void ToggleAimMode()
    {
        if(gameManager.hasFirstPersonAim)
        {
            aimModeText.text = "Auto Aim";
            gameManager.hasFirstPersonAim = false;
        }
        else
        {
            aimModeText.text = "First Person Aim";
            gameManager.hasFirstPersonAim = true;
        }
    }

    public void ToggleAgentMode()
    {
        if(gameManager.isNavMeshAgent)
        {
            agentModeText.text = "PointAndClick OFF";
            gameManager.isNavMeshAgent = false;
        }
        else
        {
            agentModeText.text = "PointAndClick ON";
            gameManager.isNavMeshAgent = true;
        }
    }

    public void ToggleInteractionMode()
    {
        if(gameManager.interactionMode == GameManager.InteractionModes.Direct)
        {
            interactionModeText.text = "Wand Mode";
            gameManager.interactionMode = GameManager.InteractionModes.Wand;
        }
        else
        {
            interactionModeText.text = "Direct Mode";
            gameManager.interactionMode = GameManager.InteractionModes.Direct;
        }
    }

    public void ExitToPauseMenu()
    {
        pauseMenu.SetActive(true);
        transform.gameObject.SetActive(false);
    }
}
