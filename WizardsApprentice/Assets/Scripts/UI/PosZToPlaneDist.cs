﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosZToPlaneDist : MonoBehaviour
{
    public RectTransform objectToPos;
    public Canvas canvas;
    private float startPosZ;
    public float multiplier = 100f;

    private void Awake()
    {
        startPosZ = objectToPos.localPosition.z;
    }

    private void Update()
    {
        float posZ = startPosZ + ((-canvas.planeDistance * multiplier));

        objectToPos.localPosition = new Vector3(objectToPos.localPosition.x, objectToPos.localPosition.y, posZ);
    }
}
