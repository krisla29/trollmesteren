﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TrophyPanelDemo : MonoBehaviour
{
    public GameManager gameManager;
    public float startShowDelay = 2;
    public float showPanelTime = 4;
    public AudioSource trophySound;
    public AudioSource demoStepDone;
    public AudioSource demoComplete;
    public Transform closeButtonGrp;
    public Transform worldKeyGrp;

    public List<CanvasGroup> trophyCanvasGroups;
    public List<TextMeshProUGUI> slotTexts;
    public List<Transform> objectsToActivate = new List<Transform>();
    public float waitTime = 3f;
    
    void StartDemo()
    {
        if (demoStepDone)
            demoStepDone.Play();

        StartCoroutine(ShowTrophyPanel());
        gameManager.hudCanvas.LockTrophyBtn();
        gameManager.hudCanvas.ResetUiPointer();        
    }

    public void EndDemo()
    {
        if (demoComplete)
            demoComplete.Play();

        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);

        StartCoroutine(TerminateDemo());
    }

    void InitializeDemo()
    {
        StartCoroutine(FadeBgIn());
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);      
        gameManager.hudCanvas.LockAll();
        gameManager.hudCanvas.UnlockTrophyBtn();
        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
        gameManager.hudCanvas.trophyBtnEmpty.gameObject.SetActive(false);
        gameManager.hudCanvas.trophyButton.gameObject.SetActive(true);
        gameManager.hudCanvas.StartTrophyBtnPulser();       
        gameManager.hudCanvas.uiPointer.SetParent(gameManager.hudCanvas.trophyBtnPulser.transform.parent);
        gameManager.hudCanvas.uiPointer.anchoredPosition = new Vector2(gameManager.hudCanvas.uiPointerBtnXpos, 0);
        gameManager.hudCanvas.uiPointer.gameObject.SetActive(true);

        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
    }

    private void OnEnable()
    {
        StartCoroutine(WaitUntilEventEnds());
    }

    private IEnumerator DisplayFadedTrophy(int trophy)
    {
        yield return new WaitForSeconds((trophy * waitTime) + waitTime);
        
        trophyCanvasGroups[trophy].alpha = 0;
        var mainObject = trophyCanvasGroups[trophy].transform.GetChild(0);
        mainObject.gameObject.SetActive(true);

        float t = 0;

        if (trophySound)
            trophySound.Play();

        while(t < 1)
        {
            t += Time.deltaTime;
            slotTexts[trophy].alpha = Mathf.Lerp(1, 0, t);
            trophyCanvasGroups[trophy].alpha = Mathf.Lerp(0, 0.75f, t);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.5f);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            slotTexts[trophy].alpha = Mathf.Lerp(0, 1, h);
            trophyCanvasGroups[trophy].alpha = Mathf.Lerp(0.75f, 0, h);
            mainObject.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        mainObject.gameObject.SetActive(false);

        trophyCanvasGroups[trophy].enabled = false;

        if(trophy == trophyCanvasGroups.Count - 1)
        {
            closeButtonGrp.gameObject.SetActive(true);
        }
    }

    void ShowCloseButtonGrp()
    {

    }

    private IEnumerator FadeBgIn()
    {
        gameManager.hudCanvas.panelBgFader.alpha = 0;
        gameManager.hudCanvas.panelBgFader.gameObject.SetActive(true);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 2;
            var h = LerpUtils.SmootherStep(t);
            gameManager.hudCanvas.panelBgFader.alpha = Mathf.Lerp(0, 0.5f, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FadeBgOut()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 2;
            var h = LerpUtils.SmootherStep(t);
            gameManager.hudCanvas.panelBgFader.alpha = Mathf.Lerp(0.5f, 0, h);
            yield return new WaitForEndOfFrame();
        }

        gameManager.hudCanvas.panelBgFader.alpha = 0;
        gameManager.hudCanvas.panelBgFader.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator TerminateDemo()
    {
        closeButtonGrp.gameObject.SetActive(false);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 2;
            var h = LerpUtils.SmootherStep(t);
            gameManager.hudCanvas.panelBgFader.alpha = Mathf.Lerp(0.5f, 0, h);
            yield return new WaitForEndOfFrame();
        }

        gameManager.hudCanvas.panelBgFader.alpha = 0;
        gameManager.hudCanvas.panelBgFader.gameObject.SetActive(false);

        gameManager.playerStats.hasSeenTrophyDemo = true;
        gameManager.playerStats.SaveGame();

        if (gameManager.hudCanvas.trophyButton.isOn)
            gameManager.hudCanvas.trophyButton.isOn = false;

        gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        gameManager.hudCanvas.trophyButton.interactable = true;
        gameManager.hudCanvas.ResetTrophyBtnPulser();
        gameManager.hudCanvas.UnlockAll();
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);

        foreach (var item in objectsToActivate)
        {
            item.gameObject.SetActive(true);
        }

        transform.gameObject.SetActive(false);
    }

    private IEnumerator ShowTrophyPanel()
    {
        //yield return new WaitForSeconds(startShowDelay);     

        for (int i = 0; i < trophyCanvasGroups.Count; i++)
        {
            StartCoroutine(DisplayFadedTrophy(i));
        }

        yield return null;
    }

    public void CheckDemo()
    {
        if(!gameManager.playerStats.hasSeenTrophyDemo)
        {
            StartDemo();
        }
    }

    private IEnumerator WaitUntilEventEnds()
    {
        
        if (gameManager)
        {
            while (gameManager.isGameEvent)
            {
                yield return new WaitForEndOfFrame();
            }
        }
        
        if(gameManager && gameManager.hudCanvas)
            InitializeDemo();

        yield return null;
    }
}
