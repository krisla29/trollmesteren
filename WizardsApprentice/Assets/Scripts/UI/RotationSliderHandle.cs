﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RotationSliderHandle : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public GameManager gameManager;
    public bool isPressed;
    public bool isWorldRotateSlider;
    //public bool isDragged;

    public void OnPointerUp(PointerEventData eventData)
    {
        isPressed = false;
        //isRelease = true;
        if(isWorldRotateSlider)
            rotatSliderUp();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if(gameManager.hudCanvas.rotateSlider.interactable && isWorldRotateSlider)
            gameManager.hudCanvas.lockRotationToggle.isOn = false;

        isPressed = true;

        if(isWorldRotateSlider)
            RotateSliderDown();
        //isRelease = false;
    }
    /*
    public void OnMouseDrag()
    {
        isDragged = true;
    }

    public void OnMouseUp()
    {
        isDragged = false;
    }
    */
    public bool rotatSliderUp()
    {
        return true;
    }

    public bool RotateSliderDown()
    {
        return true;
    }
}
