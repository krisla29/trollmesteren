﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleToScreen : MonoBehaviour
{
    [Tooltip("Defaults to transform if left open")]
    public Transform objectToScale;
    public float scaleMultiplier = 1;
    private float horizontalFactor;
    private float verticalFactor;
    private Vector3 newScale;

    public enum Axes { XY, XZ, ZY}
    public Axes axis = Axes.XY;

    private void Awake()
    {
        if (!objectToScale)
            objectToScale = transform;
    }

    private void OnEnable()
    {
        float width = Screen.width;
        float height = Screen.height;

        horizontalFactor = width / height;
        verticalFactor = height / width;
        /*
        print(width);
        print(height);
        print(horizontalFactor + "-----" + verticalFactor);
        */
        if(axis == Axes.XY)
            newScale = new Vector3(objectToScale.transform.localScale.x * horizontalFactor, objectToScale.transform.localScale.y * verticalFactor, objectToScale.transform.localScale.z) * scaleMultiplier;
        else if(axis == Axes.XZ)
            newScale = new Vector3(objectToScale.transform.localScale.x * horizontalFactor, objectToScale.transform.localScale.y, objectToScale.transform.localScale.z * verticalFactor) * scaleMultiplier;
        else if(axis == Axes.ZY)
            newScale = new Vector3(objectToScale.transform.localScale.x, objectToScale.transform.localScale.y * verticalFactor, objectToScale.transform.localScale.z * horizontalFactor) * scaleMultiplier;
        
        objectToScale.transform.localScale = newScale;
    }
}
