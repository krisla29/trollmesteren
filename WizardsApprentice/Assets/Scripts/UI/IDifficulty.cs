﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDifficulty
{
    void CheckDifficulty();
    void WrongTicker();
    void CorrectTicker();
    void SetGameSetWithDifficulty(int gameSetIndex);
    void OpenDifficultyPanelStart();
    void SetCurrentCompletedSet(int passedAchievement);
    void SetPlayerStatsCurrentCompletedSets();
    void TransferPlayerStats();
}

public interface ISetDifficulty
{
    void SetCurrentDifficulty(PlayerStats.Difficulty difficulty);
    void SetCurrentDifficultyStartSet(PlayerStats.Difficulty difficulty);
    void StartSelectedGameSet(PlayerStats.Difficulty difficulty, int setCounter);
}

interface IAchievementPanel
{
    int CheckLevelIndex(PlayerStats.Difficulty difficulty);
    int CheckCurrentAchievement();
    int CheckPrevAchievement();
    void OpenAchievementPanel(AchievementPanel achievementPanel, PlayerStats.Difficulty difficulty, int levelIndex, int totalLevels, int prevAchievement, int achievement);
}