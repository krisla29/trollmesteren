﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudCanvas : MonoBehaviour
{
    public GameManager gameManager;
    public PlayerController player;
    public CameraMovementController playerCam;
    public CameraManager cameraManager;
    public GameObject corsair;
    public Toggle lockRotationToggle;
    public Text powerUpText;
    public ActionButton actionButton;
    public RectTransform mobileControls;
    public Slider zoomSlider;
    public Slider rotateSlider;
    public Transform camJoystick;
    public Transform mainJoystick;
    public Transform gemsPanel;
    public TextMeshProUGUI gemsText;
    public Transform gemsPanelShadow;
    public Button aimButton;
    public Button exitButton;
    public Button mapButton;
    public WorldMapButton worldMapButton;
    public Transform worldMapBtnEmpty;
    public Toggle trophyButton;
    public Transform trophyBtnEmpty;
    public Button toggleCharacterButton;
    public Transform toggleCharacterBtnEmpty;
    public RectTransform buttonsPanel;
    public TrophyPanel trophyPanel;
    public RectTransform pauseInteractionIcon;
    public Image blackScreen;
    public Image blackScreenFadeToBlack;
    public Image blackScreenFadeOut;
    public GameProgressionDisplay gameProgressionDisplay;
    public DifficultyPanel difficultyPanel;
    public RectTransform uiPointer;
    public float uiPointerBtnXpos = -150f;
    public EasyScalePulse mapBtnPulser;
    public EasyScalePulse toggleCharBtnPulser;
    public EasyScalePulse trophyBtnPulser;
    public Image rayBlocker;
    public AddCharacterEffect addCharacterEffect;
    public CanvasGroup panelBgFader;
    public BgFaders bgFaders;
    //public Button aimRectMobile;
    //public Button hitArea;
    //private float sliderStartValue;
    private void Awake()
    {
        gameProgressionDisplay.gameObject.SetActive(false);
        trophyPanel.gameObject.SetActive(false);
    }
    /*
    private void Start()
    {
        
        if(gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            mobileControls.gameObject.SetActive(false);
        }
        else
        {
            mobileControls.gameObject.SetActive(true);
        }
        
        //sliderStartValue = zoomSlider.value;
    }
    private void Update()
    {
        if (!MenuPause.gamePaused)
        {
            //ToggleCorsair();
            //ActionButton();
            //ToggleCamJoy();
            //ToggleMainJoy();
            //ToggleAimButton();
            //ToggleExitButton();
            //ToggleGemPanel();
            //ToggleHitArea();
        }
        else
        {
            //corsair.SetActive(false);
            //actionButton.gameObject.SetActive(false);
            //camJoystick.gameObject.SetActive(false);
            //mainJoystick.gameObject.SetActive(false);
            //aimButton.gameObject.SetActive(false);
            //exitButton.gameObject.SetActive(false);
            //hitArea.gameObject.SetActive(false);
            //aimRectMobile.gameObject.SetActive(false);
        }
    }
    */

    public void LockRotationSlider()
    {
        rotateSlider.interactable = false;
    }

    public void UnlockRotationSlider()
    {
        rotateSlider.interactable = true;
    }

    public void LockZoomSlider()
    {
        zoomSlider.interactable = false;
    }

    public void UnlockZoomSlider()
    {
        zoomSlider.interactable = true;
    }

    public void LockToggleCharacterBtn()
    {
        toggleCharacterButton.interactable = false;
    }

    public void UnlockToggleCharacterBtn()
    {
        toggleCharacterButton.interactable = true;
    }

    public void LockTrophyBtn()
    {
        trophyButton.interactable = false;
    }

    public void UnlockTrophyBtn()
    {
        trophyButton.interactable = true;
    }

    public void LockMapBtn()
    {
        mapButton.interactable = false;
    }

    public void UnlockMapBtn()
    {
        mapButton.interactable = true;
    }

    public void ResetUiPointer()
    {
        uiPointer.gameObject.SetActive(false);
        uiPointer.SetParent(buttonsPanel);
        uiPointer.anchoredPosition = Vector2.zero;
    }

    public void LockAll()
    {
        LockRotationSlider();
        LockZoomSlider();
        LockToggleCharacterBtn();
        LockTrophyBtn();
        LockMapBtn();
    }

    public void UnlockAll()
    {
        UnlockRotationSlider();
        UnlockZoomSlider();
        UnlockToggleCharacterBtn();
        UnlockTrophyBtn();
        UnlockMapBtn();
    }

    public void StartMapBtnPulser()
    {
        mapBtnPulser.enabled = true;
    }

    public void ResetMapBtnPulser()
    {
        mapBtnPulser.enabled = false;
        mapBtnPulser.transform.localScale = Vector3.one;
    }

    public void StartTrophyBtnPulser()
    {
        trophyBtnPulser.enabled = true;
    }

    public void ResetTrophyBtnPulser()
    {
        trophyBtnPulser.enabled = false;
        trophyBtnPulser.transform.localScale = Vector3.one;
    }

    public void StartToggleCharBtnPulser()
    {
        toggleCharBtnPulser.enabled = true;
    }

    public void ResetToggleCharBtnPulser()
    {
        toggleCharBtnPulser.enabled = false;
        toggleCharBtnPulser.transform.localScale = Vector3.one;
    }

    public void HideButtons()
    {
        worldMapButton.gameObject.SetActive(false);
        worldMapBtnEmpty.gameObject.SetActive(false);
        trophyButton.gameObject.SetActive(false);
        trophyBtnEmpty.gameObject.SetActive(false);
        toggleCharacterButton.gameObject.SetActive(false);
        toggleCharacterBtnEmpty.gameObject.SetActive(false);
    }

    public void HideUI()
    {
        HideButtons();
        rotateSlider.gameObject.SetActive(false);
        lockRotationToggle.gameObject.SetActive(false);
        zoomSlider.gameObject.SetActive(false);
        //gemsPanel.gameObject.SetActive(false);
    }

    public void ShowUISlidersAndGems()
    {
        rotateSlider.gameObject.SetActive(true);
        lockRotationToggle.gameObject.SetActive(true);
        zoomSlider.gameObject.SetActive(true);
        gemsPanel.gameObject.SetActive(true);
    }

    void ToggleCamJoy()
    {
        if(gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            if(!gameManager.isScene && !gameManager.isNavMeshAgent)
            {
                camJoystick.gameObject.SetActive(true);
                
            }
            else
            {
                camJoystick.gameObject.SetActive(false);
                
            }
        }
        else
        {
            camJoystick.gameObject.SetActive(false);
        }
    }

    void ToggleMainJoy()
    {
        if (gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            if (!gameManager.isNavMeshAgent)
            {
                if (!gameManager.isScene)
                {
                    mainJoystick.gameObject.SetActive(true);
                }
                else
                {
                    if(!gameManager.currentScene.lockPlayerMovement)
                    {
                        mainJoystick.gameObject.SetActive(true);
                    }
                    else
                    {
                        mainJoystick.gameObject.SetActive(false);
                    }

                }

            }
            else
            {
                mainJoystick.gameObject.SetActive(false);

            }
        }
        else
        {
            mainJoystick.gameObject.SetActive(false);
        }
    }

    void ToggleCorsair()
    {
        if (playerCam)
        {
            if (playerCam.player.toggleAim && !gameManager.isNavMeshAgent)
            {
                corsair.SetActive(true);
                //aimRectMobile.gameObject.SetActive(true);
            }
            else
            {
                corsair.SetActive(false);
                //aimRectMobile.gameObject.SetActive(false);
            }
        }
        else
        {
            corsair.SetActive(false);
        }
    }
    /*
    void ToggleExitButton()
    {
        if(gameManager.isScene)
        {
            if(gameManager.currentScene.hasBackButton)
                exitButton.gameObject.SetActive(true);
            else
                exitButton.gameObject.SetActive(false);
        }
        else
        {
            exitButton.gameObject.SetActive(false);
        }
    }
    */
    void PowerUp(string message, int value)
    {

        powerUpText.text = message + "     " + value.ToString();

        if(value == 0)
        {
            powerUpText.text = "";
        }

    }

    void ToggleGemPanel()
    {
        if (gameManager.isScene)
        {
            if (gameManager.currentScene.isBattleScene)
            {
                gemsPanel.gameObject.SetActive(false);
                gemsPanelShadow.gameObject.SetActive(false);
            }
            else
            {
                gemsPanel.gameObject.SetActive(true);
                gemsPanelShadow.gameObject.SetActive(true);
            }
        }
        else
        {
            gemsPanel.gameObject.SetActive(true);
            gemsPanelShadow.gameObject.SetActive(true);
        }
    }
    /*
    void ToggleHitArea()
    {
        if(gameManager.isNavMeshAgent)
        {
            hitArea.gameObject.SetActive(true);
        }
        else
        {
            hitArea.gameObject.SetActive(false);
        }
    }
    */
    void ActionButton()
    {
        if (player && !gameManager.isNavMeshAgent)
        {
            actionButton.gameObject.SetActive(true);

            if (!player.isInteracting)
            {
                if ((player.toggleAim || player.toggleAimThirdPerson) && !player.isPickingUp)
                {
                    if (actionButton.fireIcon)
                    {
                        actionButton.currentImage.sprite = actionButton.fireIcon;
                        actionButton.buttonFill.color = actionButton.fireColor;
                    }
                }

                if ((player.toggleAim || player.toggleAimThirdPerson) && player.isPickingUp)
                {
                    if (actionButton.pickupIcon)
                    {
                        actionButton.currentImage.sprite = actionButton.pickupIcon;
                        actionButton.buttonFill.color = actionButton.pickupColor;
                    }
                }

                if ((!player.toggleAim && !player.toggleAimThirdPerson) && !player.isInteracting)
                {
                    if (actionButton.jumpIcon)
                    {
                        actionButton.currentImage.sprite = actionButton.jumpIcon;
                        actionButton.buttonFill.color = actionButton.jumpColor;
                    }
                }
            }
            else
            {
                if(actionButton.interactIcon)
                {
                    actionButton.currentImage.sprite = actionButton.interactIcon;
                    actionButton.buttonFill.color = actionButton.interactionColor;
                }
            }
        }
        else
        {
            actionButton.gameObject.SetActive(false);
        }
    }

    void ToggleAimButton()
    {
        if (player)
        {
            if (player.wand != null)
            {
                if (!gameManager.isScene)
                {
                    aimButton.gameObject.SetActive(true);
                }
                else
                {
                    if(!gameManager.currentScene.disableAim)
                    {
                        aimButton.gameObject.SetActive(true);
                    }
                    else
                    {
                        aimButton.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                aimButton.gameObject.SetActive(false);
            }            
        }
        else
        {
            aimButton.gameObject.SetActive(false);
        }
    }

    void ToggleAimButtonOn()
    {
        if(!gameManager.hasFirstPersonAim && aimButton.gameObject.activeSelf == false)
        {
            aimButton.gameObject.SetActive(true);
        }
    }
    void ToggleAimButtonOff()
    {
        if (aimButton.gameObject.activeSelf == true)
        {
            aimButton.gameObject.SetActive(false);
        }
    }

     void OnEnable()
    {
        //PlayerController.OnPlayerMessage += PowerUp;
        //PlayerController.OnPlayerGetsWand += ToggleAimButtonOn;
        //PlayerController.OnPlayerDeath += ToggleAimButtonOff;
        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;
        PlayerStats.OnActivateGame += ActivateGameProgressionDisplay;
        PlayerStats.OnDeactivateGame += DeactivateGameProgressionDisplay;
    }
     void OnDisable()
    {
        //PlayerController.OnPlayerMessage -= PowerUp;
        //PlayerController.OnPlayerGetsWand -= ToggleAimButtonOn;
        //PlayerController.OnPlayerDeath -= ToggleAimButtonOff;
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
        PlayerStats.OnActivateGame -= ActivateGameProgressionDisplay;
        PlayerStats.OnDeactivateGame -= DeactivateGameProgressionDisplay;
    }
    void ReferenceNewPlayer(Transform transform)
    {
        player = transform.GetComponent<PlayerController>();
        playerCam = player.cameraMovementController;
        cameraManager = playerCam.GetComponent<CameraManager>();
    }

    void ActivateGameProgressionDisplay()
    {
        gameProgressionDisplay.gameObject.SetActive(true);        
    }

    void DeactivateGameProgressionDisplay()
    {
        gameProgressionDisplay.gameObject.SetActive(false);
    }
}
