﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AchievementPanel : MonoBehaviour
{
    public PlayerStats.Difficulty difficulty;
    public Image circle;
    public Image bgShadow;
    public List<Image> stars = new List<Image>();
    public Image checkMark;
    public Sprite easyIcon;
    public Sprite normalIcon;
    public Sprite hardIcon;
    public Color colorEasy;
    public Color colorNormal;
    public Color colorHard; 
    public TextMeshProUGUI levelIndexText;
    public Image levelDifficulty;
    public AudioSource soundOpen;
    public AudioSource soundClose;
    public AudioSource soundCheckMark;
    public List<AudioSource> starSounds = new List<AudioSource>();

    public int prevAchievment;
    public int achievement;
    public int reward;
    public int levelIndex;
    public int totalLevels;

    private Color bgColor;
    private Color bgStartColor;

    private void Awake()
    {
        bgColor = bgShadow.color;
        bgStartColor = new Color(bgColor.r, bgColor.g, bgColor.b, 0);
    }

    private void OnEnable()
    {
        StartCoroutine(RewardRoutine());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator RewardRoutine()
    {
        SetDifficulty();
        SetLevelIndexText();
        SetPreviousAchievement();
        checkMark.transform.localScale = Vector3.zero;
        bgShadow.color = bgStartColor;

        float t = 0;
        float animTime1 = 1;

        if (soundOpen)
            soundOpen.Play();

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            bgShadow.color = Vector4.Lerp(bgStartColor, bgColor, h);
            yield return new WaitForEndOfFrame();
        }

        float tim = 0;
        float animTime3 = 0.5f;

        if (soundCheckMark)
            soundCheckMark.Play();

        while (tim < 1)
        {
            tim += Time.deltaTime / animTime3;
            float h = tim;
            h = tim * tim * tim * (tim * (6f * tim - 15f) + 10f);
            checkMark.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.5f);

        List<int> newStars = CheckStarsToActivate();
        float waitTime = 3 - newStars.Count;

        for (int i = 0; i < newStars.Count; i++)
        {
            float w = 0;
            float animTimeStars = 0.5f;
            stars[newStars[i]].gameObject.SetActive(true);

            if (starSounds[newStars[i]])
                starSounds[newStars[i]].Play();

            while (w < 1)
            {
                w += Time.deltaTime / animTimeStars;
                float h = w;
                h = w * w * w * (w * (6f * w - 15f) + 10f);
                stars[newStars[i]].transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(0.5f);
        }

        newStars.Clear();

        yield return new WaitForSeconds(waitTime);

        float ti = 0;
        float animTime2 = 1;

        if (soundClose)
            soundClose.Play();

        while (ti < 1)
        {
            ti += Time.deltaTime / animTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            bgShadow.color = Vector4.Lerp(bgColor, bgStartColor, h);
            yield return new WaitForEndOfFrame();
        }

        DeactivatePanel();
    }

    void SetDifficulty()
    {
        switch(difficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                circle.color = colorEasy;
                levelDifficulty.sprite = easyIcon;
                levelDifficulty.color = colorEasy;
                break;
            case (PlayerStats.Difficulty.Normal):
                circle.color = colorNormal;
                levelDifficulty.sprite = normalIcon;
                levelDifficulty.color = colorNormal;
                break;
            case (PlayerStats.Difficulty.Hard):
                circle.color = colorHard;
                levelDifficulty.sprite = hardIcon;
                levelDifficulty.color = colorHard;
                break;
            default:
                break;
        }
    }

    void SetLevelIndexText()
    {
        levelIndexText.text = levelIndex.ToString() + "/" + totalLevels.ToString();
    }

    void SetPreviousAchievement()
    {
        switch(prevAchievment)
        {
            case (0):
                stars[0].gameObject.SetActive(false);
                stars[1].gameObject.SetActive(false);
                stars[2].gameObject.SetActive(false);
                break;
            case (1):
                stars[0].gameObject.SetActive(true);
                stars[0].transform.localScale = Vector3.one;
                stars[1].gameObject.SetActive(false);
                stars[2].gameObject.SetActive(false);
                break;
            case (2):
                stars[0].gameObject.SetActive(true);
                stars[0].transform.localScale = Vector3.one;
                stars[1].gameObject.SetActive(true);
                stars[1].transform.localScale = Vector3.one;
                stars[2].gameObject.SetActive(false);
                break;
            case (3):
                stars[0].gameObject.SetActive(true);
                stars[0].transform.localScale = Vector3.one;
                stars[1].gameObject.SetActive(true);
                stars[1].transform.localScale = Vector3.one;
                stars[2].gameObject.SetActive(true);
                stars[2].transform.localScale = Vector3.one;
                break;
            default:
                stars[0].gameObject.SetActive(false);
                stars[1].gameObject.SetActive(false);
                stars[2].gameObject.SetActive(false);
                break;
        }
    }

    List<int> CheckStarsToActivate()
    {
        int newStars = achievement - prevAchievment;
        List<int> starsToReturn = new List<int>();

        for (int i = 0; i < newStars; i++)
        {
            starsToReturn.Add(prevAchievment + i);
        }

        return starsToReturn;
    }
    
    void ResetValues()
    {
        transform.localScale = Vector3.zero;

        for (int i = 0; i < stars.Count; i++)
        {
            stars[i].gameObject.SetActive(false);
            stars[i].transform.localScale = Vector3.zero;
        }

        checkMark.transform.localScale = Vector3.zero;
        bgShadow.color = new Color(bgColor.r, bgColor.g, bgColor.b, 0);
    }
    
    void DeactivatePanel()
    {
        ResetValues();
        transform.gameObject.SetActive(false);
    }
}
