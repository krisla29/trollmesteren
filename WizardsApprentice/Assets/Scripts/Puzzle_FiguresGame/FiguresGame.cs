﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FiguresGame : MonoBehaviour
{
    public GameManager gameManager;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.FiguresGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;
    public Slider unfoldSlider;
    public Slider scaleSlider;
    public ValueSlider shapeSlider1;
    public ValueSlider shapeSlider2;
    public Transform slider2Parent;
    public Transform slider2Line;
    public Button shapeSliderButton1;
    public Button shapeSliderButton2;
    public RectTransform checkButtonParent;
    public List<Button> buttonsToDeactivate = new List<Button>();
    public List<RectTransform> linesParents = new List<RectTransform>();
    public List<Transform> blockers = new List<Transform>();
    public Transform objectsCenter;
    public int gemsReward = 10;
    public Collider gemPrefab;
    public GameObject effectPrefab;
    public Transform effectsContainer;
    public Transform shadow;
    public List<FG_Figure> figures = new List<FG_Figure>();
    public List<int> figuresCompleted = new List<int>();
    public FG_Figure currentFigure;

    private Vector3 shadowStartScale;

    public enum Shapes { Square, Triangle, Circle, Rectangle, Fan, Lens, Pentagon, Hexagon, None }

    public bool hasWonGame;
    private bool lastHasWonGame;

    public Color colorUnsolved = Color.blue;
    public Color colorSolved = Color.green;

    public AudioSource soundWin;
    public AudioSource soundSpawnGems;
    public AudioSource soundWrong;
    public AudioSource soundOpenShape;
    public AudioSource soundWinGame;

    public List<Transform> ObjectsToActivateOnComplete = new List<Transform>();
    public List<Transform> ObjectsToSleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    public delegate void FiguresGameEvent(); //Sends To PlayerStats
    public static event FiguresGameEvent OnFiguresGameEnable;
    public static event FiguresGameEvent OnFiguresGameDisable;

    private void Awake()
    {
        currentFigure = figures[0];        
    }
    
    private void OnEnable()
    {
        GetPlayerStats();
        Initialize();

        if (OnFiguresGameEnable != null)
            OnFiguresGameEnable();
    }

    private void OnDisable()
    {        
        foreach (Transform child in effectsContainer)
        {
            Destroy(child.gameObject);
        }

        StopAllCoroutines();
        CancelInvoke();

        if (OnFiguresGameDisable != null)
            OnFiguresGameDisable();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    void GetPlayerStats() //Gets from PlayerStats
    {
        hasWonGame = gameManager.playerStats.hasWonFiguresGame;
        lastHasWonGame = gameManager.playerStats.hasWonFiguresGame;

        figuresCompleted.Clear();

        for (int i = 0; i < gameManager.playerStats.figuresGameFiguresCompleted.Count; i++)
        {
            figuresCompleted.Add(gameManager.playerStats.figuresGameFiguresCompleted[i]);
        }
    }

    void Initialize()
    {
        unfoldSlider.value = 1;

        foreach (var item in figures)
        {
            item.gameObject.SetActive(false);
        }

        shadowStartScale = shadow.localScale;
        //currentFigure = figures[0];

        if (figuresCompleted.Contains(figures.IndexOf(currentFigure)))
        {
            SetCurrentSolved();
        }
        else
        {
            SetCurrentUnsolved();
        }

        currentFigure.gameObject.SetActive(true);
        bool isSolved = CheckIfFigureSolved();
        CheckNumberOFShapes(isSolved);

        foreach (var item in blockers)
        {
            item.gameObject.SetActive(false);
        }

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
    }

    void MinorInitialize()
    {
        foreach (var item in blockers)
        {
            item.gameObject.SetActive(false);
        }

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
    }

    private void Update()
    {
        currentFigure.anim.Play(0, -1, unfoldSlider.normalizedValue);
        currentFigure.transform.localScale = Vector3.one + (Vector3.one * scaleSlider.value);
        shadow.localScale = shadowStartScale + (Vector3.one * (scaleSlider.value * 3f));

        if (unfoldSlider.value < 1)
        {
            scaleSlider.value = 0;
            scaleSlider.gameObject.SetActive(false);
        }
        else
        {
            scaleSlider.gameObject.SetActive(true);
        }
    }

    public void NextFigure()
    {
        var index = figures.IndexOf(currentFigure) + 1;
        currentFigure.gameObject.SetActive(false);

        if (figures.Count - 1 >= index)
            currentFigure = figures[index];
        else
            currentFigure = figures[0];

        currentFigure.gameObject.SetActive(true);
        currentFigure.transform.localRotation = Quaternion.identity;        
        unfoldSlider.value = 1;
        bool isSolved = CheckIfFigureSolved();
        CheckNumberOFShapes(isSolved);
    }

    public void LastFigure()
    {
        var index = figures.IndexOf(currentFigure) - 1;
        currentFigure.gameObject.SetActive(false);

        if (index > -1)
            currentFigure = figures[index];
        else
            currentFigure = figures[figures.Count - 1];

        currentFigure.gameObject.SetActive(true);
        currentFigure.transform.localRotation = Quaternion.identity;        
        unfoldSlider.value = 1;
        bool isSolved = CheckIfFigureSolved();
        CheckNumberOFShapes(isSolved);
    }

    void CheckNumberOFShapes(bool isSolved)
    {
        if (currentFigure.shapes.Count == 1)
        {
            slider2Parent.gameObject.SetActive(false);
            slider2Line.gameObject.SetActive(false);
            SetSliderValue(Shapes.None);
        }
        else
        {
            slider2Parent.gameObject.SetActive(true);

            if (!isSolved)
            {                
                slider2Line.gameObject.SetActive(true);
            }
            else
            {
                slider2Line.gameObject.SetActive(false);
            }
        }
    }

    bool CheckIfFigureSolved()
    {
        //if(!hasWonGame)
        //{
        if(figuresCompleted.Contains(figures.IndexOf(currentFigure)))
        {
            SetCurrentSolved();
            return true;
        }
        else
        {
            SetCurrentUnsolved();
            return false;
        }
        //}
    }

    public void CheckCurrentFigure()
    {

    }

    public void CheckShapeSliders()
    {
        if (!figuresCompleted.Contains(figures.IndexOf(currentFigure)))
        {
            if (currentFigure.shapes.Count == 1)
            {
                bool check1 = false;

                if (currentFigure.shapes[0] == CheckSliderValue(shapeSlider1.currentKey) && (CheckSliderValue(shapeSlider2.currentKey) == Shapes.None ||
                    currentFigure.shapes[0] == CheckSliderValue(shapeSlider2.currentKey)))
                    check1 = true;
                else if (currentFigure.shapes[0] == CheckSliderValue(shapeSlider2.currentKey) && (CheckSliderValue(shapeSlider1.currentKey) == Shapes.None ||
                    currentFigure.shapes[0] == CheckSliderValue(shapeSlider1.currentKey)))
                    check1 = true;

                if (check1)
                    Correct();
                else
                {
                    if (soundWrong)
                        soundWrong.PlayDelayed(0.3f);
                }
            }
            else if (currentFigure.shapes.Count == 2)
            {
                bool check2 = false;

                if ((currentFigure.shapes[0] == CheckSliderValue(shapeSlider1.currentKey) || currentFigure.shapes[0] == CheckSliderValue(shapeSlider2.currentKey))
                    && (currentFigure.shapes[1] == CheckSliderValue(shapeSlider1.currentKey) || currentFigure.shapes[1] == CheckSliderValue(shapeSlider2.currentKey))
                    && shapeSlider1.currentKey != shapeSlider2.currentKey)
                    check2 = true;

                if (check2)
                    Correct();
                else
                {
                    if (soundWrong)
                        soundWrong.PlayDelayed(0.3f);
                }
            }
            else
            {
                if (soundWrong)
                    soundWrong.PlayDelayed(0.3f);
            }

            print(CheckSliderValue(shapeSlider1.currentKey).ToString() + "  and  " + CheckSliderValue(shapeSlider2.currentKey));
        }
    }

    void Correct() //Adds to PlayerStats
    {
        if (soundWin)
            soundWin.PlayDelayed(0.3f);

        figuresCompleted.Add(figures.IndexOf(currentFigure));

        if (!gameManager.playerStats.figuresGameFiguresCompleted.Contains(figures.IndexOf(currentFigure)))
            gameManager.playerStats.figuresGameFiguresCompleted.Add(figures.IndexOf(currentFigure));

        if (figuresCompleted.Count >= figures.Count)
        {
            if(!hasWonGame)
            {
                foreach (var item in ObjectsToActivateOnComplete)
                {
                    item.gameObject.SetActive(true);
                }

                foreach (var item in ObjectsToSleepOnComplete)
                {
                    item.gameObject.SetActive(false);
                }
            }

            hasWonGame = true;
            gameManager.playerStats.hasWonFiguresGame = true;
        }

        SetCurrentSolved();

        foreach (var item in blockers)
        {
            item.gameObject.SetActive(true);
        }

        gameManager.playerStats.SaveGame();
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        gameManager.player.anim.SetTrigger("Success");
        StartCoroutine(Success());
    }

    void ResetShapeSliders()
    {
        shapeSlider1.SetKey(8);
        shapeSlider2.SetKey(8);

        shapeSlider1.valuesImage.color = colorUnsolved;
        shapeSlider2.valuesImage.color = colorUnsolved;
    }

    void SetShapeSliders()
    {
        if(currentFigure.shapes.Count == 1)
        {
            shapeSlider1.SetKey(SetSliderValue(currentFigure.shapes[0]));
            shapeSlider2.SetKey(8);
        }
        if(currentFigure.shapes.Count == 2)
        {
            shapeSlider1.SetKey(SetSliderValue(currentFigure.shapes[0]));
            shapeSlider2.SetKey(SetSliderValue(currentFigure.shapes[1]));
        }

        shapeSlider1.valuesImage.color = colorSolved;
        shapeSlider2.valuesImage.color = colorSolved;
    }

    Shapes CheckSliderValue(int sliderValue)
    {
        switch(sliderValue)
        {
            case (0):
                return Shapes.Square;
            case (1):
                return Shapes.Triangle;
            case (2):
                return Shapes.Circle;
            case (3):
                return Shapes.Rectangle;
            case (4):
                return Shapes.Fan;
            case (5):
                return Shapes.Lens;
            case (6):
                return Shapes.Pentagon;
            case (7):
                return Shapes.Hexagon;
            case (8):
                return Shapes.None;
            default:
                return Shapes.None;
        }
    }

    int SetSliderValue(Shapes shape)
    {
        switch (shape)
        {
            case (Shapes.Square):
                return 0;
            case (Shapes.Triangle):
                return 1;
            case (Shapes.Circle):
                return 2;
            case (Shapes.Rectangle):
                return 3;
            case (Shapes.Fan):
                return 4;
            case (Shapes.Lens):
                return 5;
            case (Shapes.Pentagon):
                return 6;
            case (Shapes.Hexagon):
                return 7;
            case (Shapes.None):
                return 8;
            default:
                return 8;
        }
    }

    void EnableButtons(bool enable)
    {
        foreach (var item in buttonsToDeactivate)
        {
            item.gameObject.SetActive(enable);
        }

        shapeSliderButton1.enabled = enable;
        shapeSliderButton2.enabled = enable;
        checkButtonParent.gameObject.SetActive(enable);

        foreach (var item in linesParents)
        {
            item.gameObject.SetActive(enable);
        }        
    }
    
    void SetCurrentSolved()
    {
        unfoldSlider.gameObject.SetActive(true);
        SetShapeSliders();
        EnableButtons(false);
    }

    void SetCurrentUnsolved()
    {
        unfoldSlider.gameObject.SetActive(false);
        ResetShapeSliders();
        EnableButtons(true);
    }

    private IEnumerator Success()
    {
        print("Success");

        float t = 0;
        float animTime1 = 4f;

        if (soundOpenShape)
            soundOpenShape.PlayDelayed(1f);

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            unfoldSlider.value = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }

        gameManager.player.anim.SetTrigger("Success");

        if (soundSpawnGems)
            soundSpawnGems.Play();

        SpawnEffect();

        for (int i = 0; i < gemsReward; i++)
        {
            StartCoroutine(SpawnGem());
            yield return new WaitForSeconds(0.2f);
        }

        yield return new WaitForSeconds((0.2f * gemsReward) + 0.5f); // 3 is travelTime for gems...

        if (figuresCompleted.Count == figures.Count)
            StartCoroutine(WinGame());
        else
            MinorInitialize();

        gameManager.playerStats.SaveGame();
        yield return null;
    }

    private IEnumerator WinGame()
    {
        if(!lastHasWonGame)
        {
            gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
            trophyPanel.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            if (soundWinGame)
                soundWinGame.Play();
            trophyGraphic.gameObject.SetActive(true);
            yield return new WaitForSeconds(3);           
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
            trophyGraphic.gameObject.SetActive(false);
            trophyPanel.gameObject.SetActive(false);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
            gameManager.playerStats.trophies.Add(trophy);
            gameManager.playerStats.hasWonFiguresGame = true;            
            gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        }

        lastHasWonGame = true;
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
        yield return null;
    }

    private IEnumerator SpawnGem()
    {
        float x = 0;
        float travelTime = 1f;

        var gem = Instantiate(gemPrefab, effectsContainer);
        gem.enabled = false;
        gem.transform.position = objectsCenter.transform.position;
        gem.transform.localScale = Vector3.one * 1.5f;
        var startPos = objectsCenter.transform.position;
        var endPos = gameManager.player.transform.position + Vector3.up;

        while (x < 1)
        {
            x += Time.deltaTime / travelTime;
            float h = x;
            h = x * x * x * (x * (6f * x - 15f) + 10f);
            gem.transform.position = Vector3.Lerp(startPos, endPos, h);
            yield return new WaitForEndOfFrame();
        }

        gem.enabled = true;

        yield return new WaitForEndOfFrame();
    }

    void SpawnEffect()
    {
        var effect = Instantiate(effectPrefab, effectsContainer);
        effect.transform.position = objectsCenter.position;
    }
}
