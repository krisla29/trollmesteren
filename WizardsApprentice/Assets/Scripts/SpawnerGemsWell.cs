﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerGemsWell : MonoBehaviour
{
    public GameManager gameManager;
    public GameObject gemPrefab;
    public GameObject effectPrefab;
    public Transform releaseButton;
    public PlayerStats playerStats;
    public AudioSource playerReleavesGems;
    public AudioSource soundError;
    public Transform gemsContainer;
    public bool isReady = true;
    //public TheWell theWellScript;

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += CheckIfReady;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= CheckIfReady;
        foreach (Transform child in gemsContainer)
        {
            Destroy(child.gameObject);
        }

        CancelInvoke();
        StopAllCoroutines();
    }

    void CheckIfReady(Transform passedInteractable)
    {
        if (passedInteractable == releaseButton)
        {
            if (isReady)
                ReleaseCurrentGems();
            else
            {
                if (soundError)
                    soundError.Play();
            }
        }
    }

    void ReleaseCurrentGems()
    {
        isReady = false;        

        if (playerStats.gems > 0)
        {
            gameManager.player.anim.SetTrigger("Throw");
            Invoke("SpawnEffect", 2f);

            int gemsToSpawn = playerStats.gems;

            if (playerStats.gems > 10)
                gemsToSpawn = 10;

            for (int i = 0; i < gemsToSpawn; i++)
            {
                Invoke("ReleaseGem", Random.Range(0.5f, 1.5f));
            }               
        }

        playerStats.StartAddWellGems(playerStats.gems, 2);

        releaseButton.gameObject.SetActive(false);
    }

    void SpawnEffect()
    {
        var effect = Instantiate(effectPrefab, transform.position - (Vector3.up * 2f), Quaternion.identity, gameManager.worldEffectsContainer);
    }

    void ReleaseGem()
    {
        playerReleavesGems.Play();
        var randomOffset = new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
        var randomRot = Quaternion.Euler(0, Random.Range(-179f, 179f), 0);

        var gemInstance = Instantiate(gemPrefab, transform.position + randomOffset, randomRot, gemsContainer);       
        //playerStats.gems -= 1;
    }

    private void OnDestroy()
    {
        CancelInvoke();
        StopAllCoroutines();
    }
}
