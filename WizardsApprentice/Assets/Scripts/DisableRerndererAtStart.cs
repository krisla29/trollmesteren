﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableRerndererAtStart : MonoBehaviour
{
    public MeshRenderer meshRenderer;

    void Start()
    {
        meshRenderer.enabled = false;
    }   
}
