﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillIfCollisionTag : MonoBehaviour
{
    public string tag;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(tag))
        {
            Destroy(transform.gameObject);
        }
    }
}
