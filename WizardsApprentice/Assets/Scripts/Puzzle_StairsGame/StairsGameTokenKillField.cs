﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsGameTokenKillField : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.GetComponent<StairsGameToken>())
        {
            Destroy(other.gameObject);
        }
    }
}
