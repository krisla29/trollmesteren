﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsGameKiller : MonoBehaviour
{
    public StairsGame stairsGame;
    public delegate void StairGameEvent();
    public static event StairGameEvent OnStairGameMatch;
    private Transform currentObject;

    public void OnTriggerEnter(Collider other)
    {
        print(other);

        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(other.gameObject);
        }

        if (!stairsGame.isFinished)
        {            
            if (stairsGame.currentNumber1 == stairsGame.resetNumber)
            {
                currentObject = other.transform;

                if (other.transform.GetComponent<StairsGameToken>())
                {
                    var tokenIndex = other.transform.GetComponent<StairsGameToken>().tokenIndex;

                    if (tokenIndex <= stairsGame.currentTask)
                    {
                        stairsGame.ChangeParticleColor(stairsGame.nextColor, false);
                        print("Current Number 1 is " + tokenIndex);
                        stairsGame.currentNumber1 = tokenIndex;
                    }
                    else
                    {
                        stairsGame.ChangeParticleColor(stairsGame.errorColor, true);
                        stairsGame.currentNumber1 = stairsGame.resetNumber;
                        stairsGame.currentNumber2 = stairsGame.resetNumber;
                        print("Failure, Reset");
                    }

                    Destroy(other.gameObject);
                }
            }
            if (!currentObject)
            {
                if ((stairsGame.currentNumber1 != stairsGame.resetNumber) && (stairsGame.currentNumber2 == stairsGame.resetNumber))
                {
                    if (other.transform.GetComponent<StairsGameToken>())
                    {
                        var tokenIndex = other.transform.GetComponent<StairsGameToken>().tokenIndex;

                        print("Current Number 2 is " + tokenIndex);
                        stairsGame.currentNumber2 = tokenIndex;

                        if (stairsGame.currentNumber1 + stairsGame.currentNumber2 == stairsGame.currentTask)
                        {
                            stairsGame.ChangeParticleColor(stairsGame.successColor, true);
                            print("Correct Answer");

                            if (OnStairGameMatch != null)
                            {
                                OnStairGameMatch();
                            }
                            stairsGame.currentNumber1 = stairsGame.resetNumber;
                            stairsGame.currentNumber2 = stairsGame.resetNumber;
                        }
                        else
                        {
                            stairsGame.ChangeParticleColor(stairsGame.errorColor, true);
                            stairsGame.currentNumber1 = stairsGame.resetNumber;
                            stairsGame.currentNumber2 = stairsGame.resetNumber;
                            print("Failure, Reset");
                        }

                        Destroy(other.gameObject);
                    }
                }
            }
        }

        currentObject = null;
        //Destroy(other.gameObject);
    }
}
