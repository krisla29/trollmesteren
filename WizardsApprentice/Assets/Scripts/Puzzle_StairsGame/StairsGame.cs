﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class StairsGame : MonoBehaviour
{
    public List<Transform> steps = new List<Transform>();
    public List<Transform> tokens = new List<Transform>();
    private List<GameObject> spawners;
    public Transform tokensParent;
    public GameObject tokenBirthEffect;
    public GameObject tokenKillEffect;
    public Canvas canvas;
    public Text canvasText;
    public ParticleSystem wellParticles;
    public AudioSource stonePush;
    public Color startColor;
    public Color nextColor;
    public Color successColor;
    public Color finishedColor;
    public Color errorColor;
    public Color currentColor;
    public float colorFadeTime = 2;
    public float stepMoveTime = 2;
    public Collider killer;
    //public int tokenIndex;
    public int currentTask;
    public int highestNumber;
    public int currentNumber1;
    public int currentNumber2;
    public int resetNumber = 999;
    public List <int> usedNumbers = new List<int>();
    public bool isFinished;
    public int matchCounter;
    public delegate void StairsGameMatch();
    public static event StairsGameMatch OnStairsGameCorrect;

    void Start()
    {
        for (int i = 0; i < tokens.Count; i++) // Creates tokenSpawners at location
        {
            var spawnerObject = new GameObject(tokens[i].name + "Spawner");
            spawnerObject.transform.parent = tokensParent; 
            //spawnerObject.transform.position = tokens[i].position;
            spawnerObject.transform.position = tokensParent.position + tokens[i].localPosition;
            var spawner = spawnerObject.AddComponent<Spawner>();
            spawner.waitForNewSpawn = 1;
            spawner.prefabToSpawn = tokens[i].gameObject;
            spawner.parent = transform;

            if (tokenBirthEffect)
            {
                spawner.birthEffect = tokenBirthEffect;
            }
        }

        highestNumber = (tokens.Count - 1) * 2;
        matchCounter = 0;
        currentTask = Random.Range(0, (highestNumber + 1));
        canvasText.text = currentTask.ToString();
        currentNumber1 = resetNumber;
        currentNumber2 = resetNumber;

        ChangeParticleColor(startColor, false);

        foreach (var item in steps) // Pushes steps in
        {
            item.position += - item.right * 2;
        }
    }
    
    private void OnEnable()
    {
        StairsGameKiller.OnStairGameMatch += Success;
    }
    private void OnDisable()
    {
        StairsGameKiller.OnStairGameMatch -= Success;
    }
    private void Success()
    {
        print("StairsGame Match Found!");

        currentNumber1 = resetNumber;
        currentNumber2 = resetNumber;

        if (currentTask != 0)
        {
            stonePush.Play();
            StartCoroutine("UnlockStep", steps[currentTask - 1]);
        }
        else
        {
            stonePush.Play();
            StartCoroutine("UnlockStep", steps[0]);
        }

        usedNumbers.Add(currentTask);
        matchCounter += 1;

        if (OnStairsGameCorrect != null)
        {
            OnStairsGameCorrect();
        }
        if(matchCounter == steps.Count)
        {
            isFinished = true;
        }
        else
        {
            var range = Enumerable.Range(0, (highestNumber + 1)).Where(i => !usedNumbers.Contains(i));
            var rand = new System.Random();
            int index = rand.Next(0, (highestNumber + 1) - usedNumbers.Count);
            currentTask = range.ElementAt(index);

            canvasText.text = currentTask.ToString();
        }
    }
    public void ChangeParticleColor(Color color, bool isFading)
    {   
        var col = wellParticles.colorOverLifetime;
        col.enabled = true;
        Gradient grad = new Gradient();
        grad.SetKeys(new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(0.5f, 0.5f), new GradientAlphaKey(0.0f, 1f) });
        
        col.color = grad;
        currentColor = color;
        
        if(isFading)
        {
            StartCoroutine("FadeParticleColor", color);
        }
    }

    private IEnumerator FadeParticleColor(Color color)
    {
        print("FADING COROUTINE");
        var col = wellParticles.colorOverLifetime;
        col.enabled = true;

        float elapsedTime = 0;
        Gradient currentGrad = new Gradient();

        while (elapsedTime < colorFadeTime)
        {
            currentColor = Color.Lerp(currentColor, startColor, (elapsedTime/ colorFadeTime*0.1f));
            currentGrad.SetKeys(new GradientColorKey[] { new GradientColorKey(currentColor, 0.0f), new GradientColorKey(currentColor, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(0.5f, 0.5f), new GradientAlphaKey(0.0f, 1f) });
            col.color = currentGrad;
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    private IEnumerator UnlockStep(Transform step)
    {
        print("MOVING COROUTINE");

        float elapsedTime = 0;
        var newPos = step.position + (step.right * 2);

        while(step.position.x < newPos.x)
        //while (elapsedTime < stepMoveTime)
        {
            step.position = Vector3.Lerp(step.position, step.position + (step.right * 2), (elapsedTime / stepMoveTime * 0.01f));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
