﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class HelperFunctions
{
    public static void SetNewIndex<T>(this List<T> list, int oldIndex, int newIndex) // Takes out item at oldIndex and places it at new index in list
    {
        T item = list[oldIndex];
        list.RemoveAt(oldIndex);
        list.Insert(newIndex, item);
    }

    public static void ShuffleList<T>(List<T> list) // Shuffles a List
    {
        for (int i = 0; i < list.Count; i++)
        {
            T temp = list[i];
            int randomIndex = Random.Range(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

    public static void SortListByClosestToTransform(List<Transform> listToCheck, Transform transformToCompare)
    {
        listToCheck.Sort(delegate (Transform a, Transform b)
        {
            return Vector3.Distance(transformToCompare.position, a.transform.position)
            .CompareTo(
              Vector3.Distance(transformToCompare.position, b.transform.position));
        });

        listToCheck.Reverse();
    }

    public static bool CompareList<T>(List<T> listA, List<T> listB) // Check if lists are Identical
    {
        if (listA.Count == listB.Count)
        {
            for (int i = 0; i < listA.Count; i++)
            {
                if (!listB.Contains(listA[i]))
                {
                    return false;
                }
                if (!listA.Contains(listB[i]))
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }

        return true;
    }
    public static bool ContainsAll<T>(List<T> listA, List<T> listB) // Check if listA contains all elements of listB
    {
        foreach (var item in listB.Distinct())
        {
            if (!listA.Contains(item))
            {
                return false;
            }
        }

        var listC = new List<T>();

        foreach (var item in listA)
        {
            listC.Add(item);
        }
        foreach (var item in listB)
        {
            if (!listC.Contains(item))
            {
                return false;
            }

            listC.Remove(item);
        }

        listC.Clear();

        return true;
    }

    public static void DrawPlane(Vector3 position, Vector3 normal)
    {

        Vector3 v3;

        if (normal.normalized != Vector3.forward)
            v3 = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
        else
            v3 = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude; ;

        var corner0 = position + v3;
        var corner2 = position - v3;
        var q = Quaternion.AngleAxis(90.0f, normal);
        v3 = q * v3;
        var corner1 = position + v3;
        var corner3 = position - v3;

        Debug.DrawLine(corner0, corner2, Color.green);
        Debug.DrawLine(corner1, corner3, Color.green);
        Debug.DrawLine(corner0, corner1, Color.green);
        Debug.DrawLine(corner1, corner2, Color.green);
        Debug.DrawLine(corner2, corner3, Color.green);
        Debug.DrawLine(corner3, corner0, Color.green);
        Debug.DrawRay(position, normal, Color.red);
    }

    public static void CounterParentScale(Transform child, Transform parent, float scale) //scales to uniform local size
    {
        parent = child.parent;
        child.localScale = new Vector3(scale / parent.transform.localScale.x, scale / parent.transform.localScale.y, scale / parent.transform.localScale.z);
    }

    public static Transform FindClosestItem (List<Transform> itemsToCheck, Transform objectToCompare)
    {
        var listA = new List<Transform>();

        foreach (var item in itemsToCheck)
        {
            listA.Add(item);
        }

        if (itemsToCheck.Count > 0 && objectToCompare != null && listA.Count > 0)
        {
            listA.Sort(delegate (Transform a, Transform b)
            {
                return (((objectToCompare.position - a.position).sqrMagnitude)
                .CompareTo(
                    (objectToCompare.position - b.position).sqrMagnitude));
            });

            return listA[0];
        }
        else
        {
            return null;
        }
    }

    public static List<Transform> SortToClosestItemOnAxis(List<Transform> itemsToCheck, Transform objectToCompare, Vector3 direction) //sorts a list  of transforms to closest on given axis to objectToCompare. Sorts list to closest if direction fails
    {
        var listA = new List<Transform>();

        foreach (var item in itemsToCheck)
        {
            listA.Add(item);
        }

        listA.Sort(delegate (Transform a, Transform b)
        {
            if (direction.x == 1)
            {
                return (((objectToCompare.position.x - a.position.x))
                .CompareTo(
                    (objectToCompare.position.x - b.position.x)));
            }
            else if (direction.y == 1)
            {
                return (((objectToCompare.position.y - a.position.y))
                .CompareTo(
                    (objectToCompare.position.y - b.position.y)));
            }
            else if (direction.z == 1)
            {
                return (((objectToCompare.position.z - a.position.z))
                .CompareTo(
                    (objectToCompare.position.z - b.position.z)));
            }
            else
            {
                return (((objectToCompare.position - a.position).sqrMagnitude)
            .CompareTo(
                (objectToCompare.position - b.position).sqrMagnitude));
            }
        });

        return listA;
    }

    public static void CounterParentScale2(Transform child, Transform parent, float scale, Vector3 sizeVector) // sizeVector3 = -1 == lessThan, 0 == Equals, 1 greaterThan
    {
        parent = child.parent;

        Vector3 newScale;

        if(parent.localScale.x == parent.localScale.y && parent.localScale.x == parent.localScale.z) // 1
            child.localScale = new Vector3(scale / parent.transform.localScale.x, scale / parent.transform.localScale.y, scale / parent.transform.localScale.z);
        /*
        if (parent.localScale.x < parent.localScale.y && parent.localScale.x == parent.localScale.z && parent.localScale.y == parent.localScale.z) // 2
            child.localScale = Vector3.zero;
        if (parent.localScale.x > parent.localScale.y && parent.localScale.x == parent.localScale.z && parent.localScale.y == parent.localScale.z) // 3
            child.localScale = Vector3.zero;
        if (parent.localScale.x == parent.localScale.y && parent.localScale.x < parent.localScale.z && parent.localScale.y == parent.localScale.z) // 4
            child.localScale = Vector3.zero;
        if (parent.localScale.x == parent.localScale.y && parent.localScale.x > parent.localScale.z && parent.localScale.y == parent.localScale.z) // 5
            child.localScale = Vector3.zero;
        if (parent.localScale.x == parent.localScale.y && parent.localScale.x == parent.localScale.z && parent.localScale.y < parent.localScale.z) // 6
            child.localScale = Vector3.zero;
        if (parent.localScale.x == parent.localScale.y && parent.localScale.x == parent.localScale.z && parent.localScale.y > parent.localScale.z) // 7
            child.localScale = Vector3.zero;
        if (parent.localScale.x < parent.localScale.y && parent.localScale.x < parent.localScale.z && parent.localScale.y > parent.localScale.z) // 8
            child.localScale = Vector3.zero;
        */
    }

    public static bool PointInOABB(Vector3 point, BoxCollider box) //Checks if point is within boxCollider
    {
        point = box.transform.InverseTransformPoint(point) - box.center;

        float halfX = (box.size.x * 0.5f);
        float halfY = (box.size.y * 0.5f);
        float halfZ = (box.size.z * 0.5f);
        if (point.x < halfX && point.x > -halfX &&
           point.y < halfY && point.y > -halfY &&
           point.z < halfZ && point.z > -halfZ)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool PointInOABB(Vector3 point, BoxCollider box, Transform transform) //Checks if point is within boxCollider within the space of transform
    {
        point = transform.InverseTransformPoint(point) - box.center;

        float halfX = (box.size.x * 0.5f);
        float halfY = (box.size.y * 0.5f);
        float halfZ = (box.size.z * 0.5f);
        if (point.x < halfX && point.x > -halfX &&
           point.y < halfY && point.y > -halfY &&
           point.z < halfZ && point.z > -halfZ)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static bool IsPointInsideMesh(Mesh aMesh, Vector3 aLocalPoint) // Checks if point is inside of mesh
    {
        var verts = aMesh.vertices;
        var tris = aMesh.triangles;
        int triangleCount = tris.Length / 3;
        for (int i = 0; i < triangleCount; i++)
        {
            var V1 = verts[tris[i * 3]];
            var V2 = verts[tris[i * 3 + 1]];
            var V3 = verts[tris[i * 3 + 2]];
            var P = new Plane(V1, V2, V3);
            if (P.GetSide(aLocalPoint))
                return false;
        }
        return true;
    }

    public static Vector3 RandomPointInBox(Transform transform) // returns a random point within a transform(box) //OldParam: Bounds bounds
    {
        var parentPos = transform.position;

        Vector3 pointWithinBox = new Vector3(
            Random.Range(-transform.localScale.x * 0.5f, transform.localScale.x * 0.5f),
            Random.Range(-transform.localScale.y * 0.5f, transform.localScale.y * 0.5f),
            Random.Range(-transform.localScale.z * 0.5f, transform.localScale.z * 0.5f)
        );

        pointWithinBox += parentPos;

        pointWithinBox = RotatePointAroundPivot(pointWithinBox, parentPos, transform.eulerAngles);
        
        return pointWithinBox;
    }

    public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) // Rotates a point in angles around other point
    {
        Vector3 dir = point - pivot; // get point direction relative to pivot
        dir = Quaternion.Euler(angles) * dir; // rotate it
        point = dir + pivot; // calculate rotated point
        return point; // return it
    }

    public static int RoundToList(int numberToRound, List<int> listOfNumbers)
    {
        //var list = new[] { 12, 15, 23, 94, 35, 48 };
        //var input = 17;

        var diffList = from number in listOfNumbers
                       select new
                       {
                           number,
                           difference = Mathf.Abs(number - numberToRound)
                       };
        var result = (from diffItem in diffList
                      orderby diffItem.difference
                      select diffItem).First().number;

        return result;
    }
    public static Color ModifyColor(Color colorToChange, float newHue, float newSaturation, float newValue)
    {
        float hue;
        float sat;
        float val;

        Color newColor = colorToChange;

        Color.RGBToHSV(newColor, out hue, out sat, out val);

        hue *= newHue;
        sat *= newSaturation;
        val *= newValue;
          
        newColor = Color.HSVToRGB(hue, sat, val);

        return newColor;
    }

    public static Vector3 FindCenterPoint(Vector3[] positions)
    {
        if (positions.Length == 0)
            return Vector3.zero;
        if (positions.Length == 1)
            return positions[0];

        var bounds = new Bounds(positions[0], Vector3.zero);

        for (var i = 1; i < positions.Length; i++)
            bounds.Encapsulate(positions[i]);

        return bounds.center;
    }
    /*    public static bool CompareLists<T>(List<T> listA, List<T> listB)
        {
            if (listA.Count == listB.count)
            {
                var newQuery = from li in listA where li == listB


            }
            else
            {
                return false;
            }
        }
    */

    }
