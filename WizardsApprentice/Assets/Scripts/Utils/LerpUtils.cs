﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LerpUtils
{
    public enum LerpType
    {
        Original,
        EaseOut,
        EaseIn,
        Exponential,
        SmoothStep,
        SmootherStep
    }

    public static float Original(float t) => t;
    public static float EaseOut(float t) => t * Mathf.PI * 0.5f;
    public static float EaseIn(float t) => 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
    public static float Exponential(float t) => t * t;
    public static float SmoothStep(float t) => t * t * (3f - 2f * t);
    public static float SmootherStep(float t) => t * t * t * (t * (6f * t - 15f) + 10f);

    public static float GetLerpType(LerpType type, float t)
    {
        switch (type)
        {
            case LerpType.Original:
                return Original(t);
            case LerpType.EaseOut:
                return EaseOut(t);
            case LerpType.EaseIn:
                return EaseIn(t);
            case LerpType.Exponential:
                return Exponential(t);
            case LerpType.SmoothStep:
                return SmoothStep(t);
            case LerpType.SmootherStep:
                return SmootherStep(t);
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }
}
