﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoor : MonoBehaviour
{
    public GameManager gameManager;
    public Camera cam;
    public SpriteRenderer wandHitEffect;
    public LineRenderer lineRenderer1;
    public LineRenderer lineRenderer2;
    public bool useWorldPosition;
    public BossDoor_Door door1;
    public BossDoor_Door door2;
    public SpriteRenderer door1Sprite;
    public SpriteRenderer door2Sprite;
    public Color doorsEndColor;
    public Transform locksParent;
    public Transform particles;
    public Transform glow;
    public WandUI wandUI;
    public float rotationTime = 6f;
    public Collider sceneCollider;
    public Transform finishPoint;
    public Transform rayBlocker;
    public List<BossDoorTaskObject> taskObjects = new List<BossDoorTaskObject>();
    public List<BossDoorTaskObject> currentActiveObjects;
    public BossDoorTaskObject startObject;
    public Transform pointer;

    private bool correct;
    private bool hasClicked;
    private bool wait;
    public bool gameComplete;
    private Ray ray;
    private RaycastHit hit;
    private Vector3 comparePoint;
    private Vector3[] pointPositions1;
    private Vector3[] pointPositions2;
    public float rayDistance = 7;
    public float compareRadius = 0.5f;

    private List<Coroutine> responseAnims = new List<Coroutine>();
    private List<Vector3> startScales = new List<Vector3>();

    public AudioSource soundHitObject;
    public AudioSource soundRay;
    public AudioSource soundTaskComplete;
    public AudioSource soundTaskFail;
    public AudioSource soundDoorOpen;
    public AudioSource soundUnlock;
    public AudioSource soundEarthquake;
    public AudioSource soundMagicMystery;
    public AudioSource soundMagicTwinkle;

    private void Awake()
    {
        for (int i = 0; i < taskObjects.Count; i++)
        {           
            responseAnims.Add(null);
            startScales.Add(taskObjects[i].transform.localScale);
        }
    }

    private void OnEnable()
    {
        Initialize();
        rayBlocker.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        wandHitEffect.gameObject.SetActive(false);
        gameManager.player.toggleAimThirdPerson = false;
        gameManager.player.wandTarget = Vector3.zero;
        StopAllCoroutines();

        if (gameComplete)
            sceneCollider.gameObject.SetActive(false);

        rayBlocker.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void Update()
    {
        CheckPointer();
    }

    void CheckPointer()
    {
        if (!gameComplete)
        {
            if (!startObject.isActive)
            {
                if (!pointer.gameObject.activeSelf)
                    pointer.gameObject.SetActive(true);
            }
            else
            {
                if (pointer.gameObject.activeSelf)
                    pointer.gameObject.SetActive(false);
            }
        }
    }

    private void OnMouseDown()
    {
        if (!wait)
            MouseDown();
    }

    private void OnMouseDrag()
    {        
        if (hasClicked && !wait)
        {
            if (!correct)
            {
                CheckRayTask();
                UpdateLineRenderer1();
                gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
            }

            UpdateLineRenderer2();
            RotatePlayerWand();
        }
    }

    private void OnMouseUp()
    {
        if (!wait)
            MouseUp();

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
    }

    void CheckRayTask()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 20f))
        {
            if (hit.transform == transform)
            {
                comparePoint = hit.point;
                Debug.DrawLine(ray.origin, ray.origin + (ray.direction * rayDistance), Color.red);

                for (int i = 0; i < taskObjects.Count; i++)
                {
                    if ((taskObjects[i].transform.position - comparePoint).sqrMagnitude < compareRadius * compareRadius)
                    {
                        if (!taskObjects[i].isActive)
                        {
                            currentActiveObjects.Add(taskObjects[i]);
                            taskObjects[i].isActive = true;

                            if (responseAnims[i] != null)
                                StopCoroutine(responseAnims[i]);

                            responseAnims[i] = StartCoroutine(ResponseAnim(i));
                            //StartCoroutine(PinGhost(i));

                            if (soundHitObject)
                                soundHitObject.Play();
                        }
                    }
                }
            }
        }

        if (currentActiveObjects.Count >= taskObjects.Count)
        {
            MouseUp();
        }
    }

    void MouseDown()
    {
        gameManager.player.toggleAimThirdPerson = true;
        gameManager.player.wandTarget = hit.point;

        if (soundRay)
            soundRay.Play();

        StartLineRenderer2();
        hasClicked = true;
    }

    private void MouseUp()
    {
        wandHitEffect.gameObject.SetActive(false);
        gameManager.player.toggleAimThirdPerson = false;
        gameManager.player.wandTarget = Vector3.zero;
        //StartCoroutine(RotatePlayer());
        ResetLineRenderer2();

        if (soundRay)
            soundRay.Stop();

        if (!correct)
        {
            print("Hello");
            correct = CheckAnswer();

            if (correct)
            {               
                gameComplete = true;
                StartCoroutine(Success());
                //StartCoroutine(SpellSuccessful());
            }
            else
            {
                if (soundTaskFail)
                    soundTaskFail.Play();

                ResetCurrentTask();
            }
        }

        hasClicked = false;
    }

    void ResetCurrentTask()
    {
        foreach (var taskObject in taskObjects)
        {
            taskObject.isActive = false;
        }

        currentActiveObjects.Clear();

        StartLineRenderer1();
    }

    bool CheckAnswer()
    {
        bool check = true;

        if (taskObjects.Count == currentActiveObjects.Count)
        {
            for (int i = currentActiveObjects.Count - 1; i > -1; i--)
            {
                if (i > 0)
                {
                    if (currentActiveObjects[i].value < currentActiveObjects[i - 1].value)
                    {
                        check = false;
                    }
                }
            }
        }
        else
        {
            check = false;
        }

        if (check)
        {
            print("Success!!");
            return true;
        }
        else
        {
            print("Fail!!");
            return false;
        }
    }

    void ResetLineRenderer1()
    {
        lineRenderer1.positionCount = 0;
        pointPositions1 = new Vector3[0];
        lineRenderer1.SetPositions(pointPositions1);
        lineRenderer1.enabled = false;
    }

    void ResetLineRenderer2()
    {
        lineRenderer2.positionCount = 0;
        pointPositions2 = new Vector3[0];
        lineRenderer2.SetPositions(pointPositions2);
        lineRenderer2.enabled = false;
        wandHitEffect.gameObject.SetActive(false);
    }

    void StartLineRenderer1()
    {
        lineRenderer1.positionCount = 0;
        pointPositions1 = new Vector3[0];
        lineRenderer1.SetPositions(pointPositions1);
        lineRenderer1.enabled = true;

        //lineRenderer1.loop = true;

    }

    void StartLineRenderer2()
    {
        lineRenderer2.positionCount = 0;
        pointPositions2 = new Vector3[0];
        lineRenderer2.SetPositions(pointPositions2);
        lineRenderer2.enabled = true;
        wandHitEffect.gameObject.SetActive(true);
    }

    void UpdateLineRenderer1()
    {
        lineRenderer1.positionCount = currentActiveObjects.Count + 1;
        pointPositions1 = new Vector3[currentActiveObjects.Count + 1];

        for (int i = 0; i < currentActiveObjects.Count; i++)
        {
            if (i < currentActiveObjects.Count)
            {
                if (currentActiveObjects[i])
                {
                    if(!useWorldPosition)
                        pointPositions1[i] = currentActiveObjects[i].transform.localPosition;
                    else
                        pointPositions1[i] = currentActiveObjects[i].transform.position;
                }
            }
        }

        if(!useWorldPosition)
            pointPositions1[pointPositions1.Length - 1] = transform.InverseTransformPoint(comparePoint);
        else
            pointPositions1[pointPositions1.Length - 1] = comparePoint;

        lineRenderer1.SetPositions(pointPositions1);

        //lineRenderer1.loop = true;
    }

    void UpdateLineRenderer1End()
    {
        print("Current Active objects count is ... " + (currentActiveObjects.Count));
        lineRenderer1.positionCount = currentActiveObjects.Count;
        pointPositions1 = new Vector3[currentActiveObjects.Count];

        for (int i = 0; i < currentActiveObjects.Count; i++)
        {
            if (i < currentActiveObjects.Count)
            {
                if (currentActiveObjects[i])
                {
                    if(!useWorldPosition)
                        pointPositions1[i] = currentActiveObjects[i].transform.localPosition;
                    else
                        pointPositions1[i] = currentActiveObjects[i].transform.position;
                }
            }
        }

        lineRenderer1.SetPositions(pointPositions1);        
        /*
        if (pointPositions1.Length > 2)
            lineRenderer1.loop = true;
        else
            lineRenderer1.loop = false;
        */
    }
    
    void UpdateLineRenderer2()
    {
        lineRenderer2.positionCount = 3;
        pointPositions2 = new Vector3[3];

        pointPositions2[0] = gameManager.player.wandObject.star.position;
        pointPositions2[1] = gameManager.player.wandObject.star.position;
        pointPositions2[2] = comparePoint;

        lineRenderer2.SetPositions(pointPositions2);

        wandHitEffect.transform.position = comparePoint;
        /*
        if (pointPositions1.Length > 2)
            lineRenderer1.loop = true;
        else
            lineRenderer1.loop = false;
        */
    }

    void RotatePlayerWand()
    {
        Vector3 lookVector = new Vector3(ray.direction.x, 0, ray.direction.z);
        Quaternion look = Quaternion.LookRotation(lookVector);
        gameManager.player.transform.localRotation = look;
    }

    void Initialize()
    {
        ResetBools();
        ResetLineRenderer1();
        StartLineRenderer1();
        ResetLineRenderer2();
        SetToStartScale();
    }

    void ResetBools()
    {
        correct = false;
        hasClicked = false;
    }

    void SetToStartScale()
    {
        for (int i = 0; i < taskObjects.Count; i++)
        {
            taskObjects[i].transform.localScale = startScales[i];
        }
    }

    private IEnumerator Success()
    {
        wait = true;
        hasClicked = true;
        rayBlocker.gameObject.SetActive(true);
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        wandUI.wandActive.gameObject.SetActive(false);
        wandUI.wandDeActive.gameObject.SetActive(true);
        //MouseUp();
        UpdateLineRenderer1End();
        glow.gameObject.SetActive(true);


        float t1 = 0;
        float animTime1 = 3;
        float sWidth = lineRenderer1.startWidth;
        float eWidth = lineRenderer1.endWidth;
        Color cd1 = door1Sprite.color;
        Color cd2 = door2Sprite.color;

        if (soundMagicMystery)
            soundMagicMystery.Play();        

        while (t1 < 1)
        {
            t1 += Time.deltaTime / animTime1;
            var h = LerpUtils.SmootherStep(t1);
            UpdateLineRenderer1End();
            lineRenderer1.startWidth = Mathf.Lerp(sWidth, 0.4f, t1);
            lineRenderer1.endWidth = Mathf.Lerp(eWidth, 0.4f, t1);
            door1Sprite.color = Color.Lerp(cd1, doorsEndColor, t1);
            door2Sprite.color = Color.Lerp(cd2, doorsEndColor, t1);

            for (int i = 0; i < taskObjects.Count; i++)
            {
                taskObjects[i].transform.localScale = Vector3.Lerp(startScales[i], Vector3.zero, h);
            }

            yield return new WaitForEndOfFrame();
        }

        particles.gameObject.SetActive(true);
       
        taskObjects[0].transform.parent.gameObject.SetActive(false);
        gameManager.player.anim.SetTrigger("Success");

        if (soundTaskComplete)
            soundTaskComplete.Play();

        yield return new WaitForSeconds(1);

        if (soundEarthquake)
            soundEarthquake.Play();

        float t2 = 0;
        float animTime2 = 2;
        float sWidth2 = lineRenderer1.startWidth;
        float eWidth2 = lineRenderer1.endWidth;
        

        while (t2 < 1)
        {
            t2 += Time.deltaTime / animTime2;
            UpdateLineRenderer1End();
            lineRenderer1.startWidth = Mathf.Lerp(sWidth2, sWidth, t2);
            lineRenderer1.endWidth = Mathf.Lerp(eWidth2, eWidth, t2);           
            yield return new WaitForEndOfFrame();
        }

        float t3 = 0;
        float animTime3 = 2f;
        //Gradient curCol = lineRenderer1.colorGradient;
        float sWidth3 = lineRenderer1.startWidth;
        float eWidth3 = lineRenderer1.endWidth;
        Color cd1_2 = door1Sprite.color;
        Color cd2_2 = door2Sprite.color;

        while (t3 < 1)
        {
            t3 += Time.deltaTime / animTime3;
            var h = LerpUtils.SmootherStep(t3);
            lineRenderer1.startWidth = Mathf.Lerp(sWidth3, 0, h);
            lineRenderer1.endWidth = Mathf.Lerp(eWidth3, 0, h);
            door1Sprite.color = Color.Lerp(cd1_2, cd1, t3);
            door2Sprite.color = Color.Lerp(cd2_2, cd2, t3);
            /*
            for (int i = 0; i < lineRenderer1.colorGradient.alphaKeys.Length; i++)            
            {
                lineRenderer1.colorGradient.alphaKeys[i].alpha = Mathf.Lerp(255f, 0f, h);
            }
            */
            yield return new WaitForEndOfFrame();
        }

        for (int i = locksParent.childCount - 1; i > -1; i--)        
        {
            float waitTime = Random.Range(0.1f, 0.2f);

            yield return new WaitForSeconds(waitTime);

            if (soundUnlock)
                soundUnlock.Play();

            locksParent.GetChild(i).gameObject.SetActive(false);
        }

        lineRenderer1.gameObject.SetActive(false);
        lineRenderer2.gameObject.SetActive(false);
       
        yield return new WaitForSeconds(1f);       

        if (soundDoorOpen)
            soundDoorOpen.Play();

        if (soundMagicTwinkle)
            soundMagicTwinkle.Play();

        if (soundTaskComplete)
            soundTaskComplete.Play();

        door1.StartRotation(rotationTime);
        door2.StartRotation(rotationTime);

        yield return new WaitForSeconds(rotationTime);

        particles.gameObject.SetActive(false);
        gameManager.playerStats.hasOpenedBossDoor = true;
        gameManager.playerStats.SaveGame();

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        //gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
        gameManager.player.navMeshAgent.SetDestination(finishPoint.position);
        /*
        float t3 = 0;
        float animTime3 = 3f;
        var curPos = sceneCollider.transform.position;

        while (t3 < 1)
        {
            t3 += Time.deltaTime / animTime3;
            sceneCollider.transform.position = Vector3.Lerp(curPos, Vector3.zero, t3);
            yield return new WaitForEndOfFrame();
        }
        */
        //sceneCollider.gameObject.SetActive(false);
        //gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();

        //sceneCollider.gameObject.SetActive(false);
    }

    private IEnumerator ResponseAnim(int passedIndex)
    {
        float t = 0;
        float animTime = 0.2f;
        Vector3 startScale = startScales[passedIndex];

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = Mathf.Sin(t * Mathf.PI);
            h *= 0.1f;

            if(startScale.z > 0)
                taskObjects[passedIndex].transform.localScale = startScale + (Vector3.one * h);
            else
                taskObjects[passedIndex].transform.localScale = startScale + (new Vector3(1,1-1) * h);

            yield return new WaitForEndOfFrame();
        }

        taskObjects[passedIndex].transform.localScale = startScales[passedIndex];
    }
}
