﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoor_Door : MonoBehaviour
{
    public float angle;
    private Coroutine rotation;

    public void StartRotation(float rotationTime)
    {
        if(rotation != null)
            StopCoroutine(rotation);

        rotation = StartCoroutine(RotateDoor(rotationTime));
    }

    private IEnumerator RotateDoor(float rotationTime)
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / rotationTime;
            var h = LerpUtils.SmootherStep(t);
            var curAngle = h * angle;
            transform.localEulerAngles = new Vector3(0, curAngle, 0);
            yield return new WaitForEndOfFrame();
        }
    }
}
