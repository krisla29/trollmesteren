﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorManager : MonoBehaviour
{
    public GameManager gameManager;
    public BossDoor bossDoorScript;
    public Collider sceneCollider;
    public SceneController sceneController;
    /*
    void Start()
    {
        if (gameManager.playerStats.hasCompletedWizard)
        {
            sceneCollider.gameObject.SetActive(true);
        }
        else
        {
            sceneCollider.gameObject.SetActive(false);
        }
    }
    */
    private void OnEnable()
    {
        GameManager.OnSwitchScene += CheckSceneCollider;
    }

    private void OnDisable()
    {
        GameManager.OnSwitchScene -= CheckSceneCollider;
    }

    void CheckSceneCollider(SceneController passedController)
    {
        if(passedController != sceneController)
        {
            if(bossDoorScript.gameComplete)
            {
                DisableCollider();
            }
        }
    }

    void DisableCollider()
    {
        sceneCollider.enabled = false;
        sceneCollider.gameObject.SetActive(false);
    }

    public void InitializeBossDoor() //Called by PlayerStats
    {
        bossDoorScript.gameComplete = gameManager.playerStats.hasOpenedBossDoor;

        if(bossDoorScript.gameComplete)
        {
            DisableCollider();
            bossDoorScript.door1.transform.localEulerAngles = new Vector3(0, bossDoorScript.door1.angle, 0);
            bossDoorScript.door2.transform.localEulerAngles = new Vector3(0, bossDoorScript.door2.angle, 0);
            bossDoorScript.gameObject.SetActive(false);
        }
        else
        {
            if (gameManager.playerStats.hasCompletedWizard)
            {
                sceneCollider.gameObject.SetActive(true);
            }
            else
            {
                sceneCollider.gameObject.SetActive(false);
            }
        }
    }
}
