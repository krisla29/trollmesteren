﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReference : MonoBehaviour
{
    public Transform player;

    private void OnEnable()
    {
        SpawnPlayer.OnSpawnNewPlayer += ReferncePlayer;
    }

    private void OnDisable()
    {
        SpawnPlayer.OnSpawnNewPlayer -= ReferncePlayer;
    }

    void ReferncePlayer(Transform newPlayer)
    {
        player = newPlayer;
    }
}
