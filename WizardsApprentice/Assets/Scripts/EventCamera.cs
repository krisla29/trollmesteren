﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCamera : MonoBehaviour
{
    public enum CameraMoves { Linear, Smooth, Stepped, EaseIn, EaseOut }
    public CameraMoves cameraMove;
    public enum CameraEnds { StartEndPos, Time, Condition}
    public CameraEnds cameraEnd;

    public Transform eventCamParent;

    public Vector3 camStartPos;
    public Vector3 camEndPos;
    public Vector3 parentStartPos;
    public Vector3 parentEndPos;
    public Quaternion camStartRot;
    public Quaternion camEndRot;
    public Quaternion parentStartRot;
    public Quaternion parentEndRot;

    void Start()
    {
        //transform.gameObject.SetActive(false);
    }

    void Update()
    {
        
    }
}
