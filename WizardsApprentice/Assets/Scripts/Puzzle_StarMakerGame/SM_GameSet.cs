﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SM_GameSet : MonoBehaviour
{
    public StarMaker.GameSetModes gameSetMode = StarMaker.GameSetModes.Any;
    public bool lineIsClosed;
    public bool showFigure;
    public bool canAddPoints = true;
    public bool canMovePoints = true;
    public bool orderMatters = true;
    public Vector2Int origo = Vector2Int.zero;
    public Vector2Int gridSize;
    public int gridInterval = 1;
    public List<Vector2Int> startPoints = new List<Vector2Int>();
    public List<Vector2Int> correctList = new List<Vector2Int>();
}
