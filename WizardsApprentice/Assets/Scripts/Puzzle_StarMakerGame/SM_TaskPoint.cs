﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SM_TaskPoint : MonoBehaviour
{
    public int index;
    public Vector2Int coordinate;
    public TextMeshPro indexText;
    public SpriteRenderer sprite;
    public SpriteRenderer glow;
}
