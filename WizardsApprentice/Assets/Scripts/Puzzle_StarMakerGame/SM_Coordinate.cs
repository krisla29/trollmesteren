﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SM_Coordinate : MonoBehaviour
{
    public int coordinateIndex;
    public Vector2Int coordinate;
    public StarMakerCanvas starMakerCanvas;
    public Text coordinateText;
    public Text coordinateIndexText;
    public Image coordinateImage;
    public Image coordinateGlow;
    public Button coordinateBtn;

    public void SelectCoordinate()
    {
        starMakerCanvas.starMakerScript.SetActivePointFromCoordinate(coordinateIndex);
        starMakerCanvas.currentUICordinate = this;
        starMakerCanvas.starMakerScript.AddPointButton.gameObject.SetActive(false);

        if(starMakerCanvas.starMakerScript.currentGameSet.canAddPoints)
            starMakerCanvas.starMakerScript.deletePointButton.gameObject.SetActive(true);
    }
}
