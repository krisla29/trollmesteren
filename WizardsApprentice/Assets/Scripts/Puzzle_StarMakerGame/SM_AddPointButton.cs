﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SM_AddPointButton : MonoBehaviour
{
    public StarMaker starMakerScript;
    public StarMakerCanvas starMakerCanvas;

    public void AddNewPoint()
    {
        if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Any)
        {
            if (starMakerScript.activeGridPoint && !starMakerScript.currentPoint)
                starMakerCanvas.starMakerScript.AddNewPointFromGrid();

            else if (!starMakerScript.activeGridPoint && !starMakerScript.currentPoint)
                starMakerCanvas.starMakerScript.AddNewPointFromSliders();
            else
            {
                if (starMakerScript.soundError)
                    starMakerScript.soundError.Play();
            }
        }

        else if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Plot)
        {
            if (!starMakerScript.activeGridPoint && !starMakerScript.currentPoint)
                starMakerCanvas.starMakerScript.AddNewPointFromSliders();
            else
            {
                if (starMakerScript.soundError)
                    starMakerScript.soundError.Play();
            }
        }

        else if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Drag)
        {
            if (starMakerScript.activeGridPoint && !starMakerScript.currentPoint)
                starMakerCanvas.starMakerScript.AddNewPointFromGrid();
            else if (!starMakerScript.activeGridPoint && !starMakerScript.currentPoint)
                starMakerCanvas.starMakerScript.AddNewPointFromSliders();
            else
            {
                if (starMakerScript.soundError)
                    starMakerScript.soundError.Play();
            }
        }
    }
}
