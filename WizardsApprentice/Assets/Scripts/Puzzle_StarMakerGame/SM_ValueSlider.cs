﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SM_ValueSlider : MonoBehaviour
{
    public bool isLocked;
    public StarMaker starMakerScript;
    public enum ValueType { X, Y}
    public ValueType valueType;
    public float cellSize = 128f;
    public Image valuesImage;
    //public Text valueText;
    public RectTransform rectTransform;
    public Button button;
    public List<Vector3> positions = new List<Vector3>();    
}
