﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarMakerCanvas : MonoBehaviour
{
    public StarMaker starMakerScript;
    public GameObject UICoordinatePrefab;
    public List<RectTransform> UIcoordinateSlots = new List<RectTransform>();
    public List<SM_Coordinate> UIcoordinates = new List<SM_Coordinate>();
    public SM_Coordinate currentUICordinate;
    public bool pauseUpdate;
    public Color imageColor = Color.yellow;
    public Color imageSelectColor = Color.white;
    public Color imageLockedColor = Color.grey;
    public Color textColor = Color.black;
    public Color textSelectColor = Color.green;
    public Color textLockedColor = Color.black;
    public Color imageOrderedColor = Color.blue;

    public RectTransform trophyPanel;
    public RectTransform trophy;

    public RectTransform coordinatesPanel;
    public RectTransform cp_editTheme;
    public RectTransform cp_lockTheme;
    public EasyScalePulse cp_pulser;
    public RectTransform coordinateSlidersPanel;
    public RectTransform cs_editTheme;
    public RectTransform cs_lockTheme;
    public EasyScalePulse cs_pulser;
    public List<Transform> cs_objectsToLock;
    public bool cs_editMode;
    public RectTransform addButtonsPanel;
    public RectTransform canMovePanel;
    public RectTransform cm_editTheme;
    public RectTransform cm_lockTheme;

    public delegate void StarMakerCanvasEvent(); //Sends To StartMakerScript
    public static event StarMakerCanvasEvent OnTaskComplete;

    
    public void AddFirstCoordinate() //Called by StarMaker script // not in use?
    {
        var newCoord = Instantiate(UICoordinatePrefab, UIcoordinateSlots[0]);
        UIcoordinates.Add(newCoord.GetComponent<SM_Coordinate>());
    }       

    public void ClearCoordinates()
    {
        foreach (var item in UIcoordinates)
        {
            Destroy(item.gameObject);
        }

        UIcoordinates.Clear();
    }

    public void CreateNewTask()
    {
        var list = starMakerScript.currentGameSet.correctList;

        for (int i = 0; i < list.Count; i++)
        {
            var newCoord = Instantiate(UICoordinatePrefab, UIcoordinateSlots[i]);
            var newCoordScript = newCoord.GetComponent<SM_Coordinate>();
            newCoordScript.coordinate = list[i];/////////
            print(newCoordScript.coordinate);//////////
            newCoordScript.coordinateBtn.enabled = false;
            UIcoordinates.Add(newCoordScript);
            UIcoordinates[i].coordinateText.text = " " + list[i].x + " , " + list[i].y + " ";
            newCoordScript.coordinateIndex = i;
            UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);

            if (starMakerScript.currentGameSet.orderMatters)
            {
                UIcoordinates[i].coordinateImage.color = imageLockedColor - (Color.white * (i / 20f));
                UIcoordinates[i].coordinateIndexText.gameObject.SetActive(true);
                UIcoordinates[i].coordinateIndexText.text = (i + 1).ToString();
            }
            else
            {
                UIcoordinates[i].coordinateImage.color = imageLockedColor;
                UIcoordinates[i].coordinateIndexText.gameObject.SetActive(false);
            }

            UIcoordinates[i].coordinateText.color = textLockedColor;
            newCoordScript.starMakerCanvas = this;
        }
    }

    public void UpdateCoordinatesAnyOrdered()
    {
        if (UIcoordinates.Count < starMakerScript.currentCoordinates.Count && UIcoordinates.Count < UIcoordinateSlots.Count)
        {
            var newCoord = Instantiate(UICoordinatePrefab, UIcoordinateSlots[UIcoordinates.Count]);
            var newCoordScript = newCoord.GetComponent<SM_Coordinate>();
            UIcoordinates.Add(newCoordScript);
            newCoordScript.coordinateIndex = UIcoordinates.Count - 1;
            newCoordScript.starMakerCanvas = this;
            newCoordScript.coordinateIndexText.gameObject.SetActive(true);
            newCoordScript.coordinateIndexText.text = (newCoordScript.coordinateIndex + 1).ToString();
        }
        else if (starMakerScript.currentCoordinates.Count < UIcoordinates.Count)
        {
            var tempCoord = UIcoordinates[UIcoordinates.Count - 1].transform.gameObject;
            UIcoordinates.Remove(UIcoordinates[UIcoordinates.Count - 1]); //Checking List.Remove()
            Destroy(tempCoord);
        }

        for (int i = 0; i < starMakerScript.currentCoordinates.Count; i++)
        {
            if (UIcoordinates.Count == starMakerScript.currentCoordinates.Count)
            {
                UIcoordinates[i].coordinate = new Vector2Int(starMakerScript.currentCoordinates[i].x, starMakerScript.currentCoordinates[i].y);
                UIcoordinates[i].coordinateText.text = " " + UIcoordinates[i].coordinate.x + " , " + UIcoordinates[i].coordinate.y + " ";

                if (i == starMakerScript.currentPoints.IndexOf(starMakerScript.currentPoint))
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(true);                        
                        UIcoordinates[i].coordinateImage.color = imageSelectColor;
                        UIcoordinates[i].coordinateText.color = textSelectColor;
                    }
                }
                else
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);                       
                        UIcoordinates[i].coordinateImage.color = imageColor - (Color.white * (i / 20f));
                        UIcoordinates[i].coordinateText.color = textColor;
                    }
                }
            }
        }

        bool check = true;

        for (int i = 0; i < UIcoordinates.Count; i++)
        {
            if (starMakerScript.currentCoordinates.Count - 1 >= i)
            {
                if (starMakerScript.currentGameSet.correctList.Count - 1 >= i)
                {
                    if (UIcoordinates[i].coordinate != starMakerScript.currentGameSet.correctList[i]) ////!!!!!!!!!
                    {
                        if (starMakerScript.currentTaskPoints.Count - 1 >= i)
                        {                            
                            starMakerScript.currentTaskPoints[i].glow.gameObject.SetActive(false);
                        }

                        check = false;
                    }
                    else
                    {
                        if (starMakerScript.currentTaskPoints.Count - 1 >= i)
                        {
                            if (!starMakerScript.currentTaskPoints[i].glow.gameObject.activeSelf)
                            {
                                if (starMakerScript.soundCorrect)
                                    starMakerScript.soundCorrect.Play();
                            }

                            starMakerScript.currentTaskPoints[i].glow.gameObject.SetActive(true);
                        }
                    }
                }
                else
                {
                    if (starMakerScript.currentTaskPoints.Count - 1 >= i)
                    {
                        starMakerScript.currentTaskPoints[i].glow.gameObject.SetActive(false);
                    }

                    check = false;
                }
            } 
            else
            {
                check = false;
            }
        }

        if (check && UIcoordinates.Count == starMakerScript.currentGameSet.correctList.Count)
        {
            if (OnTaskComplete != null)
            {
                OnTaskComplete();
            }

            pauseUpdate = true;
        }    
    }

    public void UpdateCoordinatesPlotOrdered()
    {
        if (UIcoordinates.Count < starMakerScript.currentCoordinates.Count && UIcoordinates.Count < UIcoordinateSlots.Count)
        {
            var newCoord = Instantiate(UICoordinatePrefab, UIcoordinateSlots[UIcoordinates.Count]);
            var newCoordScript = newCoord.GetComponent<SM_Coordinate>();
            UIcoordinates.Add(newCoordScript);
            newCoordScript.coordinateIndex = UIcoordinates.Count - 1;
            newCoordScript.starMakerCanvas = this;
            newCoordScript.coordinateIndexText.gameObject.SetActive(true);
            newCoordScript.coordinateIndexText.text = (newCoordScript.coordinateIndex + 1).ToString();
        }
        else if (starMakerScript.currentCoordinates.Count < UIcoordinates.Count)
        {
            var tempCoord = UIcoordinates[UIcoordinates.Count - 1].transform.gameObject;
            UIcoordinates.Remove(UIcoordinates[UIcoordinates.Count - 1]); //Checking List.Remove()
            Destroy(tempCoord);
        }

        for (int i = 0; i < starMakerScript.currentCoordinates.Count; i++)
        {
            if (UIcoordinates.Count == starMakerScript.currentCoordinates.Count)
            {
                UIcoordinates[i].coordinate = new Vector2Int(starMakerScript.currentCoordinates[i].x, starMakerScript.currentCoordinates[i].y);
                UIcoordinates[i].coordinateText.text = " " + UIcoordinates[i].coordinate.x + " , " + UIcoordinates[i].coordinate.y + " ";

                if (i == starMakerScript.currentPoints.IndexOf(starMakerScript.currentPoint))
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(true);
                        UIcoordinates[i].coordinateImage.color = imageSelectColor;
                        UIcoordinates[i].coordinateText.color = textSelectColor;
                    }
                }
                else
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);
                        UIcoordinates[i].coordinateImage.color = imageColor - (Color.white * (i / 20f));
                        UIcoordinates[i].coordinateText.color = textColor;
                    }
                }
            }
        }

        bool check = true;

        for (int i = 0; i < UIcoordinates.Count; i++)
        {
            if (starMakerScript.currentCoordinates.Count - 1 >= i)
            {
                if (starMakerScript.currentGameSet.correctList.Count - 1 >= i)
                {
                    if (UIcoordinates[i].coordinate != starMakerScript.currentGameSet.correctList[i])
                    {
                        if (starMakerScript.currentTaskPoints.Count - 1 >= i)
                        {
                            starMakerScript.currentTaskPoints[i].glow.gameObject.SetActive(false);
                        }

                        check = false;
                    }
                    else
                    {
                        if (starMakerScript.currentTaskPoints.Count - 1 >= i)
                        {
                            if (!starMakerScript.currentTaskPoints[i].glow.gameObject.activeSelf)
                            {
                                if (starMakerScript.soundCorrect)
                                    starMakerScript.soundCorrect.Play();
                            }

                            starMakerScript.currentTaskPoints[i].glow.gameObject.SetActive(true);
                        }
                    }
                }
                else
                {
                    if (starMakerScript.currentTaskPoints.Count - 1 >= i)
                    {
                        starMakerScript.currentTaskPoints[i].glow.gameObject.SetActive(false);
                    }

                    check = false;
                }
            }
            else
            {
                check = false;
            }
        }

        if (check && UIcoordinates.Count == starMakerScript.currentGameSet.correctList.Count)
        {
            if (OnTaskComplete != null)
            {
                OnTaskComplete();
            }

            pauseUpdate = true;
        }

    }

    public void UpdateCoordinatesDragOrdered()
    {
        bool check = true;

        for (int i = 0; i < starMakerScript.currentGameSet.correctList.Count; i++)
        {
            if (starMakerScript.currentCoordinates.Count - 1 >= i)
            {
                if (starMakerScript.currentCoordinates[i] == starMakerScript.currentGameSet.correctList[i])
                {
                    UIcoordinates[i].coordinateGlow.gameObject.SetActive(true);
                    UIcoordinates[i].coordinateImage.color = imageSelectColor;
                    UIcoordinates[i].coordinateText.color = textSelectColor;

                    if (!starMakerScript.currentPoints[i].glow2.gameObject.activeSelf)
                    {
                        if (starMakerScript.soundCorrect)
                            starMakerScript.soundCorrect.Play();
                    }

                    starMakerScript.currentPoints[i].glow2.gameObject.SetActive(true);
                }
                else
                {
                    UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);
                    UIcoordinates[i].coordinateImage.color = imageLockedColor - (Color.white * (i / 20f));
                    UIcoordinates[i].coordinateText.color = textLockedColor;
                    starMakerScript.currentPoints[i].glow2.gameObject.SetActive(false);
                    check = false;
                }
            }            
        }

        if (check && starMakerScript.currentCoordinates.Count == starMakerScript.currentGameSet.correctList.Count)
        {
            if (OnTaskComplete != null)
            {
                OnTaskComplete();
            }

            pauseUpdate = true;
        }
    }

    private void Update()
    {
        if (!pauseUpdate)
        {
            if (starMakerScript.currentGameSet)
            {
                if (starMakerScript.currentGameSet.orderMatters)
                {
                    if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Any)
                        UpdateCoordinatesAnyOrdered();
                    else if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Plot)
                        UpdateCoordinatesPlotOrdered();
                    else if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Drag)
                        UpdateCoordinatesDragOrdered();
                }
                else
                {
                    
                    if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Any)
                        UpdateCoordinatesAny();
                    else if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Plot)
                        UpdateCoordinatesPlot();
                    else if (starMakerScript.currentGameSet.gameSetMode == StarMaker.GameSetModes.Drag)
                        UpdateCoordinatesDrag();
                    
                }
            }
        }
    }

    public void UpdateCoordinatesAny()
    {
        if (UIcoordinates.Count < starMakerScript.currentCoordinates.Count && UIcoordinates.Count < UIcoordinateSlots.Count)
        {
            var newCoord = Instantiate(UICoordinatePrefab, UIcoordinateSlots[UIcoordinates.Count]);
            var newCoordScript = newCoord.GetComponent<SM_Coordinate>();
            UIcoordinates.Add(newCoordScript);
            newCoordScript.coordinateIndex = UIcoordinates.Count - 1;
            newCoordScript.starMakerCanvas = this;
            newCoordScript.coordinateIndexText.gameObject.SetActive(false);
        }
        else if (starMakerScript.currentCoordinates.Count < UIcoordinates.Count)
        {
            var tempCoord = UIcoordinates[UIcoordinates.Count - 1].transform.gameObject;
            UIcoordinates.Remove(UIcoordinates[UIcoordinates.Count - 1]); //Checking List.Remove()
            Destroy(tempCoord);
        }

        for (int i = 0; i < starMakerScript.currentCoordinates.Count; i++)
        {
            if (UIcoordinates.Count == starMakerScript.currentCoordinates.Count)
            {
                UIcoordinates[i].coordinate = new Vector2Int(starMakerScript.currentCoordinates[i].x, starMakerScript.currentCoordinates[i].y);
                UIcoordinates[i].coordinateText.text = " " + UIcoordinates[i].coordinate.x + " , " + UIcoordinates[i].coordinate.y + " ";

                if (i == starMakerScript.currentPoints.IndexOf(starMakerScript.currentPoint))
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(true);
                        UIcoordinates[i].coordinateImage.color = imageSelectColor;
                        UIcoordinates[i].coordinateText.color = textSelectColor;
                    }
                }
                else
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);
                        UIcoordinates[i].coordinateImage.color = imageColor;
                        UIcoordinates[i].coordinateText.color = textColor;
                    }
                }
            }
        }

        bool check = true;

        for (int i = 0; i < starMakerScript.currentCoordinates.Count; i++)
        {
            if (!starMakerScript.currentGameSet.correctList.Contains(starMakerScript.currentCoordinates[i]))
            {
                starMakerScript.currentPoints[i].glow2.gameObject.SetActive(false);
                check = false;
            }
            else
            {
                if (!starMakerScript.currentPoints[i].glow2.gameObject.activeSelf)
                {
                    if (starMakerScript.soundCorrect)
                        starMakerScript.soundCorrect.Play();
                }

                starMakerScript.currentPoints[i].glow2.gameObject.SetActive(true);
            }
        }

        if (check && starMakerScript.currentCoordinates.Count == starMakerScript.currentGameSet.correctList.Count)
        {
            if (OnTaskComplete != null)
            {
                OnTaskComplete();
            }

            pauseUpdate = true;
        }
    }

    public void UpdateCoordinatesPlot()
    {
        if (UIcoordinates.Count < starMakerScript.currentCoordinates.Count && UIcoordinates.Count < UIcoordinateSlots.Count)
        {
            var newCoord = Instantiate(UICoordinatePrefab, UIcoordinateSlots[UIcoordinates.Count]);
            var newCoordScript = newCoord.GetComponent<SM_Coordinate>();
            UIcoordinates.Add(newCoordScript);
            newCoordScript.coordinateIndex = UIcoordinates.Count - 1;
            newCoordScript.starMakerCanvas = this;
            newCoordScript.coordinateIndexText.gameObject.SetActive(false);
        }
        else if (starMakerScript.currentCoordinates.Count < UIcoordinates.Count)
        {
            var tempCoord = UIcoordinates[UIcoordinates.Count - 1].transform.gameObject;
            UIcoordinates.Remove(UIcoordinates[UIcoordinates.Count - 1]); //Checking List.Remove()
            Destroy(tempCoord);
        }

        for (int i = 0; i < starMakerScript.currentCoordinates.Count; i++)
        {
            if (UIcoordinates.Count == starMakerScript.currentCoordinates.Count)
            {
                UIcoordinates[i].coordinate = new Vector2Int(starMakerScript.currentCoordinates[i].x, starMakerScript.currentCoordinates[i].y);
                UIcoordinates[i].coordinateText.text = " " + UIcoordinates[i].coordinate.x + " , " + UIcoordinates[i].coordinate.y + " ";

                if (i == starMakerScript.currentPoints.IndexOf(starMakerScript.currentPoint))
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(true);
                        UIcoordinates[i].coordinateImage.color = imageSelectColor;
                        UIcoordinates[i].coordinateText.color = textSelectColor;
                    }
                }
                else
                {
                    if (UIcoordinates[i])
                    {
                        UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);
                        UIcoordinates[i].coordinateImage.color = imageColor;
                        UIcoordinates[i].coordinateText.color = textColor;
                    }
                }
            }
        }

        bool check = true;

        for (int i = 0; i < starMakerScript.currentCoordinates.Count; i++)
        {            
            if (!starMakerScript.currentGameSet.correctList.Contains(starMakerScript.currentCoordinates[i]))
            {
                starMakerScript.currentPoints[i].glow2.gameObject.SetActive(false);
                check = false;
            }
            else
            {
                if (!starMakerScript.currentPoints[i].glow2.gameObject.activeSelf)
                {
                    if (starMakerScript.soundCorrect)
                        starMakerScript.soundCorrect.Play();
                }

                starMakerScript.currentPoints[i].glow2.gameObject.SetActive(true);
            }
        }
        
        if (check && starMakerScript.currentCoordinates.Count == starMakerScript.currentGameSet.correctList.Count)
        {
            if (OnTaskComplete != null)
            {
                OnTaskComplete();
            }

            pauseUpdate = true;
        }
    }

    public void UpdateCoordinatesDrag()
    {
        bool check = true;

        for (int i = 0; i < starMakerScript.currentGameSet.correctList.Count; i++)
        {
            if (starMakerScript.currentCoordinates.Count - 1 >= i)
            {
                var coord = UIcoordinates[i].coordinate;
                //var pointIndex = starMakerScript.gridCoordinates.IndexOf(coord);
                //var curPoint = foreach

                if (starMakerScript.currentGameSet.correctList.Contains(starMakerScript.currentCoordinates[i]))
                {
                    var index = starMakerScript.currentGameSet.correctList.IndexOf(coord);                    

                    if (!starMakerScript.currentPoints[index].glow2.gameObject.activeSelf)
                    {
                        if (starMakerScript.soundCorrect)
                            starMakerScript.soundCorrect.Play();
                    }

                    starMakerScript.currentPoints[index].glow2.gameObject.SetActive(true);
                }
                else
                {                   
                    starMakerScript.currentPoints[i].glow2.gameObject.SetActive(false);
                    check = false;
                }
            }
        }

        for (int i = 0; i < UIcoordinates.Count; i++)
        {
            if(!starMakerScript.currentCoordinates.Contains(UIcoordinates[i].coordinate))
            {
                UIcoordinates[i].coordinateGlow.gameObject.SetActive(false);
                UIcoordinates[i].coordinateImage.color = imageLockedColor;
                UIcoordinates[i].coordinateText.color = textLockedColor;
            }
            else
            {
                UIcoordinates[i].coordinateGlow.gameObject.SetActive(true);
                UIcoordinates[i].coordinateImage.color = imageSelectColor;
                UIcoordinates[i].coordinateText.color = textSelectColor;
            }
        }


        if (check && starMakerScript.currentCoordinates.Count == starMakerScript.currentGameSet.correctList.Count)
        {
            if (OnTaskComplete != null)
            {
                OnTaskComplete();
            }

            pauseUpdate = true;
        }
    }
    
}
