﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StarMakerPoint : MonoBehaviour
{
    public int pointId;
    public Interactable interactable;
    public InteractableBehaviourDragToPoints dragToPoints;
    public SpriteRenderer spriteRenderer;
    public SpriteRenderer glow;
    public SpriteRenderer glow2;
    public TextMeshPro textMesh;
    public Transform currentPoint;
    public Vector2 currentCoordinate;
}
