﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SM_DeletePointButton : MonoBehaviour
{
    public StarMaker starMakerScript;

    public void DeleteCurrentPoint()
    {
        starMakerScript.DeleteCurrentPoint();
    }
}
