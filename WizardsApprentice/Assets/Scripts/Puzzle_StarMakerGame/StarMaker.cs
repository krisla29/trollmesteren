﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StarMaker : MonoBehaviour, IDifficulty, ISetDifficulty
{
    public GameManager gameManager;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.StarMakerGame;
    public PlayerStats.Difficulty starMakerGameDifficulty = PlayerStats.Difficulty.Normal;
    private int correctStreakCounter;
    private int wrongStreakCounter;
    public int numberOfCorrects = 5;
    public int numberOfWrongs = 3;

    public LineRenderer lineRenderer;
    public Gradient loopGradient;
    public Gradient defaultGradient;

    public Transform gameTransform;
    public Transform interactablesParent;
    public GameObject pointObjectPrefab;
    public SM_TaskPoint taskPointPrefab;
    public SM_TaskPoint taskPoint2Prefab;
    public GameObject gridPointPrefab;
    public GameObject victoryEffect;
    public StarMakerCanvas starMakerCanvas;
    public BoxCollider rayBlockerPlayArea;
    public ValueSlider valueSliderX;
    public Button currentValueXBtn;
    public Image valueXBtnImage;
    public ValueSlider valueSliderY;
    public Button currentValueYBtn;
    public Image valueYBtnImage;
    public Button AddPointButton;
    public Image AddPointBtnImage;
    public EasyScalePulse scalePulseAddButton;
    public Button deletePointButton;
    public Image deletePointBtnImage;
    public EasyScalePulse scalePulseDeleteButton;
    public Color normalColor = Color.white;
    public Color selectedColor = Color.yellow;
    public Color lockedColor = Color.gray;

    public RectTransform setCountPanel;
    public Image setCountImage;
    public TextMeshProUGUI setCountPanelText;

    public GameObject gemPhysicalPrefab;
    public Transform spawnGemBox;
    public GameObject winSetEffect;
    public Transform effectsContainer;

    private Vector3 lastPos;
    private Vector3[] pointPositions;

    public enum GameSetModes { Drag, Plot, Any }

    [System.Serializable]
    public class StarMakerGameSet
    {
        public GameSetModes gameSetMode;
        public bool orderMatters;
        public bool canOverlayPoints;
        public bool lineIsClosed;
        public Vector2Int origo = Vector2Int.zero;
        public Vector2Int gridSize;
        public Vector2Int startPoint;
        public List<Vector2Int> correctList = new List<Vector2Int>();
    }
    [Header("GAME SETS")]
    public List<SM_GameSet> gameSets = new List<SM_GameSet>();
    public List<SM_GameSet> gameSetsEasy = new List<SM_GameSet>();
    public List<SM_GameSet> gameSetsHard = new List<SM_GameSet>();
    public List<SM_GameSet> completedSets = new List<SM_GameSet>();
    public List<int> completedSetsAchievements = new List<int>();
    public List<StarMakerGameSet> starmakerGameSets = new List<StarMakerGameSet>();
    public SM_GameSet currentGameSet;

    //Grid:
    public List<Transform> gridPoints = new List<Transform>(); // ReadOnly!
    public List<Vector2Int> gridCoordinates;
    public Transform areaObject;
    public List<Transform> corners = new List<Transform>(4);
    public TextMeshPro axisTextPrefabX;
    public TextMeshPro axisTextPrefabY;
    public List<TextMeshPro> XaxisTexts = new List<TextMeshPro>();
    public List<TextMeshPro> YaxisTexts = new List<TextMeshPro>();
    public float pointScale = 0.2f;
    public float pointActiveScale = 0.4f;
    public Transform activeGridPoint;
    public Transform lastGridPoint;
    public Transform lockPanel;
    public Transform axisX;
    public Transform axisY;

    //Current Points
    public List<StarMakerPoint> currentPoints;
    public StarMakerPoint currentPoint;
    public List<Vector2Int> currentCoordinates;
    public LineRenderer currentTaskLineRenderer;
    public List<SM_TaskPoint> currentTaskPoints;
    private Vector3[] currentTaskPointPositions;

    //Stats
    public int setCounter = 0;
    public bool hasWonGame;
    private bool lastHasWonGame;
    public bool hasOpenedFirstTime;

    public AudioSource soundGridSelect;
    public AudioSource soundGridDeselect;
    public AudioSource soundAddPoint;
    public AudioSource soundCorrect;
    public AudioSource soundDeletePoint;
    public AudioSource soundMovePoint;
    public AudioSource soundBonus;
    public AudioSource soundWin;
    public AudioSource soundStartSet;
    public AudioSource soundError;

    public delegate void StarMakerGameEvent(); // Sends to PlayerStats
    public static event StarMakerGameEvent OnStarMakerGameEnable;
    public static event StarMakerGameEvent OnStarMakerGameDisable;

    public List<Transform> ObjectsToActivateOnComplete = new List<Transform>();
    public List<Transform> ObjectsToSleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    private void Start()
    {
        //StartGameSet(setCounter);
    }

    private void OnEnable()
    {
        TransferPlayerStats();
        lastHasWonGame = hasWonGame;

        Interactable.OnInteractablePressed += SetActivePoint;
        InteractableBehaviourDragToPoints.OnDrop += UpdatePointCoordinate;
        ValueSlider.OnValueSliderUp += CheckSliderValue;
        StarMakerCanvas.OnTaskComplete += StartTaskComplete;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel += StartSelectedGameSet;

        if (!hasOpenedFirstTime)
        {
            StartGameSet(setCounter);
            hasOpenedFirstTime = true;
        }

        OpenDifficultyPanelStart();

        if (OnStarMakerGameEnable != null)
            OnStarMakerGameEnable();
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= SetActivePoint;
        InteractableBehaviourDragToPoints.OnDrop -= UpdatePointCoordinate;
        ValueSlider.OnValueSliderUp -= CheckSliderValue;
        StarMakerCanvas.OnTaskComplete -= StartTaskComplete;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel -= StartSelectedGameSet;

        if (setCountPanel.gameObject.activeSelf)
            setCountPanel.gameObject.SetActive(false);

        foreach (Transform child in effectsContainer)
        {
            Destroy(child.gameObject);
        }

        if (OnStarMakerGameDisable != null)
            OnStarMakerGameDisable();

        StopAllCoroutines();
        CancelInvoke();
    }

    void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    private void Update()
    {
        if (lineRenderer.enabled)
            UpdateLineRenderer();
    }

    void CheckSliderValue(ValueSlider passedSlider)
    {
        if (currentGameSet.canMovePoints)
        {
            if (valueSliderX == passedSlider || valueSliderY == passedSlider)
            {
                UpdatePointPosition();
            }
        }
    }

    private void ClearCurrentTask()
    {
        axisX.gameObject.SetActive(false);
        axisY.gameObject.SetActive(false);
        starMakerCanvas.ClearCoordinates();
        ClearTaskFigure();
    }

    private void StartGameSet(int setCounter)
    {
        SetGameSetWithDifficulty(setCounter);
        gameManager.playerStats.StarMakerGameSetCount = setCounter;

        if (soundStartSet)
            soundStartSet.Play();

        StopCoroutine(ShowSetCountPanel());
        StartCoroutine(ShowSetCountPanel());

        DrawGrid(currentGameSet.gridSize);
        rayBlockerPlayArea.gameObject.SetActive(false);

        switch (currentGameSet.gameSetMode)
        {
            case (GameSetModes.Any):
                currentTaskLineRenderer.enabled = true;
                AddPointButton.enabled = true;
                AddPointBtnImage.color = new Vector4(AddPointBtnImage.color.r, AddPointBtnImage.color.g, AddPointBtnImage.color.b, 1f);
                scalePulseAddButton.enabled = true;
                valueSliderX.lockValueSlider = false;
                currentValueXBtn.enabled = true;
                valueXBtnImage.color = new Vector4(valueXBtnImage.color.r, valueXBtnImage.color.g, valueXBtnImage.color.b, 1f);
                valueSliderY.lockValueSlider = false;
                currentValueYBtn.enabled = true;
                valueYBtnImage.color = new Vector4(valueYBtnImage.color.r, valueYBtnImage.color.g, valueYBtnImage.color.b, 1f);
                lockPanel.gameObject.SetActive(false);
                starMakerCanvas.cp_editTheme.gameObject.SetActive(true);
                starMakerCanvas.cp_lockTheme.gameObject.SetActive(false);
                starMakerCanvas.cp_pulser.enabled = true;
                DrawNewTaskFigure();
                break;
            case (GameSetModes.Drag):
                currentTaskLineRenderer.enabled = false;
                AddPointButton.enabled = true;
                AddPointBtnImage.color = new Vector4(AddPointBtnImage.color.r, AddPointBtnImage.color.g, AddPointBtnImage.color.b, 1f);
                scalePulseAddButton.enabled = true;
                valueSliderX.lockValueSlider = true;
                currentValueXBtn.enabled = false;
                valueXBtnImage.color = new Vector4(valueXBtnImage.color.r, valueXBtnImage.color.g, valueXBtnImage.color.b, 0.75f);
                valueSliderY.lockValueSlider = true;
                currentValueYBtn.enabled = false;
                valueYBtnImage.color = new Vector4(valueYBtnImage.color.r, valueYBtnImage.color.g, valueYBtnImage.color.b, 0.75f);
                starMakerCanvas.CreateNewTask();
                lockPanel.gameObject.SetActive(false);
                starMakerCanvas.cp_editTheme.gameObject.SetActive(false);
                starMakerCanvas.cp_lockTheme.gameObject.SetActive(true);
                starMakerCanvas.cp_pulser.enabled = false;
                break;
            case (GameSetModes.Plot):
                currentTaskLineRenderer.enabled = true;
                AddPointButton.enabled = true;
                AddPointBtnImage.color = new Vector4(AddPointBtnImage.color.r, AddPointBtnImage.color.g, AddPointBtnImage.color.b, 1f);
                scalePulseAddButton.enabled = true;
                valueSliderX.lockValueSlider = false;
                currentValueXBtn.enabled = true;
                valueXBtnImage.color = new Vector4(valueXBtnImage.color.r, valueXBtnImage.color.g, valueXBtnImage.color.b, 1f);
                valueSliderY.lockValueSlider = false;
                currentValueYBtn.enabled = true;
                valueYBtnImage.color = new Vector4(valueYBtnImage.color.r, valueYBtnImage.color.g, valueYBtnImage.color.b, 1f);
                rayBlockerPlayArea.gameObject.SetActive(true);
                StopCoroutine(FadeOutLockPanel());
                lockPanel.gameObject.SetActive(true);
                starMakerCanvas.cp_editTheme.gameObject.SetActive(true);
                starMakerCanvas.cp_lockTheme.gameObject.SetActive(false);
                starMakerCanvas.cp_pulser.enabled = true;
                DrawNewTaskFigure();
                break;
            default:
                break;
        }

        if (currentGameSet.startPoints.Count > 0)
        {
            SpawnStartPoints(currentGameSet.startPoints);
        }

        if (currentGameSet.lineIsClosed)
            lineRenderer.colorGradient = loopGradient;
        else
            lineRenderer.colorGradient = defaultGradient;

        if (currentPoints.Count > 1)
            lineRenderer.enabled = true;
        else
            lineRenderer.enabled = false;

        SetValueSliders(valueSliderX, currentGameSet.origo.x);
        SetValueSliders(valueSliderY, currentGameSet.origo.y);       

        if (currentGameSet.canAddPoints)
        {
            AddPointButton.gameObject.SetActive(true);
            deletePointButton.gameObject.SetActive(true);
        }
        else
        {
            AddPointButton.gameObject.SetActive(false);
            deletePointButton.gameObject.SetActive(false);
        }

        if (currentGameSet.canMovePoints)
        {
            starMakerCanvas.cm_editTheme.gameObject.SetActive(true);
            starMakerCanvas.cm_lockTheme.gameObject.SetActive(false);
        }
        else
        {
            starMakerCanvas.cm_lockTheme.gameObject.SetActive(true);
            starMakerCanvas.cm_editTheme.gameObject.SetActive(false);
        }

        axisX.gameObject.SetActive(true);
        axisY.gameObject.SetActive(true);
        DeselectAll();
        StopCoroutine(FadeOutCoordinates());
        StopCoroutine(FadeOutSliders());
        StopCoroutine(FadeOutButtons());
        StopCoroutine(FadeOutCanMove());
        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        starMakerCanvas.coordinatesPanel.gameObject.SetActive(true);
        starMakerCanvas.coordinateSlidersPanel.gameObject.SetActive(true);
        starMakerCanvas.addButtonsPanel.gameObject.SetActive(true);
        starMakerCanvas.canMovePanel.gameObject.SetActive(true);
        starMakerCanvas.pauseUpdate = false;

        gameManager.playerStats.SaveGame();
    }

    void SetValueSliders(ValueSlider slider, int origo)
    {
        for (int i = 0; i < slider.values.Count; i++)
        {
            int newValue = (i * currentGameSet.gridInterval) + origo;
            slider.values[i] = newValue;
        }

        slider.SetValues();
        slider.currentKey = 0;
        slider.SetKey(0);
    }

    void DrawNewTaskFigure()
    {
        if (currentGameSet.orderMatters)
            currentTaskLineRenderer.enabled = true;
        else
            currentTaskLineRenderer.enabled = false;

        var list = currentGameSet.correctList;
        currentTaskPointPositions = new Vector3[list.Count];

        if (currentGameSet.lineIsClosed)
            currentTaskPointPositions = new Vector3[list.Count + 1];

        for (int i = 0; i < list.Count; i++)
        {
            SM_TaskPoint newTaskPoint = null;
            if (currentGameSet.orderMatters)
                newTaskPoint = Instantiate(taskPointPrefab, gameTransform);
            else
                newTaskPoint = Instantiate(taskPoint2Prefab, gameTransform);

            currentTaskPoints.Add(newTaskPoint);
            newTaskPoint.coordinate = currentGameSet.correctList[i];
            newTaskPoint.indexText.text = " " + (i + 1).ToString() + " ";
            newTaskPoint.glow.gameObject.SetActive(false);
            newTaskPoint.transform.position = gridPoints[gridCoordinates.IndexOf(list[i])].position;
            currentTaskPointPositions[i] = gameTransform.InverseTransformPoint(newTaskPoint.transform.position);
        }

        if (currentGameSet.lineIsClosed)
            currentTaskPointPositions[currentTaskPointPositions.Length - 1] = currentTaskPointPositions[0];

        currentTaskLineRenderer.positionCount = currentTaskPointPositions.Length;
        currentTaskLineRenderer.SetPositions(currentTaskPointPositions);
    }

    void ClearTaskFigure()
    {
        foreach (var item in currentTaskPoints)
        {
            Destroy(item.gameObject);
        }

        currentTaskPoints.Clear();
        currentTaskPointPositions = null;
        currentTaskLineRenderer.positionCount = 0;
        currentTaskLineRenderer.enabled = false;
    }

    public void SetSlidersToLocked()
    {
        starMakerCanvas.cs_editTheme.gameObject.SetActive(false);
        starMakerCanvas.cs_lockTheme.gameObject.SetActive(true);
        starMakerCanvas.cs_pulser.enabled = false;

        foreach (var item in starMakerCanvas.cs_objectsToLock)
        {
            item.gameObject.SetActive(false);
        }

        starMakerCanvas.cs_editMode = false;
    }

    public void SetSlidersToUnlocked()
    {
        starMakerCanvas.cs_editTheme.gameObject.SetActive(true);
        starMakerCanvas.cs_lockTheme.gameObject.SetActive(false);
        starMakerCanvas.cs_pulser.enabled = true;

        foreach (var item in starMakerCanvas.cs_objectsToLock)
        {
            item.gameObject.SetActive(true);
        }

        starMakerCanvas.cs_editMode = true;
    }

    public void SetActivePointFromCoordinate(int index)
    {
        DeActivateCurrentPoint();
        currentPoint = currentPoints[index];
        currentPoint.spriteRenderer.color = selectedColor;

        if (currentGameSet.canMovePoints)
        {
            if (!starMakerCanvas.cs_editMode)
                SetSlidersToUnlocked();

            currentPoint.glow.gameObject.SetActive(true);
        }
        else
        {
            if (starMakerCanvas.cs_editMode)
                SetSlidersToLocked();
        }

        valueSliderX.SetKey(valueSliderX.values.IndexOf(currentCoordinates[index].x));
        valueSliderY.SetKey(valueSliderY.values.IndexOf(currentCoordinates[index].y));

        if (soundMovePoint)
            soundMovePoint.Play();
    }

    void SetActivePoint(Transform interactable)
    {
        bool check = false;

        for (int i = 0; i < gridPoints.Count; i++)
        {
            if (interactable == gridPoints[i].transform)
            {
                check = true;

                lastGridPoint = activeGridPoint;

                if (lastGridPoint)
                    HelperFunctions.CounterParentScale(activeGridPoint, areaObject, pointScale);

                if (gridPoints[i] != lastGridPoint)
                {
                    activeGridPoint = gridPoints[i];
                    HelperFunctions.CounterParentScale(activeGridPoint, areaObject, pointActiveScale);
                    if (soundGridSelect)
                        soundGridSelect.Play();

                    DeActivateAllPoints();
                }
                else
                {
                    activeGridPoint = null;
                    if (soundGridDeselect)
                        soundGridDeselect.Play();
                }
            }
        }

        if (!check)
        {
            for (int i = 0; i < currentPoints.Count; i++)
            {
                if (interactable.parent == currentPoints[i].transform)
                {
                    check = true;
                    DeActivateCurrentPoint();
                    currentPoint = currentPoints[i];
                    currentPoint.spriteRenderer.color = selectedColor;

                    if (currentGameSet.canMovePoints)
                        currentPoint.glow.gameObject.SetActive(true);
                    else
                        currentPoint.glow.gameObject.SetActive(false);

                    valueSliderX.SetKey(valueSliderX.values.IndexOf(currentCoordinates[i].x));
                    valueSliderY.SetKey(valueSliderY.values.IndexOf(currentCoordinates[i].y));

                    lastGridPoint = activeGridPoint;
                    if (lastGridPoint)
                        HelperFunctions.CounterParentScale(activeGridPoint, areaObject, pointScale);

                    activeGridPoint = null;

                    AddPointButton.gameObject.SetActive(false);

                    if (currentGameSet.canAddPoints)
                        deletePointButton.gameObject.SetActive(true);

                    if(currentGameSet.canMovePoints)
                    {
                        if (!starMakerCanvas.cs_editMode)
                            SetSlidersToUnlocked();
                    }
                    else
                    {
                        if (starMakerCanvas.cs_editMode)
                            SetSlidersToLocked();
                    }
                }
            }
        }

        if (!check)
        {
            DeActivateAllPoints();

            if (soundGridDeselect)
                soundGridDeselect.Play();

            if (currentGameSet.canAddPoints)
                AddPointButton.gameObject.SetActive(true);

            deletePointButton.gameObject.SetActive(false);
        }
    }

    void DeActivateCurrentPoint()
    {
        if (currentPoint)
        {
            currentPoint.spriteRenderer.color = normalColor;
            currentPoint.glow.gameObject.SetActive(false);
            currentPoint = null;

            if (currentGameSet.canAddPoints)
                AddPointButton.gameObject.SetActive(true);

            deletePointButton.gameObject.SetActive(false);
        }

        if (!starMakerCanvas.cs_editMode)
            SetSlidersToUnlocked();
    }

    void DeActivateAllPoints()
    {
        foreach (var point in currentPoints)
        {
            point.spriteRenderer.color = normalColor;
            point.glow.gameObject.SetActive(false);
        }

        currentPoint = null;

        if (currentGameSet.canAddPoints)
            AddPointButton.gameObject.SetActive(true);

        deletePointButton.gameObject.SetActive(false);

        if (!starMakerCanvas.cs_editMode)
            SetSlidersToUnlocked();
    }

    public void DeselectAll()
    {
        if (soundGridDeselect)
            soundGridDeselect.Play();

        if (activeGridPoint)
        {
            lastGridPoint = activeGridPoint;
            if (lastGridPoint)
                HelperFunctions.CounterParentScale(activeGridPoint, areaObject, pointScale);
            activeGridPoint = null;
        }

        DeActivateAllPoints();

        DeselectCoordinate();
    }

    public void DeselectCoordinate()
    {
        if (starMakerCanvas.currentUICordinate)
        {
            starMakerCanvas.currentUICordinate.coordinateGlow.gameObject.SetActive(false);
            starMakerCanvas.currentUICordinate = null;
        }
    }

    public void DeleteCurrentPoint()
    {
        if (currentPoint != null)
        {
            DeletePoint(currentPoints.IndexOf(currentPoint));

            if (soundDeletePoint)
                soundDeletePoint.Play();

            if (currentPoints.Count < 2)
                lineRenderer.enabled = false;

            if (currentGameSet.canAddPoints)
                AddPointButton.gameObject.SetActive(true);

            deletePointButton.gameObject.SetActive(false);

            if (!starMakerCanvas.cs_editMode)
                SetSlidersToUnlocked();
        }
        else
        {
            if (soundError)
                soundError.Play();
        }
    }

    void AddPoint(int passedPointIndex)
    {
        //if (gridPoints[passedPointIndex].position != lastPos)
        //{
        if (soundAddPoint)
            soundAddPoint.Play();

        var newPoint = Instantiate(pointObjectPrefab, gridPoints[passedPointIndex].position, Quaternion.identity, gameTransform);
        var newPointBehaviour = newPoint.transform.GetChild(0).GetComponent<InteractableBehaviourDragToPoints>();
        var newPointScript = newPoint.GetComponent<StarMakerPoint>();

        newPointBehaviour.activatePoints = true;
        if (newPointBehaviour.currentPoint)
            newPointBehaviour.currentPoint.gameObject.SetActive(false);

        newPointScript.spriteRenderer.color = normalColor;
        InitializeNewPoint(newPointBehaviour, gridPoints[passedPointIndex], newPointScript);
        currentPoints.Add(newPointScript);
        lastPos = gridPoints[passedPointIndex].position;
        /*
        currentPoint = newPointScript;
        currentPoint.spriteRenderer.color = selectedColor;
        */
        /*
        if (currentGameSet.canMovePoints)
            currentPoint.glow.gameObject.SetActive(true);
        else
            currentPoint.glow.gameObject.SetActive(false);
        */
        
        valueSliderX.SetKey(valueSliderX.values.IndexOf(gridCoordinates[passedPointIndex].x));
        valueSliderY.SetKey(valueSliderY.values.IndexOf(gridCoordinates[passedPointIndex].y));

        lastGridPoint = activeGridPoint;

        if (lastGridPoint)
            HelperFunctions.CounterParentScale(activeGridPoint, areaObject, pointScale);

        //newPointBehaviour.SnapToNearestPoint();
        activeGridPoint = null;

        if (currentPoints.Count > 1)
            lineRenderer.enabled = true;

        AddPointButton.gameObject.SetActive(false);

        if (currentGameSet.canAddPoints)
            AddPointButton.gameObject.SetActive(true);


        if (currentGameSet.canMovePoints)
        {
            if (!starMakerCanvas.cs_editMode)
                SetSlidersToUnlocked();
        }
        else
        {
            if (starMakerCanvas.cs_editMode)
                SetSlidersToLocked();
        }
        //}
    }

    void SpawnStartPoints(List<Vector2Int> pointCoordinates)
    {
        for (int i = 0; i < pointCoordinates.Count; i++)
        {
            if (gridCoordinates.Contains(pointCoordinates[i]))
            {
                int newIndex = gridCoordinates.IndexOf(pointCoordinates[i]);
                Transform newPoint = gridPoints[newIndex];
                AddPoint(newIndex);
            }
            else
                print("Error!! Start Point Coordinate isn't inside of grid!");
        }
    }

    void UpdatePointCoordinate(InteractableBehaviourDragToPoints passedBehaviour, int passedPointIndex)
    {
        for (int i = 0; i < currentPoints.Count; i++)
        {
            if (currentPoints[i].transform == passedBehaviour.transform.parent)
            {
                currentCoordinates[i] = passedBehaviour.pointGridPositions[passedPointIndex];
                currentPoints[i].currentPoint = gridPoints[passedPointIndex];
                currentPoints[i].currentCoordinate = currentCoordinates[i];
                valueSliderX.SetKey(valueSliderX.values.IndexOf(currentCoordinates[i].x));
                valueSliderY.SetKey(valueSliderY.values.IndexOf(currentCoordinates[i].y));

                if (soundMovePoint)
                    soundMovePoint.Play();
            }
        }
    }

    public void AddNewPointFromSliders() //Called by SM_AddPointButton
    {
        if (CheckSlidersCoordinate())
        {
            Vector2Int testCoordinate = new Vector2Int(valueSliderX.values[valueSliderX.currentKey], valueSliderY.values[valueSliderY.currentKey]);

            int newIndex = gridCoordinates.IndexOf(testCoordinate);

            bool checkIfOccupied = false;

            foreach (var item in currentPoints)
            {
                if (item.currentPoint == gridPoints[newIndex])
                    checkIfOccupied = true;
            }

            if (!checkIfOccupied)
                AddPoint(newIndex);
            else
            {
                if (soundError)
                    soundError.Play();
            }
        }
        else
        {
            if (soundError)
                soundError.Play();
        }
    }
    public void AddNewPointFromGrid() //Called by SM_AddPointButton
    {
        if (activeGridPoint)
        {
            var index = gridPoints.IndexOf(activeGridPoint);
            //Transform newPoint = gridPoints[index];
            AddPoint(index);
        }
    }


    void UpdatePointPosition()
    {
        if (currentPoint != null)
        {
            Transform lastPoint = currentPoint.currentPoint;
            int lastIndex = gridPoints.IndexOf(lastPoint);
            currentPoint.dragToPoints.lastPoint = lastPoint;
            lastPoint.gameObject.SetActive(true);

            if (CheckSlidersCoordinate())
            {
                Vector2Int testCoordinate = new Vector2Int(valueSliderX.values[valueSliderX.currentKey], valueSliderY.values[valueSliderY.currentKey]);

                int newIndex = gridCoordinates.IndexOf(testCoordinate);

                Transform newPoint = gridPoints[newIndex];

                currentPoint.transform.position = newPoint.position;
                currentPoint.dragToPoints.SnapToNearestPoint();

                if (soundMovePoint)
                    soundMovePoint.Play();
            }
        }
    }

    bool CheckSlidersCoordinate()
    {
        var gridSize = currentGameSet.gridSize;
        var origo = currentGameSet.origo;
        var interval = currentGameSet.gridInterval;
        Vector2Int maxKeys = new Vector2Int(gridSize.x - 1, gridSize.y - 1);
        //print("MAX KEYS ARE ..." + maxKeys);
        Vector2Int minKeys = new Vector2Int(0, 0);
        //print("MIN KEYS ARE ..." + minKeys);
        Vector2Int currentKeys = new Vector2Int(valueSliderX.currentKey, valueSliderY.currentKey);


        if (currentKeys.x >= minKeys.x && currentKeys.x <= maxKeys.x && currentKeys.y >= minKeys.y && currentKeys.y <= maxKeys.y)
            return true;
        else
            return false;
    }

    void DeletePoint(int pointIndex)
    {
        Transform cp = currentPoints[pointIndex].currentPoint;

        currentCoordinates.Remove(currentCoordinates[pointIndex]);
        currentPoints.Remove(currentPoints[pointIndex]);
        var go = currentPoint.gameObject;
        currentPoint = null;
        Destroy(go);

        cp.gameObject.SetActive(true);
    }

    void InitializeNewPoint(InteractableBehaviourDragToPoints pointBehaviour, Transform passedPoint, StarMakerPoint passedScript)
    {
        pointBehaviour.isInitializedByOther = true;
        pointBehaviour.activatePoints = true;
        pointBehaviour.worldTransform = gameTransform;
        pointBehaviour.areaObject = areaObject;
        pointBehaviour.corners = corners;
        pointBehaviour.pointMode = InteractableBehaviourDragToPoints.PointModes.Custom;
        pointBehaviour.gridSize = currentGameSet.gridSize;
        pointBehaviour.points = gridPoints;
        pointBehaviour.currentPoint = passedPoint;
        pointBehaviour.startPos = passedPoint.position;
        pointBehaviour.SnapToNearestPoint();
        pointBehaviour.pointGridPositions = gridCoordinates;

        var index = pointBehaviour.points.IndexOf(pointBehaviour.currentPoint);
        currentCoordinates.Add(pointBehaviour.pointGridPositions[index]);

        //HelperFunctions.SetNewIndex(currentCoordinates, currentPoints.IndexOf(mainPoint), currentCoordinates.Count - 1); //Shuffles main to top

        passedScript.currentPoint = passedPoint;

        if (currentGameSet.gameSetMode == GameSetModes.Plot)
            passedScript.interactable.GetComponent<Collider>().enabled = false;

        if (!currentGameSet.canMovePoints)
            pointBehaviour.lockBehaviour = true;
    }

    Transform ConvertV2ToPoint(StarMakerGameSet gameSet, Vector2Int v2)
    {
        int newIndex = 0;

        for (int i = 0; i < gridCoordinates.Count; i++)
        {
            if (gridCoordinates[i] == v2)
                //newIndex = i - 1;
                newIndex = i;
        }
        if (v2 == (gameSet.gridSize - Vector2Int.one))
        {
            newIndex = gridCoordinates.Count - 1;
        }

        return gridPoints[newIndex];
    }

    void UpdateLineRenderer()
    {
        if (currentPoints.Count > 1)
        {
            lineRenderer.positionCount = currentPoints.Count + 1;
            pointPositions = new Vector3[currentPoints.Count + 1];

            for (int i = 0; i < currentPoints.Count; i++)
            {
                if (i < currentPoints.Count)
                {
                    if (currentPoints[i])
                        pointPositions[i] = gameTransform.InverseTransformPoint(currentPoints[i].transform.position);
                }
            }

            if (!currentGameSet.lineIsClosed)
                pointPositions[pointPositions.Length - 1] = gameTransform.transform.InverseTransformPoint(currentPoints[currentPoints.Count - 1].transform.position);
            else
                pointPositions[pointPositions.Length - 1] = gameTransform.transform.InverseTransformPoint(currentPoints[0].transform.position);

            lineRenderer.SetPositions(pointPositions);
        }
    }

    public void ClearCurrentSet()
    {
        ClearCurrentTask();

        foreach (var item in currentPoints)
        {
            Destroy(item.gameObject);
        }
        currentPoints.Clear();

        foreach (var item in gridPoints)
        {
            Destroy(item.gameObject);
        }
        gridPoints.Clear();

        foreach (var item in XaxisTexts)
        {
            Destroy(item.gameObject);
        }
        XaxisTexts.Clear();

        foreach (var item in YaxisTexts)
        {
            Destroy(item.gameObject);
        }
        YaxisTexts.Clear();

        gridCoordinates.Clear();

        currentCoordinates.Clear();

    }

    public void TaskComplete()
    {
        StopCoroutine(FadeOutCoordinates());
        StartCoroutine(FadeOutCoordinates());
        StopCoroutine(FadeOutSliders());
        StartCoroutine(FadeOutSliders());
        StopCoroutine(FadeOutButtons());
        StartCoroutine(FadeOutButtons());
        StopCoroutine(FadeOutCanMove());
        StartCoroutine(FadeOutCanMove());
        if (lockPanel.gameObject.activeSelf)
        {
            StopCoroutine(FadeOutLockPanel());
            StartCoroutine(FadeOutLockPanel());
        }        
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);

        var newVictoryEffect = Instantiate(victoryEffect, effectsContainer);
        if (soundWin)
            soundWin.Play();

        if (setCounter == gameSets.Count - 1)
        {

            if(!hasWonGame)
            {
                foreach (var item in ObjectsToActivateOnComplete)
                {
                    item.gameObject.SetActive(true);
                }

                foreach (var item in ObjectsToSleepOnComplete)
                {
                    item.gameObject.SetActive(false);
                }
            }

            hasWonGame = true;
            gameManager.playerStats.hasWonStarMakerGame = true;
        }

        SetCurrentCompletedSet(0);

        ClearCurrentSet();

        setCounter++;

        if (setCounter == gameSets.Count)
        {
            setCounter = 0;
            hasOpenedFirstTime = false;
        }

        StartCoroutine(TaskCompleteResponse());
    }

    void StartTaskComplete()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        Invoke("TaskComplete", 1f);
    }

    public IEnumerator TaskCompleteResponse()
    {
        yield return new WaitForSeconds(3);

        RewardPlayer();

        yield return new WaitForSeconds(4);

        if (setCounter == 0)
            StartCoroutine(WinGame());
        else
            StartGameSet(setCounter);
    }

    public IEnumerator WinGame()
    {
        if(!lastHasWonGame)
        {
            starMakerCanvas.trophyPanel.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            if (soundBonus)
                soundBonus.Play();
            starMakerCanvas.trophy.gameObject.SetActive(true);
            yield return new WaitForSeconds(3);            
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
            starMakerCanvas.trophy.gameObject.SetActive(false);
            starMakerCanvas.trophyPanel.gameObject.SetActive(false);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
            gameManager.playerStats.trophies.Add(trophy);
            gameManager.playerStats.hasWonStarMakerGame = true;
        }

        lastHasWonGame = true;
        gameManager.playerStats.SaveGame();
        yield return new WaitForSeconds(2);
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
    }

    void StartTaskCompletion()
    {
        TaskComplete();
    }

    private IEnumerator ShowSetCountPanel()
    {
        if (starMakerGameDifficulty == PlayerStats.Difficulty.Easy)
            setCountImage.color = gameManager.colorEasy;
        else if (starMakerGameDifficulty == PlayerStats.Difficulty.Normal)
            setCountImage.color = gameManager.colorNormal;
        else if (starMakerGameDifficulty == PlayerStats.Difficulty.Hard)
            setCountImage.color = gameManager.colorHard;
        else
            setCountImage.color = Color.grey;

        setCountPanelText.text = (setCounter + 1).ToString() + "/" + gameSets.Count.ToString();

        if (setCountPanelText.text.Length < 4)
            setCountPanelText.fontSize = 220;
        else if (setCountPanelText.text.Length == 4)
            setCountPanelText.fontSize = 200;
        else if (setCountPanelText.text.Length > 4)
            setCountPanelText.fontSize = 160;

        setCountPanel.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        setCountPanel.gameObject.SetActive(false);
    }


    void RewardPlayer()
    {
        int reward = 1;

        if (!completedSets.Contains(currentGameSet))
        { 
            reward = currentGameSet.correctList.Count * 2;

            gameManager.player.anim.SetTrigger("Success");

            if (soundBonus)
                soundBonus.Play();
        }

        ReleaseGems(reward);
    }

    private void ReleaseGems(int bonus)
    {
        //spawnGemBox.position = gameManager.player.transform.position + (Vector3.up * 5);

        Invoke("SpawnEffect", 0.5f);

        for (int i = 0; i < bonus; i++)
        {
            var randTime = Random.Range(0, 1f);
            Invoke("SpawnGem", randTime);
        }
    }

    void SpawnGem()
    {
        var newPos = HelperFunctions.RandomPointInBox(spawnGemBox.transform);
        var newGem = Instantiate(gemPhysicalPrefab, newPos, Quaternion.identity, null);
    }

    void SpawnEffect()
    {
        var newEffect = Instantiate(winSetEffect, spawnGemBox.position, Quaternion.LookRotation(Camera.main.transform.position), effectsContainer);
    }

    private IEnumerator FadeOutCoordinates()
    {
        float t = 0;
        
        while(t < 1)
        {
            t += Time.deltaTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            starMakerCanvas.coordinatesPanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        starMakerCanvas.coordinatesPanel.gameObject.SetActive(false);
    }

    private IEnumerator FadeOutSliders()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            starMakerCanvas.coordinateSlidersPanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        starMakerCanvas.coordinateSlidersPanel.gameObject.SetActive(false);
    }

    private IEnumerator FadeOutButtons()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            starMakerCanvas.addButtonsPanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        starMakerCanvas.addButtonsPanel.gameObject.SetActive(false);
    }

    private IEnumerator FadeOutCanMove()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            starMakerCanvas.canMovePanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        starMakerCanvas.canMovePanel.gameObject.SetActive(false);
    }

    private IEnumerator FadeOutLockPanel()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            lockPanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        lockPanel.gameObject.SetActive(false);
    }

    ////////////////////////////////////////////DRAW GRID///////////////////////////////////////////////

    public void DrawGrid(Vector2 grid)
    {
        var intervalX = ((areaObject.localScale.x / grid.x) / areaObject.localScale.x) * 2;
        var intervalY = ((areaObject.localScale.z / grid.y) / areaObject.localScale.z) * 2;
        var scaleRatio = areaObject.localScale.x / areaObject.localScale.z;
        var origo = currentGameSet.origo;
        var gridInterval = currentGameSet.gridInterval;

        intervalX += intervalX / (grid.x - 1);
        intervalY += intervalY / (grid.y - 1);

        foreach (var item in XaxisTexts)
        {
            Destroy(item.gameObject);
        }
        XaxisTexts.Clear();

        foreach (var item in YaxisTexts)
        {
            Destroy(item.gameObject);
        }
        YaxisTexts.Clear();

        gridCoordinates.Clear();
        gridCoordinates = new List<Vector2Int>();

        var offset = new Vector3(-1, 0, -1);
        var Xpos = Vector3.zero;
        var Ypos = Vector3.zero;

        for (int i = 0; i < grid.x; i++)
        {
            var newPointX = Instantiate(gridPointPrefab, areaObject);
            newPointX.name = "Xpoint " + ((i + 1 + origo.x) * gridInterval).ToString();
            gridPoints.Add(newPointX.transform);

            HelperFunctions.CounterParentScale(newPointX.transform, areaObject, pointScale);

            newPointX.transform.localRotation = Quaternion.identity;
            newPointX.transform.localPosition = new Vector3((intervalX * i), 0, 0);
            
            gridCoordinates.Add(new Vector2Int((i + origo.x) * gridInterval, origo.y));

            var XaxisText = Instantiate(axisTextPrefabX, areaObject);
            XaxisText.transform.position = newPointX.transform.position;
            XaxisText.text = ((i + origo.x) * gridInterval).ToString();
            XaxisTexts.Add(XaxisText);

            if (i == 0)
            {
                var YaxisText = Instantiate(axisTextPrefabY, areaObject);
                YaxisText.transform.position = newPointX.transform.position;

                if(((i + origo.x) * gridInterval) < 0)
                    YaxisText.text = ((i + origo.x) * gridInterval).ToString();
                else
                    YaxisText.text = " " + ((i + origo.x) * gridInterval).ToString();

                YaxisTexts.Add(YaxisText);                
            }
            
            if((i + origo.x) * gridInterval == 0)
            {
                //axisX.transform.localPosition = new Vector3((intervalX * i), 0, 0) + offset;
                Xpos = new Vector3((intervalX * i), 0, 0);
            }
            
            for (int e = 0; e < grid.y - 1; e++)
            {
                var newPointY = Instantiate(gridPointPrefab, areaObject);
                newPointY.name = "Ypoint " + ((i * e + origo.y) * gridInterval).ToString();
                gridPoints.Add(newPointY.transform);

                HelperFunctions.CounterParentScale(newPointY.transform, areaObject, pointScale);

                newPointY.transform.localRotation = Quaternion.identity;
                newPointY.transform.localPosition = new Vector3(newPointX.transform.localPosition.x, 0, intervalY + (intervalY * e));

                if (e < grid.y - 1)
                    gridCoordinates.Add(new Vector2Int(i + origo.x, e + 1 + origo.y) * gridInterval);

                if(i == 0)
                {
                    var YaxisText = Instantiate(axisTextPrefabY, areaObject);
                    YaxisText.transform.position = newPointY.transform.position;

                    if(((e + 1 + origo.y) * gridInterval) < 0)
                        YaxisText.text = ((e + 1 + origo.y) * gridInterval).ToString();
                    else
                        YaxisText.text = " " + ((e + 1 + origo.y) * gridInterval).ToString();

                    YaxisTexts.Add(YaxisText);                    
                }

                if((e + 1 + origo.y) * gridInterval == 0)
                {

                    Ypos = new Vector3(newPointX.transform.localPosition.x, 0, intervalY + (intervalY * e));
                    //axisY.transform.localPosition = new Vector3(Xpos, 0, intervalY + (intervalY * e)) + offset;
                }
            }
        }

        axisX.transform.localPosition = new Vector3(Xpos.x, 0, Ypos.z) + offset;
        axisY.transform.localPosition = new Vector3(Xpos.x, 0, Ypos.z) + offset;

        float textOffset = 0.1f;
        float textOffset2 = 0.07f;

        foreach (var item in gridPoints)
        {
            item.localPosition += offset;
        }
        foreach (var item in XaxisTexts)
        {
            item.transform.localPosition += offset;
            item.transform.localPosition += new Vector3(0, 0, Ypos.z + textOffset);
        }
        foreach (var item in YaxisTexts)
        {
            item.transform.localPosition += offset;
            if(item.text.Length < 3)
                item.transform.localPosition += new Vector3(Xpos.x + textOffset, 0, 0);
            else
                item.transform.localPosition += new Vector3(Xpos.x + textOffset2, 0, 0);
        }
    }


    //---------------------------------------------------IDifficulty/ISetDifficulty------------------------------------------------------------------


    public void CheckDifficulty()
    {
        if (!hasWonGame)
        {
            if ((correctStreakCounter >= numberOfCorrects && starMakerGameDifficulty != PlayerStats.Difficulty.Hard) || (wrongStreakCounter >= numberOfWrongs && starMakerGameDifficulty != PlayerStats.Difficulty.Easy))
            {
                if (correctStreakCounter >= numberOfCorrects && starMakerGameDifficulty == PlayerStats.Difficulty.Easy)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;
                else if (correctStreakCounter >= numberOfCorrects && starMakerGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Hard;
                else if (wrongStreakCounter >= numberOfWrongs && starMakerGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Easy;
                else if (wrongStreakCounter >= numberOfWrongs && starMakerGameDifficulty == PlayerStats.Difficulty.Hard)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;

                //print("Hello!");
                gameManager.hudCanvas.difficultyPanel.OpenPanel();
                correctStreakCounter = 0;
                wrongStreakCounter = 0;
            }
        }
    }

    public void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        starMakerGameDifficulty = requestedDifficulty;
    }
    public void SetCurrentDifficultyStartSet(PlayerStats.Difficulty requestedDifficulty)
    {
        starMakerGameDifficulty = requestedDifficulty;
        ClearCurrentSet();
        StartGameSet(setCounter);
    }

    public void SetGameSetWithDifficulty(int index)
    {
        switch (starMakerGameDifficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                currentGameSet = gameSetsEasy[index];
                break;
            case (PlayerStats.Difficulty.Normal):
                currentGameSet = gameSets[index];
                break;
            case (PlayerStats.Difficulty.Hard):
                currentGameSet = gameSetsHard[index];
                break;
            default:
                currentGameSet = gameSets[index];
                break;
        }
    }

    public void WrongTicker()
    {
        wrongStreakCounter++;
        correctStreakCounter = 0;
        CheckDifficulty();
    }


    public void CorrectTicker()
    {
        wrongStreakCounter = 0;
        correctStreakCounter++;
        CheckDifficulty();
    }

    public void OpenDifficultyPanelStart()
    {
        gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.None;
        gameManager.hudCanvas.difficultyPanel.startNewLevel = true;

        if (gameManager.playerStats.hasWonStarMakerGame)
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = true;
            gameManager.hudCanvas.difficultyPanel.currentLevelCount = gameSets.Count;
        }
        else
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = false;
        }

        gameManager.hudCanvas.difficultyPanel.gameObject.SetActive(true);
    }

    public void SetCurrentCompletedSet(int passedAchievement)
    {
        int setIndex = 0;

        if (gameSets.Contains(currentGameSet))
            setIndex = gameSets.IndexOf(currentGameSet) + gameSets.Count;
        else if (gameSetsEasy.Contains(currentGameSet))
            setIndex = gameSetsEasy.IndexOf(currentGameSet);
        else if (gameSetsHard.Contains(currentGameSet))
            setIndex = gameSetsHard.IndexOf(currentGameSet) + (gameSets.Count * 2);

        if (!gameManager.playerStats.starMakerGameCompletedSets.Contains(setIndex))
        {
            gameManager.playerStats.starMakerGameCompletedSets.Add(setIndex);
            //gameManager.playerStats.starMakerGameSetAchievements.Add(setIndex, passedAchievement);
        }
        else
        {
            //gameManager.playerStats.starMakerGameSetAchievements[setIndex] = passedAchievement;
        }

        //SetPlayerStatsCurrentCompletedSets();
    }

    public void SetPlayerStatsCurrentCompletedSets()
    {
        gameManager.playerStats.currentCompletedSets.Clear();
        gameManager.playerStats.currentAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.starMakerGameCompletedSets.Count; i++)
        {
            var index = gameManager.playerStats.starMakerGameCompletedSets[i];
            gameManager.playerStats.currentCompletedSets.Add(index);
            //gameManager.playerStats.currentAchievements.Add(index, gameManager.playerStats.starMakerGameSetAchievements[index]);
        }
    }

    public void TransferPlayerStats()
    {

        hasWonGame = gameManager.playerStats.hasWonStarMakerGame;
        setCounter = gameManager.playerStats.StarMakerGameSetCount;

        completedSets.Clear();
        completedSetsAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.starMakerGameCompletedSets.Count; i++)
        {
            var setIndex = gameManager.playerStats.starMakerGameCompletedSets[i];

            if (setIndex < gameSets.Count)
            {
                completedSets.Add(gameSetsEasy[setIndex]);
            }
            else if (setIndex >= gameSets.Count && setIndex < (gameSets.Count * 2))
            {
                completedSets.Add(gameSets[setIndex - gameSets.Count]);
            }
            else if (setIndex >= (gameSets.Count * 2) && setIndex < (gameSets.Count * 3))
            {
                completedSets.Add(gameSetsHard[setIndex - (gameSets.Count * 2)]);
            }

            //completedSetsAchievements.Add(gameManager.playerStats.starMakerGameSetAchievements[setIndex]);
        }
    }

    public void StartSelectedGameSet(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        starMakerGameDifficulty = requestedDifficulty;

        if (requestedLevel < gameSets.Count)
            setCounter = requestedLevel;
        else if (requestedLevel >= gameSets.Count && requestedLevel < (gameSets.Count * 2))
            setCounter = requestedLevel - gameSets.Count;
        else if (requestedLevel >= gameSets.Count * 2)
            setCounter = requestedLevel - (gameSets.Count * 2);

        ClearCurrentSet();
        StartGameSet(setCounter);
    }   
}
