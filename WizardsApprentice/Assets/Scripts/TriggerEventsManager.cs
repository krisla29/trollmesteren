﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEventsManager : MonoBehaviour
{
    public delegate void TriggerEvents(Transform activeObject, Transform passiveTrigger);
    public static event TriggerEvents OnTriggerEvent;

    public void OnEnable()
    {
        
    }

    public void OnDisable()
    {
        
    }

    public void SendTriggerEvent(Transform activeObject, Transform passiveTrigger)
    {

    }
}
