﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGizmo : MonoBehaviour
{
    public enum GizmoTypes { Cube, Sphere, WireCube, WireSphere};
    public GizmoTypes gizmoType;
    public float size = 0.5f;
    public Color gizmoColor = Color.red;

    private void OnDrawGizmos()
    {
        Gizmos.color = gizmoColor;
        Gizmos.matrix = transform.localToWorldMatrix;
        if(gizmoType == GizmoTypes.Cube)
            Gizmos.DrawCube(Vector3.zero, new Vector3(size, size, size));
        if (gizmoType == GizmoTypes.Sphere)
            Gizmos.DrawSphere(Vector3.zero, size);
        if (gizmoType == GizmoTypes.WireCube)
            Gizmos.DrawWireCube(Vector3.zero, new Vector3(size, size, size));
        if (gizmoType == GizmoTypes.WireSphere)
            Gizmos.DrawWireSphere(Vector3.zero, size);
    }
}
