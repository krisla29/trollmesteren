﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AreaVolumeGame : MonoBehaviour, IDifficulty, ISetDifficulty, IAchievementPanel
{
    public bool drawGizmos;
    public GameManager gameManager;
    public PlayerStats.Difficulty areaVolumeGameDifficulty = PlayerStats.Difficulty.Normal;
    private int correctStreakCounter;
    private int wrongStreakCounter;
    public int numberOfCorrects = 5;
    public int numberOfWrongs = 3;
    public AchievementPanel achievementPanel;

    public Transform worldTransform;
    private Vector3 worldTransformStartPosition;
    private Vector3 worldTransformStartAngles;
    public Transform playerStart;
    public BoxCollider gridBounds;
    private Vector3 gridBoundsStartScale;
    public BoxCollider verticalRayCatcher;
    public Transform boundsRenderer;
    public AreaVolumeObject areaVolumeObject;
    public Transform manipulatorTransform;
    private Vector3 manipulatorTransformStartPos;
    public AreaVolumeBoundryObject avbo;
    public Material manipulatorMaterialX;
    public Material manipulatorMaterialZ;
    public Material manipulatorMaterialY;
    public BoxCollider boundingBox;
    public MeshRenderer gridRenderer;
    private Vector3 gridRendererStartSize;
    private Material gridMatInstance;
    public Transform voxelContainer;
    private Vector3 voxelContainerStartPos;
    public GameObject voxelPrefab;
    private float voxelSize = 0.5f;
    //public int gridSize = 30;
    public Vector2Int gridSize = new Vector2Int(15, 15);
    public int gridHeight = 10;
    public bool[,,] coordinateStatus = new bool[30, 30, 30];
    public Vector3[,,] grid = new Vector3[30, 30, 30];
    public bool useManipulatorCollider;
    public bool movePlayerToStart;

    public RectTransform setCountPanel;
    public Image setCountImage;
    public TextMeshProUGUI setCountPanelText;
    public GameObject winSetEffect;
    public Transform effectsParent;
    public GameObject gemPhysicalPrefab;
    public Transform spawnGemBox;
    private Vector3 spawnGemBoxStartPos;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.AreaVolumeGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;

    public Canvas gameCanvas;
    public CanvasGroup gameCanvasGroup;
    public Transform canvasMainRayBlocker;
    public RectTransform canvasUIClickBlocker;
    public List<RectTransform> buttonParents = new List<RectTransform>();
    public AreaVolumeCamera areaVolumeCameraScript;

    public List<AVG_GameSet> gameSets = new List<AVG_GameSet>();
    public List<AVG_GameSet> gameSetsEasy = new List<AVG_GameSet>();
    public List<AVG_GameSet> gameSetsHard = new List<AVG_GameSet>();
    public List<AVG_GameSet> completedSets = new List<AVG_GameSet>();
    public List<int> completedSetsAchievements = new List<int>();    

    public AVG_TargetVolumeUI currentTargetVolumeUIPrefab;
    public AVG_TargetVolumeUI nextTargetVolumeUIPrefab;
    public RectTransform currentTargetVolumePanel;
    public RectTransform nextTargetVolumePanel;
    public Color mainColor = new Color(0.4f, 0.6f, 0, 1);
    public Color finalTargetColor = new Color(0.5f, 0, 0.5f, 1);
    public Color subTargetColor = new Color(0.3f, 0, 0.7f, 1);
    private TextMeshProUGUI currentTargetText;
    private Image currentTargetBG;
    private Image currentTargetOutline;

    public TextMeshProUGUI currentShapeCountText;   
    public List<GameObject> voxelStartPrefabs = new List<GameObject>();

    Dictionary<string, Transform> dict = new Dictionary<string, Transform>();
    // namingConvention: "x_" + (x +  + intOffsetX).ToString() + "y_" + (y +  + intOffsetY).ToString() + "z_" + (z +  + intOffsetZ).ToString();

    private Vector3 initialOffset;
    public TextMeshProUGUI voxelCounterText;
    private int createBoxXCount;
    private int createBoxYCount;
    private int createBoxZCount;
    public TextMeshProUGUI createBoxXText;
    public TextMeshProUGUI createBoxYText;
    public TextMeshProUGUI createBoxZText;

    public Text moveYText;
    public Text X_Text;
    public Text Y_Text;
    public Text Z_Text;

    public CanvasGroup animatedNumberParent;
    public Text animatedNumberText;
    public Image animatedNumberTextBg;

    public TextMeshProUGUI targetShapes3Text;
    public TextMeshProUGUI targetShapes2Text;
    public TextMeshProUGUI targetShapes1Text;

    private int voxelCount;

    private float offsetX;
    private float offsetY;
    private float offsetZ;
    private int intOffsetX;
    private int intOffsetY;
    private int intOffsetZ;

    public enum GameTypes { Area, Volume, Line, Fixed, Locked }
    public GameTypes currentGameType = GameTypes.Volume;
    public enum ColorModes { Default, Single, Random}
    public ColorModes currentColorMode = ColorModes.Default;
    public GameObject currentVoxel;
    private bool hasStartedFirstSet;
    private bool hasOpenedFirstTime;
    public bool hasWonGame;
    private bool lastHasWonGame;

    public int setCounter = 0;
    public AVG_GameSet currentGameSet;
    public bool currentSetManipulator;
    public List<AVG_GameSet.TargetVolume> currentVolumeTargets;
    public int currentTargetVolumeCount;
    public int currentTargetVolume;
    public Vector3Int currentTargetShapes;
    public int currentShapes;

    //History:
    [System.Serializable]
    public class HistoryObject
    {
        public bool add;
        public List<Vector3Int> voxelPositions = new List<Vector3Int>();
        public Vector3 position;
        public Vector3 scale;
        public GameObject currentVoxel;
        public float sliderPosXprogression;
        public float sliderNegXprogression;
        public float sliderPosZprogression;
        public float sliderNegZprogression;
        public float sliderPosYprogression;
        public float sliderMoveYprogression;

        public List<AVG_GameSet.TargetVolume> currentVolumeTargets = new List<AVG_GameSet.TargetVolume>();
        public int currentTargetVolumeCount;
        public int currentTargetVolume;
        public int currentShapes;
    }

    public List<HistoryObject> historyObjects = new List<HistoryObject>();
    public HistoryObject currentHistoryObject = null;
    public int undoCueLength = 10;
    public TextMeshProUGUI historyCounterText;

    public List<Transform> objects_X = new List<Transform>();
    public List<Transform> objects_X_ = new List<Transform>();
    public List<Transform> objects_Y = new List<Transform>();
    public List<Transform> objects_Z = new List<Transform>();
    public List<Transform> objects_Z_ = new List<Transform>();
    public List<Transform> objects_M_XZ = new List<Transform>();
    public List<Transform> objects_M_Y = new List<Transform>();

    public List<Transform> textObjectsX = new List<Transform>();
    public List<Transform> textObjectsY = new List<Transform>();
    public List<Transform> textObjectsZ = new List<Transform>();

    public AudioSource soundAddVoxels;
    public AudioSource soundSubtractVoxels;
    public AudioSource soundUndo;
    public AudioSource soundUnavailable;
    public AudioSource soundStartLevel;
    public AudioSource soundReachGoal;
    public AudioSource soundBonus;
    public AudioSource soundSnap;

    public delegate void AreaVolumeGameEvent(); // Sends to PlayerStats
    public static event AreaVolumeGameEvent OnAreaVolumeGameEnable;
    public static event AreaVolumeGameEvent OnAreaVolumeGameDisable;

    public List<Transform> ObjectsToActivateOnComplete = new List<Transform>();
    public List<Transform> ObjectsToSleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    private void Awake()
    {
        worldTransformStartPosition = worldTransform.localPosition;
        worldTransformStartAngles = worldTransform.localEulerAngles;
        gridBoundsStartScale = gridBounds.transform.localScale;
        //print(worldTransform.localPosition);
        voxelContainerStartPos = voxelContainer.localPosition;
        initialOffset = voxelContainer.transform.localPosition;
        manipulatorTransformStartPos = manipulatorTransform.localPosition;
        gridRendererStartSize = gridRenderer.transform.localScale;

        var newMat = Instantiate(avbo.gridMatX);
        manipulatorMaterialX = newMat;
        foreach (var renderer in avbo.meshRenderersX)
        {
            renderer.material = manipulatorMaterialX;
        }

        var newMat2 = Instantiate(avbo.gridMatZ);
        manipulatorMaterialZ = newMat2;
        foreach (var renderer in avbo.meshRenderersZ)
        {
            renderer.material = manipulatorMaterialZ;
        }

        var newMat3 = Instantiate(avbo.gridMatY);
        manipulatorMaterialY = newMat3;
        foreach (var renderer in avbo.meshRenderersY)
        {
            renderer.material = manipulatorMaterialY;
        }

        spawnGemBoxStartPos = spawnGemBox.transform.position;

        if (useManipulatorCollider)
            avbo.thisCollider.enabled = true;
        else
            avbo.thisCollider.enabled = false;
              
    }
    private void Start()
    {
        areaVolumeObject.gameObject.SetActive(false);
        grid = new Vector3[30, 30, 30];
        SetVoxelSize();
        OffsetVoxelContainer();
        StartGameSet(0);

        hasStartedFirstSet = true;
        hasOpenedFirstTime = true;
    }

    private void Update()
    {
        UpdateCreateBox();
    }

    private void OnEnable()
    {
        TransferPlayerStats();
        lastHasWonGame = hasWonGame;

        if (hasStartedFirstSet)
            CheckIfHasCurrentTarget();

        if (!hasStartedFirstSet && hasOpenedFirstTime)
        {
            ResetGameSet();
            hasStartedFirstSet = true;
        }

        canvasMainRayBlocker.gameObject.SetActive(false);
        canvasUIClickBlocker.gameObject.SetActive(false);
        OpenDifficultyPanelStart();

        AreaVolumeButtons.OnAddButtonClick += AddVoxels;
        AreaVolumeButtons.OnSubtractButtonClick += SubtractVoxels;
        AreaVolumeButtons.OnResetButtonClick += ResetGameSet;
        AreaVolumeButtons.OnUndoButtonClick += Undo;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel += StartSelectedGameSet;

        if (OnAreaVolumeGameEnable != null)
            OnAreaVolumeGameEnable();
    }

    private void OnDisable()
    {
        AreaVolumeButtons.OnAddButtonClick -= AddVoxels;
        AreaVolumeButtons.OnSubtractButtonClick -= SubtractVoxels;
        AreaVolumeButtons.OnResetButtonClick -= ResetGameSet;
        AreaVolumeButtons.OnUndoButtonClick -= Undo;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel -= StartSelectedGameSet;

        ResetAnimatedNumber();

        if (setCountPanel.gameObject.activeSelf)
            setCountPanel.gameObject.SetActive(false);

        spawnGemBox.transform.position = spawnGemBoxStartPos;
        canvasMainRayBlocker.gameObject.SetActive(false);
        canvasUIClickBlocker.gameObject.SetActive(false);

        foreach (Transform child in effectsParent)
        {
            Destroy(child.gameObject);
        }

        StopAllCoroutines();
        CancelInvoke();

        if (OnAreaVolumeGameDisable != null)
            OnAreaVolumeGameDisable();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    void Initialize()
    {        
        InitializeManipulator();
        InitializeGridMat();
        InitializeBoxBounds();
        InitilizeGrid();
    }

    void CheckCurrentVolumeTarget()
    {
        if(voxelCount == currentTargetVolume)
        {
            if (soundReachGoal)
                soundReachGoal.PlayDelayed(0.5f);

            if(currentVolumeTargets[0].targetVolume == currentTargetVolume)
                currentVolumeTargets.Remove(currentVolumeTargets[0]);

            if (currentVolumeTargets.Count > 0)
            {
                currentTargetVolume = currentVolumeTargets[0].targetVolume;
                StartCoroutine(NextTarget(false));
            }
            else
            {
                StartCoroutine(NextTarget(true));
                //WinSet();
            }
        }       
    }

    private IEnumerator NextTarget(bool isLastTarget)
    {
        //yield return new WaitForSeconds(1f);

        float t = 0;
        float animTime = 0.75f;

        Color startColorText = currentTargetText.color;
        Color startColorBG = currentTargetBG.color;
        Color startColorOutline = currentTargetOutline.color;
        var curTarget = currentTargetVolumePanel.transform.GetChild(0);
        Transform curNextTarget = null;

        if(!isLastTarget)
            curNextTarget = nextTargetVolumePanel.transform.GetChild(0);

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);

            if (currentTargetText)
                currentTargetText.color = Vector4.Lerp(startColorText, mainColor, h);
            if (currentTargetBG)
                currentTargetBG.color = Vector4.Lerp(startColorBG, (mainColor + Color.white), h);
            if (currentTargetOutline)
                currentTargetOutline.color = Vector4.Lerp(startColorOutline, mainColor, h);
            
            yield return new WaitForEndOfFrame();
        }


        float ti = 0;
        float animTime2 = 0.5f;

        while(ti < 1)
        {
            ti += Time.deltaTime / animTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            curTarget.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);

            if(!isLastTarget)
                curNextTarget.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);

            yield return new WaitForEndOfFrame();
        }

        if (!isLastTarget)
        {
            DestroyCurrentTarget();
            SetNextTarget();
        }
        else
        {
            StartCoroutine(WinSet());
        }

        yield return null;
    }

    void SetNextTarget()
    {
        Destroy(nextTargetVolumePanel.transform.GetChild(0).gameObject);

        var curVol = Instantiate(currentTargetVolumeUIPrefab, currentTargetVolumePanel.transform);        
        curVol.targetVolumeText.text = currentTargetVolume.ToString();
        
        SetCurrentTargetComponents(curVol);
        CreateCurrentTargetVolumes();
    }

    void CreateCurrentTargetVolumes()
    {
        int index = currentGameSet.targetVolumes.IndexOf(currentVolumeTargets[0]);

        int voxelIndex = 0;

        if (currentGameSet.colorMode == ColorModes.Default)
            voxelIndex = voxelStartPrefabs.Count % (index + 1);
        else if (currentGameSet.colorMode == ColorModes.Single)
            voxelIndex = currentGameSet.startShapesVoxel;
        else if (currentGameSet.colorMode == ColorModes.Random)
            voxelIndex = Random.Range(0, voxelStartPrefabs.Count);

        for (int j = 0; j < currentGameSet.targetVolumes[index].startShapes.Count; j++)
        {
            if (!currentGameSet.targetVolumes[index].isNegativeShape[j])
                AddVoxelsFromObject(currentGameSet.targetVolumes[index].startShapes[j], voxelStartPrefabs[voxelIndex]);
            else
                SubtractVoxelsFromObject(currentGameSet.targetVolumes[index].startShapes[j]);
        }
    }

    void SetCurrentTargetComponents(AVG_TargetVolumeUI currentVolume)
    {
        currentTargetText = currentVolume.targetVolumeText;
        currentTargetBG = currentVolume.targetVolumeBGfill;
        currentTargetOutline = currentVolume.targetVolumeBGOutline;

        if (currentTargetVolume == currentGameSet.targetVolumes[currentGameSet.targetVolumes.Count - 1].targetVolume)
            currentTargetText.color = finalTargetColor;
        else
            currentTargetText.color = subTargetColor;
    }

    void DestroyCurrentTarget()
    {
        if (currentTargetVolumePanel.transform.childCount == 1)
            Destroy(currentTargetVolumePanel.transform.GetChild(0).gameObject);
    }

    void CheckIfHasCurrentTarget()
    {
        bool check = false;

        if (currentTargetVolumePanel.transform.childCount == 0)
            check = true;
        else
            check = false;

        if (check)
        {
            SetCurrentTarget();
        }
    }

    void SetCurrentTarget()
    {        
        DestroyCurrentTarget();

        var curVol = Instantiate(currentTargetVolumeUIPrefab, currentTargetVolumePanel.transform);
        curVol.targetVolumeText.text = currentTargetVolume.ToString();       

        foreach (Transform child in nextTargetVolumePanel.transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < currentVolumeTargets.Count; i++)
        {
            if (i > 0)
            {
                var nextVol = Instantiate(nextTargetVolumeUIPrefab, nextTargetVolumePanel.transform);
                nextVol.targetVolumeText.text = currentVolumeTargets[i].ToString();
            }
        }

        SetCurrentTargetComponents(curVol);
    }

    void SetPreviousTarget()
    {
        DestroyCurrentTarget();

        var curVol = Instantiate(currentTargetVolumeUIPrefab, currentTargetVolumePanel.transform);
        currentTargetVolume = currentHistoryObject.currentVolumeTargets[0].targetVolume;
        curVol.targetVolumeText.text = currentTargetVolume.ToString();
        
        foreach (Transform child in nextTargetVolumePanel.transform)
        {
            Destroy(child.gameObject);
        }
        
        for (int i = 0; i < currentHistoryObject.currentVolumeTargets.Count; i++)
        {
            if(i > 0)
            {
                var nextVol = Instantiate(nextTargetVolumeUIPrefab, nextTargetVolumePanel.transform);
                nextVol.targetVolumeText.text = currentHistoryObject.currentVolumeTargets[i].ToString();
            }
        }

        SetCurrentTargetComponents(curVol);
    }
    
    void DestroyVoxels()
    {
        foreach (Transform child in voxelContainer)
        {
            Destroy(child.gameObject);
        }
                
        for (int x = 0; x < 30; x++)
        {
            for (int y = 0; y < 30; y++)
            {
                for (int z = 0; z < 30; z++)
                {
                    coordinateStatus[x, y, z] = false;
                }
            }
        }

        voxelCount = 0;
        UpdateVoxelCount();
        dict.Clear();
        print("Destroyed voxels");
    }

    void AddSelectedVoxels(List<Vector3Int> positions)
    {
        if (soundAddVoxels)
            soundAddVoxels.Play();

        StopCoroutine(AnimateVoxelCount());
        StartCoroutine(AnimateVoxelCount());

        int oldCount = voxelCount;

        for (int i = 0; i < positions.Count; i++)
        {
            var newInstance = Instantiate(currentVoxel, voxelContainer);
            newInstance.name = "x_" + positions[i].x.ToString() + "y_" + positions[i].y.ToString() + "z_" + positions[i].z.ToString();
            newInstance.transform.localPosition = grid[positions[i].x, positions[i].y, positions[i].z];
            newInstance.transform.localScale = Vector3.one * voxelSize;
            coordinateStatus[positions[i].x, positions[i].y, positions[i].z] = true;
            dict.Add(newInstance.name, newInstance.transform);
            voxelCount += 1;
        }

        currentShapes++;
        currentShapeCountText.text = currentShapes.ToString();

        UpdateVoxelCount();

        StartCoroutine(AnimateNumber(avbo.transform.position, voxelCount - oldCount, 2f));
    }

    void RemoveSelectedVoxels(List<Vector3Int> positions)
    {
        if (soundSubtractVoxels)
            soundSubtractVoxels.Play();

        StopCoroutine(AnimateVoxelCount());
        StartCoroutine(AnimateVoxelCount());

        int oldCount = voxelCount;

        for (int i = 0; i < positions.Count; i++)
        {
            var objectToKill = dict["x_" + positions[i].x.ToString() + "y_" + positions[i].y.ToString() + "z_" + positions[i].z.ToString()].gameObject;
            dict.Remove(objectToKill.name);
            Destroy(objectToKill);
            voxelCount -= 1;
            coordinateStatus[positions[i].x, positions[i].y, positions[i].z] = false;
        }

        currentShapes--;
        currentShapeCountText.text = currentShapes.ToString();

        UpdateVoxelCount();

        StartCoroutine(AnimateNumber(avbo.transform.position, voxelCount - oldCount, 2f));
    }
   
    void AddVoxels()
    {
        int randIndex = Random.Range(0, voxelStartPrefabs.Count);
        int oldCount = voxelCount;

        StopCoroutine(AnimateVoxelCount());
        StartCoroutine(AnimateVoxelCount());

        CalculateOffset();

        CreateHistoryObject(true);

        if (soundAddVoxels)
            soundAddVoxels.Play();

        for (int x = 0; x < avbo.currentVolumeGameUnits.x; x++)
        {
            for (int y = 0; y < avbo.currentVolumeGameUnits.y; y++)
            {
                for (int z = 0; z < avbo.currentVolumeGameUnits.z; z++)
                {
                    if (coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ])
                    { }
                    else
                    {
                        var newInstance = Instantiate(currentVoxel, voxelContainer);
                        newInstance.name = "x_" + (x + +intOffsetX).ToString() + "y_" + (y + +intOffsetY).ToString() + "z_" + (z + +intOffsetZ).ToString();
                        newInstance.transform.localPosition = grid[x + intOffsetX, y + intOffsetY, z + intOffsetZ];
                        newInstance.transform.localScale = Vector3.one * voxelSize;
                        coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ] = true;

                        currentHistoryObject.voxelPositions.Add(new Vector3Int(x + intOffsetX, y + intOffsetY, z + intOffsetZ));

                        dict.Add(newInstance.name, newInstance.transform);
                        voxelCount += 1;
                    }
                }
            }
        }

        currentShapes++;
        currentShapeCountText.text = currentShapes.ToString();

        UpdateVoxelCount();

        StartCoroutine(AnimateNumber(avbo.transform.position, voxelCount - oldCount, 2f));
        
        CheckCurrentVolumeTarget();
    }

    void AddVoxelsFromObject(Transform obj, GameObject voxelPF)
    {
        CalculateOffsetFromObject(obj, obj.GetChild(1));

        var volume = avbo.CalculateVolumeFromObject(obj.GetChild(0));

        for (int x = 0; x < volume.x; x++)
        {
            for (int y = 0; y < volume.y; y++)
            {
                for (int z = 0; z < volume.z; z++)
                {
                    if (coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ])
                    { }
                    else
                    {
                        var newInstance = Instantiate(voxelPF, voxelContainer);
                        newInstance.name = "x_" + (x + +intOffsetX).ToString() + "y_" + (y + +intOffsetY).ToString() + "z_" + (z + +intOffsetZ).ToString();
                        newInstance.transform.localPosition = grid[x + intOffsetX, y + intOffsetY, z + intOffsetZ];
                        newInstance.transform.localScale = Vector3.one * voxelSize;
                        coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ] = true;
                        dict.Add(newInstance.name, newInstance.transform);
                        voxelCount += 1;
                    }
                }
            }
        }

        UpdateVoxelCount();
    }

    void SubtractVoxels()
    {        
        int oldCount = voxelCount;

        StopCoroutine(AnimateVoxelCount());
        StartCoroutine(AnimateVoxelCount());

        CalculateOffset();

        CreateHistoryObject(false);

        if (soundSubtractVoxels)
            soundSubtractVoxels.Play();

        for (int x = 0; x < avbo.currentVolumeGameUnits.x; x++)
        {
            for (int y = 0; y < avbo.currentVolumeGameUnits.y; y++)
            {
                for (int z = 0; z < avbo.currentVolumeGameUnits.z; z++)
                {
                    if (coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ])
                    {
                        var objectToKill = dict["x_" + (x + +intOffsetX).ToString() + "y_" + (y + +intOffsetY).ToString() + "z_" + (z + +intOffsetZ).ToString()].gameObject;
                        dict.Remove(objectToKill.name);
                        Destroy(objectToKill);

                        voxelCount -= 1;

                        coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ] = false;

                        currentHistoryObject.voxelPositions.Add(new Vector3Int(x + intOffsetX, y + intOffsetY, z + intOffsetZ));
                    }
                    else
                    { }
                }
            }
        }

        currentShapes++;
        currentShapeCountText.text = currentShapes.ToString();

        UpdateVoxelCount();

        StartCoroutine(AnimateNumber(avbo.transform.position, voxelCount - oldCount, 2f));

        CheckCurrentVolumeTarget();
    }

    void SubtractVoxelsFromObject(Transform obj)
    {
        CalculateOffsetFromObject(obj, obj.GetChild(1));

        var volume = avbo.CalculateVolumeFromObject(obj.GetChild(0));

        for (int x = 0; x < volume.x; x++)
        {
            for (int y = 0; y < volume.y; y++)
            {
                for (int z = 0; z < volume.z; z++)
                {
                    if (coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ])
                    { 
                        var objectToKill = dict["x_" + (x + +intOffsetX).ToString() + "y_" + (y + +intOffsetY).ToString() + "z_" + (z + +intOffsetZ).ToString()].gameObject;
                        dict.Remove(objectToKill.name);
                        Destroy(objectToKill);
                        voxelCount -= 1;
                        coordinateStatus[x + intOffsetX, y + intOffsetY, z + intOffsetZ] = false;
                    }
                    else
                    { }
                }
            }
        }

        UpdateVoxelCount();
    }

    void CalculateOffset()
    {
        //A half unit is added in X and Z to move voxels between grid lines.
        offsetX = (areaVolumeObject.transform.localPosition.x / voxelSize) - (initialOffset.x / voxelSize) + (avbo.bounds[3].localPosition.x / voxelSize);
        offsetY = (areaVolumeObject.transform.localPosition.y / voxelSize) - initialOffset.y;
        offsetZ = (areaVolumeObject.transform.localPosition.z / voxelSize) - (initialOffset.z / voxelSize) + (avbo.bounds[3].localPosition.z / voxelSize);

        intOffsetX = Mathf.RoundToInt(offsetX);
        intOffsetY = Mathf.RoundToInt(offsetY);
        intOffsetZ = Mathf.RoundToInt(offsetZ);

        intOffsetX = Mathf.RoundToInt(offsetX);
        intOffsetY = Mathf.RoundToInt(offsetY);
        intOffsetZ = Mathf.RoundToInt(offsetZ);
    }

    void CalculateOffsetFromObject(Transform objParent, Transform bottomLeftCorner)
    {
        //A half unit is added in X and Z to move voxels between grid lines.
        offsetX = (objParent.localPosition.x / voxelSize) - (initialOffset.x / voxelSize) + (bottomLeftCorner.localPosition.x / voxelSize);
        offsetY = (objParent.localPosition.y / voxelSize) - initialOffset.y;
        offsetZ = (objParent.localPosition.z / voxelSize) - (initialOffset.z / voxelSize) + (bottomLeftCorner.localPosition.z / voxelSize);                

        intOffsetX = Mathf.RoundToInt(offsetX);
        intOffsetY = Mathf.RoundToInt(offsetY);
        intOffsetZ = Mathf.RoundToInt(offsetZ);

        intOffsetX = Mathf.RoundToInt(offsetX);
        intOffsetY = Mathf.RoundToInt(offsetY);
        intOffsetZ = Mathf.RoundToInt(offsetZ);
    }

    void UpdateVoxelCount()
    {
        voxelCounterText.text = voxelCount.ToString();  
    }

    void UpdateCreateBox()
    {
        createBoxXCount = Mathf.RoundToInt(avbo.currentVolumeGameUnits.x);
        createBoxYCount = Mathf.RoundToInt(avbo.currentVolumeGameUnits.y);
        createBoxZCount = Mathf.RoundToInt(avbo.currentVolumeGameUnits.z);

        string textX = "X ";
        string textY = "Y ";
        string textZ = "Z ";

        if (!currentGameSet.X && !currentGameSet.Y && !currentGameSet.Z) { }
        else if(currentGameSet.X && !currentGameSet.Y && !currentGameSet.Z) { textY = "X "; textZ = "Y ";}
        else if (!currentGameSet.X && currentGameSet.Y && !currentGameSet.Z) { textZ = "Y "; }

        if (moveYText)
            moveYText.text = (areaVolumeObject.sliderMoveY.segmentProgressionRound * 10).ToString();
        
        if(createBoxXText)
            createBoxXText.text = textX + createBoxXCount.ToString();
        if (createBoxYText)
            createBoxYText.text = textY + createBoxYCount.ToString();
        if (createBoxZText)
            createBoxZText.text = textZ + createBoxZCount.ToString();
       
        if (X_Text)
            X_Text.text = textX + createBoxXCount.ToString();        
        if (Y_Text)
            Y_Text.text = textY + createBoxYCount.ToString();
        if (Z_Text)
            Z_Text.text = textZ + createBoxZCount.ToString();
       
    }       

    void InitilizeGrid()
    {
        for (int x = 0; x < 30; x++)
        {
            for (int y = 0; y < 30; y++)
            {
                for (int z = 0; z < 30; z++)
                {
                    coordinateStatus[x, y, z] = false;
                    grid[x, y, z] = new Vector3(x, y, z) * voxelSize;
                }
            }
        }
    }
    
    void SetVoxelSize()
    {
        voxelSize = 0.5f;
    }
    
    void OffsetVoxelContainer()
    {
        voxelContainer.localPosition = voxelContainerStartPos + new Vector3((voxelSize * 0.5f), (voxelSize * 0.5f), (voxelSize * 0.5f));
    }    

    public void InitializeManipulator()
    {
        if (!currentSetManipulator)
        {
            /*
            avbo.stepSize = 2;
            //var boundry = (gridSize / 2.0f) / 2;
            //boundry = Mathf.Round(boundry * 2.0f) / 2.0f;
            var boundry = new Vector2((gridSize.x / 2.0f) / 2, (gridSize.y / 2.0f) / 2);
            boundry = new Vector2((Mathf.Round(boundry.x * 2.0f) / 2.0f), (Mathf.Round(boundry.y * 2.0f) / 2.0f));
            avbo.boundry = boundry;
            avbo.topBoundry = gridHeight;
            areaVolumeObject.transform.localScale = Vector3.one;
            Vector3 pos = Vector3.zero;
            areaVolumeObject.transform.localPosition = pos;
            avbo.startPos = pos;
            avbo.lastPos = pos;
            avbo.currentPos = pos;
            avbo.isPlaced = true;
            areaVolumeObject.gameObject.SetActive(true);
            print("HAHA");
            */
            
            SetManipulator(Vector3.zero, 1, 1, 1, 1, 1);
            areaVolumeObject.gameObject.SetActive(true);
            
        }
        else
        {
            var gs = currentGameSet;
            SetManipulator(gs.m_position, gs.posX, gs.negX, gs.posZ, gs.negZ, gs.posY);
            areaVolumeObject.gameObject.SetActive(true);
        }
    }

    void SetManipulator(Vector3 position, float posX, float negX, float posZ, float negZ, float posY)
    {
        avbo.isPlaced = false;

        avbo.stepSize = 2;

        var boundry = new Vector2((gridSize.x / 2.0f) / 2, (gridSize.y / 2.0f) / 2);
        boundry = new Vector2((Mathf.Round(boundry.x * 2.0f) / 2.0f), (Mathf.Round(boundry.y * 2.0f) / 2.0f));

        avbo.boundry = boundry;
        avbo.topBoundry = gridHeight;
        areaVolumeObject.transform.localScale = Vector3.one;

        areaVolumeObject.transform.localPosition = position;
        Vector3 scale = new Vector3((Mathf.Abs(posX) + Mathf.Abs(negX)) / 2, Mathf.Abs(posY) / 2, (Mathf.Abs(posZ) + Mathf.Abs(negZ)) / 2);
        avbo.transform.localScale = scale;

        areaVolumeObject.sliderXPos.segmentProgression = Mathf.Abs(posX) / 10f;
        areaVolumeObject.sliderXPos.InitializeSlider();
        areaVolumeObject.sliderXPos.SliderPosition();
        areaVolumeObject.sliderXNeg.segmentProgression = Mathf.Abs(negX) / 10f;
        areaVolumeObject.sliderXNeg.InitializeSlider();
        areaVolumeObject.sliderXNeg.SliderPosition();
        areaVolumeObject.sliderZPos.segmentProgression = Mathf.Abs(posZ) / 10f;
        areaVolumeObject.sliderZPos.InitializeSlider();
        areaVolumeObject.sliderZPos.SliderPosition();
        areaVolumeObject.sliderZNeg.segmentProgression = Mathf.Abs(negZ) / 10f;
        areaVolumeObject.sliderZNeg.InitializeSlider();
        areaVolumeObject.sliderZNeg.SliderPosition();
        areaVolumeObject.sliderY.segmentProgression = Mathf.Abs(posY) / 10f;
        areaVolumeObject.sliderY.InitializeSlider();
        areaVolumeObject.sliderY.SliderPosition();
        areaVolumeObject.sliderMoveY.segmentProgression = Mathf.Abs(position.y) / 10f;
        areaVolumeObject.sliderMoveY.InitializeSlider();
        areaVolumeObject.sliderMoveY.SliderPosition();

        avbo.lastPos = position;
        avbo.currentPos = position;
        avbo.PlaceCorners();
        avbo.PositionObject();
        avbo.PositionHeight();
        avbo.TransformMoveHandle();
        areaVolumeObject.ScaleBoundryObject();
        avbo.isPlaced = true;
    }    

    void InitializeGridMat()
    {
        Vector2 difference = new Vector2(30 - gridSize.x, 30 - gridSize.y);
        gridMatInstance = Instantiate(gridRenderer.material);
        if(gridMatInstance.HasProperty("_TextureSample0"))
            gridMatInstance.SetTextureScale("_TextureSample0", new Vector2(30 - difference.x, 30 - difference.y));
        else
            gridMatInstance.mainTextureScale = new Vector2(30 - difference.x, 30 - difference.y);
        var newMaterial = gridMatInstance;
        var oldMaterial = gridRenderer.material;
        gridRenderer.material = newMaterial;
        Destroy(oldMaterial);
        gridRenderer.transform.localScale = new Vector3(gridSize.x / 2, gridSize.y / 2, 1);
    }
    /*
    void InitializeGridMat()
    {
        var difference = 30 - gridSize;
        gridMatInstance = Instantiate(gridRenderer.material);
        gridMatInstance.SetTextureScale("_TextureSample0", new Vector2(30 - difference, 30 - difference));
        gridRenderer.material = gridMatInstance;
        gridRenderer.transform.localScale = new Vector3(gridSize / 2, gridSize / 2, 1);
    }
    */
    void InitializeBoxBounds()
    {
        gridBounds.transform.localScale = new Vector3(gridSize.x / 2, gridHeight, gridSize.y / 2);
    }   

    void CheckLockedManipulators()
    {
        if (currentGameSet.X) { foreach (var item in objects_X) { item.gameObject.SetActive(false); } } else { foreach (var item in objects_X) { item.gameObject.SetActive(true); } }
        if (currentGameSet.X_) { foreach (var item in objects_X_) { item.gameObject.SetActive(false); } } else { foreach (var item in objects_X_) { item.gameObject.SetActive(true); } }
        if (currentGameSet.Y) { foreach (var item in objects_Y) { item.gameObject.SetActive(false); } } else { foreach (var item in objects_Y) { item.gameObject.SetActive(true); } }
        if (currentGameSet.Z) { foreach (var item in objects_Z) { item.gameObject.SetActive(false); } } else { foreach (var item in objects_Z) { item.gameObject.SetActive(true); } }
        if (currentGameSet.Z_) { foreach (var item in objects_Z_) { item.gameObject.SetActive(false); } } else { foreach (var item in objects_Z_) { item.gameObject.SetActive(true); } }
        if (currentGameSet.Move_Y) { foreach (var item in objects_M_Y) { item.gameObject.SetActive(false); } } else { foreach (var item in objects_M_Y) { item.gameObject.SetActive(true); } }
        if (currentGameSet.Move_XZ)
        {
            foreach (var item in objects_M_XZ)
                { item.gameObject.SetActive(false); }

            if(useManipulatorCollider)
                avbo.thisCollider.enabled = false;
        }
        else
        {
            foreach (var item in objects_M_XZ)
            { item.gameObject.SetActive(true); }

            if(useManipulatorCollider)
                avbo.thisCollider.enabled = true;
        }
    }

    void CheckVisibleAxesText()
    {
        if (currentGameSet.showX) { foreach (var item in textObjectsX) { item.gameObject.SetActive(true); } } else { foreach (var item in textObjectsX) { item.gameObject.SetActive(false); } }
        if (currentGameSet.showY) { foreach (var item in textObjectsY) { item.gameObject.SetActive(true); } } else { foreach (var item in textObjectsY) { item.gameObject.SetActive(false); } }
        if (currentGameSet.showZ) { foreach (var item in textObjectsZ) { item.gameObject.SetActive(true); } } else { foreach (var item in textObjectsZ) { item.gameObject.SetActive(false); } }
    }

    void SetCurrentTargetShapesText()
    {
        targetShapes3Text.text = currentTargetShapes.x.ToString();
        targetShapes2Text.text = currentTargetShapes.y.ToString();
        targetShapes1Text.text = currentTargetShapes.z.ToString();
    }

    void ScaleWorld()
    {
        worldTransform.localScale = Vector3.one * currentGameSet.worldScale;
    }

    void SetCameraScale()
    {
        areaVolumeCameraScript.worldScaleMultipler = currentGameSet.worldScale - 1;
    }

    void StartGameSet(int gameSetIndex)
    {
        StopCoroutine(FadeInObjects());
        StopCoroutine(FadeOutObjects());
        StartCoroutine(FadeInObjects());
        SetGameSetWithDifficulty(gameSetIndex);
        canvasMainRayBlocker.gameObject.SetActive(false);
        canvasUIClickBlocker.gameObject.SetActive(false);
        //currentGameSet = gameSets[gameSetIndex];
        StopCoroutine(ShowSetCountPanel());
        StartCoroutine(ShowSetCountPanel());

        voxelCounterText.color = mainColor;

        gridSize = currentGameSet.gridSize;
        gridHeight = currentGameSet.gridHeight;

        currentTargetVolumeCount = currentGameSet.targetVolumes.Count;
        currentTargetShapes = currentGameSet.targetShapes;
        SetCurrentTargetShapesText();
        currentSetManipulator = currentGameSet.setManipulator;

        Initialize();
        CheckLockedManipulators();
        CheckVisibleAxesText();

        if (soundStartLevel)
            soundStartLevel.Play();        

        if (movePlayerToStart)
        {
            gameManager.player.navMeshAgent.Warp(playerStart.position);
            //gameManager.player.navMeshAgent.SetDestination(playerStart.position);
            gameManager.player.transform.rotation = playerStart.rotation;
        }
        //
        //Set Target volumes:
        for (int i = 0; i < currentGameSet.targetVolumes.Count; i++)
        {
            int index = 0;

            if (currentGameSet.colorMode == ColorModes.Default)
                index = voxelStartPrefabs.Count % (i + 1);
            else if (currentGameSet.colorMode == ColorModes.Single)
                index = currentGameSet.startShapesVoxel;
            else if (currentGameSet.colorMode == ColorModes.Random)
                index = Random.Range(0, voxelStartPrefabs.Count);

           

            currentVolumeTargets.Add(currentGameSet.targetVolumes[i]);

            if (i == 0)
            {
                for (int j = 0; j < currentGameSet.targetVolumes[0].startShapes.Count; j++)
                {
                    if (!currentGameSet.targetVolumes[0].isNegativeShape[j])
                        AddVoxelsFromObject(currentGameSet.targetVolumes[0].startShapes[j], voxelStartPrefabs[index]);
                    else
                        SubtractVoxelsFromObject(currentGameSet.targetVolumes[0].startShapes[j]);
                }

                var curVol = Instantiate(currentTargetVolumeUIPrefab, currentTargetVolumePanel.transform);
                currentTargetVolume = currentGameSet.targetVolumes[0].targetVolume;
                curVol.targetVolumeText.text = currentTargetVolume.ToString();

                currentTargetText = curVol.targetVolumeText;
                currentTargetBG = curVol.targetVolumeBGfill;
                currentTargetOutline = curVol.targetVolumeBGOutline;

                if (currentGameSet.targetVolumes.Count > 1)
                    curVol.targetVolumeText.color = subTargetColor;
                else
                    curVol.targetVolumeText.color = finalTargetColor;
                
            }
            else
            {
                var nextVol = Instantiate(nextTargetVolumeUIPrefab, nextTargetVolumePanel.transform);
                nextVol.targetVolumeText.text = currentGameSet.targetVolumes[i].targetVolume.ToString();
                nextVol.targetVolumeText.color = subTargetColor;
            }
        }
        //
        //Set World Options:
        if (currentGameSet.showBounds) { boundsRenderer.gameObject.SetActive(true); } else { boundsRenderer.gameObject.SetActive(false); }
        ScaleWorld();
        SetCameraScale();
        //
        //Set current Voxel:
        if (currentGameSet.colorMode == ColorModes.Default)
            currentVoxel = voxelPrefab;
        else if (currentGameSet.colorMode == ColorModes.Single)
        {
            if (currentGameSet.playerVoxel)
                currentVoxel = currentGameSet.playerVoxel;
            else
                currentVoxel = voxelPrefab;
        }
        else if (currentGameSet.colorMode == ColorModes.Random)
        {
            int index = Random.Range(0, voxelStartPrefabs.Count);
            currentVoxel = voxelStartPrefabs[index];
        }
        //
        areaVolumeCameraScript.perspectiveToggle.isOn = !currentGameSet.camIsOrthographic;
        //Set World:
        if (currentGameSet.isVertical)
        {
            worldTransform.localEulerAngles = new Vector3(-90, worldTransformStartAngles.y + currentGameSet.worldYrotation, worldTransformStartAngles.z);
            var pos = currentGameSet.worldLocalPos;
            worldTransform.localPosition = new Vector3(worldTransformStartPosition.x + pos.x, worldTransformStartPosition.y + pos.y, worldTransformStartPosition.z + pos.z);

            areaVolumeObject.sliderZPos.inputPlane = InteractableBehaviourSlidePath2.InputPlanes.Vertical;
            areaVolumeObject.sliderZNeg.inputPlane = InteractableBehaviourSlidePath2.InputPlanes.Vertical;

            areaVolumeObject.sliderZNeg.isInverted = true;

            foreach (var item in objects_Y) { item.gameObject.SetActive(false); }
            foreach (var item in objects_M_Y) { item.gameObject.SetActive(false); }
            foreach (var item in textObjectsY) { item.gameObject.SetActive(false); }

            areaVolumeCameraScript.isHorizontal = false;

            avbo.isVerticalMovement = true;
        }
        else
        {
            worldTransform.localPosition = worldTransformStartPosition;
            worldTransform.localEulerAngles = worldTransformStartAngles;

            areaVolumeObject.sliderZPos.inputPlane = InteractableBehaviourSlidePath2.InputPlanes.Horizontal;
            areaVolumeObject.sliderZNeg.inputPlane = InteractableBehaviourSlidePath2.InputPlanes.Horizontal;

            areaVolumeObject.sliderZNeg.isInverted = false;

            areaVolumeCameraScript.isHorizontal = true;
            avbo.isVerticalMovement = false;
        }

        historyCounterText.text = historyObjects.Count.ToString();
        currentShapes = 0;
        currentShapeCountText.text = currentShapes.ToString();

        gameManager.playerStats.SaveGame();
    }

    void ClearUIPanels()
    {
        if(currentTargetVolumePanel.transform.childCount == 1)
            Destroy(currentTargetVolumePanel.transform.GetChild(0).gameObject);

        foreach (Transform item in nextTargetVolumePanel.transform)
        {
            Destroy(item.gameObject);
        }
    }

    void ActivateUIButtons(bool activate)
    {
        
    }

    public void ResetGameSet()
    {
        DestroyVoxels();
        ClearUIPanels();
        currentVolumeTargets.Clear();
        ResetAnimatedNumber();
        ClearAllHistory();          
        voxelCounterText.transform.localScale = Vector3.one;
        StartGameSet(setCounter);
    }

    public void ClearGameSet()
    {
        DestroyVoxels();
        ClearUIPanels();
        currentVolumeTargets.Clear();
        ResetAnimatedNumber();
        ClearAllHistory();
        voxelCounterText.transform.localScale = Vector3.one;
        Resources.UnloadUnusedAssets();
    }

    private IEnumerator WinSet()
    {
        areaVolumeCameraScript.perspectiveToggle.isOn = true;
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        canvasMainRayBlocker.gameObject.SetActive(true);
        canvasUIClickBlocker.gameObject.SetActive(true);
        StopCoroutine(FadeInObjects());
        StopCoroutine(FadeOutObjects());
        StartCoroutine(FadeOutObjects());
        StopCoroutine(ChangeColorVoxelCount());
        StartCoroutine(ChangeColorVoxelCount());

        gameManager.player.anim.SetTrigger("Success");

        if (setCounter >= gameSets.Count - 1)
        {
            if (!hasWonGame)
            {
                gameManager.playerStats.hasWonAreaVolumeGame = true;

                foreach (var item in ObjectsToActivateOnComplete)
                {
                    item.gameObject.SetActive(true);
                }

                foreach (var item in ObjectsToSleepOnComplete)
                {
                    item.gameObject.SetActive(false);
                }

                hasWonGame = true;
            }
        }

        var achievement = CheckCurrentAchievement();
        var prevAchievement = CheckPrevAchievement();
        var highetsAchievement = 0;

        if (achievement >= prevAchievement)
            highetsAchievement = achievement;
        else
            highetsAchievement = prevAchievement;

        AddCompletedSetAndAchievement(achievement);
        SetCurrentCompletedSet(achievement);

        OpenAchievementPanel(achievementPanel, areaVolumeGameDifficulty, setCounter + 1, gameSets.Count, prevAchievement, achievement);

        CorrectTicker();

        if (setCounter >= gameSets.Count - 1)
        {
            setCounter = 0;

            //StartCoroutine(WinGame());
        }
        else
        {
            setCounter++;

            //StartCoroutine(StartNextSet());
        }

        gameManager.playerStats.AreaVolumeGameSetCount = setCounter;

        yield return new WaitForSeconds(6);

        RewardPlayer(achievement, prevAchievement);

        yield return new WaitForSeconds(4);

        if (setCounter == 0)
            StartCoroutine(WinGame());
        else
            StartCoroutine(StartNextSet());

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        yield return null;
    }

    void AddCompletedSetAndAchievement(int passedAchievement)
    {
        if (!completedSets.Contains(currentGameSet))
        {
            completedSets.Add(currentGameSet);
            completedSetsAchievements.Add(passedAchievement);
        }
    }

    void CheckCurrentShapes()
    {

    }

    private IEnumerator StartNextSet()
    {
        ClearGameSet();
        StartGameSet(setCounter);
        yield return null;
    }

    private IEnumerator WinGame()
    {
        if(!lastHasWonGame)
        {
            trophyPanel.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            if (soundReachGoal)
                soundReachGoal.Play();
            trophyGraphic.gameObject.SetActive(true);
            yield return new WaitForSeconds(3);           
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
            trophyGraphic.gameObject.SetActive(false);
            trophyPanel.gameObject.SetActive(false);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
            gameManager.playerStats.trophies.Add(trophy);
            gameManager.playerStats.hasWonAreaVolumeGame = true;
        }

        lastHasWonGame = true;
        ClearGameSet();
        hasStartedFirstSet = false;
        gameManager.playerStats.SaveGame();
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
        yield return null;
    }

    void RewardPlayer(int passedAchievement, int passedOldAchievement)
    {
        int reward = (passedAchievement - passedOldAchievement) * 5;

        if (reward <= 0)
            reward = 1;

        if (reward > 1)
        {
            gameManager.player.anim.SetTrigger("Success");

            if (soundBonus)
                soundBonus.Play();
        }

        ReleaseGems(reward);
    }

    private void ReleaseGems(int bonus)
    {
        spawnGemBox.position = gameManager.player.transform.position + (Vector3.up * 5);

        Invoke("SpawnEffect", 0.5f);

        for (int i = 0; i < bonus; i++)
        {
            var randTime = Random.Range(0, 1f);
            Invoke("SpawnGem", randTime);
        }
    }

    void SpawnGem()
    {
        var newPos = HelperFunctions.RandomPointInBox(spawnGemBox.transform);
        var newGem = Instantiate(gemPhysicalPrefab, newPos, Quaternion.identity, null);
    }

    void SpawnEffect()
    {
        var newEffect = Instantiate(winSetEffect, spawnGemBox.position, Quaternion.identity, effectsParent);
    }


    //ANIMATION COROUTINES-----------------------------------------------------------------------

    private IEnumerator AnimateNumber(Vector3 pos, int value, float totalTime)
    {
        animatedNumberParent.transform.position = pos;
        animatedNumberText.color = Color.white;

        if(value >= 0)
            animatedNumberText.text = "+" + value.ToString();
        else
            animatedNumberText.text = value.ToString();

        animatedNumberTextBg.color = Color.black;
        animatedNumberParent.alpha = 0;
        animatedNumberParent.gameObject.SetActive(true);

        float size = 2f * currentGameSet.worldScale;
        Vector3 endPos = pos + (Vector3.up * 5f);
        Color col = new Color(0.5f, 0.5f, 1, 1);
        Color bgStartCol = new Color(col.r, col.g, col.b, 0.75f);
        Color bgEndCol = new Color(col.r, col.g, col.b, 0.25f);
        Color startCol = Color.white;
        Color endCol = Color.white;

        animatedNumberParent.transform.localScale = Vector3.one * size;

        float t = 0;
        float lerpTime = totalTime;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);            
            animatedNumberParent.transform.position = Vector3.Lerp(pos, endPos, h);
            animatedNumberText.color = Vector4.Lerp(startCol, endCol, h);
            animatedNumberTextBg.color = Vector4.Lerp(bgStartCol, bgEndCol, h);
            animatedNumberParent.alpha = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }
        
        animatedNumberParent.gameObject.SetActive(false);
    }

    private void ResetAnimatedNumber()
    {
        animatedNumberParent.transform.position = Vector3.zero;
        animatedNumberText.color = Color.white;
        animatedNumberText.text = "";
        animatedNumberTextBg.color = Color.black;
        animatedNumberParent.alpha = 0;
        animatedNumberParent.gameObject.SetActive(false);
    }

    private IEnumerator AnimateVoxelCount()
    {
        float t = 0;
        float lerpTime = 0.25f;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            voxelCounterText.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 1.15f, h);
            yield return new WaitForEndOfFrame();
        }

        float ti = 0;
        float lerpTime2 = 0.25f;

        while (ti < 1)
        {
            ti += Time.deltaTime / lerpTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            voxelCounterText.transform.localScale = Vector3.Lerp(Vector3.one * 1.15f, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

    }

    private IEnumerator ChangeColorVoxelCount()
    {
        float t = 0;
        float lerpTime = 0.5f;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            voxelCounterText.color = Color.Lerp(mainColor, finalTargetColor, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ShowSetCountPanel()
    {
        if (areaVolumeGameDifficulty == PlayerStats.Difficulty.Easy)
            setCountImage.color = gameManager.colorEasy;
        else if (areaVolumeGameDifficulty == PlayerStats.Difficulty.Normal)
            setCountImage.color = gameManager.colorNormal;
        else if (areaVolumeGameDifficulty == PlayerStats.Difficulty.Hard)
            setCountImage.color = gameManager.colorHard;
        else
            setCountImage.color = Color.grey;

        setCountPanelText.text = (setCounter + 1).ToString() + "/" + gameSets.Count.ToString();

        if (setCountPanelText.text.Length < 4)
            setCountPanelText.fontSize = 220;
        else if (setCountPanelText.text.Length == 4)
            setCountPanelText.fontSize = 200;
        else if (setCountPanelText.text.Length > 4)
            setCountPanelText.fontSize = 160;

        setCountPanel.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        setCountPanel.gameObject.SetActive(false);
    }

    private IEnumerator FadeInObjects()
    {
        foreach (var item in buttonParents)
        {
            item.gameObject.SetActive(true);
        }

        float t = 0;
        float time = 1;

        while (t < 1)
        {
            t += Time.deltaTime / time;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            //gameCanvasGroup.alpha = Mathf.Lerp(0, 1, h);

            foreach (var item in buttonParents)
            {
                item.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FadeOutObjects()
    {
        float t = 0;
        float time = 1;

        while (t < 1)
        {
            t += Time.deltaTime / time;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            //gameCanvasGroup.alpha = Mathf.Lerp(1, 0, h);

            foreach (var item in buttonParents)
            {
                item.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            }

            yield return new WaitForEndOfFrame();
        }

        foreach (var item in buttonParents)
        {
            item.gameObject.SetActive(false);
        }
    }

    void ResetUIObjects()
    {
        foreach (var item in buttonParents)
        {
            item.transform.localScale = Vector3.one;
            item.gameObject.SetActive(true);
        }

        gameCanvasGroup.alpha = 1;
    }

    //HISTORY:-----------------------------------------------------------------------------------------------------------------

    void ClearAllHistory()
    {
        currentHistoryObject = null;
        historyObjects.Clear();        
    }

    void CreateHistoryObject(bool add)
    {
        var newHistoryObject = new HistoryObject();

        if (add)  //Check to see operationtype
            newHistoryObject.add = true;
        else
            newHistoryObject.add = false;

        newHistoryObject.position = areaVolumeObject.transform.localPosition;
        newHistoryObject.scale = avbo.transform.localScale;
        newHistoryObject.sliderPosXprogression = areaVolumeObject.sliderXPos.segmentProgression;
        newHistoryObject.sliderNegXprogression = areaVolumeObject.sliderXNeg.segmentProgression;
        newHistoryObject.sliderPosZprogression = areaVolumeObject.sliderZPos.segmentProgression;
        newHistoryObject.sliderNegZprogression = areaVolumeObject.sliderZNeg.segmentProgression;
        newHistoryObject.sliderPosYprogression = areaVolumeObject.sliderY.segmentProgression;
        newHistoryObject.sliderMoveYprogression = areaVolumeObject.sliderMoveY.segmentProgression;

        newHistoryObject.currentVolumeTargets.Clear();

        for (int i = 0; i < currentVolumeTargets.Count; i++)
        {
            newHistoryObject.currentVolumeTargets.Add(currentVolumeTargets[i]);
        }

        newHistoryObject.currentVoxel = currentVoxel;
        newHistoryObject.currentTargetVolumeCount = currentTargetVolumeCount;
        newHistoryObject.currentTargetVolume = currentTargetVolume;
        newHistoryObject.currentShapes = currentShapes;

        historyObjects.Add(newHistoryObject);
        currentHistoryObject = newHistoryObject;

        historyCounterText.text = historyObjects.Count.ToString();
        //if(historyObjects.Count > undoCueLength)

    }

    void AppendToCurrentHistory() // NOT IN USE!!
    {
        if (currentHistoryObject != null)
        {
            currentHistoryObject.currentVolumeTargets.Clear();

            for (int i = 0; i < currentVolumeTargets.Count; i++)
            {
                currentHistoryObject.currentVolumeTargets.Add(currentVolumeTargets[i]);
            }

            currentHistoryObject.currentTargetVolumeCount = currentTargetVolumeCount;
            currentHistoryObject.currentTargetVolume = currentTargetVolume;
            currentHistoryObject.currentShapes = currentShapes;
        }
    }

    public void Undo()
    {
        if (historyObjects.Count > 0)
        {            
            avbo.isPlaced = false;

            if (soundUndo)
                soundUndo.Play();

            areaVolumeObject.transform.localPosition = currentHistoryObject.position;
            avbo.transform.localScale = currentHistoryObject.scale;

            areaVolumeObject.sliderXPos.segmentProgression = currentHistoryObject.sliderPosXprogression;
            areaVolumeObject.sliderXPos.InitializeSlider();
            areaVolumeObject.sliderXPos.SliderPosition();
            areaVolumeObject.sliderXNeg.segmentProgression = currentHistoryObject.sliderNegXprogression;
            areaVolumeObject.sliderXNeg.InitializeSlider();
            areaVolumeObject.sliderXNeg.SliderPosition();
            areaVolumeObject.sliderZPos.segmentProgression = currentHistoryObject.sliderPosZprogression;
            areaVolumeObject.sliderZPos.InitializeSlider();
            areaVolumeObject.sliderZPos.SliderPosition();
            areaVolumeObject.sliderZNeg.segmentProgression = currentHistoryObject.sliderNegZprogression;
            areaVolumeObject.sliderZNeg.InitializeSlider();
            areaVolumeObject.sliderZNeg.SliderPosition();
            areaVolumeObject.sliderY.segmentProgression = currentHistoryObject.sliderPosYprogression;
            areaVolumeObject.sliderY.InitializeSlider();
            areaVolumeObject.sliderY.SliderPosition();
            areaVolumeObject.sliderMoveY.segmentProgression = currentHistoryObject.sliderMoveYprogression;
            areaVolumeObject.sliderMoveY.InitializeSlider();
            areaVolumeObject.sliderMoveY.SliderPosition();

            avbo.lastPos = currentHistoryObject.position;
            avbo.currentPos = currentHistoryObject.position;    
            
            avbo.PlaceCorners();
            avbo.PositionObject();
            avbo.PositionHeight();
            avbo.TransformMoveHandle();
            areaVolumeObject.ScaleBoundryObject();
            
            avbo.isPlaced = true;
            currentVoxel = currentHistoryObject.currentVoxel;

            if (currentHistoryObject.add)
                RemoveSelectedVoxels(currentHistoryObject.voxelPositions);
            else
                AddSelectedVoxels(currentHistoryObject.voxelPositions);

            //----------------------------------

            bool curTargetVolumeCheck = true;

            if(currentHistoryObject.currentTargetVolume != currentTargetVolume)
            {
                curTargetVolumeCheck = false;
            }

            if(!curTargetVolumeCheck)
                SetPreviousTarget();

            currentVolumeTargets.Clear();

            for (int i = 0; i < currentHistoryObject.currentVolumeTargets.Count; i++)
            {
                currentVolumeTargets.Add(currentHistoryObject.currentVolumeTargets[i]);
            }

            currentTargetVolumeCount = currentHistoryObject.currentTargetVolumeCount;
            //currentTargetVolume = currentHistoryObject.currentTargetVolume;
            currentShapes = currentHistoryObject.currentShapes;
            currentShapeCountText.text = currentShapes.ToString();

            //-------------------------------------

            var curObj = currentHistoryObject;
            bool check = false;

            if (historyObjects.Count > 1)
                check = true;
            else
                check = false;

            if(check)
            {
                historyObjects.Remove(historyObjects[historyObjects.Count - 1]);
                currentHistoryObject = historyObjects[historyObjects.Count - 1];
            }
            else
            {
                if (historyObjects.Count == 1)
                    historyObjects.Clear();

                currentHistoryObject = null;
            }

            historyCounterText.text = historyObjects.Count.ToString();
        }
        else
        {
            if (soundUnavailable)
                soundUnavailable.Play();
        }
    }

    //---------------------------------------------------IDifficulty/ISetDifficulty------------------------------------------------------------------

    public void CheckDifficulty()
    {
        if (!hasWonGame)
        {
            if ((correctStreakCounter >= numberOfCorrects && areaVolumeGameDifficulty != PlayerStats.Difficulty.Hard) || (wrongStreakCounter >= numberOfWrongs && areaVolumeGameDifficulty != PlayerStats.Difficulty.Easy))
            {
                if (correctStreakCounter >= numberOfCorrects && areaVolumeGameDifficulty == PlayerStats.Difficulty.Easy)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;
                else if (correctStreakCounter >= numberOfCorrects && areaVolumeGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Hard;
                else if (wrongStreakCounter >= numberOfWrongs && areaVolumeGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Easy;
                else if (wrongStreakCounter >= numberOfWrongs && areaVolumeGameDifficulty == PlayerStats.Difficulty.Hard)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;

                print("Hello!");       
                gameManager.hudCanvas.difficultyPanel.OpenPanel();
                correctStreakCounter = 0;
                wrongStreakCounter = 0;
            }
        }
    }

    public void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        areaVolumeGameDifficulty = requestedDifficulty;
    }
    public void SetCurrentDifficultyStartSet(PlayerStats.Difficulty requestedDifficulty)
    {
        areaVolumeGameDifficulty = requestedDifficulty;
        ResetGameSet();
    }

    public void SetGameSetWithDifficulty(int index)
    {
        switch (areaVolumeGameDifficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                currentGameSet = gameSetsEasy[index];
                break;
            case (PlayerStats.Difficulty.Normal):
                currentGameSet = gameSets[index];
                break;
            case (PlayerStats.Difficulty.Hard):
                currentGameSet = gameSetsHard[index];
                break;
            default:
                currentGameSet = gameSets[index];
                break;
        }
    }

    public void WrongTicker()
    {
        wrongStreakCounter++;
        correctStreakCounter = 0;
        CheckDifficulty();
    }


    public void CorrectTicker()
    {
        wrongStreakCounter = 0;
        correctStreakCounter++;
        CheckDifficulty();
    }

    public void OpenDifficultyPanelStart()
    {
        gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.None;
        gameManager.hudCanvas.difficultyPanel.startNewLevel = true;

        if (gameManager.playerStats.hasWonAreaVolumeGame)
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = true;
            gameManager.hudCanvas.difficultyPanel.currentLevelCount = gameSets.Count;
        }
        else
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = false;
        }

        gameManager.hudCanvas.difficultyPanel.gameObject.SetActive(true);
    }

    public void SetCurrentCompletedSet(int passedAchievement)
    {
        int setIndex = 0;

        if (gameSets.Contains(currentGameSet))
            setIndex = gameSets.IndexOf(currentGameSet) + gameSets.Count;
        else if (gameSetsEasy.Contains(currentGameSet))
            setIndex = gameSetsEasy.IndexOf(currentGameSet);
        else if (gameSetsHard.Contains(currentGameSet))
            setIndex = gameSetsHard.IndexOf(currentGameSet) + (gameSets.Count * 2);

        if (!gameManager.playerStats.areaVolumeGameCompletedSets.Contains(setIndex))
        {
            gameManager.playerStats.areaVolumeGameCompletedSets.Add(setIndex);
            gameManager.playerStats.areaVolumeGameSetAchievements.Add(setIndex, passedAchievement);
        }
        else
        {
            gameManager.playerStats.areaVolumeGameSetAchievements[setIndex] = passedAchievement;
        }

        //SetPlayerStatsCurrentCompletedSets();
    }

    public void SetPlayerStatsCurrentCompletedSets()
    {
        gameManager.playerStats.currentCompletedSets.Clear();
        gameManager.playerStats.currentAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.areaVolumeGameCompletedSets.Count; i++)
        {
            var index = gameManager.playerStats.areaVolumeGameCompletedSets[i];
            gameManager.playerStats.currentCompletedSets.Add(index);
            gameManager.playerStats.currentAchievements.Add(index, gameManager.playerStats.areaVolumeGameSetAchievements[index]);
        }
    }

    public void TransferPlayerStats()
    {

        hasWonGame = gameManager.playerStats.hasWonAreaVolumeGame;
        setCounter = gameManager.playerStats.AreaVolumeGameSetCount;

        completedSets.Clear();
        completedSetsAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.areaVolumeGameCompletedSets.Count; i++)
        {
            var setIndex = gameManager.playerStats.areaVolumeGameCompletedSets[i];

            if (setIndex < gameSets.Count)
            {
                completedSets.Add(gameSetsEasy[setIndex]);
            }
            else if (setIndex >= gameSets.Count && setIndex < (gameSets.Count * 2))
            {
                completedSets.Add(gameSets[setIndex - gameSets.Count]);
            }
            else if (setIndex >= (gameSets.Count * 2) && setIndex < (gameSets.Count * 3))
            {
                completedSets.Add(gameSetsHard[setIndex - (gameSets.Count * 2)]);
            }

            completedSetsAchievements.Add(gameManager.playerStats.areaVolumeGameSetAchievements[setIndex]);
        }
    }

    public void StartSelectedGameSet(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        areaVolumeGameDifficulty = requestedDifficulty;

        if (requestedLevel < gameSets.Count)
            setCounter = requestedLevel;
        else if (requestedLevel >= gameSets.Count && requestedLevel < (gameSets.Count * 2))
            setCounter = requestedLevel - gameSets.Count;
        else if (requestedLevel >= gameSets.Count * 2)
            setCounter = requestedLevel - (gameSets.Count * 2);

        ClearGameSet();
        StartGameSet(setCounter);
    }

    //IAchievementPanel:------------------------------------

    public void OpenAchievementPanel(AchievementPanel achievementPanel, PlayerStats.Difficulty difficulty, int levelIndex, int totalLevels, int prevAchievement, int achievement)
    {
        achievementPanel.difficulty = difficulty;
        achievementPanel.levelIndex = levelIndex;
        achievementPanel.prevAchievment = prevAchievement;
        achievementPanel.achievement = achievement;
        achievementPanel.totalLevels = totalLevels;

        achievementPanel.gameObject.SetActive(true);
    }

    public int CheckLevelIndex(PlayerStats.Difficulty difficulty)
    {
        switch (difficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                return gameSetsEasy.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Normal):
                return gameSets.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Hard):
                return gameSetsHard.IndexOf(currentGameSet) + 1;
            default:
                return 0;
        }
    }

    public int CheckCurrentAchievement()
    {
        int testAchievement;

        if (currentShapes <= currentGameSet.targetShapes.x)
        {
            testAchievement = 3;
        }
        else if (currentShapes <= currentGameSet.targetShapes.y && currentShapes > currentGameSet.targetShapes.x)
        {
            testAchievement = 2;
        }
        else if (currentShapes <= currentGameSet.targetShapes.z && currentShapes > currentGameSet.targetShapes.y)
        {
            testAchievement = 1;
        }
        else
        {
            testAchievement = 0;
        }

        return testAchievement;
    }

    public int CheckPrevAchievement()
    {
        int passedPrevAchievement = 0;

        if (completedSets.Contains(currentGameSet))
        {
            passedPrevAchievement = completedSetsAchievements[completedSets.IndexOf(currentGameSet)];
        }

        return passedPrevAchievement;
    }

    //DEBUG ONLY --------------------------------------------------------------------------------------------------------------

    private void OnDrawGizmos()
    {
        int gridCount;

        gridCount = 30;

        if (drawGizmos)
        {
            Gizmos.color = Color.black;
            Gizmos.matrix = voxelContainer.localToWorldMatrix;
            for (int x = 0; x < gridCount; x++)
            {
                for (int y = 0; y < gridCount; y++)
                {
                    for (int z = 0; z < gridCount; z++)
                    {
                        //dgrid[x, y, z] = new Vector3(x / 2, y / 2, z / 2);
                        Gizmos.DrawWireSphere(grid[x, y, z], 0.25f);
                        if (coordinateStatus[x, y, z])
                            Gizmos.color = Color.green;
                        else
                            Gizmos.color = Color.red;
                    }
                }
            }
        }
    }   
}
