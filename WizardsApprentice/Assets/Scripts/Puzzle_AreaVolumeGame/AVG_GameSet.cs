﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AVG_GameSet : MonoBehaviour
{
    [Header("APPEARANCE")]
    public AreaVolumeGame.ColorModes colorMode = AreaVolumeGame.ColorModes.Default;
    [Tooltip("If colorMode is set to single, use playerVoxel")]
    public GameObject playerVoxel;
    public int startShapesVoxel;

    [Header("GRID OPTIONS")]
    public Vector2Int gridSize = new Vector2Int(20, 20);
    public int gridHeight = 10;
    public float worldScale = 1;
    public bool showBounds;
    public bool isVertical;
    public Vector3 worldLocalPos = new Vector3(0, 2, 0);
    public float worldYrotation = 0;
    public bool camIsOrthographic;

    [Header("LOCKED MANIPULATORS")]
    public bool X;
    public bool X_;
    public bool Y;
    public bool Z;
    public bool Z_;
    public bool Move_XZ;
    public bool Move_Y;

    [Header("VISIBLE AXES")]
    public bool showX = true;
    public bool showY = true;
    public bool showZ = true;

    [Header("TARGET VOLUMES")]
    public Vector3Int targetShapes;

    [System.Serializable]
    public class TargetVolume
    {
        public int targetVolume;        
        public List<Transform> startShapes = new List<Transform>();
        public List<bool> isNegativeShape = new List<bool>();
    }

    public List<TargetVolume> targetVolumes = new List<TargetVolume>();
    
    [Header("MANIPULATOR OPTIONS")]
    public bool setManipulator;
    public Vector3 m_position;
    public float posX;
    public float negX;
    public float posZ;
    public float negZ;
    public float posY;
}
