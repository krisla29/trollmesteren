﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaVolumeButtons : MonoBehaviour
{
    public delegate void AreaVolumeButtonEvents(); //Sends to AreaVolumeGame
    public static event AreaVolumeButtonEvents OnAddButtonClick;
    public static event AreaVolumeButtonEvents OnSubtractButtonClick;
    public static event AreaVolumeButtonEvents OnResetButtonClick;
    public static event AreaVolumeButtonEvents OnUndoButtonClick;

    public void AddClick()
    {
        if(OnAddButtonClick != null)
        {
            OnAddButtonClick();
        }
    }

    public void SubtractClick()
    {
        if(OnSubtractButtonClick != null)
        {
            OnSubtractButtonClick();
        }
    }
    
    public void ResetClick()
    {
        if(OnResetButtonClick != null)
        {
            OnResetButtonClick();
        }
    }

    public void UndoClick()
    {
        if (OnUndoButtonClick != null)
        {
            OnUndoButtonClick();
        }
    }
}
