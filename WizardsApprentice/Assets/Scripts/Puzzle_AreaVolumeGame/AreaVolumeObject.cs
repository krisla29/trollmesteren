﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaVolumeObject : MonoBehaviour
{
    public AreaVolumeGame areaVolumeGameScript;
    public InteractableBehaviourSlidePath2 sliderXPos;
    public InteractableBehaviourSlidePath2 sliderXNeg;
    public InteractableBehaviourSlidePath2 sliderZPos;
    public InteractableBehaviourSlidePath2 sliderZNeg;
    public InteractableBehaviourSlidePath2 sliderY;
    public InteractableBehaviourSlidePath2 sliderMoveY;
    public List<Transform> sliders = new List<Transform>();
    private List<InteractableBehaviourSlidePath2> slidersInteractables = new List<InteractableBehaviourSlidePath2>();
    public AreaVolumeBoundryObject boundryObject;    
    private float scaleFactor = 5f;
    public float scaleOffset = 0.2f;
    private float offset = 0.001f;
    public bool anySliderPressed;

    private Vector3 startPos;
    private Vector3 startScale;   

    private void Start()
    {
        //Not in use...
        sliders.Add(sliderXPos.transform);
        slidersInteractables.Add(sliderXPos);
        sliders.Add(sliderXNeg.transform);
        slidersInteractables.Add(sliderXNeg);
        sliders.Add(sliderZPos.transform);
        slidersInteractables.Add(sliderZPos);
        sliders.Add(sliderZNeg.transform);
        slidersInteractables.Add(sliderZNeg);
        sliders.Add(sliderY.transform);
        slidersInteractables.Add(sliderY);
        sliders.Add(sliderMoveY.transform);
        slidersInteractables.Add(sliderMoveY);
        //...

        startPos = boundryObject.transform.localPosition;
    }

    private void Update()
    {
        CheckSlidersPressed();
    }

    public void ScaleBoundryObject()
    {
        var lastPos = boundryObject.transform.localPosition;
        var lastScale = boundryObject.transform.localScale;

        Vector3 pos = boundryObject.transform.localPosition;
        Vector3 scale = boundryObject.transform.localScale + (Vector3.one * offset);

        pos.x = startPos.x - (sliderXPos.segmentProgressionRound * scaleFactor / 2f) + (sliderXNeg.segmentProgressionRound * scaleFactor / 2f);
        scale.x = (sliderXPos.segmentProgressionRound * scaleFactor) + (sliderXNeg.segmentProgressionRound * scaleFactor);

        pos.z = startPos.z - (sliderZPos.segmentProgressionRound * scaleFactor / 2f) + (sliderZNeg.segmentProgressionRound * scaleFactor / 2f);
        scale.z = (sliderZPos.segmentProgressionRound * scaleFactor) + (sliderZNeg.segmentProgressionRound * scaleFactor);

        pos.y = -startPos.y + (sliderY.segmentProgressionRound * (scaleFactor / 2f));
        scale.y = sliderY.segmentProgressionRound * scaleFactor;

        boundryObject.transform.localPosition = pos;
        boundryObject.transform.localScale = scale - (Vector3.one * offset); // subtracts a tiny offset to prevent z-fighting

        if (areaVolumeGameScript)
        {
            areaVolumeGameScript.manipulatorMaterialY.mainTextureScale = new Vector2(scale.x * 2, scale.z * 2);
            areaVolumeGameScript.manipulatorMaterialZ.mainTextureScale = new Vector2(scale.z * 2, scale.y * 2);
            areaVolumeGameScript.manipulatorMaterialX.mainTextureScale = new Vector2(scale.x * 2, scale.y * 2);

            if (areaVolumeGameScript.soundSnap)
            {
                if (anySliderPressed)
                {
                    if (boundryObject.transform.localScale != lastScale)
                    {
                        areaVolumeGameScript.soundSnap.Stop();
                        areaVolumeGameScript.soundSnap.Play();
                    }
                }
            }
        }        
        /*
        for (int i = 0; i < 4; i++)
        {
            print(boundryObject.sideMeshes[i].uv.Length);

            for (int e = 0; e < boundryObject.sideMeshes[i].uv.Length; e++)
            {
                if(e == 0 || e == 1)
                {
                    boundryObject.sideMeshes[i].uv[e].y = scale.y * 2;
                }
                if(e == 2 || e == 3)
                {
                    boundryObject.sideMeshes[i].uv[e].y = -scale.y * 2;
                }
            }
        }
        */
        /*
        foreach (var item in boundryObject.spriteRenderersSideX)
        {
            item.size = new Vector2(scale.z, scale.y) * 2;
        }
        foreach (var item in boundryObject.spriteRenderersSideZ)
        {
            item.size = new Vector2(scale.x, scale.y) * 2;
        }
        foreach (var item in boundryObject.spriteRenderersTop)
        {
            item.size = new Vector2(scale.x, scale.z) * 2;
        }

        boundryObject.spritesParent.localPosition = boundryObject.transform.localPosition;
        */
        //boundryObject.spritesParent.localScale = boundryObject.transform.localScale;
    }
    
    public void CheckLockSliders()
    {
        
        if (sliderXNeg.lockPositive)
            boundryObject.limitPosX = true;
        else
            boundryObject.limitPosX = false;

        if (sliderXPos.lockPositive)
            boundryObject.limitNegX = true;
        else
            boundryObject.limitNegX = false;

        if (sliderZNeg.lockPositive)
            boundryObject.limitPosZ = true;
        else
            boundryObject.limitPosZ = false;

        if (sliderZPos.lockPositive)
            boundryObject.limitNegZ = true;
        else
            boundryObject.limitNegZ = false;

        if (sliderY.lockPositive)
            boundryObject.limitY = true;
        else
            boundryObject.limitY = false;
        
        /*
        if (boundryObject.minLimitY)
            sliderY.lockNegative = true;
        else
            sliderY.lockNegative = false;
        */       
    }   
    
    private void CheckSlidersPressed()
    {
        bool check = false;

        for (int i = 0; i < slidersInteractables.Count; i++)
        {
            if (slidersInteractables[i].interactable.isPressed)
                check = true;
        }

        anySliderPressed = check;
    }
}

