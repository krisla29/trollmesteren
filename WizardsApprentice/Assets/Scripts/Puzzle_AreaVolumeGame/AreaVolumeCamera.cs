﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaVolumeCamera : MonoBehaviour
{
    public Camera thisCam;
    public Transform camStandIn;
    private float startFOV;
    public Transform sceneTransform;
    public PlayerReference playerReferenceScript;
    public AreaVolumeGame areaVolumeGame;
    public Transform manipulator;
    public Transform manipulatorBounds;
    public Transform gamePivot;
    public Slider zoomSlider;
    public Toggle perspectiveToggle;
    public float speed = 3f;
    public Transform originalParent;
    public Transform targetObjectTranslate;
    public Transform targetObjectRotate;
    public Transform camParent;
    public Transform camParentParent;
    public Transform player;
    public bool isHorizontal;
    private Vector3 camStartPos;
    public Vector3 camParentStartPos;
    public Quaternion camParentStartRot;
    public Quaternion camParentPerspectiveRot;
    private Quaternion camParentParentStartRot;
    public float worldScaleMultipler = 0;
    public float orthographicAngleStart = 89;
    public float orthographicAngle = 89;
    public
    //private float startHeight;
    void Awake()
    {
        camParentStartPos = camParent.localPosition;
        camParentStartRot = camParent.localRotation;
        camParentParentStartRot = camParentParent.localRotation;
        camStartPos = camStandIn.localPosition;
        startFOV = thisCam.fieldOfView;
    }

    private void OnEnable()
    {
        perspectiveToggle.isOn = true;

        if (playerReferenceScript.player)
            player = playerReferenceScript.player;

        ResetTransforms();
        /*
        originalParent = transform.parent;

        if (!tempParentParent)
            tempParentParent = new GameObject("tempSceneCamGrandParent");

        tempParentParent.transform.SetParent(originalParent);
        tempParentParent.transform.position = gamePivot.position;
        tempParentParent.transform.rotation = player.transform.rotation;

        if (!tempParent)
            tempParent = new GameObject("tempSceneCamParent");

        tempParent.transform.SetParent(tempParentParent.transform);
        tempParent.transform.position = gamePivot.position;
        tempParent.transform.localRotation = Quaternion.identity;

        transform.SetParent(tempParent.transform);

        camStartPos = transform.localPosition;
        tempParent.transform.localRotation = transform.localRotation;
        transform.localRotation = Quaternion.identity;
        */
        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;        
    }

    private void OnDisable()
    {
        //Invoke("ReparentAndDestroy", 0);
        ResetTransforms();
        
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
    }

    void ResetTransforms()
    {
        camParentParent.localPosition = Vector3.zero;
        camParentParent.localRotation = camParentParentStartRot;
        camParent.localPosition = camParentStartPos;
        camParent.localRotation = camParentStartRot;
        camStandIn.localPosition = camStartPos;
        camStandIn.localRotation = Quaternion.identity;
        targetObjectTranslate = player;
        thisCam.fieldOfView = startFOV;
    }
    /*
    private void ReparentAndDestroy()
    {
        if (tempParent)
        {
            if (transform.parent == tempParent.transform)
                transform.SetParent(originalParent);

            Destroy(tempParent);
        }

        if (tempParentParent)
        {
            if (transform.parent == tempParentParent.transform)
                transform.SetParent(originalParent);

            Destroy(tempParentParent);
        }
    }
    */
    private void Update()
    {        
        //LookAtGameCenter();
        if (perspectiveToggle.isOn)
        {
            if(player)
                targetObjectTranslate = player;

            if (isHorizontal)
            {
                TranslatePerspectiveHorizontal();
                RotatePerspectiveHorizontal();
            }
            else
            {
                TranslatePerspectiveVertical();
                RotatePerspectiveVertical();
            }
        }
        else
        {
            if(manipulator)
                targetObjectTranslate = manipulator;

            if (isHorizontal)
            {
                TranslateTopViewHorizontal();
                RotateTopViewHorizontal();
            }
            else
            {
                TranslateSideViewVertical();
                RotateSideViewVertical();
            }
        }

        Zoom();        
    }
    
    private void LateUpdate()
    {
        transform.position = camStandIn.position - (camStandIn.forward * worldScaleMultipler);
        transform.rotation = camStandIn.rotation;
    }
    
    /*
    void LookAtPlayerReverse()
    {
        if (camParent && player)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var newRot = Quaternion.Slerp(camParent.transform.rotation, Quaternion.LookRotation(gamePivot.position - player.position), smoothLerp);
            camParent.transform.rotation = newRot;           
        }
    }
    */
    void TranslatePerspectiveHorizontal()
    {
        if (camParent && player && areaVolumeGame && areaVolumeGame.areaVolumeObject)
        {
            var Ypos = (((areaVolumeGame.areaVolumeObject.sliderY.segmentProgression + areaVolumeGame.areaVolumeObject.boundryObject.heightSlider.segmentProgression) / 2) * 8f) + camParentStartPos.y;
            var newPos = new Vector3(targetObjectTranslate.position.x, Ypos, targetObjectTranslate.position.z);

            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var pos = Vector3.Lerp(camParent.transform.position, newPos, smoothLerp);
            camParent.transform.position = pos;
            var locPos = camStandIn.localPosition;
            camStandIn.localPosition = Vector3.Lerp(locPos, camStartPos, smoothLerp);
        }
    }

    void TranslatePerspectiveVertical()
    {
        if (camParent && player && areaVolumeGame && areaVolumeGame.areaVolumeObject)
        {
            var Ypos = (((areaVolumeGame.areaVolumeObject.sliderZNeg.segmentProgression) / 2) * 8f) + (camParentStartPos.y * 0.85f);
            var newPos = new Vector3(targetObjectTranslate.position.x, Ypos, targetObjectTranslate.position.z) - (camParent.transform.forward * 5);

            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var pos = Vector3.Lerp(camParent.transform.position, newPos, smoothLerp);
            camParent.transform.position = pos;
            var locPos = camStandIn.localPosition;
            camStandIn.localPosition = Vector3.Lerp(locPos, camStartPos, smoothLerp);
        }
    }

    void RotatePerspectiveHorizontal()
    {
        if (camParentParent && camParent)
        {
            Quaternion rot = camParent.transform.localRotation;
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            camParent.transform.localRotation = Quaternion.Slerp(rot, camParentStartRot, smoothLerp);
            camParentParent.transform.rotation = Quaternion.Slerp(camParentParent.transform.rotation, Quaternion.LookRotation(gamePivot.position - targetObjectRotate.position), smoothLerp);
        }
    }

    void RotatePerspectiveVertical()
    {
        if (camParentParent && camParent)
        {
            Quaternion rot = camParent.transform.localRotation;
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            camParent.transform.localRotation = Quaternion.Slerp(rot, Quaternion.AngleAxis(25f, Vector3.right), smoothLerp);
            camParentParent.transform.rotation = Quaternion.Slerp(camParentParent.transform.rotation, Quaternion.LookRotation(gamePivot.position - targetObjectRotate.position), smoothLerp);
        }
    }

    void RotateTopViewHorizontal()
    {
        if (camParentParent && camParent)
        {
            Quaternion rot = camParent.transform.localRotation;
            Quaternion topDownRot = Quaternion.AngleAxis(89, Vector3.right);
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * (speed * 1.5f));
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            camParent.transform.localRotation = Quaternion.Slerp(rot, topDownRot, smoothLerp);
            camParentParent.transform.localRotation = Quaternion.Slerp(camParentParent.transform.localRotation, camParentParentStartRot, smoothLerp);
        }
    }

    void RotateSideViewVertical()
    {
        if (camParentParent && camParent)
        {
            Quaternion rot = camParent.transform.localRotation;
            Quaternion faceForwardRot = Quaternion.AngleAxis(1, Vector3.right);
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * (speed * 1.5f));
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            camParent.transform.localRotation = Quaternion.Slerp(rot, faceForwardRot, smoothLerp);
            //camParent.transform.localRotation = Quaternion.Slerp(rot, Quaternion.LookRotation(camParent.transform.position - targetObjectTranslate.position), smoothLerp); 
            camParentParent.transform.localRotation = Quaternion.Slerp(camParentParent.transform.localRotation, camParentParentStartRot, smoothLerp);
            //camParentParent.transform.rotation = Quaternion.Slerp(camParentParent.transform.rotation, Quaternion.identity, smoothLerp);
        }
    }

    void TranslateTopViewHorizontal()
    {
        if (camParent && player && areaVolumeGame && areaVolumeGame.areaVolumeObject)
        {
            var Ypos = (((areaVolumeGame.areaVolumeObject.sliderY.segmentProgression + areaVolumeGame.areaVolumeObject.boundryObject.heightSlider.segmentProgression) / 2) * 8f) + camParentStartPos.y;
            var newPos = new Vector3(manipulator.position.x - manipulatorBounds.localPosition.x, Ypos, manipulator.position.z - manipulatorBounds.localPosition.z);

            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * (speed * 1.5f));
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var pos = Vector3.Lerp(camParent.transform.position, newPos, smoothLerp);
            camParent.transform.position = pos;
            var locPos = camStandIn.localPosition;
            camStandIn.localPosition = Vector3.Lerp(locPos, Vector3.zero, smoothLerp);
        }
    }

    void TranslateSideViewVertical()
    {
        if (camParent && player && areaVolumeGame && areaVolumeGame.areaVolumeObject)
        {
            var Ypos = (camParentStartPos.y * 0.15f) + manipulator.localPosition.z + manipulatorBounds.localPosition.z; //(((areaVolumeGame.areaVolumeObject.sliderZNeg.segmentProgression) / 2) * 8f) + 
            var newPos = new Vector3(manipulator.position.x - manipulatorBounds.localPosition.x, Ypos, manipulator.position.z) - (camParent.transform.forward * 9f);

            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * (speed * 1.5f));
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            var pos = Vector3.Lerp(camParent.transform.position, newPos, smoothLerp);
            camParent.transform.position = pos;
            var locPos = camStandIn.localPosition;
            camStandIn.localPosition = Vector3.Lerp(locPos, Vector3.zero, smoothLerp);
        }
    }

    void Zoom()
    {
        float zoom = zoomSlider.value * 30f;        
        thisCam.fieldOfView = startFOV + zoom;
    }

    void ReferenceNewPlayer(Transform newPlayer)
    {
        player = newPlayer;
    }
}
