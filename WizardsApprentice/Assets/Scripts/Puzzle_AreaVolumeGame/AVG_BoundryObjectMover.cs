﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AVG_BoundryObjectMover : MonoBehaviour
{
    public AreaVolumeBoundryObject areaVolumeBoundryObject;

    private void OnMouseDown()
    {
        if(areaVolumeBoundryObject)
        {
            areaVolumeBoundryObject.M_Down();
        }
    }

    private void OnMouseDrag()
    {
        if (areaVolumeBoundryObject)
        {
            areaVolumeBoundryObject.M_Drag();
        }
    }

    private void OnMouseUp()
    {
        if (areaVolumeBoundryObject)
        {
            areaVolumeBoundryObject.M_Up();
        }
    }
}
