﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaVolumeBoundryObject : InteractableBehaviour
{
    public AreaVolumeObject areaVolumeObjectScript;
    public Transform tempObj;
    public Transform moveHandle;
    public SpriteRenderer moveHandleSprite;
    //public BoxCollider moveCollider;
    public BoxCollider thisCollider;
    public List<MeshRenderer> meshRenderersX = new List<MeshRenderer>();
    public List<MeshRenderer> meshRenderersZ = new List<MeshRenderer>();
    public List<MeshRenderer> meshRenderersY = new List<MeshRenderer>();
    //public List<MeshFilter> meshFilters = new List<MeshFilter>();
    public List<SpriteRenderer> spriteRenderersSideX = new List<SpriteRenderer>();
    public List<SpriteRenderer> spriteRenderersSideZ = new List<SpriteRenderer>();
    public List<SpriteRenderer> spriteRenderersTop = new List<SpriteRenderer>();
    public Transform spritesParent;
    //public List<Mesh> sideMeshes = new List<Mesh>();
    public Material gridMatX;
    public Material gridMatZ;
    public Material gridMatY;
    [Range(0, 2)] public float moveSpeed = 1;
    public bool isVerticalMovement;
    public List<SpriteRenderer> linesX = new List<SpriteRenderer>();
    public List<SpriteRenderer> linesY = new List<SpriteRenderer>();
    public List<SpriteRenderer> linesZ = new List<SpriteRenderer>();
    public Color linesColor = Color.cyan;
    public Transform lineX;
    public Transform lineY;
    public Transform lineZ;
    public SpriteRenderer planeVisualizer;
    private bool isInitialized;
    public bool isPlaced;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;
    public float speed = 20;
    public float stepSize = 2.5f;
    //public float boundry = 7.5f;
    public Vector2 boundry = new Vector2(7.5f, 7.5f);
    public float topBoundry = 10f;
    private Camera cam;
    public InteractableBehaviourSlidePath2 heightSlider;
    public Transform heightSliderGroup;
    public List<Transform> bounds = new List<Transform>(8);
    public List<InteractableBehaviourSlidePath2> sliderInteractables = new List<InteractableBehaviourSlidePath2>();
    public bool boundsCheck = true;
    public bool heightCheck = true;
    public Vector3 startPos = Vector3.zero;
    public Vector3 currentPos = Vector3.zero;
    public Vector3 lastPos = Vector3.zero;
    public Vector3 currentVolume;
    public Vector3 currentVolumeGameUnits;

    public bool limitPosX;
    public bool limitNegX;
    public bool limitPosZ;
    public bool limitNegZ;
    public bool limitY;
    public bool minLimitY;

    public bool xPosPress;
    public bool xNegPress;
    public bool zPosPress;
    public bool zNegPress;
    public bool yPress;
    public bool yMovePress;

    private List<float> interactablesProgressions = new List<float>();
    private List<Vector3> interactablesStartPos = new List<Vector3>();
    //private List<bool> slidersPressed = new List<bool>();
    /*
    void Awake()
    {
        for (int i = 0; i < 4; i++)
        {
            var newMesh = new Mesh();
            sideMeshes.Add(newMesh);
            newMesh.vertices = meshFilters[i].sharedMesh.vertices;
            newMesh.triangles = meshFilters[i].sharedMesh.triangles;
            newMesh.uv = meshFilters[i].sharedMesh.uv;
            newMesh.normals = meshFilters[i].sharedMesh.normals;
            newMesh.tangents = meshFilters[i].sharedMesh.tangents;
            meshFilters[i].sharedMesh = newMesh;
        }
    }
    */
    private void Start()
    {
        
        /*
        //Not in use...
        slidersPressed.Add(xPosPress);
        slidersPressed.Add(xNegPress);
        slidersPressed.Add(zPosPress);
        slidersPressed.Add(zNegPress);
        slidersPressed.Add(yPress);
        slidersPressed.Add(yMovePress);
        //...
        */
        for (int i = 0; i < sliderInteractables.Count; i++)
        {
            interactablesProgressions.Add(sliderInteractables[i].segmentProgression);
            interactablesStartPos.Add(sliderInteractables[i].interactable.mainObject.transform.localPosition);
        }

        //startPos = interactable.mainObject.transform.localPosition;

        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        foreach (var item in linesX)
        {
            item.color = linesColor;
        }
        foreach (var item in linesY)
        {
            item.color = linesColor;
        }
        foreach (var item in linesZ)
        {
            item.color = linesColor;
        }
    }

    private void Update()
    {
        if (isPlaced)
        {
            lastPos = interactable.mainObject.transform.localPosition;

            PlaceCorners();
            CheckBounds();
            PositionObject();

            PositionHeight();
            TransformMoveHandle();

            areaVolumeObjectScript.CheckLockSliders();
            areaVolumeObjectScript.ScaleBoundryObject();
            ResetChecks();
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    public void M_Down()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }        
    }

    public void M_Drag()
    {
        if (!isInitialized)
        {
            tempObj.transform.SetParent(transform.parent.parent);
            tempObj.transform.position = interactable.mainObject.transform.position;
            cam = Camera.main;
        }

        isInitialized = true;

        var dir = cam.transform.TransformDirection(Vector3.forward);
        tempObj.transform.rotation = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.z), Vector3.up);

        Debug.DrawLine(tempObj.transform.position + Vector3.up, tempObj.transform.position + Vector3.up + tempObj.transform.forward * 10f, Color.blue);
        Debug.DrawLine(tempObj.transform.position + Vector3.up, tempObj.transform.position + Vector3.up + tempObj.transform.right * 10f, Color.red);

        if (!isVerticalMovement)
        {
            tempObj.transform.Translate(tempObj.transform.forward * Input.GetAxis("Mouse Y") * (moveSpeed * 0.25f), Space.World);
            tempObj.transform.Translate(tempObj.transform.right * Input.GetAxis("Mouse X") * (moveSpeed * 0.25f), Space.World);
        }
        else
        {
            tempObj.transform.Translate(tempObj.transform.up * Input.GetAxis("Mouse Y") * (moveSpeed * 0.25f), Space.World);
            tempObj.transform.Translate(tempObj.transform.right * Input.GetAxis("Mouse X") * (moveSpeed * 0.25f), Space.World);
        }

        var pos = tempObj.transform.localPosition;
        //currentPos = new Vector3(Mathf.Clamp(pos.x, -boundry, boundry), Mathf.Clamp(pos.y, -topBoundry, topBoundry), Mathf.Clamp(pos.z, -boundry, boundry));
        currentPos = new Vector3(Mathf.Clamp(pos.x, -boundry.x, boundry.x), Mathf.Clamp(pos.y, -topBoundry, topBoundry), Mathf.Clamp(pos.z, -boundry.y, boundry.y));
    }

    public void M_Up()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }
        //ResetChecks();

        isInitialized = false;
    }

    public void ResetChecks()
    {
        boundsCheck = true;
        heightCheck = true;

        limitPosX = false;
        limitNegX = false;
        limitPosZ = false;
        limitNegZ = false;
        limitY = false;
        minLimitY = false;
    }

    public void PositionObject()
    {
        var pos = interactable.mainObject.transform.localPosition;

        if (boundsCheck)
        {
            var newPos = new Vector3((Mathf.Round(currentPos.x * stepSize) / stepSize), currentPos.y, (Mathf.Round(currentPos.z * stepSize) / stepSize));

            interactable.mainObject.transform.localPosition = newPos;            
        }
        else
        {
            interactable.mainObject.transform.localPosition = lastPos;
        }

        if (areaVolumeObjectScript.areaVolumeGameScript)
        {
            if (areaVolumeObjectScript.areaVolumeGameScript.soundSnap)
            {
                if (interactable.isPressed || isInitialized)
                {
                    if (interactable.mainObject.transform.localPosition != pos)
                    {
                        areaVolumeObjectScript.areaVolumeGameScript.soundSnap.Stop();
                        areaVolumeObjectScript.areaVolumeGameScript.soundSnap.Play();
                    }
                }
            }
        }
    }

    public void TransformMoveHandle()
    {
        var mainObject = interactable.mainObject.transform;

        //Vector3 colliderPos = new Vector3(0, (currentPos.y + (transform.localScale.y * 0.1f)) * -1, 0);
        //Vector3 colliderSize = new Vector3(2 - (transform.localScale.x * 0.1f), 0.1f - (transform.localScale.y * 0.015f), 2 - (transform.localScale.z * 0.1f));

        moveHandleSprite.size = new Vector2(2 + (transform.localScale.x * 2), 2 + (transform.localScale.z * 2));
        moveHandleSprite.transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);

        moveHandle.transform.localScale = new Vector3(2 + transform.localScale.x, 0.1f, 2 + transform.localScale.z);
        moveHandle.transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);

        if (lineX)
            lineX.transform.localScale = new Vector3(transform.localScale.x, 1, 1);

        if (lineY)
            lineY.transform.localScale = new Vector3(1, transform.localScale.y * 2, 1);

        if (lineZ)
            lineZ.transform.localScale = new Vector3(1, 1, transform.localScale.z);

        foreach (var item in linesX)
        {
            item.transform.parent.localScale = new Vector3(transform.localScale.x, 1, 1);
        }
        foreach (var item in linesY)
        {
            item.transform.parent.localScale = new Vector3(1, transform.localScale.y * 2, 1);
        }
        foreach (var item in linesZ)
        {
            item.transform.parent.localScale = new Vector3(1, 1, transform.localScale.z);
        }

        if (planeVisualizer)
        {
            planeVisualizer.size = new Vector2((transform.localScale.x * 2), (transform.localScale.z * 2));
            planeVisualizer.transform.localPosition = new Vector3(transform.localPosition.x, 0, transform.localPosition.z);
        }

        //moveCollider.center = colliderPos;
        //moveCollider.size = colliderSize;
    }

    public void CheckBounds()
    {       
        foreach (var item in bounds)
        {
            var testPos = currentPos + item.localPosition;

            if (testPos.x < -boundry.x || testPos.x > boundry.x || testPos.z < -boundry.y || testPos.z > boundry.y)
            {
                boundsCheck = false;

                if (testPos.x < -boundry.x)
                    limitNegX = true;
                else
                    limitNegX = false;

                if (testPos.x > boundry.x)
                    limitPosX = true;
                else
                    limitPosX = false;

                if (testPos.z < -boundry.y)
                    limitNegZ = true;
                else
                    limitNegZ = false;

                if (testPos.z > boundry.y)
                    limitPosZ = true;
                else
                    limitPosZ = false;
            }            
        }
        /*
        if ((currentPos.y + bounds[4].localPosition.y + heightSlider.transform.localPosition.y) > topBoundry)
        {
            heightCheck = false;
            limitY = true;
        }
        else
        {
            heightCheck = true;
            limitY = false;
        }
        if (areaVolumeObjectScript.sliderY.segmentProgression <= 0.1f)//-------------
            minLimitY = true;
        else
            minLimitY = false;
        */
    }
    
    void CheckBoundsSliders()
    {
        CheckRectSlidersBounds();

        if ((interactable.mainObject.transform.localPosition.y + bounds[4].localPosition.y + heightSlider.transform.localPosition.y) > topBoundry)
        {
            heightCheck = false;
            limitY = true;
        }
        else
        {
            heightCheck = true;
            limitY = false;
        }
        if (areaVolumeObjectScript.sliderY.segmentProgression <= 0.1f)//-------------
            minLimitY = true;
        else
            minLimitY = false;
    }
    
    void CheckRectSlidersBounds()
    {
        bool check = true;

        foreach (var item in bounds)
        {
            var testPos = interactable.mainObject.transform.localPosition + item.localPosition;

            if (testPos.x <= -boundry.x || testPos.x >= boundry.x || testPos.z <= -boundry.y || testPos.z >= boundry.y)
            {
                boundsCheck = false;
                check = false;

                if (testPos.x <= -boundry.x)
                    limitNegX = true;
                else
                    limitNegX = false;

                if (testPos.x >= boundry.x)
                    limitPosX = true;
                else
                    limitPosX = false;

                if (testPos.z <= -boundry.y)
                    limitNegZ = true;
                else
                    limitNegZ = false;

                if (testPos.z >= boundry.y)
                    limitPosZ = true;
                else
                    limitPosZ = false;
            }
        }

        if (check)
            boundsCheck = true;
    }

    bool CheckSliderPositive(InteractableBehaviourSlidePath2 passedInteractable)
    {
        Transform boundToCheck = CheckConnectedBound(passedInteractable);

        return false;
    }

    bool CheckSliderNegative(InteractableBehaviourSlidePath2 passedInteractable)
    {
        Transform boundToCheck = CheckConnectedBound(passedInteractable);

        return false;
    }

    Transform CheckConnectedBound(InteractableBehaviourSlidePath2 passedInteractable)
    {
        Transform boundToReturn = null;

        for (int i = 0; i < bounds.Count; i++)
        {

        }

        return boundToReturn;
    }

    void PositionHeight2()
    {
        var lastPosInteractable = interactable.mainObject.transform.localPosition;
        var lastPosGroup = heightSliderGroup.localPosition;

        if (heightCheck)
        {
            var pos = interactable.mainObject.transform.localPosition;
            var newPosHeight = new Vector3(pos.x, (heightSlider.segmentProgressionRound) * 5f, pos.z);
            interactable.mainObject.transform.localPosition = newPosHeight;

            var groupPos = heightSliderGroup.localPosition;
            var newGroupPos = new Vector3(groupPos.x, -(heightSlider.segmentProgressionRound) * 5f, groupPos.z);
            heightSliderGroup.localPosition = newGroupPos;
        }
        else
        {
            interactable.mainObject.transform.localPosition = lastPosInteractable;
            heightSliderGroup.localPosition = lastPosGroup;
        }
    }

    public void PositionHeight()
    {
        var pos = interactable.mainObject.transform.localPosition;
        var newPosHeight = new Vector3(pos.x, (heightSlider.segmentProgressionRound) * 5f, pos.z);
        interactable.mainObject.transform.localPosition = newPosHeight;

        var groupPos = heightSliderGroup.localPosition;
        var newGroupPos = new Vector3(groupPos.x, -(heightSlider.segmentProgressionRound) * 5f, groupPos.z);
        heightSliderGroup.localPosition = newGroupPos;
    }

    public void PlaceCorners()
    {
        for (int i = 0; i < bounds.Count; i++)
        {
            if (i == 0)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x/2, -transform.localPosition.y, transform.localScale.z/2);
            }
            if (i == 1)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x/2, -transform.localPosition.y, transform.localScale.z/2);
            }
            if (i == 2)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x/2, -transform.localPosition.y, -transform.localScale.z/2);
            }
            if (i == 3)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x/2, -transform.localPosition.y, -transform.localScale.z/2);
            }
            if (i == 4)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z / 2);
            }
            if (i == 5)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z / 2);
            }
            if (i == 6)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x / 2, transform.localScale.y / 2, -transform.localScale.z / 2);
            }
            if (i == 7)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x / 2, transform.localScale.y / 2, -transform.localScale.z / 2);
            }
        }

        var volumeX = Mathf.Abs(bounds[0].localPosition.x) + Mathf.Abs(bounds[1].localPosition.x);
        var volumeY = Mathf.Abs(bounds[0].localPosition.y) + Mathf.Abs(bounds[4].localPosition.y);
        var volumeZ = Mathf.Abs(bounds[0].localPosition.z) + Mathf.Abs(bounds[2].localPosition.z);

        currentVolume = new Vector3(volumeX, volumeY, volumeZ);
        currentVolumeGameUnits = currentVolume * 2;
        //Check [0] and [1], [0] and [2], and [0] and [4]
    }

    public Vector3 CalculateVolumeFromObject(Transform obj)
    {
        Transform[] corners = new Transform[8];

        for (int i = 0; i < corners.Length; i++)
        {
            corners[i] = new GameObject().transform;
        }

        for (int i = 0; i < corners.Length; i++)
        {
            
            if (i == 0)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(obj.localScale.x, -obj.localPosition.y, obj.localScale.z);
            }
            if (i == 1)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(-obj.localScale.x, -obj.localPosition.y, obj.localScale.z);
            }
            if (i == 2)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(obj.localScale.x, -obj.localPosition.y, -obj.localScale.z);
            }
            if (i == 3)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(-obj.localScale.x, -obj.localPosition.y, -obj.localScale.z);
            }
            if (i == 4)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(obj.localScale.x, obj.localScale.y, obj.localScale.z);
            }
            if (i == 5)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(-obj.localScale.x, obj.localScale.y, obj.localScale.z);
            }
            if (i == 6)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(obj.localScale.x, obj.localScale.y, -obj.localScale.z);
            }
            if (i == 7)
            {
                corners[i].localPosition = obj.localPosition + new Vector3(-obj.localScale.x, obj.localScale.y, -obj.localScale.z);
            }
        }

        var volumeX = Mathf.Abs(corners[0].localPosition.x) + Mathf.Abs(corners[1].localPosition.x);
        var volumeY = Mathf.Abs(corners[0].localPosition.y) + Mathf.Abs(corners[4].localPosition.y);
        var volumeZ = Mathf.Abs(corners[0].localPosition.z) + Mathf.Abs(corners[2].localPosition.z);

        var volume = new Vector3(volumeX, volumeY, volumeZ);

        foreach (var item in corners)
        {
            Destroy(item.gameObject);
        }
        /*
        for (int i = corners.Length - 1; i > -1; i--)
        {
            //var go = 
        }
        */
        return volume;
    }

    public void ResetBoundryObject()
    {
        for (int i = 0; i < sliderInteractables.Count; i++)
        {
            sliderInteractables[i].segmentProgression = interactablesProgressions[i];
            sliderInteractables[i].segmentProgressionRound = Mathf.Round(sliderInteractables[i].segmentProgression * sliderInteractables[i].stepSize) / sliderInteractables[i].stepSize;
            sliderInteractables[i].interactable.mainObject.transform.localPosition = interactablesStartPos[i];
        }

        currentPos = startPos;
    }

    public void ResetSlider(InteractableBehaviourSlidePath2 sliderInteractable)
    {
        //sliderInteractable.segmentProgression = 1;
        //sliderInteractable.segmentProgressionRound = Mathf.Round(sliderInteractables[i].segmentProgression * sliderInteractables[i].stepSize) / sliderInteractables[i].stepSize;
    }

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += CheckSlidersPress;
        Interactable.OnInteractableReleased += CheckSlidersRelease;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= CheckSlidersPress;
        Interactable.OnInteractableReleased -= CheckSlidersRelease;

        isInitialized = false;
        StopAllCoroutines();
    }
    //Not in use...
    void CheckSlidersPress(Transform interactable)
    {
        if (interactable == areaVolumeObjectScript.sliderXPos.transform)
            xPosPress = true;
        if (interactable == areaVolumeObjectScript.sliderXNeg.transform)
            xNegPress = true;
        if (interactable == areaVolumeObjectScript.sliderZPos.transform)
            zPosPress = true;
        if (interactable == areaVolumeObjectScript.sliderZNeg.transform)
            zNegPress = true;
        if (interactable == areaVolumeObjectScript.sliderY.transform)
            yPress = true;
        if (interactable == areaVolumeObjectScript.sliderMoveY.transform)
            yMovePress = true;
    }
    
    void CheckSlidersRelease(Transform interactable)
    {
        if (interactable == areaVolumeObjectScript.sliderXPos.transform)
            xPosPress = false;
        if (interactable == areaVolumeObjectScript.sliderXNeg.transform)
            xNegPress = false;
        if (interactable == areaVolumeObjectScript.sliderZPos.transform)
            zPosPress = false;
        if (interactable == areaVolumeObjectScript.sliderZNeg.transform)
            zNegPress = false;
        if (interactable == areaVolumeObjectScript.sliderY.transform)
            yPress = false;
        if (interactable == areaVolumeObjectScript.sliderMoveY.transform)
            yMovePress = false;
    }
    //...

    private void OnDestroy()
    {
        isInitialized = false;
        StopAllCoroutines();
        //Invoke("DestroyMeshes", 0);
    }
    
    public void OnDrawGizmos()
    {
        
        for (int i = 0; i < bounds.Count; i++)
        {
            Gizmos.color = Color.red;

            if (i == 0)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x/2, -transform.localPosition.y, transform.localScale.z/2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 1)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x/2, -transform.localPosition.y, transform.localScale.z/2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 2)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x/2, -transform.localPosition.y, -transform.localScale.z/2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 3)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x/2, -transform.localPosition.y, -transform.localScale.z/2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 4)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z / 2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 5)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x / 2, transform.localScale.y / 2, transform.localScale.z / 2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 6)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(transform.localScale.x / 2, transform.localScale.y / 2, -transform.localScale.z / 2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 7)
            {
                bounds[i].localPosition = transform.localPosition + new Vector3(-transform.localScale.x / 2, transform.localScale.y / 2, -transform.localScale.z / 2);
                Gizmos.DrawWireCube(bounds[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
        }
        
        Gizmos.color = Color.yellow;

        if (tempObj)
        {
            Gizmos.DrawWireCube(tempObj.transform.position, new Vector3(0.5f, 0.5f, 0.5f));
        }
    }

    private void OnMouseDown()
    {
        M_Down();
    }

    private void OnMouseDrag()
    {
        M_Drag();
    }

    private void OnMouseUp()
    {
        M_Up();
    }
    /*    
    void DestroyMeshes()
    {
        for (int i = 3; i > -1; i--)
        {
            var tempMesh = sideMeshes[i];
            meshFilters[i].sharedMesh = meshFilters[5].sharedMesh;
            sideMeshes.Remove(sideMeshes[i]);
            Destroy(tempMesh);
        }

        sideMeshes.Clear();
    }
    */
}
