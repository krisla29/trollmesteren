﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AVG_TargetVolumeUI : MonoBehaviour
{
    public TextMeshProUGUI targetVolumeText;
    public Image targetVolumeBGfill;
    public Image targetVolumeBGOutline;
}
