﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwakenByScene : MonoBehaviour
{
    public GameManager gameManager;
    public bool checkByScene;
    public bool checkByEvent;
    public List<SceneController> sceneControllers = new List<SceneController>();
    public bool includeWorldScene;
    public List<GameEvent> gameEvents = new List<GameEvent>();
    public List<Transform> objects = new List<Transform>();
    [Tooltip("Check to set isInitialized from other script, this script will be ignored until initialized by other!")]
    public bool initializeByOther;
    public bool isInitialized;
    public bool useTimed;
    public float waitTime;
    public enum Methods { Sleep, Wakeup}
    public Methods method;
    public bool doMethodOnAwake;
    public bool doReverseOnAwake;

    private Coroutine execute;

    private void Awake()
    {
        if(doMethodOnAwake)
        {
            foreach (var item in objects)
            {
                if (method == Methods.Wakeup)
                    item.gameObject.SetActive(true);
                else
                    item.gameObject.SetActive(false);
            }
        }

        if(doReverseOnAwake)
        {
            foreach (var item in objects)
            {
                if (method == Methods.Wakeup)
                    item.gameObject.SetActive(false);
                else
                    item.gameObject.SetActive(true);
            }
        }
    }

    private void OnEnable()
    {
        GameManager.OnSwitchScene += CheckControllersScene;
        GameManager.OnSwitchEvent += CheckControllersEvent;
    }

    private void OnDisable()
    {
        GameManager.OnSwitchScene -= CheckControllersScene;
        GameManager.OnSwitchEvent -= CheckControllersEvent;

        if (execute != null)
            CheckAll(false);

        StopAllCoroutines();
        execute = null;
    }

    private void OnDestroy()
    {
        if (execute != null)
            CheckAll(false);

        StopAllCoroutines();
        execute = null;
    }

    private void CheckControllersScene(SceneController passedController)
    {
        if (!initializeByOther || (initializeByOther && isInitialized))
        {
            if (checkByScene)
            {
                if (useTimed)
                    CheckAll(true);
                else
                    CheckAll(false);
            }
        }
    }

    private void CheckControllersEvent(GameEvent passedEvent)
    {
        if (!initializeByOther || (initializeByOther && isInitialized))
        {
            if (checkByEvent)
            {
                if (useTimed)
                    CheckAll(true);
                else
                    CheckAll(false);
            }
        }
    }

    private void CheckAll(bool useTimed)
    {
        bool check = false;

        if (sceneControllers.Count > 0)
        {
            if (gameManager.currentScene != null)
            {
                for (int i = 0; i < sceneControllers.Count; i++)
                {
                    if (sceneControllers[i] == gameManager.currentScene)
                    {
                        check = true;
                    }
                }
            }
        }

        if (gameEvents.Count > 0)
        {
            if (gameManager.currentGameEvent != null)
            {
                for (int i = 0; i < gameEvents.Count; i++)
                {
                    if (gameEvents[i] == gameManager.currentGameEvent)
                    {
                        check = true;
                    }
                }
            }
        }

        if(includeWorldScene && gameManager.currentScene == null)
        {
            check = true;
        }

        if (check)
        {
            CheckMethod(true, useTimed);
        }
        else
        {
            CheckMethod(false, useTimed);
        }
    }

    private void CheckMethod(bool doMethod, bool useTimed)
    {
        if (useTimed)
        {
            if (doMethod)
            {
                if (method == Methods.Wakeup)
                {
                    execute = StartCoroutine(WaitAndExecute(waitTime, true));
                }
                else
                {
                    execute = StartCoroutine(WaitAndExecute(waitTime, false));
                }
            }
            else
            {
                if (method == Methods.Wakeup)
                {
                    execute = StartCoroutine(WaitAndExecute(waitTime, false));
                }
                else
                {
                    execute = StartCoroutine(WaitAndExecute(waitTime, true));
                }
            }
        }
        else
        {
            if (doMethod)
            {
                if (method == Methods.Wakeup)
                {
                    ExecuteImmediately(true);
                }
                else
                {
                    ExecuteImmediately(false);
                }
            }
            else
            {
                if (method == Methods.Wakeup)
                {
                    ExecuteImmediately(false);
                }
                else
                {
                    ExecuteImmediately(true);
                }
            }
        }
    }

    private void ExecuteImmediately(bool isWakeUp)
    {
        if (isWakeUp)
        {
            foreach (var item in objects)
            {
                item.gameObject.SetActive(true);
            }
        }
        else
        {
            foreach (var item in objects)
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    private IEnumerator WaitAndExecute(float time, bool isWakeUp)
    {
        yield return new WaitForSeconds(time);

        if(isWakeUp)
        {
            foreach (var item in objects)
            {
                item.gameObject.SetActive(true);
            }
        }
        else
        {
            foreach (var item in objects)
            {
                item.gameObject.SetActive(false);
            }
        }
    }
}
