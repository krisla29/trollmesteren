﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mountain : MonoBehaviour
{
    public GameManager gameManager;
    private CameraMovement lastCamMovement;

    public Transform camTarget;
    private bool isActive;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(!gameManager.isScene)
            {               
                SetActive();
                isActive = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!gameManager.isScene)
            {
                Deactivate();
                isActive = false;
            }
        }
    }

    private void Update()
    {
        if(gameManager.isScene || gameManager.isBattle)
        {
            if (isActive)
                Deactivate();
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void SetActive()
    {
        lastCamMovement = gameManager.player.cameraMovementController.currentCameraMovement;
        lastCamMovement.enabled = false;
        gameManager.player.cameraMovementController.currentCameraMovement = null;
        gameManager.player.cameraMovementController.followerPos.gameObject.SetActive(true);
        gameManager.player.cameraMovementController.followerPos.target = camTarget;
        gameManager.player.cameraMovementController.followerLook.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameManager.player.cameraMovementController.currentCameraMovement = lastCamMovement;
        gameManager.player.cameraMovementController.currentCameraMovement.enabled = true;
        gameManager.player.cameraMovementController.followerPos.gameObject.SetActive(false);
        gameManager.player.cameraMovementController.followerLook.gameObject.SetActive(false);
    }
}
