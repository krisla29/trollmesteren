﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalTravelScene : MonoBehaviour
{
    public string nextScene;
    public List<Transform> objectsToScaleDown = new List<Transform>();
    public float waitTime = 25f;
    public List<Transform> objectsToHide = new List<Transform>();
    public float hideTime = 22f;

    private void Start()
    {
        StartCoroutine(StartNextScene());
        StartCoroutine(HideObjects());
    }

    private IEnumerator StartNextScene()
    {
        yield return new WaitForSeconds(waitTime * 0.5f);

        List<Vector3> startScales = new List<Vector3>();

        for (int i = 0; i < objectsToScaleDown.Count; i++)
        {
            startScales.Add(objectsToScaleDown[i].localScale);
        }

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / (waitTime * 0.5f);
            var h = LerpUtils.SmootherStep(t);
            for (int i = 0; i < objectsToScaleDown.Count; i++)
            {
                objectsToScaleDown[i].localScale = Vector3.Lerp(startScales[i], Vector3.zero, h);
            }
            yield return new WaitForEndOfFrame();
        }

        SceneManager.LoadSceneAsync(nextScene);
    }    

    private IEnumerator HideObjects()
    {
        yield return new WaitForSeconds(hideTime);

        foreach (var item in objectsToHide)
        {
            if(item)
            {
                item.gameObject.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
