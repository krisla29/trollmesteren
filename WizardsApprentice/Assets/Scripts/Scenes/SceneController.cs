﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public Transform player;
    public Camera sceneCamera;
    public SceneCamera sceneCameraScript;
    public Canvas sceneCanvas;
    public Collider sceneCollider;
    public Transform startPos;
    public Transform startPosSpriter;
    public Transform exitPos;
    public Transform cameraStart;
    public Transform cameraEnd;
    public AudioSource soundTrack;
    public Transform sceneLight;
    public bool hasCursor;
    public bool isNavmesh;
    public bool overrideNavMeshAgent;
    public bool disableAim;
    public bool forceAim;
    public bool lockPlayerMovement;
    public bool hidePlayer;
    public bool lockSpriterMovement;
    public bool hideSpriter;
    public bool hasBackButton;
    public bool teleportAgent;
    public bool isBattleScene;
}
