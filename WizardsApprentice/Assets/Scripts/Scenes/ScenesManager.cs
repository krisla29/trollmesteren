﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenesManager : MonoBehaviour
{
    public List<SceneController> scenes = new List<SceneController>();
    
    private void Awake()
    {
        foreach(var item in scenes)
        {
            item.sceneCamera.gameObject.SetActive(false);
            item.sceneCanvas.gameObject.SetActive(false);
        }
    }
    
}
