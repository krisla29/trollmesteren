﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSceneManager : MonoBehaviour
{
    //public bool isEntering = false;
    //public bool isExiting = false;
    public float timeToChange = 2f;
    public float timer = 0;
    private float startTime;
    public Camera transitionCamera;
    public Camera playerCamera;
    public PlayerController playerController;
    public Camera currentCamera;
    public SceneController lastSceneController;
    public SceneController currentSceneController;
    public Vector3 parentPos;
    public Quaternion parentRot;
    public Vector3 pos;
    public Quaternion rot;
    // public Camera eventCamera;
    public Transform eventCamParent;
    public GameEvent gameEvent;
    public List<GameEvent> eventsQueue = new List<GameEvent>();
    public List<Transform> scenesQueue = new List<Transform>();
    public Transform currentScene;    
    public bool isInsideSceneCollider;
    public HudCanvas hudCanvas;
    public GameManager gameManager;
    public delegate void CameraChange(Transform transform); // Sends to GameManager and CameraEvent and MOG_BossDoor
    public delegate void CameraChangeEvent(GameEvent gameEvent); // Sends to GameManager and CameraEvent and MOG_BossDoor
    public static event CameraChangeEvent OnChangeToEventCam;
    public static event CameraChangeEvent OnChangeFromEventCam;
    public static event CameraChange OnChangeToSceneCam;
    public static event CameraChange OnChangeToSceneCamStart;
    public static event CameraChange OnChangeToPlayerCam;
    public static event CameraChange OnChangeToPlayerCamStart;

    public SceneController bossSceneController;
    //private bool hasSwitchedFirstTime;
    //public int openWorldCount = 0;

    private void Start()
    {         
        hudCanvas = GameObject.FindGameObjectWithTag("HUD").GetComponent<HudCanvas>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        if (gameManager.player)
        {
            playerController = gameManager.player;
            playerCamera = gameManager.player.playerCam.GetComponent<Camera>();
            currentCamera = Camera.main;
        }
    }

    //Undone//Changed All PlayerCamera and SceneCamera localTransforms to global transforms !!!!-------- before, parent's transforms were global, obj's transforms were local (might be smoother)

    private IEnumerator CountDownToSceneCam(Transform other, bool fromPlayer)
    {
        currentSceneController = other.transform.parent.gameObject.GetComponent<SceneController>();

        CheckSceneCamPosition(currentSceneController, currentSceneController.cameraStart, currentSceneController.cameraEnd);        

        if (fromPlayer)
        {
            parentPos = playerCamera.transform.parent.position;
            parentRot = playerCamera.transform.parent.rotation;
            pos = playerCamera.transform.localPosition;
            rot = playerCamera.transform.localRotation;
        }       
        else
        {
            /*
            parentPos = lastSceneController.transform.position;
            parentRot = lastSceneController.transform.rotation;
            pos = lastSceneController.sceneCamera.transform.localPosition;
            rot = lastSceneController.sceneCamera.transform.localRotation;
            */
            parentPos = transitionCamera.transform.parent.position;
            parentRot = transitionCamera.transform.parent.rotation;
            pos = transitionCamera.transform.localPosition;
            rot = transitionCamera.transform.localRotation;
        }
        
        
        transitionCamera.transform.parent.position = parentPos;
        transitionCamera.transform.parent.rotation = parentRot;
        transitionCamera.transform.localPosition = pos;
        transitionCamera.transform.localRotation = rot;

        currentCamera = transitionCamera;
        currentCamera.gameObject.SetActive(true);

        if (fromPlayer)
        {
            playerCamera.enabled = false;
            playerCamera.GetComponent<AudioListener>().enabled = false;
        }
        else
        {
            lastSceneController.sceneCamera.gameObject.SetActive(false);
            lastSceneController.sceneCanvas.gameObject.SetActive(false);
        }
        //currentCamera.GetComponent<AudioListener>().enabled = true;

        timer = 0;

        while (timer < 1)
        {
            timer += Time.deltaTime / timeToChange;

            var lerp = Mathf.Lerp(0.0f, 1.0f, timer);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            var lerpToScenePos = Vector3.Lerp(parentPos, currentSceneController.transform.position, smoothLerp);
            var lerpToSceneCamPos = Vector3.Lerp(pos, currentSceneController.sceneCamera.transform.localPosition, smoothLerp);
            var lerpToSceneRot = Quaternion.Slerp(parentRot, currentSceneController.transform.rotation, smoothLerp);
            var lerpToSceneCamRot = Quaternion.Slerp(rot, currentSceneController.sceneCamera.transform.localRotation, smoothLerp);

            transitionCamera.transform.parent.position = lerpToScenePos;
            transitionCamera.transform.localPosition = lerpToSceneCamPos;
            transitionCamera.transform.parent.rotation = lerpToSceneRot;
            transitionCamera.transform.localRotation = lerpToSceneCamRot;
           
            yield return new WaitForEndOfFrame();
        }

        if(currentScene == other)
        {
            ChangeToSceneCam(other);
        }

        //isEntering = false;
    }

    private IEnumerator CountDownToPlayerCam(Transform other)
    {
        currentSceneController = other.transform.parent.gameObject.GetComponent<SceneController>();

        parentPos = currentSceneController.transform.position;
        parentRot = currentSceneController.transform.rotation;
        
        pos = currentSceneController.sceneCamera.transform.localPosition;
        rot = currentSceneController.sceneCamera.transform.localRotation;
        
        transitionCamera.transform.parent.position = parentPos;
        transitionCamera.transform.parent.rotation = parentRot;
        transitionCamera.transform.localPosition = pos;
        transitionCamera.transform.localRotation = rot;

        currentCamera = transitionCamera;
        currentCamera.gameObject.SetActive(true);
        currentSceneController.sceneCamera.gameObject.SetActive(false);
        currentSceneController.sceneCanvas.gameObject.SetActive(false);

        if (currentSceneController.sceneLight)
            currentSceneController.sceneLight.gameObject.SetActive(true);

        playerCamera.GetComponent<AudioListener>().enabled = false;
        //sController.sceneCamera.gameObject.GetComponent<AudioListener>().enabled = true;

        timer = 0;

        while (timer < 1)
        {
            timer += Time.deltaTime / timeToChange;

            var lerp = Mathf.Lerp(0.0f, 1.0f, timer);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            var lerpToPlayerPos = Vector3.Lerp(parentPos, playerCamera.transform.parent.position, smoothLerp);
            var lerpToPlayerCamPos = Vector3.Lerp(pos, playerCamera.transform.localPosition, smoothLerp);
            var lerpToPlayerRot = Quaternion.Slerp(parentRot, playerCamera.transform.parent.rotation, smoothLerp);
            var lerpToPlayerCamRot = Quaternion.Slerp(rot, playerCamera.transform.localRotation, smoothLerp);

            transitionCamera.transform.parent.position = lerpToPlayerPos;
            transitionCamera.transform.localPosition = lerpToPlayerCamPos;
            transitionCamera.transform.parent.rotation = lerpToPlayerRot;
            transitionCamera.transform.localRotation = lerpToPlayerCamRot;
            
            yield return new WaitForEndOfFrame();
        }

        if (currentSceneController.isBattleScene)
            currentScene = null;

        if (currentScene == null)
        {
            ChangeToPlayerCam(other);
        }
        
        //isExiting = false;
    }    

    private void ChangeToEventCam(GameEvent cameraEvent)
    {        
        //var currCamEvent = cameraEventTransform.GetComponent<CameraEvent>();
        eventsQueue.Add(cameraEvent);
       
        if (eventsQueue.Count > 0)
        {                        
            StartCamEvent(eventsQueue[0]);
            print("CameraSceneManager recieved camera event");
        }
    }

    private void StartCamEvent(GameEvent passedEvent)
    {       
        gameEvent = passedEvent;
        currentCamera = gameEvent.eventCamera;
        currentCamera.gameObject.SetActive(true);

        if (!isInsideSceneCollider)
        {
            if (!transitionCamera.gameObject.activeSelf)
            {
                playerCamera.enabled = false;
                playerCamera.GetComponent<AudioListener>().enabled = false;
                currentCamera.GetComponent<AudioListener>().enabled = true;
                playerController.disableAim = true;
                SetHudElements(false);
                playerController.toggleAim = false;
                playerController.toggleAimThirdPerson = false;                
            }
            else
            {
                transitionCamera.gameObject.SetActive(false);
            }            
        }
        else
        {
            currentSceneController.sceneCamera.gameObject.SetActive(false);
            currentSceneController.sceneCanvas.gameObject.SetActive(false);
        }

        if (gameEvent.lockPlayer)
            gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;

        if (gameEvent.playerStartPos)
        {
            if (!gameManager.isNavMeshAgent)
            {
                playerController.transform.position = gameEvent.playerStartPos.position;
            }
            else
            {
                playerController.playerMovementController.playerMovementAgent.SetNewDestination(gameEvent.playerStartPos.position);
            }
        }

        if (OnChangeToEventCam != null)
        {
            OnChangeToEventCam(passedEvent);
        }

        //Resources.UnloadUnusedAssets();
    }

    private void ChangeFromEventCam(GameEvent passedEvent)
    {        
        var nIndex = 0;

        for (int i = 0; i < eventsQueue.Count; i++)
        {
            if(eventsQueue[i] == passedEvent)
            {
                nIndex = i;
                print("nIndex = " + nIndex);
            }
        }

        if(nIndex > 0)
        {
            StartCamEvent(eventsQueue[nIndex - 1]);
            StopCamEvent(passedEvent);
        }
        else
        {
            StopCamEvent(passedEvent);
        }


    }

    private void StopCamEvent(GameEvent passedEvent)
    {
        //var curCamEvent = passedEventTransform.GetComponent<CameraEvent>();
        eventsQueue.Remove(passedEvent);

        if (eventsQueue.Count == 0)
        {
            if (gameEvent.deactivateOnCompletion)
                gameEvent.gameObject.SetActive(false);

            gameEvent = null;

            if (!isInsideSceneCollider)
            {
                playerCamera.enabled = true;
                currentCamera = playerCamera;
                playerCamera.GetComponent<AudioListener>().enabled = true;
                playerController.playerCam = playerCamera.transform;
                playerController.orientTransform = playerCamera.transform;
                //playerController.isNavMeshAgent = false;
                playerController.isLocked = false;
                playerController.playerMovementController.playerMovementAgent.lockAgent = false;

                if (!gameManager.isNavMeshAgent)
                    playerController.isNavMeshAgent = false;
                else
                    playerController.isNavMeshAgent = true;

                playerController.disableAim = false;
                playerController.forceAim = false;

                if (!gameManager.isOnMountainTop && gameManager.player.characterSelector.playerCharacter != CharacterSelector.PlayerCharacter.Wizard)
                    playerController.spriter.gameObject.SetActive(true);
                else
                    playerController.spriter.gameObject.SetActive(false);

                playerController.spriter.followerScript.target = playerController.spriter.playerTarget;
                playerController.spriter.isLocked = false;
                gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
                SetHudElements(true);
            }
            else
            {
                //currentSceneController.sceneCamera.gameObject.SetActive(true);
                //currentSceneController.sceneCanvas.gameObject.SetActive(true);
                ChangeToSceneCam(currentSceneController.sceneCollider.transform);

                if(!currentSceneController.lockPlayerMovement)
                {
                    playerController.isLocked = false;
                    gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;

                    if (!gameManager.isNavMeshAgent)
                        playerController.isNavMeshAgent = false;
                    else
                        playerController.isNavMeshAgent = true;
                }
            }

            if (OnChangeFromEventCam != null)
            {
                OnChangeFromEventCam(passedEvent);
            }           
        }

        //Resources.UnloadUnusedAssets();
    }

    void LockPlayer()
    {
        //playerController.isLocked = true;
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
    }

    public void ChangeToSceneCam(Transform other)
    {
        currentSceneController = other.transform.parent.gameObject.GetComponent<SceneController>();
        parentPos = currentSceneController.transform.position;
        parentRot = currentSceneController.transform.rotation;
        currentCamera = currentSceneController.sceneCamera;
        currentCamera.gameObject.SetActive(true);
        currentCamera.gameObject.tag = "MainCamera";
        currentSceneController.sceneCanvas.gameObject.SetActive(true);
        playerController.playerCam = currentCamera.transform;

        if (currentSceneController.sceneCameraScript.sceneTransform != null)
        {
            playerController.orientTransform = currentSceneController.sceneCameraScript.sceneTransform;
        }
        playerCamera.enabled = false;
        playerCamera.GetComponent<AudioListener>().enabled = false;
        transitionCamera.gameObject.SetActive(false);
        //playerCamera.GetComponent<AudioListener>().enabled = false;
        SetHudElements(false);

        if (currentSceneController.sceneLight)
            currentSceneController.sceneLight.gameObject.SetActive(false);

        if (currentSceneController.hasBackButton)
            hudCanvas.exitButton.gameObject.SetActive(true);

        if(currentSceneController.soundTrack)
        {
            gameManager.soundTrack.gameObject.SetActive(false);
            currentSceneController.soundTrack.gameObject.SetActive(true);
            currentSceneController.soundTrack.Play();
        }
        if (currentSceneController.isNavmesh)
        {
            playerController.isNavMeshAgent = true;           
        }
        if (currentSceneController.disableAim)
        {
            playerController.disableAim = true;

            if (hudCanvas.aimButton.gameObject.activeSelf)
            {
                hudCanvas.aimButton.gameObject.SetActive(false);                
            }
            if (playerController.toggleAim)
            {
                playerController.toggleAim = false;
            }
            if (playerController.toggleAimThirdPerson)
            {
                playerController.toggleAimThirdPerson = false;
            }
        }
        if (currentSceneController.overrideNavMeshAgent)
            playerController.overrideNavMeshAgent = true;
        if (currentSceneController.forceAim)
            playerController.forceAim = true;
        if (currentSceneController.lockPlayerMovement)
        {
            //playerController.isLocked = true;
            playerController.playerMovementController.playerMovementAgent.lockAgent = true;
        }
        if (currentSceneController.startPos != null)
        {
            if (!gameManager.isNavMeshAgent || !currentSceneController.isNavmesh || currentSceneController.lockPlayerMovement)
            {
                playerController.transform.position = currentSceneController.startPos.position;
                playerController.transform.rotation = currentSceneController.startPos.rotation;
            }
            else
            {
                playerController.playerMovementController.playerMovementAgent.SetNewDestination(currentSceneController.startPos.position);
                playerController.playerMovementController.playerMovementAgent.SetRotationAtDestination(currentSceneController.startPos.rotation);
            }
        }
        if(currentSceneController.startPosSpriter != null)
        {
            playerController.spriter.followerScript.target = currentSceneController.startPosSpriter;
        }
        if(currentSceneController.lockSpriterMovement)
        {
            playerController.spriter.isLocked = true;
        }
        if(currentSceneController.hidePlayer)
        {
            playerController.gameObject.SetActive(false);
        }
        if(currentSceneController.hideSpriter)
        {
            playerController.spriter.gameObject.SetActive(false);
        }

        if (OnChangeToSceneCam != null)
        {
            OnChangeToSceneCam(currentSceneController.transform);
        }

        if(currentSceneController == bossSceneController)
        {
            scenesQueue.Clear();
        }

        //Resources.UnloadUnusedAssets();
    }

    public void ChangeToPlayerCam(Transform other)
    {
        currentSceneController = other.transform.parent.gameObject.GetComponent<SceneController>();
        currentSceneController.sceneCamera.gameObject.tag = "SceneCam";
        currentSceneController.sceneCamera.gameObject.SetActive(false);
        currentSceneController.sceneCanvas.gameObject.SetActive(false);
        transitionCamera.gameObject.SetActive(false);
        playerController.gameObject.SetActive(true);

        if(!gameManager.isOnMountainTop && gameManager.player.characterSelector.playerCharacter != CharacterSelector.PlayerCharacter.Wizard)
            playerController.spriter.gameObject.SetActive(true);
        else
            playerController.spriter.gameObject.SetActive(false);

        playerController.orientTransform = playerCamera.transform;

        playerCamera.enabled = true;
        currentCamera = playerCamera;
        playerCamera.GetComponent<AudioListener>().enabled = true;
        playerController.playerCam = playerCamera.transform;

        if (currentSceneController.soundTrack)
        {            
            currentSceneController.soundTrack.gameObject.SetActive(false);
            gameManager.soundTrack.gameObject.SetActive(true);

            //if(hasSwitchedFirstTime)
            gameManager.soundTrack.Play();
        }

        if (!gameManager.isNavMeshAgent)
            playerController.isNavMeshAgent = false;
        else
        {
            playerController.isNavMeshAgent = true;
            playerController.navMeshAgent.speed = 5f;
        }
        //if(currentSceneController.hasBackButton && playerController.isNavMeshAgent)
        //playerController.playerMovementController.playerMovementAgent.SetNewDestination(currentSceneController.exitPos.position);
        playerController.overrideNavMeshAgent = false;
        playerController.navMeshAgent.baseOffset = 0;
        playerController.disableAim = false;
        playerController.forceAim = false;
        playerController.isLocked = false;
        playerController.playerMovementController.playerMovementAgent.lockAgent = false;
        playerController.spriter.followerScript.target = playerController.spriter.playerTarget;
        playerController.spriter.isLocked = false;

        lastSceneController = null;
        currentScene = null;
        isInsideSceneCollider = false;
        scenesQueue.Clear();

        SetHudElements(true);
        /*
        if(sController.exitPos != null)
        {
            if (!gameManager.isNavMeshAgent && !sController.isNavmesh)
            {
                playerController.transform.position = sController.exitPos.position;
            }
            else
            {
                playerController.playerMovementController.playerMovementAgent.SetNewDestination(sController.exitPos.position);
            }
        }
        */
        if (OnChangeToPlayerCam != null)
        {
            OnChangeToPlayerCam(playerController.playerCam.transform);
        }

        //hasSwitchedFirstTime = true;
        //openWorldCount++;
        //print(openWorldCount);
        //Resources.UnloadUnusedAssets();
    }

    private void TriggerEnterScene(Transform triggerObject, Transform player)
    {
        if (triggerObject.gameObject.CompareTag("Scene"))
        {
            timer = 0;
            isInsideSceneCollider = true;

            if (scenesQueue.Count == 0)
                lastSceneController = triggerObject.parent.gameObject.GetComponent<SceneController>();
            else
                lastSceneController = scenesQueue[scenesQueue.Count - 1].parent.gameObject.GetComponent<SceneController>();

            print(lastSceneController.gameObject.name);
            //isEntering = true;
            //isExiting = false;

            if (!scenesQueue.Contains(triggerObject))
                scenesQueue.Add(triggerObject);

            if (OnChangeToSceneCamStart != null)
            {
                OnChangeToSceneCamStart(triggerObject.transform.parent); // is scene 
            }

            if (currentScene == null)
            {
                StartCoroutine(CountDownToSceneCam(triggerObject, true));
            }
            else
            {
                StartCoroutine(CountDownToSceneCam(triggerObject, false));
            }

            currentScene = triggerObject;            
        }

        //Resources.UnloadUnusedAssets();
    }

    public void MapEnterScene(Transform scene)
    {
        timer = 0;
        isInsideSceneCollider = true;

        if (scenesQueue.Count == 0)
            lastSceneController = scene.gameObject.GetComponent<SceneController>();
        else
            lastSceneController = scenesQueue[scenesQueue.Count - 1].parent.gameObject.GetComponent<SceneController>();

        print(lastSceneController.gameObject.name);

        if (!scenesQueue.Contains(lastSceneController.sceneCollider.transform))
            scenesQueue.Add(lastSceneController.sceneCollider.transform);

        if (OnChangeToSceneCamStart != null)
        {
            OnChangeToSceneCamStart(scene); // is scene 
        }

        if (currentScene == null)
        {
            StartCoroutine(CountDownToSceneCam(lastSceneController.sceneCollider.transform, true));
        }
        else
        {
            StartCoroutine(CountDownToSceneCam(lastSceneController.sceneCollider.transform, false));
        }

        currentScene = lastSceneController.sceneCollider.transform;
    }

    private void TriggerExitScene(Transform triggerObject, Transform player)
    {
        if (triggerObject.gameObject.CompareTag("Scene"))
        {
            timer = 0;
            //isExiting = true;
            //isEntering = false;            

            if (scenesQueue.Contains(triggerObject))
                scenesQueue.Remove(triggerObject);

            if (scenesQueue.Count == 0)
                lastSceneController = null;
            else
                lastSceneController = triggerObject.parent.gameObject.GetComponent<SceneController>();

            if (OnChangeToPlayerCamStart != null)
            {
                OnChangeToPlayerCamStart(playerCamera.transform); // is Player Cam Collider
            }

            if (scenesQueue.Count == 0)
            {
                scenesQueue.Clear();
                currentScene = null;
                isInsideSceneCollider = false;
                StartCoroutine(CountDownToPlayerCam(triggerObject));
            }
            else
            {
                StartCoroutine(CountDownToSceneCam(scenesQueue[scenesQueue.Count - 1], false));
                currentScene = scenesQueue[scenesQueue.Count - 1];
            }
        }

        //Resources.UnloadUnusedAssets();
    }

    private void OnEnable()
    {
        TriggerByTag.OnTriggerByTagEnter += TriggerEnterScene;
        TriggerByTag.OnTriggerByTagExit += TriggerExitScene;
        GameEvent.OnCameraEventStart += ChangeToEventCam;
        GameEvent.OnCameraEventStop += ChangeFromEventCam;
        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;
        BattleManager.OnVictory += StartCountdownToPlayerCam;
    }

    private void OnDisable()
    {
        TriggerByTag.OnTriggerByTagEnter -= TriggerEnterScene;
        TriggerByTag.OnTriggerByTagExit -= TriggerExitScene;
        GameEvent.OnCameraEventStart -= ChangeToEventCam;
        GameEvent.OnCameraEventStop -= ChangeFromEventCam;
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
        BattleManager.OnVictory -= StartCountdownToPlayerCam;
        StopAllCoroutines();
    }

    void ReferenceNewPlayer(Transform transform)
    {
        playerController = transform.GetComponent<PlayerController>();
        playerCamera = playerController.playerCam.GetComponent<Camera>();
        currentCamera = Camera.main;
    }

    void CheckSceneCamPosition(SceneController sceneController, Transform camStart, Transform camEnd)
    {
        if (camEnd)
        {
            if (camStart)
            {
                if (Vector3.Distance(playerController.transform.position, camEnd.position) < Vector3.Distance(playerController.transform.position, camStart.position))
                {
                    sceneController.sceneCamera.transform.position = camEnd.position;
                    sceneController.sceneCamera.transform.rotation = camEnd.rotation;
                }
                else
                {
                    sceneController.sceneCamera.transform.position = camStart.position;
                    sceneController.sceneCamera.transform.rotation = camStart.rotation;
                }
            }
            else
            {
                sceneController.sceneCamera.transform.position = camEnd.position;
                sceneController.sceneCamera.transform.rotation = camEnd.rotation;
            }
        }
        if (!camEnd && camStart)
        {
            sceneController.sceneCamera.transform.position = camStart.position;
            sceneController.sceneCamera.transform.rotation = camStart.rotation;
        }
    }

    private void StartCountdownToPlayerCam(Transform passedCollider)
    {
        StartCoroutine(CountDownToPlayerCam(passedCollider));
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void SetHudElements(bool visibility)
    {
        if (gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            //hudCanvas.camJoystick.gameObject.SetActive(visibility);
            //hudCanvas.actionButton.gameObject.SetActive(visibility);
                //hudCanvas.zoomSlider.gameObject.SetActive(visibility);
                //hudCanvas.rotateSlider.gameObject.SetActive(visibility);
            //hudCanvas.mainJoystick.gameObject.SetActive(visibility);
            //hudCanvas.aimButton.gameObject.SetActive(visibility);
                //hudCanvas.toggleCharacterButton.gameObject.SetActive(visibility);
                //if(!gameManager.isWorldMap)
                    //hudCanvas.worldMapButton.gameObject.SetActive(visibility);
        }
    }
}
