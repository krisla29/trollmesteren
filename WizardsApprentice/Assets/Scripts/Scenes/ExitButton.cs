﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ExitButton : MonoBehaviour
{
    public GameManager gameManager;

    public delegate void ExitButtonEvent(Transform currentSceneTransform); //Sends to BattleManager
    public static event ExitButtonEvent OnExitButtonPress;
    
    public void MovePlayerToSceneExit()
    {
        if(gameManager.player.navMeshAgent.agentTypeID != 0)
            gameManager.player.navMeshAgent.agentTypeID = 0;

        if (OnExitButtonPress != null)
        {
            if(gameManager.currentScene)
                OnExitButtonPress(gameManager.currentScene.transform);
        }

        if (!gameManager.isBattle)
        {
            if (!gameManager.isNavMeshAgent || !gameManager.currentScene.isNavmesh || gameManager.currentScene.lockPlayerMovement || gameManager.currentScene.hidePlayer)
            {
                if (!gameManager.player.gameObject.activeSelf)
                {
                    gameManager.player.gameObject.SetActive(true);
                }

                if (gameManager.currentScene.exitPos != null)
                {
                    if (gameManager.player.isNavMeshAgent)
                    {
                        gameManager.player.navMeshAgent.Warp(gameManager.currentScene.exitPos.position);
                        gameManager.player.playerMovementController.playerMovementAgent.SetNewDestination(gameManager.currentScene.exitPos.position);
                    }

                    gameManager.player.transform.position = gameManager.currentScene.exitPos.position;
                }

                gameManager.cameraSceneManager.ChangeToPlayerCam(gameManager.currentScene.sceneCollider.transform);
            }
            else
            {
                if (gameManager.currentScene.exitPos != null)
                {
                    if (gameManager.currentScene.teleportAgent)
                    {
                        gameManager.player.navMeshAgent.Warp(gameManager.currentScene.exitPos.position);
                        gameManager.player.playerMovementController.playerMovementAgent.SetNewDestination(gameManager.currentScene.exitPos.position);
                    }
                    else
                    {
                        gameManager.player.playerMovementController.playerMovementAgent.SetNewDestination(gameManager.currentScene.exitPos.position);
                    }
                }
            }
        }

        gameManager.player.navMeshAgent.baseOffset = 0;        
    }
}
