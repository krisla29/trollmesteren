﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveObject
{
    public bool ring1;
    public int ring1Points;
    public int ring1Instrument;
    public float ring1Volume;
    //public bool ring1Mute;
    //public bool ring1Solo;
    //public bool ring1forceResonance;
    public List<int> ring1Tones = new List<int>();
    public Vector3 ring1EulerAngles;

    public bool ring2;
    public int ring2Points;
    public int ring2Instrument;
    public float ring2Volume;
    //public bool ring2Mute;
    //public bool ring2Solo;
    //public bool ring2forceResonance;
    public List<int> ring2Tones = new List<int>();
    public Vector3 ring2EulerAngles;

    public bool ring3;
    public int ring3Points;
    public int ring3Instrument;
    public float ring3Volume;
    //public bool ring3Mute;
    //public bool ring3Solo;
    //public bool ring3forceResonance;
    public List<int> ring3Tones = new List<int>();
    public Vector3 ring3EulerAngles;

    public bool ring4;
    public int ring4Points;
    public int ring4Instrument;
    public float ring4Volume;
    //public bool ring4Mute;
    //public bool ring4Solo;
    //public bool ring4forceResonance;
    public List<int> ring4Tones = new List<int>();
    public Vector3 ring4EulerAngles;

    public float tempo;

    public string date;
    public string time;    
}
