﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveObjectWebGl
{
    public bool a_;

    public bool a_ring1;
    public int a_ring1Points;
    public int a_ring1Instrument;
    public float a_ring1Volume;
    public List<int> a_ring1Tones = new List<int>();
    public Vector3 a_ring1EulerAngles;

    public bool a_ring2;
    public int a_ring2Points;
    public int a_ring2Instrument;
    public float a_ring2Volume;
    public List<int> a_ring2Tones = new List<int>();
    public Vector3 a_ring2EulerAngles;

    public bool a_ring3;
    public int a_ring3Points;
    public int a_ring3Instrument;
    public float a_ring3Volume;
    public List<int> a_ring3Tones = new List<int>();
    public Vector3 a_ring3EulerAngles;

    public bool a_ring4;
    public int a_ring4Points;
    public int a_ring4Instrument;
    public float a_ring4Volume;
    public List<int> a_ring4Tones = new List<int>();
    public Vector3 a_ring4EulerAngles;

    public float a_tempo;

    public string a_date;
    public string a_time;

    public bool b_;

    public bool b_ring1;
    public int b_ring1Points;
    public int b_ring1Instrument;
    public float b_ring1Volume;
    public List<int> b_ring1Tones = new List<int>();
    public Vector3 b_ring1EulerAngles;

    public bool b_ring2;
    public int b_ring2Points;
    public int b_ring2Instrument;
    public float b_ring2Volume;
    public List<int> b_ring2Tones = new List<int>();
    public Vector3 b_ring2EulerAngles;

    public bool b_ring3;
    public int b_ring3Points;
    public int b_ring3Instrument;
    public float b_ring3Volume;
    public List<int> b_ring3Tones = new List<int>();
    public Vector3 b_ring3EulerAngles;

    public bool b_ring4;
    public int b_ring4Points;
    public int b_ring4Instrument;
    public float b_ring4Volume;
    public List<int> b_ring4Tones = new List<int>();
    public Vector3 b_ring4EulerAngles;

    public float b_tempo;

    public string b_date;
    public string b_time;

    public bool c_;

    public bool c_ring1;
    public int c_ring1Points;
    public int c_ring1Instrument;
    public float c_ring1Volume;
    public List<int> c_ring1Tones = new List<int>();
    public Vector3 c_ring1EulerAngles;

    public bool c_ring2;
    public int c_ring2Points;
    public int c_ring2Instrument;
    public float c_ring2Volume;
    public List<int> c_ring2Tones = new List<int>();
    public Vector3 c_ring2EulerAngles;

    public bool c_ring3;
    public int c_ring3Points;
    public int c_ring3Instrument;
    public float c_ring3Volume;
    public List<int> c_ring3Tones = new List<int>();
    public Vector3 c_ring3EulerAngles;

    public bool c_ring4;
    public int c_ring4Points;
    public int c_ring4Instrument;
    public float c_ring4Volume;
    public List<int> c_ring4Tones = new List<int>();
    public Vector3 c_ring4EulerAngles;

    public float c_tempo;

    public string c_date;
    public string c_time;

    public bool d_;

    public bool d_ring1;
    public int d_ring1Points;
    public int d_ring1Instrument;
    public float d_ring1Volume;
    public List<int> d_ring1Tones = new List<int>();
    public Vector3 d_ring1EulerAngles;

    public bool d_ring2;
    public int d_ring2Points;
    public int d_ring2Instrument;
    public float d_ring2Volume;
    public List<int> d_ring2Tones = new List<int>();
    public Vector3 d_ring2EulerAngles;

    public bool d_ring3;
    public int d_ring3Points;
    public int d_ring3Instrument;
    public float d_ring3Volume;
    public List<int> d_ring3Tones = new List<int>();
    public Vector3 d_ring3EulerAngles;

    public bool d_ring4;
    public int d_ring4Points;
    public int d_ring4Instrument;
    public float d_ring4Volume;
    public List<int> d_ring4Tones = new List<int>();
    public Vector3 d_ring4EulerAngles;

    public float d_tempo;

    public string d_date;
    public string d_time;

    public bool e_;

    public bool e_ring1;
    public int e_ring1Points;
    public int e_ring1Instrument;
    public float e_ring1Volume;
    public List<int> e_ring1Tones = new List<int>();
    public Vector3 e_ring1EulerAngles;

    public bool e_ring2;
    public int e_ring2Points;
    public int e_ring2Instrument;
    public float e_ring2Volume;
    public List<int> e_ring2Tones = new List<int>();
    public Vector3 e_ring2EulerAngles;

    public bool e_ring3;
    public int e_ring3Points;
    public int e_ring3Instrument;
    public float e_ring3Volume;
    public List<int> e_ring3Tones = new List<int>();
    public Vector3 e_ring3EulerAngles;

    public bool e_ring4;
    public int e_ring4Points;
    public int e_ring4Instrument;
    public float e_ring4Volume;
    public List<int> e_ring4Tones = new List<int>();
    public Vector3 e_ring4EulerAngles;

    public float e_tempo;

    public string e_date;
    public string e_time;
}
