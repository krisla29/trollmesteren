//using Megapop.Logging;
using System;
using System.Collections.Generic;


/// <summary>
/// ----------------------------------------Example Only!!!!
/// </summary>
[Serializable]
public class GameProgressData
{
    //private ILog Log { get; } = Logs.Get(LogCategory.Client);

    public const string BoolsKey = "_bool";
    public const string IntsKey = "_integers";
    public const string ItemsKey = "_items";
    public const string StringsKey = "_strings";

    // TODO version number

    //public readonly Dictionary<string, ItemLocation> Items = new Dictionary<string, ItemLocation>();
    public readonly Dictionary<string, bool> Booleans = new Dictionary<string, bool>();
    public readonly Dictionary<string, int> Integers = new Dictionary<string, int>();
    public readonly Dictionary<string, string> Strings = new Dictionary<string, string>();

    public Dictionary<string, object> ToExportDictionary()
    {
        var dict = new Dictionary<string, object>
        {
            {BoolsKey, Booleans},
            {IntsKey, Integers},
            //{ItemsKey, Items},
            {StringsKey, Strings}
        };

        return dict;
    }

    public void LoadExternalData(Dictionary<string, object> data)
    {
        if (data[BoolsKey] is Dictionary<string, object> booleans)
        {
            foreach (var kvp in booleans)
            {
                var boolValue = Convert.ToBoolean(kvp.Value);

                if (!Booleans.ContainsKey(kvp.Key))
                    Booleans.Add(kvp.Key, boolValue);
                else
                    Booleans[kvp.Key] = boolValue;
            }
        }

        if (data[IntsKey] is Dictionary<string, object> integers)
        {
            foreach (var kvp in integers)
            {
                var intValue = Convert.ToInt32(kvp.Value);

                if (!Integers.ContainsKey(kvp.Key))
                    Integers.Add(kvp.Key, intValue);
                else
                    Integers[kvp.Key] = intValue;
            }
        }
        /*
        if (data[ItemsKey] is Dictionary<string, object> items)
        {
            foreach (var kvp in items)
            {
                Enum.TryParse(kvp.Value as string, out ItemLocation itemLocationValue);

                if (!Items.ContainsKey(kvp.Key))
                    Items.Add(kvp.Key, itemLocationValue);
                else
                    Items[kvp.Key] = itemLocationValue;
            }
        }
        */
        if (data[StringsKey] is Dictionary<string, object> strings)
        {
            foreach (var kvp in strings)
            {
                var s = kvp.Value as string;

                if (!Strings.ContainsKey(kvp.Key))
                    Strings.Add(kvp.Key, s);
                else
                    Strings[kvp.Key] = s;
            }
        }
    }

    public void Clear()
    {
        Booleans.Clear();
        Integers.Clear();
        //Items.Clear();
        Strings.Clear();
    }
}


