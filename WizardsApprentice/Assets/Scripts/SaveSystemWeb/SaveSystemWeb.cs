﻿
using MEC;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveSystemWeb
{
    public delegate void WebSaveEvents(PlayerDataWeb playerData);
    public static event WebSaveEvents OnLoadWebData;

    public static void InitWebGl()
    {
        Debug.Log("Redirecting WebGl init to RestManager2...");
        Timing.RunCoroutine(RestManager2.instance.Initialize());
    }
    
    public static void SaveWebGl(PlayerStats playerStats)
    {
        Debug.Log("saving player web data...");
        PlayerDataWeb data = SavePlayerDataWeb.SaveData(playerStats);
        string json = JsonUtility.ToJson(data);
        Timing.RunCoroutine(RestManager2.instance.SaveGame(json));
    }
   
    public static void LoadWebGl()
    {
        Debug.Log("loading player web data...");
        Timing.RunCoroutine(RestManager2.instance.LoadSaveGame());
    }

    public static void ReturnWebGlLoad(string saveString) //callback
    {
        Debug.Log("getting player web data...");
        PlayerDataWeb saveGame = JsonUtility.FromJson<PlayerDataWeb>(saveString);

        if (OnLoadWebData != null)
            OnLoadWebData(saveGame);
    }
}
