﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDataWeb
{
    public int[] trophies;
    public int difficulty;
    public float battleSortEnemyHealth;

    public int gems;
    public int wellGems;
    public int[] worldGemsTaken;
    public int[] nightGemsTaken;

    public int[] timeOfDay;

    public int lastLocation;
    public int[] unlockedLocations;

    public int[] playedEvents;

    public int currentCharacter;
    public int[] unlockedCharacters;
    public int startCharacter;

    public bool hasSeenTheWellFirstStep;
    public bool hasMetWizard;
    public bool hasCompletedWizard;
    public bool hasMetUnderboss;
    public bool hasSeenDifficultyPanelDemo;
    public bool hasSeenTheWellDemo;
    public bool hasSeenOpening;
    public bool hasSeenInteractionDemo;
    public bool hasSeenTrophyDemo;
    public bool hasUnlockedMap;
    public bool hasUnlockedHat;
    public bool hasOpenedCaveDoor;
    public bool hasOpenedTowerScanner;
    public bool hasOpenedBossDoor;

    public int OrderGameSetCount;
    public bool hasWonOrderGame;
    public int[] orderGameCompletedSets;
    public int TileGameSetCount;
    public bool hasWonTileGame;
    public int[] tileGameCompletedSets;
    public int[] tileGameSetAchievementsKeys;
    public int[] tileGameSetAchievementsValues;
    public int BossTileGameSetCount;
    public bool hasWonBossTileGame;
    public int[] bossTileGameCompletedSets;
    public int[] bossTileGameSetAchievementsKeys;
    public int[] bossTileGameSetAchievementsValues;
    public int ShooterSetCount;
    public bool hasWonShooterGame;
    public int[] shooterGameCompletedSets;
    public int[] shooterGameSetAchievementsKeys;
    public int[] shooterGameSetAchievementsValues;
    public int MazeGameSetCount;
    public bool hasWonMazeGame;
    public int[] mazeGameCompletedSets;
    public int[] mazeGameSetAchievementsKeys;
    public int[] mazeGameSetAchievementsValues;
    public bool hasWonAreaVolumeGame;
    public int AreaVolumeGameSetCount;
    public int[] areaVolumeGameCompletedSets;
    public int[] areaVolumeGameSetAchievementsKeys;
    public int[] areaVolumeGameSetAchievementsValues;
    public int StarMakerGameSetCount;
    public bool hasWonStarMakerGame;
    public int[] starMakerGameCompletedSets;
    public int FiguresGameSetCount;
    public bool hasWonFiguresGame;
    public int[] figuresGameFiguresCompleted;
    public bool hasWonClockGame;
    public int clockGameCompletedTasks;
    public int ClockGameSetCount;
    public bool hasWonRacingGame;
    public bool hasWonBattleSort;
    public bool hasPlayedBattleDemoDesc;
    public bool hasPlayedBattleDemoAsc;
}
