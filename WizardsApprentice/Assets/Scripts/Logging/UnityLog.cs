﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Megapop.Logging
{
    public class UnityLog : DefaultLog
    {
        [CanBeNull] private UnityEngine.Object Context { get; }

        public UnityLog(LogCategory category, UnityEngine.Object context) : base(category)
        {
            Context = context;
        }

        public override void Trace(object message)
        {
            if (Skip(LogLevel.Trace)) return;

            Debug.Log(Format(message), Context);
        }

        public override void Info(object message)
        {
            if (Skip(LogLevel.Info)) return;

            Debug.Log(Format(message), Context);
        }

        public override void Warn(object message)
        {
            if (Skip(LogLevel.Warn)) return;

            Debug.LogWarning(Format(message), Context);
        }

        public override void WarnOnce(string id, object message)
        {
            if (Skip(LogLevel.Warn) || LoggedOnce.Contains(id)) return;

            LoggedOnce.Add(id);

            Debug.LogWarning($"{Format(message)} (Logged Once)", Context);
        }

        public override void Error(object message)
        {
            Debug.LogError(Format(message), Context);
        }

        public override void Exception(Exception e)
        {
            Debug.LogException(e, Context);
        }
    }
}