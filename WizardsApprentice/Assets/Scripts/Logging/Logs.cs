﻿using System;
using System.Collections.Generic;

namespace Megapop.Logging
{
    public static class Logs
    {
        public static readonly Dictionary<LogCategory, LogLevel> LogLevels = new Dictionary<LogCategory, LogLevel>();
        private static readonly Dictionary<LogCategory, ILog> loggers = new Dictionary<LogCategory, ILog>();

        public static ILog Get(LogCategory category)
        {
            if (!loggers.ContainsKey(category))
            {
                loggers[category] = new DefaultLog(category);
            }

            return loggers[category];
        }

        public static void SetRedirectLog(ILog log)
        {
            var logCategories = Enum.GetValues(typeof(LogCategory));
            foreach (LogCategory logCategory in logCategories)
            {
                if (!(Get(logCategory) is DefaultLog redirectLog)) continue;
                redirectLog.RedirectLog = log;
            }
        }

#if UNITY_2018_1_OR_NEWER

        public static ILog Get(LogCategory category, UnityEngine.Object context)
        {
            return new UnityLog(category, context);
        }

        public static ILog Get(CustomLogCategory customCategory, UnityEngine.Object context = null)
        {
            return new CustomUnityLog(customCategory, context);
        }

#endif
    }
}
