﻿namespace Megapop.Logging
{
    /// <summary>
    /// High level log categories in the game.
    /// </summary>
    public enum LogCategory
    {
        None,

        /// <summary>
        /// Client code that does not fit into any other category.
        /// </summary>
        Client,

        /// <summary>
        /// Debugging code, like tests and debugging panels.
        /// </summary>
        Debug,

        /// <summary>
        /// Editor only code.
        /// </summary>
        Editor,

        /// <summary>
        /// Server code that does not fit into any other category.
        /// </summary>
        Server,

        /// <summary>
        /// Scene loading and transition code.
        /// </summary>
        Scene,

        /// <summary>
        /// Client user interface code.
        /// </summary>
        UI,

        /// <summary>
        /// Build post processing and other Build related code.
        /// </summary>
        Build,

    }
}