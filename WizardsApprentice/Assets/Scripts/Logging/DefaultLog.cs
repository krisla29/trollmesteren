﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Megapop.Logging
{
    /// <inheritdoc />
    /// <summary>
    /// Default log adapter with no context.
    /// Prints to UnityEngine.Debug in Unity, and to Console.Out otherwise.
    /// Optional if RedirectLog is set, all logging will go to this instance instead.
    /// </summary>
    public class DefaultLog : ILog
    {
        public ILog RedirectLog { get; set; }
        protected LogCategory Category { get; }

        protected static readonly HashSet<string> LoggedOnce = new HashSet<string>();

        public DefaultLog(LogCategory category)
        {
            Category = category;
        }

        public virtual void Trace(object message)
        {
            if (Skip(LogLevel.Trace)) return;

            if (RedirectLog != null)
            {
                RedirectLog.Info(Format(message));
                return;
            }

#if UNITY_2018_1_OR_NEWER
            UnityEngine.Debug.Log(Format(message));
#else
            Console.Out.WriteLine(Format(message));
#endif
        }

        public virtual void Info(object message)
        {
            if (Skip(LogLevel.Info)) return;

            if (RedirectLog != null)
            {
                RedirectLog.Info(Format(message));
                return;
            }

#if UNITY_2018_1_OR_NEWER
            UnityEngine.Debug.Log(Format(message));
#else
            Console.Out.WriteLine(Format(message));
#endif
        }

        public virtual void Warn(object message)
        {
            if (Skip(LogLevel.Warn)) return;

            if (RedirectLog != null)
            {
                RedirectLog.Warn(Format(message));
                return;
            }

#if UNITY_2018_1_OR_NEWER
            UnityEngine.Debug.LogWarning(Format(message));
#else
            Console.Out.WriteLine(Format(message));
#endif
        }

        public virtual void WarnOnce(string id, object message)
        {
            if (Skip(LogLevel.Warn) || LoggedOnce.Contains(id)) return;

            LoggedOnce.Add(id);

            if (RedirectLog != null)
            {
                RedirectLog.WarnOnce(id, Format(message));
                return;
            }

#if UNITY_2018_1_OR_NEWER
            UnityEngine.Debug.LogWarning($"{Format(message)} (Logged Once)");
#else
            Console.Out.WriteLine(Format(message));
#endif
        }


        public virtual void Error(object message)
        {
            if (RedirectLog != null)
            {
                RedirectLog.Error(Format(message));
                return;
            }

#if UNITY_2018_1_OR_NEWER
            UnityEngine.Debug.LogWarning(Format(message));
#else
            Console.Error.WriteLine(Format(message));
#endif
        }

        public virtual void Exception(Exception e)
        {
            if (RedirectLog != null)
            {
                RedirectLog.Info(e);
                return;
            }

#if UNITY_2018_1_OR_NEWER
            UnityEngine.Debug.LogException(e);
#else
            Console.Out.WriteLine(e);
#endif
        }

        protected bool Skip(LogLevel logLevel)
        {
            return (Logs.LogLevels.TryGetValue(Category, out var minimumLevel) ? minimumLevel : LogLevel.Info) > logLevel;
        }

        protected string Format(object message)
        {
            var sb = new StringBuilder();

            sb.Append($"{DateTime.Now:HH:mm:ss.fff} [{Category}] ");
            sb.Append(message);

            return sb.ToString();
        }
    }
}