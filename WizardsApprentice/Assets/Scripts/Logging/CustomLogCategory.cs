﻿namespace Megapop.Logging
{
    /// <summary>
    /// Custom log categories that can be completely enabled or disabled locally.
    /// </summary>
    public enum CustomLogCategory
    {
        Christian,
        Fingar,
        Sørensen,
        Winger,
        Erlend,
    }
}