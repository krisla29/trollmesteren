﻿namespace Megapop.Logging
{
    /// <summary>
    /// Log levels.
    /// </summary>
    public enum LogLevel
    {
        Trace,
        Info,
        Warn,
        Error
    }
}