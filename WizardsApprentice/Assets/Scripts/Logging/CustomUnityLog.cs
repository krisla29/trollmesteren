﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Megapop.Logging
{
    /// <summary>
    /// Custom log category, for local testing, and invisible to other developers.
    /// </summary>
    public class CustomUnityLog : ILog
    {
        public static Dictionary<CustomLogCategory, Color> Colors { get; } =
            new Dictionary<CustomLogCategory, Color>
            {
                {CustomLogCategory.Christian, Color.yellow },
                {CustomLogCategory.Sørensen, Color.magenta },
                {CustomLogCategory.Winger, Color.blue},
                {CustomLogCategory.Fingar, Color.green}
            };

        public static HashSet<CustomLogCategory> EnabledCategories { get; } =
            new HashSet<CustomLogCategory>
            {
                CustomLogCategory.Christian //TODO remove after we have Editor support.
            };

        private CustomLogCategory CustomCategory { get; }

        [CanBeNull] private UnityEngine.Object Context { get; }

        public CustomUnityLog(CustomLogCategory customCategory, [CanBeNull] UnityEngine.Object context)
        {
            CustomCategory = customCategory;
            Context = context;
        }

        private bool Skip => !EnabledCategories.Contains(CustomCategory);

        public void Trace(object message)
        {
            if (Skip) return;

            Debug.Log(Format(message), Context);
        }

        public void Info(object message)
        {
            if (Skip) return;

            Debug.Log(Format(message), Context);
        }

        public void Warn(object message)
        {
            if (Skip) return;

            Debug.LogWarning(Format(message), Context);
        }

        public void WarnOnce(string id, object message)
        {
            if (Skip) return;

            Debug.LogWarning(Format(message), Context);
        }

        public void Error(object message)
        {
            Debug.LogError(Format(message), Context);
        }

        public void Exception(Exception e)
        {
            Debug.LogException(e, Context);
        }

        private string Format(object message) =>
            $"<color=#{ColorUtility.ToHtmlStringRGB(Colors[CustomCategory])}>{DateTime.Now:HH:mm:ss.fff} [Custom/{CustomCategory}] {message}</color>";
    }
}