﻿using System;

namespace Megapop.Logging
{
    /// <summary>
    /// Logger adapter interface.
    ///
    /// Implement this interface to implement a custom log destination.
    /// ToString() will be called on non-null message objects.
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Log an Trace level message.
        /// </summary>	
        void Trace(object message);

        /// <summary>
	    /// Log an Info level message.
	    /// </summary>	
        void Info(object message);

        /// <summary>
	    /// Log an Warning level message.
	    /// </summary>	
        void Warn(object message);

        /// <summary>
	    /// Log a Warning level message once.
	    /// Only the first message with a particular id will be logged.
	    /// This is useful for logs that could otherwise be spammy (like getters).
	    /// </summary>	        
        void WarnOnce(string id, object message);

        /// <summary>
	    /// Log an Error level message.
	    /// </summary>	        
        void Error(object message);

        /// <summary>
	    /// Log an Exception level message.
	    /// </summary>	 
        void Exception(Exception e);
    }
}