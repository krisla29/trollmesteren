﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureHuntGame : MonoBehaviour
{
    public InputManager inputManager;
    public Camera cam;
    public RectTransform cross;
    public int boxNumber;
    public Color boxColor;
    public BoxCollider boxTriggerCollider;
    public TreasureHuntGame requiredBox;
    public List<Transform> coins = new List<Transform>();
    public AudioSource successSound;
    [SerializeField] private bool isFinished = false;
    private bool gameFinished = false;
    private Vector3 startPos;
    private Material material;

    public delegate void TreasureHunt(int boxIndex);
    public static event TreasureHunt OnBoxFound;
    public static event TreasureHunt OnTreasureHuntFinished;
    
    void Start()
    {
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        Ray ray = cam.ViewportPointToRay(new Vector3((cross.anchorMax.x + cross.anchorMin.x)/2, (cross.anchorMax.y + cross.anchorMin.y)/2, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            var newPos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            transform.position = newPos;
            startPos = transform.localPosition;
        }
          
        material = transform.GetComponent<MeshRenderer>().material;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            if (inputManager.Action() && !isFinished)
            {
                if (requiredBox.isFinished || boxNumber == 1)
                {
                    var posY = Mathf.Lerp(transform.localPosition.y, transform.localPosition.y + 1, Time.time * 0.00025f);

                    transform.localPosition = new Vector3(transform.localPosition.x, posY, transform.localPosition.z);

                    if (posY >= startPos.y + 1)
                    {
                        foreach (var item in coins)
                        {
                            item.gameObject.SetActive(true);
                        }

                        if(!isFinished)
                        {                           
                            successSound.Play();
                        }

                        isFinished = true;
                        material.color = boxColor;

                        if (OnBoxFound != null)
                        {
                            OnBoxFound(boxNumber);

                            if (boxNumber == 3)
                            {
                                gameFinished = true;
                            }
                        }

                        boxTriggerCollider.size = Vector3.zero;
                    }
                }

            }
        }
    }
}
