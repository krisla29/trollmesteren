﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureHuntGameCanvas : MonoBehaviour
{
    public List<GameObject> crossHairs = new List<GameObject>();

    void Update()
    {
        
    }

    public void OnEnable()
    {
        TreasureHuntGame.OnBoxFound += Check;
    }

    public void OnDisable()
    {
        TreasureHuntGame.OnBoxFound -= Check;
    }
    
    private void Check(int boxNumber)
    {
        //crossHairs[boxNumber -1].gameObject.SetActive(false);     
        Destroy(crossHairs[boxNumber - 1]);
    }
}
