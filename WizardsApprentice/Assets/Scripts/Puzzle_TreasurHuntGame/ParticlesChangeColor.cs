﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesChangeColor : MonoBehaviour
{
    public Color newColor;
    public List<Transform> coins = new List<Transform>();
    public AudioSource taskCompleteSound;
    private bool isFinished;

    void Start()
    {
        
    }


    public void OnEnable()
    {
        TreasureHuntGame.OnBoxFound += ChangeColor;
    }
    public void OnDisable()
    {
        TreasureHuntGame.OnBoxFound -= ChangeColor;
    }

    private void ChangeColor(int boxNumber)
    {
        if (boxNumber == 3)
        {
            var particleColorGrad = transform.GetComponent<ParticleSystem>().colorOverLifetime;
            particleColorGrad.enabled = true;

            Gradient grad = new Gradient();
            grad.SetKeys(new GradientColorKey[] { new GradientColorKey(newColor, 0.0f), new GradientColorKey(newColor, 0.5f), new GradientColorKey(newColor, 1.0f) },
                         new GradientAlphaKey[] { new GradientAlphaKey(0.0f, 0.0f), new GradientAlphaKey(1.0f, 0.5f), new GradientAlphaKey(0.0f, 1.0f) });

            particleColorGrad.color = grad;

            foreach (var item in coins)
            {
                item.gameObject.SetActive(true);
            }

            if(!isFinished)
            {
                taskCompleteSound.Play();
            }

            isFinished = true;
        }
    }
}
