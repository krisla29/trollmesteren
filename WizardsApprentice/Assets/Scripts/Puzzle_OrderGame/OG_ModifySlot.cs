﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OG_ModifySlot : MonoBehaviour
{
    public OrderGame orderGameScript;
    public OrderGame.Modifiers slotModifier;
    public int slotModifierValue;

    private void Awake()
    {
        slotModifier = OrderGame.Modifiers.None;
        slotModifierValue = 0;
    }

    private void OnEnable()
    {
        OG_Interactable.OnPlace += SetValues;
        OG_Interactable.OnRemove += ResetValues;
    }

    private void OnDisable()
    {
        OG_Interactable.OnPlace -= SetValues;
        OG_Interactable.OnRemove -= ResetValues;
    }

    void SetValues(OG_Interactable passedInteractable, Transform passedParent, OrderGame.Modifiers passedModifier, int passedModifierValue)
    {
        if(passedParent == orderGameScript.modifySlot)
        {
            slotModifier = passedModifier;
            slotModifierValue = passedModifierValue;
        }
    }

    void ResetValues(OG_Interactable passedInteractable, Transform passedParent, OrderGame.Modifiers passedModifier, int passedModifierValue)
    {   
        if(passedParent == orderGameScript.modifySlot)
        {
            slotModifier = OrderGame.Modifiers.None;
            slotModifierValue = 0;
        }
    }
}
