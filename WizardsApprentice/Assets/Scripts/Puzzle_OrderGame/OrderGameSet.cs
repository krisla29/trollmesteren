﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderGameSet : MonoBehaviour
{
    public int interval; // step interval between adjacent indexes
    public int startNum; // starting slot/tile
    public bool showStart = true; // if true, instantiate graphical indicator for start
    public int numberOfCorrects; // How many repetitions of current set
    public bool shuffle; // shuffle the order before layout
    public bool shuffleAfterWrong; // shuffle the order when reset
    public List<int> setSlots = new List<int>(); //number and indexes of included slots
    public List<int> unknownValues = new List<int>(); // number and indexes of blank slots, must be member of setSlots. Set shuffle and shuffle after wrong to false

    [System.Serializable]
    public class OG_Set_Modifiers
    {
        public OrderGame.Modifiers modifier = OrderGame.Modifiers.None;
        public int modifierValue;
    }
    [Header("MODIFIERS")]
    public List<OG_Set_Modifiers> setModifiers = new List<OG_Set_Modifiers>(); //Set modifiers and their values
    public int maxCountModifiers; //Maximum modifiers that can be spawned in set --------------------------------TODO: Hide modification tools from scene when maxCount = 0
    public bool allowModifySlot; //Should modifySlot be available  
    public Vector2Int wildValueRange = new Vector2Int(0, 10); //range from wild value will select a number
}
