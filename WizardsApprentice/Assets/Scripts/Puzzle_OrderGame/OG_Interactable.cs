﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OG_Interactable : MonoBehaviour
{
    public OrderGame orderGameScript;
    public Interactable interactable;
    public GameObject activeIndicator;
    private bool lastPressed;
    public float outlinesLerpTime = 0.25f;
    public OrderGame.Modifiers interactableModifier;
    public int interactableModValue = 5;

    //public OrderGame.Modifiers passedModifier = OrderGame.Modifiers.None; //Gets from modifySlot
    //public int passedModValue = 0;//

    [SerializeField]private Rigidbody rb;
    public GameObject deathEffectPrefab;

    public List<SpriteRenderer> sprites = new List<SpriteRenderer>();
    public List<TextMeshPro> textMeshes = new List<TextMeshPro>();
    public List<Sprite> images;
    public AudioSource deathSound;
    public AudioSource attractSound;
    public float spritesStartScale = 0.5f;
    public int value = 5;
    private Vector3 lastVelocity;
    private Transform lastParent;
    private bool isLerping;
    private bool trashOrdered;
    private Vector3 endPos = new Vector3(0, 0, -100);

    public bool isPlaced;

    public delegate void OG_InteractableEvent(); // Sends to OG_SpawnButton
    public static event OG_InteractableEvent OnAddInteractable;
    public static event OG_InteractableEvent OnRemoveInteractable;
    public delegate void OG_InteractableModifyEvent(OG_Interactable thisBehaviour, Transform parent, OrderGame.Modifiers modifier, int modifierValue); //Sends to OrderGame, OG_Interactable, OG_ModifySlot, OrderGameObject
    public static event OG_InteractableModifyEvent OnPlace;
    public static event OG_InteractableModifyEvent OnRemove;

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        OG_Spawner.OnFinishedPassData += InitializeObject;
        //OnPlace += CheckModifications; //Change event, get from this
        //OnRemove += CheckModifications;

    }

    private void OnDisable()
    {
        OG_Spawner.OnFinishedPassData -= InitializeObject;
        //OnPlace -= CheckModifications;
        //OnRemove -= CheckModifications;
        Invoke("DestroyOutlines", 0);
        StopAllCoroutines();
    }

    void CheckModifications(OG_Interactable passedInteractable, Transform passedParent, OrderGame.Modifiers passedModifier, int passedModifierValue)
    {
        if (passedInteractable != this && passedParent == orderGameScript.modifySlot)
        {
            int tempValue = interactableModValue;

            if (passedModifier != OrderGame.Modifiers.None)
                tempValue = orderGameScript.Modify(passedModifier, tempValue, passedModifierValue);

            value = tempValue;

            ChangeText(value);
        }
    }

    void ResetValue()
    {
        value = interactableModValue;
    }

    private void Update()
    {
        if (lastVelocity != Vector3.zero && rb.velocity == Vector3.zero && !isPlaced)
        {
            Transform parent = CheckDistanceToSlots();
            print("Checking distance");

            if (parent != null && !isLerping)
            {               
                transform.parent = parent;
                StartCoroutine(LerpToSlot());

                if (OnPlace != null)
                    OnPlace(this, transform.parent, interactableModifier, interactableModValue);

                activeIndicator.SetActive(true);

                if (transform.localPosition == endPos && transform.localRotation == Quaternion.identity)
                    isPlaced = true;
                else
                    CheckDistanceToSlots();
            }
            else
            {
                ResetTransform();
            }
        }

        if (interactable.isPressed && !lastPressed) // Press
        {
            print("Hello!");
            ResetTransform();
            StopAllCoroutines();
            orderGameScript.currentlyAvailable.Clear();
            orderGameScript.currentOutlines.Clear();
            isLerping = false;
            CheckSlotsAvailable();
            CheckAvailableSlots();
        }
        
        if(!interactable.isPressed && lastPressed) //Release
        {
            CheckReleasedBox();
            print("Goodbye!");
            ClearAvailableSlots();
        }

        if(interactable.isPressed)
        {
            DeactivateRigidbody();
            ResetTransform();
        }
        else
        {
            ActivateRigidBody();
        }

        lastVelocity = rb.velocity;
        lastPressed = interactable.isPressed;
        lastParent = transform.parent;
    }

    private void CheckReleasedBox()
    {
        Transform parent = CheckDistanceToSlots();
        print("Checking distance");

        if (parent != null && !isLerping)
        {
            transform.parent = parent;
            StartCoroutine(LerpToSlot());

            if (OnPlace != null)
                OnPlace(this, transform.parent, interactableModifier, interactableModValue);

            //activeIndicator.SetActive(true);
            //isPlaced = true;
        }
        else
        {
            ResetTransform();
        }
    }

    private void ResetTransform()
    {
        transform.parent = orderGameScript.interactablesParent;

        if (OnRemove != null)
            OnRemove(this, lastParent, OrderGame.Modifiers.None, 0);

        if(isPlaced)
            isPlaced = false;

        //rb.MovePosition(transform.position);
        activeIndicator.SetActive(false);
    }

    void DeactivateRigidbody()
    {
        rb.isKinematic = true;
        rb.useGravity = false;
    }

    void ActivateRigidBody()
    {
        rb.isKinematic = false;
        rb.useGravity = true;
    }

    void CheckAvailableSlots()
    {
        orderGameScript.currentOutlines.Clear();

        if (orderGameScript.currentlyAvailable.Count > 0)
        {
            for (int i = 0; i < orderGameScript.currentlyAvailable.Count; i++)
            {
                var slot = orderGameScript.currentlyAvailable[i];
                var newOutline = slot.GetChild(0);
                orderGameScript.currentOutlines.Add(newOutline);
                newOutline.gameObject.SetActive(true);
                StartCoroutine(ShowAvailable(newOutline));
            }
        }
    }

    void ClearAvailableSlots()
    {
        if (orderGameScript.currentOutlines.Count > 0)
        {
            for (int i = 0; i < orderGameScript.currentOutlines.Count; i++)
            {
                StartCoroutine(HideAvailable(orderGameScript.currentOutlines[i]));
            }
        }

        orderGameScript.currentlyAvailable.Clear();
    }

    private IEnumerator ShowAvailable(Transform newOutline)
    {                       
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / outlinesLerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            newOutline.transform.localScale = Vector3.Lerp(Vector3.zero, (Vector3.one), h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator HideAvailable(Transform outline)
    {       
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / (outlinesLerpTime * 2);
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            outline.transform.localScale = Vector3.Lerp((Vector3.one), Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        if (outline.gameObject.activeSelf)
        {
            outline.gameObject.SetActive(false);
        }

        //if(outline == orderGameScript.currentOutlines)
        
    }

    private void HideOutlines()
    {
        foreach (var item in orderGameScript.currentOutlines)
        {
            if (item)
                item.gameObject.SetActive(false);
        }
    }

    Transform CheckDistanceToSlots()
    {
        var minDist = 1f;
        Transform closestSlot = null;

        for (int i = 0; i < orderGameScript.currentSlots.Count + 2; i++)
        {
            if (i < orderGameScript.currentSlots.Count && orderGameScript.currentSlots[i].gameObject.activeSelf)
            {
                bool checkStartSlot = false;

                if (orderGameScript.currentStartObject)
                {
                    // Checks if not starting slot
                    if (orderGameScript.currentSlots[i] == orderGameScript.currentStartObject.transform.parent) 
                    {
                        checkStartSlot = true;
                    }
                    //Check to prevent modified object goes lower than start...
                    if (orderGameScript.currentGameSet.interval >= 0 &&
                            orderGameScript.Modify(interactableModifier, orderGameScript.orderGameObjects[i].index, interactableModValue)
                            < orderGameScript.sortedIndexes[0])
                    {
                        checkStartSlot = true;
                    }
                    //Check to prevent modified object goes higher than start...
                    if (orderGameScript.currentGameSet.interval < 0 &&
                           orderGameScript.Modify(interactableModifier, orderGameScript.orderGameObjects[i].index, interactableModValue)
                           > orderGameScript.sortedIndexes[0])
                    {
                        checkStartSlot = true;
                    }
                    //check to prevent unknowns to be modified
                    if (orderGameScript.orderGameObjects[i].isUnknown)
                    {
                        checkStartSlot = true;
                    }
                }

                if (!checkStartSlot)
                {
                    var curDist = Vector3.Distance(transform.position, orderGameScript.currentSlots[i].transform.position);
                    if (curDist < minDist)
                    {
                        closestSlot = orderGameScript.currentSlots[i];
                        minDist = curDist;                        
                    }
                }
                
            }
            else if(i == orderGameScript.currentSlots.Count && orderGameScript.modifySlot.gameObject.activeSelf) // Modify Slot
            {
                var curDist = Vector3.Distance(transform.position, orderGameScript.modifySlot.transform.position);
                if (curDist < minDist)
                {
                    closestSlot = orderGameScript.modifySlot;
                    minDist = curDist;
                }
            }
            else if (i == orderGameScript.currentSlots.Count + 1 && orderGameScript.trashSlot.gameObject.activeSelf) //Trash Slot
            {
                var curDist = Vector3.Distance(transform.position, orderGameScript.trashSlot.transform.position);
                if (curDist < minDist)
                {
                    closestSlot = orderGameScript.trashSlot;
                    minDist = curDist;
                    trashOrdered = true;
                }
            }
        }

        return closestSlot;
    }

    private void CheckSlotsAvailable()
    {
        for (int i = 0; i < orderGameScript.currentSlots.Count + 2; i++)
        {
            if (i < orderGameScript.currentSlots.Count && orderGameScript.currentSlots[i].gameObject.activeSelf)
            {
                bool checkStartSlot = false;

                if (orderGameScript.currentStartObject)
                {
                    // Checks if not starting slot
                    if (orderGameScript.currentSlots[i] == orderGameScript.currentStartObject.transform.parent)
                    {
                        checkStartSlot = true;
                    }
                    //Check to prevent modified object goes lower than start...
                    if (orderGameScript.currentGameSet.interval >= 0 &&
                            orderGameScript.Modify(interactableModifier, orderGameScript.orderGameObjects[i].index, interactableModValue)
                            < orderGameScript.sortedIndexes[0])
                    {
                        checkStartSlot = true;
                    }
                    //Check to prevent modified object goes higher than start...
                    if (orderGameScript.currentGameSet.interval < 0 &&
                           orderGameScript.Modify(interactableModifier, orderGameScript.orderGameObjects[i].index, interactableModValue)
                           > orderGameScript.sortedIndexes[0])
                    {
                        checkStartSlot = true;
                    }
                    //check to prevent unknowns to be modified
                    if (orderGameScript.orderGameObjects[i].isUnknown)
                    {
                        checkStartSlot = true;
                    }
                }

                if (!checkStartSlot)
                {
                    if(orderGameScript.currentSlots[i].childCount == 2)
                        orderGameScript.currentlyAvailable.Add(orderGameScript.currentSlots[i]);
                }

            }
            else if (i == orderGameScript.currentSlots.Count && orderGameScript.modifySlot.gameObject.activeSelf) // Modify Slot
            {
                orderGameScript.currentlyAvailable.Add(orderGameScript.modifySlot);
            }
            else if (i == orderGameScript.currentSlots.Count + 1 && orderGameScript.trashSlot.gameObject.activeSelf) //Trash Slot
            {
                orderGameScript.currentlyAvailable.Add(orderGameScript.trashSlot);
            }
        }
    }

    private IEnumerator LerpToSlot()
    {
        //attractSound.Play();
        isLerping = true;
        float t = 0;
        Vector3 startPos = transform.localPosition;
        //Vector3 startPos = transform.position;
        Quaternion startRot = transform.localRotation;

        while(t < 1)
        {
            t += Time.deltaTime / 0.25f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.localPosition = Vector3.Lerp(startPos, endPos, h);
            //rb.position = Vector3.Lerp(startPos, position, h);
            transform.localRotation = Quaternion.Slerp(startRot, Quaternion.identity, h);
            yield return new WaitForEndOfFrame();
        }

        isLerping = false;

        if (trashOrdered)
        {
            var deathEffect = Instantiate(deathEffectPrefab, transform.position, Quaternion.identity, null);
            deathEffect.transform.localScale = Vector3.one * 0.75f;
            Destroy(transform.gameObject);
        }
    }

    private void OnDestroy()
    {       
        if (deathSound)
            deathSound.Play();

        HideOutlines();

        orderGameScript.currentInteractables.Remove(this);

        if (OnRemoveInteractable != null)
            OnRemoveInteractable();

        StopAllCoroutines();
    }

    private void InitializeObject(OG_Interactable passedInteractable)
    {
        if (passedInteractable == this)
        {
            int randNum = Random.Range(0, images.Count);

            //ChangeText(interactableModValue);

            lastVelocity = rb.velocity;

            orderGameScript.currentInteractables.Add(this);

            if (OnAddInteractable != null)
                OnAddInteractable();

            orderGameScript.currentlyAvailable.Clear();
            orderGameScript.currentOutlines.Clear();
        }       
    }

    public void ChangeText(int passedValue) //-----NOT IN USE
    {
        var tempText = "";

        switch (interactableModifier)
        {
            case (OrderGame.Modifiers.Add):
                tempText = "x + " + passedValue.ToString();
                break;
            case (OrderGame.Modifiers.Subtract):
                tempText = "x - " + passedValue.ToString();
                break;
            case (OrderGame.Modifiers.Multiply):
                tempText = "x • " + passedValue.ToString();
                break;
            case (OrderGame.Modifiers.Divide):
                tempText = "x : " + passedValue.ToString();
                break;
            case OrderGame.Modifiers.Wild:
                int rand = Random.Range(0, 3);
                int randVal = Random.Range(orderGameScript.currentGameSet.wildValueRange.x, orderGameScript.currentGameSet.wildValueRange.y);

                if (rand == 0)
                    tempText = "x + " + randVal.ToString();
                else if (rand == 1)
                    tempText = "x - " + randVal.ToString();
                else if (rand == 2)
                    tempText = "x • " + randVal.ToString();
                else
                {
                    print("Wild Modifier Error .... Didn't find options");
                    break;
                }

                break;
            default:
                print("Modify Error .... Fell through Switch");
                break;
        }

        for (int i = 0; i < 6; i++)
        {
            textMeshes[i].text = tempText;
        }
    }
}
