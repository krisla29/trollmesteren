﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OG_Spawner : MonoBehaviour
{
    public OrderGame orderGameScript;
    public GameObject prefabToSpawn;
    public GameObject birthEffect;
    public float waitForNewSpawn = 10f;
    public Transform parent;
    public bool rotateWithParent;
    public AudioSource spawnerBirthSound;
    public AudioSource interactableDeathSound;
    public AudioSource interactableAttractSound;

    public OrderGame.Modifiers spawnerModifier;
    public int spawnerModValue;

    public delegate void OG_SpawnerEvent(OG_Interactable spawnedInteractable); // Sends to OG_Interactable
    public static event OG_SpawnerEvent OnFinishedPassData;

    private void OnDisable()
    {
        spawnerModifier = OrderGame.Modifiers.None;
        spawnerModValue = 0;
    }

    public void SpawnObject()
    {
        if (spawnerBirthSound)
        {
            spawnerBirthSound.Play();
        }

        var newObject = Instantiate(prefabToSpawn);
        var newObjectScript = newObject.GetComponent<OG_Interactable>();
        newObjectScript.orderGameScript = orderGameScript;
        newObjectScript.interactableModifier = spawnerModifier;
        newObjectScript.interactableModValue = spawnerModValue;
        newObjectScript.deathSound = interactableDeathSound;
        newObjectScript.attractSound = interactableAttractSound;

        if (OnFinishedPassData != null)
            OnFinishedPassData(newObjectScript);

        if (parent)
        {
            newObject.transform.parent = parent;
        }
        if (birthEffect)
        {
            Instantiate(birthEffect, transform);
        }

        newObject.transform.position = transform.position;

        if (rotateWithParent)
            newObject.transform.localRotation = Quaternion.identity;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
