﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OrderGameObject : MonoBehaviour
{
    public OrderGame orderGame;
    public GameObject orderGameObject;
    public Transform playerResetPoint;
    public Image image;
    public Collider objectCollider;
    public int index;
    public bool isUnknown;
    public bool isUnknownAlternative;
    public TextMeshProUGUI text;
    public int fontSize = 100; 
    public bool isPressed;
    public bool isCorrect;
    public bool isLocked;
    public float timer;
    public Vector3 startPos;
    public Color startColor;

    public MeshRenderer meshRenderer;

    public OrderGame.Modifiers objectModifier = OrderGame.Modifiers.None;
    public int objModifiervalue = 0;
    
    public int newIndex;

    public delegate void OrderObjectEvent(); // Sends to OrderGame
    public static event OrderObjectEvent OnRightObject;   
    public static event OrderObjectEvent OnWrongObject;
    public static event OrderObjectEvent OnTaskCompletete;
    public static event OrderObjectEvent OnRightObjectTutorial;
    public static event OrderObjectEvent OnWrongObjectTutorial;
    public static event OrderObjectEvent OnTaskCompleteteTutorial;
    //public static event OrderObjectEvent OnUnknownPressed;
    public delegate void OrderObjectModEvent(OrderGameObject thisScript, OrderGame.Modifiers passedModifier, int passedModValue, int oldIndex, int newIndex, OG_Interactable passedInteractable); // Sends to OrderGame
    public static event OrderObjectModEvent OnModIndex;
    public delegate void OrderObjectFirstEvent(Transform firstSlot); // Sends to OrderGame
    public static event OrderObjectFirstEvent OnFirstCorrectObject;
    public delegate void OrderObjectUnknownEvent(int passedIndex); // Sends to OrderGame
    public static event OrderObjectUnknownEvent OnUnknownPressed;
    public delegate void OrderObjectUnknownAltEvent(OrderGameObject unknownAltObject); // Sends to OrderGame
    public static event OrderObjectUnknownAltEvent OnUnknownAltPressed;

    private void OnEnable()
    {
        newIndex = index;
        OrderGame.OnShowObject += CheckShowObject;
        OrderGame.OnHideObject += CheckShowObject;
        objectCollider.enabled = true;
    }

    private void OnDisable()
    {
        OrderGame.OnShowObject -= CheckShowObject;
        OrderGame.OnHideObject -= CheckShowObject;
        StopAllCoroutines();
    }

    void CheckShowObject(Transform passedObject, bool showObject)
    {
        if(passedObject == transform)
        {
            if(showObject)
            {
                if(passedObject.gameObject.activeSelf)
                    StartCoroutine(TaskStart());
            }
            else
            {
                if (passedObject.gameObject.activeSelf)
                    StartCoroutine(TaskComplete());
            }
        }
    }

    void CheckModifications(OG_Interactable passedInteractable, Transform passedParent, OrderGame.Modifiers passedModifier, int passedModifierValue) //Not in use....
    {
        if (passedParent == transform.parent)
        {
            int tempIndex = index;

            if (passedModifier != OrderGame.Modifiers.None)
                tempIndex = orderGame.Modify(passedModifier, index, passedModifierValue);

            newIndex = tempIndex;

            if (OnModIndex != null)
                OnModIndex(this, passedModifier, passedModifierValue, index, newIndex, passedInteractable);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.CompareTag("Player") && !isPressed && !isCorrect)
        if (other.gameObject.CompareTag("Player") && !isPressed)
        {
            if(orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = orderGame.agentStepOffset;
            }

            timer = Time.time;
            isPressed = false;
            StartCoroutine("Timer");
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = 0;
            }
            StopCoroutine("Timer");
            isPressed = false;
        }
    }

    // -------------------Edit:------------------From here, changed 'index' to 'newIndex' 

    private IEnumerator Timer()
    {
        print(orderGame.stayTime);

        yield return new WaitForSeconds(orderGame.stayTime);
        
        if(CheckConditions())
        {
            if (!isUnknown)
            {
                isCorrect = true;
                StartCoroutine(Correct());
                orderGame.rightCounter += 1;
            }
            else
            {
                if (OnUnknownPressed != null)
                    OnUnknownPressed(newIndex);

                StartCoroutine(Unknown());
            }
        }
        else
        {
            isCorrect = false;
            StartCoroutine(Wrong());
        }       
        
        yield break;   
    }

    private bool CheckConditions()
    {
        bool check = true;

        for (int i = 0; i < orderGame.sortedIndexes.Count; i++)
        {
            print("sortedIndex number " + i + " = " + orderGame.sortedIndexes[i]);
        }

        if ((orderGame.sortedIndexes.IndexOf(newIndex) == (orderGame.rightCounter) && orderGame.rightCounter > 0)
                        || (newIndex == orderGame.lastIndex)   // if index is identical to last index     
                        || (newIndex == orderGame.sortedIndexes[0] && orderGame.rightCounter == 0))
        {
            check = true;
        }
        else
        {
            check = false;
        }
              
        return check;
    }
   
    private IEnumerator Correct()
    {
        orderGame.stonePush.Play();

        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / orderGame.lerpTime;
            transform.position = Vector3.Lerp(startPos, new Vector3(startPos.x, startPos.y - 0.1f, startPos.z), time);

            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = Mathf.Lerp(orderGame.agentStepOffset, 0, time);
            }

            image.color = Color.Lerp(startColor, new Vector4(startColor.r, startColor.g, startColor.b, 0), time);
            yield return new WaitForEndOfFrame();
        }

        orderGame.correct.Play();

        orderGame.lastIndex = newIndex;
        //orderGame.lastIndex = orderGame.sortedIndexes.IndexOf(newIndex);
        //NB ---- New Conditions
        //if(newIndex == orderGame.currentGameSet.startNum + ((orderGame.indexes.Count - 1) * orderGame.currentGameSet.interval))
        if (newIndex == orderGame.sortedIndexes[orderGame.sortedIndexes.Count - 1] && orderGame.rightCounter == orderGame.currentSlots.Count)
        {
            orderGame.TransformPlayer();
            orderGame.complete.Play();

            if (OnTaskCompletete != null)
                OnTaskCompletete();
        }
        else
        {
            
        }
       
        if (newIndex == orderGame.sortedIndexes[0])
        {
            if (OnFirstCorrectObject != null)
                OnFirstCorrectObject(transform.parent);
        }

        if (isUnknownAlternative)
        {
            if (OnUnknownAltPressed != null)
                OnUnknownAltPressed(this);

            for (int i = 0; i < orderGame.orderGameObjects.Count; i++)
            {
                if (orderGame.orderGameObjects[i].index == index)
                    orderGame.orderGameObjects[i].text.text = index.ToString();
            }

            transform.gameObject.SetActive(false);
        }

        objectCollider.enabled = false;

        if (OnRightObject != null)
            OnRightObject();

        yield return null;
    }
    private IEnumerator Wrong()
    {
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / orderGame.lerpTime;
            image.color = Color.Lerp(startColor, Color.red, time);
            yield return new WaitForEndOfFrame();
        }

        orderGame.wrong.Play();

        orderGame.TransformPlayer();
        
        if (OnWrongObject != null)
            OnWrongObject();

        if (isUnknownAlternative)
        {
            if (OnUnknownAltPressed != null)
                OnUnknownAltPressed(this);

            transform.gameObject.SetActive(false);
        }

        objectCollider.enabled = false;

        yield return null;
    }

    private IEnumerator Unknown()
    {
        orderGame.stonePush.Play();

        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / orderGame.lerpTime;
            transform.position = Vector3.Lerp(startPos, new Vector3(startPos.x, startPos.y - 0.1f, startPos.z), time);

            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = Mathf.Lerp(orderGame.agentStepOffset, 0, time);
            }

            image.color = Color.Lerp(startColor, new Vector4(startColor.r, startColor.g, startColor.b, 0), time);
            yield return new WaitForEndOfFrame();
        }

        objectCollider.enabled = false;
    }

    private IEnumerator TaskComplete()
    {
        orderGame.stonePush.Play();
        var nStartPos = transform.position;
        var nStartColor = image.color;

        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / orderGame.lerpTime;
            transform.position = Vector3.Lerp(nStartPos, new Vector3(startPos.x, startPos.y - 0.1f, startPos.z), time);
            /*
            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = Mathf.Lerp(orderGame.agentStepOffset, 0, time);
            }
            */
            image.color = Color.Lerp(nStartColor, new Vector4(startColor.r, startColor.g, startColor.b, 0), time);
            yield return new WaitForEndOfFrame();
        }

        objectCollider.enabled = false;
        isCorrect = false;
        transform.gameObject.SetActive(false);
    }

    private IEnumerator TaskStart()
    {
        orderGame.stonePush.Play();

        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / orderGame.lerpTime;
            transform.position = Vector3.Lerp(new Vector3(startPos.x, startPos.y - 0.1f, startPos.z), startPos, time);
            /*
            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = Mathf.Lerp(orderGame.agentStepOffset, 0, time);
            }
            */
            image.color = Color.Lerp(new Vector4(startColor.r, startColor.g, startColor.b, 0), startColor, time);
            yield return new WaitForEndOfFrame();
        }

        timer = 0;
        objectCollider.enabled = true;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }    
}
