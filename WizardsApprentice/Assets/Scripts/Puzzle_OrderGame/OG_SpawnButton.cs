﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OG_SpawnButton : MonoBehaviour
{
    public OrderGame orderGame;
    public Transform playerResetPoint;
    public int maxCount = 3;
    private float timer;
    private bool isPressed;
    private bool isAvailable;

    public MeshRenderer meshRenderer;
    public Material activeMaterial;
    public Material deactiveMaterial;
    public Transform glowObject;

    public Image image;
    private Color startColor;
    private Vector3 startPos;

    private void Awake()
    {
        startPos = transform.position;
        startColor = image.color;
    }

    private void OnEnable()
    {
        OG_Interactable.OnAddInteractable += CheckButtonState;
        OG_Interactable.OnRemoveInteractable += CheckButtonState;
        OrderGameObject.OnRightObject += CheckButtonState;
        OrderGame.OnNewSetCreated += CheckButtonState;
    }

    private void OnDisable()
    {
        OG_Interactable.OnAddInteractable -= CheckButtonState;
        OG_Interactable.OnRemoveInteractable -= CheckButtonState;
        OrderGameObject.OnRightObject -= CheckButtonState;
        OrderGame.OnNewSetCreated -= CheckButtonState;

        startPos = transform.position;
        startColor = image.color;
        StopAllCoroutines();
    }

    void CheckButtonState()
    {
        if(orderGame.currentInteractables.Count < orderGame.currentGameSet.maxCountModifiers && orderGame.rightCounter == 0)
        {
            meshRenderer.material = activeMaterial;
            glowObject.gameObject.SetActive(true);
        }
        else
        {
            meshRenderer.material = deactiveMaterial;
            glowObject.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !isPressed && orderGame.currentInteractables.Count < orderGame.currentGameSet.maxCountModifiers)
        {
            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = orderGame.agentStepOffset;
            }
            timer = Time.time;
            isPressed = false;
            SetSpawnerValues();
            StartCoroutine("Timer");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = 0;
            }
            StopCoroutine("Timer");
            isPressed = false;
            StartCoroutine(ResetButton());
        }
    }

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(orderGame.stayTime);

        StartCoroutine("Activate");
    }

    private void SetSpawnerValues()
    {
        if (orderGame.currentGameSet.setModifiers.Count > 0)
        {
            var randSet = Random.Range(0, orderGame.currentGameSet.setModifiers.Count);

            orderGame.spawner.spawnerModifier = orderGame.currentGameSet.setModifiers[randSet].modifier;
            orderGame.spawner.spawnerModValue = orderGame.currentGameSet.setModifiers[randSet].modifierValue;
        }
        else
        {
            orderGame.spawner.spawnerModifier = OrderGame.Modifiers.None;
            orderGame.spawner.spawnerModValue = 0;
        }
    }

    private IEnumerator Activate()
    {
        orderGame.stonePush.Play();

        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / orderGame.lerpTime;
            transform.position = Vector3.Lerp(startPos, new Vector3(startPos.x, startPos.y - 0.1f, startPos.z), time);

            if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
            {
                orderGame.playerAgent.baseOffset = Mathf.Lerp(orderGame.agentStepOffset, 0, time);
            }

            image.color = Color.Lerp(startColor, new Vector4(startColor.r, startColor.g, startColor.b, 0), time);
            yield return new WaitForEndOfFrame();
        }

        orderGame.spawner.SpawnObject();
        MovePlayer();
    }

    private IEnumerator ResetButton()
    {
        //orderGame.stonePush.Play();

        float time = 0;
        Vector3 curPos = transform.position;
        Color curColor = image.color;

        while (time < 1)
        {
            time += Time.deltaTime / (orderGame.lerpTime / 2);
            transform.position = Vector3.Lerp(curPos, startPos, time);
            image.color = Color.Lerp(curColor, startColor, time);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator RotatePlayer()
    {
        if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
        {
            yield return new WaitForSeconds(0.6f);

            Vector3 startRot = orderGame.player.localEulerAngles;
            Vector3 newRot = new Vector3(startRot.x, 0, startRot.x);
            float time = 0;

            while (time < 1)
            {
                time += Time.deltaTime / (orderGame.lerpTime / 3);

                orderGame.playerAgent.transform.localEulerAngles = Vector3.Lerp(startRot, newRot, time);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    void MovePlayer()
    {
        if (orderGame.playerAgent != null && orderGame.playerAgent.enabled)
        {
            orderGame.playerAgent.SetDestination(playerResetPoint.position);
            StartCoroutine(RotatePlayer());
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
