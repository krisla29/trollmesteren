﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrderGameKillZone : MonoBehaviour
{
    public OrderGame orderGameScript;
    public bool invert = true;
    public GameObject deathEffectPrefab;

    private void OnTriggerExit(Collider other)
    {
        if (invert)
        {
            var interactable = other.gameObject.GetComponent<OG_Interactable>();
            //if (other.gameObject.CompareTag("Interactable"))
            if (interactable != null)
            {
                //if (other.transform.parent.CompareTag("Interactable"))
                //{
                    //DeathActions(other.transform.parent);
                //}
                //else
                //{
                    DeathActions(other.transform);
                //}
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!invert)
        {
            var interactable = other.transform.parent.gameObject.GetComponent<OG_Interactable>();

            //if (other.gameObject.CompareTag("Interactable"))
            if(interactable != null)
            {

                //if (other.transform.parent.CompareTag("Interactable"))
                //{
                    DeathActions(other.transform.parent);
                //}
                //else
                //{
                    //DeathActions(other.transform);
                //}
            }
        }
    }

    void DeathActions(Transform passedTransform)
    {
        var interactableScript = passedTransform.gameObject.GetComponent<OG_Interactable>();
        orderGameScript.spawner.spawnerModifier = interactableScript.interactableModifier;
        orderGameScript.spawner.spawnerModValue = interactableScript.interactableModValue;

        if(orderGameScript.currentInteractables.Count <= orderGameScript.currentGameSet.maxCountModifiers)
            Invoke("SpawnNew", 1f);

        Die(passedTransform);
    }

    void SpawnNew()
    {
        orderGameScript.spawner.SpawnObject();
    }
   
    void Die(Transform passedTransform)
    {
        var deathEffect = Instantiate(deathEffectPrefab, passedTransform.position, Quaternion.identity, null);
        deathEffect.transform.localScale = Vector3.one * 0.75f;
        Destroy(passedTransform.gameObject);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }
}
