﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;

public class OrderGame : MonoBehaviour, IDifficulty, ISetDifficulty
{
    public GameManager gameManager;
    public Transform gameTransform;
    public PlayerStats.Difficulty orderGameDifficulty = PlayerStats.Difficulty.Normal;
    private int correctStreakCounter;
    private int wrongStreakCounter;
    public int numberOfCorrects = 5;
    public int numberOfWrongs = 3;
    public List<Transform> slots = new List<Transform>();
    public List<OrderGameObject> orderGameObjects = new List<OrderGameObject>();
    public List<Transform> unknownSlots = new List<Transform>();
    public List<OrderGameObject> unknownObjects = new List<OrderGameObject>();
    public Material unknownAltMat;
    public Transform interactablesParent;
    public Transform modifySlot;
    public Transform trashSlot;
    public Transform spawnButton;
    public Transform landingArea;
    public Transform spawnGemBox;
    //public TextMeshProUGUI levelText;
    public List<OG_Spawner> interactableSpawners = new List<OG_Spawner>();
    public OG_Spawner spawner;
    public List<OG_Interactable> currentInteractables = new List<OG_Interactable>();
    public List<int> indexes = new List<int>();
    public List<int> sortedIndexes = new List<int>();
    public List<int> unknownIndexes = new List<int>();
    public List<Color> colors = new List<Color>();
    public Color unknownColor = Color.yellow;
    public int lastIndex;
    public GameObject orderObjectPrefab;
    public GameObject startIndicatorPrefab;
    public GameObject outlineAvailablePrefab;
    public GameObject modifierDeathEffectPrefab;
    public GameObject gem;
    //public Trophy trophy;
    public RectTransform setCountPanel;
    public Image setCountImage;
    public TextMeshProUGUI setCountPanelText;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.OrderGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;
    public GameObject winSetEffect;
    public RectTransform rewardsPanel;
    public Image gemRewardBG;
    public int bonusReward = 3;
    public float rewardsPanelShowTime = 3f;
    public TextMeshProUGUI gemRewardText;
    public TextMeshProUGUI bonusBoxCountText;
    public Transform gemGraphic;
    public Transform boxGraphic;
    public RectTransform winGamePanel;
    public Color defaultColor = Color.gray;
    public Color firstTileColor = Color.yellow;
    public Color pressedColor = Color.black;
    public Transform playerResetPoint;
    public float stayTime = 1f;
    public float lerpTime = 0.5f;
    public AudioSource stonePush;
    public AudioSource correct;
    public AudioSource wrong;
    public AudioSource complete;
    public AudioSource countGemsSound;
    public AudioSource winGameSound;
    public AudioSource winGameSound2;

    public Transform player;
    public NavMeshAgent playerAgent;
    public float agentStepOffset = 0.15f;

    public enum Modifiers { Add, Subtract, Divide, Multiply, Wild, None }
    public List<OrderGameSet> gameSets = new List<OrderGameSet>();
    public List<OrderGameSet> gameSetsEasy = new List<OrderGameSet>();
    public List<OrderGameSet> gameSetsHard = new List<OrderGameSet>();
    public List<OrderGameSet> totalCompletedSets = new List<OrderGameSet>();

    public OrderGameSet currentGameSet;
    public List<Transform> currentSlots;
    public Transform currentStartIndicator;
    public OrderGameObject currentStartObject;

    public int gameSetCounter;
    public int levelCounter;
    public int setsInLevel = 3;
    public int correctCounter;
    public int rightCounter;
    public int bonusCounter;
    private bool hasCheckedModifications;

    public List<Transform> currentlyAvailable = new List<Transform>();
    public List<Transform> currentOutlines = new List<Transform>();

    public bool isComplete;
    public bool hasWonGame;
    public int completedSets = 0;

    public List<Transform> ObjectsToActivateOnComplete = new List<Transform>();
    public List<Transform> ObjectsToSleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    private int currentReward = 1;

    public delegate void OrderGameEvent(); // Sends to PlayerStats
    public static event OrderGameEvent OnOrderGameEnable;
    public static event OrderGameEvent OnOrderGameDisable;
    public static event OrderGameEvent OnOrderGameCorrect;
    public static event OrderGameEvent OnOrderGameComplete;
    public static event OrderGameEvent OnFirstObjectPressed; // Sends to OrderGameObject
    public static event OrderGameEvent OnNewSetCreated; // Sends to SpawnButton ... err, no.. um, yes still
    public delegate void OrderGameActivationEvent(Transform orderGameObjectT, bool showObject); // Sends to OrderGameObject
    public static event OrderGameActivationEvent OnHideObject;
    public static event OrderGameActivationEvent OnShowObject;

    void Awake()
    {
        // INITIALIZE:
        for (int i = 0; i < slots.Count; i++)
        {
            var newObject = Instantiate(orderObjectPrefab, slots[i]);
            newObject.name = i.ToString();
            var newObjectScript = newObject.GetComponent<OrderGameObject>();
            newObjectScript.orderGame = transform.GetComponent<OrderGame>();
            orderGameObjects.Add(newObjectScript);
            newObjectScript.index = i;
            indexes.Add(i);
            newObjectScript.text.text = i.ToString();
            newObjectScript.startPos = newObject.transform.position;
            newObjectScript.startColor = newObjectScript.image.color;
        }

        for (int i = 0; i < unknownSlots.Count; i++)
        {
            var newObject = Instantiate(orderObjectPrefab, unknownSlots[i]);
            newObject.name = "X - " + i.ToString();
            var newObjectScript = newObject.GetComponent<OrderGameObject>();
            newObjectScript.orderGame = transform.GetComponent<OrderGame>();
            unknownObjects.Add(newObjectScript);
            newObjectScript.index = i;
            newObjectScript.isUnknownAlternative = true;
            unknownIndexes.Add(i);
            newObjectScript.text.text = i.ToString();
            newObjectScript.startPos = newObject.transform.position;
            newObjectScript.startColor = unknownColor;
            newObjectScript.meshRenderer.material = unknownAltMat;
        }

        unknownSlots[0].transform.parent.gameObject.SetActive(false);

        lastIndex = indexes.Count - 1;
        gameSetCounter = 0;
        levelCounter = 0;

        boxGraphic.gameObject.SetActive(false);
        boxGraphic.transform.localScale = Vector3.zero;
        gemRewardText.text = "";
        bonusBoxCountText.text = "";
        rewardsPanel.gameObject.SetActive(false);
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerAgent = player.GetComponent<NavMeshAgent>();

        NewSet(true);
    }

    private void Shuffle()
    {
        for (int i = 0; i < indexes.Count; i++)
        {
            int tempIndex = slots.IndexOf(currentSlots[i]);
            orderGameObjects[tempIndex].gameObject.name = indexes[i].ToString();

            if (!currentGameSet.unknownValues.Contains(indexes[i]))
                orderGameObjects[tempIndex].text.text = indexes[i].ToString();
            else
            {
                orderGameObjects[tempIndex].text.text = "?";
                orderGameObjects[tempIndex].isUnknown = true;
            }

            orderGameObjects[tempIndex].index = indexes[i];
            orderGameObjects[tempIndex].newIndex = indexes[i];
            orderGameObjects[tempIndex].timer = 0;
            orderGameObjects[tempIndex].transform.position = orderGameObjects[tempIndex].startPos;
            orderGameObjects[tempIndex].image.color = orderGameObjects[tempIndex].startColor;
            orderGameObjects[tempIndex].isPressed = false;
            orderGameObjects[tempIndex].isCorrect = false;

            if (indexes[i] == currentGameSet.startNum && currentGameSet.showStart)
            {
                if (currentStartIndicator)
                {
                    Destroy(currentStartIndicator.gameObject);
                }

                currentStartObject = orderGameObjects[tempIndex];
                var slotTransform = orderGameObjects[tempIndex].transform.parent;
                var startObject = Instantiate(startIndicatorPrefab, orderGameObjects[tempIndex].startPos, slotTransform.rotation, slotTransform);
                currentStartIndicator = startObject.transform;
            }
        }
    }

    private void ShuffleUnknowns(int correctAnswer)
    {
        int randNum = Random.Range(0, 5);
        List<int> wrongVals = new List<int>();

        for (int i = 0; i < unknownObjects.Count; i++)
        {
            if(i != randNum)
            {
                int randVal = 0;

                randVal = GenerateRandVal(correctAnswer);

                while (randVal == correctAnswer || wrongVals.Contains(randVal) || randVal == lastIndex) //BUG!!!!!!!!!!!!!!!! 
                {                    
                    randVal = GenerateRandVal(correctAnswer);                    
                }

                wrongVals.Add(randVal);
                unknownObjects[i].index = randVal;
                unknownObjects[i].text.text = randVal.ToString();
            }
            else
            {
                unknownObjects[i].index = correctAnswer;
                unknownObjects[i].text.text = correctAnswer.ToString();
                //unknownObjects[i].isCorrect = true;
            }
        }

        wrongVals.Clear();
    }

    int GenerateRandVal(int correctAnswer)
    {
        int extentionValue = 6;

        int returnVal = Random.Range(correctAnswer - extentionValue, correctAnswer + extentionValue);
        //int extensionValue = 3;
        /*
        if (currentGameSet.interval > 0)
        {
            
            returnVal = Random.Range(lastIndex - extensionValue, lastIndex + currentGameSet.interval + extensionValue);
            
            if(currentGameSet.interval < 5) // Checks if there are enough options
            {
                returnVal = Random.Range(lastIndex, lastIndex + currentGameSet.interval + 5);
            }
            
        }
        else
        {
            returnVal = Random.Range(lastIndex + currentGameSet.interval + extensionValue, lastIndex - extensionValue);
            
            if (currentGameSet.interval > -5) // Checks if there are enough options
            {
                returnVal = Random.Range(lastIndex + currentGameSet.interval, lastIndex - 5);
            }
            
        }
        */
        return returnVal;
    }

    void ResetObjects()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            orderGameObjects[i].gameObject.name = i.ToString();
            orderGameObjects[i].text.text = i.ToString();
            orderGameObjects[i].index = i;
            orderGameObjects[i].timer = 0;
            orderGameObjects[i].transform.position = orderGameObjects[i].startPos;
            orderGameObjects[i].image.color = orderGameObjects[i].startColor;
            orderGameObjects[i].isPressed = false;
            orderGameObjects[i].isCorrect = false;

            orderGameObjects[i].objectModifier = OrderGame.Modifiers.None;
            orderGameObjects[i].objModifiervalue = 0;
            
            orderGameObjects[i].newIndex = orderGameObjects[i].index;
        }

        //Resources.UnloadUnusedAssets();
    }

    void ResetUnknownObjects()
    {
        for (int i = 0; i < unknownObjects.Count; i++)
        {
            unknownObjects[i].gameObject.name = i.ToString();
            unknownObjects[i].text.text = "X - " + i.ToString();
            unknownObjects[i].index = i;
            unknownObjects[i].timer = 0;
            unknownObjects[i].transform.position = unknownObjects[i].startPos;
            unknownObjects[i].image.color = unknownColor;
            unknownObjects[i].isPressed = false;
            unknownObjects[i].isCorrect = false;

            unknownObjects[i].objectModifier = OrderGame.Modifiers.None;
            unknownObjects[i].objModifiervalue = 0;

            unknownObjects[i].newIndex = unknownObjects[i].index;
        }
    }

    public void TransformPlayer()
    {
        if (playerAgent != null && playerAgent.enabled)
        {
            playerAgent.SetDestination(playerResetPoint.position);
        }
        player.transform.position = playerResetPoint.position;
    }

    public void NewSet(bool isNewLevel)
    {
        //print("hello");
        ResetObjects();
        ResetUnknownObjects();
        correctCounter = 0;
        bonusCounter = 0;

        SetGameSetWithDifficulty(gameSetCounter);
        gameManager.playerStats.OrderGameSetCount = gameSetCounter;
        //currentGameSet = gameSets[gameSetCounter];

        indexes.Clear();
        sortedIndexes.Clear();

        boxGraphic.gameObject.SetActive(false);
        boxGraphic.transform.localScale = Vector3.zero;
        //levelText.text = "";
        gemRewardText.text = "";
        bonusBoxCountText.text = "";       
        rewardsPanel.gameObject.SetActive(false);

        //if (!hasWonGame && gameSetCounter > completedSets)
            //completedSets = gameSetCounter;

        //if (isNewLevel)
        StopCoroutine(ShowSetCountPanel());
        //levelText.transform.parent.gameObject.SetActive(false);
        StartCoroutine(ShowSetCountPanel());

        foreach (var item in orderGameObjects)
        {
            item.isUnknown = false;
            item.gameObject.SetActive(false);
        }

        foreach (var item in currentInteractables)
        {
            Destroy(item.gameObject);
        }

        currentInteractables.Clear();
        //-------------NEW----------------------------------------
        var tempList = new List<int>();

        for (int i = 0; i < currentGameSet.setSlots.Count; i++)
        {
            tempList.Add(currentGameSet.setSlots[i]);
        }

        tempList.Sort();

        currentSlots = new List<Transform>();

        for (int i = 0; i < tempList.Count; i++)
        {
            currentSlots.Add(slots[tempList[i]]);           
        }       

        for (int i = 0; i < slots.Count; i++)
        {
            if (tempList.Contains(i))
            {
                slots[i].gameObject.SetActive(true);
                orderGameObjects[i].gameObject.SetActive(true);

                if (OnShowObject != null)
                    OnShowObject(orderGameObjects[i].transform, true);
            }
            else
            {               
                slots[i].gameObject.SetActive(false);
                orderGameObjects[i].gameObject.SetActive(false);
            }            

            orderGameObjects[i].text.fontSize = orderGameObjects[i].fontSize;
        }
        //------------------------------------------------------------
        for (int i = 0; i < tempList.Count; i++)
        {
            if (i == 0)
            {
                indexes.Add(currentGameSet.startNum);
                sortedIndexes.Add(currentGameSet.startNum);
            }
            else
            {
                indexes.Add(currentGameSet.startNum + (currentGameSet.interval * i));
                sortedIndexes.Add(currentGameSet.startNum + (currentGameSet.interval * i));
            }            
        }

        if (currentGameSet.setModifiers.Count > 0)
            ModificationTools(true);
        else
            ModificationTools(false);

        modifySlot.gameObject.SetActive(currentGameSet.allowModifySlot);

        lastIndex = currentGameSet.startNum;
        rightCounter = 0;
        
        if (currentGameSet.shuffle)
            HelperFunctions.ShuffleList(indexes);
        
        Shuffle();

        if (OnNewSetCreated != null)
            OnNewSetCreated();

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        gameManager.playerStats.SaveGame();
    }

    public void NewTask()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;

        if (currentStartIndicator)
            Destroy(currentStartIndicator.gameObject);

        CorrectTicker();

        if (!isComplete)
        {            
            correctCounter += 1;

            if (correctCounter == currentGameSet.numberOfCorrects)
            {
                if (gameSetCounter < gameSets.Count - 1)
                {
                    gameSetCounter += 1;
                    //gameManager.playerStats.OrderGameSetCount = gameSetCounter;

                    if (gameSetCounter % setsInLevel == 0)
                    {
                        levelCounter++;
                        StartCoroutine(NextLevel());
                    }
                    else
                    {
                        StartCoroutine(NextSet());
                    }
                }
                else
                {
                    isComplete = true;                    
                    StartCoroutine(WinGame());
                }
            }
            else
            {
                lastIndex = currentGameSet.startNum;
                rightCounter = 0;

                if (currentGameSet.shuffle)
                    HelperFunctions.ShuffleList(indexes);

                Shuffle();

                gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
                gameManager.hudCanvas.exitButton.gameObject.SetActive(true);               
            }
        }
        else
        {
            lastIndex = currentGameSet.startNum;
            rightCounter = 0;

            HelperFunctions.ShuffleList(indexes);

            Shuffle();

            gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
            gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        }
    }

    public void ModifySortedList()
    {

    }

    void HideAllObjects()
    {
        foreach (var item in orderGameObjects)
        {
            //item.gameObject.SetActive(false);
            if (OnHideObject != null)
                OnHideObject(item.transform, false);
        }

        unknownSlots[0].transform.parent.gameObject.SetActive(false);

        if (currentStartIndicator)
            Destroy(currentStartIndicator.gameObject);
    }

    public void ResetLevel()
    {
        WrongTicker();

        gameSetCounter = levelCounter * setsInLevel;
        //gameManager.playerStats.OrderGameSetCount = gameSetCounter;
        StartCoroutine(ResetLevelRoutine());               
    }

    void FinalizeTask(Transform passedStartSlot) // Operations after first tile is pressed
    {
        if (OnFirstObjectPressed != null)
            OnFirstObjectPressed();

        if (currentStartIndicator)
            Destroy(currentStartIndicator.gameObject);

        for (int i = 0; i < currentSlots.Count; i++)
        {
            if(currentSlots[i].childCount > 2 && currentSlots[i] != passedStartSlot)
            {
                var modifier = currentSlots[i].GetChild(2).GetComponent<OG_Interactable>();
                var orderObject = currentSlots[i].GetChild(1).GetComponent<OrderGameObject>();

                if (modifier)
                {
                    orderObject.newIndex = Modify(modifier.interactableModifier, orderObject.index, modifier.interactableModValue);
                    CheckModdedIndex(orderObject, modifier.interactableModifier, modifier.interactableModValue, orderObject.index, orderObject.newIndex, modifier);
                    bonusCounter++;
                }
            }
        }

        if (currentInteractables.Count > 0)
        {
            foreach (var item in currentInteractables)
            {
                var deathEffect = Instantiate(modifierDeathEffectPrefab, item.transform.position, Quaternion.identity, item.transform.parent.parent.parent);
                Destroy(item.gameObject);
            }
        }

        ModificationTools(false);
    }

    void CheckModdedIndex(OrderGameObject passedObject, Modifiers passedModifier, int passedModValue, int passedOldIndex, int passedNewIndex, OG_Interactable passedInteractable)
    {
        var oldIndex = sortedIndexes.IndexOf(passedOldIndex);
        var newIndex = sortedIndexes.IndexOf(passedNewIndex);
        
        for (int i = 0; i < sortedIndexes.Count; i++)
        {
            if(i == oldIndex)
            {
                sortedIndexes[i] = passedNewIndex;
            }
        }

        sortedIndexes.Sort();

        newIndex = sortedIndexes.IndexOf(passedNewIndex);
        
        if (passedModifier != Modifiers.None)
        {
            ChangeText(passedObject, passedModifier, passedOldIndex, passedModValue, true);
        }
        else
        {
            ChangeText(passedObject, passedModifier, passedOldIndex, passedModValue, false);
        }        
    }

    void ShowAlternatives(int passedIndex)
    {
        if (currentStartIndicator)
            Destroy(currentStartIndicator.gameObject);

        ShuffleUnknowns(passedIndex);

        unknownSlots[0].transform.parent.gameObject.SetActive(true);

        for (int i = 0; i < unknownObjects.Count; i++)
        {
            unknownObjects[i].gameObject.SetActive(true);
            unknownObjects[i].objectCollider.enabled = true;
            unknownObjects[i].timer = 0;

            if (OnShowObject != null)
                OnShowObject(unknownObjects[i].transform, true);


        }

        StartCoroutine(ShowUnknownOutlines(true, lerpTime));
    }
    
    void HideAlternatives(OrderGameObject passedUnknownAltObject)
    {
        
        if(passedUnknownAltObject != null)
        {
            for (int i = 0; i < unknownObjects.Count; i++)
            {
                if (unknownObjects[i] != passedUnknownAltObject)
                    OnHideObject(unknownObjects[i].transform, false);
            }
        }
        else
        {       
            foreach (var item in unknownObjects)
            {
                OnHideObject(item.transform, false);
            }
        }

        StartCoroutine(ShowUnknownOutlines(false, 0));
    }

    private IEnumerator ShowUnknownOutlines(bool active, float time)
    {
        yield return new WaitForSeconds(time);

        foreach (var item in unknownSlots)
        {
            item.GetChild(0).gameObject.SetActive(active);
        }
    }

    void CheckIfOrder()
    {       
        GameObject startObject = null;
        var nextIndex = lastIndex + currentGameSet.interval;
        OrderGameObject nextObject = null;

        for (int i = 0; i < orderGameObjects.Count; i++)
        {
            if (orderGameObjects[i].newIndex == nextIndex && orderGameObjects[i].gameObject.activeSelf)
                nextObject = orderGameObjects[i];
        }

        if (!currentGameSet.shuffle && nextObject && currentGameSet.unknownValues.Count > 1) //Check if use guided mode
        {
            if (currentStartIndicator)
                Destroy(currentStartIndicator.gameObject);

            var slotTransform = nextObject.transform.parent;
            startObject = Instantiate(startIndicatorPrefab,nextObject.startPos, slotTransform.rotation, slotTransform);
            currentStartIndicator = startObject.transform;
        }
    }

    private void OnEnable()
    {
        TransferPlayerStats();
        //SetPlayerStatsCurrentCompletedSets(); //---FiX 03.03.21---
        OrderGameObject.OnRightObject += CheckIfOrder;
        OrderGameObject.OnTaskCompletete += NewTask;
        OrderGameObject.OnWrongObject += ResetLevel;
        OrderGameObject.OnFirstCorrectObject += FinalizeTask;
        OrderGameObject.OnUnknownPressed += ShowAlternatives;
        OrderGameObject.OnUnknownAltPressed += HideAlternatives;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel += StartSelectedGameSet;
        NewSet(true);

        if (OnOrderGameEnable != null)
            OnOrderGameEnable();
        //OrderGameObject.OnModIndex += CheckModdedIndex;
        OpenDifficultyPanelStart();
    }

    private void OnDisable()
    {
        OrderGameObject.OnRightObject -= CheckIfOrder;
        OrderGameObject.OnTaskCompletete -= NewTask;
        OrderGameObject.OnWrongObject -= ResetLevel;
        OrderGameObject.OnFirstCorrectObject -= FinalizeTask;
        OrderGameObject.OnUnknownPressed -= ShowAlternatives;
        OrderGameObject.OnUnknownAltPressed -= HideAlternatives;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel -= StartSelectedGameSet;
        //OrderGameObject.OnModIndex -= CheckModdedIndex;
        gemRewardBG.gameObject.SetActive(false);
        gameSetCounter = levelCounter * setsInLevel;
        ResetUnknownObjects();

        if (setCountPanel.gameObject.activeSelf)
            setCountPanel.gameObject.SetActive(false);

        StopAllCoroutines();
        CancelInvoke();

        if (OnOrderGameDisable != null)
            OnOrderGameDisable();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    private IEnumerator WinGame()
    {
        yield return new WaitForSeconds(0.5f);
        if (player)
            gameManager.player.anim.SetTrigger("Success");
        yield return new WaitForSeconds(1);
        CheckRewards();
        //StartCoroutine(RewardsPanelAnim());       
        //yield return new WaitForSeconds((lerpTime * 2) + rewardsPanelShowTime);
        yield return new WaitForSeconds(1);
        HideAllObjects();
        yield return new WaitForSeconds(lerpTime);
        ReleaseGems();
        yield return new WaitForSeconds(3);
        levelCounter = 0;
        gameSetCounter = 0;
        gameSetCounter = 0;
        isComplete = false;
        ModificationTools(false);
        
        if (!hasWonGame)
        {            
            var newEffect = Instantiate(winSetEffect, rewardsPanel.transform.position, Quaternion.identity, gameTransform);
            StartCoroutine(ShowRewardBG());

            trophyPanel.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            if (winGameSound)
                winGameSound.Play();
            trophyGraphic.gameObject.SetActive(true);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
            trophyGraphic.gameObject.SetActive(false);
            trophyPanel.gameObject.SetActive(false);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
            gameManager.playerStats.trophies.Add(trophy);
            gameManager.playerStats.hasWonOrderGame = true;

            foreach (var item in ObjectsToActivateOnComplete)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in ObjectsToSleepOnComplete)
            {
                item.gameObject.SetActive(false);
            }
        }

        hasWonGame = true;
        gameManager.playerStats.SaveGame();
        //gameManager.playerStats.OrderGameSetCount = 0;
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
    }

    private IEnumerator NextSet()
    {
        yield return new WaitForSeconds(0.5f);
        if (player)
            gameManager.player.anim.SetTrigger("Success");
        yield return new WaitForSeconds(1);
        CheckRewards();
        //StartCoroutine(RewardsPanelAnim());        
        //yield return new WaitForSeconds((lerpTime * 2) + rewardsPanelShowTime);

        HideAllObjects();
        yield return new WaitForSeconds(lerpTime);
        ReleaseGems();
        yield return new WaitForSeconds(3);
        NewSet(false);
    }

    private IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(0.5f);
        if (player)
            gameManager.player.anim.SetTrigger("Success");
        yield return new WaitForSeconds(1);
        CheckRewards();
        //StartCoroutine(RewardsPanelAnim());        
        //yield return new WaitForSeconds((lerpTime * 2) + rewardsPanelShowTime);

        HideAllObjects();
        yield return new WaitForSeconds(lerpTime);
        ReleaseGems();
        yield return new WaitForSeconds(3);       
        NewSet(true);
    }

    private IEnumerator ResetLevelRoutine()
    {
        yield return new WaitForSeconds(1);
        HideAllObjects();

        NewSet(true);
    }

    private IEnumerator ShowSetCountPanel()
    {
        if (orderGameDifficulty == PlayerStats.Difficulty.Easy)
            setCountImage.color = gameManager.colorEasy;
        else if (orderGameDifficulty == PlayerStats.Difficulty.Normal)
            setCountImage.color = gameManager.colorNormal;
        else if (orderGameDifficulty == PlayerStats.Difficulty.Hard)
            setCountImage.color = gameManager.colorHard;
        else
            setCountImage.color = Color.grey;

        setCountPanelText.text = (gameSetCounter + 1).ToString() + "/" + gameSets.Count.ToString();

        if (setCountPanelText.text.Length < 4)
            setCountPanelText.fontSize = 220;
        else if (setCountPanelText.text.Length == 4)
            setCountPanelText.fontSize = 200;
        else if (setCountPanelText.text.Length > 4)
            setCountPanelText.fontSize = 160;

        setCountPanel.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            setCountPanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        setCountPanel.gameObject.SetActive(false);
    }

    private IEnumerator ShowWinGamePanel()
    {
        float t = 0;
        float time = 1f;

        winGamePanel.gameObject.SetActive(true);

        while (t < 1)
        {
            t += Time.deltaTime / time;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            winGamePanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / time;
            float f = ti;
            f = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            winGamePanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, f);
            yield return new WaitForEndOfFrame();
        }

        winGamePanel.gameObject.SetActive(false);
    }

    void CheckRewards()
    {
        int testBonus = 1;

        if (CheckRewardCondition())
            testBonus = CalculateReward();

        currentReward = testBonus;

        //int bonusValue = 0;
        //int bonusMultiplier = 0;

        //var bonusVal = Mathf.Clamp(bonusValue, 0.001f, 9999f); // To prevent division by 0
        //var testBon = Mathf.Clamp(testBonus, 0.001f, 9999f);
    }

    private IEnumerator RewardsPanelAnim()
    {
        rewardsPanel.transform.localScale = Vector3.zero;
        rewardsPanel.gameObject.SetActive(true);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            rewardsPanel.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        float tim = 0;

        int testBonus = 1;

        if(CheckRewardCondition())
            testBonus = CalculateReward();

        currentReward = testBonus;

        int bonusValue = 0;
        int bonusMultiplier = 0;

        if (countGemsSound)
            countGemsSound.Play();

        while(tim <= 1)
        {
            tim += Time.deltaTime / (rewardsPanelShowTime / 2);

            var bonusVal = Mathf.Clamp(bonusValue, 0.001f, 9999f); // To prevent division by 0
            var testBon = Mathf.Clamp(testBonus, 0.001f, 9999f);

            //if ((bonusValue > 0 && (tim > (float)bonusValue / (float)testBonus) && bonusValue < testBonus) || bonusValue == 0)
            if ((bonusValue > 0 && (tim > bonusVal / testBon) && bonusValue < testBonus) || bonusValue == 0)
            {
                bonusValue++;
                //print(bonusValue / testBonus);
                print(tim);
                gemRewardText.text = " " + bonusValue.ToString();

                if (bonusValue > (rightCounter + (bonusReward * bonusMultiplier)))
                {
                    if (!boxGraphic.gameObject.activeSelf)
                        boxGraphic.gameObject.SetActive(true);
                    bonusMultiplier++;
                    bonusBoxCountText.text = " " + bonusMultiplier.ToString();
                }
            }

            yield return new WaitForEndOfFrame();
        }

        if (countGemsSound.isPlaying)
            countGemsSound.Stop();

        yield return new WaitForSeconds(rewardsPanelShowTime / 2);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / lerpTime;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            rewardsPanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        rewardsPanel.transform.localScale = Vector3.zero;
        rewardsPanel.gameObject.SetActive(false);
        gemRewardText.text = "";
        bonusBoxCountText.text = "";
    }

    bool wasRewarded = false;

    private bool CheckRewardCondition()
    {
        //if (!hasWonGame && (completedSets < gameSetCounter || (gameSetCounter == gameSets.Count - 1 && completedSets == gameSets.Count - 1)))
        if (!totalCompletedSets.Contains(currentGameSet))
        {
            totalCompletedSets.Add(currentGameSet);
            SetCurrentCompletedSet(0); //passed dummy value
            wasRewarded = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    private int CalculateReward()
    {
        return rightCounter + (bonusCounter * bonusReward);
    }
    private void ReleaseGems()
    {
        //var bonus = 1;

        //if (wasRewarded)
            //bonus = CalculateReward();

        var bonus = currentReward;

        var newEffect = Instantiate(winSetEffect, rewardsPanel.transform.position, Quaternion.identity, gameTransform);
        StartCoroutine(ShowRewardBG());
        complete.Play();

        for (int i = 0; i < bonus; i++)
        {
            var randTime = Random.Range(0, 1f);
            Invoke("SpawnGem", randTime);         
        }
    }

    void SpawnGem()
    {
        var newPos = HelperFunctions.RandomPointInBox(spawnGemBox.transform);
        var newGem = Instantiate(gem, newPos, spawnGemBox.transform.rotation, null);
    }

    private IEnumerator ShowRewardBG()
    {
        gemRewardBG.gameObject.SetActive(true);

        Vector4 startColor = new Vector4(gemRewardBG.color.r, gemRewardBG.color.g, gemRewardBG.color.b, 0);
        Vector4 endColor = new Vector4(gemRewardBG.color.r, gemRewardBG.color.g, gemRewardBG.color.b, 0.25f);
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            gemRewardBG.color = Vector4.Lerp(startColor, endColor, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / lerpTime;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            gemRewardBG.color = Vector4.Lerp(endColor, startColor, h);
            yield return new WaitForEndOfFrame();
        }

        gemRewardBG.gameObject.SetActive(false);
    }

    public int Modify(OrderGame.Modifiers modifier, int valueToModify, int modifierValue)
    {
        switch (modifier)
        {
            case OrderGame.Modifiers.Add:
                return valueToModify + modifierValue;
            case OrderGame.Modifiers.Subtract:
                return valueToModify - modifierValue;
            case OrderGame.Modifiers.Multiply:
                return valueToModify * modifierValue;
            case OrderGame.Modifiers.Divide:
                return valueToModify / modifierValue;
            case OrderGame.Modifiers.Wild:
                int randNum = Random.Range(0, 3);
                int randVal = Random.Range(currentGameSet.wildValueRange.x, currentGameSet.wildValueRange.y);

                if (randNum == 0)
                    return valueToModify + randVal;
                else if (randNum == 1)
                    return valueToModify - randVal;
                else if (randNum == 2)
                    return valueToModify - randVal;
                else
                {
                    print("Wild Modifier Error .... Didn't find options");
                    return valueToModify;
                }
            case OrderGame.Modifiers.None:
                return valueToModify;
            default:
                print("Modify Error .... Fell through Switch");
                return valueToModify;
        }
    }

    public void ChangeText(OrderGameObject passedObject, Modifiers passedModifier, int originalValue, int passedValue, bool isNew) // Change OrderGameObject with this
    {
        var tempText = "";

        if (isNew)
        {
            switch (passedModifier)
            {
                case (OrderGame.Modifiers.Add):
                    tempText = originalValue.ToString() + " + " + passedValue.ToString();
                    break;
                case (OrderGame.Modifiers.Subtract):
                    tempText = originalValue.ToString() + " - " + passedValue.ToString();
                    break;
                case (OrderGame.Modifiers.Multiply):
                    tempText = originalValue.ToString() + " • " + passedValue.ToString();
                    break;
                case (OrderGame.Modifiers.Divide):
                    tempText = originalValue.ToString() + " : " + passedValue.ToString();
                    break;
                case OrderGame.Modifiers.Wild:
                    int rand = Random.Range(0, 3);
                    int randVal = Random.Range(currentGameSet.wildValueRange.x, currentGameSet.wildValueRange.y);

                    if (rand == 0)
                        tempText = originalValue.ToString() + " + " + randVal.ToString();
                    else if (rand == 1)
                        tempText = originalValue.ToString() + " - " + randVal.ToString();
                    else if (rand == 2)
                        tempText = originalValue.ToString() + " • " + randVal.ToString();
                    else
                    {
                        print("Wild Modifier Error .... Didn't find options");
                        break;
                    }

                    break;
                default:
                    print("Modify Error .... Fell through Switch");
                    break;
            }
        }
        else
        {
            tempText = originalValue.ToString();
        }

        if (tempText.Length > 3 && tempText.Length < 6)
            passedObject.text.fontSize = 80;
        else if (tempText.Length == 6)
            passedObject.text.fontSize = 60;
        else if (tempText.Length > 6)
            passedObject.text.fontSize = 50;
        else if(tempText.Length < 3)
            passedObject.text.fontSize = passedObject.fontSize;

        passedObject.text.text = tempText;
    }

    public void ChangeTextModifier(OrderGameObject passedObject, Modifiers passedModifier, int originalValue, int passedValue, OG_Interactable passedInteractable, bool isNew) // Change OrderGameObject with this
    {
        var tempText = "";

        if (isNew)
        {
            switch (passedModifier)
            {
                case (OrderGame.Modifiers.Add):
                    tempText = originalValue.ToString() + " + " + passedValue.ToString();
                    break;
                case (OrderGame.Modifiers.Subtract):
                    tempText = originalValue.ToString() + " - " + passedValue.ToString();
                    break;
                case (OrderGame.Modifiers.Multiply):
                    tempText = originalValue.ToString() + " • " + passedValue.ToString();
                    break;
                case (OrderGame.Modifiers.Divide):
                    tempText = originalValue.ToString() + " : " + passedValue.ToString();
                    break;
                case OrderGame.Modifiers.Wild:
                    int rand = Random.Range(0, 3);
                    int randVal = Random.Range(currentGameSet.wildValueRange.x, currentGameSet.wildValueRange.y);

                    if (rand == 0)
                        tempText = originalValue.ToString() + " + " + randVal.ToString();
                    else if (rand == 1)
                        tempText = originalValue.ToString() + " - " + randVal.ToString();
                    else if (rand == 2)
                        tempText = originalValue.ToString() + " • " + randVal.ToString();
                    else
                    {
                        print("Wild Modifier Error .... Didn't find options");
                        break;
                    }

                    break;
                default:
                    print("Modify Error .... Fell through Switch");
                    break;
            }

            for (int i = 0; i < 6; i++)
            {
                passedInteractable.textMeshes[i].text = tempText;
            }
        }
        else
        {
            passedInteractable.ChangeText(passedInteractable.interactableModValue);
        }
    }

    public void ModificationTools(bool isActive)
    {
        modifySlot.gameObject.SetActive(isActive);
        trashSlot.gameObject.SetActive(isActive);
        spawner.gameObject.SetActive(isActive);
        spawnButton.gameObject.SetActive(isActive);
        landingArea.gameObject.SetActive(isActive);
    }

    //--------------------------------------------IDIFFICULTY-----------------------------------------------------------------------------

    public void CheckDifficulty()
    {
        if (!hasWonGame)
        {
            if ((correctStreakCounter >= numberOfCorrects && orderGameDifficulty != PlayerStats.Difficulty.Hard) || (wrongStreakCounter >= numberOfWrongs && orderGameDifficulty != PlayerStats.Difficulty.Easy))
            {
                if (correctStreakCounter >= numberOfCorrects && orderGameDifficulty == PlayerStats.Difficulty.Easy)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;
                else if (correctStreakCounter >= numberOfCorrects && orderGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Hard;
                else if (wrongStreakCounter >= numberOfWrongs && orderGameDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Easy;
                else if (wrongStreakCounter >= numberOfWrongs && orderGameDifficulty == PlayerStats.Difficulty.Hard)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;

                gameManager.hudCanvas.difficultyPanel.OpenPanel();
                correctStreakCounter = 0;
                wrongStreakCounter = 0;
            }
        }
    }

    public void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        orderGameDifficulty = requestedDifficulty;
        //hasWonGame = gameManager.playerStats.hasWonOrderGame;
        //gameSetCounter = gameManager.playerStats.OrderGameSetCount;
        //NewSet(true);
    }

    public void SetCurrentDifficultyStartSet(PlayerStats.Difficulty requestedDifficulty)
    {
        orderGameDifficulty = requestedDifficulty;
        TransformPlayer();
        //StopAllCoroutines();
        NewSet(true);
    }

    public void SetGameSetWithDifficulty(int index)
    {
        switch (orderGameDifficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                currentGameSet = gameSetsEasy[index];
                break;
            case (PlayerStats.Difficulty.Normal):
                currentGameSet = gameSets[index];
                break;
            case (PlayerStats.Difficulty.Hard):
                currentGameSet = gameSetsHard[index];
                break;
            default:
                currentGameSet = gameSets[index];
                break;
        }
    }

    public void WrongTicker()
    {
        wrongStreakCounter++;
        correctStreakCounter = 0;
        CheckDifficulty();
    }

    public void CorrectTicker()
    {
        wrongStreakCounter = 0;
        correctStreakCounter++;
        CheckDifficulty();
    }

    public void OpenDifficultyPanelStart()
    {
        gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.None;
        gameManager.hudCanvas.difficultyPanel.startNewLevel = true;

        if (gameManager.playerStats.hasWonOrderGame)
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = true;
            gameManager.hudCanvas.difficultyPanel.currentLevelCount = gameSets.Count;
        }
        else
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = false;
        }

        gameManager.hudCanvas.difficultyPanel.gameObject.SetActive(true);
    }

    public void SetCurrentCompletedSet(int passedAchievement)
    {
        int setIndex = 0;

        if (gameSets.Contains(currentGameSet))
            setIndex = gameSets.IndexOf(currentGameSet) + gameSets.Count;
        else if (gameSetsEasy.Contains(currentGameSet))
            setIndex = gameSetsEasy.IndexOf(currentGameSet);
        else if (gameSetsHard.Contains(currentGameSet))
            setIndex = gameSetsHard.IndexOf(currentGameSet) + (gameSets.Count * 2);

        gameManager.playerStats.orderGameCompletedSets.Add(setIndex);
        //SetPlayerStatsCurrentCompletedSets();
    }

    public void SetPlayerStatsCurrentCompletedSets()
    {
        gameManager.playerStats.currentCompletedSets.Clear();

        for (int i = 0; i < gameManager.playerStats.orderGameCompletedSets.Count; i++)
        {
            gameManager.playerStats.currentCompletedSets.Add(gameManager.playerStats.orderGameCompletedSets[i]);
        }

        //print("Order game transfered " + gameManager.playerStats.currentCompletedSets.Count.ToString() + " completed sets back to playerStats");
    }

    public void TransferPlayerStats()
    {
        hasWonGame = gameManager.playerStats.hasWonOrderGame;
        gameSetCounter = gameManager.playerStats.OrderGameSetCount;
        
        totalCompletedSets.Clear();

        for (int i = 0; i < gameManager.playerStats.orderGameCompletedSets.Count; i++)
        {
            var setIndex = gameManager.playerStats.orderGameCompletedSets[i];

            if (setIndex < gameSets.Count)
            {
                totalCompletedSets.Add(gameSetsEasy[setIndex]);
            }
            else if (setIndex >= gameSets.Count && setIndex < (gameSets.Count * 2))
            {
                totalCompletedSets.Add(gameSets[setIndex - gameSets.Count]);
            }
            else if (setIndex >= (gameSets.Count * 2) && setIndex < (gameSets.Count * 3))
            {
                totalCompletedSets.Add(gameSetsHard[setIndex - (gameSets.Count * 2)]);
            }           
        }

        //print("Recieving order game sets ..." + totalCompletedSets.Count);
    }

    public void StartSelectedGameSet(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        orderGameDifficulty = requestedDifficulty;

        if (requestedLevel < gameSets.Count)
            gameSetCounter = requestedLevel;
        else if (requestedLevel >= gameSets.Count && requestedLevel < (gameSets.Count * 2))
            gameSetCounter = requestedLevel - gameSets.Count;
        else if (requestedLevel >= gameSets.Count * 2)
            gameSetCounter = requestedLevel - (gameSets.Count * 2);

        //StopAllCoroutines();
        TransformPlayer();
        NewSet(true);
    }   
}
