﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorControllerQuadruped : MonoBehaviour
{
    public Animator anim;
    public CharacterController characterController;

    private float moveSpeed;
    private int groundedBool;

    private void Awake()
    {
        anim = transform.GetComponent<Animator>();
        characterController = transform.GetComponent<CharacterController>();
        groundedBool = Animator.StringToHash("Grounded");
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        NormalMove();
    }

    void NormalMove()
    {
        if (characterController.isGrounded)
        {
            anim.SetBool(groundedBool, true);

            moveSpeed = Vector2.ClampMagnitude(new Vector2(characterController.velocity.x, characterController.velocity.z), 1f).magnitude;

            anim.SetFloat("Speed", moveSpeed);

            anim.SetBool(groundedBool, true);
        }
        else
        {
            anim.SetBool(groundedBool, false);
        }
    }
}
