﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagerOld : MonoBehaviour
{
    public Camera playerCamera;
    public BasicBehaviour basicBehaviour;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "SceneCam")
        {
            other.gameObject.GetComponent<Camera>().enabled = true;
            basicBehaviour.playerCamera = other.transform;
            playerCamera.enabled = false;

            if(other.transform.GetChild(0))
            {
                other.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "SceneCam")
        {
            playerCamera.enabled = true;
            basicBehaviour.playerCamera = playerCamera.transform;
            other.gameObject.GetComponent<Camera>().enabled = false;

            if (other.transform.GetChild(0))
            {
                other.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
}
