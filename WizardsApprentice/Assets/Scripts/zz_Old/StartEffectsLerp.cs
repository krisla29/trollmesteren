﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEffectsLerp : MonoBehaviour
{
    //Position
    [SerializeField] private Vector3 start;
    [SerializeField] private Vector3 end;
    [SerializeField] private bool useLocal;
    [SerializeField] private LerpUtils.LerpType lerpType;
    [SerializeField] private bool hasDelay;
    [SerializeField] private float delayTime;
    [SerializeField] private float moveTime;
    

    void OnEnable()
    {
        if (hasDelay)
        {
            StartCoroutine("Delay", delayTime);
        }
        else
        {
            StartCoroutine("Translate");
        }
    }
    private IEnumerator Translate()
    {
        float t = 0;
        
        while (t < moveTime)
        {
            var lerp = LerpUtils.GetLerpType(lerpType, t);

            print("Translating " + transform.name +", position is " + transform.position);
            if (!useLocal)
            {
                transform.position = Vector3.Lerp(start, end, lerp);
            }
            else
            {
                transform.localPosition = Vector3.Lerp(start, end, lerp);
            }

            t += moveTime <= 0 ? 1 : Time.deltaTime / moveTime;

            yield return new WaitForEndOfFrame();
        }
    }
    private IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(time);
        print("WaitAndPrint " + Time.time);
        StartCoroutine("Translate");
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
