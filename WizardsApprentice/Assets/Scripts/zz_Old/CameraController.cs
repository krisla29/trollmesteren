﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 smoothV;
    public PlayerController player;
    //----------------------------------------------------!!!!!!!!!!!!!!!!!!!!
    //public PlayerFly playerFly;
    //public PlayerNormalMove playerNormal;
    //public PlayerMovement currentMovement; //--IS SET BY PLAYERCONTROLLER
    //----------------------------------------------------!!!!!!!!!!!!!!!!!!!!
    public InputManager inputManager;
    private GameObject camParent;
    private Camera cam;
    private HudCanvas hudCanvas;
    public float horizontalSpeedMultiplier = 1;
    public float verticalSpeedMultiplier = 1;
    public float rotationSpeedMultiplier = 1.5f;
    private bool isAiming;
    private bool isNormal;
    private Transform target;
    public float lerpSpeed = 5f;
    public float returnSpeed = 3f;
    float startTimer = 0;
    private Vector2 mouseDelta;
    public Vector3 camOffset = new Vector3(0, 1.35f, -2.85f);
    //public float normalZoom = 60;
    //private float timer;

    // Use this for initialization
    void Start()
    {
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        camParent = new GameObject();
        camParent.name = "CamParent";
        hudCanvas = GameObject.FindGameObjectWithTag("HUD").GetComponent<HudCanvas>();
        //hudCanvas.playerCam = transform.GetComponent<CameraController>();
        target = player.transform;
        cam = GetComponent<Camera>();

        //Camera.main.fieldOfView = normalZoom;

        camParent.transform.position = target.position;
        transform.SetParent(camParent.transform);
        transform.localPosition = camOffset;

        startTimer = Time.time;
        mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());
        //timer = 0;
    }
    /*
    //Split rotation euler degrees to positive and negative values:
    float fieldAngle(float angle)
    {
        if (angle > 180)
        { angle -= 360; }
        else
        { }
        return angle;
    }
    */
    void Update()
    {       
        if (player == null)
        {
            Destroy(transform.parent.gameObject);
        }
        else
        {
            /*
            if(!playerFly.enabled)
            {
                player = playerNormal;
            }
            else
            {
                player = playerFly;
            }
            */

            if (player.toggleAim)
            {
                isAiming = true;
                AimMovement();
                startTimer = Time.time;
            }

            if (!player.toggleAim && isAiming == true)
            {
                AimReturn();
            }
            
            if (!isAiming)
            {
                mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());

                camParent.transform.position = player.transform.position;

                transform.localPosition = camOffset;

                transform.localEulerAngles = Vector3.zero;



                if (mouseDelta != Vector2.zero)
                {
                    NormalMovement();
                }

                else
                {
                    NormalReturn();
                }
            }
            
        }
        /*
                                  RESETTING CAM IF AIM IS INTERRUPTED!
        if (INSERT CONDITION!!)
            {
            transform.localPosition = camOffset;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        */
        //---------------------------------------------------------------------------------------------------------------------------------------------------


    }

    void NormalMovement()
    {
        float verticalSpeed = 2f;
        float horizontalSpeed = 2f;
        float camSpeed = 50;

        mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());

        mouseLook.y = mouseDelta.y * verticalSpeed * verticalSpeedMultiplier;
        mouseLook.x = mouseDelta.x * horizontalSpeed * horizontalSpeedMultiplier;

        camParent.transform.eulerAngles += new Vector3(-mouseLook.y, mouseLook.x, 0f) * Time.deltaTime * camSpeed * rotationSpeedMultiplier;

        //testTime = Time.time;
    }

    void NormalReturn()
    {
        mouseDelta = new Vector2(0, 0);
        smoothV = new Vector2(0, 0);
        mouseLook = new Vector2(0, 0);
    }

    void AimMovement()
    {
        camParent.transform.rotation = Quaternion.Slerp(camParent.transform.rotation, target.rotation, lerpSpeed * 10 * Time.deltaTime);
        Vector3 aimPos = target.position + (target.transform.right * 1f) + (target.transform.up * 1.5f) + (target.transform.forward * -1f);

        transform.position = Vector3.Lerp(transform.position, aimPos, Time.deltaTime * 100f);

        float aimSpeed = 100f;

        mouseDelta.x = inputManager.RightHorizontal();
        mouseDelta.y = inputManager.RightVertical();
        mouseLook.y = mouseDelta.y * 1.5f;
        mouseLook.x = mouseDelta.x * 1.5f;

        float yRot = transform.localEulerAngles.y;
        float xRot = transform.localEulerAngles.x;

        //Clamping rotations:
        /*
        if (transform.localEulerAngles.y < 90f || transform.localEulerAngles.y > 270f)
        { yRot += mouseLook.x * Time.deltaTime * aimSpeed; }

        if (transform.localEulerAngles.y >= 90f && transform.localEulerAngles.y <= 270f)
            if (transform.localEulerAngles.y > 90f && transform.localEulerAngles.y < 180f)
            { yRot = Mathf.Lerp(yRot, 89f, Time.deltaTime * 10f); }

        if (transform.localEulerAngles.y < 270f && transform.localEulerAngles.y > 180f)
        { yRot = Mathf.Lerp(yRot, 271f, Time.deltaTime * 10f); }
        */
        if (transform.localEulerAngles.x < 60f || transform.localEulerAngles.x > 300f)
        { xRot += -mouseLook.y * Time.deltaTime * aimSpeed; }

        if (transform.localEulerAngles.x >= 60f && transform.localEulerAngles.x <= 300f)
            if (transform.localEulerAngles.x > 60f && transform.localEulerAngles.x < 180f)
            { xRot = Mathf.Lerp(xRot, 59f, Time.deltaTime * 10f); }

        if (transform.localEulerAngles.x < 300f && transform.localEulerAngles.x > 180f)
        { xRot = Mathf.Lerp(xRot, 301f, Time.deltaTime * 10f); }

        transform.localEulerAngles = new Vector3(xRot, 0, 0);
    }
    
    
    void AimReturn()
    {
        mouseDelta = new Vector2(0, 0);
        smoothV = new Vector2(0, 0);
        mouseLook = new Vector2(0, 0);

        if ((Time.time - startTimer) > returnSpeed)     
        {
            transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(0, 0, 0), lerpSpeed * Time.deltaTime);
        }
        else
        {
            isAiming = false;
        }       
    }
}
