﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerByReference : MonoBehaviour
{
    public List<Transform> references = new List<Transform>();
    public delegate void TriggerByReferenceEvent(Transform trigger, Transform reference); // Sends to RacingGame
    public static event TriggerByReferenceEvent OnTrigByRefEnter;
    public static event TriggerByReferenceEvent OnTrigByRefExit;

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < references.Count; i++)
        {
            if(references[i] == other.transform)
            {
                if(OnTrigByRefEnter != null)
                {
                    OnTrigByRefEnter(transform, references[i]);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        for (int i = 0; i < references.Count; i++)
        {
            if (references[i] == other.transform)
            {
                if (OnTrigByRefExit != null)
                {
                    OnTrigByRefExit(transform, references[i]);
                }
            }
        }
    }
}
