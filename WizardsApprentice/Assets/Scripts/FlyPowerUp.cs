﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyPowerUp : MonoBehaviour
{
    public int powerUpTime;

    public delegate void FlyPowerUpEvent(int time);
    public static event FlyPowerUpEvent OnFlyPowerUp;

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            if (OnFlyPowerUp != null)
            {
                OnFlyPowerUp(powerUpTime);
            }

            Destroy(transform.gameObject);        
        }
    }   
}
