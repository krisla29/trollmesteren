﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CheckCorrectSlots : MonoBehaviour
{
    public NavMeshObstacle navMeshObstacle;
    public InteractableBehaviourDTPFree interactableDTP;
    public List<Transform> correctSlots = new List<Transform>();
    private Transform lastPoint;

    private void Update()
    {
        CheckLastPoint();
    }

    void CheckLastPoint()
    {
        if(interactableDTP.lastPoint != lastPoint)
        {
            CheckIfInCorrectPosition();
        }

        lastPoint = interactableDTP.lastPoint;
    }

    void CheckIfInCorrectPosition()
    {
        if(correctSlots.Contains(interactableDTP.lastPoint))
        {
            IsCorrect();
        }
        else
        {
            IsWrong();
        }
    }

    void IsCorrect()
    {
        if (navMeshObstacle.enabled)
            navMeshObstacle.enabled = false;
    }

    void IsWrong()
    {
        if (!navMeshObstacle.enabled)
            navMeshObstacle.enabled = true;
    }
}
