﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAllField : MonoBehaviour
{
    [SerializeField] private bool isInverse;

    private void OnTriggerEnter(Collider other)
    {
        if (!isInverse)
        {
            if (!other.gameObject.CompareTag("Indestructable"))
            {
                print("Killed " + other.gameObject.name);
                Destroy(other.gameObject);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isInverse)
        {
            if (!other.gameObject.CompareTag("Indestructable"))
            {
                print("Killed " + other.gameObject.name);
                Destroy(other.gameObject);
            }
        }
    }    
}
