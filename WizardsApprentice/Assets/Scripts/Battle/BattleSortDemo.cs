﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleSortDemo : MonoBehaviour
{
    public CanvasGroup panelCanvasGroup;
    public RectTransform pointerParent;
    public Image pointerImage;
    public RectTransform bottleGraphic;
    public RectTransform pointerGraphic;
    public RectTransform forceArrow;
    public Image forceArrowImage;
    public RectTransform directionArrow;
    public Image directionArrowImage;

    public RectTransform pointerGem1;
    public RectTransform pointerGem2;
    public RectTransform pointerGem3;
    public RectTransform pointerGem4;

    public RectTransform bottleRestPos;
    public RectTransform bottleChargedPos;
    public RectTransform bottleAimPos;
    public RectTransform pointerInitialPos;

    public int demoNumber = 0;

    public float zPos = -60;
    
    public Vector3 startPos;
    public Vector3 bottlePos;
    public Vector3 middlePos;
    public Vector3 bottomPos;
    
    public bool isActive;
    public bool hasTerminated;

    private void OnEnable()
    {
        isActive = true;

        StartCoroutine(ShowPanel());
    }

    private void OnDisable()
    {
        isActive = false;
        hasTerminated = false;
        StopAllCoroutines();
        ResetGraphics();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void Update()
    {
        if(!isActive && !hasTerminated)
        {
            StopAllCoroutines();
            StartCoroutine(DisableScript());
            hasTerminated = true;
        }
    }

    private IEnumerator ShowPanel()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 0.5f;
            panelCanvasGroup.alpha = Mathf.Lerp(0, 1, t);
            yield return new WaitForEndOfFrame();
        }
        /*
        if (demoNumber == 0)
            StartCoroutine(DemoSequenceGem(pointerGem4));
        else if (demoNumber == 1)
            StartCoroutine(DemoSequenceGem(pointerGem3));
        else if (demoNumber == 2)
            StartCoroutine(DemoSequenceGem(pointerGem1));
        */
        if (demoNumber == 3)
            StartCoroutine(DemoSequenceBottleThrow());
    }

    private IEnumerator DemoSequenceBottleThrow()
    {       
        pointerGraphic.gameObject.SetActive(true);
        bottleGraphic.gameObject.SetActive(false);
        pointerGraphic.localPosition = Vector3.zero;
        bottleGraphic.localPosition = Vector3.zero;
        bottleGraphic.localScale = Vector3.one;
        pointerParent.localPosition = startPos;
        //pointerParent.localPosition = Vector3.zero;
        //pointerParent.anchoredPosition = pointerInitialPos.anchoredPosition;
        forceArrow.gameObject.SetActive(false);
        directionArrow.gameObject.SetActive(false);
        pointerParent.gameObject.SetActive(true);
        
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 1;
            panelCanvasGroup.alpha = Mathf.Lerp(0, 1, t);
            yield return new WaitForEndOfFrame();
        }
        
        
        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 0.5f;
            float h = ti;            
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            pointerParent.localPosition = Vector3.Lerp(startPos, bottlePos, h);
            yield return new WaitForEndOfFrame();
        }
        
        yield return new WaitForSeconds(0.5f);

        bottleGraphic.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        float tim = 0;

        while (tim < 1)
        {
            tim += Time.deltaTime / 1f;
            float h = tim;
            h = tim * tim * tim * (tim * (6f * tim - 15f) + 10f);
            pointerParent.localPosition = Vector3.Lerp(bottlePos, middlePos, h);
            yield return new WaitForEndOfFrame();
        }
        
        forceArrow.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);
        
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / 1f;
            float h = time;
            h = time * time * time * (time * (6f * time - 15f) + 10f);
            pointerParent.localPosition = Vector3.Lerp(middlePos, bottomPos, h);
            yield return new WaitForEndOfFrame();
        }
        
        yield return new WaitForSeconds(0.5f);

        //directionArrow.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        forceArrow.gameObject.SetActive(false);

        float timer = 0;

        while (timer < 1)
        {
            timer += Time.deltaTime / 0.3f;
            float h = timer;
            h = timer * timer * timer * (timer * (6f * timer - 15f) + 10f);
            pointerGraphic.localPosition = Vector3.Lerp(Vector3.zero, new Vector3(150, 0, 0), h);
            yield return new WaitForEndOfFrame();
        }

        float timerX = 0;

        while (timerX < 1)
        {
            timerX += Time.deltaTime / 1f;
            float h = timerX;
            h = timerX * timerX * timerX * (timerX * (6f * timerX - 15f) + 10f);
            bottleGraphic.localPosition = Vector3.Lerp(Vector3.zero, new Vector3(0, 512f, 0), h);
            bottleGraphic.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 0.5f, h);
            yield return new WaitForEndOfFrame();
        }
        
        yield return new WaitForSeconds(0.5f);

        bottleGraphic.gameObject.SetActive(false);

        yield return new WaitForSeconds(0.5f);

        float timerXI = 0;

        while (timerXI < 1)
        {
            timerXI += Time.deltaTime / 1;
            panelCanvasGroup.alpha = Mathf.Lerp(1, 0, timerXI);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(6);

        StartCoroutine(DemoSequenceBottleThrow());
    }

    private IEnumerator DisableScript()
    {       
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 0.5f;
            panelCanvasGroup.alpha = Mathf.Lerp(1, 0, t);
            yield return new WaitForEndOfFrame();
        }

        pointerGem1.gameObject.SetActive(false);
        pointerGem2.gameObject.SetActive(false);
        pointerGem3.gameObject.SetActive(false);
        pointerGem4.gameObject.SetActive(false);

        transform.gameObject.SetActive(false);
    }

    private IEnumerator DemoSequenceGem(RectTransform pointer)
    {
        pointer.gameObject.SetActive(true);

        yield return null;
        /*
        yield return new WaitForSeconds(6);

        pointer.gameObject.SetActive(false);

        yield return new WaitForSeconds(6);

        StartCoroutine(DemoSequenceGem(pointer));
        */
    }

    private void ResetGraphics()
    {
        pointerGraphic.gameObject.SetActive(false);
        bottleGraphic.gameObject.SetActive(false);
        pointerGraphic.localPosition = Vector3.zero;
        bottleGraphic.localPosition = Vector3.zero;
        bottleGraphic.localScale = Vector3.one;
        pointerParent.localPosition = startPos;
        forceArrow.gameObject.SetActive(false);
        directionArrow.gameObject.SetActive(false);
        pointerParent.gameObject.SetActive(false);
    }
}
