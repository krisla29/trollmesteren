﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCollider : MonoBehaviour
{
    public GameManager gameManager;
    public bool playerInsideCollider;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            playerInsideCollider = true;

            if (gameManager.player)
            {
                if(gameManager.player.navMeshAgent.enabled)
                    gameManager.player.navMeshAgent.SetDestination(other.transform.position);

                gameManager.player.isLocked = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {       
        if (other.gameObject.CompareTag("Player"))
        {
            playerInsideCollider = false;
        }
    }    
}
