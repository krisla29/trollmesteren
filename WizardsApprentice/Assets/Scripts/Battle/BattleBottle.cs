﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleBottle : MonoBehaviour
{
    public BattleSort battleSortScript;
    public GameObject bottleInstance;
    public Collider bottleCollider;
    public Text bottleText;
    public SkinnedMeshRenderer bottleLiquid;
    public Transform bottleParent;
    public SpriteRenderer aimArrow;
    public SpriteRenderer glow;
    public EasyRotate2 rotateScript;
    public float liquidBlend;
    public int value = 0;
    private float lerpTime = 1;
    private Camera cam;
    private Vector3 currentDir;
    private float throwForce = 5f;
    public bool isLocked;

    public AudioSource pickupBottleAudio;
    public AudioSource throwBottleAudio;
    public AudioSource damageHitAudio;
    public AudioSource emptyHitAudio;
    public AudioSource missHitAudio;
    public AudioSource gasReleaseAudio;

    public delegate void BattleBottleEvent(); // Sends to BattleSort
    public static event BattleBottleEvent OnVictory;

    private void Start()
    {
        bottleLiquid.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        BattleSort.OnBottleChangeValue += GetValue;
        BottleProjectile.OnHitEnemy += StartHit;
        BottleProjectile.OnHitOther += HitOther;
    }

    private void OnDisable()
    {
        BattleSort.OnBottleChangeValue -= GetValue;
        BottleProjectile.OnHitEnemy -= StartHit;
        BottleProjectile.OnHitOther -= HitOther;
        StopAllCoroutines();
    }

    public void GetValue(int passedValue)
    {
        int currentValue = value;

        if (passedValue > 0)
        {
            bottleLiquid.gameObject.SetActive(true);

            if (currentValue + passedValue > battleSortScript.bottleMaxValue)
                StartCoroutine(ChangeLiquid(battleSortScript.bottleMaxValue));
            else
                StartCoroutine(ChangeLiquid(currentValue + passedValue));
        }
        else
        {            
            StartCoroutine(ChangeLiquid(0));
        }        
    }

    void SetNewValue(int passedValue)
    {
        value = passedValue;
        bottleText.text = value.ToString();
    }

    private IEnumerator ChangeLiquid(int targetValue)
    {
        yield return new WaitForSeconds(1f);

        float currentVolume = bottleLiquid.GetBlendShapeWeight(0);
        float targetVolumeInvert = ((float)targetValue / (float)battleSortScript.bottleMaxValue) * 100f;
        float targetVolume = 100f - targetVolumeInvert;
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / lerpTime;
            bottleLiquid.SetBlendShapeWeight(0, Mathf.Lerp(currentVolume, targetVolume, t));
            yield return new WaitForEndOfFrame();
        }

        if(targetValue == 0)
        {
            bottleLiquid.gameObject.SetActive(false);
        }

        SetNewValue(targetValue);
    }

    private void OnMouseDown()
    {
        if (!isLocked)
        {
            battleSortScript.gameManager.hudCanvas.exitButton.gameObject.SetActive(false);

            glow.gameObject.SetActive(false);
            rotateScript.enabled = false;
            bottleText.gameObject.SetActive(false);
            aimArrow.gameObject.SetActive(true);
            transform.SetParent(battleSortScript.transform);
            cam = Camera.main;
            transform.localScale = Vector3.one * 100f;
            aimArrow.transform.parent.rotation = Quaternion.identity;
            pickupBottleAudio.Play();
        }
    }

    private void OnMouseDrag()
    {
        if (!isLocked)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);

            Debug.DrawLine(ray.origin, ray.origin + ray.direction * 5f, Color.red);
            Vector3 pos = ray.origin + (ray.direction * 0.2f);

            var lookDir = new Vector3(ray.direction.x, -ray.direction.y, ray.direction.z);

            aimArrow.transform.parent.rotation = Quaternion.LookRotation(lookDir, Vector3.up);
            transform.position = pos;

            currentDir = (aimArrow.transform.parent.rotation * Vector3.forward);
        }
    }

    private void OnMouseUp()
    {
        if (!isLocked)
        {
            battleSortScript.gameManager.hudCanvas.exitButton.gameObject.SetActive(true);

            var centerPixel = Screen.height / 2;
            var suspentionForce = 1f;

            if (Input.mousePosition.y < centerPixel)
            {
                suspentionForce += (centerPixel - Input.mousePosition.y) / Screen.height;
            }

            var newBottleInstance = Instantiate(bottleInstance, transform.position, transform.rotation, battleSortScript.transform);
            var rb = newBottleInstance.GetComponent<Rigidbody>();
            var projectileScript = newBottleInstance.GetComponent<BottleProjectile>();

            if (value > 0)
            {
                projectileScript.isEmpty = false;
                projectileScript.liquid.gameObject.SetActive(true);
            }
            else
            {
                projectileScript.isEmpty = true;
                projectileScript.liquid.gameObject.SetActive(false);
            }

            rb.AddForce(currentDir * throwForce * suspentionForce, ForceMode.Impulse);

            rotateScript.enabled = true;
            bottleText.gameObject.SetActive(true);
            aimArrow.gameObject.SetActive(false);
            transform.SetParent(bottleParent);
            transform.localPosition = Vector3.zero;
            transform.localScale = Vector3.one * 200f;
            aimArrow.transform.parent.rotation = Quaternion.identity;
            glow.gameObject.SetActive(true);

            throwBottleAudio.Play();
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void StartHit()
    {
        StartCoroutine(HitEnemy());
    }

    void HitOther()
    {
        missHitAudio.Play();
    }
    private IEnumerator HitEnemy()
    {
        if(value > 0)
        {
            damageHitAudio.Play();
            gasReleaseAudio.Play();
            battleSortScript.liquidAudio.PlayDelayed(0.75f);

            if (!battleSortScript.firstBloodDrawn)
            {
                battleSortScript.firstBloodDrawn = true;
                battleSortScript.battleSortDemo.isActive = false;
            }
        }

        else
        {
            emptyHitAudio.Play();
        }

        StartCoroutine(ChangeLiquid(0));
        battleSortScript.ResetValues();

        var enemy = battleSortScript.battleManager.currentEnemy;
        var startScale = enemy.scaleTransform.localScale.x;
        var startHealth = battleSortScript.enemyCurrentHealth;
        var targetScale = 1f;
        var targetHealth = 100f;

        float t = 0;

        if (enemy.scaleTransform.localScale.x - ((value / (battleSortScript.enemyToughness * 100) * 0.75f)) < battleSortScript.enemyMinSize)
            targetScale = 0;
        else
            targetScale = enemy.scaleTransform.localScale.x - ((value / (battleSortScript.enemyToughness * 100) * 0.75f));

        if (battleSortScript.enemyCurrentHealth - ((value / (battleSortScript.enemyToughness * 100) * 0.75f)) < 0)
            targetHealth = 0;
        else
            targetHealth = battleSortScript.enemyCurrentHealth - ((value / (battleSortScript.enemyToughness * 100) * 0.75f));

        battleSortScript.pauseGrowth = true;
        

        while (t < 1)
        {
            t += Time.deltaTime;
            enemy.scaleTransform.localScale = Vector3.Lerp(Vector3.one * startScale, Vector3.one * targetScale, t);
            battleSortScript.enemyCurrentHealth = Mathf.Lerp(startHealth, targetHealth, t);
            yield return new WaitForEndOfFrame();
        }

        if(battleSortScript.streakCounter > 0)
        {
            for (int i = 0; i < battleSortScript.streakCounter; i++)
            {

            }
        }

        //if (targetScale >= battleSortScript.enemyMinSize)
        //    battleSortScript.pauseGrowth = false;
        if(targetHealth > 0)
            battleSortScript.pauseGrowth = false;
        else
        {
            if (OnVictory != null)
                OnVictory();
        }            
    }

    private IEnumerator BonusHit()
    {
        float t = 0;
        float time = 0.5f;

        while(t < 1)
        {
            t += Time.deltaTime / time;

            yield return new WaitForEndOfFrame();
        }
    }
}
