﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleSort : Battle
{
    public GameManager gameManager;
    public GameObject battleObjectPrefab;
    public GameObject popEffect;
    public GameObject trophyPrefab;
    public GameObject megaGemPrefab;
    public GameObject victoryEffect;
    public GameObject loseEffect;
    public RectTransform winGamePanel;
    public RectTransform loseGamePanel;
    public BattleManager battleManager;
    public BattleSortDemo battleSortDemo;
    public List<BattleSortSlot> battleSlots = new List<BattleSortSlot>();
    public List<Material> slotMaterials = new List<Material>();
    private float slotScale = 90f;
    public Material slotDefaultMatActive;
    public Material slotDefaultMatInactive;
    public Material slotMatError;
    public BattleSortSlot activeSlot;
    public BattleBottle battleBottle;
    public AddNoiseToProperty bottleNoise;
    public int bottleMaxValue = 100;
    public float enemyToughness = 0.75f; //enemy downscale is determined by value / enemyToughness * 100
    public float enemyMinSize = 0.5f; //determines when game is won
    public float enemyMaxSize = 5f; //determines when game is lost

    public float enemyMaxHealth = 200;
    public float enemyCurrentHealth;
    public float enemyStartHealth = 100;

    public int correctValue = 30;
    public int streakBonus = 10;
    public int fullBottleBonus = 20;
    public TextMeshProUGUI bonusText;

    public Image symbolOrderAscending;
    public Image symbolOrderDescending;

    public Color rangePanelColorDescending;
    public Color rangePanelColorAscending;
    public Color rangeTextColorDescending;
    public Color rangeTextColorAscending;
    public TextMeshProUGUI currentRangeText;
    public RectTransform rangeTextPanel;
    public Image rangeTextPanelImage;
    public TextMeshProUGUI currentRangeTextCopy;
    public float rangeTextCopyFontSize = 175f;
    public RectTransform rangeTextCopyPanel;
    public Image rangeTextCopyPanelImage;
    public CanvasGroup textCopyCanvasGrp;
    private Vector2 startAnchorMax;
    private Vector2 startAnchorMin;
    private Vector2 startSizeDelta;
    private Vector2 startPivot;

    public CanvasGroup animatedNumberParent;
    public Text animatedNumberText;
    public Image animatedNumberTextBg;

    public enum SortMethods { Descending, Ascending, }

    public enum SlotOrders { Descending, Ascending, Random}
    [Header("BattleObjectsProperties")]
    public Vector2 timeRange = new Vector2(1f, 3f);
    public Vector2 speedRange = new Vector2(2f, 4f);
    public Vector2 lifeSpanRange = new Vector2(4f, 8f);
    public Vector2 sizeRange = new Vector2(0.5f, 2f);

    public List<BattleSortObject> battleObjects = new List<BattleSortObject>();
    private List<float> startTimes = new List<float>();
    private List<float> lifeSpans = new List<float>();
    private List<float> speeds = new List<float>();
    private List<Vector3> directions = new List<Vector3>();
    private List<int> values = new List<int>();

    public Color cObjectTextAscending = Color.red;
    public Color cObjectTextDescending = Color.cyan;

    public bool hasInitialized;
    public bool maxValueReached;
    public int streakCounter = 0;
    public int currentValue;
    public int currentBonusValue;
    public float currentGrowthRatio = 1.5f;
    public bool pauseGrowth;
    public bool isVictory;
    public bool hasWonGame;
    public bool isDefeat;
    public bool firstBloodDrawn; //Used by BattleBottle
    public SlotOrders currentSlotOrder = SlotOrders.Descending;
    private Vector2Int currentValueRange = new Vector2Int(0, 20);

    public AudioSource popDiamondAudio;
    public AudioSource correctAudio;
    public AudioSource streakAudio;
    public AudioSource dropSlotAudio;
    public AudioSource liquidAudio;
    public AudioSource wrongAudio;
    public AudioSource winGameAudio;
    public AudioSource winGameAudio2;
    public AudioSource loseGameAudio;
    public AudioSource monsterEvilLaugh;

    private bool hasPlayedFirstTime;
    //private int firstRoundsDesc;
    //private int fisrtRoundsAsc;
    private int firstRounds;

    /*
    private bool addToActive;
    private BattleSortObject lastBattleSortObject;
    private int lastIndex;
    Coroutine AddToActiveInst = null; //Holder for the currently running WaitForAddToActiveSlot IEnumerator
    */
    Coroutine AnimateNumberInst = null; //Holder for the currently running AnimateNumber IEnumerator
    
    private int round;
    //public TextMeshProUGUI winText;
    //public TextMeshProUGUI loseText;

    public delegate void BattleSortEvent(int value); // Sends to BattleBottle
    public static event BattleSortEvent OnBottleChangeValue;

    public delegate void BattleSortMainEvent(); //Sends to PlayerStats
    public static event BattleSortMainEvent OnBattleSortEnable;
    public static event BattleSortMainEvent OnBattleSortDisable;

    private void Awake()
    {
        startAnchorMax = rangeTextCopyPanel.anchorMax;
        startAnchorMin = rangeTextCopyPanel.anchorMin;
        startSizeDelta = rangeTextCopyPanel.sizeDelta;
        startPivot = rangeTextCopyPanel.pivot;
        rangeTextCopyPanel.gameObject.SetActive(false);
    }

    private void OnEnable()
    {              
        BattleBottle.OnVictory += StartVictory;

        if (gameManager)
        {
            if (gameManager.player)
            {
                gameManager.player.isBattle = true;
            }
        }

        InitializeGame();

        Invoke("SpawnObjects", Random.Range(timeRange.x, timeRange.y));

        battleManager.enemyHealthBar.gameObject.SetActive(true);
        enemyCurrentHealth = gameManager.playerStats.battleSortEnemyHealth;
        round = 0;
        
        if (!hasPlayedFirstTime)
        {
            firstRounds = 0;
            SetTutorialLevel(0);
        }
        else
        {            
            SetCurrentRange();
        }

        hasInitialized = true;

        if (OnBattleSortEnable != null)
            OnBattleSortEnable();
    }
   
    private void OnDisable()
    {
        BattleBottle.OnVictory -= StartVictory;

        if (gameManager)
        {
            if (gameManager.player)
            {
                gameManager.player.isBattle = false;
            }
        }

        CancelInvoke();

        foreach (var item in battleObjects)
        {
            Destroy(item.gameObject);
        }

        battleObjects.Clear();
        startTimes.Clear();
        lifeSpans.Clear();
        directions.Clear();
        speeds.Clear();
        hasInitialized = false;
        battleManager.enemyHealthBar.gameObject.SetActive(false);
        enemyCurrentHealth = 0.5f;
        symbolOrderAscending.gameObject.SetActive(false);
        symbolOrderDescending.gameObject.SetActive(false);
        StopAllCoroutines();

        if (OnBattleSortDisable != null)
            OnBattleSortDisable();
    }

    void InitializeGame()
    {
        isDefeat = false;
        isVictory = false;

        if (currentSlotOrder == SlotOrders.Descending)
        {
            hasPlayedFirstTime = gameManager.playerStats.hasPlayedBattleDemoDesc;            
        }
        else if (currentSlotOrder == SlotOrders.Ascending)
        {
            hasPlayedFirstTime = gameManager.playerStats.hasPlayedBattleDemoAsc;            
        }

        firstBloodDrawn = hasPlayedFirstTime;
        hasWonGame = gameManager.playerStats.hasWonBattleSort;
        enemyCurrentHealth = gameManager.playerStats.battleSortEnemyHealth;
        //enemyCurrentHealth = 0.5f;

        bonusText.transform.parent.gameObject.SetActive(false);
        activeSlot = null;
        maxValueReached = false;
        streakCounter = 0;
        currentValue = 0;
        currentBonusValue = 0;
        pauseGrowth = false;

        if(currentSlotOrder == SlotOrders.Descending)
        {
            rangeTextPanelImage.color = rangePanelColorDescending;
            rangeTextCopyPanelImage.color = rangePanelColorDescending;
            currentRangeText.color = rangeTextColorDescending;
            currentRangeTextCopy.color = rangeTextColorDescending;
            symbolOrderDescending.gameObject.SetActive(true);
        }
        else if (currentSlotOrder == SlotOrders.Ascending)
        {
            rangeTextPanelImage.color = rangePanelColorAscending;
            rangeTextCopyPanelImage.color = rangePanelColorAscending;
            currentRangeText.color = rangeTextColorAscending;
            currentRangeTextCopy.color = rangeTextColorAscending;
            symbolOrderAscending.gameObject.SetActive(true);
        }

        foreach (var item in battleSlots)
        {
            item.valueText.gameObject.SetActive(false);
            item.textGlow.gameObject.SetActive(false);
        }

        InitializeSlots();
    }

    private void Update()
    {
        for (int i = 0; i < battleObjects.Count; i++)
        {
            if (battleObjects[i] != null)
            {
                MoveObjects(battleObjects[i].transform, directions[i], speeds[i]);

                if (isVictory || isDefeat)
                {
                    ScaleObjects(battleObjects[i].transform, 0.01f);
                }

                KillObjects(battleObjects[i], startTimes[i], lifeSpans[i]);                
            }
        }
        
        if(battleManager.enemyHealthBar && battleManager.currentEnemy)
        {
            battleManager.enemyHealthBar.position = battleManager.currentEnemy.angryNoise.transform.position;
        }

        SetRangeText();
        GrowEnemy();
        ReplenishHealth();

        if(!hasPlayedFirstTime)
        {
            battleBottle.isLocked = true;
        }
        else
        {
            battleBottle.isLocked = false;
        }
    }

    void GrowEnemy()
    {
        if (battleManager.currentEnemy)
        {
            float growth = (Time.deltaTime / 100f) * currentGrowthRatio;
            var enemyScale = battleManager.currentEnemy.scaleTransform.localScale;

            if (!pauseGrowth)
            {
                if(battleManager.currentEnemy.scaleTransform.localScale.x < 2)
                    battleManager.currentEnemy.scaleTransform.localScale += new Vector3(growth, growth, growth);

                if (battleManager.currentEnemy.scaleTransform.localScale.x > enemyMaxSize)
                {
                    //StartLose();
                    //pauseGrowth = true;
                }
            }

            var modifier1 = 1 - (enemyScale.x / enemyMaxSize);
            var modifier2 = 1 - (enemyScale.x / enemyMaxSize);

            var angryNoise = battleManager.currentEnemy.angryNoise.properties[0];
            angryNoise.multiplier = Mathf.Clamp(modifier1 / 3f, 0f, 10f);
            //angryNoise.noiseSpeed = new Vector2(Mathf.Clamp((modifier1 * 12f) - 2f, 0f, 20f), Mathf.Clamp((modifier1 * 8f) - 1f, 0f, 20f) + 1f);

            var generalNoise = battleManager.currentEnemy.generalNoise.properties[0];
            generalNoise.multiplier = Mathf.Clamp(modifier2 / 2f, 0f, 10f);
            //generalNoise.noiseSpeed = new Vector2(Mathf.Clamp((modifier2 * 8f) - 3, 0f, 20f), Mathf.Clamp((modifier2 * 4f) - 1f, 0f, 20f) + 1f);
        }
    }

    void ReplenishHealth()
    {
        if (battleManager.currentEnemy && battleManager.enemyHealthBar)
        {
            float replenish = (Time.deltaTime / 200) * currentGrowthRatio;

            if (!pauseGrowth)
            {
                enemyCurrentHealth += replenish;

                if (battleManager.enemyHealthImage.fillAmount >= 1f)
                {
                    StartLose();
                    pauseGrowth = true;
                }
            }

            battleManager.enemyHealthImage.fillAmount = enemyCurrentHealth * 0.5f;
        }
    }

    void SetRangeText()
    {
        int minVal = currentValueRange.x;
        int maxVal = currentValueRange.y;
        string minText = minVal.ToString();
        if (minVal < 0)
            minText = "(" + minVal.ToString() + ")";
        string maxText = maxVal.ToString();
        if (maxVal < 0)
            maxText = "(" + maxVal.ToString() + ")";

        if(currentSlotOrder == SlotOrders.Descending)
            currentRangeText.text = maxText + " → " + minText;
        else if(currentSlotOrder == SlotOrders.Ascending)
            currentRangeText.text = minText + " → " + maxText;

        currentRangeTextCopy.text = currentRangeText.text;
    }

    void SetCurrentRange()
    {
        if (round < 2)
            currentValueRange = new Vector2Int(0, 20);
        else if (round >= 2 && round < 5)
        {
            int rand1 = Random.Range(-5, 6);
            int rand2 = Random.Range(-5, 6);

            currentValueRange = new Vector2Int(currentValueRange.x + rand1 - 4, currentValueRange.y + rand2);
        }
        else if (round >= 5 && round < 8)
        {
            int rand1 = Random.Range(-5, 30);
            int rand2 = Random.Range(-30, rand1);

            currentValueRange = new Vector2Int(rand2 - 4, rand1);
        }
        else if (round >= 5 && round < 8)
        {
            int rand1 = Random.Range(-5, 60);
            int rand2 = Random.Range(-60, rand1);

            currentValueRange = new Vector2Int(rand2 - 4, rand1);
        }
        else if (round >= 8 && round < 14)
        {
            int rand1 = Random.Range(-5, 60);
            int rand2 = Random.Range(-60, rand1);

            currentValueRange = new Vector2Int(rand2 - 4, rand1);
        }
        else if(round >= 14)
        {
            int rand1 = Random.Range(-5, 99);
            int rand2 = Random.Range(-99, rand1);

            currentValueRange = new Vector2Int(rand2 - 4, rand1);
        }

        StartCoroutine(RangeTextPanelAnim());
    }

    void SetFixedRange(int passedRound)
    {
        switch (firstRounds)
        {
            case 0:
                break;
            case 1:
                currentValueRange = new Vector2Int(0, 8);
                break;
            case 2:
                currentValueRange = new Vector2Int(-2, 2);
                break;
            case 3:
                currentValueRange = new Vector2Int(0, 20);
                break;
            default:
                break;

        }
    }

    void SpawnObjects()
    {
        var newObject = Instantiate(battleObjectPrefab, transform.parent);
        newObject.transform.position = battleManager.spawnerTransform.position;
        var startSize = Random.Range(sizeRange.x, sizeRange.y);
        newObject.transform.localScale = new Vector3(startSize, startSize, startSize); 
        var newObjectScript = newObject.GetComponent<BattleSortObject>();
        battleObjects.Add(newObjectScript);
        newObjectScript.meshRenderer.material = battleManager.currentEnemy.meshRenderer.material;
        if (currentSlotOrder == SlotOrders.Descending)
            newObjectScript.textMesh.color = cObjectTextDescending;
        else if(currentSlotOrder == SlotOrders.Ascending)
            newObjectScript.textMesh.color = cObjectTextAscending;
        newObjectScript.battleScript = this;
        var startTime = Time.time;
        startTimes.Add(startTime);
        var lifeSpan = Random.Range(lifeSpanRange.x, lifeSpanRange.y);
        lifeSpans.Add(lifeSpan);
        var direction = Random.insideUnitSphere.normalized;
        directions.Add(direction);
        var speed = Random.Range(speedRange.x, speedRange.y);
        speeds.Add(speed);
        var value = Random.Range(currentValueRange.x, currentValueRange.y + 1);
        values.Add(value);
        newObjectScript.value = value;
        newObjectScript.textMesh.text = value.ToString();
        Invoke("SpawnObjects", Random.Range(timeRange.x, timeRange.y));
    }

    void MoveObjects(Transform objectToMove, Vector3 direction, float speed)
    {        
        objectToMove.Translate(direction * Time.deltaTime * speed/10f, Space.Self);
    }

    void ScaleObjects(Transform objectToScale, float scaleFactor)
    {
        if(objectToScale && objectToScale.localScale.x > 0)
            objectToScale.localScale -= new Vector3(scaleFactor, scaleFactor, scaleFactor);
    }

    public void KillObjects(BattleSortObject objectToKill, float startTime, float lifeTime)
    {
        if ((Time.time - startTime) > lifeTime)
        {
            var index = battleObjects.IndexOf(objectToKill);
            battleObjects.Remove(battleObjects[index]);
            startTimes.Remove(startTimes[index]);
            lifeSpans.Remove(lifeSpans[index]);
            directions.Remove(directions[index]);
            speeds.Remove(speeds[index]);
            var effect = Instantiate(popEffect, transform.parent);
            effect.transform.position = objectToKill.transform.position;            
            Destroy(objectToKill.gameObject);           
        }
    }

    public void KillObjectClicked(BattleSortObject objectToKill)
    {
        if (AnimateNumberInst != null)
        {
            StopCoroutine(AnimateNumberInst);
            ResetAnimatedNumber();
        }

        if (battleSortDemo.gameObject.activeSelf && battleSortDemo.demoNumber < 3) // First 3 demo levels
        {
            battleSortDemo.isActive = false;
        }

        popDiamondAudio.Play();
        var testIndex = 0;

        for (int i = 0; i < battleSlots.Count; i++)
        {
            if (battleSlots[i].active == true)
            {
                testIndex = i;
            }
        }

        float timeToWait = 0.75f; // is split between lerps in IEnumerator Animate Number under

        AnimateNumberInst = StartCoroutine(AnimateNumber(objectToKill.transform.position, testIndex, objectToKill.value, timeToWait));
      
        AddToActiveSlot(objectToKill, testIndex);

        var index = battleObjects.IndexOf(objectToKill);
        battleObjects.Remove(battleObjects[index]);
        startTimes.Remove(startTimes[index]);
        lifeSpans.Remove(lifeSpans[index]);
        directions.Remove(directions[index]);
        speeds.Remove(speeds[index]);
        var effect = Instantiate(popEffect, transform.parent);
        effect.transform.position = objectToKill.transform.position;        
        Destroy(objectToKill.gameObject);
    }

    private void ResetAnimatedNumber()
    {
        animatedNumberParent.transform.position = Vector3.zero;
        animatedNumberText.color = Color.white;
        animatedNumberText.text = "";
        animatedNumberTextBg.color = Color.black;
        animatedNumberParent.alpha = 0;
        animatedNumberParent.gameObject.SetActive(false);
    }

    private IEnumerator AnimateNumber(Vector3 pos, int index, int value, float totalTime)
    {
        animatedNumberParent.transform.position = pos;
        animatedNumberText.color = Color.white;
        animatedNumberText.text = value.ToString();
        animatedNumberTextBg.color = Color.black;
        animatedNumberParent.alpha = 1;
        animatedNumberParent.gameObject.SetActive(true);

        float size = Vector3.Distance(pos, battleManager.battleCanvas.transform.position) * 0.75f;

        float t = 0;
        float lerpTime = totalTime / 3f;

        while (t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            animatedNumberParent.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * size, h);
            yield return new WaitForEndOfFrame();
        }

        float ti = 0;
        float lerpTime2 = (totalTime / 2f) + (totalTime / 3f);
        Vector3 endPos = battleSlots[index].transform.position;
        Color bgStartCol = new Color(Color.black.r, Color.black.g, Color.black.b, 1);
        Color bgEndCol = new Color(Color.black.r, Color.black.g, Color.black.b, 0.5f);
        Color startCol = Color.white;
        Color endCol = Color.white;

        while (ti < 1)
        {
            ti += Time.deltaTime / lerpTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            animatedNumberParent.transform.localScale = Vector3.Lerp(Vector3.one * size, Vector3.one * 0.25f, h);
            animatedNumberParent.transform.position = Vector3.Lerp(pos, endPos, h);
            animatedNumberText.color = Vector4.Lerp(startCol, endCol, h);
            animatedNumberTextBg.color = Vector4.Lerp(bgStartCol, bgEndCol, h);
            yield return new WaitForEndOfFrame();
        }

        animatedNumberParent.gameObject.SetActive(false);
    }

    void CheckForKill(BattleSortObject passedBattleObject) // IN USE?
    {
        foreach(var item in battleObjects)
        {
            if(item == passedBattleObject)
            {                
                KillObjects(item, 0, 0);
            }
        }
    }

    void SetActiveSlotDescending(int index)
    {
        if (index < 3)
        {
            correctAudio.PlayDelayed(0.75f);
            SetActiveSlot(index + 1);
        }
        else
            StartCoroutine(CheckAnswer());
        
    }

    void SetActiveSlotAscending(int index)
    {
        if (index > 0)
            SetActiveSlot(index - 1);
        else
            StartCoroutine(CheckAnswer());
    }

    void SetActiveSlotRandom()
    {
        var remainingSlots = new List<int>();

        for (int i = 0; i < battleSlots.Count; i++)
        {
            if (!battleSlots[i].isFilled)
                remainingSlots.Add(i);
        }

        if (remainingSlots.Count > 0)
        {
            HelperFunctions.ShuffleList(remainingSlots);
            SetActiveSlot(remainingSlots[0]);
        }
        else
            StartCoroutine(CheckAnswer());
    }
    /*
    private IEnumerator WaitForAddToActiveSlot(BattleSortObject passedBattleObject, int index, float timeToWait)
    {        
        addToActive = true;

        yield return new WaitForSeconds(timeToWait);

        AddToActiveSlot(passedBattleObject, index);
    }
    */
    void AddToActiveSlot(BattleSortObject passedBattleObject, int index)
    {
        //lastBattleSortObject = passedBattleObject;
        //lastIndex = index;

        battleSlots[index].value = passedBattleObject.value;
        battleSlots[index].valueText.text = passedBattleObject.value.ToString();
        battleSlots[index].valueText.gameObject.SetActive(true);
        battleSlots[index].textGlow.gameObject.SetActive(true);
        battleSlots[index].meshRenderer.material = battleSlots[index].activeMat;
        battleSlots[index].isFilled = true;

        SetDeactiveFilledSlot(index);


        bool check = true;

        if(index > 0 && index < 3)
        {
            check = ValidateSlot(index, check);
        }

        if (check)
        {           
            switch (currentSlotOrder)
            {
                case SlotOrders.Ascending:
                    SetActiveSlotAscending(index);
                    break;
                case SlotOrders.Descending:
                    SetActiveSlotDescending(index);
                    break;
                case SlotOrders.Random:
                    SetActiveSlotRandom();
                    break;
                default:
                    break;

            }
        }
        else // Wrong Answer Before Final Check
        {
            wrongAudio.PlayDelayed(0.75f);
            liquidAudio.PlayDelayed(0.75f);
            StartCoroutine(WrongAnswer());
        }

        //addToActive = false;
    }

    bool ValidateSlot(int passedIndex, bool passedCheck)
    {
        print("Checking index no..." + passedIndex);

        if (currentSlotOrder == SlotOrders.Descending)
        {
            if (battleSlots[passedIndex].value < battleSlots[passedIndex - 1].value)
                passedCheck = true;
            else
                passedCheck = false;
        }
        else if (currentSlotOrder == SlotOrders.Ascending)
        {
            if (battleSlots[passedIndex].value > battleSlots[passedIndex + 1].value)
                passedCheck = true;
            else
                passedCheck = false;
        }

        return passedCheck;
    }

    void ConfigureSlot(int slotIndex, int slotValue)
    {
        battleSlots[slotIndex].value = slotValue;
        battleSlots[slotIndex].valueText.text = slotValue.ToString();
        battleSlots[slotIndex].valueText.gameObject.SetActive(true);
        battleSlots[slotIndex].glowSprite.gameObject.SetActive(false);

        battleSlots[slotIndex].meshRenderer.material = battleSlots[slotIndex].activeMat;
        battleSlots[slotIndex].isFilled = true;

        battleSlots[slotIndex].rotationScript.enabled = false;
        battleSlots[slotIndex].meshRenderer.transform.localRotation = Quaternion.identity;
        battleSlots[slotIndex].scalePulseScript.enabled = false;
        battleSlots[slotIndex].meshRenderer.transform.parent.localScale = new Vector3(slotScale, slotScale, slotScale);
        battleSlots[slotIndex].active = false;
    }

    private IEnumerator WrongAnswer()
    {
        if (OnBottleChangeValue != null)
        {
            OnBottleChangeValue(0);
        }

        foreach (var item in battleSlots)
        {
            if (item.isFilled)
                item.meshRenderer.material = slotMatError;
        }

        float t = 0;
        float time = 1;

        while(t < 1)
        {
            t += Time.deltaTime / time;
            yield return new WaitForEndOfFrame();
        }

        ResetValues();
        InitializeSlots();

        if (!hasPlayedFirstTime)
        {
            SetTutorialLevel(firstRounds);
        }
    }

    void SetActiveSlot(int index)
    {
        battleSlots[index].active = true;
        battleSlots[index].glowSprite.gameObject.SetActive(true);
        activeSlot = battleSlots[index];
        activeSlot.meshRenderer.material = slotDefaultMatActive;
        activeSlot.scalePulseScript.enabled = true;
    }

    void SetDeactiveFilledSlot(int index)
    {
        battleSlots[index].glowSprite.gameObject.SetActive(false);
        battleSlots[index].rotationScript.enabled = false;
        battleSlots[index].meshRenderer.transform.localRotation = Quaternion.identity;
        battleSlots[index].scalePulseScript.enabled = false;
        battleSlots[index].meshRenderer.transform.parent.localScale = new Vector3(slotScale, slotScale, slotScale);
        battleSlots[index].active = false;
    }

    void SetDeactiveEmptySlot(int index)
    {
        battleSlots[index].active = false;
        battleSlots[index].glowSprite.gameObject.SetActive(false);
        battleSlots[index].rotationScript.enabled = true;
        battleSlots[index].value = 0;
        battleSlots[index].valueText.text = "";
        battleSlots[index].isFilled = false;
        battleSlots[index].valueText.gameObject.SetActive(false);
        battleSlots[index].textGlow.gameObject.SetActive(false);
        battleSlots[index].meshRenderer.material = slotDefaultMatInactive;
        battleSlots[index].activeMat = slotMaterials[index];

        battleSlots[index].scalePulseScript.enabled = false;
        battleSlots[index].meshRenderer.transform.parent.localScale = new Vector3(slotScale, slotScale, slotScale);
    }

    private IEnumerator RangeTextPanelAnim()
    {        
        currentRangeTextCopy.fontSize = rangeTextCopyFontSize;
        rangeTextCopyPanel.anchorMax = startAnchorMax;
        rangeTextCopyPanel.anchorMin = startAnchorMin;
        rangeTextCopyPanel.sizeDelta = startSizeDelta;
        rangeTextCopyPanel.pivot = startPivot;
        textCopyCanvasGrp.alpha = 1;
        rangeTextCopyPanel.gameObject.SetActive(true);

        float t = 0;
        float lerpTime = 0.5f;

        while(t < 1)
        {
            t += Time.deltaTime / lerpTime;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            rangeTextCopyPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1.5f);

        float ti = 0;
        float lerpTime2 = 1f;
        Vector2 pos = rangeTextCopyPanel.anchoredPosition;
        Vector2 size = rangeTextCopyPanel.sizeDelta;
        Vector2 pivot = startPivot;

        while (ti < 1)
        {
            ti += Time.deltaTime / lerpTime2;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            //rangeTextCopyPanel.anchoredPosition = Vector2.Lerp(pos, rangeTextPanel.anchoredPosition, h);
            //rangeTextCopyPanel.pivot = Vector2.Lerp(pos, rangeTextPanel.pivot, h);
            //rangeTextCopyPanel.sizeDelta = Vector2.Lerp(size, rangeTextPanel.sizeDelta, h);
            rangeTextCopyPanel.anchorMax = Vector2.Lerp(startAnchorMax, rangeTextPanel.anchorMax, h);
            rangeTextCopyPanel.anchorMin = Vector2.Lerp(startAnchorMin, rangeTextPanel.anchorMin, h);
            currentRangeTextCopy.fontSize = Mathf.Lerp(rangeTextCopyFontSize, currentRangeText.fontSize, h);
            yield return new WaitForEndOfFrame();
        }

        float tim = 0;
        float lerpTime3 = 0.5f;

        while (tim < 1)
        {
            tim += Time.deltaTime / lerpTime3;
            float h = tim;
            h = tim * tim * tim * (tim * (6f * tim - 15f) + 10f);
            textCopyCanvasGrp.alpha = Mathf.Lerp(1, 0, h);
            yield return new WaitForEndOfFrame();
        }

        rangeTextCopyPanel.gameObject.SetActive(false);
    }

    bool CheckAnswerDescending()
    {
        bool check = true;

        for (int i = 0; i < battleSlots.Count; i++)
        {
            if (i > 0)
            {
                if (battleSlots[i].value < battleSlots[i - 1].value)
                {

                }
                else
                {
                    check = false;
                }
            }
        }

        if (check)
            return true;
        else
            return false;
    }

    bool CheckAnswerAscending()
    {
        bool check = true;

        for (int i = 0; i < battleSlots.Count; i++)
        //for (int i = battleSlots.Count - 1; i > -1; i--)
        {
            if (i > 0)
            {
                if (battleSlots[i].value < battleSlots[i - 1].value)
                {

                }
                else
                {
                    check = false;
                }
            }
        }

        if (check)
            return true;
        else
            return false;
    }

    private IEnumerator CheckAnswer()
    {
        if (!maxValueReached)
        {
            battleManager.rayBlocker.gameObject.SetActive(true);
            battleBottle.glow.gameObject.SetActive(false);

            bool check = true;

            if (currentSlotOrder == SlotOrders.Descending)
                check = CheckAnswerDescending();
            if (currentSlotOrder == SlotOrders.Ascending)
                check = CheckAnswerAscending();

            int newValue = 0;

            if (check)
            {
                streakAudio.PlayDelayed(0.75f);
                newValue = CorrectAnswer(newValue);
                StartCoroutine(CorrectAnim());
            }

            if (OnBottleChangeValue != null)
            {
                OnBottleChangeValue(newValue);
            }

            liquidAudio.PlayDelayed(0.75f);

            if (!check)
            {
                wrongAudio.PlayDelayed(0.75f);                

                foreach (var item in battleSlots)
                {
                    if (item.isFilled)
                        item.meshRenderer.material = slotMatError;
                }

                yield return new WaitForSeconds(2);
                ResetValues();
                InitializeSlots();
                battleManager.rayBlocker.gameObject.SetActive(false);
               
                if(!hasPlayedFirstTime)
                {                   
                    SetTutorialLevel(firstRounds);
                }
                else
                {
                    battleBottle.glow.gameObject.SetActive(true);
                }
            }                      
        }
    }

    public int CorrectAnswer(int currentCorrectSum)
    {        
        currentCorrectSum = Mathf.Abs(battleSlots[0].value) + Mathf.Abs(battleSlots[1].value) + Mathf.Abs(battleSlots[2].value) + Mathf.Abs(battleSlots[3].value);
        currentValue += currentCorrectSum;
        //currentValue += correctValue;
        streakCounter++;

        if (streakCounter > 1)
        {
            currentBonusValue += streakBonus;

            if (currentValue >= bottleMaxValue)
            {
                currentBonusValue += bottleMaxValue;
                maxValueReached = true;
            }

            bonusText.transform.parent.gameObject.SetActive(true);
            bonusText.text = "+ " + (currentBonusValue).ToString();
        }

        return currentCorrectSum;
    }

    public void ResetValues()
    {
        currentValue = 0;
        currentBonusValue = 0;
        streakCounter = 0;
        maxValueReached = false;
        bonusText.transform.parent.gameObject.SetActive(false);
        bonusText.text = "+ " + (streakBonus * streakCounter).ToString();
    }

    private IEnumerator CorrectAnim()
    {
        bottleNoise.enabled = false;

        yield return new WaitForSeconds(0.25f);
       
        for (int i = 0; i < battleSlots.Count; i++)
        {
            yield return new WaitForSeconds(0.05f);
            battleSlots[i].valueText.gameObject.SetActive(false);
            battleSlots[i].textGlow.gameObject.SetActive(false);
            float t = 0;
            float distToBottle = Vector3.Distance(battleSlots[i].slotCollider.transform.position, battleBottle.transform.position);
            var firstPos = battleSlots[i].slotCollider.transform.position;
            var endPos = battleBottle.transform.position;

            float time = distToBottle * 0.5f;

            dropSlotAudio.Play();

            while (t < 1)
            {
                t += Time.deltaTime / time;
                float f = t;
                f = 1 - Mathf.Cos(f * Mathf.PI * 0.5f);
                battleSlots[i].slotCollider.transform.position = Vector3.Lerp(firstPos, endPos, f);
                yield return new WaitForEndOfFrame();
            }            
        }
       
        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < battleSlots.Count; i++)
        {
            battleSlots[i].slotCollider.transform.localPosition = Vector3.zero;
        }       
        
        InitializeSlots();
        battleManager.rayBlocker.gameObject.SetActive(false);       

        if (!hasPlayedFirstTime && firstRounds >= 3)
        {
            enemyCurrentHealth = 0.99f;    
            
            if(currentSlotOrder == SlotOrders.Descending)
                gameManager.playerStats.hasPlayedBattleDemoDesc = true;
            else if(currentSlotOrder == SlotOrders.Ascending)
                gameManager.playerStats.hasPlayedBattleDemoAsc = true;

            battleSortDemo.demoNumber = 3;
            battleSortDemo.isActive = true;
            battleSortDemo.gameObject.SetActive(true);

            hasPlayedFirstTime = true;
        }

        if (!hasPlayedFirstTime && firstRounds < 3)
        {
            firstRounds++;
            SetTutorialLevel(firstRounds);            
        }
        else
        {
            round++;
            SetCurrentRange();
            battleBottle.glow.gameObject.SetActive(true);
            bottleNoise.enabled = true;
        }
    }

    void InitializeSlots()
    {
        for (int i = 0; i < battleSlots.Count; i++)
        {
            SetDeactiveEmptySlot(i);
        }

        switch (currentSlotOrder)
        {
            case SlotOrders.Ascending:
                SetActiveSlot(3);
                break;
            case SlotOrders.Descending:
                SetActiveSlot(0);
                break;
            case SlotOrders.Random:
                SetActiveSlotRandom();
                break;
            default:
                break;

        }
    }

    void StartVictory()
    {
        battleManager.enemyHealthBar.gameObject.SetActive(false);
        isVictory = true;       
        StartCoroutine(Victory());
    }

    public void StartLose()
    {
        battleManager.enemyHealthBar.gameObject.SetActive(false);
        isDefeat = true;
        StartCoroutine(Defeat());
    }

    private IEnumerator Victory()
    {
        //winText.gameObject.SetActive(true);
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        CancelInvoke();
        
        battleManager.gameMangager.player.isBattle = false; //NB!?
        var pos = battleManager.currentEnemy.transform.position;
        var spawner = battleManager.currentEnemy.shrouderSpawner;
        var spawnerIndex = spawner.shrouderControllers.IndexOf(battleManager.currentEnemy);
        var offset = Vector3.up * 2;

        spawner.shrouderControllers.Remove(battleManager.currentEnemy);
        battleManager.spawnerTransform.SetParent(battleManager.battleSceneController.transform);
        battleManager.spawnerTransform.localPosition = Vector3.zero;
        
        Destroy(battleManager.currentEnemy.gameObject);

        battleManager.gameMangager.player.anim.SetTrigger("Success");

        yield return new WaitForSeconds(0.5f);
        battleManager.battleSceneController.soundTrack.Stop();
        winGameAudio2.Play();
        winGameAudio.PlayDelayed(1.5f);
        var winEffect = Instantiate(victoryEffect, pos + offset, Quaternion.identity, transform.parent);
        yield return new WaitForSeconds(0.5f);

        GameObject reward = null;

        if (!battleManager.gameMangager.playerStats.hasWonBattleSort)
        {
            //winGamePanel.gameObject.SetActive(true);

            float t = 0;

            while (t < 1)
            {
                t += Time.deltaTime / 1;
                float h = t;
                h = t * t * t * (t * (6f * t - 15f) + 10f);
                //winGamePanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
                yield return new WaitForEndOfFrame();
            }

            reward = Instantiate(trophyPrefab, pos + offset, Quaternion.identity, transform.parent);       
            var trophy = reward.GetComponent<Trophy>();            
            trophy.gameObject.SetActive(true);

            yield return new WaitForSeconds(3);

            float ti = 0;

            while (ti < 1)
            {
                ti += Time.deltaTime / 1;
                float h = ti;
                h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
                //winGamePanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
                yield return new WaitForEndOfFrame();
            }

            //winGamePanel.gameObject.SetActive(false);

            yield return new WaitForSeconds(1);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            gameManager.hudCanvas.trophyPanel.GainTrophy(trophy.trophy);
            Destroy(reward);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
            gameManager.playerStats.trophies.Add(trophy.trophy);
            yield return new WaitForSeconds(1);
        }
        else
        {
            reward = Instantiate(megaGemPrefab, pos + offset, Quaternion.identity, transform.parent);
            yield return new WaitForSeconds(3);
        }

        hasWonGame = true;
        battleManager.gameMangager.playerStats.hasWonBattleSort = true;
        gameManager.playerStats.SaveGame();
        battleManager.DeactivateBattle();
        yield return null;
    }

    private IEnumerator ShowWinPanel()
    {        
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            //winGamePanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator HideWinPanel()
    {
        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            //winGamePanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator Defeat()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        CancelInvoke();
        battleManager.gameMangager.player.isBattle = false; //NB!?  
        var pos = battleManager.currentEnemy.transform.position + (Vector3.up * 2);
        var defeatEffect = Instantiate(loseEffect, pos, Quaternion.identity, transform.parent);

        if (battleSortDemo.gameObject.activeSelf)
        {
            battleSortDemo.isActive = false;
            battleSortDemo.hasTerminated = false;
        }

        yield return new WaitForSeconds(1);

        battleManager.battleSceneController.soundTrack.Stop();
        loseGameAudio.Play();
        battleManager.gameMangager.player.anim.SetTrigger("Defeat");

        yield return new WaitForSeconds(1);

        loseGamePanel.gameObject.SetActive(true);

        if (monsterEvilLaugh)
            monsterEvilLaugh.Play();
       
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            loseGamePanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * 2, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        gameManager.hudCanvas.blackScreenFadeToBlack.gameObject.SetActive(true);

        yield return new WaitForSeconds(2);        

        /*
        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            loseGamePanel.localScale = Vector3.Lerp(Vector3.one * 2, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }
        */
        loseGamePanel.gameObject.SetActive(false);
        loseGamePanel.localScale = Vector3.zero;

        //yield return new WaitForSeconds(0.5f);

        InitializeGame();
        var exitTransform = gameManager.currentScene.exitPos;
        gameManager.player.navMeshAgent.Warp(exitTransform.position);
        gameManager.player.playerMovementController.playerMovementAgent.SetNewDestination(exitTransform.position);
        Instantiate(battleManager.exitBattleEffect, exitTransform.position, Quaternion.identity, null);
        loseGameAudio.Play();
        gameManager.hudCanvas.blackScreenFadeOut.gameObject.SetActive(true);
        gameManager.hudCanvas.blackScreenFadeToBlack.gameObject.SetActive(false);
        //battleManager.DeactivateBattle();
    }

    void SetTutorialLevel(int tutorialRound)
    {
        if (currentSlotOrder == SlotOrders.Descending)
        {
            switch (tutorialRound)
            {
                case 0:
                    battleSortDemo.demoNumber = 0;
                    battleSortDemo.isActive = true;
                    battleSortDemo.gameObject.SetActive(true);
                    SetLevel(3, new Vector2Int(1, 4), 0.05f);
                    break;
                case 1:
                    battleSortDemo.demoNumber = 1;
                    battleSortDemo.isActive = true;
                    battleSortDemo.gameObject.SetActive(true);
                    SetLevel(2, new Vector2Int(1, 4), 0.05f);
                    break;
                case 2:
                    battleSortDemo.demoNumber = 2;
                    battleSortDemo.isActive = true;
                    battleSortDemo.gameObject.SetActive(true);
                    SetLevel(0, new Vector2Int(1, 4), 0.05f);
                    break;
                case 3:
                    SetLevel(0, new Vector2Int(1, 8), 0.05f);
                    break;
                default:
                    break;
            }
        }
        else if(currentSlotOrder == SlotOrders.Ascending)
        {
            switch (tutorialRound)
            {
                case 0:
                    battleSortDemo.demoNumber = 0;
                    battleSortDemo.isActive = true;
                    battleSortDemo.gameObject.SetActive(true);
                    SetLevel(3, new Vector2Int(1, 4), 0.05f);
                    break;
                case 1:
                    battleSortDemo.demoNumber = 1;
                    battleSortDemo.isActive = true;
                    battleSortDemo.gameObject.SetActive(true);
                    SetLevel(2, new Vector2Int(1, 4), 0.05f);
                    break;
                case 2:
                    battleSortDemo.demoNumber = 2;
                    battleSortDemo.isActive = true;
                    battleSortDemo.gameObject.SetActive(true);
                    SetLevel(0, new Vector2Int(1, 4), 0.05f);
                    break;
                case 3:
                    SetLevel(0, new Vector2Int(1, 8), 0.05f);
                    break;
                default:
                    break;
            }
        }

        StartCoroutine(RangeTextPanelAnim());
    }

    void SetLevel(int slotsFilledCount, Vector2Int valueRange, float enemyHealth)
    {
        currentValueRange = valueRange;
        print("New round " + firstRounds);
        enemyCurrentHealth = enemyHealth;

        if (currentSlotOrder == SlotOrders.Descending)
        {
            for (int i = 0; i < slotsFilledCount; i++)
            {
                ConfigureSlot(i, (valueRange.y - i));
            }

            SetActiveSlot(slotsFilledCount);
        }
        else if (currentSlotOrder == SlotOrders.Ascending)
        {
            int invertCount = battleSlots.Count - slotsFilledCount;

            for (int i = battleSlots.Count - 1; i > invertCount - 1; i--)
            {
                ConfigureSlot(i, (valueRange.y - i));
            }

            SetActiveSlot(invertCount - 1);
        }
    }

    private IEnumerator DropSlotsToBottleEffect()
    {
        yield return null;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
