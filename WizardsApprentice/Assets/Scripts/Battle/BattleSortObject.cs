﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BattleSortObject : MonoBehaviour
{
    public BattleSort battleScript;
    public int id;
    public int value;
    public string text;
    public MeshRenderer meshRenderer;
    public SpriteRenderer sprite;
    public TextMeshPro textMesh;

    private void OnMouseDown()
    {
        battleScript.KillObjectClicked(this);
    }
}
