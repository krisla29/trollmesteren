﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour
{
    public GameManager gameMangager;
    public CameraSceneManager cameraSceneManager;
    public ShrouderController currentEnemy;
    public SceneController battleSceneController;
    public Canvas battleCanvas;
    public Collider rayBlocker;
    public BattleCollider battleCollider;
    public Camera battleCamera;
    public Transform spawnerTransform;
    public BattleSort battleSort;
    public Battle currentBattleScript;
    public Transform enemyHealthBar;
    public Image enemyHealthImage;
    public GameObject exitBattleEffect;
    public AudioSource exitBattleSound;

    public delegate void BattleManagerEvent(Transform battleCollider); //Sends to CameraSceneManager
    public static event BattleManagerEvent OnVictory;

    private void OnEnable()
    {
        ExitButton.OnExitButtonPress += CheckIfBattling;
    }

    private void OnDisable()
    {
        ExitButton.OnExitButtonPress -= CheckIfBattling;
    }

    void CheckIfBattling(Transform passedSceneTransform)
    {
        if(battleCanvas.gameObject.activeSelf)
        {
            if(battleSceneController.transform == passedSceneTransform)
            {
                //ExitBattle();
                if (battleSort.enabled)
                    battleSort.StartLose();
            }
        }
    }

    public void ActivateBattle()
    {
        if (gameMangager.player && currentEnemy)
        {
            var dir = (currentEnemy.transform.position - gameMangager.player.transform.position).normalized;
            var left = Vector3.Cross(dir, Vector3.up.normalized);
            var right = -left;
            var camPos = gameMangager.player.transform.position + (Vector3.up * 2) + (dir * -1) + (left * 3.5f);
            var camDir = ((currentEnemy.transform.position + Vector3.up) - camPos).normalized;
            var camRot = Quaternion.LookRotation(camDir);
            battleSceneController.startPos.position = gameMangager.player.transform.position + (new Vector3(dir.x, 0.2f, dir.y) / 2);// - playerReference.player.TransformDirection(shrouderSpawner.battleSceneController.sceneCamera.transform.forward);
            battleSceneController.startPos.rotation = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.z));
            battleSceneController.sceneCollider.gameObject.SetActive(true);
            battleSceneController.sceneCollider.transform.position = gameMangager.player.transform.position;
            battleCollider.playerInsideCollider = true;
            battleSceneController.sceneCamera.transform.position = camPos;
            battleSceneController.sceneCamera.transform.rotation = camRot;
            //enemyHealthBar.gameObject.SetActive(true);
            battleSort.currentSlotOrder = currentEnemy.shrouderSpawner.battleSortMethod;
            battleSort.enabled = true;
        }
    }

    public void DeactivateBattle()
    {
        battleCollider.gameObject.SetActive(false);       

        if(currentEnemy)
        {
            spawnerTransform.SetParent(battleSceneController.transform);
            spawnerTransform.localPosition = Vector3.zero;
            currentEnemy.scaleTransform.transform.localScale = Vector2.one * 2;
        }

        if (battleSort.isVictory)
        {
            if (OnVictory != null)
            {
                OnVictory(battleCollider.transform);
            }
        }

        battleCollider.transform.position = Vector3.zero;
        battleCamera.transform.position = Vector3.zero;
        battleCamera.transform.rotation = Quaternion.identity;
        battleSort.enabled = false;
        //enemyHealthBar.gameObject.SetActive(false);
    }

    public void ExitBattle()
    {
        print("Exiting Battle");
        /*
        battleCollider.gameObject.SetActive(false);

        if (currentEnemy)
        {
            spawnerTransform.SetParent(battleSceneController.transform);
            spawnerTransform.localPosition = Vector3.zero;
            currentEnemy.scaleTransform.transform.localScale = Vector2.one * 2;
        }

        battleCollider.transform.position = Vector3.zero;
        battleCamera.transform.position = Vector3.zero;
        battleCamera.transform.rotation = Quaternion.identity;
        battleSort.enabled = false;
        */
        if(exitBattleEffect)
            Instantiate(exitBattleEffect, battleSceneController.exitPos.position + Vector3.up, Quaternion.identity, null);

        if (exitBattleSound)
            exitBattleSound.Play();

        gameMangager.hudCanvas.blackScreenFadeOut.gameObject.SetActive(true);
    }
}
