﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottleProjectile : MonoBehaviour
{
    public bool isEmpty;
    public Transform liquid;
    public GameObject hitEffectPrefab;
    public delegate void BottleProjectileEvent(); //Sends to BattleBottle script
    public static event BottleProjectileEvent OnHitEnemy;
    public static event BottleProjectileEvent OnHitOther;
    private bool hasHit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Shrouder"))
        {
            if(OnHitEnemy != null)
            {
                OnHitEnemy();
            }

            print("HIT SHROUDER!");

            if(!isEmpty)
                Instantiate(hitEffectPrefab, transform.position, Quaternion.identity, null);

            Destroy(gameObject);
        }        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!hasHit)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("Player") || collision.gameObject.layer == LayerMask.NameToLayer("WorldObjects"))
            {
                if (OnHitOther != null)
                {
                    OnHitOther();
                }

                hasHit = true;
            }
        }
    }
}
