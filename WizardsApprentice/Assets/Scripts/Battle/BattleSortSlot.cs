﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleSortSlot : MonoBehaviour
{
    public bool active;
    public bool isFilled;
    public int value;

    public Collider slotCollider;
    public MeshRenderer meshRenderer;
    public Rigidbody rb;
    public Text valueText;
    public Image textGlow;
    public TextMeshProUGUI placeValueText;
    public SpriteRenderer glowSprite;
    public EasyRotate2 rotationScript;
    public EasyScalePulse scalePulseScript;

    public Material activeMat;

    void OnMouseDown()
    {

    }

    private void OnMouseUp()
    {
        
    }
}
