﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleCamera : MonoBehaviour
{
    public List<Transform> collisionObjects = new List<Transform>();
    private LayerMask layerMask;
    private Camera cam;
    private Ray ray;
    public float rayDist = 10f;
    public float sphereSize = 1f;

    private void OnEnable()
    {
        cam = transform.GetComponent<Camera>();
    }

    private void Update()
    {
        if (cam.transform.position != Vector3.zero)
        {
            var dir = new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z).normalized;
            ray = new Ray(cam.transform.position, dir);
            RaycastHit hit;
            layerMask = 1 << 9;

            //Checks if there are world objects in front of camera in battle scene, and hides them if there are

            if (Physics.SphereCast(ray.origin, sphereSize, ray.direction, out hit, rayDist, layerMask, QueryTriggerInteraction.Ignore))
            {
                var collisionParent = hit.collider.transform.parent;
                collisionObjects.Add(collisionParent);
                collisionParent.gameObject.SetActive(false);
            }

            Debug.DrawLine(ray.origin, ray.origin + (ray.direction * rayDist), Color.red);
        }
    }
    
    private void OnDisable()
    {
        foreach (var item in collisionObjects)
        {
            if(item)
                item.gameObject.SetActive(true);
        }

        collisionObjects.Clear();
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        if(ray.origin != Vector3.zero)
        {
            Gizmos.DrawSphere(ray.origin + (ray.direction * rayDist), sphereSize);
        }
    }
}
