﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOther : MonoBehaviour
{
    private bool isFinished = false;
    public bool triggerOnce;
    public List<Transform> linkedObjects = new List<Transform>();
    public AudioSource audioSource;
    public Collider triggerObject;
    private bool isTriggered;

    private void OnEnable()
    {
        PlayerController.OnPlayerTrigger += TriggerActivation;
    }
    private void OnDisable()
    {
        PlayerController.OnPlayerTrigger -= TriggerActivation;
    }
    private void TriggerActivation(Transform transform)
    {
        if(transform == triggerObject.transform && !isFinished)
        {
            isTriggered = true;

            if (linkedObjects.Count > 0)
            {
                foreach (var item in linkedObjects)
                {
                    if (!item.gameObject.activeSelf)
                    {
                        item.gameObject.SetActive(true);
                    }
                    else
                    {
                        item.gameObject.SetActive(false);
                    }
                }
            }
            if (audioSource)
            {               
                audioSource.Play();
            }

            if(triggerOnce)
            {
                if (triggerObject is BoxCollider)
                {
                    var t = triggerObject as BoxCollider;
                    t.size = Vector3.zero;
                }
                if (triggerObject is SphereCollider)
                {
                    var t = triggerObject as SphereCollider;
                    t.radius = 0f;
                }
                if (triggerObject is CapsuleCollider)
                {
                    var t = triggerObject as CapsuleCollider;
                    t.radius = 0f;
                }

                isFinished = true;
            }

            isTriggered = false;
        }
    }
}
