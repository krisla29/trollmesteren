﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOtherBehaviour : MonoBehaviour
{
    private bool isFinished = false;
    public MonoBehaviour linkedBehaviour;
    public bool triggerOnce;
    public AudioSource audioSource;
    public Collider triggerObject;
    private bool isTriggered;

    private void OnEnable()
    {
        PlayerController.OnPlayerTrigger += TriggerActivation;
    }
    private void OnDisable()
    {
        PlayerController.OnPlayerTrigger -= TriggerActivation;
    }
    private void TriggerActivation(Transform transform)
    {
        if (transform == triggerObject.transform && !isFinished)
        {
            isTriggered = true;

            if (linkedBehaviour)
            {
                if (!linkedBehaviour.enabled)
                {
                    linkedBehaviour.enabled = true;
                }
                else
                {
                    linkedBehaviour.enabled = false;
                }
            }
            if (audioSource)
            {
                audioSource.Play();
            }

            if (triggerOnce)
            {
                isFinished = true;
            }

            isTriggered = false;
        }
    }
}

