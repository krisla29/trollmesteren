﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    public GameManager gameManager;
    //public GameObject controlsMenu;
    //public List<Transform> canvasesWithCursor = new List<Transform>();
    public bool isCursorLocked;

    public void Start()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            if (!gameManager.isNavMeshAgent && gameManager.interactionMode != GameManager.InteractionModes.Direct)
            {
                Cursor.lockState = CursorLockMode.Locked;
                isCursorLocked = true;
            }
        }
    }

    public void Update()
    {
        /*
        if (controlsMenu.activeSelf && Input.GetKeyUp("k"))
        {
            controlsMenu.SetActive(false);
        }
        if (!controlsMenu.activeSelf && Input.GetKeyDown("k"))
        {
            controlsMenu.SetActive(true);
        } 
        */
        if(!gameManager.isNavMeshAgent && gameManager.controlsMode != GameManager.ControlsMode.Mobile && !MenuPause.gamePaused &&
                gameManager.interactionMode != GameManager.InteractionModes.Direct)
        {
            if (gameManager.currentScene != null)
            {
                if (!gameManager.currentScene.hasCursor)
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    isCursorLocked = true;
                }
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                isCursorLocked = true;
            }
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            isCursorLocked = false;
        }
    }
    public void Pause()
    {
        /*
        if (canvas.gameObject.activeInHierarchy == false)
        {
            canvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            canvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
        */
    }
    private void OnEnable()
    {
        CameraManager.OnChangeToPlayerCam += LockCursor;
        CameraManager.OnChangeToSceneCam += UnlockCursor;
    }
    private void OnDisable()
    {
        CameraManager.OnChangeToPlayerCam -= LockCursor;
        CameraManager.OnChangeToSceneCam -= UnlockCursor;
    }
    void LockCursor(Transform transform)
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            if (!gameManager.isNavMeshAgent && gameManager.interactionMode != GameManager.InteractionModes.Direct)
            {
                Cursor.lockState = CursorLockMode.Locked;
                isCursorLocked = true;
            }
        }
    }
    void UnlockCursor(Transform transform)
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            if(transform.GetComponent<SceneController>().hasCursor)
            {
                Cursor.lockState = CursorLockMode.None;
                isCursorLocked = false;
            }
        }
    }
}
