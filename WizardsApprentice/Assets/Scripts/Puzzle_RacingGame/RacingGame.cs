﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RacingGame : MonoBehaviour
{
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.RacingGame;
    public GameManager gameManager;
    public ShooterDemo joystickDemo;
    public MultipleChoiceHolder mcHolder;
    //public TrophyPanel TrophyPanel;
    public BezierCurve mainTrack;
    public Camera gameCam;
    public float taskDistOffset = 4f;
    public float mainTrackSpeedModifier = 30f;
    public float countDown = 3f;
    public float currentSpeedModifier = 30f;
    public int totalLaps = 10;
    public int nextCheckPoint;
    public bool hasAnswered;
    public bool taskDismissed;
    public bool missedLastTask;
    public int currentSubTrack; // if -1, currentTrack == mainTrack
    public int score;
    [Header("Player")]
    public Follower playerFollower;
    public Transform playerControlTransform;
    public Collider playerCollider;
    public GameObject correctEffectPrefab;
    public float playerSpeed = 1f;
    public float playerMaxSpeed = 1.6f;
    public float playerMinSpeed = 0.8f;
    public float playerFollowerSpeedMult = 5f;
    public int playerLapCounter; //ReadOnly
    public float playerMainTrackProgression; //ReadOnly
    public float playerScore; //ReadOnly
    public int playerCurrentPosition; //ReadOnly
    public List<float> scores = new List<float>();
    public BezierCurve playerCurrentTrack; //ReadOnly
    public TrackDifficulty currentDifficulty; //ReadOnly

    public enum TrackDifficulty { Easy, Normal, Hard}

    [Header("TASKS")]
    public RacingGameTask taskNormal;
    public RacingGameTask taskEasy;
    public RacingGameTask taskHard;
    public RacingGameTask currentTask; //ReadOnly
    public MultipleChoiceTask currentMC;
    public float fontSize1 = 17;
    public float fontSize2 = 13;

    [System.Serializable]
    public class SubTrack
    {
        public BezierCurve track;
        public Transform switchObject;
        public bool isNewLap;
        public TrackDifficulty SubTrackDifficulty;
        [Tooltip("Higher value == longer time to complete")]
        public float speedModifier = 30f;
    }
    [Header("SubTracks")]
    public List<SubTrack> subTracks = new List<SubTrack>();
    private List<Vector3> sliderStartPositions = new List<Vector3>();
    private List<Quaternion> sliderStartRotations = new List<Quaternion>();
    [Header("LaneSwitches")]
    public List<Transform> laneSwitches = new List<Transform>();

    [System.Serializable]
    public class EnemyRacers
    {
        public Follower enemyFollower;
        public int enemyId; //ReadOnly
        public float enemySpeed = 1f;
        public int lapCounter; //ReadOnly
        public float mainTrackProgression; //ReadOnly
        public BezierCurve enemyCurrentTrack; //ReadOnly
        public Vector2 randomSpeed = new Vector2(1f, 2f);
    }
    [Header("Enemies")]
    public List<EnemyRacers> enemyRacers = new List<EnemyRacers>();
    [Header("UI")]
    public RacingCanvas racingCanvas;
    [Header("Audio")]
    public AudioSource correctAnswerSound;
    public AudioSource wrongAnswerSound;
    public AudioSource changeLaneSound;
    public AudioSource newLapSound;
    public AudioSource winGameSound;
    public AudioSource loseGameSound;

    private Vector3 camParentStartPos;
    private Quaternion camParentStartRot;
    private Vector3 camStartPos;
    private Quaternion camStartRot;
    private Vector3 playerStartPos;
    private Quaternion playerStartRot;
    private Vector3 player_SO_position;
    private Quaternion player_SO_rotation;
    private List<Vector3> enemies_SO_positions = new List<Vector3>();
    private List<Quaternion> enemies_SO_rotations = new List<Quaternion>();

    public Transform underbossTransformTrigger;
    public Transform underbossTransformEvent;
    public CharacterSelector.PlayerCharacter unlockCharacter;

    public delegate void RacingGameEvent(); //Sends to PlayerStats
    public static event RacingGameEvent OnCorrectAnswer;
    public static event RacingGameEvent OnRacingGameEnable;
    public static event RacingGameEvent OnRacingGameDisable;

    public bool hasWonGame;

    private void Awake()
    {
        playerStartPos = playerFollower.transform.position;
        playerStartRot = playerFollower.transform.rotation;
        camParentStartPos = gameCam.transform.parent.position;
        camParentStartRot = gameCam.transform.parent.rotation;
        camStartPos = gameCam.transform.position;
        camStartRot = gameCam.transform.rotation;
        player_SO_position = mainTrack.slideObjects[0].slideObject.position;
        player_SO_rotation = mainTrack.slideObjects[0].slideObject.rotation;

        for (int i = 0; i < enemyRacers.Count; i++)
        {
            enemies_SO_positions.Add(mainTrack.slideObjects[i + 1].slideObject.position);
            enemies_SO_rotations.Add(mainTrack.slideObjects[i + 1].slideObject.rotation);
        }
    }

    private void Start()
    {
        for (int i = 0; i < subTracks.Count; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                sliderStartPositions.Add(subTracks[i].track.slideObjects[j].slideObject.transform.position);
                sliderStartRotations.Add(subTracks[i].track.slideObjects[j].slideObject.transform.rotation);
            }
        }

        for (int i = 0; i < enemyRacers.Count; i++)
        {
            scores.Add(0);
        }

        scores.Add(0); //Player
    }
    private void OnEnable()
    {
        racingCanvas.trophyPanel.gameObject.SetActive(false);
        joystickDemo.gameObject.SetActive(true);
        InitializeGame();

        for (int i = 0; i < enemyRacers.Count; i++)
        {          
            StartCoroutine(EnemiesWaitForStart(enemyRacers[i]));
        }
        
        StartCoroutine(PlayerWaitForStart());
       
        TriggerByReference.OnTrigByRefEnter += SwitchToSubTrack;
        TriggerByReference.OnTrigByRefEnter += CheckAnswer;

        if (OnRacingGameEnable != null)
            OnRacingGameEnable();
    }


    private void OnDisable()
    {
        TriggerByReference.OnTrigByRefEnter -= SwitchToSubTrack;
        TriggerByReference.OnTrigByRefEnter -= CheckAnswer;
        StopAllCoroutines();
        CancelInvoke();
        ResetTracks();
        //InitializeGame();

        if (OnRacingGameDisable != null)
            OnRacingGameDisable();
    }

    void InitializeGame()
    {
        taskEasy.gameObject.SetActive(false);
        taskNormal.gameObject.SetActive(false);
        taskHard.gameObject.SetActive(false);

        currentMC = null;
        currentTask = null;

        for (int i = 0; i < enemyRacers.Count; i++)
        {
            enemyRacers[i].enemyCurrentTrack = mainTrack;
            enemyRacers[i].enemyId = i + 1;
            enemyRacers[i].enemySpeed = 1f;
            enemyRacers[i].lapCounter = 0;
            
            //enemyRacers[i].mainTrackProgression = 0;
            int pathProgression = 20 - (i * 5);
            enemyRacers[i].mainTrackProgression = pathProgression; //Test!!!!!!!!
            //StartCoroutine(EnemiesWaitForStart(enemyRacers[i]));
            UpdateLapCounter(i + 1);
            mainTrack.slideObjects[i + 1].currentControlPoint = 0;
            mainTrack.slideObjects[i + 1].pathProgression = pathProgression;
            mainTrack.slideObjects[i + 1].slideObject.position = enemies_SO_positions[i];
            mainTrack.slideObjects[i + 1].slideObject.rotation = enemies_SO_rotations[i];
            enemyRacers[i].enemyFollower.target = mainTrack.slideObjects[i + 1].slideObject;
        }

        hasAnswered = false;
        taskDismissed = false;
        missedLastTask = false;
        playerSpeed = 1f;
        playerLapCounter = 0;
        playerCurrentTrack = mainTrack;
        mainTrack.slideObjects[0].currentControlPoint = 0;
        mainTrack.slideObjects[0].pathProgression = 0;
        playerMainTrackProgression = 0;
        UpdateLapCounter(0);
        StartCoroutine(PlayerWaitForStart());
        currentDifficulty = TrackDifficulty.Normal;
        nextCheckPoint = playerCurrentTrack.slideObjects[0].currentControlPoint + 1;
        playerCurrentPosition = 4;

        mainTrack.slideObjects[0].slideObject.position = player_SO_position;
        mainTrack.slideObjects[0].slideObject.rotation = player_SO_rotation;

        playerFollower.target = mainTrack.slideObjects[0].slideObject;

        playerFollower.transform.position = playerStartPos;
        playerFollower.transform.rotation = playerStartRot;
        gameCam.transform.parent.position = camParentStartPos;
        gameCam.transform.parent.rotation = camParentStartRot;
        gameCam.transform.position = camStartPos;
        gameCam.transform.rotation = camStartRot;        
    }

    void ResetTracks()
    {
        for (int i = 0; i < subTracks.Count; i++)
        {
            for (int j = 0; j < subTracks[i].track.slideObjects.Count; j++)
            {
                subTracks[i].track.slideObjects[j].currentControlPoint = 0;
                subTracks[i].track.slideObjects[j].pathProgression = 0;
            }
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }


    private void Update()
    {
        CheckPlayerPosition();
        UpdatePlayerFollowerSpeed();
    }

    void UpdatePlayerFollowerSpeed()
    {
        playerFollower.followProperty01.speed = ((playerSpeed * 5) * ((playerFollowerSpeedMult * 10) / currentSpeedModifier)) - 1;
        playerFollower.followProperty02.speed = ((playerSpeed * 5) * ((playerFollowerSpeedMult * 10) / currentSpeedModifier)) - 1;
    }

    private IEnumerator EnemiesWaitForStart(EnemyRacers enemy)
    {
        yield return new WaitForSeconds(countDown);

        StartCoroutine(MoveEnemies(enemy, false));
    }

    private IEnumerator PlayerWaitForStart()
    {
        yield return new WaitForSeconds(countDown);

        StartCoroutine(MovePlayer());
    }

    private IEnumerator MoveEnemiesInitialSubTrack(EnemyRacers enemy)
    {
        float t = 0;
        var enemyProg = enemy.mainTrackProgression;
        var playerProg = playerMainTrackProgression;

        while(t < 1)
        {
            t += Time.deltaTime;
            playerCurrentTrack.slideObjects[enemy.enemyId].pathProgression = Mathf.Lerp(0, Mathf.Clamp(enemyProg - playerProg, 0f, 100f), t);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(MoveEnemies(enemy, true));
    }
    private IEnumerator MoveEnemies(EnemyRacers enemy, bool isSubTrack)
    {
        float t = 0;
        var randomizer = Random.Range(enemy.randomSpeed.x, enemy.randomSpeed.y);
        bool isFirstFrame = true;

        while(t < 1)
        {
            if (isFirstFrame) // insert coroutine for nice lerp to current initialposition subTrack
            {
                if (isSubTrack)
                    t = Mathf.Clamp(((enemy.mainTrackProgression - playerMainTrackProgression) / 100f), 0f, 1f);
                else
                    t = enemy.mainTrackProgression / 100f;

                isFirstFrame = false;
            }

            t += Time.deltaTime / ((randomizer/enemy.enemySpeed) * currentSpeedModifier);
            enemy.enemyCurrentTrack.slideObjects[enemy.enemyId].pathProgression = Mathf.Lerp(0, 100, t);

            if(enemy.enemyCurrentTrack == mainTrack)
                enemy.mainTrackProgression = enemy.enemyCurrentTrack.slideObjects[enemy.enemyId].pathProgression;

            yield return new WaitForEndOfFrame();
        }

        if (enemy.enemyCurrentTrack == mainTrack)
        {
            enemy.lapCounter++;
            UpdateLapCounter(enemy.enemyId);
            enemy.mainTrackProgression = 0;
        }
        else
        {
            int subTrack = 0;
            for (int i = 0; i < subTracks.Count; i++)
            {
                if (enemy.enemyCurrentTrack == subTracks[i].track)
                {
                    subTrack = i + 1;
                }
            }
            enemy.enemyCurrentTrack = mainTrack;
            SwitchToMainTrack(enemy.enemyFollower.transform, subTrack);
        }

        StartCoroutine(MoveEnemies(enemy, false));
    }

    private IEnumerator MovePlayerInitialSubTrack()
    {
        playerFollower.enabled = false;
        float t = 0;
        var oldPos = playerFollower.transform.position;
        var oldRot = playerFollower.transform.rotation;

        while (t < 1)
        {
            t += Time.deltaTime * 10f;
            playerFollower.transform.position = Vector3.Lerp(oldPos, playerCurrentTrack.slideObjects[0].slideObject.transform.position, t);
            playerFollower.transform.rotation = Quaternion.Lerp(oldRot, Quaternion.LookRotation(playerCurrentTrack.pointPositions[1] - playerCurrentTrack.pointPositions[0]), t);
            yield return new WaitForEndOfFrame();
        }

        playerFollower.enabled = true;
        StartCoroutine(MovePlayer());
    }

    private IEnumerator MovePlayer()
    {
        float t = 0;
        bool isFirstFrame = true;

        while (t < 1)
        {
            if (playerCurrentTrack == mainTrack && isFirstFrame)
            {
                t = playerMainTrackProgression / 100f;               
            }

            isFirstFrame = false;

            t += Time.deltaTime / (currentSpeedModifier/playerSpeed);

            playerCurrentTrack.slideObjects[0].pathProgression = Mathf.Lerp(0, 100, t);

            if (playerCurrentTrack == mainTrack)
                playerMainTrackProgression = playerCurrentTrack.slideObjects[0].pathProgression;

            if (playerCurrentTrack.slideObjects[0].pathProgression > 98.5f && !taskDismissed)
                MissedLastTask();

            if(playerCurrentTrack.slideObjects[0].currentControlPoint == nextCheckPoint)
            {
                if(playerCurrentTrack.slideObjects[0].currentControlPoint != 0 && playerCurrentTrack.slideObjects[0].currentControlPoint < playerCurrentTrack.controlPoints.Count - 1)
                    GenerateNewTask();

                if (nextCheckPoint + 1 <= playerCurrentTrack.controlPoints.Count - 2)
                    nextCheckPoint++;
                else if (playerCurrentTrack != mainTrack && nextCheckPoint + 1 > playerCurrentTrack.controlPoints.Count - 2)
                {
                    if(mainTrack.slideObjects[4 + currentSubTrack].currentControlPoint == playerCurrentTrack.controlPoints.Count - 2)
                        nextCheckPoint = 0;
                    else
                        nextCheckPoint = mainTrack.slideObjects[4 + currentSubTrack].currentControlPoint + 1; //is slider on mainTrack at subTrack end
                }
                else if (playerCurrentTrack == mainTrack && nextCheckPoint + 1 > playerCurrentTrack.controlPoints.Count - 2)
                    nextCheckPoint = 0;

            }
            
            yield return new WaitForEndOfFrame();
        }
        if (playerLapCounter < totalLaps)
        {
            if (playerCurrentTrack == mainTrack)
            {
                playerLapCounter++;
                UpdateLapCounter(0);
                playerMainTrackProgression = 0;
            }
            else
            {
                int subTrack = 0;
                for (int i = 0; i < subTracks.Count; i++)
                {
                    if (playerCurrentTrack == subTracks[i].track)
                    {
                        subTrack = i + 1;
                    }
                }
                //playerCurrentTrack = mainTrack; // --------------------------------------------------------FIX HERE!!!!!!!!!!!!!!!!!!!!!!!!------------------------------------------------------------
                SwitchToMainTrack(playerFollower.transform, subTrack);
            }

            missedLastTask = false;
            StartCoroutine(MovePlayer());
        }
        else
        {
            StartCoroutine(FinishGame());
        }
    }

    private IEnumerator FinishGame()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);

        bool check = false;

        racingCanvas.task.text = "";
        yield return new WaitForSeconds(3);

        racingCanvas.FadeOut(3);

        if(playerCurrentPosition == 1)
        {
            if (winGameSound)
                winGameSound.Play();

            if(!hasWonGame)
            {
                racingCanvas.trophyPanel.gameObject.SetActive(true);
                yield return new WaitForSeconds(2);
                racingCanvas.trophy.gameObject.SetActive(true);
                yield return new WaitForSeconds(3);
                gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
                gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
                yield return new WaitForSeconds(2);
                gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
                racingCanvas.trophy.gameObject.SetActive(false);
                racingCanvas.trophyPanel.gameObject.SetActive(false);
                yield return new WaitForSeconds(3);
                gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
                gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
                gameManager.playerStats.trophies.Add(trophy);
                gameManager.playerStats.hasWonRacingGame = true;
                check = true;
            }

            hasWonGame = true;

            yield return new WaitForSeconds(2);
        }
        else
        {
            if (loseGameSound)
                loseGameSound.Play();

            yield return new WaitForSeconds(2);
        }

        gameManager.playerStats.SaveGame();

        yield return new WaitForSeconds(2);

        if(check)
        {
            if (underbossTransformTrigger)
                underbossTransformTrigger.gameObject.SetActive(true);

            if (underbossTransformEvent)
                underbossTransformEvent.gameObject.SetActive(true);
        }

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
    }

    private void SwitchToSubTrack(Transform switchTransform, Transform playerColiderTransform) //triggered by playerCollider event
    {
        bool isMatch = false;

        for (int i = 0; i < subTracks.Count; i++)
        {
            if(switchTransform == subTracks[i].switchObject)
            {
                StopAllCoroutines();

                if (subTracks[i].isNewLap)
                    DismissCurrentTask(true);
                else
                    DismissCurrentTask(false);

                changeLaneSound.Play();
                //currentSubTrack = i;
                //playerCurrentTrack = subTracks[i].track;
                subTracks[i].track.slideObjects[0].pathProgression = 0;
                nextCheckPoint = 1;
                playerFollower.target = subTracks[i].track.slideObjects[0].slideObject;
                //StartCoroutine(MovePlayer());

                currentDifficulty = subTracks[i].SubTrackDifficulty;
                currentSpeedModifier = subTracks[i].speedModifier;
                isMatch = true;               
                currentSubTrack = i;
                playerCurrentTrack = subTracks[i].track;
                StartCoroutine(MovePlayer());
                GenerateNewTask();
            }
        }

        if(isMatch)
        {
            for (int j = 0; j < enemyRacers.Count; j++)
            {
                enemyRacers[j].enemyFollower.target = playerCurrentTrack.slideObjects[j + 1].slideObject;
                enemyRacers[j].enemyCurrentTrack = playerCurrentTrack;
                StartCoroutine(MoveEnemiesInitialSubTrack(enemyRacers[j]));
            }
        }
    }
    
    private void SwitchToMainTrack(Transform racer, int subTrack)
    {
        if(racer == playerFollower.transform)
        {            
            if (subTracks[subTrack - 1].isNewLap)
            {
                if (playerLapCounter < totalLaps)
                {
                    playerLapCounter++;                   
                    UpdateLapCounter(0);
                }
                else
                {
                    StartCoroutine(FinishGame());
                }
            }
            if (!(subTracks[subTrack - 1].isNewLap && playerLapCounter >= totalLaps))
            {                
                currentSubTrack = -1;
                playerMainTrackProgression = mainTrack.slideObjects[3 + subTrack].pathProgression; //Since first 4 sliders are player + 3 enemies, rest are subTrack end points            
                playerFollower.target = mainTrack.slideObjects[0].slideObject;                
                currentSpeedModifier = mainTrackSpeedModifier;
                currentDifficulty = TrackDifficulty.Normal;
                nextCheckPoint = 0;
                subTracks[subTrack - 1].track.slideObjects[0].pathProgression = 0;
                subTracks[subTrack - 1].track.slideObjects[0].currentControlPoint = 0;
                subTracks[subTrack - 1].track.slideObjects[0].slideObject.transform.position = sliderStartPositions[(subTrack - 1) * 4];
                subTracks[subTrack - 1].track.slideObjects[0].slideObject.transform.rotation = sliderStartRotations[(subTrack - 1) * 4];

                if (subTracks[subTrack - 1].isNewLap)
                    DismissCurrentTask(true);
                else
                    DismissCurrentTask(false);                
            }

            playerCurrentTrack = mainTrack;
        }
       
        for (int i = 0; i < enemyRacers.Count; i++)
        {
            if(racer == enemyRacers[i].enemyFollower.transform)
            {
                for (int j = 4; j < mainTrack.slideObjects.Count; j++) //Since first 4 sliders are player + 3 enemies, rest are subTrack end points
                {                  
                    enemyRacers[i].mainTrackProgression = mainTrack.slideObjects[3 + subTrack].pathProgression;
                    enemyRacers[i].enemyFollower.target = mainTrack.slideObjects[i + 1].slideObject;
                    subTracks[subTrack - 1].track.slideObjects[i + 1].pathProgression = 0;
                    subTracks[subTrack - 1].track.slideObjects[i + 1].currentControlPoint = 0;
                    subTracks[subTrack - 1].track.slideObjects[i + 1].slideObject.transform.position = sliderStartPositions[((subTrack - 1) * 4) + i];
                    subTracks[subTrack - 1].track.slideObjects[i + 1].slideObject.transform.rotation = sliderStartRotations[((subTrack - 1) * 4) + i];
                }
                if (subTracks[subTrack - 1].isNewLap)
                {
                    enemyRacers[i].lapCounter++;
                    UpdateLapCounter(i + 1);
                }
            }
        }
    }

    void SwitchTargetToMainTrack()
    {
        playerFollower.target = mainTrack.slideObjects[0].slideObject;
    }

    private void UpdateLapCounter(int racer)
    {
        if(racer == 0)
        {
            racingCanvas.playerLapCounter.text = (playerLapCounter + 1).ToString() + "/" + (totalLaps + 1).ToString();
            racingCanvas.task.text = " # " + (playerLapCounter + 1).ToString();
            CheckLaneSwitches();

            if(playerLapCounter >= 1)
                newLapSound.Play();
        }
        for (int i = 1; i < enemyRacers.Count + 1; i++)
        {
            if(i == racer)
            {
                racingCanvas.enemyLapCounters[i - 1].text = (enemyRacers[i - 1].lapCounter + 1).ToString() + "/" + (totalLaps + 1).ToString();
            }
        }        
    }

    void CheckLaneSwitches()
    {
        Transform activeLap = null;

        for (int i = 0; i < laneSwitches.Count; i++)
        {
            if(i == playerLapCounter)
            {
                if (laneSwitches[i])
                {
                    laneSwitches[i].gameObject.SetActive(true);
                    activeLap = laneSwitches[i];
                }
            }
            else
            {
                if (laneSwitches[i])
                    if(laneSwitches[i] != activeLap)
                        laneSwitches[i].gameObject.SetActive(false);
            }
        }
    }

    void GenerateNewTask()
    {
        //if (!hasAnswered && !taskDismissed && !(playerCurrentTrack != mainTrack && playerCurrentTrack.slideObjects[0].currentControlPoint == 0) && currentTask != null)
        if (!hasAnswered && !taskDismissed && currentTask != null)
        {
            var newSpeed = Mathf.Clamp(playerSpeed - 0.2f, playerMinSpeed, playerMaxSpeed);
            playerSpeed = newSpeed;
            print("Racing Game NoAnswer!");
            racingCanvas.task.text = "...";
            wrongAnswerSound.Play();
        }
        
        if (currentTask)
        {
            currentTask.gameObject.SetActive(false);
            currentTask.transform.localPosition = Vector3.zero;
            currentTask.transform.localRotation = Quaternion.identity;
        }

        hasAnswered = false;
        taskDismissed = false;

        RacingGameTask newTask = null;
        var newPos = Vector3.zero;               

        if (currentDifficulty == TrackDifficulty.Easy)
        {
            int randNum = Random.Range(0, mcHolder.easyTasks.Count);
            currentMC = mcHolder.easyTasks[randNum];

            if (playerCurrentTrack.slideObjects[0].currentControlPoint + 1 < playerCurrentTrack.controlPoints.Count)
            {
                if (playerCurrentTrack.slideObjects[0].currentControlPoint + 1 <= playerCurrentTrack.controlPoints.Count - 1)
                {
                    int currentControlPoint = playerCurrentTrack.slideObjects[0].currentControlPoint + 1;
                    int adjustPoint = Mathf.RoundToInt((playerCurrentTrack.pointsPerSegment * currentControlPoint) - (playerCurrentTrack.pointsPerSegment / taskDistOffset));
                    newPos = playerCurrentTrack.pointPositions[adjustPoint];
                }
                else
                {
                    int adjustPoint = Mathf.RoundToInt(playerCurrentTrack.pointsPerSegment/ taskDistOffset);
                    newPos = playerCurrentTrack.pointPositions[adjustPoint];
                }

                newTask = taskEasy;                
            }            
        }
        else if (currentDifficulty == TrackDifficulty.Normal)
        {
            int randNum = Random.Range(0, mcHolder.normalTasks.Count);
            currentMC = mcHolder.normalTasks[randNum];

            if (playerCurrentTrack.slideObjects[0].currentControlPoint + 1 < playerCurrentTrack.controlPoints.Count)
            {
                if (playerCurrentTrack.slideObjects[0].currentControlPoint + 1 <= playerCurrentTrack.controlPoints.Count - 1)
                {
                    int currentControlPoint = playerCurrentTrack.slideObjects[0].currentControlPoint + 1;
                    int adjustPoint = Mathf.RoundToInt((playerCurrentTrack.pointsPerSegment * currentControlPoint) - (playerCurrentTrack.pointsPerSegment / taskDistOffset));
                    newPos = playerCurrentTrack.pointPositions[adjustPoint];
                }
                else
                {
                    int adjustPoint = Mathf.RoundToInt(playerCurrentTrack.pointsPerSegment / taskDistOffset);
                    newPos = playerCurrentTrack.pointPositions[adjustPoint];
                }

                newTask = taskNormal;
            }
        }
        else if (currentDifficulty == TrackDifficulty.Hard)
        {
            int randNum = Random.Range(0, mcHolder.hardTasks.Count);
            currentMC = mcHolder.hardTasks[randNum];

            if (playerCurrentTrack.slideObjects[0].currentControlPoint + 1 < playerCurrentTrack.controlPoints.Count)
            {
                if (playerCurrentTrack.slideObjects[0].currentControlPoint + 1 <= playerCurrentTrack.controlPoints.Count - 1)
                {
                    int currentControlPoint = playerCurrentTrack.slideObjects[0].currentControlPoint + 1;
                    int adjustPoint = Mathf.RoundToInt((playerCurrentTrack.pointsPerSegment * currentControlPoint) - (playerCurrentTrack.pointsPerSegment / taskDistOffset));
                    newPos = playerCurrentTrack.pointPositions[adjustPoint];
                }
                else
                {
                    int adjustPoint = Mathf.RoundToInt(playerCurrentTrack.pointsPerSegment / taskDistOffset);
                    newPos = playerCurrentTrack.pointPositions[adjustPoint];
                }

                newTask = taskHard;
            }
        }

        InitializeTask(currentMC, newTask);
        racingCanvas.NewQuestion(currentMC.question.ToString());
        print(playerCurrentTrack.slideObjects[0].currentControlPoint);
        newTask.transform.position = newPos;
        currentTask = newTask;
        newTask.playerCollider = playerCollider.transform;
        currentTask.gameObject.SetActive(true);
    }

    void InitializeTask(MultipleChoiceTask mc_Task, RacingGameTask rg_TaskObject)
    {
        for (int i = 0; i < rg_TaskObject.ringsColliders.Count; i++)
        {
            rg_TaskObject.collidersPlayerRefs[i].references.Add(playerCollider.transform);
        }

        int randNum = Random.Range(0, 4);
        var wrongIds = new List<int>();

        for (int i = 0; i < 4; i++)
        {
            if (i == randNum)
            {
                rg_TaskObject.ringsTexts[i].text = mc_Task.rightAnswer;
                SetFontSize(rg_TaskObject.ringsTexts[i]);
                rg_TaskObject.correctIndex = i;
            }
            else
            {
                wrongIds.Add(i);
            }            
        }

        HelperFunctions.ShuffleList(wrongIds);

        for (int i = 0; i < 3; i++)
        {
            rg_TaskObject.ringsTexts[wrongIds[i]].text = mc_Task.wrongAnswers[i];
            SetFontSize(rg_TaskObject.ringsTexts[wrongIds[i]]);            
        }
    }

    void SetFontSize(TextMeshPro textObject)
    {
        if (textObject.text.Length < 2)
            textObject.fontSize = fontSize1;
        else
            textObject.fontSize = fontSize2;
    }

    void DismissCurrentTask(bool isNewLap)
    {
        currentTask.gameObject.SetActive(false);
        if(isNewLap)
            racingCanvas.task.text = " # " + playerLapCounter.ToString();
        else
            racingCanvas.task.text = "";
        currentTask.transform.localPosition = Vector3.zero;
        currentTask.transform.localRotation = Quaternion.identity;
        taskDismissed = true;
    }

    void MissedLastTask()
    {
        if (!hasAnswered && !taskDismissed && currentTask != null)
        {
            var newSpeed = Mathf.Clamp(playerSpeed - 0.2f, playerMinSpeed, playerMaxSpeed);
            playerSpeed = newSpeed;
            print("Racing Game NoAnswer!");
            racingCanvas.task.text = "...";
            wrongAnswerSound.Play();
            currentTask.gameObject.SetActive(false);
            currentTask.transform.localPosition = Vector3.zero;
            currentTask.transform.localRotation = Quaternion.identity;
            hasAnswered = false;
            taskDismissed = true;
            //missedLastTask = true;
        }
    }

    void CheckAnswer(Transform taskAlternativeTransform, Transform playerColiderTransform) // Triggered by playerCollider
    {
        if (!hasAnswered)
        {
            if (taskAlternativeTransform == currentTask.ringsColliders[currentTask.correctIndex])
            {
                int reward = 0;

                if (currentDifficulty == TrackDifficulty.Easy)
                {
                    score++;
                    reward = 1;
                }
                else if (currentDifficulty == TrackDifficulty.Normal)
                {
                    score += 2;
                    reward = 2;
                }
                else if (currentDifficulty == TrackDifficulty.Hard)
                {
                    score += 3;
                    reward = 3;
                }

                var newSpeed = Mathf.Clamp(playerSpeed + 0.2f, playerMinSpeed, playerMaxSpeed);

                playerSpeed = newSpeed;
                print("Racing Game Correct!");
                correctAnswerSound.Play();
                racingCanvas.task.text = "* * *";
                Instantiate(correctEffectPrefab, playerControlTransform.transform);
                gameManager.playerStats.racingGameCorrect(reward);
                hasAnswered = true;
            }
            else
            {
                for (int i = 0; i < currentTask.ringsColliders.Count; i++)
                {
                    if (currentTask.ringsColliders[i] == taskAlternativeTransform && i != currentTask.correctIndex)
                    {
                        var newSpeed = Mathf.Clamp(playerSpeed - 0.2f, playerMinSpeed, playerMaxSpeed);
                        playerSpeed = newSpeed;
                        print("Racing Game Wrong!");
                        racingCanvas.task.text = "...";
                        wrongAnswerSound.Play();
                        hasAnswered = true;
                    }
                }
            }
        }
    }

    void CheckPlayerPosition()
    {
        for (int i = 0; i < scores.Count - 1; i++)
        {
            scores[i] = enemyRacers[i].lapCounter + (playerCurrentTrack.slideObjects[i + 1].pathProgression / 100f);            
        }

        scores[scores.Count - 1] = playerLapCounter + (playerCurrentTrack.slideObjects[0].pathProgression / 100f);

        int counter = 1;

        for (int i = 0; i < enemyRacers.Count; i++)
        {
            if (scores[i] >= scores[scores.Count - 1])
                counter++;
        }

        playerCurrentPosition = counter;
    }   
}
