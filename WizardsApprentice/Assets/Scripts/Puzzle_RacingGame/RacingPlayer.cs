﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacingPlayer : MonoBehaviour
{
    public RacingCanvas canvas;
    public Transform playerControlTransform;

    private Vector2 mouseDelta;

    public float lerpSpeed = 5f;
    public float horizontalDist = 3;
    public float verticalDist = 3;

    public void Update()
    {
        Method01();
    }

    void Method01()
    {
        mouseDelta = new Vector2(canvas.LeftHorizontal(), canvas.LeftVertical());
        var pos = playerControlTransform.localPosition;
        var newPos = new Vector3(mouseDelta.x * horizontalDist, mouseDelta.y * verticalDist, pos.z);

        var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * lerpSpeed);
        var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
        playerControlTransform.localPosition = Vector3.Lerp(pos, newPos, smoothLerp);
    }
}
