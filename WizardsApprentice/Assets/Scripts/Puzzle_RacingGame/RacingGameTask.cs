﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RacingGameTask : MonoBehaviour
{
    public enum Difficulty { Easy, Normal, Hard }
    public string question;
    public string correct;
    public List<string> wrongs = new List<string>();
    public MultipleChoiceTask task;
    public int correctIndex;
    public Transform playerCollider;

    public List<TextMeshPro> ringsTexts = new List<TextMeshPro>();
    public List<Transform> ringsColliders = new List<Transform>();
    public List<TriggerByReference> collidersPlayerRefs = new List<TriggerByReference>(); //SetOnly from Racing Game Script
    
    public delegate void RacingGameTaskEvent(Transform task, string questionText); // Sends to RacingCanvas
    public static event RacingGameTaskEvent OnTaskActive;
    /*
    private void OnEnable()
    {
        for (int i = 0; i < ringsColliders.Count; i++)
        {
            collidersPlayerRefs[i].references.Add(playerCollider);
        }

        int randNum = Random.Range(0, 4);
        var wrongIds = new List<int>();

        for (int i = 0; i < 4; i++)
        {
            if (i == randNum)
            {
                ringsTexts[i].text = correct;
                correctIndex = i;
            }
            else
            {
                wrongIds.Add(i);
            }
        }     

        HelperFunctions.ShuffleList(wrongIds);

        for (int i = 0; i < 3; i++)
        {
            ringsTexts[wrongIds[i]].text = wrongs[i];
        }

        if (OnTaskActive != null)
        {
            OnTaskActive(transform, question);
        }
    }
    */
}
