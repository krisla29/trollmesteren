﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RacingCanvas : MonoBehaviour
{
    public GameManager gameManager;
    public RacingGame racingGameScript;
    public Joystick camJoystick;
    public TextMeshProUGUI header;
    public TextMeshProUGUI task;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI playerLapCounter;
    public RectTransform playerPositionPanel;
    public EasyScalePulse playerPosScalePulse;
    public TextMeshProUGUI playerPositionText;
    public Color colorFirst = Color.green;
    public Color colorSecond = Color.yellow;
    public Color colorThird = Color.blue;
    public Color colorFourth = Color.red;
    public CanvasGroup fader;
    public CanvasGroup countDownGrp;
    public TextMeshProUGUI countDownText;
    public RectTransform trophyPanel;
    public RectTransform trophy;
    public List<TextMeshProUGUI> enemyLapCounters = new List<TextMeshProUGUI>();
    public Image speedBar;
    public Image speedBarBg;
    public float speedBarModifier = 1f;
    public float speedBarAdjust = 0.6f;

    private int lastPosition; //readOnly
    private Coroutine fade;
    private Coroutine count;

    //public bool isUiInteracting;

    //public List<RectTransform> uiRects = new List<RectTransform>();   

    public void Update()
    {
        //PowerBarAction();
        //IsMouseOverUIelements();
        //CheckInteraction();
        PlayerPositionPanel();
        SpeedBar();
    }

    public float LeftHorizontal()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            float r = 0.0f;
            r += Input.GetAxis("Horizontal");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            return camJoystick.Horizontal;
        }
    }

    public float LeftVertical()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            float r = 0.0f;
            r += Input.GetAxis("Vertical");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            return camJoystick.Vertical;
        }
    }

    public Vector2 LeftAxis()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            return new Vector2(LeftHorizontal(), LeftVertical());
        }
        else
        {
            return camJoystick.Direction;
        }
    }

    public void NewQuestion(string question)
    {
        task.text = question;
    }

    private void OnEnable()
    {
        //RacingGameTask.OnTaskActive += NewQuestion;
        trophyPanel.gameObject.SetActive(false);
        trophy.gameObject.SetActive(false);
        fader.gameObject.SetActive(true);
        fader.alpha = 1;
        countDownGrp.gameObject.SetActive(true);
        countDownGrp.alpha = 1;
        countDownText.transform.localScale = Vector3.one;        
        count = StartCoroutine(CountDown());
    }

    private void OnDisable()
    {
        //RacingGameTask.OnTaskActive -= NewQuestion;
        StopAllCoroutines();
        fade = null;
        count = null;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        fade = null;
        count = null;
    }

    public void SpeedBar()
    {
        var currentFill = speedBar.fillAmount;
        var convertedSpeed = Mathf.Clamp((racingGameScript.playerSpeed - speedBarAdjust) * speedBarModifier, 0f, 1f);
        //speedBar.fillAmount = convertedSpeed;
        speedBar.fillAmount = Mathf.Lerp(currentFill, convertedSpeed, Time.deltaTime);
    }

    private void PlayerPositionPanel()
    {
        if(racingGameScript.playerCurrentPosition != lastPosition)
        {
            switch(racingGameScript.playerCurrentPosition)
            {
                case (1):
                    playerPositionText.text = "1st";
                    playerPositionText.color = colorFirst;
                    playerPosScalePulse.enabled = true;
                    break;
                case (2):
                    playerPositionText.text = "2nd";
                    playerPositionText.color = colorSecond;
                    playerPosScalePulse.enabled = false;
                    playerPositionPanel.localScale = Vector3.one;
                    break;
                case (3):
                    playerPositionText.text = "3rd";
                    playerPositionText.color = colorThird;
                    playerPosScalePulse.enabled = false;
                    playerPositionPanel.localScale = Vector3.one;
                    break;
                case (4):
                    playerPositionText.text = "4th";
                    playerPositionText.color = colorFourth;
                    playerPosScalePulse.enabled = false;
                    playerPositionPanel.localScale = Vector3.one;
                    break;
                default:
                    playerPositionText.text = "4th";
                    playerPositionText.color = colorFourth;
                    playerPosScalePulse.enabled = false;
                    playerPositionPanel.localScale = Vector3.one;
                    break;
            }
        }

        lastPosition = racingGameScript.playerCurrentPosition;
    }

    public void FadeOut(float time)
    {
        if (fade != null)
            StopCoroutine(fade);

        fade = StartCoroutine(Fade(false, time));
    }

    public void FadeIn(float time)
    {
        if (fade != null)
            StopCoroutine(fade);

        fade = StartCoroutine(Fade(true, time));
    }

    private IEnumerator Fade(bool fadeIn, float time)
    {
        fader.gameObject.SetActive(true);

        float t = 0;
        float fadeTime = time;

        while(t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);

            if (fadeIn)
                fader.alpha = Mathf.Lerp(1, 0, h);
            else
                fader.alpha = Mathf.Lerp(0, 1, h);

            yield return new WaitForEndOfFrame();
        }

        if(fadeIn)
            fader.gameObject.SetActive(false);
    }

    private IEnumerator CountDown()
    {
        countDownGrp.gameObject.SetActive(true);

        int time = Mathf.RoundToInt(racingGameScript.countDown);

        for (int i = time - 1; i > -1; i--)
        {
            if(i == 0)
                fade = StartCoroutine(Fade(true, 2));

            countDownText.text = (i + 1).ToString();
            countDownText.transform.localScale = Vector3.one;
            countDownGrp.alpha = 1;
            float t = 0;

            while (t < 1)
            {
                t += Time.deltaTime;
                var h = LerpUtils.SmootherStep(t);
                countDownText.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
                countDownGrp.alpha = Mathf.Lerp(1, 0, h);
                yield return new WaitForEndOfFrame();
            }
        }       

        countDownText.text = 0.ToString();
        countDownText.transform.localScale = Vector3.zero;
        countDownGrp.alpha = 0;

        countDownGrp.gameObject.SetActive(false);
    }

    /*
    public void IsMouseOverUIelements()
    {
        var mousePosition = Input.mousePosition;
        int testInt = 0;

        for (int i = 0; i < uiRects.Count; i++)
        {
            if (uiRects[i])
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(uiRects[i], Input.mousePosition, Camera.main))
                {
                    testInt += 1;
                }
            }
        }

        if (testInt == 0)
            isUiInteracting = false;
        else
            isUiInteracting = true;
    }
    */
    /*
    private void OnMouseUp()
    {
        print("FireProjectile");
    }
    */
    /*
    void CheckInteraction()
    {
        if (shooterScript.interactionType == Shooter.InteractionTypes.Direct)
        {
            powerBar.gameObject.SetActive(false);
            powerBarBg.gameObject.SetActive(false);
        }
        if (shooterScript.interactionType == Shooter.InteractionTypes.PointRelease)
        {
            powerBar.gameObject.SetActive(true);
            powerBarBg.gameObject.SetActive(true);
        }
    }
    */
    /*
    public void LerpPowerToZero()
    {
        power = Mathf.Lerp(power, 0, Time.deltaTime);
    }
    */
}
