﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourRotate : InteractableBehaviour
{
    [SerializeField] private GameObject tempObj;
    public GameObject objectToRotate;
    public Transform worldTransform;
    public float rotationSpeed = 1f;
    public bool hasStartRotation;
    public float stepSize = 0;
    private Quaternion startRot;
    public enum RotationAxes { X, Y, Z, Free }
    public RotationAxes rotationAxis;

    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private Quaternion currentRotation;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        startRot = interactable.mainObject.transform.rotation;
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        currentRotation = interactable.mainObject.transform.rotation;
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject == transform.gameObject)
            {
                Debug.DrawLine(ray.origin, hit.point, Color.magenta);

                if (!isInitialized)
                {
                    startRot = interactable.mainObject.transform.rotation;
                }

                isInitialized = true;

                var newWorldPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);

                var newPos = newWorldPos;

                if (rotationAxis == RotationAxes.X)
                {
                    newPos = new Vector3(interactable.mainObject.transform.position.x, newWorldPos.y, newWorldPos.z);                                
                }

                if (rotationAxis == RotationAxes.Y)
                {
                    newPos = new Vector3(newWorldPos.x, interactable.mainObject.transform.position.y, newWorldPos.z);                   
                }

                if (rotationAxis == RotationAxes.Z)
                {
                    newPos = new Vector3(newWorldPos.x, newWorldPos.y, interactable.mainObject.transform.position.z);                 
                }
                if (rotationAxis == RotationAxes.Free)
                {
                    newPos = newWorldPos;                  
                }

                interactable.mainObject.transform.LookAt(newPos);
            }
        }
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        isInitialized = false;
    }

    private void OnDisable()
    {
        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        isInitialized = false;
        StopAllCoroutines();
    }
}
