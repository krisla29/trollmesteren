﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourRotateLocal : InteractableBehaviour
{
    [SerializeField] private GameObject tempObj;
    public GameObject objectToRotateY;
    public GameObject objectToRotateX;
    public Transform worldTransform;
    public float rotationSpeed = 1f;
    public bool hasStartRotation;
    public float stepSize = 0;
    private Quaternion startRot;
    private Quaternion lastRot;
    public enum RotationAxes { Axis, FreeLook, FreeAdditive }
    public RotationAxes rotationAxis;
    public Collider hitCollider;

    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        startRot = interactable.mainObject.transform.rotation;
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider == hitCollider || hit.collider.gameObject == transform.gameObject)
            {
                Debug.DrawLine(ray.origin, hit.point, Color.magenta);

                if (!isInitialized)
                {                    
                    if (rotationAxis == RotationAxes.FreeAdditive)
                    {
                        //var lookRotation = Quaternion.LookRotation(Camera.main.transform.forward);
                        //objectToRotate.transform.localRotation = lookRotation;
                        //interactable.mainObject.transform.localRotation = lastRot;
                        //var oldRot = interactable.mainObject.transform.localRotation;
                        //worldTransform.localRotation = interactable.mainObject.transform.localRotation;
                        //interactable.mainObject.transform.localRotation = oldRot;
                    }

                    startRot = interactable.mainObject.transform.localRotation;
                }

                isInitialized = true;
                   
                if (rotationAxis == RotationAxes.Axis)
                {
                    var newWorldPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                    var newPos = worldTransform.InverseTransformPoint(newWorldPos);
                    newPos = new Vector3(newPos.x, worldTransform.position.y, newPos.z);
                    var rot = Quaternion.LookRotation(newPos, worldTransform.up);

                    if (Mathf.Round(rot.eulerAngles.y) % stepSize == 0)

                    {
                        interactable.mainObject.transform.localRotation = Quaternion.Euler(0, rot.eulerAngles.y, 0);
                    }
                }
                if (rotationAxis == RotationAxes.FreeLook)
                {
                    var newWorldPos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                    var newPos = worldTransform.InverseTransformPoint(newWorldPos);
                    var rot = Quaternion.LookRotation(newPos);
                    
                    interactable.mainObject.transform.localRotation = rot;

                    //interactable.mainObject.transform.localEulerAngles = rot.eulerAngles - startRot.eulerAngles;
                }
                if (rotationAxis == RotationAxes.FreeAdditive)
                {
                    //InputSpeedRotation();
                    //MouseSpeedRotation();
                    //CamBasedAddRotation();

                    TestRotation();
                }
            }
        }
    }

    private void InputSpeedRotation()
    {
        var camDir = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z);
        var objDir = interactable.mainObject.transform.forward;
        var speedVector = Vector3.zero;

        if (Vector3.Dot(camDir, objDir) >= 0.5f)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(Input.GetAxis("Mouse Y") * rotationSpeed, -Input.GetAxis("Mouse X") * rotationSpeed, 0));
        if (Vector3.Dot(camDir, objDir) < 0.5f && Vector3.Dot(camDir, objDir) >= 0)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(0, -Input.GetAxis("Mouse X") * rotationSpeed, Input.GetAxis("Mouse Y") * rotationSpeed));
        if (Vector3.Dot(camDir, objDir) < 0 && Vector3.Dot(camDir, objDir) >= -0.5f)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(0, -Input.GetAxis("Mouse X") * rotationSpeed, -Input.GetAxis("Mouse Y") * rotationSpeed));
        if (Vector3.Dot(camDir, objDir) < -0.5f)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(-Input.GetAxis("Mouse Y") * rotationSpeed, -Input.GetAxis("Mouse X") * rotationSpeed, 0));

        var nRot = Quaternion.Euler(speedVector);
        interactable.mainObject.transform.localRotation *= nRot;
    }

    private void MouseSpeedRotation()
    {
        var camDir = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z);
        var objDir = worldTransform.forward;
        var speedVector = Vector3.zero;
        
        if (Vector3.Dot(camDir, objDir) >= 0.5f)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(Input.GetAxis("Mouse Y") * rotationSpeed * 10f, -Input.GetAxis("Mouse X") * rotationSpeed * 10f, 0));
        if (Vector3.Dot(camDir, objDir) < 0.5f && Vector3.Dot(camDir, objDir) >= 0)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(0, -Input.GetAxis("Mouse X") * rotationSpeed * 10f, -Input.GetAxis("Mouse Y") * rotationSpeed * 10f));
        if (Vector3.Dot(camDir, objDir) < 0 && Vector3.Dot(camDir, objDir) >= -0.5f)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(0, -Input.GetAxis("Mouse X") * rotationSpeed * 10f, Input.GetAxis("Mouse Y") * rotationSpeed * 10f));
        if (Vector3.Dot(camDir, objDir) < -0.5f)
            speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(-Input.GetAxis("Mouse Y") * rotationSpeed * 10f, -Input.GetAxis("Mouse X") * rotationSpeed * 10f, 0));
        
        //speedVector = interactable.mainObject.transform.InverseTransformVector(new Vector3(Input.GetAxis("Mouse Y") * rotationSpeed, -Input.GetAxis("Mouse X") * rotationSpeed, 0));

        var nRot = Quaternion.Euler(speedVector);

        if (Mathf.Round(nRot.eulerAngles.y) % stepSize == 0)
        {
            interactable.mainObject.transform.localRotation *= nRot;
        }
    }

    private void CamBasedAddRotation()
    {
        var camDirRight = new Vector3(Camera.main.transform.right.x, 0, Camera.main.transform.right.z);
        var camDirForward = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z);
        //var camDir = Camera.main.transform.forward;
        var objDir = worldTransform.forward;
        var speedVector = Vector3.zero;
      
        var newRotY = Quaternion.AngleAxis(-Input.GetAxis("Mouse X") * rotationSpeed * 10f, worldTransform.up);
        var newRotX = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * rotationSpeed * 10f, camDirRight);

        var newRot = newRotX * newRotY;
        
        interactable.mainObject.transform.rotation *= newRot;

    }

    private void TestRotation()
    {
        var heading = Camera.main.transform.position - interactable.mainObject.transform.position;
        var distance = heading.magnitude;
        var direction = heading / distance;

        var camDir = new Vector3(Camera.main.transform.parent.forward.x, 0, Camera.main.transform.forward.z);
        var objDir = worldTransform.forward;


        objectToRotateY.transform.Rotate(objectToRotateY.transform.up, -Input.GetAxis("Mouse X") * rotationSpeed * 10f);
        objectToRotateX.transform.Rotate(objectToRotateX.transform.InverseTransformDirection(direction.x, 0, direction.z), Input.GetAxis("Mouse X") * rotationSpeed * 10f);

        /*
        if (Vector3.Dot(camDir, objDir) >= 0.5f)
            interactable.mainObject.transform.Rotate(Camera.main.transform.parent.right, Input.GetAxis("Mouse Y") * rotationSpeed * 10f);
        if (Vector3.Dot(camDir, objDir) < 0.5f && Vector3.Dot(camDir, objDir) >= 0)
            interactable.mainObject.transform.Rotate(Camera.main.transform.parent.forward, Input.GetAxis("Mouse Y") * rotationSpeed * 10f);
        if (Vector3.Dot(camDir, objDir) < 0 && Vector3.Dot(camDir, objDir) >= -0.5f)
            interactable.mainObject.transform.Rotate(-Camera.main.transform.parent.right, Input.GetAxis("Mouse Y") * rotationSpeed * 10f);
        if (Vector3.Dot(camDir, objDir) < -0.5f)
            interactable.mainObject.transform.Rotate(-Camera.main.transform.parent.forward, Input.GetAxis("Mouse Y") * rotationSpeed * 10f);
        */
        //interactable.mainObject.transform.Rotate(Camera.main.transform.parent.forward, -Input.GetAxis("Mouse Y") * rotationSpeed * 10f);

        //lastRot =  objectToRotate.transform.localRotation;
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        isInitialized = false;
    }

    private void OnDisable()
    {
        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDrawGizmos()
    {
        //DrawPlane(worldTransform.position, worldTransform.up);
    }
    /*
    void DrawPlane( Vector3 position, Vector3 normal)
    {

        Vector3 v3;

        if (normal.normalized != Vector3.forward)
            v3 = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
        else
            v3 = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude; ;

        var corner0 = position + v3;
        var corner2 = position - v3;
        var q = Quaternion.AngleAxis(90.0f, normal);
        v3 = q * v3;
        var corner1 = position + v3;
        var corner3 = position - v3;

        Debug.DrawLine(corner0, corner2, Color.green);
        Debug.DrawLine(corner1, corner3, Color.green);
        Debug.DrawLine(corner0, corner1, Color.green);
        Debug.DrawLine(corner1, corner2, Color.green);
        Debug.DrawLine(corner2, corner3, Color.green);
        Debug.DrawLine(corner3, corner0, Color.green);
        Debug.DrawRay(position, normal, Color.red);
    }
    */
    //OLD ROT:--------------
    //var newPos = new Vector3(locPosMainObj.x, newLocPos.y, newLocPos.z);
    //var oldRot = interactable.mainObject.transform.localRotation;
    //var newPos = new Vector3(interactable.mainObject.transform.position.x, newWorldPos.y, newWorldPos.z);
    //var newRot = Quaternion.LookRotation(interactable.mainObject.transform.position - newPos);
    //var newRotSpeed = newRot * Quaternion.Euler(newRot.x * rotationSpeed * 10f, newRot.y, newRot.z);
    //var newRotSpeedStepped = Quaternion.Euler(Mathf.Round(newRotSpeed.eulerAngles.x), newRotSpeed.eulerAngles.y, newRotSpeed.eulerAngles.z);

    //if (Mathf.RoundToInt(newRotSpeedStepped.eulerAngles.x) % stepSize == 0)
    //{
    //interactable.mainObject.transform.localRotation = newRot;

    //} 
    //---------------------
}
