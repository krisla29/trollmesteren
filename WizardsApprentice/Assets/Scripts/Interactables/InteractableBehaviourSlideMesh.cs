﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourSlideMesh : InteractableBehaviour
{
    private GameObject tempObj;
    public Transform goal;
    public Vector3 startPos;
    private Vector3 startScale;
    public float moveSpeed = 0.3f;
    public bool rotateToNormal;
    public Vector3 slideObjectRotation = Vector3.zero;
    public List<GameObject> meshObjects = new List<GameObject>();
    public Transform lastParent;
    public Vector3 offset;
    public bool hitsMeshObject;
    public float distanceToWall = 0.5f;
    private Vector3 lastPos;
    private Quaternion lastRot;
    public bool reachedEnd;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        startScale = interactable.mainObject.transform.localScale;
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        print("picking interactable up");
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.collider.gameObject == transform.gameObject)
            {
                Debug.DrawLine(ray.origin, hit.point, Color.yellow);

                if (!isInitialized)
                {
                    tempObj = new GameObject("slideObject");
                    tempObj.transform.SetParent(interactable.mainObject.transform.parent);
                    startPos = transform.position;
                    lastPos = transform.position;
                    lastRot = transform.rotation;
                }

                isInitialized = true;
                
                tempObj.transform.position = ray.origin + Vector3.Distance(hit.point, ray.origin) * ray.direction;

                Ray nRay = new Ray(transform.position, transform.forward);
                RaycastHit nHit;

                Debug.DrawRay(nRay.origin, nRay.direction, Color.magenta);

                if (Physics.Raycast(nRay.origin, nRay.direction, out nHit))
                {
                    print(nHit.point);

                    int counter = 0;

                    foreach (var item in meshObjects)
                    {
                        if(nHit.collider.gameObject == item)
                        {
                            counter = 1;
                            hitsMeshObject = true;
                            //interactable.mainObject.transform.SetParent(null);
                            if (item != lastParent)
                            {
                                //lastRot = interactable.mainObject.transform.rotation;
                                interactable.mainObject.transform.SetParent(null);
                                //interactable.mainObject.transform.rotation = Quaternion.identity;
                                interactable.mainObject.transform.localScale = startScale;
                                //interactable.mainObject.transform.rotation = lastRot;
                                interactable.mainObject.transform.SetParent(item.transform);
                            }
                            //transform.SetParent(item.transform);
                            //tempObj.transform.SetParent(item.transform);
                            lastParent = item.transform;
                        }
                    }

                    if(counter == 0)
                    {
                        hitsMeshObject = false;
                        //lastRot = interactable.mainObject.transform.rotation;
                        interactable.mainObject.transform.SetParent(null);
                        //interactable.mainObject.transform.rotation = Quaternion.identity;
                        interactable.mainObject.transform.localScale = startScale;
                        //interactable.mainObject.transform.rotation = lastRot;
                        interactable.mainObject.transform.SetParent(lastParent);
                        //transform.SetParent(lastParent);
                        //tempObj.transform.SetParent(lastParent);
                    }
                }               

                //if(startDistance == 0)
                    //startDistance = Vector3.Distance(nHit.point, transform.position);

                if (hitsMeshObject)
                {
                    var rot = transform.rotation;
                    interactable.mainObject.transform.rotation = Quaternion.Slerp(rot, Quaternion.LookRotation(nHit.normal * -1, Vector3.up), Time.deltaTime * 10f);
                    lastRot = interactable.mainObject.transform.rotation;
                    lastPos = interactable.mainObject.transform.position;

                    TransformDirect(nHit);
                }
                else
                {
                    interactable.mainObject.transform.position = lastPos;
                    //interactable.mainObject.transform.rotation = lastRot;
                }

                //interactable.mainObject.transform.localScale = startScale;
            }
        }
    }

    private void TransformDirect(RaycastHit wallHit)
    {
        var translation = (transform.right * Input.GetAxisRaw("Mouse X") * moveSpeed) + (transform.up * Input.GetAxisRaw("Mouse Y") * moveSpeed);
        var translationInversed = Camera.main.transform.InverseTransformDirection(translation);
        interactable.mainObject.transform.position += translation;
        var zTranslation = Vector3.Lerp(transform.position, wallHit.point + (wallHit.normal * distanceToWall), Time.deltaTime * 10f);
        interactable.mainObject.transform.position = zTranslation;      
    }

    private void TransformFollow(RaycastHit pointerHit)
    {
        if (pointerHit.collider.gameObject != transform.gameObject && pointerHit.collider.gameObject != interactable.mainObject.transform.gameObject)
        {
            var translation = Vector3.Lerp(interactable.mainObject.transform.position, pointerHit.point + (pointerHit.normal * distanceToWall), Time.deltaTime * 2f);
            interactable.mainObject.transform.position = translation;
        }           
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        //startDistance = 0;
        hitsMeshObject = false;
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDisable()
    {
        //startDistance = 0;
        hitsMeshObject = false;
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDestroy()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }   
}

