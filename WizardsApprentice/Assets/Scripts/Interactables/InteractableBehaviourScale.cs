﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourScale : InteractableBehaviour
{
    //private float posY;
    //private Vector3 initialPosition;
    [SerializeField] private GameObject tempObj;
    public Transform mainCollider;
    public enum ScaleAxes { All, X, Y, Z, XY, XZ, YZ}
    public ScaleAxes scaleAxis;
    public float maxScale = 2f;
    public float minScale = 0.5f;
    public float scaleSpeed = 2f;
    private Vector3 newScale;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;
    private Transform aimIcon;
    private Vector3 aimIconCounterScale;
    private Vector3 aimScale = new Vector3(1,1,1);

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
        /*
        if(interactable.mainObject.transform.localScale != Vector3.one)
        {
            ScaleObject();
        }
        */
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        //print("picking interactable up");
        //StartCoroutine("MoveUp");
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            //if (hit.collider.gameObject == transform.gameObject)
            //{
                if (!isInitialized)
                {
                    tempObj = new GameObject("pickupObject");
                    tempObj.transform.position = interactable.mainObject.transform.position;
                    tempObj.transform.SetParent(interactable.mainObject.transform);
                    tempObj.transform.localRotation = Quaternion.identity;
                    foreach (Transform child in transform)
                    {
                        if(child.CompareTag("InteractableAim"))
                        {
                            aimIcon = child.transform;
                        }
                    }
                }

                isInitialized = true;

                tempObj.transform.position = ray.origin + Vector3.Distance(hit.point, ray.origin) * ray.direction;

                ScaleObject();
            /*
            if (scaleAxis == ScaleAxes.All)
            { 
                newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3(aimScale.x / newScaleClamped.x, aimScale.y / newScaleClamped.y, aimScale.z / newScaleClamped.z);
            }
            if (scaleAxis == ScaleAxes.X)
            {
                newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), 0, 0) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3((aimScale.x / newScaleClamped.x), aimScale.y, aimScale.z);
            }   
            if(scaleAxis == ScaleAxes.Y)
            { 
                newScale = localScale + (new Vector3(0, (mouseDelta.x + mouseDelta.y), 0) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3(aimScale.x, (aimScale.y / newScaleClamped.y), aimScale.z);
            }
            if (scaleAxis == ScaleAxes.Z)
            { 
                newScale = localScale + (new Vector3(0, 0, (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3(aimScale.x, aimScale.y, (aimScale.z / newScaleClamped.z));
            }
            if (scaleAxis == ScaleAxes.XY)
            {
                newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y), 0) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3((aimScale.x / newScaleClamped.x), (aimScale.y / newScaleClamped.y), aimScale.z);
            }
            if (scaleAxis == ScaleAxes.XZ)
            {
                newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), 0, (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3((aimScale.x / newScaleClamped.x), aimScale.y, (aimScale.z / newScaleClamped.z));
            }
            if (scaleAxis == ScaleAxes.YZ)
            {
                newScale = localScale + (new Vector3(0, (mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
                newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));
                aimIconCounterScale = new Vector3(aimScale.x, (aimScale.y / newScaleClamped.y), (aimScale.z / newScaleClamped.z));
            }
            */
            
            
        }
    }

    private void ScaleObject()
    {
        var localScale = interactable.mainObject.transform.localScale;

        var mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        var newScaleClamped = interactable.mainObject.transform.localScale;

        if (scaleAxis == ScaleAxes.All)
        {
            newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3(1f / newScaleClamped.x, 1f / newScaleClamped.y, 1f / newScaleClamped.z);
            }
        }
        if (scaleAxis == ScaleAxes.X)
        {
            newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), 0, 0) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3((1f / newScaleClamped.x), 1f, 1f);
            }
        }
        if (scaleAxis == ScaleAxes.Y)
        {
            newScale = localScale + (new Vector3(0, (mouseDelta.x + mouseDelta.y), 0) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3(1f, (1f / newScaleClamped.y), 1f);
            }
        }
        if (scaleAxis == ScaleAxes.Z)
        {
            newScale = localScale + (new Vector3(0, 0, (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3(1f, 1f, (1f / newScaleClamped.z));
            }
        }
        if (scaleAxis == ScaleAxes.XY)
        {
            newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y), 0) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3((1f / newScaleClamped.x), (1f / newScaleClamped.y), 1f);
            }
        }
        if (scaleAxis == ScaleAxes.XZ)
        {
            newScale = localScale + (new Vector3((mouseDelta.x + mouseDelta.y), 0, (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3((1f / newScaleClamped.x), 1f, (1f / newScaleClamped.z));
            }
        }
        if (scaleAxis == ScaleAxes.YZ)
        {
            newScale = localScale + (new Vector3(0, (mouseDelta.x + mouseDelta.y), (mouseDelta.x + mouseDelta.y)) * (0.1f * scaleSpeed));
            newScaleClamped = new Vector3(Mathf.Clamp(newScale.x, minScale, maxScale), Mathf.Clamp(newScale.y, minScale, maxScale), Mathf.Clamp(newScale.z, minScale, maxScale));

            foreach (Transform item in interactable.mainObject.transform)
            {
                item.transform.localScale = new Vector3(1f, (1f / newScaleClamped.y), (1f / newScaleClamped.z));
            }
        }

        interactable.mainObject.transform.localScale = newScaleClamped;
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }
        if (aimIcon)
        {
            aimIcon = null;
        }

        Destroy(tempObj);
        isInitialized = false;

        //print("putting interactable down");
    }

    private void OnDisable()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        if (aimIcon)
        {
            aimIcon = null;
        }

        Destroy(tempObj);
        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        Destroy(tempObj);
        isInitialized = false;
        StopAllCoroutines();
    }
}
