﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourSlide : InteractableBehaviour
{
    private GameObject tempObj;
    public enum SlideAxes { X, Y, Z, }
    public SlideAxes slideAxis;
    public float stepSize = 1;
    public Transform goal;
    public Vector3 startPos;
    public Transform start;
    public Transform end;
    public bool reachedEnd;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        print("picking interactable up");
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.collider.gameObject == transform.gameObject)
            {
                if (!isInitialized)
                {
                    tempObj = new GameObject("slideObject");                   
                    tempObj.transform.SetParent(interactable.mainObject.transform.parent);
                    startPos = transform.localPosition;                    
                }

                isInitialized = true;

                tempObj.transform.position = ray.origin + Vector3.Distance(hit.collider.transform.position, ray.origin) * ray.direction;
                
                var oldLocPos = interactable.mainObject.transform.localPosition;
                var newLocPos = new Vector3(tempObj.transform.localPosition.x, tempObj.transform.localPosition.y, tempObj.transform.localPosition.z);

                var startLocPos = start.localPosition;
                var endLocPos = end.localPosition;

                if (slideAxis == SlideAxes.X)
                {
                    interactable.mainObject.transform.localPosition = new Vector3(Mathf.Clamp(newLocPos.x, endLocPos.x, startLocPos.x), oldLocPos.y, oldLocPos.z);                         
                }
                if (slideAxis == SlideAxes.Y)
                {
                    interactable.mainObject.transform.localPosition = new Vector3(oldLocPos.x, Mathf.Clamp(newLocPos.y, startLocPos.y, endLocPos.y), oldLocPos.z);
                }
                if (slideAxis == SlideAxes.Z)
                {
                    interactable.mainObject.transform.localPosition = new Vector3(oldLocPos.x, oldLocPos.y, Mathf.Clamp(newLocPos.z, startLocPos.z, endLocPos.z));
                }
            }
        }
    }
    
    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }
   
    private void OnDisable()
    {      
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDestroy()
    {      
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }
}
