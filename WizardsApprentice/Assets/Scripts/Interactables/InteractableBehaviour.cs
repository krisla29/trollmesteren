﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviour : MonoBehaviour
{
    public bool lockBehaviour;

    public Interactable interactable;
    /*
    public void Update()
    {

    }
    */
    public virtual void InteractableAction()
    {

    }
}
