﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourRotateFree : InteractableBehaviour
{
    public float rotationSpeed = 1f;
    private Camera cam;
    private GameObject tempParent;
    public Transform mainObjectParent;

    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        cam = Camera.main;
        tempParent = new GameObject("rotationParent");
        tempParent.transform.parent = mainObjectParent;
        tempParent.transform.position = interactable.mainObject.transform.position;
        tempParent.transform.rotation = cam.transform.rotation;
        tempParent.transform.localScale = Vector3.one;
        interactable.mainObject.transform.parent = tempParent.transform;
    }

    private void OnMouseDrag()
    {
        var hSpeed = -Input.GetAxis("Mouse X") * rotationSpeed;
        var vSpeed = Input.GetAxis("Mouse Y") * rotationSpeed;
       
        if(tempParent)
            tempParent.transform.Rotate(vSpeed, hSpeed, 0);
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        interactable.mainObject.transform.parent = mainObjectParent;

        if (tempParent)
            Destroy(tempParent.gameObject);

        isInitialized = false;
    }

    private void OnDisable()
    {
        Invoke("ResetParent", 0);

        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        Invoke("ResetParent", 0);

        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDrawGizmos()
    {
        
    }   

    void ResetParent()
    {
        interactable.mainObject.transform.parent = mainObjectParent;

        if (tempParent)
            Destroy(tempParent.gameObject);
    }
}
