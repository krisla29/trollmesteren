﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourRotate2 : InteractableBehaviour
{
    public bool countCycles;
    public int maxCycles;
    [Tooltip("Compares if lastAngle was grater than x and currentAngle less then y")]
    public Vector2 cycleCountcompareAngles = new Vector2(270, 90);
    //public int currentCycles; // ReadOnly!!
    public int cycleCounter; // ReadOnly!!
    public float currentAngle; // ReadOnly!!
    private float lastAngle;
    public bool invertCurrentAngle;
    public float stepsFromZero; //ReadOnly!!
    public float stepsFromZeroRound; //ReadOnly!!
    public bool hasMaxSteps;
    public int maxStepsFromZero;
    public float rotationSpeed = 1f;
    public bool hasStartRotation;
    public bool useStepsOnDrag = true;
    public float stepSize = 0;
    private float currentSpeed;
    private Quaternion startRot;
    private Quaternion currentRot;
    private Quaternion newRot;
    public Transform localSpaceTransform;
    public enum InputTypes { Directional, Aim, LookPlanar }
    public InputTypes inputType = InputTypes.Directional;
    public enum InputPlanes { Horizontal, Vertical }
    public InputPlanes inputPlane;
    public bool twoAxisInput;
    public bool invertXInput;
    public bool invertYInput;
    public enum RotationAxes { X, Y, Z, Free }
    public RotationAxes rotationAxis;
    //public bool lockPositive; //May be controlled by other
    //public bool lockNegative; //May be controlled by other
    public bool invertDirection;
    public bool snapOnRelease;
    private Camera cam;

    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        startRot = interactable.mainObject.transform.rotation;

        //currentCycles = 0;
        //cycleCounter = 0;
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        cam = Camera.main;
    }

    private void OnMouseDrag()
    {
        if (inputType != InputTypes.LookPlanar)
            UpdateRotation();
        else
            UpdateRotationLook();
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        if (snapOnRelease)
        {
            if (!hasMaxSteps)
                SnapToNearestStep(false);
            else
                SnapToNearestStep(true);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void UpdateCycleCount()
    {
        if (lastAngle > cycleCountcompareAngles.x && currentAngle < cycleCountcompareAngles.y)
        {
            if (cycleCounter < maxCycles)
                cycleCounter++;
            else
                cycleCounter = 0;
        }

        if(lastAngle < cycleCountcompareAngles.y && currentAngle > cycleCountcompareAngles.x)
        {
            if (cycleCounter > 0)
                cycleCounter--;
            else
                cycleCounter = maxCycles;
        }
    }

    public void UpdateRotation()
    {
        interactable.mainObject.transform.localRotation *= RotateOnAxis(CurrentSpeed());

        if (rotationAxis != RotationAxes.Free)
        {
            UpdateCurrentAngle();
            UpdateStepsFromZero();
            SetCurrentRoundedStep();

            if (countCycles)
                UpdateCycleCount();

            lastAngle = currentAngle;
        }
    }

    public void UpdateRotationFromAngle(float angle)
    {
        interactable.mainObject.transform.localRotation = RotateOnAxis(angle);

        if (rotationAxis != RotationAxes.Free)
        {
            UpdateCurrentAngle();
            UpdateStepsFromZero();
            SetCurrentRoundedStep();

            if (countCycles)
                UpdateCycleCount();

            lastAngle = currentAngle;
        }
    }

    public void UpdateRotationLook()
    {
        var obj = interactable.mainObject.transform;
        var spaceTransform = obj.parent;

        if (localSpaceTransform)
            spaceTransform = localSpaceTransform;

        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        float dist = Vector3.Distance(ray.origin, obj.position);
        Vector3 comparePoint = ray.origin + (ray.direction * dist);
        Debug.DrawLine(ray.origin, ray.origin + (ray.direction * dist), Color.red);        
        Vector3 dir = (comparePoint - obj.position).normalized;
        Debug.DrawLine(obj.position, obj.position + (dir * 5), Color.green);

        Quaternion lookRot = Quaternion.LookRotation(spaceTransform.InverseTransformDirection(dir), spaceTransform.up);

        if (invertDirection)
            lookRot = Quaternion.LookRotation(spaceTransform.InverseTransformDirection(-dir), spaceTransform.up);

        Vector3 eulerRot = lookRot.eulerAngles;
        Vector3 finalRot = eulerRot;

        if (rotationAxis == RotationAxes.X)
            finalRot = new Vector3(eulerRot.x, 0, 0);
        else if (rotationAxis == RotationAxes.Y)
            finalRot = new Vector3(0, eulerRot.y, 0);
        else if (rotationAxis == RotationAxes.Z)
            finalRot = new Vector3(0, 0, eulerRot.z);
   
        interactable.mainObject.transform.localEulerAngles = finalRot;

        if (rotationAxis != RotationAxes.Free)
        {
            UpdateCurrentAngle();
            UpdateStepsFromZero();
            SetCurrentRoundedStep();

            if (countCycles)
                UpdateCycleCount();

            lastAngle = currentAngle;
        }
    }

    private Quaternion RotateOnAxis(float speed)
    {
        if(rotationAxis == RotationAxes.X)
        {
            currentRot = Quaternion.AngleAxis(FinalRotation(speed), interactable.mainObject.transform.InverseTransformDirection(interactable.mainObject.transform.right));
        }
        if (rotationAxis == RotationAxes.Y)
        {
            currentRot = Quaternion.AngleAxis(FinalRotation(speed), interactable.mainObject.transform.InverseTransformDirection(interactable.mainObject.transform.up));
        }
        if (rotationAxis == RotationAxes.Z)
        {
            currentRot = Quaternion.AngleAxis(FinalRotation(speed), interactable.mainObject.transform.InverseTransformDirection(interactable.mainObject.transform.forward));
        }
        if (rotationAxis == RotationAxes.Free)
        {
            currentRot = Quaternion.AngleAxis(FinalRotation(speed), interactable.mainObject.transform.InverseTransformDirection(cam.transform.right));
        }

        return currentRot;
    }

    private float FinalRotation(float speed)
    {
        var angleStepped = speed;

        if(useStepsOnDrag)
            angleStepped = Mathf.Round(speed / stepSize) * stepSize;

        return angleStepped;
    }

    void SnapOnReleaseRotation(bool hasMaxStep)
    {
        int roundedSteps;

        if (!hasMaxStep)
            roundedSteps = Mathf.RoundToInt(stepsFromZero);
        else
            roundedSteps = Mathf.RoundToInt(Mathf.Clamp(stepsFromZero, 0, maxStepsFromZero));

        var angle = roundedSteps * stepSize;
        
        Quaternion rotation = Quaternion.identity;

        if (rotationAxis == RotationAxes.X)
            interactable.mainObject.transform.localRotation = Quaternion.AngleAxis(angle, interactable.mainObject.transform.InverseTransformDirection(interactable.mainObject.transform.right));
        if (rotationAxis == RotationAxes.Y)
            interactable.mainObject.transform.localRotation = Quaternion.AngleAxis(angle, interactable.mainObject.transform.InverseTransformDirection(interactable.mainObject.transform.up));
        if (rotationAxis == RotationAxes.Z)
            interactable.mainObject.transform.localRotation = Quaternion.AngleAxis(angle, interactable.mainObject.transform.InverseTransformDirection(interactable.mainObject.transform.forward));       
    }
    /*
    private void UpdateCurrentAngle()
    {        
        var angle = 0f;

        if(rotationAxis == RotationAxes.X)
            angle = Mathf.Atan2(interactable.mainObject.transform.forward.y, interactable.mainObject.transform.forward.z) * Mathf.Rad2Deg;
        if (rotationAxis == RotationAxes.Y)
            angle = Mathf.Atan2(interactable.mainObject.transform.right.y, interactable.mainObject.transform.right.z) * Mathf.Rad2Deg;
        if (rotationAxis == RotationAxes.Z)
            angle = Mathf.Atan2(interactable.mainObject.transform.up.y, interactable.mainObject.transform.up.x) * Mathf.Rad2Deg;

        if (invertCurrentAngle)
            angle = -angle;

        if (angle < 0)
            angle += 360;

        angle = Mathf.Round((angle * 360) / 360);

        currentAngle = angle;
    }
    */
    private void UpdateCurrentAngle()
    {
        float angle = 0;

        if (rotationAxis == RotationAxes.X)
        {

            angle = Mathf.Atan2(interactable.mainObject.transform.forward.y, interactable.mainObject.transform.forward.z) * Mathf.Rad2Deg;

            if (angle < 0)
                angle += 360;

            if (invertCurrentAngle)
                angle = Mathf.Abs(360 - angle);

        }
        else if (rotationAxis == RotationAxes.Y)
        {
            angle = interactable.mainObject.transform.localEulerAngles.y;

            if (angle < 0)
                angle += 360;

            if (invertCurrentAngle)
                angle = -angle;
        }
        else if (rotationAxis == RotationAxes.Z)
        {
            angle = interactable.mainObject.transform.localEulerAngles.z;

            if (angle < 0)
                angle += 360;

            if (invertCurrentAngle)
                angle = -angle;
        }

        angle = Mathf.Round((angle * 360) / 360);

        currentAngle = angle;
    }

    private void UpdateStepsFromZero()
    {     
        stepsFromZero = (currentAngle / stepSize);
    }

    private float CurrentSpeed()
    {
        if (inputType == InputTypes.Directional)
        {
            if (inputPlane == InputPlanes.Horizontal)
            {
                if (rotationAxis == RotationAxes.X)
                {
                    if (!twoAxisInput)
                    {
                        if (!invertXInput)
                            currentSpeed = Input.GetAxis("Mouse X") * rotationSpeed;
                        else
                            currentSpeed = -Input.GetAxis("Mouse X") * rotationSpeed;
                    }
                    else
                    {
                        if (!invertXInput && !invertYInput)
                            currentSpeed = (Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (invertXInput && !invertYInput)
                            currentSpeed = -(Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (!invertXInput && invertYInput)
                            currentSpeed = (Input.GetAxis("Mouse X") + -Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (invertXInput && invertYInput)
                            currentSpeed = -(Input.GetAxis("Mouse X") + -Input.GetAxis("Mouse Y")) * rotationSpeed;
                    }
                }
                if (rotationAxis == RotationAxes.Y)
                {
                    if (!twoAxisInput)
                    {
                        if (!invertXInput)
                            currentSpeed = -Input.GetAxis("Mouse X") * rotationSpeed;
                        else
                            currentSpeed = Input.GetAxis("Mouse X") * rotationSpeed;
                    }
                    else
                    {
                        if (!invertXInput && !invertYInput)
                            currentSpeed = -(Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (invertXInput && !invertYInput)
                            currentSpeed = (Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (!invertXInput && invertYInput)
                            currentSpeed = -(Input.GetAxis("Mouse X") + -Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (invertXInput && invertYInput)
                            currentSpeed = (Input.GetAxis("Mouse X") + -Input.GetAxis("Mouse Y")) * rotationSpeed;
                    }
                }
                if (rotationAxis == RotationAxes.Z)
                {
                    if (!twoAxisInput)
                    {
                        if (!invertYInput)
                            currentSpeed = Input.GetAxis("Mouse Y") * rotationSpeed;
                        else
                            currentSpeed = -Input.GetAxis("Mouse Y") * rotationSpeed;
                    }
                    else
                    {
                        if (!invertYInput && !invertXInput)
                            currentSpeed = (Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (invertYInput && !invertXInput)
                            currentSpeed = -(Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (!invertYInput && invertXInput)
                            currentSpeed = (Input.GetAxis("Mouse Y") + -Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (invertYInput && invertXInput)
                            currentSpeed = -(Input.GetAxis("Mouse Y") + -Input.GetAxis("Mouse X")) * rotationSpeed;
                    }
                }
                if (rotationAxis == RotationAxes.Free)
                {
                    if (!invertYInput && !invertXInput)
                        currentSpeed = Input.GetAxis("Mouse Y") * Input.GetAxis("Mouse X") * rotationSpeed;
                    else if (invertYInput && !invertXInput)
                        currentSpeed = -Input.GetAxis("Mouse Y") * Input.GetAxis("Mouse X") * rotationSpeed;
                    else if (!invertYInput && invertXInput)
                        currentSpeed = Input.GetAxis("Mouse Y") * -Input.GetAxis("Mouse X") * rotationSpeed;
                    else if (invertYInput && invertXInput)
                        currentSpeed = -Input.GetAxis("Mouse Y") * -Input.GetAxis("Mouse X") * rotationSpeed;
                }
            }
            else
            {
                if (rotationAxis == RotationAxes.X)
                {
                    if (!twoAxisInput)
                    {
                        if (!invertYInput)
                            currentSpeed = Input.GetAxis("Mouse Y") * rotationSpeed;
                        else
                            currentSpeed = -Input.GetAxis("Mouse Y") * rotationSpeed;
                    }
                    else
                    {
                        if (!invertYInput && !invertXInput)
                            currentSpeed = (Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (invertYInput && !invertXInput)
                            currentSpeed = -(Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (!invertYInput && invertXInput)
                            currentSpeed = (Input.GetAxis("Mouse Y") + -Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (invertYInput && invertXInput)
                            currentSpeed = -(Input.GetAxis("Mouse Y") + -Input.GetAxis("Mouse X")) * rotationSpeed;
                    }
                }
                if (rotationAxis == RotationAxes.Y)
                {
                    if (!twoAxisInput)
                    {
                        if (!invertYInput)
                            currentSpeed = -Input.GetAxis("Mouse Y") * rotationSpeed;
                        else
                            currentSpeed = Input.GetAxis("Mouse Y") * rotationSpeed;
                    }
                    else
                    {
                        if (!invertYInput && !invertXInput)
                            currentSpeed = -(Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (invertYInput && !invertXInput)
                            currentSpeed = (Input.GetAxis("Mouse Y") + Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (!invertYInput && invertXInput)
                            currentSpeed = -(Input.GetAxis("Mouse Y") + -Input.GetAxis("Mouse X")) * rotationSpeed;
                        else if (invertYInput && invertXInput)
                            currentSpeed = (Input.GetAxis("Mouse Y") + -Input.GetAxis("Mouse X")) * rotationSpeed;
                    }
                }
                if (rotationAxis == RotationAxes.Z)
                {
                    if (!twoAxisInput)
                    {
                        if (!invertXInput)
                            currentSpeed = Input.GetAxis("Mouse X") * rotationSpeed;
                        else
                            currentSpeed = -Input.GetAxis("Mouse X") * rotationSpeed;
                    }
                    else
                    {
                        if (!invertXInput && !invertYInput)
                            currentSpeed = (Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (invertXInput && !invertYInput)
                            currentSpeed = -(Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (!invertXInput && invertYInput)
                            currentSpeed = (Input.GetAxis("Mouse X") + -Input.GetAxis("Mouse Y")) * rotationSpeed;
                        else if (invertXInput && invertYInput)
                            currentSpeed = -(Input.GetAxis("Mouse X") + -Input.GetAxis("Mouse Y")) * rotationSpeed;
                    }
                }
                if (rotationAxis == RotationAxes.Free)
                {
                    if (!invertYInput && !invertXInput)
                        currentSpeed = Input.GetAxis("Mouse Y") * Input.GetAxis("Mouse X") * rotationSpeed;
                    else if (invertYInput && !invertXInput)
                        currentSpeed = -Input.GetAxis("Mouse Y") * Input.GetAxis("Mouse X") * rotationSpeed;
                    else if (!invertYInput && invertXInput)
                        currentSpeed = Input.GetAxis("Mouse Y") * -Input.GetAxis("Mouse X") * rotationSpeed;
                    else if (invertYInput && invertXInput)
                        currentSpeed = -Input.GetAxis("Mouse Y") * -Input.GetAxis("Mouse X") * rotationSpeed;
                }
            }
        }
        else if(inputType == InputTypes.Aim)
        {
            var obj = interactable.mainObject.transform;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            float dist = Vector3.Distance(ray.origin, obj.position);
            Vector3 comparePoint = ray.origin + (ray.direction * dist);
            //Debug.DrawLine(ray.origin, ray.origin + (ray.direction * dist), Color.red);
            float mouseSpeed = Mathf.Abs(Input.GetAxis("Mouse X") + Input.GetAxis("Mouse Y"));            
            Vector3 dir = (comparePoint - obj.position).normalized;
            //Debug.DrawLine(obj.position, obj.position + (dir * 5), Color.green);
            float dirNum = AngleDir(obj.forward, dir, obj.up);
            //print(dirNum);
            currentSpeed = dirNum * (mouseSpeed * Time.deltaTime * 80) * rotationSpeed;
        }

        if (!invertDirection)
            return currentSpeed;
        else
            return -currentSpeed;
    }

    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return 1f;
        }
        else if (dir < 0f)
        {
            return -1f;
        }
        else
        {
            return 0f;
        }
    }

    void SnapToNearestStep(bool hasMaxStep)
    {
        if(rotationAxis != RotationAxes.Free)
        {
            SnapOnReleaseRotation(hasMaxStep);
            UpdateCurrentAngle();
            UpdateStepsFromZero();
            SetCurrentRoundedStep();
            //print("snapped!");
        }
    }

    public void SetCurrentRoundedStep()
    {
        stepsFromZeroRound = Mathf.RoundToInt(stepsFromZero);
    }
}
