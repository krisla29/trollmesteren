﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourDTPFree : InteractableBehaviour
{
    public bool snapOnEnable;
    public bool followsCurrentPoint;
    [Tooltip("If referenced, renderer will disable OnMouseDrag")]
    public MeshRenderer mainObjectRenderer; //If referenced, renderer will disable OnMouseDrag
    [Tooltip("If true, object will rotate to point on snap")]
    public bool autoRotate;
    public bool lockWhileUnderOther = true;
    public Transform player;
    public bool canInteractWithPlayer;
    public float minDistToPlayer = 0.5f;
    private bool isMinDistToPlayer; //readOnly
    private GameObject tempObj; //readOnly
    public GameObject tempObjPrefab; // Use if not null
    public GameObject pointObjectPrefab;
    public float pointScale = 0.25f;
    public Transform startPoint; //Required
    public Transform pointsParent; //if referenced, children will append to pointList onAwake
    public List<Transform> points = new List<Transform>();
    public float snapDistance = 1;
    public List<Transform> pointsVisited = new List<Transform>();
    public int maxCountPointsVisited = 10;
    //public Transform currentPoint; //readOnly
    public Transform lastPoint; //readOnly
    private Transform mainObjectLastPos; //readOnly
    public List<Transform> nestedPoints = new List<Transform>();
    public List<Transform> playerPoints = new List<Transform>();
    public bool drawPointsInEditor;
    private bool normalDragAllowed = true;
    private bool hasItemsOnTop; //readOnly
    private bool lastHasItemsOnTop; // readOnly
    public bool hasPlayerOnTop;
    private bool lastHasPlayerOnTop;
    //
    [Header("VERTICAL OPTIONS")] //Doesent't really work, slow and buggy, needs to be fixed
    [TextArea(2, 4)]
    public string description = "Set height ray origin points to X units directly above the surface to check," +
        "where X is the height you want to check";
    public bool useVerticalCheck;
    public List<Transform> heightRayOrigins = new List<Transform>();
    private List<Transform> currentHeightRayOrigins = new List<Transform>();
    //
    [Header("VOLUME OPTIONS")]
    public bool useVolume = false;    
    public BoxCollider thisBoxCollider;
    public GameObject mainObjectClone;
    private GameObject mainObjectChild;
    private BoxCollider boxCollider;    
    public bool ignoreBoxCenter = true;
    public List<Transform> volumeTempPoints = new List<Transform>();
    private List<Transform> lastPoints = new List<Transform>();
    public List<Transform> socketPoints = new List<Transform>();
    private List<Transform> tempSocketPoints = new List<Transform>();
    private bool volumeDragAllowed = true;

    [Header("ROTATION OPTIONS")]
    public Interactable rotationInteractable; // enables rotation if adding an Interactable base (as button), uses button event from Interactable class
    public Vector3 rotationAxis = new Vector3(0, 1, 0);
    public float rotationStep = 90f;

    private Camera cam; //readOnly
    private bool isInitialized; //readOnly
    private Rigidbody rb; //readOnly
    private bool rbIsKinematic; //readOnly
    private bool rbUseGravity; //readOnly
    private bool hasInitialized; //readOnly

    public delegate void DTPFreeEvent(Transform behaviourObject); //Sends to MazeGame script
    public static event DTPFreeEvent OnDTPFreeStart;
    public static event DTPFreeEvent OnDTPFreeDestroy;
    public static event DTPFreeEvent OnDTPFreeItemOnTop; // Sends to MazeGameItem script
    public static event DTPFreeEvent OnDTPFreeNoItem; // Sends to MazeGameItem script
    public static event DTPFreeEvent OnDTPFreePlayerOnTop; // Sends to MazeGameItem script
    public static event DTPFreeEvent OnDTPFreePlayerNotOnTop; // Sends to MazeGameItem script
    public static event DTPFreeEvent OnDTPFreeRotation; //Sends to MazeGame script
    public static event DTPFreeEvent OnDTPFreeRotationError; //Sends to MazeGame script
    public static event DTPFreeEvent OnDTPFreeDrop; //Sends to MazeGame script
    public static event DTPFreeEvent OnDTPFreePickUp; //Sends to MazeGame script
    public static event DTPFreeEvent OnDTPFreeError; //Sends to MazeGame script

    private void Awake()
    {
        if(pointsParent)
        {
            for (int i = 0; i < pointsParent.childCount; i++)
            {
                points.Add(pointsParent.GetChild(i));
            }
        }
    }

    private void Start()
    {
        if (!hasInitialized)
            Initialize();
    }

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += CheckRotation;

        if (snapOnEnable)
            Initialize();
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= CheckRotation;

        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    public void Initialize()
    {
        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        if (!useVolume)
            SnapToNearestPointInitialization();
        else
        {
            mainObjectClone = new GameObject(interactable.mainObject.name + " V Clone");
            mainObjectClone.layer = 2;
            mainObjectClone.transform.SetParent(interactable.mainObject.transform.parent);
            mainObjectClone.transform.position = interactable.mainObject.transform.position;
            mainObjectClone.transform.rotation = interactable.mainObject.transform.rotation;
            mainObjectClone.transform.localScale = interactable.mainObject.transform.localScale;

            for (int i = 0; i < socketPoints.Count; i++)
            {
                GameObject socketPoint = new GameObject("SocketPoint_" + i.ToString());
                socketPoint.transform.SetParent(mainObjectClone.transform);
                socketPoint.transform.position = socketPoints[i].position;
                socketPoint.transform.rotation = socketPoints[i].rotation;
                tempSocketPoints.Add(socketPoint.transform);
            }

            if (useVerticalCheck)
                InitializeHeightRayOrigins(mainObjectClone.transform);

            mainObjectChild = new GameObject(mainObjectClone.name + " Box Object");
            mainObjectChild.layer = 2;
            mainObjectChild.transform.SetParent(mainObjectClone.transform);
            mainObjectChild.transform.localPosition = Vector3.zero;
            mainObjectChild.transform.localEulerAngles = Vector3.zero;
            mainObjectChild.transform.localScale = Vector3.one;
            boxCollider = mainObjectChild.AddComponent<BoxCollider>();

            if (ignoreBoxCenter)
                boxCollider.center = Vector3.zero;
            else
                boxCollider.center = thisBoxCollider.center;

            boxCollider.size = thisBoxCollider.size;
            boxCollider.isTrigger = true;

            SnapToNearestPointVolumeInitialisation();
        }

        if (OnDTPFreeStart != null)
        {
            OnDTPFreeStart(transform);
        }

        ItemOnTopCheck();

        hasInitialized = true;
    }

    private void InitializeHeightRayOrigins(Transform parentTransform)
    {
        for (int i = 0; i < heightRayOrigins.Count; i++)
        {
            var newObject = new GameObject("_heightRayOrigin_" + i.ToString());
            newObject.transform.SetParent(parentTransform);
            newObject.transform.localPosition = heightRayOrigins[i].localPosition;
            newObject.transform.localRotation = Quaternion.identity;
            currentHeightRayOrigins.Add(newObject.transform);
        }
    }

    private void ClearHeightRayOrigins()
    {
        for (int i = currentHeightRayOrigins.Count - 1; i > -1; i--)
        {
            var origin = currentHeightRayOrigins[i].gameObject;
            currentHeightRayOrigins.Remove(currentHeightRayOrigins[i]);
            Destroy(origin);
        }

        currentHeightRayOrigins.Clear();
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (!lockBehaviour)
        {
            if (interactable.rigidBody != null)
            {
                rb.isKinematic = true;
                rb.useGravity = false;
            }

            cam = Camera.main;
            
            ItemOnTopCheck();

            if (!hasItemsOnTop)
            {
                if (canInteractWithPlayer)
                {
                    AllowDrag();
                }
                else
                {
                    if(!player)
                    {
                        AllowDrag();
                    }
                    else
                    {
                        PlayerOnTopCheck();

                        if(!hasPlayerOnTop)
                        {
                            AllowDrag();
                        }
                        else
                        {
                            print("hasPlayerOnTop");
                        }
                    }
                }
            }

            if(hasItemsOnTop || hasPlayerOnTop)
            {
                volumeDragAllowed = false;

                if (lockWhileUnderOther)
                    normalDragAllowed = false;

                if(useVolume || lockWhileUnderOther)
                    print("Can't move object while other object is on top!"); //insert some indication

                if(player && !canInteractWithPlayer && hasPlayerOnTop) //insert some indication
                    print("Can't moveobject while Player is on top!");

                if(OnDTPFreeError != null)
                {
                    OnDTPFreeError(transform);
                }

            }            

            if (volumeDragAllowed)
            {
                foreach (var item in volumeTempPoints)
                {
                    if (item)
                        item.gameObject.SetActive(true);
                }
            }            
            
        }
    }

    void AllowDrag()
    {
        volumeDragAllowed = true;
        normalDragAllowed = true;

        if (lastPoint)
        {
            lastPoint.gameObject.SetActive(true);
        }

        if(OnDTPFreePickUp != null)
        {
            OnDTPFreePickUp(transform);
        }
    }

    private void Update()
    {        
        if (pointsVisited.Count >= maxCountPointsVisited)
        {
            pointsVisited.Remove(pointsVisited[0]);
        }

        if (lastPoint)
        {
            if (lastPoint.position != interactable.mainObject.transform.position)
            {
                if (followsCurrentPoint)
                {
                    interactable.mainObject.transform.position = lastPoint.position;
                }
                
                else
                {
                    if (interactable.isPressed == false) //TESTCODE
                    {
                        if (!useVolume)
                            SnapToNearestPoint();
                        else
                            SnapToNearestPointVolume();
                    }
                }
                
            }
        }

        ItemOnTopCheck();
    }

    public void ItemOnTopCheck()
    {
        //CHECK FOR ITEM ON TOP
        bool check = false;

        if (nestedPoints.Count > 0)
        {
            foreach (var nestedPoint in nestedPoints)
            {
                if (!nestedPoint.gameObject.activeSelf)
                {
                    hasItemsOnTop = true;
                    check = true;
                }               
            }
        }

        if (!check)
            hasItemsOnTop = false;

        if (lastHasItemsOnTop != hasItemsOnTop)
        {
            if (hasItemsOnTop)
            {
                if (OnDTPFreeItemOnTop != null)
                    OnDTPFreeItemOnTop(transform);
            }

            if (!hasItemsOnTop)
            {
                if (OnDTPFreeNoItem != null)
                    OnDTPFreeNoItem(transform);
            }
        }

        lastHasItemsOnTop = hasItemsOnTop;
    }

    void PlayerOnTopCheck()
    {
        if (!CheckForPlayer())
            hasPlayerOnTop = false;
        else
            hasPlayerOnTop = true;

        if (lastHasPlayerOnTop != hasPlayerOnTop)
        {
            if (hasPlayerOnTop)
            {
                if (OnDTPFreePlayerOnTop != null)
                    OnDTPFreePlayerOnTop(transform);
            }

            if (!hasPlayerOnTop)
            {
                if (OnDTPFreePlayerNotOnTop != null)
                    OnDTPFreePlayerNotOnTop(transform);
            }
        }

        lastHasPlayerOnTop = hasPlayerOnTop;
    }

    bool CheckForPlayer()
    {
        bool check = false;

        var tempPoints = new List<Transform>();

        if (playerPoints.Count > 0)
        {
            for (int i = 0; i < playerPoints.Count; i++)
            {
                tempPoints.Add(playerPoints[i]);
            }
        }
        else
        {
            for (int i = 0; i < nestedPoints.Count; i++)
            {
                tempPoints.Add(nestedPoints[i]);
            }
        }

        if (tempPoints.Count > 0)
        {
            foreach (var tempPoint in playerPoints)
            {
                var dist = Vector3.Distance(tempPoint.position, player.transform.position);
                print(dist);

                if (dist < minDistToPlayer)
                {
                    hasPlayerOnTop = true;
                    check = true;
                }
            }
        }

        if (check)
            return true;
        else
            return false;
    }

    private void OnMouseDrag()
    {
        if (mainObjectRenderer)
            mainObjectRenderer.enabled = false;

        if (!lockBehaviour)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit))
            {
                if (!isInitialized)
                {
                    if (tempObjPrefab)
                    {
                        tempObj = Instantiate(tempObjPrefab);
                        tempObj.transform.rotation = interactable.mainObject.transform.rotation;
                    }
                    else
                        tempObj = new GameObject("slideObject");

                    tempObj.SetActive(true);
                    tempObj.transform.SetParent(interactable.mainObject.transform.parent);

                    if (useVerticalCheck && !useVolume)
                    {
                        currentHeightRayOrigins.Clear();
                        InitializeHeightRayOrigins(tempObj.transform);
                    }
                }

                isInitialized = true;

                tempObj.transform.position = ray.origin + Vector3.Distance(hit.point, ray.origin) * ray.direction;

                if (!useVolume)
                {
                    if (normalDragAllowed)
                        SnapToNearestPointDrag();
                }
                else
                {
                    if (volumeDragAllowed)
                        SnapToNearestPointDragVolume();
                }
            }
        }
    }

    public bool CheckDistanceToPlayer(Vector3 positionToCheck)
    {
        if (!canInteractWithPlayer)
        {
            if (player)
            {
                if (Vector3.Distance(positionToCheck, player.transform.position) < minDistToPlayer)
                {
                    return false;
                }
                else
                {
                    return true;
                }               
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    public void SnapToNearestPoint()
    {
        if(lastPoint)
            lastPoint.gameObject.SetActive(true);

        var tempList = new List<Transform>();

        for (int i = 0; i < points.Count; i++)
        {
            tempList.Add(points[i]);
        }

        if(followsCurrentPoint) //TESTCODE
            HelperFunctions.SortListByClosestToTransform(tempList, interactable.mainObject.transform);
        else
            HelperFunctions.SortListByClosestToTransform(tempList, tempObj.transform);

        Transform nearestPoint = null;

        for (int i = 0; i < tempList.Count; i++)
        {
            if(tempList[i].gameObject.activeSelf)
            {
                if(useVerticalCheck)
                {
                    if (!CheckVertical(tempList[i]))
                        nearestPoint = tempList[i];
                }
                else
                {
                    nearestPoint = tempList[i];
                }
            }
        }

        if (!nearestPoint && lastPoint)
            nearestPoint = lastPoint;
        else if(!nearestPoint && !lastPoint)
        {
            nearestPoint = interactable.mainObject.transform;
            print("Couldn't find valid snap point...");
        }
        //Transform nearestPoint = HelperFunctions.FindClosestItem(points, interactable.mainObject.transform);

        // SEND NEW POSITION EVENT:
        if (nearestPoint != mainObjectLastPos)
        {
            if (OnDTPFreeDrop != null)
            {
                OnDTPFreeDrop(transform);
            }
        }

        interactable.mainObject.transform.position = nearestPoint.position;

        //AutoRotate:
        if(autoRotate)
        {
            if(tempObj)
                tempObj.transform.rotation = nearestPoint.rotation;

            interactable.mainObject.transform.rotation = nearestPoint.rotation;
        }
        //-----------

        foreach (var item in nestedPoints)
        {
            item.gameObject.SetActive(true);
        }
        
        lastPoint = nearestPoint;
        mainObjectLastPos = nearestPoint;
        lastPoint.gameObject.SetActive(false);
    }

    public void SnapToNearestPointInitialization() // INITIALIZATION !!!
    {
        Transform nearestPoint = null;

        if (startPoint)
            nearestPoint = startPoint;
        else
            nearestPoint = HelperFunctions.FindClosestItem(points, interactable.mainObject.transform);

        interactable.mainObject.transform.position = new Vector3(nearestPoint.position.x, nearestPoint.position.y, nearestPoint.position.z);

        //AutoRotate:
        if (autoRotate)
        {
            if (tempObj)
                tempObj.transform.rotation = nearestPoint.rotation;

            interactable.mainObject.transform.rotation = nearestPoint.rotation;
        }
        //-----------

        lastPoint = nearestPoint;
        mainObjectLastPos = nearestPoint;
        lastPoint.gameObject.SetActive(false);
    }

    public void SnapToNearestPointVolume()
    {
        if (lastPoint)
            lastPoint.gameObject.SetActive(true);

        volumeTempPoints.Clear();
        //CHECK POSSIBLE POSITIONS:
        List<Transform> checkList = new List<Transform>();

        for (int i = 0; i < points.Count; i++)
        {
            if (points[i].gameObject.activeSelf == true)
            {
                mainObjectClone.transform.position = points[i].position;
                var checkBounds = true;

                checkBounds = CheckVolume(checkBounds);

                //if(useVerticalCheck)
                    //checkBounds = !CheckVertical(points[i])
                
                if (checkBounds)
                {
                    checkList.Add(points[i]);
                }
            }
        }

        //FIND NEAREST POINT:
        Transform nearestPoint = null;

        if(checkList.Count > 0)
        {
            if(followsCurrentPoint) //TestCode
                HelperFunctions.SortListByClosestToTransform(checkList, interactable.mainObject.transform);
            else
                HelperFunctions.SortListByClosestToTransform(checkList, mainObjectClone.transform);

            for (int i = 0; i < checkList.Count; i++)
            {
                if(useVerticalCheck)
                {
                    if (!CheckVertical(checkList[i]))
                        nearestPoint = checkList[i];
                }
                else
                {
                    nearestPoint = checkList[i];
                }
            }

            if(useVerticalCheck)
            {
                if (!nearestPoint && lastPoint)
                    nearestPoint = lastPoint;
                else if(!nearestPoint && !lastPoint)
                {
                    nearestPoint = interactable.transform;
                    print("Couldn't find valid snap point, not enough space above object...");
                }
            }
        }
        //Old Nearest Point:
        /*
        if (checkList.Count > 0)
        {
            nearestPoint = HelperFunctions.FindClosestItem(checkList, interactable.mainObject.transform);
        }
        */
        //Old Nearest Point End
        else if(checkList.Count <= 0 && lastPoint)
        {
            nearestPoint = lastPoint;
        }
        else if (checkList.Count <= 0 && !lastPoint)
        {
            nearestPoint = interactable.transform;
            print("Couldn't find valid snap point...");
        }
        // SEND NEW POSITION EVENT:
        if (nearestPoint != mainObjectLastPos)
        {
            if (OnDTPFreeDrop != null)
            {
                OnDTPFreeDrop(transform);
            }
        }

        //POSITION OBJECT:
        interactable.mainObject.transform.position = nearestPoint.position;
        mainObjectClone.transform.position = interactable.mainObject.transform.position;

        //AutoRotate:
        if (autoRotate)
        {
            if (tempObj)
                tempObj.transform.rotation = nearestPoint.rotation;

            if (mainObjectClone)
                mainObjectClone.transform.rotation = nearestPoint.rotation;
           
            interactable.mainObject.transform.rotation = nearestPoint.rotation;
        }
        //-----------

        //RESET NESTED POINTS:

        foreach (var item in nestedPoints)
        {
            item.gameObject.SetActive(true);
        }

        // DEACTIVATE POINTS INSIDE VOLUME
        SetAndDeactivateVolumeTempPoints();
        
        lastPoint = nearestPoint;
        mainObjectLastPos = nearestPoint;

        if (lastPoint)
            lastPoint.gameObject.SetActive(false);        
    }

    public void SnapToNearestPointVolumeInitialisation() // VOLUME INITIALISATION !!!
    {
        volumeTempPoints.Clear();

        foreach (var item in points)
        {
            if (HelperFunctions.PointInOABB(item.position, boxCollider))
            {
                if (!volumeTempPoints.Contains(item))
                    if(!nestedPoints.Contains(item))
                        volumeTempPoints.Add(item);
            }                                
        }

        Transform nearestPoint = null;

        if (startPoint)
            nearestPoint = startPoint;
        else
            nearestPoint = HelperFunctions.FindClosestItem(points, interactable.mainObject.transform);

        interactable.mainObject.transform.position = new Vector3(nearestPoint.position.x, nearestPoint.position.y, nearestPoint.position.z);
        mainObjectClone.transform.position = nearestPoint.position;

        //AutoRotate:
        if (autoRotate)
        {
            if (tempObj)
                tempObj.transform.rotation = nearestPoint.rotation;

            if (mainObjectClone)
                mainObjectClone.transform.rotation = nearestPoint.rotation;

            interactable.mainObject.transform.rotation = nearestPoint.rotation;
        }
        //-----------

        foreach (var item in volumeTempPoints)
        {
            item.gameObject.SetActive(false);
        }

        lastPoint = nearestPoint;
        mainObjectLastPos = nearestPoint;
        lastPoint.gameObject.SetActive(false);
    }

    void SnapToNearestPointDrag()
    {
        for (int i = 0; i < points.Count; i++)
        {
            if (Vector3.Distance(points[i].position, tempObj.transform.position) < snapDistance)
            {
                if (CheckDistanceToPlayer(points[i].position)) //DISTANCE TO PLAYER CHECK
                {
                    if (!useVerticalCheck || !CheckVertical(tempObj.transform))
                    {
                        if (points[i].gameObject.activeSelf == true)
                        {
                            if(followsCurrentPoint) // TESTCODE:
                                interactable.mainObject.transform.position = new Vector3(points[i].position.x, points[i].position.y, points[i].position.z);

                            //AutoRotate:
                            if (autoRotate)
                            {
                                if (tempObj)
                                    tempObj.transform.rotation = points[i].rotation;

                                interactable.mainObject.transform.rotation = points[i].rotation;
                            }
                            //-----------

                            if (points[i].transform != lastPoint)
                            {
                                pointsVisited.Add(points[i].transform);
                            }

                            lastPoint = points[i].transform;
                        }
                    }
                }
            }            
        }                 
    }

    void SnapToNearestPointDragVolume()
    {
        for (int i = 0; i < points.Count; i++)
        {
            if (Vector3.Distance(points[i].position, tempObj.transform.position) < snapDistance)
            {
                if (points[i].gameObject.activeSelf == true)
                {
                    mainObjectClone.transform.position = points[i].position;
                    var checkBounds = true;

                    checkBounds = CheckVolume(checkBounds);
                    
                    if (!canInteractWithPlayer) // CHECK FOR PLAYER INSIDE
                    {
                        if(player)
                        {
                            if (HelperFunctions.PointInOABB(player.position, boxCollider))
                            {
                                checkBounds = false;
                            }
                        }
                    }

                    if (useVerticalCheck)
                    {
                        if (CheckVertical(mainObjectClone.transform))
                            checkBounds = false;
                        else
                            checkBounds = true;
                    }


                    if (checkBounds)
                    {
                        if (followsCurrentPoint) // TESTCODE:
                            interactable.mainObject.transform.position = new Vector3(points[i].position.x, points[i].position.y, points[i].position.z);

                        //AutoRotate:
                        if (autoRotate)
                        {
                            if (tempObj)
                                tempObj.transform.rotation = points[i].rotation;

                            if (mainObjectClone)
                                mainObjectClone.transform.rotation = points[i].rotation;

                            interactable.mainObject.transform.rotation = points[i].rotation;
                        }
                        //-----------

                        if (points[i].transform != lastPoint)
                        {
                            pointsVisited.Add(points[i].transform);
                        }

                        lastPoint = points[i].transform;
                    }
                }
            }                       
        }
    }

    void CheckRotation(Transform interactableRotation) // ROTATION !!!
    {
        if (rotationInteractable)
        {
            if (interactableRotation == rotationInteractable.transform)
            {
                print("rotate Object");

                if (useVolume)
                {
                    var checkBounds = true;

                    foreach (var nestedPoint in nestedPoints)
                    {
                        if (!nestedPoint.gameObject.activeSelf)
                        {
                            checkBounds = false;
                            print("Can't rotate while object on top!");
                        }
                    }

                    if (!canInteractWithPlayer) // CHECK FOR PLAYER INSIDE
                    {
                        if (player)
                        {
                            if (HelperFunctions.PointInOABB(player.position, boxCollider))
                            {
                                checkBounds = false;
                                print("Can't rotate while player on top!");
                            }
                        }
                    }
                    
                    if (!checkBounds)
                    {
                        print("Can't rotate"); // insert code to indicate

                        if (OnDTPFreeRotationError != null)
                        {
                            OnDTPFreeRotationError(transform);
                        }
                    }
                    else
                    {
                        mainObjectClone.transform.localEulerAngles += (rotationAxis * rotationStep);
                        var rot = mainObjectClone.transform.localEulerAngles;
                        mainObjectClone.transform.localEulerAngles = new Vector3(Mathf.RoundToInt(rot.x), Mathf.RoundToInt(rot.y), Mathf.RoundToInt(rot.z));
                        
                        if(checkBounds)
                            checkBounds = CheckVolume(checkBounds);

                        if (useVerticalCheck)
                        {
                            bool test = CheckVertical(mainObjectClone.transform);

                            if (test)
                            {
                                checkBounds = false;
                                print("cant't rotate, something above object blocking...");
                            }
                            else
                                checkBounds = true;
                        }
                        //NEW_CHECK_END:
                        if (checkBounds)
                        {
                            interactable.mainObject.transform.localEulerAngles = mainObjectClone.transform.localEulerAngles;

                            foreach (var item in volumeTempPoints)
                            {
                                item.gameObject.SetActive(true);
                            }

                            volumeTempPoints.Clear();
                            
                            SetAndDeactivateVolumeTempPoints();
                            
                            if (OnDTPFreeRotation != null)
                            {
                                OnDTPFreeRotation(transform);
                            }
                        }
                        else
                        {
                            print("Not available space for rotation"); //insert code to indicate
                            mainObjectClone.transform.localEulerAngles = interactable.mainObject.transform.localEulerAngles;

                            if (OnDTPFreeRotationError != null)
                            {
                                OnDTPFreeRotationError(transform);
                            }
                        }
                    }
                }

                else // ROTATION OF MAIN OBJECT:
                {
                    bool checkRotation = true;

                    if(useVerticalCheck)
                    {
                        InitializeHeightRayOrigins(interactable.mainObject.transform);

                        bool verticalCheck = !CheckVertical(interactable.mainObject.transform);

                        //if(!useVolume)
                        ClearHeightRayOrigins();

                        if (verticalCheck)
                        {
                            checkRotation = false;
                            print("cant't rotate, something above object blocking...");
                        }
                        else
                            checkRotation = true;
                    }

                    if (!CheckForPlayer())
                    {
                        checkRotation = true;
                    }
                    else
                    {
                        print("Can't rotate while Player on top!");  //insert code to indicate

                        if (OnDTPFreeRotationError != null)
                        {
                            OnDTPFreeRotationError(transform);
                        }

                        checkRotation = false;
                    }

                    if(checkRotation)
                    {
                        interactable.mainObject.transform.localEulerAngles += (rotationAxis * rotationStep);
                        var rot = interactable.mainObject.transform.localEulerAngles;
                        interactable.mainObject.transform.localEulerAngles = new Vector3(Mathf.RoundToInt(rot.x), Mathf.RoundToInt(rot.y), Mathf.RoundToInt(rot.z));

                        if (OnDTPFreeRotation != null)
                        {
                            OnDTPFreeRotation(transform);
                        }
                    }
                }
            }
        }
    }

    private bool CheckVertical(Transform transformToCheck)
    {
        bool check = false;
        // deactivates possible colliders;
        if (interactable.mainCollider)
            interactable.mainCollider.enabled = false;

        if (useVolume)
        {
            thisBoxCollider.enabled = false;
            boxCollider.enabled = false;
        }
        
        var tempPlayerPoints = new List<Transform>();

        foreach (var item in playerPoints)
        {
            if(item.gameObject.activeSelf)
            {
                tempPlayerPoints.Add(item);
                item.gameObject.SetActive(false);
            }
        }
        //
        for (int i = 0; i < currentHeightRayOrigins.Count; i++)
        {
            float margin = 0.25f; // to ensure to hit bottom of objects...
            Vector3 origin = currentHeightRayOrigins[i].position;
            float length = origin.y - transformToCheck.position.y - margin; 

            RaycastHit hit = new RaycastHit();

            if(Physics.Raycast(origin, -Vector3.up, out hit, length))
            {
                if (hit.collider)
                {
                    if ((hit.collider.CompareTag("Player") || hit.collider.CompareTag("Interactable")) &&
                        hit.collider != interactable.mainCollider)
                    {
                        if (rotationInteractable)
                        {
                            if(hit.transform != rotationInteractable.transform)
                            {
                                check = true;
                                Debug.DrawLine(origin, hit.point, Color.red);
                            }
                            else
                            {
                                Debug.DrawLine(origin, hit.point, Color.yellow);
                            }
                        }
                        else
                        {
                            check = true;
                            Debug.DrawLine(origin, hit.point, Color.red);
                        }
                    }
                    else
                        Debug.DrawLine(origin, hit.point, Color.yellow);
                }
                else
                {
                    Debug.DrawLine(origin, origin - (Vector3.up * length), Color.green);
                }
            }
        }
        // activates possible colliders;
        if (interactable.mainCollider)
            interactable.mainCollider.enabled = true;

        if (useVolume)
        {
            thisBoxCollider.enabled = true;
            boxCollider.enabled = true;
        }

        foreach (var item in tempPlayerPoints)
        {
            item.gameObject.SetActive(true);
        }

        tempPlayerPoints.Clear();
        print("CheckVertical");
        //
        if (check)
            return true;
        else
            return false;
    }

    private bool CheckVolume(bool check)
    {
        var candidates = new List<Transform>();

        foreach (var item in points)
        {
            if (HelperFunctions.PointInOABB(item.position, boxCollider))
            {
                candidates.Add(item);
            }
        }

        if (candidates.Count < tempSocketPoints.Count)
        {
            check = false;
            return check;
        }
        else
        {
            int distCheckNum = 0;

            for (int i = 0; i < tempSocketPoints.Count; i++)
            {
                bool distCheck = false;

                for (int e = 0; e < candidates.Count; e++)
                {
                    if (Vector3.Distance(tempSocketPoints[i].position, candidates[e].position) < 0.1f)
                    {
                        if (volumeTempPoints.Contains(candidates[e]))
                        {
                            distCheck = true;
                        }
                        else
                        {
                            if (candidates[e].gameObject.activeSelf)
                                distCheck = true;
                        }
                    }
                }

                if (distCheck)
                    distCheckNum++;
            }

            if (distCheckNum < tempSocketPoints.Count)
            {
                check = false;
                return check;
            }
        }

        return check;
    }

    void SetAndDeactivateVolumeTempPoints()
    {
        for (int i = 0; i < points.Count; i++)
        {
            if (!nestedPoints.Contains(points[i]))
            {
                if (!volumeTempPoints.Contains(points[i]))
                {
                    for (int e = 0; e < tempSocketPoints.Count; e++)
                    {
                        if (Vector3.Distance(points[i].position, tempSocketPoints[e].position) < 0.1f)
                        {
                            volumeTempPoints.Add(points[i]);
                            points[i].gameObject.SetActive(false);
                        }
                    }                    
                }               
            }
        }
    }
    
    private void OnMouseUp()
    {
        if (mainObjectRenderer)
            mainObjectRenderer.enabled = true;

        if (!lockBehaviour)
        {
            if (!useVolume)
            {
                if (normalDragAllowed)
                    SnapToNearestPoint();
            }
            else
            {
                if (volumeDragAllowed)
                    SnapToNearestPointVolume();
            }

            if (interactable.rigidBody != null)
            {
                rb.isKinematic = rbIsKinematic;
                rb.useGravity = rbUseGravity;
            }

            if (tempObj)
            {
                if (useVerticalCheck && !useVolume)
                    currentHeightRayOrigins.Clear();

                Destroy(tempObj);
            }
            
            isInitialized = false;
        }
    }   

    private void OnDestroy()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;

        if(OnDTPFreeDestroy != null)
        {
            OnDTPFreeDestroy(transform);
        }
    }

    public void OnDrawGizmos()
    {
        if (drawPointsInEditor)
        {
            if (points.Count > 0)
            {
                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i] != null && points[i].gameObject.activeSelf)
                    {
                        Gizmos.color = Color.green;
                        Gizmos.DrawWireSphere(points[i].position, 0.25f);
                    }
                    else if (points[i] != null && !points[i].gameObject.activeSelf)
                    {
                        Gizmos.color = Color.gray;
                        Gizmos.DrawWireSphere(points[i].position, 0.25f);
                    }
                }
            }
            if(nestedPoints.Count > 0)
            {
                for (int i = 0; i < nestedPoints.Count; i++)
                {
                    if (nestedPoints[i] != null && nestedPoints[i].gameObject.activeSelf)
                    {
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawWireSphere(nestedPoints[i].position, 0.25f);
                    }
                    if (nestedPoints[i] != null && !nestedPoints[i].gameObject.activeSelf)
                    {
                        Gizmos.color = Color.grey;
                        Gizmos.DrawWireSphere(nestedPoints[i].position, 0.25f);
                    }
                }
            }
        }
        if (tempObj)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(tempObj.transform.position, Vector3.one / 4);
        }
        if (useVolume)
        {
            Gizmos.matrix = interactable.mainObject.transform.localToWorldMatrix;
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(thisBoxCollider.center, thisBoxCollider.size);
        }
        if (useVolume)
        {
            if (mainObjectClone)
            {
                Gizmos.matrix = mainObjectClone.transform.localToWorldMatrix;
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(boxCollider.center, boxCollider.size);
            }
        }        
    }
}
