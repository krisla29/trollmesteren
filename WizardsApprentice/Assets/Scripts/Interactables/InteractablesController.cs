﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractablesController : MonoBehaviour
{
    public enum Icons { Normal, Pickup, Slide, Rotate, Scale, AddForce, Swipe, Pull, Push, Press }
    public List<Sprite> interactableIcons = new List<Sprite>();
    /*
    [System.Serializable]

    public struct 
    */
}
