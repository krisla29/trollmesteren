﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourSlidePath : InteractableBehaviour
{
    private GameObject tempObj;
    //public enum SlideAxes { X, Y, Z, }
    //public SlideAxes slideAxis;
    //public float stepSize = 1;
    public float speed = 1.5f;
    public Transform goal;
    //public Vector3 startPos;
    public List<Transform> checkPoints = new List<Transform>();
    public Transform nextPoint;
    public Transform lastPoint;
    [SerializeField] private int checkPointCounter = 0;

    [SerializeField] private Vector3 startLocPos;
    [SerializeField] private Quaternion startLocRot;
    [SerializeField] private Vector3 endLocPos;
    [SerializeField] private Quaternion endLocRot;
    //public Transform start;
    //public Transform end;
    public bool canTrackBackwards;
    public bool reachedEnd;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        //print("picking interactable up");
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            //if (hit.collider.gameObject == transform.gameObject)
            //{
                if (!isInitialized)
                {
                    tempObj = new GameObject("RayHitObject");
                    tempObj.transform.SetParent(interactable.mainObject.transform.parent);
                }
                isInitialized = true;
                
                tempObj.transform.position = ray.origin + Vector3.Distance(hit.point, ray.origin) * ray.direction;
                //var inversedPosition = Camera.main.transform.Inv

                var oldLocPos = interactable.mainObject.transform.localPosition;
                var oldLocRot = interactable.mainObject.transform.localRotation;
                var newLocPos = tempObj.transform.localPosition;

                if (checkPointCounter == 0)
                {
                    startLocPos = checkPoints[0].localPosition;
                    startLocRot = checkPoints[0].localRotation;
                    endLocPos = checkPoints[1].localPosition;
                    endLocRot = checkPoints[1].localRotation;
                    lastPoint = checkPoints[0];
                    nextPoint = checkPoints[1];
                }
                         

                if (interactable.mainObject.transform.localPosition.x == nextPoint.localPosition.x)
                {
                    if (checkPointCounter <= checkPoints.Count - 3)
                    {
                        interactable.mainObject.transform.localRotation = endLocRot;

                        startLocPos = checkPoints[checkPointCounter + 1].localPosition;
                        startLocRot = checkPoints[checkPointCounter + 1].localRotation;
                        endLocPos = checkPoints[checkPointCounter + 2].localPosition;
                        endLocRot = checkPoints[checkPointCounter + 2].localRotation;
                                               
                        lastPoint = checkPoints[checkPointCounter + 1];
                        nextPoint = checkPoints[checkPointCounter + 2];
                        
                        
                        interactable.mainObject.transform.localPosition = new Vector3(interactable.mainObject.transform.localPosition.x + 0.1f, 0, 0);
                        print("SlidePath reached next point");
                        checkPointCounter += 1;
                    }
                }

                if (interactable.mainObject.transform.localPosition.x == lastPoint.localPosition.x)
                {
                    if (checkPointCounter >= 1)
                    {                       
                        startLocPos = checkPoints[checkPointCounter - 1].localPosition;
                        startLocRot = checkPoints[checkPointCounter - 1].localRotation;
                        endLocPos = checkPoints[checkPointCounter].localPosition;
                        endLocRot = checkPoints[checkPointCounter].localRotation;
                       
                        lastPoint = checkPoints[checkPointCounter - 1];
                        nextPoint = checkPoints[checkPointCounter];

                        interactable.mainObject.transform.localRotation = lastPoint.localRotation;
                        print("SlidePath reached last point");
                        checkPointCounter -= 1;
                    }
                }


                // NORMALIZING PATH:
                var lengthCurrentPathX = (nextPoint.localPosition.x - lastPoint.localPosition.x);
                var lengthCurrentPathZ = (nextPoint.localPosition.z - lastPoint.localPosition.z);
                var progressCurrentPathX = (newLocPos.x - lastPoint.localPosition.x) / lengthCurrentPathX;
                //print(progressCurrentPathX);
                var newLocPosClamped = new Vector3(Mathf.Clamp(newLocPos.x, endLocPos.x, startLocPos.x), oldLocPos.y, lastPoint.localPosition.z + lengthCurrentPathZ * progressCurrentPathX);

                interactable.mainObject.transform.localPosition = newLocPosClamped;
                //NewTranslation();
                //}
        }
    }

    private void NewTranslation()
    {
        var translation = (transform.right * Input.GetAxisRaw("Mouse X") * speed) + (transform.right * Input.GetAxisRaw("Mouse Y") * speed);
        var translationInversed = Camera.main.transform.InverseTransformDirection(translation);
        interactable.mainObject.transform.position += translation;
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDisable()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDestroy()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }
}
