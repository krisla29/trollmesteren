﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourSlidePlane : InteractableBehaviour
{
    public float speed = 1;
    private GameObject tempObj;
    public Transform goal;
    public Vector3 startPos;
    public Transform areaObject;
    public List<Transform> corners = new List <Transform>(4);
    public bool reachedEnd;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }       
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        print("picking interactable up");
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            //if (hit.collider.gameObject == transform.gameObject)
            //{               
                if (!isInitialized)
                {
                    tempObj = new GameObject("slideObject");
                    tempObj.transform.SetParent(interactable.mainObject.transform.parent);
                    startPos = transform.localPosition;
                }

                isInitialized = true;

                tempObj.transform.position = ray.origin + Vector3.Distance(hit.collider.transform.position, ray.origin) * ray.direction;

                var oldLocPos = interactable.mainObject.transform.localPosition;
                var newWorldPos = new Vector3(tempObj.transform.localPosition.x, tempObj.transform.localPosition.y, tempObj.transform.localPosition.z);
                var newPos = new Vector3(Mathf.Clamp(newWorldPos.x, corners[1].localPosition.x, corners[2].localPosition.x), oldLocPos.y, Mathf.Clamp(newWorldPos.z, corners[2].localPosition.z, corners[1].localPosition.z));

                interactable.mainObject.transform.localPosition = Vector3.Lerp(interactable.mainObject.transform.localPosition, newPos, Time.deltaTime * 10 * speed);              
            //}
        }
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDisable()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDestroy()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    public void OnGUI()
    {
        
    }

    public void OnDrawGizmos()
    {        
        for (int i = 0; i < corners.Count; i++)
        {            
            Gizmos.color = Color.red;

            if(i == 0)
            {
                corners[i].localPosition = areaObject.transform.localPosition + new Vector3(areaObject.transform.localScale.x, 0, areaObject.transform.localScale.z);
                //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 1)
            {
                corners[i].localPosition = areaObject.transform.localPosition + new Vector3(-areaObject.transform.localScale.x, 0, areaObject.transform.localScale.z);
                //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 2)
            {
                corners[i].localPosition = areaObject.transform.localPosition + new Vector3(areaObject.transform.localScale.x, 0, -areaObject.transform.localScale.z);
                //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
            if (i == 3)
            {
                corners[i].localPosition = areaObject.transform.localPosition + new Vector3(-areaObject.transform.localScale.x, 0, -areaObject.transform.localScale.z);
                //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
            }
        }
    }
}
