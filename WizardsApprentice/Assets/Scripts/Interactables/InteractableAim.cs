﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableAim : MonoBehaviour
{
    public List<Sprite> icons = new List<Sprite>();

    [SerializeField] private SpriteRenderer spriteRendererSmallIcon;
    public SpriteRenderer mainSprite;
    private Interactable interactable;
    public EasyScalePulse easyScalePulse;
    public LookAtCamera lookAtCameraScript;
    private float lerpAlphaTime = 0.5f;
    private float alpha = 0.9f;

    private void Start()
    {
        // Counter parent scale:
        Transform oldParent = transform.parent;
        transform.SetParent(null);
        transform.localScale = Vector3.one;
        transform.SetParent(oldParent);

        mainSprite.color = new Color(mainSprite.color.r, mainSprite.color.g, mainSprite.color.b, 0);       
        interactable = transform.parent.GetComponent<Interactable>();

        if (interactable != null)
        {
            if(interactable.icon != null)
            {
                mainSprite.sprite = interactable.icon;               
            }

            if (interactable.iconLookAtCamera)
                lookAtCameraScript.enabled = true;
            else
                lookAtCameraScript.enabled = false;

            mainSprite.transform.localScale *= interactable.iconSize;
            mainSprite.transform.localPosition = interactable.iconOffset;
            mainSprite.transform.localEulerAngles = interactable.iconLocalRotation;
            mainSprite.color = interactable.iconColor;

            if(interactable.iconMaterial != null)
            {
                mainSprite.material = interactable.iconMaterial;
                mainSprite.material.color = interactable.iconColor;
            }

            if (!interactable.animateIcon)
                easyScalePulse.enabled = false;
        }

        
        StartCoroutine("LerpAlphaStart");
    }

    private IEnumerator LerpAlphaStart()
    {
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / lerpAlphaTime;
            mainSprite.color = Color.Lerp(new Color(mainSprite.color.r, mainSprite.color.g, mainSprite.color.b, 0), new Color(mainSprite.color.r, mainSprite.color.g, mainSprite.color.b, alpha), time);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator LerpAlphaEnd()
    {
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / lerpAlphaTime;
            mainSprite.color = Color.Lerp(new Color(mainSprite.color.r, mainSprite.color.g, mainSprite.color.b, alpha), new Color(mainSprite.color.r, mainSprite.color.g, mainSprite.color.b, 0), time);
            yield return new WaitForEndOfFrame();
        }
    }

    private void SwitchInteractableIcon()
    {
        if (interactable != null)
        {
            switch (interactable.interactableType)
            {
                case (Interactable.InteractableTypes.AddForce):
                    //spriteRendererSmallIcon.sprite = pressIcon;
                    break;
                case (Interactable.InteractableTypes.Normal):
                    //spriteRendererSmallIcon.sprite = pressIcon;
                    break;
                case (Interactable.InteractableTypes.Pickup):
                    break;
                case (Interactable.InteractableTypes.Press):
                    break;
                case (Interactable.InteractableTypes.Pull):
                    break;
                case (Interactable.InteractableTypes.Push):
                    break;
                case (Interactable.InteractableTypes.Rotate):
                    break;
                case (Interactable.InteractableTypes.Scale):
                    break;
                case (Interactable.InteractableTypes.Slide):
                    break;
                case (Interactable.InteractableTypes.Swipe):
                    break;
                default:
                    break;
            }
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
