﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourPickup : InteractableBehaviour
{
    //private float posY;
    //private Vector3 initialPosition;
    [SerializeField] private GameObject tempObj;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        print("picking interactable up");
        //StartCoroutine("MoveUp");
    }

    private void OnMouseDrag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            if (hit.collider.gameObject == transform.gameObject)
            {
                if (!isInitialized)
                {
                    tempObj = new GameObject("pickupObject");
                    tempObj.transform.position = interactable.mainObject.transform.position;
                }

                isInitialized = true;

                tempObj.transform.position = ray.origin + Vector3.Distance(hit.collider.transform.position, ray.origin) * ray.direction;
                tempObj.transform.rotation = tempObj.transform.rotation * Quaternion.LookRotation(-Camera.main.transform.forward, Vector3.up);

                var newWorldPos = new Vector3(tempObj.transform.position.x, tempObj.transform.position.y, tempObj.transform.position.z);

                interactable.mainObject.transform.localPosition = newWorldPos;
            }            
        }
    }

    private void OnMouseUp()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        Destroy(tempObj);
        isInitialized = false;

        print("putting interactable down");
    }
    /*
    private IEnumerator MoveUp()
    {
        print("hello");
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / 0.5f;
            var pos = interactable.mainObject.transform.localPosition;
            interactable.mainObject.transform.localPosition = new Vector3( pos.x, (Mathf.Lerp(pos.y, posY + 2, time)), pos.z);
            yield return new WaitForEndOfFrame();
        }
    }
    */
    private void OnDisable()
    {
        Destroy(tempObj);
        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        Destroy(tempObj);
        isInitialized = false;
        StopAllCoroutines();
    }
}
