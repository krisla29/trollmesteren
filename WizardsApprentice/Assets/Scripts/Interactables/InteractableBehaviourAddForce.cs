﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourAddForce : InteractableBehaviour
{
    [SerializeField] private GameObject tempObj;   
    public enum ForceApplications { Throw, Direct, Inverse, Swipe}
    public ForceApplications forceApplication;
    public bool buildUpForce;
    public float buildUpSpeed = 1f;
    //public float maxSpeed = 10f;
    private float buildUpTimeStamp;
    public enum ForceTypes { Force, Torque }
    public ForceTypes forceType;
    public enum DirectionTypes { Camera, WorldAxis, CameraPlanarHorizontal, CameraPlanarVertical}
    public DirectionTypes directionType;
    public Vector3 worldDirection = Vector3.up;
    public Vector3 forceDirection = Vector3.zero;
    public float force;
    public float minForce = 1f;
    public float maxForce = 10f;
    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;
    private Vector3 oldPos = Vector3.zero;
    public Vector3 currentSpeed = Vector3.zero;
    private Vector3 initialPos = Vector3.zero;
    private float distance = 0;
    public float rayHitMaxDistance = 15f;

    private void Start()
    {
        float posY = interactable.mainObject.transform.localPosition.y;

        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null && forceApplication == ForceApplications.Throw)
        {           
            rb.isKinematic = true;
            rb.useGravity = false;            
        }

        buildUpTimeStamp = Time.time;

    }

    private void OnMouseDrag()
    {        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            //if (hit.collider.gameObject == transform.gameObject)
            //{             
                if (!isInitialized)
                {
                    tempObj = new GameObject("pickupObject");

                    if (forceApplication == ForceApplications.Direct || forceApplication == ForceApplications.Throw)
                    {
                        tempObj.transform.position = interactable.mainObject.transform.position;
                        oldPos = interactable.mainObject.transform.position;                        
                    }
                    else
                    {
                        tempObj.transform.position = ray.origin + Vector3.Distance(hit.point, ray.origin) * ray.direction;
                        oldPos = tempObj.transform.position;                        
                    }

                    initialPos = interactable.mainObject.transform.position;
                }

                isInitialized = true;

            if (Vector3.Distance(hit.point, interactable.mainObject.transform.position) < rayHitMaxDistance)
            {
                // CURRENT HITOBJ POSITION:
                if (forceApplication == ForceApplications.Direct || forceApplication == ForceApplications.Throw)
                {
                    tempObj.transform.position = ray.origin + Vector3.Distance(hit.collider.transform.position, ray.origin) * ray.direction;
                }
                else
                {
                    tempObj.transform.position = ray.origin + Vector3.Distance(hit.point, ray.origin) * ray.direction;
                }

                tempObj.transform.rotation = tempObj.transform.rotation * Quaternion.LookRotation(-Camera.main.transform.forward, Vector3.up);
                var newWorldPos = new Vector3(tempObj.transform.position.x, tempObj.transform.position.y, tempObj.transform.position.z);

                if (buildUpForce)
                {
                    var nForce = (minForce + (Time.time - buildUpTimeStamp)) * buildUpSpeed;
                    force = Mathf.Clamp(nForce, minForce, maxForce);
                }
                //SET FORCE
                if (forceApplication == ForceApplications.Throw)
                {
                    if(directionType == DirectionTypes.WorldAxis || directionType == DirectionTypes.Camera)
                        interactable.mainObject.transform.position = newWorldPos;
                    else if(directionType == DirectionTypes.CameraPlanarHorizontal)
                        interactable.mainObject.transform.position = new Vector3(newWorldPos.x, initialPos.y , newWorldPos.z);
                    else if (directionType == DirectionTypes.CameraPlanarVertical)
                        interactable.mainObject.transform.position = new Vector3(newWorldPos.x, newWorldPos.y, initialPos.z);

                    currentSpeed = interactable.mainObject.transform.position - oldPos;
                    oldPos = interactable.mainObject.transform.position;
                }

                if (forceApplication == ForceApplications.Swipe)
                {
                    currentSpeed = tempObj.transform.position - oldPos;
                }

                if (forceApplication == ForceApplications.Inverse)
                {
                    currentSpeed = -1 * (tempObj.transform.position - oldPos);
                }
            }
        }
    }

    private void OnMouseUp()
    {
        // SET DIRECTION:
        if (directionType != DirectionTypes.WorldAxis)
        {
            if (forceApplication != ForceApplications.Throw)
            {
                distance = Vector3.Distance(tempObj.transform.position, initialPos);
                distance = Mathf.Clamp(distance / 5f, -rayHitMaxDistance, rayHitMaxDistance);
            }
            else
            {
                distance = Vector3.Distance(currentSpeed, Vector3.zero);
                distance = Mathf.Clamp(distance * 5f, -rayHitMaxDistance, rayHitMaxDistance);
            }


            if (forceApplication == ForceApplications.Direct)
            {
                var newDir = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z);
                forceDirection = newDir;
            }
            else
            {
                var newDir = currentSpeed.normalized * distance;
                forceDirection = newDir;
            }

            if(forceType == ForceTypes.Torque)
            {
                var newDir = new Vector3(Camera.main.transform.right.x, 0, Camera.main.transform.right.z);
                forceDirection = newDir;
            }
            /*
            if (directionType == DirectionTypes.CameraPlanarHorizontal)
                forceDirection = Camera.main.transform.TransformDirection(new Vector3(forceDirection.x, 0, forceDirection.z));

            if (directionType == DirectionTypes.CameraPlanarVertical)
                forceDirection = Camera.main.transform.TransformDirection(new Vector3(forceDirection.x, forceDirection.y, 0));
            */
        }
        else
        {
            forceDirection = worldDirection;
        }
        // ACTIVATE RIGIDBODY:
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        // APPLY FORCE:
        print("Interactable Force force is  " + force);
        print("Interactable Direction is  " + forceDirection);

        if (forceType == ForceTypes.Force)
        {
            rb.AddForce(forceDirection * force, ForceMode.Impulse);
        }
        else
        {
            rb.AddTorque(forceDirection * force, ForceMode.Impulse);
        }
        

        Destroy(tempObj);
        isInitialized = false;

        buildUpTimeStamp = 0;
        print("putting interactable down");

        distance = 0;
        currentSpeed = Vector3.zero;
        forceDirection = Vector3.zero;       
    }

    private void OnDisable()
    {
        distance = 0;
        currentSpeed = Vector3.zero;
        forceDirection = Vector3.zero;
        buildUpTimeStamp = 0;
        Destroy(tempObj);
        isInitialized = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        Destroy(tempObj);
        isInitialized = false;
        StopAllCoroutines();
    }
}
