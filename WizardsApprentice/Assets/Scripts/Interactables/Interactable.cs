﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour
{
    public Sprite icon;
    public float iconSize = 1f;
    public bool animateIcon = true;
    public Vector3 iconOffset = Vector3.zero;
    public Vector3 iconLocalRotation = Vector3.zero;
    public Color iconColor = Color.white;
    public Material iconMaterial;
    public bool iconLookAtCamera = true;
    public GameObject particleSystemPrefab;
    private GameObject particleSystemInstance;
    public enum InteractableTypes { Normal, Pickup, Slide, Rotate, Scale, AddForce, Swipe, Pull, Push, Press}
    public InteractableTypes interactableType;
    public bool isEnemy;
    public bool isReachable;
    public bool isPressed;
    public InteractableBehaviour interactableBehaviour;
    public GameObject mainObject;
    public Collider mainCollider;
    public Rigidbody rigidBody;
    public Collider SlideGoal;
    public bool isKinematic;
    public bool useGravity;

    public AudioSource soundPress;
    public AudioSource soundRelease;
    public AudioSource soundDrag;
    public AudioSource soundDestroy;
    public AudioSource soundSpawn;
    public AudioSource soundEnable;
    public AudioSource soundDisable;

    public delegate void InteractableState(Transform interactable); //Sends to InteractablesManager
    public static event InteractableState OnInteractableActive;
    public static event InteractableState OnInteractableDeactive;
    public static event InteractableState OnInteractableActiveEnemy;
    public static event InteractableState OnInteractableDeactiveEnemy;
    public static event InteractableState OnInteractableBirth;
    public static event InteractableState OnInteractableDeath;
    public static event InteractableState OnInteractableEnemyBirth;
    public static event InteractableState OnInteractableEnemyDeath;
    public static event InteractableState OnInteractablePressed;
    public static event InteractableState OnInteractableReleased;

    private void Start()
    {
        if (soundSpawn)
            soundSpawn.Play();

        if (rigidBody)
        {
            isKinematic = rigidBody.isKinematic;
            useGravity = rigidBody.useGravity;
        }
        if (!isEnemy)
        {
            if (OnInteractableBirth != null)
            {
                OnInteractableBirth(transform);
            }
        }
        else
        {
            if (OnInteractableEnemyBirth != null)
            {
                OnInteractableEnemyBirth(transform);
            }
        }
        if (!isEnemy)
        {
            if (OnInteractableActive != null)
            {
                OnInteractableActive(transform);
            }
        }
        else
        {
            if (OnInteractableActiveEnemy != null)
            {
                OnInteractableActiveEnemy(transform);
            }
        }
    }
    private void OnDestroy()
    {
        if (soundDestroy)
            soundDestroy.Play();

        if (!isEnemy)
        {
            if (OnInteractableDeath != null)
            {
                OnInteractableDeath(transform);
            }
        }
        else
        {
            if (OnInteractableEnemyDeath != null)
            {
                OnInteractableEnemyDeath(transform);
            }
        }
        if (!isEnemy)
        {
            if (OnInteractableDeactive != null)
            {
                OnInteractableDeactive(transform);
            }
        }
        else
        {
            if (OnInteractableDeactiveEnemy != null)
            {
                OnInteractableDeactiveEnemy(transform);
            }
        }
    }
    private void OnEnable()
    {
        if (soundEnable)
            soundEnable.Play();

        if (!isEnemy)
        {
            if (OnInteractableActive != null)
            {
                OnInteractableActive(transform);
            }
        }
        else
        {
            if (OnInteractableActiveEnemy != null)
            {
                OnInteractableActiveEnemy(transform);
            }
        }
    }
    private void OnDisable()
    {
        if (soundDisable)
            soundDisable.Play();

        if (!isEnemy)
        {
            if (OnInteractableDeactive != null)
            {
                OnInteractableDeactive(transform);
            }
        }
        else
        {
            if (OnInteractableDeactiveEnemy != null)
            {
                OnInteractableDeactiveEnemy(transform);
            }
        }
    }

    private void OnMouseDown()
    {
        if (soundPress)
            soundPress.Play();

        isPressed = true;

        if(null != particleSystemPrefab)
            particleSystemInstance = Instantiate(particleSystemPrefab, transform);

        if(OnInteractablePressed != null)
        {
            OnInteractablePressed(transform);
        }
    }

    private void OnMouseEnter()
    {
        
    }

    private void OnMouseDrag()
    {
        if (soundDrag)
        {
            if(!soundDrag.isPlaying)
                soundDrag.Play();
        }
    }

    private void OnMouseExit()
    {
        
    }

    private void OnMouseUp()
    {
        if (soundRelease)
            soundRelease.Play();

        if (null != particleSystemPrefab)
            Destroy(particleSystemInstance);

        isPressed = false;

        if (OnInteractableReleased != null)
        {
            OnInteractableReleased(transform);
        }
    }

    private void OnMouseUpAsButton()
    {
        
    }
}
