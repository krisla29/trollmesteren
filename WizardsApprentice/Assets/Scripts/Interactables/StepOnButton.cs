﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class StepOnButton : MonoBehaviour
{
    public float stepHeight = 0.25f;
    public float stayTime = 0.2f;
    public float lerpTime = 1f;
    //public bool isPressed;
    public float timer;
   
    public AudioSource stonePush;
    //public NavMeshAgent agent;
    private Transform prevParent;
    private Vector3 startPos;
    private Transform player;
    private float startPosY;
    

    private void Start()
    {
        //isPressed = false;
    }
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.CompareTag("Player"))
        {
            player = other.transform;
            startPos = player.position;
            player.position = new Vector3(player.position.x, player.position.y + stepHeight, player.position.z);
            prevParent = player.parent;
            player.SetParent(transform);
            startPosY = player.position.y;
            //agent = other.GetComponent<PlayerController>().navMeshAgent;
            //agent.baseOffset = stepHeight;
            timer = Time.time;
            //isPressed = false;
            StartCoroutine(Timer());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //if (agent && agent.enabled)
            //{
            //agent.baseOffset = 0;
            //}
            player.SetParent(prevParent);
            player.position = new Vector3(player.position.x, player.position.y - stepHeight, player.position.z);
            StopCoroutine(Timer());
            //isPressed = false;
        }
    }

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(stayTime);

        StartCoroutine(MoveStone());
        
        yield break;
    }

    private IEnumerator MoveStone()
    {
        stonePush.Play();
        var startPos = transform.localPosition;
        float time = 0;

        while (time < 1)
        {
            
            transform.position = Vector3.Lerp(startPos, new Vector3(startPos.x, startPos.y - stepHeight, startPos.z), time);

            //if (agent && agent.enabled)
            //{
                //agent.baseOffset = Mathf.Lerp(stepHeight, 0, time);
            //}
            //else
            //{
                //player.position = new Vector3(player.position.x, Mathf.Lerp(player.position.y, startPosY - stepHeight, time), player.position.z);
            //}

            time += Time.deltaTime / lerpTime;
            yield return new WaitForEndOfFrame();
        }

        //if(agent)
        //{
        //agent.baseOffset = 0;
        //}
        player.SetParent(prevParent);
        transform.gameObject.SetActive(false);

        yield return null;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
