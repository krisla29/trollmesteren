﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourDragToPoints : InteractableBehaviour
{
    public bool drawGizmos = true;
    private GameObject tempObj; //readOnly
    public bool isInitializedByOther;
    public bool activatePoints;
    //public Transform goal;
    public Transform worldTransform;
    public Vector3 startPos; //readOnly
    //public bool useAreaObject = true;
    public Transform areaObject;
    public List<Transform> corners = new List<Transform>(4);
    public GameObject pointObjectPrefab;
    public float pointScale = 0.25f;
    public enum PointModes { Grid, Custom}
    public PointModes pointMode;
    public List<Transform> points = new List<Transform>();
    public Vector2 gridSize;
    public List<Vector2Int> pointGridPositions;
    public bool snapOnly;
    public float snapDistance = 1;
    public List<Transform> pointsVisited = new List<Transform>();
    public int maxCountPointsVisited = 10;
    public bool ignoreCurrentPoint;
    public Transform currentPoint;
    public Transform lastPoint; //readOnly
    public bool reachedEnd; //readOnly
    private bool isInitialized; //readOnly
    private Rigidbody rb; //readOnly
    private bool rbIsKinematic; //readOnly
    private bool rbUseGravity; //readOnly

    public delegate void DragToPointsEvent(Transform interactableMainObject, Transform pointTransform); // Sends to MatchObjectGame -------- ATTENTION!!!!! MATCH OBJECT GAME DOES NOT CHECK TRANSFORMS!!!---------FIX---------
    public static event DragToPointsEvent OnArriveCurrentPoint;
    public static event DragToPointsEvent OnLeaveCurrentPoint;
    public static event DragToPointsEvent OnInitializationComplete; // Sends to starMakerGame
    public static event DragToPointsEvent OnAddingNewVisitedPoint; // Sends to starMakerGame
    public static event DragToPointsEvent OnRemovingNewVisitedPoint; // Sends to starMakerGame
    public delegate void DPSBehaviourEvent(InteractableBehaviourDragToPoints thisBehaviour, int index); // Sends to StarMakerGame
    public static event DPSBehaviourEvent OnDrop; // Sends to starMakerGame
    public static event DPSBehaviourEvent OnPickup; // Sends to starMakerGame

    private void Start()
    {
        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }

        if (!isInitializedByOther)
        {
            Initialize(gridSize);
        }
    }

    public void Initialize(Vector2 grid)
    {
        // DRAW GRID:
        if (pointMode == PointModes.Grid)
        {
            DrawGrid(grid);
        }

        if (snapOnly)
        {
            SnapToNearestPoint();
        }
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (!lockBehaviour)
        {
            if (interactable.rigidBody != null)
            {
                rb.isKinematic = true;
                rb.useGravity = false;
            }
            if (activatePoints)
            {
                lastPoint.gameObject.SetActive(true);
            }

            if (OnPickup != null)
            {
                OnPickup(this, points.IndexOf(lastPoint));
            }

            print("picking interactable up");
        }
    }

    public void SimulatedMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }
        if (activatePoints)
        {
            lastPoint.gameObject.SetActive(true);
        }

        if (OnPickup != null)
        {
            OnPickup(this, points.IndexOf(lastPoint));
        }
        print("Simulating interactable up");
    }

    private void OnMouseDrag()
    {
        if (!lockBehaviour)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                //if (hit.collider.gameObject == transform.gameObject)
                //{
                if (!isInitialized)
                {
                    tempObj = new GameObject("slideObject");
                    tempObj.transform.SetParent(interactable.mainObject.transform.parent);
                    startPos = transform.localPosition;
                }

                isInitialized = true;

                tempObj.transform.position = ray.origin + Vector3.Distance(hit.collider.transform.position, ray.origin) * ray.direction;

                var oldLocPos = interactable.mainObject.transform.localPosition;
                var newWorldPos = new Vector3(tempObj.transform.localPosition.x, tempObj.transform.localPosition.y, tempObj.transform.localPosition.z);

                interactable.mainObject.transform.localPosition = new Vector3(Mathf.Clamp(newWorldPos.x, corners[1].localPosition.x, corners[2].localPosition.x), oldLocPos.y, Mathf.Clamp(newWorldPos.z, corners[2].localPosition.z, corners[1].localPosition.z));

                for (int i = 0; i < points.Count; i++)
                {
                    var convertedPos = worldTransform.InverseTransformPoint(points[i].position);

                    if (Vector3.Distance(convertedPos, tempObj.transform.localPosition) < interactable.mainObject.transform.localScale.x / 2 * snapDistance)
                    {
                        if (points[i].gameObject.activeSelf == true)
                        {
                            interactable.mainObject.transform.localPosition = new Vector3(convertedPos.x, interactable.mainObject.transform.localPosition.y, convertedPos.z);
                        }
                    }
                    else
                    {
                        if (points[i] == currentPoint)
                        {
                            currentPoint = null;

                            if (OnLeaveCurrentPoint != null)
                            {
                                OnLeaveCurrentPoint(interactable.mainObject.transform, points[i]);
                            }
                        }
                    }
                }
            }
        }
    }

    public void SnapToNearestPoint()
    {
        var tempPoints = new List<Transform>();

        foreach (var item in points)
        {
            if(item.gameObject.activeSelf == true)
            {
                tempPoints.Add(item);
            }
        }
        
        Transform nearestPoint = HelperFunctions.FindClosestItem(tempPoints, interactable.mainObject.transform);

        var convertedPos = worldTransform.InverseTransformPoint(nearestPoint.position);

        interactable.mainObject.transform.localPosition = new Vector3(convertedPos.x, interactable.mainObject.transform.localPosition.y, convertedPos.z);

        currentPoint = nearestPoint;

        if(OnDrop != null)
        {
            OnDrop(this, points.IndexOf(currentPoint));
        }

        if (currentPoint != lastPoint)
        {
            pointsVisited.Add(currentPoint);

            if(OnAddingNewVisitedPoint != null)
            {
                OnAddingNewVisitedPoint(transform, currentPoint);
            }

            if (pointsVisited.Count > maxCountPointsVisited) ////!!!!!!
            {              
                if (OnRemovingNewVisitedPoint != null)
                {
                    OnRemovingNewVisitedPoint(transform, pointsVisited[0]);
                }

                pointsVisited.Remove(pointsVisited[0]);
            }
        }
       
        lastPoint = currentPoint;

        if(activatePoints)
        {
            lastPoint.gameObject.SetActive(false);
        }

        if (OnArriveCurrentPoint != null)
        {
            OnArriveCurrentPoint(interactable.mainObject.transform, nearestPoint);
        }
    }

    private void OnMouseUp()
    {
        if (!lockBehaviour)
        {
            if (snapOnly)
            {
                SnapToNearestPoint();
            }

            if (interactable.rigidBody != null)
            {
                rb.isKinematic = rbIsKinematic;
                rb.useGravity = rbUseGravity;
            }

            if (tempObj)
                Destroy(tempObj);

            isInitialized = false;
        }
    }

    private void OnDisable()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDestroy()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    public void DrawGrid(Vector2 grid)
    {
        if (points.Count < grid.x * grid.y)
        {
            var intervalX = ((areaObject.localScale.x / grid.x) / areaObject.localScale.x) * 2;
            var intervalY = ((areaObject.localScale.z / grid.y) / areaObject.localScale.z) * 2;
            var scaleRatio = areaObject.localScale.x / areaObject.localScale.z;

            intervalX += intervalX / (grid.x - 1);
            intervalY += intervalY / (grid.y - 1);

            pointGridPositions = new List<Vector2Int>();

            var offset = new Vector3(-1, 0, -1);

            for (int i = 0; i < grid.x; i++)
            {
                var newPointX = Instantiate(pointObjectPrefab, areaObject);
                newPointX.name = "Xpoint " + (i + 1).ToString();
                points.Add(newPointX.transform);
                
                HelperFunctions.CounterParentScale(newPointX.transform, areaObject, pointScale);

                newPointX.transform.localRotation = Quaternion.identity;
                newPointX.transform.localPosition = new Vector3((intervalX * i), 0, 0);

                pointGridPositions.Add(new Vector2Int(i, 0));

                for (int e = 0; e < grid.y - 1; e++)
                {
                    var newPointY = Instantiate(pointObjectPrefab, areaObject);
                    newPointY.name = "Ypoint " + (i * e).ToString();
                    points.Add(newPointY.transform);
                    
                    HelperFunctions.CounterParentScale(newPointY.transform, areaObject, pointScale);

                    newPointY.transform.localRotation = Quaternion.identity;
                    newPointY.transform.localPosition = new Vector3(newPointX.transform.localPosition.x, 0, intervalY + (intervalY * e));

                    if(e < grid.y - 1)
                        pointGridPositions.Add(new Vector2Int(i, e + 1));
                }
            }
            foreach (var item in points)
            {
                item.localPosition += offset;
            }
        }

        if (OnInitializationComplete != null)
        {
            OnInitializationComplete(transform, transform);
        }
    }

    public void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            if (areaObject)
            {
                for (int i = 0; i < corners.Count; i++)
                {
                    Gizmos.color = Color.red;

                    if (i == 0)
                    {
                        corners[i].localPosition = areaObject.transform.localPosition + new Vector3(areaObject.transform.localScale.x, 0, areaObject.transform.localScale.z);
                        //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                        Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
                    }
                    if (i == 1)
                    {
                        corners[i].localPosition = areaObject.transform.localPosition + new Vector3(-areaObject.transform.localScale.x, 0, areaObject.transform.localScale.z);
                        //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                        Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
                    }
                    if (i == 2)
                    {
                        corners[i].localPosition = areaObject.transform.localPosition + new Vector3(areaObject.transform.localScale.x, 0, -areaObject.transform.localScale.z);
                        //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                        Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
                    }
                    if (i == 3)
                    {
                        corners[i].localPosition = areaObject.transform.localPosition + new Vector3(-areaObject.transform.localScale.x, 0, -areaObject.transform.localScale.z);
                        //corners[i].rotation = interactable.mainObject.transform.parent.localRotation;
                        Gizmos.DrawWireCube(corners[i].position, new Vector3(0.5f, 0.5f, 0.5f));
                    }
                }
            }

            if (points.Count > 0)
            {
                for (int i = 0; i < points.Count; i++)
                {
                    if (points[i] != null)
                    {
                        Gizmos.color = Color.green;
                        Gizmos.DrawWireSphere(points[i].position, 0.25f);
                    }
                }
            }
        }
    }
}
