﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablesDetector : MonoBehaviour
{
    public GameManager gameManager;
    public List<Transform> interactables = new List<Transform>();
    public List<Transform> enemies = new List<Transform>();
    public GameObject aimPrefab;
    public float detectionDistance = 80f;

    private void Update()
    {
        if (gameManager)
        {
            if (gameManager.cameraSceneManager)
            {
                if (gameManager.cameraSceneManager.currentCamera)
                    transform.position = gameManager.cameraSceneManager.currentCamera.transform.position;
            }
        }

        transform.localScale = Vector3.one * detectionDistance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Interactable"))
        {
            if (CheckIfNew(other.transform, interactables))
            {
                interactables.Add(other.transform);
                var aim = Instantiate(aimPrefab, other.transform);
            }

        }
        if (other.transform.CompareTag("Enemy"))
        {
            if (CheckIfNew(other.transform, enemies))
            {
                enemies.Add(other.transform);
                var aim = Instantiate(aimPrefab, other.transform);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Interactable"))
        {
            if (!CheckIfNew(other.transform, interactables))
            {
                interactables.Remove(other.transform);
                foreach(Transform child in other.transform)
                {
                    if(child.CompareTag("InteractableAim"))
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
        }
        if (other.transform.CompareTag("Enemy"))
        {
            if (!CheckIfNew(other.transform, enemies))
            {
                enemies.Remove(other.transform);
                foreach (Transform child in other.transform)
                {
                    if (child.CompareTag("InteractableAim"))
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
        }
    }

    private void OnEnable()
    {
        Interactable.OnInteractableActive += AddInteractable;
        Interactable.OnInteractableDeactive += RemoveInteractable;
        Interactable.OnInteractableActiveEnemy += AddEnemy;
        Interactable.OnInteractableDeactiveEnemy += RemoveEnemy;
    }

    private void OnDisable()
    {
        Interactable.OnInteractableActive -= AddInteractable;
        Interactable.OnInteractableDeactive -= RemoveInteractable;
        Interactable.OnInteractableActiveEnemy -= AddEnemy;
        Interactable.OnInteractableDeactiveEnemy -= RemoveEnemy;
    }

    private void AddInteractable(Transform interactable)
    {
        if (CheckIfNew(interactable, interactables))
        {
            if (CheckDistance(interactable))
            {
                interactables.Add(interactable);
                var aim = Instantiate(aimPrefab, interactable);
            }
        }
    }

    private void RemoveInteractable(Transform interactable)
    {
        if (!CheckIfNew(interactable, interactables))
        {
            interactables.Remove(interactable);
            foreach (Transform child in interactable)
            {
                if (child.CompareTag("InteractableAim"))
                {
                    Destroy(child.gameObject);
                }
            }
        }
    }

    private void AddEnemy(Transform interactable)
    {
        if (CheckIfNew(interactable, enemies))
        {
            if (CheckDistance(interactable))
            {
                enemies.Add(interactable);
                var aim = Instantiate(aimPrefab, interactable);
            }
        }
    }

    private void RemoveEnemy(Transform interactable)
    {
        if (!CheckIfNew(interactable, enemies))
        {
            enemies.Remove(interactable);
            foreach (Transform child in interactable)
            {
                if (child.CompareTag("InteractableAim"))
                {
                    Destroy(child.gameObject);
                }
            }
        }
    }

    private bool CheckIfNew(Transform transformToCheck, List<Transform> listToCompare)
    {
        bool isNew = true;

        foreach (var item in listToCompare)
        {
            if (item == transformToCheck)
            {
                isNew = false;
            }
        }

        if (isNew)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public bool CheckDistance(Transform objectToCheck) // Same distance check as in Wand
    {
        float distance = transform.localScale.x;
        Vector3 position = transform.position;

        if (objectToCheck != null)
        {
            Vector3 diff = objectToCheck.position - position;
            float curDistance = diff.sqrMagnitude;          

            if (curDistance < distance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }
    }
}
