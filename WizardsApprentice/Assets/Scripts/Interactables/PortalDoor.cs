﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PortalDoor : MonoBehaviour
{
    public GameManager gameManager;
    public AudioSource clickSound;
    public Transform destination;
    public bool warp;

    private void OnMouseDown()
    {
        if (gameManager)
        {
            if (gameManager.player)
            {
                if (gameManager.player.navMeshAgent)
                {
                    if (gameManager.player.navMeshAgent.enabled == true && gameManager.player.navMeshAgent.isOnNavMesh)
                    {
                        Vector3 curDest = Vector3.zero;

                        if(!destination)
                            curDest = transform.position - Vector3.up;
                        else
                            curDest = destination.position;

                        gameManager.player.navMeshAgent.SetDestination(curDest);
                        //gameManager.player.navMeshAgent.nextPosition = curDest;

                        if (clickSound)
                            clickSound.Play();
                    }
                }
            }
        }
    }

    private void OnDisable()
    {
        if (gameManager)
        {
            if (gameManager.player)
            {
                if (gameManager.player.navMeshAgent)
                {
                    if (gameManager.player.navMeshAgent.enabled == true)
                    {
                        if(gameManager.player.navMeshAgent.isOnNavMesh)
                            gameManager.player.navMeshAgent.ResetPath();
                    }
                }
            }
        }
    }
}
