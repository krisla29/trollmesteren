﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableBehaviourSlidePath2 : InteractableBehaviour
{
    [Header("User Input Settings")]
    public bool initializeOnEnable;
    private GameObject tempObj;      
    public enum InputTypes { DragDirect, Press, Auto,}
    public InputTypes inputType;
    [Tooltip("boundingBox = If not null, slider will lock in specified direction if outside of bounds")]
    public BoxCollider boundingBox;
    [Tooltip("pointToCheck = If not null, position of referenced object will be used for boundingBox Check")]
    public Transform pointToCheck;
    private Transform point;
    [Tooltip("CanTrackBackwards = If Checked, slider traverses both backwards and forwards")]
    public bool canTrackBackwards;
    public float stepSize = 100;
    [Tooltip("Speed = Multiplier for input speed")]
    [Range(1, 10f)] public float speed = 1f;       
    public enum InputPlanes { Horizontal, Vertical }
    [Tooltip("Input Plane = Should Speed represent a vertical or horizontal plane")]
    public InputPlanes inputPlane;
    [Tooltip("Is Inverted = Should Direction be reversed")]
    public bool isInverted;
    [Tooltip("Is Negative = Should Speed be reversed")]
    public bool isNegative;
    [Header("Path Settings")]
    [Tooltip("CheckPoints = List of checkpoints that will be traversed in order of list")]
    public List<Transform> checkPoints = new List<Transform>();
    [Tooltip("Goal = One of the Transforms in CheckPoints. Will register event when reached")]
    public Transform goal;
    [Tooltip("NextCheckPoint = If left open, system will attempt to automate based on distance")]
    public Transform nextCheckPoint;
    [Tooltip("LastCheckPoint = If left open, system will attempt to automate based on distance")]
    public Transform lastCheckPoint;
    [Tooltip("CheckPoints AutoLook = If checked, checkpoints will rotate towards next checkpoint")]
    public bool checkPointsAutoLook;
    
    [Tooltip("PathProgression = point on normalized segment")]
    [Range(0, 1)] public float segmentProgression = 0.5f;
    public float segmentProgressionRound; //readonly
    [SerializeField] private int checkPointCounter = 0; //readonly
    [Tooltip("PathProgression = point on normalized path")]
    [Range(0, 1)] public float pathProgression = 0.5f; //readonly
    [SerializeField] private float currentSpeed; //readonly

    public bool isAtEnd; //readonly
    public bool isAtStart; //readonly
    public bool reachedGoal; //readonly

    private bool isInitialized;
    private Rigidbody rb;
    private bool rbIsKinematic;
    private bool rbUseGravity;
    private Vector3 inputSpeedDirection;
    private Vector3 dragSpeedDirection;
    private Vector3 dragSpeedDirectionPlanar;
    private Vector2 dragSpeedPlanar;
    private float currentPathLength;
    private Camera cam;
    private Transform camTransform;
    public float currentTotalLength;
    public bool lockPositive; //WriteOnly
    public bool lockNegative; //WriteOnly
    private float lastSegmentPos; //readOnly
    private Vector3 lastPos; //readOnly

    public delegate void PathSliderEvent (Transform mainObjectTransform); // EVENTS SENT TO PLAYERTARGET SCRIPT && NAVMESHELEVATOR SCRIPT && AreaVolumeObject
    public static event PathSliderEvent OnReachStart;
    public static event PathSliderEvent OnReachEnd;
    public static event PathSliderEvent OnLeaveStart;
    public static event PathSliderEvent OnLeaveEnd;
    public static event PathSliderEvent OnReachCheckPoint;
    public static event PathSliderEvent OnReachGoal;
    public static event PathSliderEvent OnMouse_Down;
    public static event PathSliderEvent OnMouse_Drag;
    public static event PathSliderEvent OnMouse_Up;

    private void Start()
    {
        if (interactable.rigidBody != null)
        {
            rb = interactable.rigidBody;
            rbIsKinematic = rb.isKinematic;
            rbUseGravity = rb.useGravity;
        }
        
        InitializeSlider();  //AUTOMATICALLY FIND INITIAL POSITION:
    }

    public override void InteractableAction()
    {
        base.InteractableAction();
    }

    private void OnMouseDown()
    {
        if (interactable.rigidBody != null)
        {
            rb.isKinematic = true;
            rb.useGravity = false;
        }

        cam = Camera.main;
        camTransform = cam.transform;
    }

    private void OnMouseDrag()
    {
        if (!lockBehaviour)
        {          
            if (inputType == InputTypes.DragDirect)
            {
                DragSpeed();
            }

            ReachCheckpoint();
            SliderPosition();
            SliderLocalRotation();

            lastSegmentPos = segmentProgression;
        }
    }

    public void SliderPosition()
    {       
        //INSERT BOUNDS CHECK HERE:
            segmentProgressionRound = Mathf.Round(segmentProgression * stepSize) / stepSize;
            var pos = lastCheckPoint.position + ((nextCheckPoint.position - lastCheckPoint.position) * segmentProgressionRound);
            var lastPos = lastCheckPoint.position + ((nextCheckPoint.position - lastCheckPoint.position) * lastSegmentPos);

            interactable.mainObject.transform.position = pos;
            lastPos = interactable.mainObject.transform.position;
    }

    //CHECKPOINT COUNTER = CHECKPOINTS.COUNT, NOT CHECKPOINTS.INDEXOF
    private void CheckPointProgression()
    {
        //print("hello!");
        if (checkPointCounter < checkPoints.Count)
        {            
            checkPointCounter += 1;
        }
        if (checkPointCounter < checkPoints.Count)
        {
            nextCheckPoint = checkPoints[checkPointCounter];
            lastCheckPoint = checkPoints[checkPointCounter - 1];
        }
        if(checkPointCounter == checkPoints.Count)
        {
            nextCheckPoint = checkPoints[checkPointCounter - 1];
            lastCheckPoint = checkPoints[checkPointCounter - 1];
        }
        
        if (checkPointCounter < checkPoints.Count)
        {
            //if(!isAtEnd)
                segmentProgression = 0.05f;
        } 
    }

    private void CheckPointRegression()
    {
        LeaveEnd();

        if (checkPointCounter > 1)
        {            
            checkPointCounter -= 1;
        }
        if (checkPointCounter > 1)
        {
            lastCheckPoint = checkPoints[checkPointCounter - 1];
            nextCheckPoint = checkPoints[checkPointCounter];
        }
        if (checkPointCounter == 1)
        {
            lastCheckPoint = checkPoints[0];
            nextCheckPoint = checkPoints[checkPointCounter];
        }
        if (checkPointCounter >= 1)
        {
            //if (!isAtStart)
            if (!isAtStart && checkPointCounter < checkPoints.Count) ////!!!!!
                segmentProgression = 0.95f;
        }       
    }

    private void SliderLocalRotation()
    {
        interactable.mainObject.transform.localRotation = lastCheckPoint.localRotation;
    }

    private void DragSpeed()
    {
        if (inputPlane == InputPlanes.Horizontal)
        {
            if(!isNegative && !isInverted)
                inputSpeedDirection = interactable.mainObject.transform.forward;
            else
                inputSpeedDirection = -interactable.mainObject.transform.forward;

            dragSpeedDirection = camTransform.InverseTransformDirection(inputSpeedDirection);
            dragSpeedDirectionPlanar = new Vector3(dragSpeedDirection.x * Input.GetAxis("Mouse X"), 0, dragSpeedDirection.z * Input.GetAxis("Mouse Y"));
        }

        if (inputPlane == InputPlanes.Vertical)
        {
            if (!isNegative && !isInverted)
                inputSpeedDirection = interactable.mainObject.transform.up;
            else
                inputSpeedDirection = -interactable.mainObject.transform.up;

            dragSpeedDirection = camTransform.InverseTransformDirection(inputSpeedDirection);
            dragSpeedDirectionPlanar = new Vector3(dragSpeedDirection.x * Input.GetAxis("Mouse X"), dragSpeedDirection.y * Input.GetAxis("Mouse Y"), 0);
        }

        Vector3 currentSpeed;

        if(pathProgression < 0.01f || pathProgression > 0.99f)
            currentSpeed = dragSpeedDirectionPlanar.normalized * (1.25f) * Time.deltaTime;
        else
            currentSpeed = dragSpeedDirectionPlanar.normalized * (speed / 2) * Time.deltaTime;

        float positive = 1;
        float negative = -1;
        
        //InsertSliderCheck!!!!!!!!!!!!!!!!!!!!!!
        if (boundingBox)
        {
            lockPositive = false;
            lockNegative = false;
            
            if (HelperFunctions.PointInOABB(point.position, boundingBox))
            {

            }
            else
            {
                //print("Hit bounds, locking progression...");

                if (!isNegative)
                    lockPositive = true;
                else
                    lockNegative = true;
            }
        }
        //

        if (lockPositive)
            positive = 0;
        if (lockNegative)
            negative = 0;

        //if (checkPointCounter < checkPoints.Count && !(checkPointCounter == 1 && segmentProgression == 0))
        if (!(checkPointCounter == 1 && segmentProgression == 0))
        {
            if (inputPlane == InputPlanes.Horizontal)
                segmentProgression += (Mathf.Clamp(currentSpeed.x, negative, positive) + Mathf.Clamp(currentSpeed.z, negative, positive));
            if (inputPlane == InputPlanes.Vertical)
                segmentProgression += (Mathf.Clamp(currentSpeed.x, negative, positive) + Mathf.Clamp(currentSpeed.y, negative, positive));
        }
        else if(checkPointCounter == 1 && segmentProgression == 0)
        {
            currentSpeed *= 2f;

            if (inputPlane == InputPlanes.Horizontal)
                segmentProgression += (Mathf.Clamp(currentSpeed.x, negative, positive) + Mathf.Clamp(currentSpeed.z, negative, positive));
            if (inputPlane == InputPlanes.Vertical)
                segmentProgression += (Mathf.Clamp(currentSpeed.x, negative, positive) + Mathf.Clamp(currentSpeed.y, negative, positive));
            //print(currentSpeed);            
        }
    }
    
    private void ReachCheckpoint()
    {
        if(checkPointCounter == checkPoints.Count)
        {
            if(segmentProgression != 1)
            {
                checkPointCounter -= 1;
                isAtEnd = false;

                if (OnLeaveEnd != null)
                {
                    OnLeaveEnd(interactable.mainObject.transform);
                    //print(transform.name + " leaves End..");
                }
            }
        }

        if (segmentProgression >= 0.98f)
        {
            segmentProgression = 1;

            ReachCheckpointEvents();

            if (checkPointCounter == checkPoints.Count - 1)
            {
                if (!isAtEnd)
                {
                    if (OnReachEnd != null)
                    {
                        OnReachEnd(interactable.mainObject.transform);
                        //print(transform.name + " reaches End..");
                    }
                }

                checkPointCounter += 1;
                isAtEnd = true;
            }
        }
        if (segmentProgression <= 0.02f)
        {
            segmentProgression = 0;

            ReachCheckpointEvents();

            if (checkPointCounter == 1)
            {
                if (!isAtStart)
                {
                    if (OnReachStart != null)
                    {
                        OnReachStart(interactable.mainObject.transform);
                        //print(transform.name + " reaches Start..");
                    }
                }

                isAtStart = true;
            }
        }

        if (segmentProgression == 1)
        {
            if(!isAtEnd)
            {
                CheckPointProgression();
            }           
        }
        else if(segmentProgression == 0)
        {
            if(!isAtStart)
            {
                CheckPointRegression();
            }
        }       
        if (isAtStart)
        {
            if (segmentProgression != 0)
            {
                if (OnLeaveStart != null)
                {
                    OnLeaveStart(interactable.mainObject.transform);
                    //print(transform.name + " leaves Start..");
                }

                isAtStart = false;
            }
        }
    }

    void LeaveEnd()
    {
        if (isAtEnd)
        {
            if (OnLeaveEnd != null)
            {
                OnLeaveEnd(interactable.mainObject.transform);
                //print(transform.name + " leaves End..");
            }
        }

        isAtEnd = false;
    }

    void ReachCheckpointEvents()
    {
        if (OnReachCheckPoint != null)
        {
            OnReachCheckPoint(interactable.mainObject.transform);
        }

        if (goal)
        {
            if (nextCheckPoint == goal)
            {
                if (OnReachGoal != null)
                {
                    OnReachGoal(interactable.mainObject.transform);
                }
            }
        }
    }

    private void GetPathProgression()
    {
        if (currentTotalLength == 0)
        {
            for (int i = 0; i < checkPoints.Count - 1; i++)
            {
                currentTotalLength += Vector3.Distance(checkPoints[i + 1].position, checkPoints[i].position);
            }
        }

        var currentSegmentProgDist = Vector3.Distance(interactable.mainObject.transform.position, lastCheckPoint.position);
        float testLength = 0f;

        for (int i = 0; i < checkPoints.IndexOf(lastCheckPoint); i++)
        {
            testLength += Vector3.Distance(checkPoints[i + 1].position, checkPoints[i].position);
        }

        testLength += currentSegmentProgDist;

        pathProgression = ((testLength * 100f) / currentTotalLength) / 100;
    }

    public void InitializeSlider()
    {
        if (!nextCheckPoint && !lastCheckPoint)
        {
            List<Transform> checkpointsByDistance = new List<Transform>();

            foreach (var item in checkPoints)
            {
                checkpointsByDistance.Add(item);
            }

            checkpointsByDistance.Sort(delegate (Transform a, Transform b)
            {
                return (((interactable.mainObject.transform.position - a.position).sqrMagnitude)
                .CompareTo(
                    (interactable.mainObject.transform.position - b.position).sqrMagnitude));
            });

            if (checkPoints.IndexOf(checkpointsByDistance[0]) < checkPoints.IndexOf(checkpointsByDistance[1]))
            {
                lastCheckPoint = checkpointsByDistance[0];
                nextCheckPoint = checkpointsByDistance[1];
            }
            else
            {
                lastCheckPoint = checkpointsByDistance[1];
                nextCheckPoint = checkpointsByDistance[0];
            }
        }

        if (!nextCheckPoint && lastCheckPoint)
        {
            for (int i = 0; i < checkPoints.Count; i++)
            {
                if (lastCheckPoint == checkPoints[i])
                {
                    if (i <= checkPoints.Count - 2)
                    {
                        nextCheckPoint = checkPoints[i + 1];
                    }
                    else
                    {
                        nextCheckPoint = checkPoints[checkPoints.Count - 1];
                        lastCheckPoint = checkPoints[checkPoints.Count - 2];
                    }

                    break;
                }
                if (i == checkPoints.Count - 1 && lastCheckPoint != checkPoints[i])
                {
                    Debug.LogError("Object of 'Last Check Point' is not a member of Chek Points list. Please use a Check Point member.");
                }
            }
        }

        if (nextCheckPoint && !lastCheckPoint)
        {
            for (int i = 0; i < checkPoints.Count; i++)
            {
                if (nextCheckPoint == checkPoints[i])
                {
                    if (i <= checkPoints.Count - 1)
                    {
                        lastCheckPoint = checkPoints[i - 1];
                    }
                    else
                    {
                        lastCheckPoint = checkPoints[0];
                        lastCheckPoint = checkPoints[1];
                    }

                    break;
                }
                if (i == checkPoints.Count - 1 && lastCheckPoint != checkPoints[i])
                {
                    Debug.LogError("Object of 'Next Check Point' is not a member of Chek Points list. Please use a Check Point member.");
                }
            }
        }

        checkPointCounter = 1 + checkPoints.IndexOf(lastCheckPoint);

        interactable.mainObject.transform.position = lastCheckPoint.position + ((nextCheckPoint.position - lastCheckPoint.position) * segmentProgression);
        interactable.mainObject.transform.localRotation = lastCheckPoint.localRotation;
        lastPos = interactable.mainObject.transform.position;

        if(boundingBox)
        {
            if (pointToCheck)
                point = pointToCheck;
            else
                point = interactable.mainObject.transform;
        }

        //FIND TOTAL PROGRESSION:

        currentTotalLength = 0;

        GetPathProgression();
    }

    private void OnMouseUp()
    {
        GetPathProgression();

        if (interactable.rigidBody != null)
        {
            rb.isKinematic = rbIsKinematic;
            rb.useGravity = rbUseGravity;
        }

        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnEnable()
    {
        segmentProgressionRound = Mathf.Round(segmentProgression * stepSize) / stepSize;

        if (initializeOnEnable)
        {
            InitializeSlider();
        }
    }

    private void OnDisable()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDestroy()
    {
        if (tempObj)
            Destroy(tempObj);

        isInitialized = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        for (int i = 0; i < checkPoints.Count; i++)
        {
            if (i <= checkPoints.Count - 2)
            {
                Gizmos.DrawLine(checkPoints[i].position, checkPoints[i + 1].position);

                if (checkPointsAutoLook)
                {
                    checkPoints[i].transform.LookAt(checkPoints[i + 1], Vector3.up);
                }
            }
        }
    }
}
