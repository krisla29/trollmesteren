﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePressActivator : MonoBehaviour
{
    public bool activate;
    public Interactable interactable;
    public List<Transform> interactablesToActivate = new List<Transform>();
    public bool interactablePressed;

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += CheckInteractablePress;
        Interactable.OnInteractableReleased += CheckInteractableRelease;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed -= CheckInteractablePress;
        Interactable.OnInteractableReleased -= CheckInteractableRelease;
    }
    
    private void Start()
    {
        if (activate)
        {
            foreach (var item in interactablesToActivate)
            {
                item.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (var item in interactablesToActivate)
            {
                item.gameObject.SetActive(true);
            }
        }
    }

    void CheckInteractablePress(Transform passedInteractable)
    {
        if(passedInteractable == interactable.transform)
        {
            interactablePressed = true;

            if (activate)
            {
                foreach (var item in interactablesToActivate)
                {
                    item.gameObject.SetActive(true);
                }
            }
            else
            {
                foreach (var item in interactablesToActivate)
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
    }

    void CheckInteractableRelease(Transform passedInteractable)
    {
        if (passedInteractable == interactable.transform)
        {
            interactablePressed = false;

            if (activate)
            {
                foreach (var item in interactablesToActivate)
                {
                    item.gameObject.SetActive(false);
                }
            }
            else
            {
                foreach (var item in interactablesToActivate)
                {
                    item.gameObject.SetActive(true);
                }
            }
        }
    }
}
