﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InputManager : MonoBehaviour
{
    public GameManager gameManager;
    public GameManager.ControlsMode controlsMode;
    public bool isTouch;
    public bool aim;
    //Get From HUD Canvas:
    public Joystick leftJoystick;
    public Joystick rightJoystick;
    public ActionButton actionButton;
    public AimButton aimButton;
    public Button cancelButton;
    public Slider zoomSlider;
    public Slider rotateSlider;
    private RotationSliderHandle rotateSliderHandle;

    public bool isPressing;
    //public AimRect aimRect;
    //public Button hitArea;
    public bool rotationHandlePressed;
    public bool isSetRotation;
    public float rotationLevel;
    public float zoomLevel;    
    public bool release;
    public float touchCamSpeed = 1f;
    private float currentCamSpeed;
    public int actionClickCounter;
    public float rotateBackWaitTime = 10f;
    private float rotateTimer;

    public delegate void InputManagerEvents();
    public static event InputManagerEvents OnInputMouseDown;
    public static event InputManagerEvents OnInputMouseUp;

    public void Start()
    {        
        actionButton.GetComponent<Button>().onClick.AddListener(() => ActionClick(actionClickCounter));
        rotateSliderHandle = rotateSlider.GetComponent<RotationSliderHandle>();
        //aimRect.GetComponent<Button>().onClick.AddListener(AimLook);
        currentCamSpeed = touchCamSpeed;
        controlsMode = gameManager.controlsMode;
        zoomLevel = gameManager.startZoom;
        rotationLevel = 0;
    }
    public void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            if(OnInputMouseDown != null)
            {
                OnInputMouseDown();
            }

            isPressing = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (OnInputMouseUp != null)
            {
                OnInputMouseUp();
            }

            isPressing = false;
        }
        // TEST REMOVE:
        
        rotationHandlePressed = rotateSliderHandle.isPressed;

        if (rotationHandlePressed)
        {
            rotateTimer = 0;
            isSetRotation = true;
        }
        /*
        if (!rotationHandlePressed)
        {
            rotateTimer += Time.deltaTime;

            if (rotateSlider.value - 0.500f != 0 && rotateTimer > rotateBackWaitTime)
            {
                rotateSlider.value = Mathf.Lerp(rotateSlider.value, 0.500f, Time.deltaTime);
                isSetRotation = false;
            }
        }
        */
        if (!rotationHandlePressed)
        {
            //rotateTimer += Time.deltaTime;

            if (rotateSlider.value - 0.500f != 0 && gameManager.hudCanvas.lockRotationToggle.isOn)
            {
                rotateSlider.value = Mathf.Lerp(rotateSlider.value, 0.500f, Time.deltaTime);
                isSetRotation = false;
            }
        }
        //
        if (gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            isTouch = true;
        }
        else
        {
            isTouch = false;
        }

        aim = aimButton.isOn;

        if(aim && gameManager.hasFirstPersonAim)
        {
            touchCamSpeed = currentCamSpeed / 5f; 
        }
        else
        {
            touchCamSpeed = currentCamSpeed / 2f;
        }

        if(gameManager.controlsMode != GameManager.ControlsMode.Mobile && gameManager.controlsMode != GameManager.ControlsMode.GamePad)
        {
            ZoomBuilder();
            RotationBuilder();
        }
    }
    
    public bool GetRotateHandle()
    {
        return rotateSliderHandle.isPressed;
    }

    public void ActionClick(int number)
    {
        actionClickCounter += 1;
    }
    public void AimLook()
    {
        //print("AimLook");
    }

    public float LeftHorizontal()
    {
        if (!isTouch)
        {
            float r = 0.0f;
            r += Input.GetAxis("Horizontal");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            return leftJoystick.Horizontal;
        }
    }

    public float LeftVertical()
    {
        if (!isTouch)
        {
            float r = 0.0f;
            r += Input.GetAxis("Vertical");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            return leftJoystick.Vertical;
        }
    }

    public Vector2 LeftAxis()
    {
        if (!isTouch)
        {
            return new Vector2(LeftHorizontal(), LeftVertical());
        }
        else
        {
            return leftJoystick.Direction;
        }
    }

    //---------------------------------------------------------------

    public float RightHorizontal()
    {
        if (!isTouch)
        {
            float r = 0.0f;
            r += Input.GetAxisRaw("Mouse X");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            float r = 0.0f;
            r += rightJoystick.Horizontal * touchCamSpeed;
            return Mathf.Clamp(r, -1.0f, 1.0f);
            //return rightJoystick.Horizontal;
        }
    }

    public float RightVertical()
    {
        if (!isTouch)
        {
            float r = 0.0f;
            r += Input.GetAxisRaw("Mouse Y");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            float r = 0.0f;
            r += rightJoystick.Vertical * touchCamSpeed;
            return Mathf.Clamp(r, -1.0f, 1.0f);
            //return rightJoystick.Vertical;
        }
    }

    public Vector2 RightAxis()
    {
        if (!isTouch)
        {
            return new Vector2(RightHorizontal(), RightVertical());
        }
        else
        {
            return new Vector2(RightHorizontal(), RightVertical());
            //return rightJoystick.Direction;
        }
    }
    
    public bool Action()
    {
        if (!isTouch)
        {
            return Input.GetButton("Fire1");
        }
        else
        {
            return actionButton.isPressed;
            //return false;
        }
    }
    public bool ActionUp()
    {
        if (!isTouch)
        {
            return Input.GetButtonUp("Fire1");
        }
        else
        {
            return actionButton.isPressed;
            //return actionButtonScript.ActionButtonUp();
        }
    }
    public bool ActionDown()
    {
        if (!isTouch)
        {
            return Input.GetButtonDown("Fire1");
        }
        else
        {
            return actionButton.isPressed;
            //return actionButtonScript.ActionButtonDown();
        }
    }
    public bool Aim()
    {
        if (!isTouch)
        {
            return Input.GetMouseButton(1);
        }
        else
        {
            //return aimButton.isPressed;
            return false;
        }
    }
    public bool AimUp()
    {
        if (!isTouch)
        {
            return Input.GetMouseButtonUp(1);
        }
        else
        {
            //return aimButton.isOn;
            return false;
        }
    }
    public bool AimDown()
    {
        if (!isTouch)
        {
            return Input.GetMouseButtonDown(1);
        }
        else
        {
            //return aimButton.isPressed;
            //return aimButton.isPressed;
           return false;
        }
    }
    /*
    public bool AimMobileDown()
    {
        return aimRect.isPressed;
    }

    public bool AimMobileUp()
    {
        return aimRect.isPressed;
    }
    */
    public float Zoom()
    {
        if(!isTouch)
        {
            if (controlsMode == GameManager.ControlsMode.PC)
            {
                return Mathf.Clamp(zoomLevel, 0, 1.0f);
            }
            else if (controlsMode == GameManager.ControlsMode.GamePad)
            {
                zoomLevel += Input.GetAxisRaw("Mouse ScrollWheel");
                return Mathf.Clamp(zoomLevel, 0, 1.0f);
            }
            else
            {
                return zoomLevel;
            }
        }
        else
        {
            return zoomSlider.value;
        }
    }
    public void ZoomBuilder()
    {       
        if(Input.GetKeyDown("[-]"))
        {
            if(zoomLevel < 1)
            {
                zoomLevel += 0.1f;
                print(zoomLevel);
            }           
        }
        else if(Input.GetKeyDown("[+]"))
        {
            if (zoomLevel > 0)
            {
                zoomLevel -= 0.1f;
                print(zoomLevel);
            }
        }

        zoomSlider.value = zoomLevel;
    }

    public float RotateCamera()
    {
        if (!isTouch)
        {
            if (controlsMode == GameManager.ControlsMode.PC)
            {
                return Mathf.Clamp(rotationLevel, -0.500f, 0.500f);
            }
            else if (controlsMode == GameManager.ControlsMode.GamePad)
            {
                //rotationLevel += Input.GetAxisRaw("Mouse ScrollWheel");
                return Mathf.Clamp(rotationLevel, -0.500f, 0.500f);
            }
            else
            {
                return Mathf.Clamp(rotationLevel, -0.500f, 0.500f);
            }
        }
        else
        {
            return Mathf.Clamp(rotateSlider.value - 0.500f, -0.500f, 0.500f);
        }
    }
    public void RotationBuilder()
    {
        if (Input.GetKey("e"))
        {
            if (rotationLevel < 1)
            {
                rotationLevel += 0.005f;
                //print(zoomLevel);
            }
        }
        else if (Input.GetKey("q"))
        {
            if (rotationLevel > 0)
            {
                rotationLevel -= 0.005f;
                //print(zoomLevel);
            }
        }

        var sliderValueTransposed = Mathf.Clamp(rotateSlider.value - 0.500f, -0.500f, 0.500f);
        sliderValueTransposed = rotationLevel;
        //rotateSlider.value = rotationLevel;
    }

    public bool Cancel()
    {
        return Input.GetKey(KeyCode.Escape);
    }
    public bool CancelUp()
    {
        return Input.GetKeyUp(KeyCode.Escape);
    }
    public bool CancelDown()
    {
        return Input.GetKeyDown(KeyCode.Escape);
    }
    /*
    private IEnumerator SlideRotationBack()
    {
        float t = 0;
    }
    */
    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}


