﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DrillGameQuestion : MonoBehaviour
{
    public string question;
    public string rightAnswer;
    public string wrongAnswerX;
    public string wrongAnswerY;
    public string wrongAnswerZ;
}
