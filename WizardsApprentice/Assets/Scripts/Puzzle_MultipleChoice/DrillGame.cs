﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DrillGame : MonoBehaviour
{
    public GameManager gameManager;
    public MultipleChoiceHolder mc_Holder;
    public TextMeshProUGUI questionSlot;
    public List<Button> answerSlots = new List<Button>();
    public List<TextMeshProUGUI> answerSlotsTexts = new List<TextMeshProUGUI>();

    //public List<GameObject> questions = new List<GameObject>();
    public Transform questionsParent;

    public delegate void DrillGameEvent();
    public static event DrillGameEvent OnRightAnswer;
    public static event DrillGameEvent OnWrongAnswer;

    private GameObject currentQuestionObject;
    [SerializeField] private int randNum;
    public MultipleChoiceTask currentQuestion;

    public AudioSource soundCorrect;
    public AudioSource soundWrong;

    private List<Color> startColors = new List<Color>();

    private void Awake()
    {
        startColors.Add(questionSlot.color);

        for (int i = 0; i < answerSlotsTexts.Count; i++)
        {
            startColors.Add(answerSlotsTexts[i].color);
        }
        /*
        foreach (Transform child in questionsParent)
        {
            questions.Add(child.gameObject);
        }
        */
    }

    void Start()
    {
        answerSlots[0].onClick.AddListener(delegate { CheckAnswer(0); });
        answerSlots[1].onClick.AddListener(delegate { CheckAnswer(1); });
        answerSlots[2].onClick.AddListener(delegate { CheckAnswer(2); });
        answerSlots[3].onClick.AddListener(delegate { CheckAnswer(3); });
    }

    private void OnEnable()
    {
        StopAllCoroutines();
        Reset();

        NextQuestion();
    }

    private void OnDisable()
    {
        Reset();

        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void Reset()
    {
        if (startColors.Count == 5)
        {
            questionSlot.color = startColors[0];

            for (int i = 0; i < answerSlotsTexts.Count; i++)
            {
                answerSlotsTexts[i].color = startColors[i + 1];
            }
        }

        for (int i = 0; i < answerSlots.Count; i++)
        {
            answerSlots[i].enabled = true;
        }
    }

    void NextQuestion()
    {
        int randDifficulty = Random.Range(0, 3);
        List<MultipleChoiceTask> curList;

        if (randDifficulty == 0)
            curList = mc_Holder.easyTasks;
        else if (randDifficulty == 1)
            curList = mc_Holder.normalTasks;
        else
            curList = mc_Holder.hardTasks;

        int randQuestion = Random.Range(0, curList.Count);

        currentQuestion = curList[randQuestion];

        questionSlot.text = currentQuestion.question;

        randNum = Random.Range(0, answerSlots.Count);

        int randAlternative = Random.Range(0,3);

        List<int> wrongs = new List<int>();
        
        for (int i = 0; i < answerSlots.Count; i++)
        {
            if (i == randNum)
            {
                answerSlotsTexts[i].text = currentQuestion.rightAnswer;
            }
            else
            {
                wrongs.Add(i);
                
            }
        }

        HelperFunctions.ShuffleList(wrongs);

        for (int i = 0; i < wrongs.Count; i++)
        {
            answerSlotsTexts[wrongs[i]].text = currentQuestion.wrongAnswers[i];
        }
    }

    private IEnumerator CoolDown()
    {
        for (int i = 0; i < answerSlots.Count; i++)
        {
            answerSlots[i].enabled = false;
        }

        float fadeTime = 1.5f;
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(t);
            questionSlot.color = Vector4.Lerp(startColors[0], startColors[0] * 0, h);
            for (int i = 0; i < answerSlotsTexts.Count; i++)
            {
                answerSlotsTexts[i].color = Vector4.Lerp(startColors[i + 1], startColors[i + 1] * 0, h);
            }
            yield return new WaitForEndOfFrame();
        }

        NextQuestion();

        float e = 0;

        while (e < 1)
        {
            e += Time.deltaTime / fadeTime;
            var h = LerpUtils.SmootherStep(e);
            questionSlot.color = Vector4.Lerp(startColors[0] * 0, startColors[0], h);
            for (int i = 0; i < answerSlotsTexts.Count; i++)
            {
                answerSlotsTexts[i].color = Vector4.Lerp(startColors[i + 1] * 0, startColors[i + 1], h);
            }
            yield return new WaitForEndOfFrame();
        }

        for (int i = 0; i < answerSlots.Count; i++)
        {
            answerSlots[i].enabled = true;
        }
    }

    void CheckAnswer(int buttonNumber)
    {
        print("Checking Answer On Button Number " + buttonNumber);

        if(buttonNumber == randNum)
        {
            print("Right Answer");
            if (soundCorrect)
                soundCorrect.Play();
            gameManager.player.anim.SetTrigger("Success");
            RightAnswer();
            StopAllCoroutines();
            StartCoroutine(CoolDown());
        }
        else
        {
            print("Wrong Answer");
            if (soundWrong)
                soundWrong.Play();
            WrongAnswer();
            StopAllCoroutines();
            StartCoroutine(CoolDown());
        }
    }
    void RightAnswer()
    {
        if (OnRightAnswer != null)
        {
            OnRightAnswer();
        }
    }
    void WrongAnswer()
    {
        if (OnWrongAnswer != null)
        {
            OnWrongAnswer();
        }
    }
}
