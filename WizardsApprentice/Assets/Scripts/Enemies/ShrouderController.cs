﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrouderController : MonoBehaviour
{
    public Enemies.EnemyType enemyType;
    public PlayerReference playerReference;
    public GameManager gameManager;
    public ShrouderSpawner shrouderSpawner;
    public Transform scaleTransform;
    public float startScale = 2f;
    public Transform slider;
    public SpriteRenderer angryFace;
    public SpriteRenderer idleFace;
    public Transform sleepFace;
    public ParticleSystem pSystem;
    public ParticleSystem.MainModule pSystemMain;
    public ParticleSystemRenderer pSystemRenderer;
    public ParticleSystem.EmissionModule pSystemEmission;
    public AddNoiseToProperty generalNoise;
    public float generalNoiseMult = 0.2f;
    public Vector2 generalNoiseSpeed = new Vector2(0.5f, 0.5f);
    public float pSystemIdleSpeed = 0.3f;
    public float pSystemAngrySpeed = 3f;
    public AddNoiseToProperty patrolLookNoise;
    public AddNoiseToProperty angryNoise;
    public float angryNoiseMult = 0.1f;
    public Vector2 angryNoiseSpeed;
    public MeshRenderer meshRenderer;
    public Color idleColor = Color.magenta;
    public Color angryColor = Color.black;
    //public Animator animator;
    public Follower followerScript;
    public ShrouderDetector shrouderDetector;
    public ShrouderCollider shrouderCollider;
    public float detectorStartScale = 7f;
    public float detectorCheckTime = 0.5f;
    public float shrouderSpeed = 5f;
    public Transform sleepEffect;

    public enum ShrouderStates { Patrol, Attack, Battle, Death}
    public ShrouderStates shrouderState;
    //public Vector3 offsetToPlayer;
    public Transform playerTarget;
    private Transform currentTarget;
    public bool isLocked;
    public bool isSleeping;

    public delegate void ShrouderControllerEvent(ShrouderController thisController); //Sends to ShrouderSpawner
    public static event ShrouderControllerEvent OnShrouderDeath;

    private void Start()
    {           
        StartCoroutine(GrowUp());
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        playerReference = gameManager.playerReference;
        followerScript.enabled = true;
        shrouderState = ShrouderStates.Patrol;
        pSystemMain = pSystem.main;
        pSystemEmission = pSystem.emission;
        pSystemRenderer = pSystem.GetComponent<ParticleSystemRenderer>();
        pSystemMain = pSystem.main;
        ResetNoise();
        SetIdleBehaviour();
    }

    private void Update()
    {
        CheckState();
        CheckTimeOfDay();
    }

    private void OnEnable()
    {
        ShrouderCollider.OnHitPlayer += SetToBattle;
    }

    private void OnDisable()
    {
        ShrouderCollider.OnHitPlayer -= SetToBattle;
        StopAllCoroutines();
    }

    void ResetNoise()
    {
        generalNoise.properties[0].multiplier = generalNoiseMult;
        generalNoise.properties[0].noiseSpeed = generalNoiseSpeed;
        angryNoise.properties[0].multiplier = angryNoiseMult;
        angryNoise.properties[0].noiseSpeed = angryNoiseSpeed;
    }

    void Patrol()
    {
        if (slider)
        {
            followerScript.target = slider;
        }
        else
        {
            followerScript.target = transform;
        }
    }

    void CheckState()
    {
        if(shrouderDetector.playerInsideDetector)
        {
            if (shrouderState != ShrouderStates.Battle)
            {
                SetAngryBehaviour();

                if (playerReference)
                {
                    if (playerReference.player)
                    {
                        followerScript.target = playerReference.player;
                    }
                }
            }
        }
        else
        {
            if (shrouderState != ShrouderStates.Battle)
            {
                shrouderState = ShrouderStates.Patrol;
                SetIdleBehaviour();
                Patrol();
            }
        }

        if(shrouderState == ShrouderStates.Battle) // ExitBattleState
        {
            if(!shrouderSpawner.battleManager.battleCollider.playerInsideCollider)
            {
                shrouderSpawner.battleManager.currentEnemy = null;
                shrouderSpawner.battleManager.DeactivateBattle();
                patrolLookNoise.transform.localRotation = Quaternion.identity;
                shrouderDetector.enabled = true;
                shrouderCollider.enabled = true;
                followerScript.enabled = true;
                if (slider)
                    followerScript.target = slider;

                ResetNoise();
                SetIdleBehaviour();
                scaleTransform.localScale = Vector3.one * startScale;
                shrouderState = ShrouderStates.Patrol;                
            }
        }
    }

    void CheckTimeOfDay()
    {
        if (gameManager.playerStats.timeGameScript.timeOfDay != TimeGame.TimeOfDay.Night)
        {
            isSleeping = false;

            if(sleepEffect.gameObject.activeSelf)
                sleepEffect.gameObject.SetActive(false);
        }
        else
        {
            isSleeping = true;

            if (shrouderState == ShrouderStates.Patrol)
            {
                if(!sleepEffect.gameObject.activeSelf)
                    sleepEffect.gameObject.SetActive(true);                
            }
            else
            {
                if(sleepEffect.gameObject.activeSelf)
                    sleepEffect.gameObject.SetActive(false);
            }
        }
    }

    void SetToBattle(Transform passedColliderTransform) // To be moved to battle scripts......
    {
        if(passedColliderTransform == transform)
        {            
            shrouderDetector.playerInsideDetector = false;
            shrouderDetector.enabled = false;
            shrouderCollider.enabled = false;

            shrouderSpawner.battleManager.currentEnemy = this; 
            shrouderSpawner.battleManager.ActivateBattle();
            shrouderSpawner.battleManager.spawnerTransform.SetParent(angryNoise.transform);
            shrouderSpawner.battleManager.spawnerTransform.localPosition = Vector3.zero;

            SetAngryBehaviour();

            if (playerReference)
            {
                if (playerReference.player)
                {
                    followerScript.enabled = false;                    
                }
            }
            transform.rotation = Quaternion.identity;
            var dir = (transform.position - playerReference.player.transform.position).normalized;
            patrolLookNoise.transform.localRotation = Quaternion.LookRotation(-dir);                
            shrouderState = ShrouderStates.Battle;
        }
    }

    void SetAngryBehaviour()
    {
        idleFace.gameObject.SetActive(false);
        angryFace.gameObject.SetActive(true);
        patrolLookNoise.enabled = false;
        patrolLookNoise.transform.localEulerAngles = Vector3.zero;
        angryNoise.enabled = true;
        pSystemMain.startSpeed = pSystemAngrySpeed;
        meshRenderer.material.color = angryColor;
        followerScript.followProperty01.speed = 5f;
        followerScript.followProperty02.speed = 13f;
        //pSystemMain.startColor = angryColor;
        pSystemMain.startSize = 0.5f;
        pSystemMain.startLifetime = 1f;
        pSystemRenderer.material.color = angryColor;
        pSystemEmission.rateOverTime = 10f;
        pSystemMain.startSpeed = 1.5f;
    }

    void SetIdleBehaviour()
    {
        idleFace.gameObject.SetActive(true);
        angryFace.gameObject.SetActive(false);        
        angryNoise.enabled = false;
        pSystemMain.startSpeed = pSystemIdleSpeed;
        meshRenderer.material.color = idleColor;

        if (!isSleeping)
        {
            idleFace.gameObject.SetActive(true);
            sleepFace.gameObject.SetActive(false);
            followerScript.followProperty01.speed = 3f;
            followerScript.followProperty02.speed = 3f;
            patrolLookNoise.enabled = true;
            pSystemMain.startSize = 1f;
            pSystemMain.startLifetime = 0.5f;
            pSystemRenderer.material.color = idleColor;
            pSystemEmission.rateOverTime = 10f;
            pSystemMain.startSpeed = 1;
            shrouderDetector.transform.localScale = Vector3.one * detectorStartScale;
        }
        else
        {
            idleFace.gameObject.SetActive(false);
            sleepFace.gameObject.SetActive(true);
            followerScript.followProperty01.speed = 0f;
            followerScript.followProperty02.speed = 0f;
            patrolLookNoise.enabled = false;
            pSystemMain.startSize = 0.75f;
            pSystemMain.startLifetime = 0.5f;
            pSystemRenderer.material.color = idleColor;
            pSystemEmission.rateOverTime = 5f;
            pSystemMain.startSpeed = 0.75f;
            shrouderDetector.transform.localScale = Vector3.zero;
        }
        //pSystemMain.startColor = idleColor;
        
    }    

    private void OnDestroy()
    {
        if(OnShrouderDeath != null)
        {
            OnShrouderDeath(this);
        }

        StopAllCoroutines();
    }

    private IEnumerator GrowUp()
    {
        float t = 0;

        while(t < 1)
        {
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, t);
            t += Time.deltaTime / 2f;
            yield return new WaitForEndOfFrame();
        }
    }


}

