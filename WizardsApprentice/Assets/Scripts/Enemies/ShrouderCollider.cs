﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrouderCollider : MonoBehaviour
{
    public ShrouderController shrouderController;
    public delegate void ShrouderColliderEvent(Transform thisShrouderController); // Sends to ShrouderController
    public static event ShrouderColliderEvent OnHitPlayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(OnHitPlayer != null)
            {
                OnHitPlayer(shrouderController.transform);
            }
        }
    }
}
