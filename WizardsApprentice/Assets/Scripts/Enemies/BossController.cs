﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public Animator anim;
    public Transform rig;
    public LookAtObject2 lookerScript;
    public int maxIdleVariationTime = 41;
    public bool isAnimTest;

    public bool victory;
    public bool defeat;
    public bool death;
    
    void SetIdleVariation()
    {
        anim.SetTrigger("IdleVariation");
        int randTime = Random.Range(0, maxIdleVariationTime);
        Invoke("SetIdleVariation", randTime);
    }

    public int SetAttackAnim()
    {
        print("Attack!");
        ResetAllTriggers();

        int randNum = Random.Range(0, 4);

        if(randNum == 0)
            anim.SetTrigger("Attack1");
        else if (randNum == 1)
            anim.SetTrigger("Attack2");
        else if (randNum == 2)
            anim.SetTrigger("Attack3");
        else if (randNum == 3)
            anim.SetTrigger("Attack4");

        return randNum;
    }

    public void SetHitAnim()
    {
        ResetAllTriggers();

        int randNum = Random.Range(0, 3);

        if (randNum == 0)
            anim.SetTrigger("Hit1");
        else if (randNum == 1)
            anim.SetTrigger("Hit2");
        else if (randNum == 2)
            anim.SetTrigger("Hit3");
    }

    public void SetMajorHitAnim()
    {
        ResetAllTriggers();

        anim.SetTrigger("HitMajor");
    }

    public void SetCastSecondWaveAnim()
    {
        ResetAllTriggers();

        anim.SetTrigger("CastSecondWave");
    }

    public void SetDefeat(bool defeat)
    {
        anim.SetBool("IsDefeated", defeat);
    }

    public void SetFinalDefeat()
    {
        anim.SetBool("isFinalDefeated", true);
        StartCoroutine(Death());
    }

    public void ResetFinalDefeat()
    {
        anim.SetBool("isFinalDefeated", false);
    }

    public void SetVictory(bool victory)
    {
        anim.SetBool("IsVictorious", victory);
    }

    public void SetFlying(bool flying)
    {
        anim.SetBool("IsFlying", flying);
    }

    public void SetFailAnim()
    {
        ResetAllTriggers();

        anim.SetTrigger("Fail");
    }

    private void OnEnable()
    {
        float randStart = Random.Range(0, 1f);
        anim.Play(0, 0, randStart);
        transform.rotation = Quaternion.identity;
        lookerScript.enabled = true;

        //int randTime = Random.Range(0, maxIdleVariationTime);
        //Invoke("SetIdleVariation", randTime);
    }

    void ResetAllTriggers()
    {
        anim.ResetTrigger("Attack1");
        anim.ResetTrigger("Attack2");
        anim.ResetTrigger("Attack3");
        anim.ResetTrigger("Attack4");
        anim.ResetTrigger("Hit1");
        anim.ResetTrigger("Hit2");
        anim.ResetTrigger("Hit3");
        anim.ResetTrigger("HitMajor");
        anim.ResetTrigger("CastSecondWave");
    }

    private void OnDisable()
    {
        CancelInvoke();
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        CancelInvoke();
        StopAllCoroutines();
    }

    private void Update()
    {
        if(isAnimTest)
        {
            if (Input.GetKeyDown(KeyCode.K))
                SetAttackAnim();

            if (Input.GetKeyDown(KeyCode.L))
                SetHitAnim();

            if (Input.GetKeyDown(KeyCode.H))
            {
                defeat = !defeat;
                SetDefeat(defeat);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                victory = !victory;
                SetVictory(victory);
            }

            if (Input.GetKeyDown(KeyCode.G))
                SetMajorHitAnim();

            if (Input.GetKeyDown(KeyCode.F))
                SetCastSecondWaveAnim();
        }
    }

    private IEnumerator Death()
    {
        float t = 0;
        float animTime = 3f;
        lookerScript.enabled = false;
        transform.localEulerAngles = new Vector3(-90, 0, 0);
        transform.localPosition += Vector3.up * 1.5f;
        Vector3 locEulAng = transform.localEulerAngles;

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            transform.localEulerAngles = Vector3.Lerp(locEulAng, locEulAng + (Vector3.up * 360f), h);
            yield return new WaitForEndOfFrame();
        }
    }
}
