﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrouderDetector : MonoBehaviour
{
    public bool playerInsideDetector;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            playerInsideDetector = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            playerInsideDetector = false;
        }
    }
}
