﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiamondTrollController : MonoBehaviour
{
    public Animator anim;
    public int maxIdleVariationTime = 41;

    void SetIdleVariation()
    {
        anim.SetTrigger("IdleVariation");
        int randTime = Random.Range(0, maxIdleVariationTime);
        Invoke("SetIdleVariation", randTime);
    }

    public void SetAttackAnim()
    {
        anim.SetTrigger("Attack");
    }

    public void SetHitAnim()
    {
        anim.SetTrigger("Hit");
    }

    public void SetStepAnim()
    {
        anim.SetTrigger("Step");
    }

    private void OnEnable()
    {
        float randStart = Random.Range(0, 1f);
        anim.Play(0, 0, randStart);

        int randTime = Random.Range(0, maxIdleVariationTime);
        Invoke("SetIdleVariation", randTime);
    }

    private void OnDisable()
    {
        CancelInvoke();
    }

    private void OnDestroy()
    {
        CancelInvoke();
    }
}
