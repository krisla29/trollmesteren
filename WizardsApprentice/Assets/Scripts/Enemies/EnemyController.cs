﻿using UnityEngine;
using System.Collections;


public class EnemyController : MonoBehaviour
{
    public CharacterController characterController;
    public Collider FOV;
    private Transform player;
    public bool isSeeingPlayer = false;
    public bool isHit = false;
    public bool isHittingPlayer;
    //public Transform playerCam;

    public float speed = 6.0f;
    public float rotSpeed = 10f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public float distanceToPlayer = 0.8f;
    private float startSpeed;

    private Vector3 moveDirection = Vector3.zero;
    public WandInteractable wandInteractable;

    void Start()
    {
        startSpeed = speed;
    }

    void Update()
    {
        if(isHit)
        {
            //Destroy(transform.gameObject);
        }

        moveDirection.y -= gravity * Time.deltaTime;

        if (player == null)
        {
            if (GameObject.FindGameObjectWithTag("Player").transform != null)
            {
                player = GameObject.FindGameObjectWithTag("Player").transform;
            }
        }
        else
        {
            var dist = Vector3.Distance(player.position, transform.position);

            if (dist >= distanceToPlayer)
            {
                speed = startSpeed;
            }
            else
            {
                speed = 0;
            }

            if (characterController.isGrounded && isSeeingPlayer)
            {
                var newMoveDir = -(transform.position - player.position).normalized;

                moveDirection = new Vector3(newMoveDir.x, 0, newMoveDir.z);
                moveDirection *= speed;
            }

            else if (characterController.isGrounded && !isSeeingPlayer)
            {
                moveDirection = Vector3.zero;
            }
            // Move the controller
            characterController.Move(moveDirection*Time.deltaTime);

            // Rotate the controller
            if (isSeeingPlayer && dist >= distanceToPlayer)
            {
                Quaternion targetRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0, moveDirection.z), Vector3.up);

                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotSpeed);
            }
        }
        if(wandInteractable)
        {
            if(wandInteractable.isInteracting)
            {
                isHit = true;
            }
            else
            {
                isHit = false;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {       
        if(other.gameObject.tag == "Player")
        {
            isHittingPlayer = true;
        }       
    }
    private void OnTriggerExit(Collider other)
    {       
        if (other.gameObject.tag == "Player")
        {
            isHittingPlayer = false;
        }       
    }
}