﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrouderSpawner : MonoBehaviour
{
    public BattleManager battleManager;
    public BattleSort.SlotOrders battleSortMethod = BattleSort.SlotOrders.Descending;
    public GameObject shrouderPrefab;
    public int maxShrouders = 1;
    public float patrolSpeed = 1;
    public List<ShrouderController> shrouderControllers = new List<ShrouderController>();
    public List<Transform> curveSliders = new List<Transform>();
    public List<float> timeCounters = new List<float>();
    public GameObject birthEffect;
    public BezierCurve patrolCurve;
    public float waitForNewSpawn = 1000f;
    public Transform parent;
    public Transform startPos;
    public bool spawnAtStart;
    public AudioSource spawnerBirthSound;
    private GameObject currentObject;
    private float timeStamp;
    private bool timeSet = false;

    private void Start()
    {
        if (spawnAtStart)
        {
            SpawnObject();
        }
    }

    private void Update()
    {
        if (shrouderControllers.Count < maxShrouders && !timeSet)
        {
            timeStamp = Time.time;
            timeSet = true;
        }
        if (shrouderControllers.Count < maxShrouders && timeSet)
        {
            if (waitForNewSpawn <= Time.time - timeStamp)
            {
                SpawnObject();
            }
        }

        MoveSliders();
    }

    private void OnEnable()
    {
        ShrouderController.OnShrouderDeath += UpdateCurveSliderCount;
    }

    private void OnDisable()
    {
        ShrouderController.OnShrouderDeath -= UpdateCurveSliderCount;
    }

    void SpawnObject()
    {
        if (spawnerBirthSound)
        {
            spawnerBirthSound.Play();
        }

        currentObject = Instantiate(shrouderPrefab);
        var currentObjectController = currentObject.GetComponent<ShrouderController>();
        currentObjectController.shrouderSpawner = this;
        shrouderControllers.Add(currentObjectController);

        var newSlider = new GameObject("curveSlider");
        newSlider.transform.parent = patrolCurve.transform;
        curveSliders.Add(newSlider.transform);
        var newSlideObject = new BezierCurve.SlideObject();
        patrolCurve.slideObjects.Add(newSlideObject);
        newSlideObject.slideObject = newSlider.transform;
        newSlideObject.rotateWithPath = true;
        currentObjectController.slider = newSlider.transform;
        var newTimeCounter = Random.Range(0.01f, 0.99f);
        timeCounters.Add(newTimeCounter);

        if(parent)
        {
            currentObject.transform.parent = parent;
        }
        else
        {
            currentObject.transform.parent = transform;
        }

        if (birthEffect)
        {
            Instantiate(birthEffect, currentObject.transform);
        }

        if(startPos)
        {
            currentObject.transform.position = startPos.position;
        }
        else
        {
            currentObject.transform.position = transform.position;
        }
        
        timeSet = false;
    }

    public void UpdateCurveSliderCount(ShrouderController passedController)
    {
        ShrouderController controller = null;

        for (int i = 0; i < shrouderControllers.Count; i++)
        {
            if(shrouderControllers[i] == passedController)
            {
                controller = passedController;
            }
        }

        if(controller != null)
        {
            var index = shrouderControllers.IndexOf(controller);
            var lastSlider = curveSliders[index];

            curveSliders.Remove(lastSlider);
            patrolCurve.slideObjects.Remove(patrolCurve.slideObjects[index]);
            timeCounters.Remove(timeCounters[index]);
            Destroy(lastSlider.gameObject);
        }
    }

    void MoveSliders()
    {               
        for (int i = 0; i < patrolCurve.slideObjects.Count; i++)
        {
            if (timeCounters[i] >= 9.99f)
            {
                timeCounters[i] = 0.01f;
            }
            var progression = Mathf.Clamp((timeCounters[i] * 10f), 0.01f, 99.99f);
            patrolCurve.slideObjects[i].pathProgression = progression;

            timeCounters[i] += Time.deltaTime * patrolSpeed;
        }        
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
}
