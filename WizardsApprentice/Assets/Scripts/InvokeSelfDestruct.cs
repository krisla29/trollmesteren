﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeSelfDestruct : MonoBehaviour
{
    public float lifeTime = 2f;

    void Start()
    {
        Invoke("Death", lifeTime);
    }

    void Death()
    {
        Destroy(transform.gameObject);
    }
    
}
