﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablesManager : MonoBehaviour
{
    public List<Transform> wandInteractables = new List<Transform>();
    public List<Transform> enemies = new List<Transform>();

    private void Start()
    {
        foreach(var item in GameObject.FindGameObjectsWithTag("WandInteractable"))
        {
            wandInteractables.Add(item.transform);
        }
        foreach (var item in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            enemies.Add(item.transform);
        }
    }

    private void OnEnable()
    {
        WandInteractable.OnWandInteractableActive += AddWandInteractable;
        WandInteractable.OnWandInteractableDeactive += RemoveWandInteractable;
        WandInteractable.OnWandInteractableActiveEnemy += AddEnemy;
        WandInteractable.OnWandInteractableDeactiveEnemy += RemoveEnemy;
    }

    private void OnDisable()
    {
        WandInteractable.OnWandInteractableActive -= AddWandInteractable;
        WandInteractable.OnWandInteractableDeactive -= RemoveWandInteractable;
        WandInteractable.OnWandInteractableActiveEnemy -= AddEnemy;
        WandInteractable.OnWandInteractableDeactiveEnemy -= RemoveEnemy;
    }

    private void AddWandInteractable(Transform wandInteractable)
    {
        bool isNew = true;

        foreach (var item in wandInteractables)
        {
            if(item == wandInteractable)
            {
                isNew = false;
            }
        }

        if(isNew)
        {
            wandInteractables.Add(wandInteractable);
        }
    }

    private void RemoveWandInteractable(Transform wandInteractable)
    {
        bool isNew = true;

        foreach (var item in wandInteractables)
        {
            if (item == wandInteractable)
            {
                isNew = false;
            }
        }

        if(!isNew)
        {
            wandInteractables.Remove(wandInteractable);
        }
    }

    private void AddEnemy(Transform wandInteractable)
    {
        bool isNew = true;

        foreach (var item in enemies)
        {
            if (item == wandInteractable)
            {
                isNew = false;
            }
        }

        if (isNew)
        {
            enemies.Add(wandInteractable);
        }
    }

    private void RemoveEnemy(Transform wandInteractable)
    {
        bool isNew = true;

        foreach (var item in enemies)
        {
            if (item == wandInteractable)
            {
                isNew = false;
            }
        }

        if(!isNew)
        {
            enemies.Remove(wandInteractable);
        }
    }


}
