﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudArea : MonoBehaviour
{
    public List<Transform> clouds = new List<Transform>();
    public GameEvent connectedGameEvent;
    public Vector2 randMinMax = new Vector2(0, 5f);
    public Vector2 randLerpMinMax = new Vector2(3f, 10f);
    private List<int> finishedClouds = new List<int>();

    private void Awake()
    {
        clouds.Clear();

        foreach (Transform item in transform)
        {
            clouds.Add(item);
        }
    }

    private void OnEnable()
    {
        GameEvent.OnGameEventStart += StartSequence;
    }

    private void OnDisable()
    {
        GameEvent.OnGameEventStart -= StartSequence;
        StopAllCoroutines();
    }

    void StartSequence(GameEvent passedEvent)
    {
        if(connectedGameEvent == passedEvent)
        {
            for (int i = 0; i < clouds.Count; i++)
            {
                var randNum = Random.Range(randMinMax.x, randMinMax.y);

                StartCoroutine(ShrinkCloud(clouds[i], randNum, i));
            }
        }
    }

    private IEnumerator ShrinkCloud(Transform cloud, float waitTime, int cloudNumber)
    {
        Vector3 currentScale = cloud.localScale;
        float randTime = Random.Range(randLerpMinMax.x, randLerpMinMax.y);

        yield return new WaitForSeconds(waitTime);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / randTime;
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            //var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            cloud.localScale = new Vector3(Mathf.SmoothStep(currentScale.x, 0f, lerp), Mathf.SmoothStep(currentScale.y, 0f, lerp), Mathf.SmoothStep(currentScale.z, 0f, lerp));
            yield return new WaitForEndOfFrame();
        }

        finishedClouds.Add(cloudNumber);

        FinishedCheck();

    }

    void FinishedCheck()
    {
        if (finishedClouds.Count >= clouds.Count)
        {
            DeactivateObject();
        }
    }

    void DeactivateObject()
    {
        transform.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }


}
