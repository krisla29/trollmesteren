﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerStayTag : MonoBehaviour
{
    public string tag;
    public bool isActive;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == (tag))
        {
            isActive = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == (tag))
        {
            isActive = false;
        }
    }
}
