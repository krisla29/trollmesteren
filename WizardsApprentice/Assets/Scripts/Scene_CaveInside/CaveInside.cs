﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveInside : MonoBehaviour
{
    //public AwakenByScene awakenScript;
    //public GameEvent meetUnderbossEvent;
    public GameManager gameManager;
    public List<Transform> objectsToWakeUp = new List<Transform>();
    public Transform hatButton;

    private void OnEnable()
    {
        CheckConditions();
    }

    private void OnDisable()
    {
        SleepObjects();
    }

    void CheckConditions()
    {
        if(gameManager.playerStats.hasMetUnderboss)
        {
            foreach (var item in objectsToWakeUp)
            {
                item.gameObject.SetActive(true);
            }
        }

        if (gameManager.playerStats.hasUnlockedHat)
            hatButton.gameObject.SetActive(true);
        else
            hatButton.gameObject.SetActive(false);
    }

    void SleepObjects()
    {
        foreach (var item in objectsToWakeUp)
        {
            item.gameObject.SetActive(false);
        }
    }
}
