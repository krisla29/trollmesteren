﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCollector : MonoBehaviour
{
    public float coins;
    public Text coinText;
    public delegate void Coin();
    public static event Coin OnAddCoin;
    public static event Coin OnLoseCoin;

    private void OnEnable()
    {
        PlayerStats.addCoin += AddCoin;
        DrillGame.OnRightAnswer += AddCoin;
        DrillGame.OnWrongAnswer += LoseCoin;
        StairsGame.OnStairsGameCorrect += AddCoin;
        OrderGame.OnOrderGameComplete += AddCoin;
    }
    private void OnDisable()
    {
        PlayerStats.addCoin -= AddCoin;
        DrillGame.OnRightAnswer -= AddCoin;
        DrillGame.OnWrongAnswer -= LoseCoin;
        StairsGame.OnStairsGameCorrect -= AddCoin;
        OrderGame.OnOrderGameComplete -= AddCoin;
    }
    private void AddCoin()
    {
        coins += 1;
        coinText.text = coins.ToString();
        if(OnAddCoin != null)
        {
            OnAddCoin();
        }
    }
    private void LoseCoin()
    {
        coins -= 1;
        coinText.text = coins.ToString();
        if (OnLoseCoin != null)
        {
            OnLoseCoin();
        }
    }
}
