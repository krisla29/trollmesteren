﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagerOld2 : MonoBehaviour
{
    public Camera playerCamera;
    public PlayerController playerController;
    public delegate void CameraChange(Transform transform);
    public static event CameraChange OnChangeToSceneCam;
    public static event CameraChange OnChangeToPlayerCam;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "SceneCam")
        {
            if (OnChangeToSceneCam != null)
            {
                OnChangeToSceneCam(other.transform.GetChild(0));
            }
            other.gameObject.GetComponent<Camera>().enabled = true;
            other.gameObject.GetComponent<AudioListener>().enabled = true;
            var sceneCamera = other.GetComponent<SceneCamera>();
            sceneCamera.isActive = true;
            playerController.playerCam = other.transform;
            playerController.orientTransform = sceneCamera.sceneTransform;
            playerCamera.enabled = false;
            playerCamera.GetComponent<AudioListener>().enabled = false;

            if (other.transform.GetChild(0))
            {
                other.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "SceneCam")
        {
            if (OnChangeToPlayerCam != null)
            {
                OnChangeToPlayerCam(playerController.playerCam.transform);
            }
            other.gameObject.GetComponent<AudioListener>().enabled = false;
            var sceneCamera = other.GetComponent<SceneCamera>();
            playerController.orientTransform = playerCamera.transform;
            sceneCamera.isActive = false;
            playerCamera.enabled = true;
            playerCamera.GetComponent<AudioListener>().enabled = true;
            playerController.playerCam = playerCamera.transform;
            other.gameObject.GetComponent<Camera>().enabled = false;

            if (other.transform.GetChild(0))
            {
                other.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }
}
