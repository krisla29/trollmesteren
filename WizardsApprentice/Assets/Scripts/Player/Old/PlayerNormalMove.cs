﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNormalMove : PlayerController
{
    private float moveSpeed;
    //private int groundedBool;
    //private int jumpBool;
    private bool jump;

    private Vector3 moveDirection = Vector3.zero;
    private float rotTimer = 0;

    public override void Awake()
    {
        base.Awake();
        //groundedBool = Animator.StringToHash("Grounded");
        //jumpBool = Animator.StringToHash("Jump");
    }

    public override void Update()
    {
        isFlying = false;
        base.Update();
        NormalMove();
    }

    void NormalMove()
    {
        if (isGrounded)
        {
            //moveDirection = (new Vector3(playerCam.forward.x, 0, playerCam.forward.z) * Input.GetAxis("Vertical")) + (new Vector3(playerCam.right.x, 0, playerCam.right.z) * Input.GetAxis("Horizontal"));
            moveDirection = (orientTransform.forward * Input.GetAxis("Vertical")) + (orientTransform.right * Input.GetAxis("Horizontal"));
            moveDirection *= speed;

            moveSpeed = Vector2.ClampMagnitude(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")), 1f).magnitude;
            //anim.SetFloat(speed, moveSpeed, speedDampTime, Time.deltaTime);
            anim.SetFloat("Speed", moveSpeed);

            anim.SetBool(groundedBool, true);          
        }
        else
        {
            anim.SetBool(groundedBool, false);
        }

        if(characterController.isGrounded)
        {
            if (Input.GetButtonDown("Jump"))
            {
                anim.SetBool(jumpBool, true);
                moveDirection.y = jumpSpeed;
                isGrounded = false;
            }
            else
            {
                anim.SetBool(jumpBool, false);
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);

        // Rotate the transform
        if (!toggleAim)
        {
            if (Input.GetAxis("Vertical") != 0 || (Input.GetAxis("Horizontal") != 0))
            {
                Quaternion targetRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0, moveDirection.z), Vector3.up);

                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotSpeed);
            }
        }
        else
        {
            transform.rotation *= Quaternion.AngleAxis(Input.GetAxis("Mouse X") * Time.deltaTime * 200f, Vector3.up);
        }
    }
}

