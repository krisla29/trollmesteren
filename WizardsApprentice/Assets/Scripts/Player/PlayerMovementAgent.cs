﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class PlayerMovementAgent : PlayerMovement
{    
    public float agentSpeed = 5f;
    public float rotationSpeed = 5f;
    public float customJumpSpeed = 8f;
    public float jumpHeight = 1f;
    private Vector3 currentHitPoint;
    private bool hitAreaClicked;
    public bool isJumping;
    private RectTransform exitButton;
    private RectTransform zoomSlider;
    private RectTransform rotateLock;
    private RectTransform rotateSlider;
    private RectTransform uiButtons;
    private RectTransform trophyPanel;
    private RectTransform gameProgressionHUD;

    public NavMeshPointer navMeshPointer;
    public bool lockAgent;

    public override void Start()
    {
        base.Start();
        exitButton = playerController.gameManager.hudCanvas.exitButton.GetComponent<RectTransform>();
        zoomSlider = playerController.gameManager.hudCanvas.zoomSlider.GetComponent<RectTransform>();
        rotateLock = playerController.gameManager.hudCanvas.lockRotationToggle.GetComponent<RectTransform>();
        rotateSlider = playerController.gameManager.hudCanvas.rotateSlider.GetComponent<RectTransform>();
        uiButtons = playerController.gameManager.hudCanvas.buttonsPanel.GetComponent<RectTransform>();
        trophyPanel = playerController.gameManager.hudCanvas.trophyPanel.GetComponent<RectTransform>();
        gameProgressionHUD = playerController.gameManager.hudCanvas.gameProgressionDisplay.transform.GetChild(0).GetComponent<RectTransform>();
        //inputManager.hitArea.onClick.AddListener(HitAreaClick);
    }

    public override void MovementAction()
    {
        //if (playerController.navMeshAgent)
            //print(playerController.navMeshAgent.velocity.magnitude);

        if (!playerController.navMeshAgent.isOnOffMeshLink)
        {
            isJumping = false;
            playerController.anim.SetBool(playerController.groundedBool, true);
            playerController.anim.SetBool(playerController.jumpBool, false);
        }

        if (playerController.navMeshAgent.isOnOffMeshLink && !isJumping)
        {
            playerController.navMeshAgent.speed = 2.5f;
                   
            //print("Agent Start Jump");

            StartCoroutine("Jump");
         
            isJumping = true;
        }       

        playerController.anim.SetFloat("Speed", playerController.navMeshAgent.velocity.magnitude/4f);
        
        if (!playerController.overrideNavMeshAgent && playerController.canMove)
        {
            //if (hitAreaClicked)
            if(Input.GetMouseButtonDown(0) && !lockAgent)
            {
                var mousePosition = Input.mousePosition;                
                //Vector2 localMousePosition = exitButton.InverseTransformPoint(Input.mousePosition);

                float screenMargin = Screen.width / 20f;

                if (mousePosition.x > screenMargin && mousePosition.x < Screen.width - screenMargin)
                {
                    if (!inputManager.ActionDown() && !inputManager.AimDown())
                    {
                        if (!IsBlockingUIelements())
                        {
                            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                            RaycastHit hit = new RaycastHit();

                            SetNavMeshPoint(ray, hit);
                        }
                    }
                }
            }
        }

        hitAreaClicked = false;

        
    }

    public void SetNavMeshPoint(Ray inRay, RaycastHit inHit)
    {
        if (Physics.Raycast(inRay, out inHit) && inHit.transform.CompareTag("Walkable"))
        {
            NavMeshHit navHit;

            //print("NavMeshAgent Hit Walkable!");

            if (NavMesh.SamplePosition(inHit.point, out navHit, 1.0f, NavMesh.AllAreas))
            {
                //print("NavMeshAgent Hit Navmesh!");

                hitAreaClicked = true;

                Debug.DrawLine(inRay.origin, navHit.position, Color.red);
                currentHitPoint = navHit.position;

                if (navMeshPointer)
                {
                    navMeshPointer.gameObject.SetActive(false);
                    navMeshPointer.gameObject.SetActive(true);
                    navMeshPointer.transform.position = navHit.position;
                }

                playerController.navMeshAgent.updateRotation = false;
                var targetRotation = Quaternion.LookRotation((currentHitPoint - transform.position).normalized, Vector3.up);
                StartCoroutine("SetDestination", targetRotation);
            }
            else
            {
                //print("NavMeshAgent Didn't Hit Navmesh...");
                //print(" Hit...   " + navHit.distance);
            }
        }
        else
        {
            //print("NavMeshAgent Didn't Hit Walkable...");
            //print(" Hit...   " + inHit.collider.name);
        }
    }
    /*
    private void HitAreaClick()
    {
        hitAreaClicked = true;
    }
    */
    public IEnumerator SetDestination(Quaternion targetRot)
    {
        playerController.isAgentRotating = true;

        float t = 0;

        while (t < 1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, Time.deltaTime * rotationSpeed);
            t += Time.deltaTime * rotationSpeed;
            yield return new WaitForEndOfFrame();
        }

        SetNewDestination(currentHitPoint);

        playerController.isAgentRotating = false;
    }

    public IEnumerator Jump()
    {
        OffMeshLinkData data = playerController.navMeshAgent.currentOffMeshLinkData;
        var targetRotation = Quaternion.identity;

        //print("player pos y is " + transform.position.y);
        //print("data endPos is " + data.endPos.y);
        //print("data startPos is " + data.startPos.y);
        
        targetRotation = Quaternion.LookRotation((data.endPos - transform.position).normalized, Vector3.up);

        var time = 0f;
        playerController.navMeshAgent.updateRotation = false;
        playerController.navMeshAgent.SetDestination(transform.position);

        while(time < 1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
            time += Time.deltaTime * rotationSpeed;
            //print("rotating before jump");
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.2f);
        playerController.anim.SetBool(playerController.groundedBool, false);
        playerController.anim.SetBool(playerController.jumpBool, true);
        yield return new WaitForSeconds(0.1f);
        //playerController.navMeshAgent.SetDestination(currentHitPoint);

        float t = 0;
        float jumpHT = jumpHeight;
        Vector3 midPoint = (data.endPos + (data.startPos - data.endPos) / 2);
        float duration = ((data.endPos - data.startPos).magnitude) / customJumpSpeed;

        while (t < 1)
        {
            if (t >= 0.05f)
            {
                playerController.anim.SetBool(playerController.groundedBool, true);
                playerController.anim.SetBool(playerController.jumpBool, false);
            }

            if (t < 0.5f)
            {
                transform.position = Vector3.Lerp(transform.position, midPoint + (Vector3.up * jumpHT), t * 2f);
            }

            if (t >= 0.5f && t < 1)
            {
                transform.position = Vector3.Lerp(midPoint + (Vector3.up * jumpHT), data.endPos, (t * 2f) - 1);
            }

            //print("Moving Player");
            t += Time.deltaTime / duration;
            yield return new WaitForEndOfFrame();
        }

        playerController.anim.SetFloat("Speed", playerController.navMeshAgent.velocity.magnitude / 4f);
        playerController.navMeshAgent.CompleteOffMeshLink();

        playerController.navMeshAgent.SetDestination(transform.position);
        yield return new WaitForSeconds(0.5f);
        playerController.navMeshAgent.SetDestination(currentHitPoint);

        var time2 = 0f;
        targetRotation = Quaternion.LookRotation((currentHitPoint - transform.position).normalized, Vector3.up);

        while (time < 1)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
            time2 += Time.deltaTime * rotationSpeed;
            //print("rotating before jump");
            yield return new WaitForEndOfFrame();
        }

        playerController.navMeshAgent.baseOffset = 0;
        playerController.navMeshAgent.speed = agentSpeed;
        playerController.navMeshAgent.updateRotation = true;
        
        //print("Agent End Jump");
    }

    public IEnumerator TraverseOffMeshLink()
    {
        while (playerController.navMeshAgent.isOnOffMeshLink)
        {
            playerController.navMeshAgent.CompleteOffMeshLink();
            yield return null;
        }
    }

    public void SetNewDestination(Vector3 hitPoint)
    {
        playerController.navMeshAgent.updateRotation = true;

        if (playerController.navMeshAgent.isOnNavMesh)
            playerController.navMeshAgent.SetDestination(hitPoint);
    }

    public void SetRotationAtDestination(Quaternion rotation)
    {
        StartCoroutine(SetQuickRotation(rotation));              
    }

    public IEnumerator SetQuickRotation(Quaternion rotation)
    {
        yield return new WaitForSeconds(0.5f);
        print(playerController.navMeshAgent.remainingDistance);
        print(playerController.navMeshAgent.hasPath);
        print(playerController.navMeshAgent.pathStatus);
        float dist = playerController.navMeshAgent.remainingDistance;

        if (dist != Mathf.Infinity && playerController.navMeshAgent.pathStatus == NavMeshPathStatus.PathComplete && playerController.navMeshAgent.hasPath)
        {
            while (playerController.navMeshAgent.remainingDistance > 0)
            {
                //print("Agent still has path...");
                yield return new WaitForEndOfFrame();
            }
        }

        playerController.navMeshAgent.updateRotation = false;

        float t = 0;
        var oldRot = playerController.transform.rotation;

        while (t < 1)
        {
            t += Time.deltaTime * 4f;
            playerController.transform.rotation = Quaternion.Lerp(oldRot, rotation, t);
            yield return new WaitForEndOfFrame();
        }

        playerController.navMeshAgent.updateRotation = true;
    }

    public bool IsBlockingUIelements()
    {
        var mousePosition = Input.mousePosition;
        Vector2 localMousePositionExit = exitButton.InverseTransformPoint(mousePosition);
        Vector2 localMousePositionRlock = rotateLock.InverseTransformPoint(mousePosition);
        Vector2 localMousePositionRotate = rotateSlider.InverseTransformPoint(mousePosition);
        Vector2 localMousePositionZoom = zoomSlider.InverseTransformPoint(mousePosition);
        Vector2 localMousePositionButtons= uiButtons.InverseTransformPoint(mousePosition);
        Vector2 localMousePositionTP = trophyPanel.InverseTransformPoint(mousePosition);
        Vector2 localMousePositionGameProg = gameProgressionHUD.InverseTransformPoint(mousePosition);


        if (playerController.gameManager.isScene)
        {
            if (exitButton.rect.Contains(localMousePositionExit) || gameProgressionHUD.rect.Contains(localMousePositionGameProg))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else 
        {
            if (rotateSlider.rect.Contains(localMousePositionRotate) || zoomSlider.rect.Contains(localMousePositionZoom) || rotateLock.rect.Contains(localMousePositionRlock)
                || uiButtons.rect.Contains(localMousePositionButtons) || (trophyPanel.gameObject.activeSelf && trophyPanel.rect.Contains(localMousePositionTP)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    private void OnEnable()
    {
        if (navMeshPointer)
        {
            navMeshPointer.transform.SetParent(null);
        }
        NavMeshHit navHit;

        if (NavMesh.SamplePosition(transform.position, out navHit, 2.0f, NavMesh.AllAreas))
        {
            playerController.navMeshAgent.Warp(navHit.position);
        }
    }

    private void OnDisable()
    {
        if (navMeshPointer)
        {           
            navMeshPointer.gameObject.SetActive(false);
        }

        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
