﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float rotSpeed = 10f;
    public float gravity = 20.0f;

    public InputManager inputManager;
    public PlayerController playerController;
    public PlayerMovementController playerMovementController;

    public float moveSpeed;
    public bool jump;

    public Vector3 moveDirection = Vector3.zero;
    public float rotTimer = 0;

    public virtual void Start()
    {
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
    }

    public void Update()
    {
        MovementAction();
    }
    public virtual void MovementAction()
    {

    }

}
