﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementLocked : PlayerMovement
{
    private float duration = 0.2f;

    private void OnEnable()
    {
        //StartCoroutine(SlowDownToHalt());
        if(playerController.anim)
            playerController.anim.SetFloat("Speed", 0);
    }

    public override void MovementAction()
    {
        /*
        if (playerController.isGrounded)
        {
            
        }
        else
        {
           
        }
        */
        //moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        //playerController.characterController.Move(moveDirection * Time.deltaTime);

        // Rotate the transform
        /*
        if (!playerController.toggleAim)
        {
            if (inputManager.LeftHorizontal() != 0 || inputManager.LeftVertical() != 0)
            {
                Quaternion targetRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0, moveDirection.z), Vector3.up);

                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotSpeed);
            }
        }
        else
        {
            transform.rotation *= Quaternion.AngleAxis(inputManager.RightHorizontal() * Time.deltaTime * 200f, Vector3.up);
        }
        */
    }

    private IEnumerator SlowDownToHalt()
    {
        float t = 0;
        float currentSpeed = playerController.anim.GetFloat("Speed");

        while(t < 1)
        {
            playerController.anim.SetFloat("Speed", Mathf.InverseLerp(0, currentSpeed, t));
            t += Time.deltaTime / duration;
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
