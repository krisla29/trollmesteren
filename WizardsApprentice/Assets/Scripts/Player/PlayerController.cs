﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public GameManager gameManager;
    public InputManager inputManager;
    public CharacterController characterController;
    public CharacterSelector characterSelector;
    public NavMeshAgent navMeshAgent;
    public Collider coll;
    public PlayerMovementController playerMovementController;
    public PlayerMovement currentPlayerMovement;
    public CameraMovementController cameraMovementController;
    public Animator anim;    
    public Transform playerCam;
    public Transform orientTransform;
    public WandInteractableDetector wandInteractableDetector;
    public InteractablesDetector interactablesDetector;
    public float detectionDistance = 15f;
    public WandWorld wandObject;
    public Vector3 wandObjectAngles = new Vector3(0, -90, 0);
    public Vector3 wandObjectLocPos = new Vector3(0, 0.1f, 0.025f);
    public Wand wand;
    public Vector3 wandTarget;
    public SpriterWorldController spriter;
    public Transform rightHand;
    public Transform rightForearm;
    public Transform rightArm;
    public Transform head;
    public AudioSource footStepsAudio;
    public AudioSource getWandAudio;
    public AudioSource getFlyingAudio;
    public AudioSource flyingAudio;
    public AudioSource victoryAudio;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float rotSpeed = 10f;
    public float gravity = 20.0f;
    public float groundedTimeSpeed = 10f;
    public bool disableAim;
    public bool forceAim;
    public bool isLocked;
    public bool canMove = true;
    public bool toggleAimThirdPerson;
    public bool isFirstPersonAim;
    public bool toggleAim;
    public bool isFlying;
    public bool isFiring;
    public bool isInteracting;
    public bool isPickingUp;
    public bool isGrounded;
    public bool isNavMeshAgent;
    public bool isAgentRotating;
    public bool isBattle;
    public bool overrideNavMeshAgent;
    public int flyingIntializer;
    public int flyingTimer;
    public int flyingCounter;          
    public float groundedTimer;
    private Transform currentInteractable;

    //Animator fields:
    public int hFloat;
    public int vFloat;
    public int aimBool;
    public int groundedBool;
    public int jumpBool;
    public int battleBool;


    //Messages:
    public delegate void PlayerMessage(string message, int value);
    public static event PlayerMessage OnPlayerMessage;
    public delegate void PlayerTrigger(Transform transform);
    public static event PlayerTrigger OnPlayerTrigger;
    public delegate void PlayerEvent();
    public static event PlayerEvent OnPlayerGetsWand;
    public static event PlayerEvent OnPlayerDeath;

    public virtual void Awake()
    {
        //characterController = GetComponent<CharacterController>();
        //playerMovementController = GetComponent<PlayerMovementController>();
        //currentPlayerMovement = GetComponent<PlayerMovement>();

        //anim = GetComponent<Animator>();
        aimBool = Animator.StringToHash("Aim");
        hFloat = Animator.StringToHash("H");
        vFloat = Animator.StringToHash("V");
        groundedBool = Animator.StringToHash("Grounded");
        jumpBool = Animator.StringToHash("Jump");
        battleBool = Animator.StringToHash("InBattle");
        //inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        //gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        orientTransform = playerCam.transform;
        groundedTimer = 0;
        isFirstPersonAim = gameManager.hasFirstPersonAim;
    }
    public void Start()
    {
        print(navMeshAgent.agentTypeID);
    }

    void OnEnable()
    {
        SignPostCanvas.OnHudCanvasDisable += SetCanMove;
        SignPostCanvas.OnHudCanvasEnable += SetCannotMove;
        FlyPowerUp.OnFlyPowerUp += StartFlying;
    }

    void OnDisable()
    {
        SignPostCanvas.OnHudCanvasDisable -= SetCanMove;
        SignPostCanvas.OnHudCanvasEnable -= SetCannotMove;
        FlyPowerUp.OnFlyPowerUp -= StartFlying;
    }

    public virtual void Update()
    {
        wandInteractableDetector.transform.localScale = new Vector3(detectionDistance, detectionDistance, detectionDistance);

        anim.SetFloat(hFloat, inputManager.LeftHorizontal());
        anim.SetFloat(vFloat, inputManager.LeftVertical());
       
        currentPlayerMovement = playerMovementController.currentMovement;

        Movement();
        CheckAimMode();
        //ResetForcedAim();
        IsForcedAim();
        
        IsNavmeshAgent();
        IsFlying();      
        IsAiming();
        IsGrounded();
        IsPickingUp();
        IsInteracting();
        FootStepAudio();
        IsBattling();
        
    }

    void SetCanMove()
    {
        canMove = true;
    }

    void SetCannotMove()
    {
        canMove = false;
    }

    public void FootStepAudio()
    {
        if (!isNavMeshAgent)
        {
            if (isGrounded && (inputManager.LeftHorizontal() != 0 || inputManager.LeftVertical() != 0))
            {
                if (!footStepsAudio.isPlaying)
                {
                    footStepsAudio.Play();
                }
            }
            else
            {
                footStepsAudio.Stop();
            }
        }
        else
        {
            if (navMeshAgent.velocity != Vector3.zero && navMeshAgent.isOnNavMesh)
            {
                if (!footStepsAudio.isPlaying)
                {
                    footStepsAudio.Play();
                }
            }
            else
            {
                footStepsAudio.Stop();
            }
        }

    }
    public void Movement()
    {
        if(currentPlayerMovement)
        {
            currentPlayerMovement.MovementAction();
        }
    }

    public void CheckAimMode()
    {
        if(gameManager.hasFirstPersonAim != isFirstPersonAim)
        {
            toggleAim = false;
            toggleAimThirdPerson = false;
            isFirstPersonAim = gameManager.hasFirstPersonAim;
        }
    }

    private void IsForcedAim()
    {
        if (!disableAim)
        {
            if (!forceAim)
            {
                ToggleAim();
            }
            else
            {
                ForceAim();
            }
        }
    }

    private void ResetForcedAim()
    {
        if(!forceAim)
        {
            toggleAim = false;
            toggleAimThirdPerson = false;
        }
    }

    public void IsAiming()
    {
        if (toggleAim)
        {
            anim.SetBool(aimBool, true);

            if(inputManager.Action())
            {
                isFiring = true;
            }
            else
            {
                isFiring = false;
            }
        }
        else
        {
            anim.SetBool(aimBool, false);
        }

        if (toggleAimThirdPerson)
        {
            if (inputManager.Action())
            {
                isFiring = true;
            }
            else
            {
                isFiring = false;
            }
        }
    }

    public void IsGrounded()
    {
        if(isGrounded && !characterController.isGrounded)
        {
            if(groundedTimer < 1)
            {
                groundedTimer += Time.deltaTime * groundedTimeSpeed;
            }
            
            if(groundedTimer >= 1)
            {
                isGrounded = false;
            }
        }

        if(!isGrounded && characterController.isGrounded)
        {
            isGrounded = true;
            groundedTimer = 0;
        }
    }

    public void IsPickingUp()
    {
        if(wand)
        {
            if(wand.isPickingUp)
            {
                isPickingUp = true;
            }
            else
            {
                isPickingUp = false;
            }
        }
    }

    public void IsNavmeshAgent()
    {
        if (gameManager.isScene)
        {
            if (gameManager.currentScene.isNavmesh)
            {
                isNavMeshAgent = true;
            }
            else
            {
                isNavMeshAgent = gameManager.isNavMeshAgent;
            }
        }
        else
        {
            isNavMeshAgent = gameManager.isNavMeshAgent;
        }
    }

    public void ToggleAim()
    {
        if (gameManager.hasFirstPersonAim)
        {
            if (!inputManager.isTouch)
            {
                if (inputManager.AimDown())
                {
                    if (!toggleAim)
                    {
                        toggleAim = true;
                    }
                    else
                    {
                        toggleAim = false;
                    }
                }
            }
            else
            {
                if (inputManager.aim)
                {
                    toggleAim = true;
                }
                else
                {
                    toggleAim = false;
                }
            }           
        }
        else
        {
            if (!inputManager.isTouch)
            {
                if (inputManager.AimDown())
                {
                    if (!toggleAimThirdPerson)
                    {
                        toggleAimThirdPerson = true;
                    }
                    else
                    {
                        toggleAimThirdPerson = false;
                    }
                }
            }
            else
            {
                if (inputManager.aim)
                {
                    toggleAimThirdPerson = true;
                }
                else
                {
                    toggleAimThirdPerson = false;
                }
            }
        }
    }

    private void ForceAim()
    {
        if (gameManager.hasFirstPersonAim)
        {
            toggleAim = true;
        }
        else
        {
            toggleAimThirdPerson = true;
        }
    }

    private void IsInteracting()
    {
        if(isInteracting)
        {
            if(inputManager.ActionDown())
            {
                if(OnPlayerTrigger != null)
                {
                    OnPlayerTrigger(currentInteractable);
                }
            }
        }
    }

    private void IsBattling()
    {
        if (isBattle)
            anim.SetBool(battleBool, true);
        else
            anim.SetBool(battleBool, false);
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("KillField"))
        {
            Destroy(transform.gameObject);
        }
        if (other.gameObject.CompareTag("Wand"))
        {
            if(getWandAudio && !wand)
            {
                getWandAudio.Play();
            }
            
            wand = other.gameObject.GetComponent<Wand>();
            wand.isActive = true;
            other.transform.parent = rightHand;
            other.transform.localPosition = Vector3.zero;
            other.transform.localRotation = rightHand.localRotation;

            if(OnPlayerGetsWand != null)
            {
                OnPlayerGetsWand();
            }
        }
        if(other.gameObject.CompareTag("FlyPowerUp"))
        {
            getFlyingAudio.Play();
        }
        if (other.gameObject.CompareTag("Interactable"))
        {
            if(!toggleAim)
            {
                isInteracting = true;
                currentInteractable = other.transform;
            }            
        }
    }
  
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Interactable"))
        {
            isInteracting = false;
        }
        if(other.gameObject.CompareTag("KillFieldInverted"))
        {
            Destroy(transform.gameObject);
        }
    }
    void LateUpdate()
    //OVERRIDE ROTATIONS OF SKELETAL JOINTS:
    {
        if (playerCam.CompareTag("MainCamera"))
        {
            if(toggleAim)
            {
                head.transform.rotation *= Quaternion.Euler(playerCam.localEulerAngles.x, 0, playerCam.localEulerAngles.z);
                rightArm.transform.LookAt(-playerCam.transform.up * 1000f, playerCam.forward);
                rightHand.transform.localEulerAngles = new Vector3(0, -90, 0);

                if (wand)
                {
                    wand.transform.localEulerAngles = new Vector3(0, 0, -45f);
                    wand.transform.localPosition = new Vector3(0, 0.1f, 0);
                }
            }
            else
            {
                if (wand)
                {
                    wand.transform.localEulerAngles = Vector3.zero;
                    wand.transform.localPosition = Vector3.zero;
                }
            }

            if(toggleAimThirdPerson)
            {
                rightArm.transform.LookAt(-transform.transform.up * 1000f, transform.forward);
                rightForearm.transform.LookAt(-transform.transform.up * 1000f, transform.forward);
                rightHand.transform.localEulerAngles = new Vector3(0, -90, 0);

                if (wand)
                {
                    wand.transform.localEulerAngles = new Vector3(0, 0, -45f);
                    wand.transform.localPosition = new Vector3(0, 0.1f, 0);
                }
            }

            else
            {
                if (wand)
                {
                    wand.transform.localEulerAngles = Vector3.zero;
                    wand.transform.localPosition = Vector3.zero;
                }
            }
        }
        else if(playerCam.CompareTag("SceneCam"))
        {
            if(toggleAim)
            {
                head.transform.rotation *= Quaternion.Euler(transform.localEulerAngles.x, 0, playerCam.localEulerAngles.z);
                rightArm.transform.LookAt(-transform.transform.up * 1000f, transform.forward);
                rightHand.transform.localEulerAngles = new Vector3(0, -90, 0);

                if (wand)
                {
                    wand.transform.localEulerAngles = new Vector3(0, 0, -45f);
                    wand.transform.localPosition = new Vector3(0, 0.1f, 0);
                }
            }
            else
            {
                if (wand)
                {
                    wand.transform.localEulerAngles = Vector3.zero;
                    wand.transform.localPosition = Vector3.zero;
                }
            }

            if (toggleAimThirdPerson)
            {
                if (wandTarget != Vector3.zero)
                {
                    rightArm.transform.LookAt(-transform.transform.up * 1000f, transform.forward);
                    rightForearm.transform.LookAt(-transform.transform.up * 1000f, transform.forward);
                }
                else
                {
                    rightArm.transform.LookAt(-transform.transform.up * 1000f * wandTarget.y, transform.forward);
                    rightForearm.transform.LookAt(-transform.transform.up * 1000f * wandTarget.y, transform.forward);
                }

                rightHand.transform.localEulerAngles = new Vector3(0, -90, 0);

                if (wand)
                {
                    wand.transform.localEulerAngles = new Vector3(0, 0, -45f);
                    wand.transform.localPosition = new Vector3(0, 0.1f, 0);
                }               
            }

            else
            {
                if (wand)
                {
                    wand.transform.localEulerAngles = Vector3.zero;
                    wand.transform.localPosition = Vector3.zero;
                }
               
            }
        } 
        if(isBattle)
        {
            head.transform.rotation *= Quaternion.Euler(-20f, 0, 0);
        }

        if (toggleAimThirdPerson)
        {
            if (wandObject)
            {
                if (!wandObject.gameObject.activeSelf)
                    wandObject.gameObject.SetActive(true);

                if (wandObject.transform.parent != rightHand)
                    wandObject.transform.SetParent(rightHand);

                if (wandObject.transform.localPosition != wandObjectLocPos)
                    wandObject.transform.localPosition = wandObjectLocPos;

                if (wandObject.transform.localEulerAngles != wandObjectAngles)
                    wandObject.transform.localEulerAngles = wandObjectAngles;
            }
        }
        else
        {
            if (wandObject)
            {
                if (wandObject.transform.parent != transform)
                    wandObject.transform.SetParent(transform);

                if (wandObject.gameObject.activeSelf)
                    wandObject.gameObject.SetActive(false);

                if (wandObject.transform.localPosition != wandObjectLocPos)
                    wandObject.transform.localPosition = wandObjectLocPos;

                if (wandObject.transform.localEulerAngles != wandObjectAngles)
                    wandObject.transform.localEulerAngles = wandObjectAngles;
            }
        }
    }
  
    public void StartFlying(int time)
    {
        print(time);
        flyingIntializer = (int)Time.time;
        flyingTimer = time;
        flyingCounter = 0;
    }
    public void IsFlying()
    {
        if(!isFlying)
        {
            flyingAudio.Play();
        }

        if (flyingTimer > flyingCounter)
        {
            isFlying = true;
           
            if (flyingCounter < (int)Time.time - flyingIntializer)
            {
                flyingCounter += 1;

                if(OnPlayerMessage != null)
                {
                    OnPlayerMessage("FLYING", (flyingTimer - flyingCounter));
                }
            }
        }
        else
        {
            if(isFlying)
            {
                if (OnPlayerMessage != null)
                {
                    OnPlayerMessage("", 0);
                }
            }

            flyingAudio.Stop();

            isFlying = false;
        }
    }

    public void OnDestroy()
    {
        if(OnPlayerDeath != null)
        {
            OnPlayerDeath();
        }
    }
}