﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemDetector : MonoBehaviour
{
    public List<Transform> gems = new List<Transform>();

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Gem"))
        {
            if(!gems.Contains(other.transform))
            {
                gems.Add(other.transform);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Gem"))
        {
            if(gems.Contains(other.transform))
            {
                gems.Remove(other.transform);
            }
        }
    }

    void RemoveFromGems(int passedValue, Transform passedGem)
    {
        if(gems.Contains(passedGem))
        {
            gems.Remove(passedGem);
        }
    }

    private void OnEnable()
    {
        GemCollector.OnCollectGem += RemoveFromGems;
    }

    private void OnDisable()
    {
        GemCollector.OnCollectGem -= RemoveFromGems;
    }
}
