﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour
{
    public bool isStartSpawner;
    public GameObject playerPrefab;
    private GameObject player;
    public delegate void SpawnNewPlayer(Transform player);
    public static event SpawnNewPlayer OnSpawnNewPlayer;

    void Awake()
    {
        if (isStartSpawner)
        {
            SpawnPlayerAction();
        }

        if (OnSpawnNewPlayer != null)
        {
            OnSpawnNewPlayer(player.transform);
        }
    }
    void Update()
    {
       if(player == null)
        {
            SpawnPlayerAction();

            if (OnSpawnNewPlayer != null)
            {
                OnSpawnNewPlayer(player.transform);
            }
        }
    }

    void SpawnPlayerAction()
    {
        player = Instantiate(playerPrefab);
        player.name = playerPrefab.name;
        player.transform.position = transform.position;
        player.transform.rotation = transform.rotation;
    }

    void OnDrawGizmos()
    {        
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
