﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemCollector : MonoBehaviour
{
    public Transform worldGemsParent;
    public Transform nightGemsParent;
    public delegate void CollectGemEvent(int collectedGems, Transform gem);
    public static event CollectGemEvent OnCollectGem; //Sends to PlayerStats && GemDetector

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Gem"))
        {
            if(OnCollectGem != null)
            {
                OnCollectGem(1, other.transform);
            }

            if (other.gameObject.transform.parent == worldGemsParent || other.gameObject.transform.parent == nightGemsParent)
                other.gameObject.SetActive(false);
            else
                Destroy(other.gameObject);
        }       
    }
}
