﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerTarget : MonoBehaviour
{
    public PlayerController playerController;   
    public Interactable interactable;
    public PlayerTarget linkedPlayerTarget;
    public Transform goalTrigger;
    public bool hasReachedGoal;
    //public Transform walkableExit;
    public Transform targetTransform;
    public Transform playerTrigger;
    private Vector3 oldPos;
    public enum PlayerTargetTypes { Elevator, GoalZone, GoalPoint, Warp, Simple }
    public PlayerTargetTypes playerTargetType;
    public bool elevatorIsTwoWay;
    public bool elevatorIsMoving;
    public Transform elevatorObject;
    //public bool isAtAccessPoint;
    
    public bool lockPlayer;
    public bool lockPlayerManually;

    public bool isLocked;
    public bool isPressed;
    public bool inPosition;

    private PlayerMovementAgent playerMovementAgent;
    public delegate void PlayerTargetEvent(Transform thisObject, Transform subjectTransform); //SENDS TO NAVMESHELEVATOR
    public static event PlayerTargetEvent OnPlayerEnter;
    public static event PlayerTargetEvent OnPlayerExit;
    public static event PlayerTargetEvent OnPlayerLock;
    public static event PlayerTargetEvent OnPlayerUnLock;

    private IEnumerator EnterTarget()
    {
        if (!elevatorIsMoving)
        {
            if(isLocked)
            {
                UnLockPlayer();
            }

            //print("PlayerTarget Enters Target");

            Ray ray = new Ray(targetTransform.position + Vector3.up * 0.5f, -Vector3.up);
            RaycastHit hit = new RaycastHit();

            Debug.DrawLine(ray.origin, ray.origin + ray.direction, Color.red);

            playerMovementAgent.SetNavMeshPoint(ray, hit);

            //print("PlayerTarget NavMeshAgent sets navMeshPosition");
        }

        yield return null;
    }

    private IEnumerator ExitTarget()
    {
        if (!elevatorIsMoving)
        {
            //print("PlayerTarget Exits Target");

            NavMeshPath path = new NavMeshPath();

            while (!NavMesh.CalculatePath(playerController.transform.position, targetTransform.position, NavMesh.AllAreas, path))
            {
                //print("Elevator exit path not found...");
                yield return new WaitForEndOfFrame();
            }

            //print("Elevator exit path found!");

            UnLockPlayer();
        }

        yield return null;
    }

    private IEnumerator LeaveTarget()
    {
        if (!elevatorIsMoving)
        {
            //print("PlayerTarget Exits Target");

            NavMeshPath path = new NavMeshPath();

            while (!NavMesh.CalculatePath(playerController.transform.position, targetTransform.position, NavMesh.AllAreas, path))
            {
                //print("Elevator exit path not found...");
                yield return new WaitForEndOfFrame();
            }

            //print("Elevator exit path found!");

            UnLockPlayer();

            var mousePosition = Input.mousePosition;
            float screenTenth = Screen.width / 10f;

            if (mousePosition.x > screenTenth && mousePosition.x < Screen.width - screenTenth)
            {
                if (!playerMovementAgent.inputManager.ActionDown() && !playerMovementAgent.inputManager.AimDown())
                {
                    if (!playerMovementAgent.IsBlockingUIelements())
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit = new RaycastHit();

                        playerMovementAgent.SetNavMeshPoint(ray, hit);
                    }
                }
            }
        }

        yield return null;
    }

    private bool CheckPlayerInPosition()
    {
        if(inPosition)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void OnEnable()
    {
        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;
        Interactable.OnInteractablePressed += InteractablePressed;
        Interactable.OnInteractablePressed += InteractableReleased;
        TriggerByTag.OnTriggerByTagEnter += PlayerEntersPosition;
        TriggerByTag.OnTriggerByTagExit += PlayerExitsPosition;
        NavMeshElevator.OnNavMeshElevatorReachStart += ElevatorReachStart;
        NavMeshElevator.OnNavMeshElevatorReachEnd += ElevatorReachEnd;
        NavMeshElevator.OnNavMeshElevatorStartMoving += ElevatorMoving;
        InputManager.OnInputMouseDown += MouseDown;
        InputManager.OnInputMouseDown += MouseUp;

    }

    private void OnDisable()
    {
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
        Interactable.OnInteractablePressed -= InteractablePressed;
        Interactable.OnInteractablePressed -= InteractableReleased;
        TriggerByTag.OnTriggerByTagEnter -= PlayerEntersPosition;
        TriggerByTag.OnTriggerByTagExit -= PlayerExitsPosition;
        NavMeshElevator.OnNavMeshElevatorReachStart -= ElevatorReachStart;
        NavMeshElevator.OnNavMeshElevatorReachEnd -= ElevatorReachEnd;
        NavMeshElevator.OnNavMeshElevatorStartMoving -= ElevatorMoving;
        InputManager.OnInputMouseDown -= MouseDown;
        InputManager.OnInputMouseDown -= MouseUp;

        StopAllCoroutines();
    }

    private void MouseDown()
    {
        if(!elevatorIsMoving)
        {
            if(isLocked)
            {
                StartCoroutine("LeaveTarget");
            }
        }
    }

    private void MouseUp()
    {
        /*
        if (!elevatorIsMoving)
        {
            if (isLocked)
            {
                UnLockPlayer();
            }
        }
        */
    }
    
    private void UnLockPlayer()
    {
        if (inPosition && isLocked)
        {
            playerController.isLocked = false;
            playerController.transform.SetParent(null);
            isLocked = false;

            if (OnPlayerUnLock != null)
            {
                OnPlayerUnLock(transform, playerController.transform);
            }

            print("PlayerTarget Unlocked Player");
        }
    }
    private void LockPlayer()
    {
        if(lockPlayer)
        {
            if(!lockPlayerManually)
            {                               
                if(!playerController.isNavMeshAgent || !playerController.navMeshAgent)
                {
                    //print("warped player to targetTransform");
                    playerController.transform.position = targetTransform.position;
                }
                if (playerController.navMeshAgent && !playerController.navMeshAgent.isOnNavMesh)
                {
                    //playerController.navMeshAgent.Warp(targetTransform.position);
                    print("warped player agent to targetTransform");
                }
                if (playerController.navMeshAgent && playerController.navMeshAgent.isOnNavMesh)
                {
                    playerController.navMeshAgent.SetDestination(targetTransform.position);
                    playerController.navMeshAgent.Warp(targetTransform.position);
                    //print("warped player agent to targetTransform and set destination");
                }
                
                inPosition = true;

                playerController.isLocked = true;
                playerController.transform.SetParent(targetTransform);
                //playerController.transform.position = targetTransform.position;
                isLocked = true;

                print("PlayerTarget locked Player");

                if(OnPlayerLock != null)
                {
                    OnPlayerLock(transform, playerController.transform);
                }
            }
            else
            {
                print("PlayerTarget Sends Callback to Script...");
            }
        }
    }

    private void PlayerEntersPosition(Transform trigger, Transform player)
    {
        if (trigger == playerTrigger && player == playerController.transform)
        {
            if(OnPlayerEnter != null)
            {
                OnPlayerEnter(transform, playerController.transform);
            }

            if (playerTargetType == PlayerTargetTypes.Elevator)
            {
                if (!elevatorIsMoving)
                {
                    if (!inPosition)
                    {
                        LockPlayer();
                    }
                }
                else
                {
                    playerController.navMeshAgent.Warp(oldPos);

                    //print("PlayerTarget Player retreat, elevator moving");
                }
            }
            else
            {
                if (!inPosition)
                {
                    LockPlayer();
                }
            }            
        }        
    }

    private void PlayerExitsPosition(Transform trigger, Transform player)
    {
        if (trigger == playerTrigger && player == playerController.transform)
        {
            if (OnPlayerExit != null)
            {
                OnPlayerExit(transform, playerController.transform);
            }

            inPosition = false;
            //print("PlayerTarget Player exits trigger");
        }
    }

    private void PlayerEntersGoal(Transform trigger, Transform player)
    {
        if (trigger == goalTrigger && player == playerController.transform)
        {
            hasReachedGoal = true;

            StartCoroutine("ExitTarget");
        }
    }

    private void PlayerExitsGoal(Transform trigger, Transform player)
    {
        if (trigger == goalTrigger && player == playerController.transform)
        {
            hasReachedGoal = false;
        }
    }

    private void ReferenceNewPlayer(Transform transform)
    {
        playerController = transform.GetComponent<PlayerController>();
        playerMovementAgent = playerController.GetComponent<PlayerMovementAgent>();
    }

    private void InteractablePressed(Transform linkedInteractable)
    {
        if(linkedInteractable == interactable.transform)
        {
            if (!isPressed)
            {
                oldPos = playerController.transform.position;

                StartCoroutine("EnterTarget");
            }

            isPressed = true;
        }
    }

    private void InteractableReleased(Transform linkedInteractable)
    {
        if (linkedInteractable == interactable.transform)
        {
            isPressed = false;
        }
    }

    private void ElevatorReachStart(Transform elevator)
    {
        if (elevator == elevatorObject)
        {
            elevatorIsMoving = false;

            if (elevatorIsTwoWay)
            {
                StartCoroutine("ExitTarget");
            }            
        }
    }

    private void ElevatorReachEnd(Transform elevator)
    {
        if(elevator == elevatorObject)
        {
            elevatorIsMoving = false;

            StartCoroutine("ExitTarget");           
        }
    }

    private void ElevatorMoving(Transform elevator)
    {
        if (elevator == elevatorObject)
        {
            if(CheckPlayerInPosition())
            {
                LockPlayer();
            }

            elevatorIsMoving = true;
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
