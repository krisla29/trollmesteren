﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    public PlayerController playerController;    
    public PlayerMovementNormal playerMovementNormal;
    public PlayerMovementFlying playerMovementFlying;
    public PlayerMovementLocked playerMovementLocked;
    public PlayerMovementAgent playerMovementAgent;
    public PlayerMovement currentMovement;

    void Start()
    {

    }

    void Update()
    {
        CheckConditions();
    }

    void CheckConditions()
    {
        if(playerController.isFlying)
        {
            playerController.characterController.enabled = true;
            playerController.navMeshAgent.enabled = false;
            playerController.coll.enabled = false;

            playerMovementFlying.enabled = true;
            currentMovement = playerMovementFlying;
            playerMovementNormal.enabled = false;
            playerMovementAgent.enabled = false;
            playerMovementLocked.enabled = false;
        }
        else if(playerController.isLocked)
        {
            playerController.navMeshAgent.enabled = false;
            playerController.characterController.enabled = false;            
            playerController.coll.enabled = true;

            playerMovementLocked.enabled = true;
            currentMovement = playerMovementLocked;
            playerMovementNormal.enabled = false;
            playerMovementFlying.enabled = false;
            playerMovementAgent.enabled = false;
        }
        else if (playerController.isNavMeshAgent)
        {
            playerController.characterController.enabled = false;
            playerController.navMeshAgent.enabled = true;
            playerController.coll.enabled = true;

            playerMovementAgent.enabled = true;
            currentMovement = playerMovementAgent;
            playerMovementNormal.enabled = false;
            playerMovementFlying.enabled = false;
            playerMovementLocked.enabled = false;
        }
        else if (!playerController.isNavMeshAgent && !playerController.isLocked && !playerController.isFlying)
        {
            playerController.characterController.enabled = true;
            playerController.navMeshAgent.enabled = false;
            playerController.coll.enabled = false;

            playerMovementNormal.enabled = true;
            currentMovement = playerMovementNormal;
            playerMovementAgent.enabled = false;
            playerMovementFlying.enabled = false;
            playerMovementLocked.enabled = false;
        }
    }
}
