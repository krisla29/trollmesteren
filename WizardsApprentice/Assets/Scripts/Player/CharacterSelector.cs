﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterSelector : MonoBehaviour
{
    public PlayerController playerController;
    public AddCharacterEffect addCharacterEffectScript;
    public enum PlayerCharacter { Boy = 0, Girl = 1, Wizard = 2, OG_Boy = 3, TG_Girl = 4, SH_Boy = 5, MG_Boy = 6, SM_Boy = 7, FG_Girl = 8, AV_Girl = 9, CG_Girl = 10, None = 11 }
    public PlayerCharacter playerCharacter;
    private PlayerCharacter lastCharacter;
    [System.Serializable]
    public class Character
    {
        public Animator characterAnim;
        public PlayerCharacter characterType;
        public Transform head;
        public Transform rightHand;
        public Transform rightForeArm;
        public Transform rightArm;
    }

    public List<Character> characters = new List<Character>();

    public delegate void CharacterEvent(PlayerCharacter character); //Sends to CharacterSelectorOther, FriendlyCharacterController, Broom, PlayerStats, CapturedCharacter
    public static event CharacterEvent OnChangeCharacter;

    private void Start()
    {
        SetActiveCharacter(playerCharacter);

        if(!playerController.gameManager.playerStats.hasSeenOpening)
            playerController.anim.transform.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        SetActiveCharacter(playerCharacter);

        if (!playerController.gameManager.playerStats.hasSeenOpening)
            playerController.anim.transform.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(playerCharacter != lastCharacter)
        {
            switch (playerCharacter)
            {
                case(PlayerCharacter.Boy):
                    SetActiveCharacter(PlayerCharacter.Boy);
                    break;
                case (PlayerCharacter.Girl):
                    SetActiveCharacter(PlayerCharacter.Girl);
                    break;
                case (PlayerCharacter.Wizard):
                    SetActiveCharacter(PlayerCharacter.Wizard);
                    break;
                case (PlayerCharacter.AV_Girl):
                    SetActiveCharacter(PlayerCharacter.AV_Girl);
                    break;
                case (PlayerCharacter.CG_Girl):
                    SetActiveCharacter(PlayerCharacter.CG_Girl);
                    break;
                case (PlayerCharacter.FG_Girl):
                    SetActiveCharacter(PlayerCharacter.FG_Girl);
                    break;
                case (PlayerCharacter.MG_Boy):
                    SetActiveCharacter(PlayerCharacter.MG_Boy);
                    break;
                case (PlayerCharacter.OG_Boy):
                    SetActiveCharacter(PlayerCharacter.OG_Boy);
                    break;
                case (PlayerCharacter.SH_Boy):
                    SetActiveCharacter(PlayerCharacter.SH_Boy);
                    break;
                case (PlayerCharacter.SM_Boy):
                    SetActiveCharacter(PlayerCharacter.SM_Boy);
                    break;
                case (PlayerCharacter.TG_Girl):
                    SetActiveCharacter(PlayerCharacter.TG_Girl);
                    break;
                default:
                    SetActiveCharacter(PlayerCharacter.Boy);
                    break;
            }
        }

        lastCharacter = playerCharacter;
    }

    void SetActiveCharacter(PlayerCharacter requestedCharacter)
    {
        for (int i = 0; i < characters.Count; i++)
        {
            if(characters[i].characterType == requestedCharacter)
            {
                characters[i].characterAnim.transform.SetSiblingIndex(0);
                characters[i].characterAnim.transform.gameObject.SetActive(true);
                playerController.anim = characters[i].characterAnim;
                SetActiveBodyParts(characters[i]);
            }
            else
            {
                characters[i].characterAnim.transform.gameObject.SetActive(false);
            }
        }

        if (OnChangeCharacter != null)
            OnChangeCharacter(requestedCharacter);
        
        if(!playerController.gameManager.playerStats.hasSeenOpening)
            playerController.anim.transform.gameObject.SetActive(false);
        else
        {
            //Wizard spriter rules;

            if(requestedCharacter == PlayerCharacter.Wizard)
            {
                if (playerController.spriter.gameObject.activeSelf)
                    playerController.spriter.gameObject.SetActive(false);
            }
            else
            {
                if(!playerController.spriter.gameObject.activeSelf)
                {
                    if(!playerController.gameManager.isScene)
                    {
                        playerController.spriter.gameObject.SetActive(true);
                    }
                    else if(playerController.gameManager.isScene && playerController.gameManager.currentScene)
                    {
                        if(!playerController.gameManager.currentScene.hideSpriter)
                        {
                            playerController.spriter.gameObject.SetActive(true);
                        }
                    }
                }                    
            }
        }
            
    }

    void SetActiveBodyParts(Character character)
    {
        /*
        string rootPath = root.name;
        playerController.head = GameObject.Find(rootPath + "/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head").transform;
        playerController.rightHand = GameObject.Find(rootPath + "/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/mixamorig:RightHand").transform;
        playerController.rightForearm = GameObject.Find(rootPath + "/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm").transform;
        playerController.rightArm = GameObject.Find(rootPath + "/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm").transform;
        */
        playerController.head = character.head;
        playerController.rightHand = character.rightHand;
        playerController.rightForearm = character.rightForeArm;
        playerController.rightArm = character.rightArm;
    }

    public void ToggleCharacter()
    {
        int index = 0;

        if (playerController.gameManager.playerStats.unlockedCharacters.Count > 0)
        {
            for (int i = 0; i < characters.Count; i++)
            {
                if (characters[i].characterType == playerCharacter)
                    index = i;
            }

            var unlocked = playerController.gameManager.playerStats.unlockedCharacters;
            index = unlocked.IndexOf(index);

            if (index < unlocked.Count - 1)
            {
                var next = unlocked[index + 1];
                print(next);
                playerCharacter = characters[next].characterType;               
            }
            else
            {
                playerCharacter = characters[unlocked[0]].characterType;
            }

            addCharacterEffectScript.ChangeCharacterEffect();
        }
        else
        {
            addCharacterEffectScript.NoCharacterToChange();
        }
    }

    public void ToggleMainCharacter() //Used in Cave
    {
        if (playerCharacter == PlayerCharacter.Boy)
            playerCharacter = PlayerCharacter.Girl;
        else
            playerCharacter = PlayerCharacter.Boy;

        addCharacterEffectScript.ChangeCharacterEffect();
    }

    public int GetCharacterIndex(PlayerCharacter passedCharacter)
    {
        int index = 0;
        //index = passedCharacter.GetHashCode();
        
        for (int i = 0; i < characters.Count; i++)
        {
            if (characters[i].characterType == passedCharacter)
                index = i;
        }
        
        return index;
    }
}
