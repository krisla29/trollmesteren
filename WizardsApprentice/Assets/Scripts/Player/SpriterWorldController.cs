﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriterWorldController : MonoBehaviour
{
    public PlayerController player;
    public GameManager gameManager;
    public Animator animator;
    public Follower followerScript;
    public GemDetector gemDetector;
    public float detectorCheckTime = 0.5f;

    public Vector3 offsetToPlayer;
    public Transform playerTarget;
    private Transform currentTarget;
    public bool isLocked;

    private void Start()
    {
        animator.enabled = false;
        offsetToPlayer = transform.localPosition;
        transform.parent = null;
        followerScript.enabled = true;
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();


        InvokeRepeating("CheckGemDetectorList", detectorCheckTime, detectorCheckTime);
    }

    private void OnEnable()
    {
        Interactable.OnInteractablePressed += SetNewFollowerTarget;
        Interactable.OnInteractableReleased += SetFollowerTargetPlayer;
    }

    private void OnDisable()
    {
        Interactable.OnInteractablePressed-= SetNewFollowerTarget;
        Interactable.OnInteractableReleased -= SetFollowerTargetPlayer;
        StopAllCoroutines();
    }

    void CheckGemDetectorList()
    {
        if (gemDetector.gems.Count > 0 && !isLocked)
        {
            if (gemDetector.gems[0])
            {
                SetFollowerCollectable(gemDetector.gems[0]);
            }
        }
    }

    void SetFollowerCollectable(Transform collectable)
    {
        FollowCollectable(collectable);
    }

    private void FollowCollectable(Transform collectable)
    {
        followerScript.target = collectable;
        currentTarget = collectable;

        followerScript.followProperty01.followProperty = Follower.FollowProperties.Position;
        followerScript.followProperty01.maintainOffset = false;
        followerScript.followProperty01.smooth = true;
        followerScript.followProperty01.speed = 10f;

        followerScript.followProperty02.followProperty = Follower.FollowProperties.None;
        followerScript.followProperty03.followProperty = Follower.FollowProperties.None;

        if(!gameManager.isScene || gameManager.currentScene.startPosSpriter == null)
            Invoke("ReturnToFollowPlayer", detectorCheckTime);

        if (gameManager.isScene && gameManager.currentScene.startPosSpriter != null)
            Invoke("ReturnToScenePos", detectorCheckTime);
    }

    private void ReturnToFollowPlayer()
    {
        followerScript.target = playerTarget;
        currentTarget = playerTarget;
        FollowPlayer();
        //FollowInteractable();
    }

    private void ReturnToScenePos()
    {
        followerScript.target = gameManager.currentScene.startPosSpriter;
        currentTarget = gameManager.currentScene.startPosSpriter;
        FollowPlayer();
    }

    void SetNewFollowerTarget(Transform interactableTransform)
    {
        if (!isLocked)
        {
            var interactable = interactableTransform.GetComponent<Interactable>();

            if (null != interactable.particleSystemPrefab)
            {
                followerScript.target = interactableTransform;
                currentTarget = interactableTransform;
                FollowInteractable();
            }
        }
    }

    void SetFollowerTargetPlayer(Transform interactableTransform)
    {
        if (!isLocked)
        {
            if (currentTarget == interactableTransform)
            {
                followerScript.target = playerTarget;
                currentTarget = playerTarget;
                FollowPlayer();
            }
        }
    }

    void FollowInteractable()
    {
        followerScript.followProperty01.followProperty = Follower.FollowProperties.Position;
        followerScript.followProperty01.maintainOffset = false;
        followerScript.followProperty01.smooth = true;
        followerScript.followProperty01.speed = 20f;

        followerScript.followProperty02.followProperty = Follower.FollowProperties.None;
        followerScript.followProperty03.followProperty = Follower.FollowProperties.None;
    }

    void FollowPlayer()
    {
        followerScript.followProperty01.followProperty = Follower.FollowProperties.Position;
        followerScript.followProperty01.maintainOffset = false;
        followerScript.followProperty01.smooth = true;
        followerScript.followProperty01.speed = 5f;

        followerScript.followProperty02.followProperty = Follower.FollowProperties.Rotation;
        followerScript.followProperty02.maintainOffset = false;
        followerScript.followProperty02.smooth = true;
        followerScript.followProperty02.speed = 5f;

        followerScript.followProperty03.followProperty = Follower.FollowProperties.None;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }


}

