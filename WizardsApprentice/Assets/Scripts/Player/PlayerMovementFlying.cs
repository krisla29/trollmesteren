﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementFlying : PlayerMovement
{
    public override void MovementAction()
    {
        if (playerController.isGrounded)
        {
            //moveDirection = (new Vector3(playerCam.forward.x, 0, playerCam.forward.z) * Input.GetAxis("Vertical")) + (new Vector3(playerCam.right.x, 0, playerCam.right.z) * Input.GetAxis("Horizontal"));
            moveDirection = (playerController.orientTransform.forward * inputManager.LeftVertical()) + (playerController.orientTransform.right * inputManager.LeftHorizontal());
            moveDirection *= speed;

            moveSpeed = Vector2.ClampMagnitude(new Vector2(inputManager.LeftHorizontal(), inputManager.LeftVertical()), 1f).magnitude;
            //anim.SetFloat(speed, moveSpeed, speedDampTime, Time.deltaTime);
            playerController.anim.SetFloat("Speed", moveSpeed);

            playerController.anim.SetBool(playerController.groundedBool, true);
        }
        else
        {
            //moveDirection = (new Vector3(playerCam.forward.x, 0, playerCam.forward.z) * Input.GetAxis("Vertical")) + (new Vector3(playerCam.right.x, 0, playerCam.right.z) * Input.GetAxis("Horizontal"));
            moveDirection = (playerController.orientTransform.forward * inputManager.LeftVertical()) + (playerController.orientTransform.right * inputManager.LeftHorizontal());
            moveDirection *= speed;
            playerController.anim.SetBool(playerController.groundedBool, false);
        }

        //if (characterController.isGrounded)
        if (inputManager.Action() && !playerController.isInteracting && !playerController.toggleAim && !playerController.toggleAimThirdPerson)
        {
            //anim.SetBool(jumpBool, true);
            moveDirection.y = jumpSpeed;
            playerController.isGrounded = false;
        }
        else
        {
            //anim.SetBool(jumpBool, false);
        }


        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)

        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        playerController.characterController.Move(moveDirection * Time.deltaTime);

        // Rotate the transform
        if (!playerController.toggleAim)
        {
            if (inputManager.LeftHorizontal() != 0 || inputManager.LeftVertical() != 0)
            {
                Quaternion targetRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0, moveDirection.z), Vector3.up);

                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotSpeed);
            }
        }
        else
        {
            transform.rotation *= Quaternion.AngleAxis(inputManager.RightHorizontal() * Time.deltaTime * 200f, Vector3.up);
        }
    }
}

