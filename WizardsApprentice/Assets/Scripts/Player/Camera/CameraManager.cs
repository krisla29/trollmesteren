﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public bool isEntering = false;
    public bool isExiting = false;
    public float timeToChange = 2f;
    public float timer = 0;
    private float startTime;
    public Camera playerCamera;
    public PlayerController playerController;
    public Camera currentCamera;
    public HudCanvas hudCanvas;
    public GameManager gameManager;
    public delegate void CameraChange(Transform transform);
    public static event CameraChange OnChangeToSceneCam;
    public static event CameraChange OnChangeToSceneCamStart;
    public static event CameraChange OnChangeToPlayerCam;
    public static event CameraChange OnChangeToPlayerCamStart;

    private void Start()
    {
        hudCanvas = GameObject.FindGameObjectWithTag("HUD").GetComponent<HudCanvas>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Scene"))
        {
            timer = 0;
            isEntering = true;
            isExiting = false;

            if (OnChangeToSceneCamStart != null)
            {
                OnChangeToSceneCamStart(other.transform.parent); // is scene 
            }

            StartCoroutine(CountDownToSceneCam(other));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Scene"))
        {
            timer = 0;
            isExiting = true;
            isEntering = false;

            if (OnChangeToPlayerCamStart != null)
            {
                OnChangeToPlayerCamStart(playerCamera.transform); // is Player Cam Collider
            }

            StartCoroutine(CountDownToPlayerCam(other));
        }
    }

    private IEnumerator CountDownToSceneCam(Collider other)
    {
        while(timer < timeToChange)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        if(isEntering)
        {
            ChangeToSceneCam(other);
        }

        isEntering = false;
    }

    private IEnumerator CountDownToPlayerCam(Collider other)
    {
        while (timer < timeToChange)
        {
            timer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        
        if (!isEntering)
        {
            ChangeToPlayerCam(other);
        }
        
        isExiting = false;
    }    

    private void ChangeToSceneCam(Collider other)
    {
        var sController = other.transform.parent.gameObject.GetComponent<SceneController>();
        currentCamera = sController.sceneCamera;
        currentCamera.gameObject.SetActive(true);
        currentCamera.gameObject.tag = "MainCamera";
        sController.sceneCanvas.gameObject.SetActive(true);
        playerController.playerCam = currentCamera.transform;

        if (sController.sceneCameraScript.sceneTransform != null)
        {
            playerController.orientTransform = sController.sceneCameraScript.sceneTransform;
        }
        playerCamera.enabled = false;
        playerCamera.GetComponent<AudioListener>().enabled = false;

        if (sController.isNavmesh)
        {
            playerController.isNavMeshAgent = true;

            if (gameManager.controlsMode == GameManager.ControlsMode.Mobile)
            {
                hudCanvas.camJoystick.gameObject.SetActive(false);
                hudCanvas.actionButton.gameObject.SetActive(false);
                hudCanvas.zoomSlider.gameObject.SetActive(false);
                hudCanvas.mainJoystick.gameObject.SetActive(false);
                hudCanvas.aimButton.gameObject.SetActive(false);
            }
        }
        if (sController.disableAim)
        {
            playerController.disableAim = true;

            if (hudCanvas.aimButton.gameObject.activeSelf)
            {
                hudCanvas.aimButton.gameObject.SetActive(false);                
            }
            if (playerController.toggleAim)
            {
                playerController.toggleAim = false;
            }
            if (playerController.toggleAimThirdPerson)
            {
                playerController.toggleAimThirdPerson = false;
            }
        }
        if (sController.forceAim)
            playerController.forceAim = true;
        if (sController.lockPlayerMovement)
            playerController.isLocked = true;
        if (sController.startPos != null)
        {
            if (!gameManager.isNavMeshAgent && !sController.isNavmesh)
            {
                playerController.transform.position = sController.startPos.position;
            }
            else
            {
                playerController.playerMovementController.playerMovementAgent.SetNewDestination(sController.startPos.position);
            }
        }

        if (OnChangeToSceneCam != null)
        {
            OnChangeToSceneCam(sController.transform);
        }
    }

    private void ChangeToPlayerCam(Collider other)
    {
        var sController = other.transform.parent.gameObject.GetComponent<SceneController>();
        sController.sceneCamera.gameObject.tag = "SceneCam";
        sController.sceneCamera.gameObject.SetActive(false);
        sController.sceneCanvas.gameObject.SetActive(false);
        playerController.orientTransform = playerCamera.transform;

        playerCamera.enabled = true;
        currentCamera = playerCamera;
        playerCamera.GetComponent<AudioListener>().enabled = true;
        playerController.playerCam = playerCamera.transform;

        playerController.isNavMeshAgent = false;
        playerController.disableAim = false;
        playerController.forceAim = false;
        playerController.isLocked = false;

        if (gameManager.controlsMode == GameManager.ControlsMode.Mobile)
        {
            hudCanvas.camJoystick.gameObject.SetActive(true);
            hudCanvas.actionButton.gameObject.SetActive(true);
            hudCanvas.zoomSlider.gameObject.SetActive(true);
            hudCanvas.mainJoystick.gameObject.SetActive(true);
            hudCanvas.aimButton.gameObject.SetActive(true);
        }
        /*
        if(sController.exitPos != null)
        {
            if (!gameManager.isNavMeshAgent && !sController.isNavmesh)
            {
                playerController.transform.position = sController.exitPos.position;
            }
            else
            {
                playerController.playerMovementController.playerMovementAgent.SetNewDestination(sController.exitPos.position);
            }
        }
        */
        if (OnChangeToPlayerCam != null)
        {
            OnChangeToPlayerCam(playerController.playerCam.transform);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
