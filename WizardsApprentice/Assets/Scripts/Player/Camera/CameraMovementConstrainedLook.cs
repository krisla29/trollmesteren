﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementConstrainedLook : CameraMovement
{
    [Range(0f, 2f)] public float inputClampingHorizontal = 0.5f;
    [Range(0f, 2f)] public float inputClampingVertical = 0.5f;

    private float constrainStart;
    private float constrainTimer;
    private bool isConstrained;

    public override void CameraAction()
    {
        base.CameraAction();

        if(controller.player)
        {
            controller.camParent.transform.position = controller.player.transform.position;
        }
        
        transform.localPosition = controller.currentCameraMovement.camOffset;
        transform.localEulerAngles = Vector3.zero;

        if (inputManager.RightAxis() != Vector2.zero)
        {
            FreeLook();
        }

        else
        {
             WaitForConstrained();        
        }
    }

    private void FreeLook()
    {
        isConstrained = false;
        constrainStart = 0;
        constrainTimer = 0;

        float verticalSpeed = 2f;
        float horizontalSpeed = 2f;
        float camSpeed = 50;

        mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());

        mouseLook.y = mouseDelta.y * verticalSpeed * verticalSpeedMultiplier;
        mouseLook.x = mouseDelta.x * horizontalSpeed * horizontalSpeedMultiplier;

        var newRot = new Vector3(-mouseLook.y, mouseLook.x, 0f) * Time.deltaTime * camSpeed * rotationSpeedMultiplier;
        var rot = controller.camParent.transform.eulerAngles + new Vector3(newRot.x, newRot.y, newRot.z);

        var rotX = rot.x;

        if (rotX > 30f && rotX < 180f)
            rotX = 30f;
        if (rotX < 330 && rotX > 180f)
            rotX = 330f;

        controller.camParent.transform.rotation = Quaternion.Euler(rotX, rot.y, rot.z);
        
    }

    private void Constrained()
    {
        isConstrained = true;

        controller.camParent.transform.rotation = Quaternion.Slerp(controller.camParent.transform.rotation, controller.target.rotation, Time.deltaTime * controller.constrainReturnSpeed);
        
    }

    private void WaitForConstrained()
    {
        if (constrainStart == 0)
        {
            constrainStart = Time.time;
        }
        else
        {
            constrainTimer = Time.time - constrainStart;
        }

        if (constrainTimer >= controller.constrainTime)
        {
            if (inputManager.LeftHorizontal() < inputClampingHorizontal && inputManager.LeftHorizontal() > -inputClampingHorizontal)
            {
                if (inputManager.LeftVertical() > -inputClampingVertical)
                {
                    Constrained();
                }
            }
        }
    }
    private void CheckForConstrained()
    {

    }
}
