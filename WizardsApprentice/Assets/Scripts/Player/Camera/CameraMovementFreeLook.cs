﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementFreeLook : CameraMovement
{

    public override void CameraAction()
    {
        controller.camParent.transform.position = controller.player.transform.position;
        transform.localPosition = controller.currentCameraMovement.camOffset;
        transform.localEulerAngles = Vector3.zero;

        base.CameraAction();

        float verticalSpeed = 2f;
        float horizontalSpeed = 2f;
        float camSpeed = 50;

        mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());

        mouseLook.y = mouseDelta.y * verticalSpeed * verticalSpeedMultiplier;
        mouseLook.x = mouseDelta.x * horizontalSpeed * horizontalSpeedMultiplier;

        controller.camParent.transform.eulerAngles += new Vector3(-mouseLook.y, mouseLook.x, 0f) * Time.deltaTime * camSpeed * rotationSpeedMultiplier;
    }
}
