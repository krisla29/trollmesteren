﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementTransition : CameraMovement
{
    public override void CameraAction()
    {
        if(controller.nextCam.CompareTag("SceneCam"))
        {
            //TransitionToScene();
            //print(controller.cameraManager.timer / controller.cameraManager.timeToChange);
        }
        if(controller.nextCam.CompareTag("MainCamera"))
        {
            //TransitionToPlayerCam();
            //print(controller.cameraManager.timer / controller.cameraManager.timeToChange);
        }
    }

    private void TransitionToScene()
    {
        Vector3 lastPos = transform.position;
        transform.position = Vector3.Lerp(lastPos, controller.nextCam.transform.position, controller.cameraManager.timer / controller.cameraManager.timeToChange);
        controller.camParent.transform.rotation = Quaternion.Slerp(controller.camParent.transform.rotation, controller.nextCam.transform.rotation, controller.cameraManager.timer / controller.cameraManager.timeToChange);
    }

    private void TransitionToPlayerCam()
    {
        Vector3 currPos = controller.camParent.transform.position;
        Vector3 currLocalPos = transform.localPosition;
        transform.position = Vector3.Lerp(controller.lastSceneCam.transform.position, controller.camParent.transform.position + camOffset, controller.cameraManager.timer / controller.cameraManager.timeToChange);
        controller.camParent.transform.rotation = Quaternion.Slerp(controller.lastSceneCam.transform.rotation, controller.player.transform.rotation, controller.cameraManager.timer / controller.cameraManager.timeToChange);
    }
}
