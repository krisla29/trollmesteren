﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementAimMobile : CameraMovement
{
    private Ray aimRay = new Ray();
    private RaycastHit aimHit;

    public override void CameraAction()
    {
        base.CameraAction();

        controller.camParent.transform.rotation = Quaternion.Slerp(controller.camParent.transform.rotation, controller.target.rotation, lerpSpeed * 10 * Time.deltaTime);
        Vector3 aimPos = controller.target.position + (controller.target.transform.right * 1f) + (controller.target.transform.up * 1.5f) + (controller.target.transform.forward * -1f);

        transform.position = Vector3.Lerp(transform.position, aimPos, Time.deltaTime * 100f);

        float aimSpeed = 200f;
        ///////////////////////
        /*
        var pointerPos = controller.cam.ScreenToViewportPoint(Input.mousePosition);

        aimRay = controller.cam.ViewportPointToRay(new Vector3(pointerPos.x, pointerPos.y, 0));
        Debug.DrawLine(aimRay.origin, aimRay.origin + aimRay.direction * 100f, Color.blue);

        if(inputManager.AimMobileUp())
        {
            if(Physics.Raycast(aimRay.origin, aimRay.direction, out aimHit, 100f))
            {
                transform.LookAt(aimHit.point, Vector3.up);
                //controller.camParent.transform.LookAt(aimHit.point, Vector3.up);
                print("look at hit");
            }
            else
            {
                transform.LookAt(aimRay.origin + (aimRay.direction*100f), Vector3.up);
                //controller.camParent.transform.LookAt(aimHit.point, Vector3.up);
                print("look at void");
            }
        }
        */
        /////////////////////////
        
        mouseDelta.x = inputManager.RightHorizontal();
        mouseDelta.y = inputManager.RightVertical();
        mouseLook.y = mouseDelta.y * 1.5f;
        mouseLook.x = mouseDelta.x * 1.5f;

        float yRot = transform.localEulerAngles.y;
        float xRot = transform.localEulerAngles.x;

        if (transform.localEulerAngles.x < 60f || transform.localEulerAngles.x > 300f)
        { xRot += -mouseLook.y * Time.deltaTime * aimSpeed; }

        if (transform.localEulerAngles.x >= 60f && transform.localEulerAngles.x <= 300f)
            if (transform.localEulerAngles.x > 60f && transform.localEulerAngles.x < 180f)
            { xRot = Mathf.Lerp(xRot, 59f, Time.deltaTime * aimSpeed); }

        if (transform.localEulerAngles.x < 300f && transform.localEulerAngles.x > 180f)
        { xRot = Mathf.Lerp(xRot, 301f, Time.deltaTime * aimSpeed); }

        transform.localEulerAngles = new Vector3(xRot, 0, 0);
        
    }
    public override void AimLook()
    {
        base.AimLook();
        //print("Got click");
    }

}
