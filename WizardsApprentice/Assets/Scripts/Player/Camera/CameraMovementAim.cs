﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementAim : CameraMovement
{
    public override void CameraAction()
    {
        base.CameraAction();

        controller.camParent.transform.rotation = Quaternion.Slerp(controller.camParent.transform.rotation, controller.target.rotation, lerpSpeed * 10 * Time.deltaTime);
        Vector3 aimPos = controller.target.position + (controller.target.transform.right * 1f) + (controller.target.transform.up * 1.5f) + (controller.target.transform.forward * -1f);

        transform.position = Vector3.Lerp(transform.position, aimPos, Time.deltaTime * 100f);

        float aimSpeed = 100f;
        //print("hello");

        mouseDelta.x = inputManager.RightHorizontal();
        mouseDelta.y = inputManager.RightVertical();
        mouseLook.y = mouseDelta.y * 1.5f;
        mouseLook.x = mouseDelta.x * 1.5f;

        float yRot = transform.localEulerAngles.y;
        float xRot = transform.localEulerAngles.x;

        if (transform.localEulerAngles.x < 60f || transform.localEulerAngles.x > 300f)
        { xRot += -mouseLook.y * Time.deltaTime * aimSpeed; }

        if (transform.localEulerAngles.x >= 60f && transform.localEulerAngles.x <= 300f)
            if (transform.localEulerAngles.x > 60f && transform.localEulerAngles.x < 180f)
            { xRot = Mathf.Lerp(xRot, 59f, Time.deltaTime * 10f); }

        if (transform.localEulerAngles.x < 300f && transform.localEulerAngles.x > 180f)
        { xRot = Mathf.Lerp(xRot, 301f, Time.deltaTime * 10f); }

        transform.localEulerAngles = new Vector3(xRot, 0, 0);
    }

}
