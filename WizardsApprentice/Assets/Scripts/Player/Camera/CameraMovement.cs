﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovement : MonoBehaviour
{
    public GameManager gameManager;
    public InputManager inputManager;
    public CameraMovementController controller;
    public LayerMask Mask;
    public float lerpSpeed = 5f;
    public float returnSpeed = 3f;
    public float startTimer = 0;
    
    public Vector3 camOffset = new Vector3(0, 1.35f, -2.85f);
    public Vector3 currentCamOffset;
    public float zoom = 60;
    public float timer;

    public Vector2 mouseDelta;
    public Vector2 mouseLook;
    public Vector2 smoothV;
    public float horizontalSpeedMultiplier = 1;
    public float verticalSpeedMultiplier = 1;
    public float rotationSpeedMultiplier = 1.5f;

    public float rotSliderSpeed = 2f;

    private float currentZoomLevel;
    public Ray camRay;
    public RaycastHit camHit;
    public bool isObstructed;
    private bool isObstructedInit;

    public void Start()
    {
        //inputManager.rotateSlider.GetComponent<Slider>().OnMove.AddListener(() => ActionClick(actionClickCounter));
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        controller = transform.GetComponent<CameraMovementController>();

        transform.localPosition = camOffset;
        currentCamOffset = camOffset;

        startTimer = Time.time;
        mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());

        //inputManager.aimRect.transform.GetComponent<Button>().onClick.AddListener(AimLook);
        //inputManager.aimRect.transform.GetComponent<TriggerEventsManager>().
        currentZoomLevel = inputManager.zoomLevel;
    }

    public void Update()
    {
        CameraAction();       
    }

    public virtual void CameraAction()
    {
        CollisionHandling();
    }

    public virtual void AimLook()
    {

    }

    public void RotateParent()
    {
        if (inputManager.rotateSlider.gameObject.activeSelf && !controller.rotationLock)
        {
            Vector3 lastRot = Vector3.zero;

            if (inputManager.rotateSlider)
            {
                //if (inputManager.GetRotateHandle())
                //{
                float rotation = (-inputManager.RotateCamera() * rotSliderSpeed);
                float rotationClamped = Mathf.Clamp(rotation, -359, 359);

                if (inputManager.rotationHandlePressed)
                {
                    controller.camParent.transform.localEulerAngles += new Vector3(0, (-inputManager.RotateCamera() * rotSliderSpeed), 0);
                    lastRot = controller.camParent.transform.localEulerAngles;
                }
                
                else if(!inputManager.rotationHandlePressed && !inputManager.isSetRotation)
                {
                    controller.camParent.transform.localEulerAngles += new Vector3(0, (-inputManager.RotateCamera() * rotSliderSpeed), 0);
                }                
            } 
           
        }
    }

    public void CollisionHandling()
    {
        if (!isObstructed)
        {
            camRay = new Ray(controller.cam.transform.position, (controller.player.transform.position + Vector3.up) - controller.cam.transform.position);
            Debug.DrawLine(camRay.origin, camRay.origin + camRay.direction * 100f, Color.green);
        }
        else
        {
            camRay = new Ray(controller.camStandIn.transform.position, (controller.player.transform.position + Vector3.up) - controller.camStandIn.transform.position);
            Debug.DrawLine(camRay.origin, camRay.origin + camRay.direction * 100f, Color.red);
        }
 
        if(Physics.Raycast(camRay, out camHit))
        {
            if(camHit.transform.CompareTag("CamBlocker"))
            {
                print("Cam Hits CamBlocker");
            }

            if((!camHit.transform.CompareTag("Player")  || camHit.transform.CompareTag("CamBlocker")) && camHit.transform.gameObject.layer != 5)
            {
                if(controller.extraCamParent == null)
                {
                    controller.extraCamParent = new GameObject("CollisionCamParent");
                    
                    if(controller.camStandIn == null)
                    {
                        controller.camStandIn = new GameObject("CamStandIn");
                        controller.camStandIn.transform.SetParent(controller.camParent.transform);                        
                    }
                }

                if(!isObstructedInit)
                {
                    controller.extraCamParent.transform.rotation = controller.camParent.transform.rotation;
                    controller.extraCamParent.transform.position = controller.target.position;
                    controller.cam.transform.SetParent(controller.extraCamParent.transform);
                    controller.camStandIn.transform.position = controller.cam.transform.position;
                }

                controller.extraCamParent.transform.position = controller.target.position;
                var rot = controller.extraCamParent.transform.localEulerAngles;
                controller.extraCamParent.transform.rotation = Quaternion.Slerp(controller.extraCamParent.transform.rotation, controller.camParent.transform.rotation * Quaternion.Euler(60f, 1, 1), Time.deltaTime * controller.constrainReturnSpeed);

                isObstructedInit = true;
                isObstructed = true;            
            }
            else
            {
                if (controller.extraCamParent != null)
                {
                    if (isObstructedInit)
                    {
                        controller.extraCamParent.transform.position = controller.target.position;
                        var rot = controller.extraCamParent.transform.localEulerAngles;
                        controller.extraCamParent.transform.rotation = Quaternion.Slerp(controller.extraCamParent.transform.rotation, controller.camParent.transform.rotation, Time.deltaTime * controller.constrainReturnSpeed);



                        if (Quaternion.Dot(controller.extraCamParent.transform.rotation, controller.camParent.transform.rotation) > 0.9999999f ||
                            Quaternion.Dot(controller.extraCamParent.transform.rotation, controller.camParent.transform.rotation) < -0.9999999f)
                        {
                            controller.cam.transform.SetParent(controller.camParent.transform);
                            isObstructed = false;
                            isObstructedInit = false;
                        }
                    }
                }               
            }
        }
    }

    public void LateUpdate()
    {
        RotateParent();
    }

    public void OnEnable()
    {
        if (controller && controller.camParent)
        {
            transform.SetParent(controller.camParent.transform);
        }
    }
}
