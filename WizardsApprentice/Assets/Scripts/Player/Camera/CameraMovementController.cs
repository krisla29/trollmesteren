﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMovementController : MonoBehaviour
{
    public Ray ray;
    public RaycastHit hit;
    public GameManager gameManager;
    public CameraSceneManager cameraManager;
    public PlayerController player;
    public HudCanvas hudCanvas;
    public GameObject camParent;
    public GameObject extraCamParent;
    public GameObject camStandIn;
    public Camera cam;
    public Transform target;
    public bool toggleConstrain = true;
    [Range(1, 2)] public float cameraOffset = 1f;
    public Vector3 camOffset;
    [Range(0, 5)] public float constrainReturnSpeed = 2f;
    public float constrainTime;
    public bool isTransition;

    public bool rotationLock;
    private bool isAiming;
    private bool isNormal;

    public Transform lastSceneCam;
    public Transform nextCam;
    public CameraMovementFreeLook cameraMovementFreeLook;
    public CameraMovementConstrainedLook cameraMovementConstrainedLook;
    public CameraMovementAim cameraMovementAim;
    public CameraMovementAimMobile cameraMovementAimMobile;
    public CameraMovementTransition cameraMovementTransition;
    public CameraMovementAgent cameraMovementAgent;
    public CameraMovement currentCameraMovement;

    public Follower2 followerPos;
    public Follower2 followerLook;

    void Start()
    {
        nextCam = transform;
        hudCanvas = GameObject.FindGameObjectWithTag("HUD").GetComponent<HudCanvas>();
        camParent = new GameObject();
        camParent.name = "CamParent";

        target = player.transform;
        cam = GetComponent<Camera>();

        camParent.transform.position = target.position;
        transform.SetParent(camParent.transform);

        currentCameraMovement = cameraMovementConstrainedLook;
        camOffset = transform.localPosition;
        gameManager = player.gameManager;
        cameraManager = gameManager.cameraSceneManager;
    }

    void Update()
    {
        if (player == null)
        {
            Destroy(transform.parent.gameObject);
        }
        
       // CheckTransition();

        //if (!isTransition)
        //{
            if (!player.isNavMeshAgent)
            {
                AimControl();
            }
            else
            {
                CheckIfAgent();
            }

            ZoomControl();
        //}
        //else
        //{
            //TransitionControl();
        //}
        
    }
    /*
    void CheckTransition()
    {
        if (!cameraManager.isEntering && !cameraManager.isExiting)
        {
            isTransition = false;
            cameraMovementTransition.enabled = false;
        }
        else
        {
            isTransition = true;
        }
    }
    */
    void CheckIfAgent()
    {
        if(!player.gameManager.isScene && player.isNavMeshAgent)
        {
            cameraMovementAgent.enabled = true;
            currentCameraMovement = cameraMovementAgent;
            cameraMovementConstrainedLook.enabled = false;
            cameraMovementAim.enabled = false;
            cameraMovementAimMobile.enabled = false;
            cameraMovementFreeLook.enabled = false;
            cameraMovementTransition.enabled = false;
        }
    }

    void ZoomControl()
    {
        if (!player.toggleAim)
        {
            var co = currentCameraMovement.currentCamOffset;

            currentCameraMovement.camOffset = new Vector3(co.x, co.y * cameraOffset, co.z * cameraOffset) * (1 + player.inputManager.Zoom());
        }
    }

    void AimControl()
    {
        if (player.toggleAim)
        {
            isAiming = true;
            isNormal = false;

            if (!player.inputManager.isTouch)
            {
                currentCameraMovement = cameraMovementAim;
                cameraMovementAim.enabled = true;
                cameraMovementAimMobile.enabled = false;
            }
            else
            {
                currentCameraMovement = cameraMovementAimMobile;
                cameraMovementAimMobile.enabled = true;
                cameraMovementAim.enabled = false;
            }

            cameraMovementAim.enabled = true;
            cameraMovementConstrainedLook.enabled = false;
            cameraMovementFreeLook.enabled = false;
            cameraMovementAgent.enabled = false;

        }

        if (!player.toggleAim)
        {
            isAiming = false;
            isNormal = true;

            if (toggleConstrain)
            {
                if (!player.inputManager.isTouch)
                {
                    currentCameraMovement = cameraMovementConstrainedLook;
                }
                else
                {
                    currentCameraMovement = cameraMovementConstrainedLook;
                }

                cameraMovementConstrainedLook.enabled = true;
                cameraMovementAim.enabled = false;
                cameraMovementAimMobile.enabled = false;
                cameraMovementFreeLook.enabled = false;
                cameraMovementAgent.enabled = false;
            }
            else
            {
                if (!player.inputManager.isTouch)
                {
                    currentCameraMovement = cameraMovementFreeLook;
                }
                else
                {
                    currentCameraMovement = cameraMovementFreeLook;
                }

                currentCameraMovement = cameraMovementFreeLook;
                cameraMovementFreeLook.enabled = true;
                cameraMovementAim.enabled = false;
                cameraMovementAimMobile.enabled = false;
                cameraMovementConstrainedLook.enabled = false;
                cameraMovementAgent.enabled = false;
            }
        }
    }
    /*
    private void TransitionControl()
    {
        cameraMovementFreeLook.enabled = false;
        cameraMovementConstrainedLook.enabled = false;
        cameraMovementAim.enabled = false;
        cameraMovementAimMobile.enabled = false;
        cameraMovementAgent.enabled = false;
        cameraMovementTransition.enabled = true;
        currentCameraMovement = cameraMovementTransition;
}

    private void OnEnable()
    {
        CameraManager.OnChangeToSceneCamStart += TransitionToScene;
        CameraManager.OnChangeToPlayerCamStart += TransitionToPlayer;
    }

    private void OnDisable()
    {
        CameraManager.OnChangeToSceneCamStart -= TransitionToScene;
        CameraManager.OnChangeToPlayerCamStart -= TransitionToPlayer;
    }

    private void TransitionToScene(Transform sceneTransform)
    {
        var sceneController = sceneTransform.GetComponent<SceneController>();

        if (sceneController != null)
        {
            lastSceneCam = sceneController.sceneCamera.transform;
            nextCam = sceneController.sceneCamera.transform;
        }
    }

    private void TransitionToPlayer(Transform playerCameraTransform)
    {
        if (playerCameraTransform.CompareTag("MainCamera"))
        {
            nextCam = playerCameraTransform;
        }
    }
    */
    private void OnDestroy()
    {
        Destroy(camParent);
        Destroy(extraCamParent);
        Destroy(camStandIn);
    }
}

