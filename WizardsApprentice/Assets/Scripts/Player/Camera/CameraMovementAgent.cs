﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementAgent : CameraMovement
{
    [Range(0f, 2f)] public float inputClampingHorizontal = 0.5f;
    [Range(0f, 2f)] public float inputClampingVertical = 0.5f;

    public override void CameraAction()
    {
        base.CameraAction();

        if (controller.player)
        {           
            //if (!controller.player.playerMovementController.playerMovementAgent.isJumping)
            //{
                controller.camParent.transform.position = controller.player.transform.position;
                transform.localPosition = controller.currentCameraMovement.camOffset;
                transform.localEulerAngles = new Vector3(20, 0, 0);

                if (!controller.player.navMeshAgent.hasPath && !controller.player.isAgentRotating && (!inputManager.rotationHandlePressed && !inputManager.isSetRotation))
                {
                        controller.camParent.transform.rotation = Quaternion.Slerp(controller.camParent.transform.rotation, controller.target.rotation, Time.deltaTime * controller.constrainReturnSpeed);
                }
            //}
        }
    }    
}
