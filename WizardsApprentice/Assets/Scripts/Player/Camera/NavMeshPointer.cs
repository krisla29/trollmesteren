﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavMeshPointer : MonoBehaviour
{
    public SpriteRenderer arrow;
    public SpriteRenderer circle;
    public Color arrowStartColor;
    public Color circleStartColor;

    private void OnEnable()
    {
        StartCoroutine("LerpEffects");
    }

    private IEnumerator LerpEffects()
    {
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / 2f;

            arrow.color = Color.Lerp(arrowStartColor, new Vector4(arrowStartColor.r, arrowStartColor.g, arrowStartColor.b, 0), time);
            circle.color = Color.Lerp(circleStartColor, new Vector4(circleStartColor.r, circleStartColor.g, circleStartColor.b, 0), time);
            yield return new WaitForEndOfFrame();
        }

        transform.gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
