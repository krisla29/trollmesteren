﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShooterCanvas : MonoBehaviour
{
    public GameManager gameManager;
    public Shooter shooterScript;
    public Joystick camJoystick;
    public TextMeshProUGUI header;
    public TextMeshProUGUI task;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI startRoundText;
    public RectTransform setCountPanel;
    public Image setCountImage;
    public TextMeshProUGUI setCountPanelText;
    public List<TextMeshProUGUI> bonusTexts = new List<TextMeshProUGUI>();
    public Image powerBarOverlay;
    public Image powerBar;
    public Image powerBarBg;
    public Image taskBg;
    public Image filler;
    public Color fillerColor;
    public float powerBuildUpMult = 0.5f;
    public float power = 0;
    //public bool showUIElements;

    public bool isUiInteracting;

    public List<RectTransform> uiRects = new List<RectTransform>();

    public void Update()
    {
        PowerBarAction();
        IsMouseOverUIelements();
        //CheckInteraction();
    }

    public float LeftHorizontal()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            float r = 0.0f;
            r += Input.GetAxis("Horizontal");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            return camJoystick.Horizontal;
        }
    }

    public float LeftVertical()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            float r = 0.0f;
            r += Input.GetAxis("Vertical");
            return Mathf.Clamp(r, -1.0f, 1.0f);
        }
        else
        {
            return camJoystick.Vertical;
        }
    }

    public Vector2 LeftAxis()
    {
        if (gameManager.controlsMode != GameManager.ControlsMode.Mobile)
        {
            return new Vector2(LeftHorizontal(), LeftVertical());
        }
        else
        {
            return camJoystick.Direction;
        }
    }

    public void PowerBarAction()
    {
        if (shooterScript.interactionType == Shooter.InteractionTypes.PointRelease)
        {
            if (Input.GetMouseButton(0))
            {
                if (!isUiInteracting)
                {
                    var p = Time.deltaTime * powerBuildUpMult;
                    power += p;
                    power = Mathf.Clamp(power, 0, 1f);
                }
            }
            else
            {
                if (power > 0.01)
                    power = Mathf.Lerp(power, 0, Time.deltaTime * 2f);
            }

            powerBar.fillAmount = power;
        }
    }

    public void IsMouseOverUIelements()
    {
        var mousePosition = Input.mousePosition;
        int testInt = 0;

        for (int i = 0; i < uiRects.Count; i++)
        {
            if (uiRects[i])
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(uiRects[i], Input.mousePosition, Camera.main))
                {
                    testInt += 1;
                }
            }
        }
        
        if (testInt == 0)
            isUiInteracting = false;
        else
            isUiInteracting = true;       
    }

    private void OnMouseUp()
    {
        print("FireProjectile");
    }

    void CheckInteraction()
    {
        if(shooterScript.interactionType == Shooter.InteractionTypes.Direct)
        {
            powerBar.gameObject.SetActive(false);
            powerBarBg.gameObject.SetActive(false);
        }
        if(shooterScript.interactionType == Shooter.InteractionTypes.PointRelease)
        {
            powerBar.gameObject.SetActive(true);
            powerBarBg.gameObject.SetActive(true);
        }
    }

    public void HideUIElements()
    {
        camJoystick.gameObject.SetActive(false);
        powerBar.gameObject.SetActive(false);
        powerBarBg.gameObject.SetActive(false);
        powerBarOverlay.gameObject.SetActive(false);
        //timer.gameObject.SetActive(false);
        //taskBg.gameObject.SetActive(false);
        task.gameObject.SetActive(false);
    }

    public void ShowUIElements()
    {
        camJoystick.gameObject.SetActive(true);
        powerBar.gameObject.SetActive(true);
        powerBarBg.gameObject.SetActive(true);
        powerBarOverlay.gameObject.SetActive(true);
        //timer.gameObject.SetActive(true);
        //taskBg.gameObject.SetActive(true);
        task.gameObject.SetActive(true);
    }
    /*
    public void LerpPowerToZero()
    {
        power = Mathf.Lerp(power, 0, Time.deltaTime);
    }
    */
}
