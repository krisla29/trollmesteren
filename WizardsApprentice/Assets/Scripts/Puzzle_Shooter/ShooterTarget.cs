﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShooterTarget : MonoBehaviour
{
    private Shooter.TargetType targetType = Shooter.TargetType.Normal;
    public string text = "0";
    public int value = 0;
    public TextMeshPro textMesh;
    public MeshFilter mesh;
    public MeshRenderer meshRenderer;
    public Material material;
    public SpriteRenderer sprite;
    public EasyRotate2 rotationScript;
    public Material materialInstance;
    public Collider targetCollider;
    public float destroyTime;
    private ShooterTarget thisShooterTarget;
    private bool isDirect;
    private Shooter shooterScript;

    public delegate void ShooterTargetEvent(Transform targetObject, int value, string stringValue, bool isNormal); // Sends To Shooter Script
    public static event ShooterTargetEvent OnTargetHit;
    public static event ShooterTargetEvent OnTargetDestroy;

    private void OnEnable()
    {
        Shooter.OnAssignValue += AssignValue;
        Shooter.OnAssignBonus += AssignBonus;
    }

    private void OnDisable()
    {
        Shooter.OnAssignValue -= AssignValue;
        Shooter.OnAssignBonus -= AssignBonus;
        StopAllCoroutines();
    }

    private void AssignValue(Transform passedObject, int passedValue, string passedText, Shooter passedShooter, Shooter.TargetType passedTargetType)
    {
        StartCoroutine(ScaleTargetUp(passedObject));

        if (passedObject)
        {
            if (passedObject == transform)
            {
                targetType = Shooter.TargetType.Normal;
                text = passedValue.ToString();
                textMesh.text = passedValue.ToString();
                value = passedValue;
                shooterScript = passedShooter;

                if (shooterScript.interactionType == Shooter.InteractionTypes.Direct)
                {
                    isDirect = true;
                }
            }
        }
    }

    void AssignBonus(Transform passedObject, int passedValue, string passedText, Shooter passedShooter, Shooter.TargetType passedTargetType)
    {
        StartCoroutine(ScaleTargetUp(passedObject));

        if (passedObject)
        {
            if (passedObject == transform)
            {
                targetType = Shooter.TargetType.TimeBonus;
                //text = passedValue.ToString();
                //textMesh.text = passedValue.ToString();
                //textMesh.color = Color.black;
                textMesh.gameObject.SetActive(false);
                value = passedValue * 5;
                shooterScript = passedShooter;
                mesh.mesh = passedShooter.bonusTargetMesh;

                var lookRotation = Quaternion.LookRotation(passedShooter.sceneCamera.transform.forward, Vector3.up);
                var lookRotationY = lookRotation.eulerAngles.y;
                mesh.transform.localRotation = Quaternion.Euler(-90, 0, 0);
                transform.rotation *= Quaternion.Euler(0, lookRotationY, 0);
                rotationScript.enabled = true;
                //sprite.color = Color.grey;
                //mesh.transform.localScale = Vector3.one * 2;

                if (passedValue == 3)
                {
                    mesh.transform.localScale = Vector3.one;
                    meshRenderer.material = passedShooter.bonusTargetMaterial;
                    //sprite.color = Color.magenta;
                    sprite.transform.localScale = Vector3.one;
                }
                if (passedValue == 2)
                {
                    mesh.transform.localScale = Vector3.one * 2;
                    meshRenderer.material = passedShooter.bonusTargetMaterial2;
                    //sprite.color = Color.blue;
                    sprite.transform.localScale = Vector3.one * 2;
                }
                if (passedValue == 1)
                {
                    mesh.transform.localScale = Vector3.one * 3;
                    meshRenderer.material = passedShooter.bonusTargetMaterial3;
                    //sprite.color = Color.green;
                    sprite.transform.localScale = Vector3.one * 3;
                }

                if (shooterScript.interactionType == Shooter.InteractionTypes.Direct)
                {
                    isDirect = true;
                }
            }
        }
    }
    
    private void OnMouseDown()
    {
        if (isDirect)
        {
            if (!shooterScript.shooterCanvas.isUiInteracting)
            {
                if (targetType == Shooter.TargetType.Normal)
                {
                    if (null != OnTargetHit)
                    {
                        OnTargetHit(transform, value, text, true);
                    }
                }
                else
                {
                    if (null != OnTargetHit)
                    {
                        OnTargetHit(transform, value, text, false);
                    }
                }

                Invoke("DestroySequence", destroyTime);
            }
        }
    }
    
    private void DestroySequence()
    {
        Destroy(transform.gameObject);
    }

    private void OnDestroy()
    {
        if (targetType == Shooter.TargetType.Normal)
        {
            if (null != OnTargetDestroy)
            {
                OnTargetDestroy(transform, value, text, true);
            }
        }
        else
        {
            if (null != OnTargetDestroy)
            {
                OnTargetDestroy(transform, value, text, false);
            }
        }

        StopAllCoroutines();
    }

    private IEnumerator ScaleTargetUp(Transform passedObject)
    {
        float t = 0;
        float time = 2;

        while(t < 1)
        {
            t += Time.deltaTime / time;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            if(passedObject)
                passedObject.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isDirect)
        {
            if (other.gameObject.CompareTag("ShooterProjectile"))
            {
                if (targetType == Shooter.TargetType.Normal)
                {
                    if (null != OnTargetHit)
                    {
                        OnTargetHit(transform, value, text, true);
                    }
                }
                else
                {
                    if (null != OnTargetHit)
                    {
                        OnTargetHit(transform, value, text, false);
                    }
                }

                Invoke("DestroySequence", destroyTime);
            }
        }
    }
}
