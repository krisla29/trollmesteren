﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterCam : MonoBehaviour
{
    public InputManager inputManager;
    public ShooterCanvas canvas;
    private Ray aimRay = new Ray();
    private RaycastHit aimHit;
    private Vector2 mouseDelta;
    public Vector2 mouseLook;
    public float lerpSpeed = 5f;
    public float horizontalSpeedMultiplier = 1;
    public float verticalSpeedMultiplier = 1;
    public float rotationSpeedMultiplier = 1.5f;

    private void OnEnable()
    {
        Shooter.OnWinRoundCamera += ResetRotation;
        mouseDelta = new Vector2(inputManager.RightHorizontal(), inputManager.RightVertical());
        transform.parent.rotation = Quaternion.identity;
    }
    private void OnDisable()
    {
        Shooter.OnWinRoundCamera -= ResetRotation;
        transform.parent.rotation = Quaternion.identity;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    public void Update()
    {
        Method1();
    } 

    void Method1()
    {
        float verticalSpeed = 2f;
        float horizontalSpeed = 2f;
        float camSpeed = 50;

        mouseDelta = new Vector2(canvas.LeftHorizontal(), canvas.LeftVertical());

        mouseLook.y = mouseDelta.y * verticalSpeed * verticalSpeedMultiplier;
        mouseLook.x = mouseDelta.x * horizontalSpeed * horizontalSpeedMultiplier;

        var newRot = new Vector3(-mouseLook.y, mouseLook.x, 0f) * Time.deltaTime * camSpeed * rotationSpeedMultiplier;
        var rot = transform.parent.eulerAngles + new Vector3(newRot.x, newRot.y, newRot.z);

        var rotX = rot.x;

        if (rotX > 10f && rotX < 180f)
            rotX = 10f;
        if (rotX < 300 && rotX > 180f)
            rotX = 300f;

        transform.parent.rotation = Quaternion.Euler(rotX, rot.y, rot.z);
    }

    void ResetRotation()
    {
        if (transform.parent.rotation != Quaternion.identity)
        {
            StopCoroutine(ResetCamRotation());
            StartCoroutine(ResetCamRotation());
        }
    }

    private IEnumerator ResetCamRotation()
    {
        float t = 0;
        var startRot = transform.parent.rotation;

        while(t < 1)
        {
            t += Time.deltaTime / 1f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.parent.rotation = Quaternion.Slerp(startRot, Quaternion.identity, h);
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }
}
