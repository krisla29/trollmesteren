﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShooterDemo : MonoBehaviour
{
    public RectTransform pointer;
    public CanvasGroup canvasGroup;

    private void OnEnable()
    {
        Reset();
        StartCoroutine(AnimatePointer());
    }

    private void OnDisable()
    {
        Reset();
        StopAllCoroutines();
    }

    private IEnumerator AnimatePointer()
    {
        yield return new WaitForSeconds(5);

        float t = 0;
        float animTime1 = 2f;
        float startTime1 = Time.time;

        while(t < 2)
        {
            t += Time.deltaTime / animTime1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            var posY = pointer.anchoredPosition.y;
            posY = Mathf.Sin((Time.time - startTime1) * 2);
            posY *= 128;
            pointer.anchoredPosition = new Vector2(0, posY);
            print(posY);
            canvasGroup.alpha = Mathf.Lerp(0, 1, h);
            yield return new WaitForEndOfFrame();
        }

        float ti = 0;
        float animTime2 = 2f;
        float startTime2 = Time.time;
        pointer.anchoredPosition = Vector2.zero;

        while (ti < 2)
        {
            ti += Time.deltaTime / animTime2;
            float h = t;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            var posX = pointer.anchoredPosition.x;
            posX = Mathf.Sin((Time.time - startTime2) * 2);
            posX *= 128;
            pointer.anchoredPosition = new Vector2(posX, pointer.anchoredPosition.y);
            yield return new WaitForEndOfFrame();
        }

        float tim = 0;
        float animTime3 = 1;

        while (ti < 1)
        {
            tim += Time.deltaTime / animTime3;
            float h = tim;
            h = tim * tim * tim * (tim * (6f * tim - 15f) + 10f);
            canvasGroup.alpha = Mathf.Lerp(1, 0, tim);
            yield return new WaitForEndOfFrame();
        }

        transform.gameObject.SetActive(false);
    }

    private void Reset()
    {
        pointer.anchoredPosition = Vector2.zero;
        canvasGroup.alpha = 0;
    }
}
