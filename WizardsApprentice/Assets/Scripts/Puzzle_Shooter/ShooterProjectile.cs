﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterProjectile : MonoBehaviour
{
    public Rigidbody rb;
    private bool lookAtVelocity = false;
    private float timeStamp;
    private float waitTime = 0.1f;

    public float lifeTime;
    private float timeStampLife;
    private bool isCountingDownToDeath;

    public void StartCountDown() //Called By ShooterScript
    {
        timeStampLife = Time.time;
        isCountingDownToDeath = true;
    }

    private void Update()
    {
        if (!rb.isKinematic)
        {
            if(!lookAtVelocity)
            {
                timeStamp = Time.time;
                lookAtVelocity = true;
            }
            if (timeStamp < Time.time - waitTime)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(rb.velocity, Vector3.up), Time.deltaTime * 10f);
            }

            if (isCountingDownToDeath)
            {
                if (lifeTime < Time.time - timeStampLife)
                {
                    Destroy(transform.gameObject);
                }
            }
        }
    }
}
