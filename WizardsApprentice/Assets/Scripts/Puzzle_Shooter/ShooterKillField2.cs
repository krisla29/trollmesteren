﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterKillField2 : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("ShooterProjectile"))
        {
            Destroy(other.gameObject);
        }
    }
}
