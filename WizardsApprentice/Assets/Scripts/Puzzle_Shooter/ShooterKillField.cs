﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterKillField : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Destructable"))
        {
            Destroy(other.gameObject);
        }
    }
}
