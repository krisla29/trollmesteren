﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterSpawner : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public GameObject birthEffect;
    public float birthInterval = 10f;
    public float randomValue = 2;
    public Transform targetParent;
    public bool spawnAtStart;
    public AudioSource spawnerBirthSound;
    private GameObject currentObject;
    private float timeStamp;
    private bool timeSet = false;

    public delegate void ShooterSpawnerEvent(Transform spawnedObject); // Sends To Shooter Script
    public static event ShooterSpawnerEvent OnShooterSpawn;

    private void Start()
    {
        if (spawnAtStart)
        {
            SpawnObject();
        }
    }

    private void Update()
    {
        if (!timeSet)
        {
            var rand = Random.Range(-randomValue, randomValue);
            timeStamp = Time.time + rand;
            timeSet = true;
        }
        if (timeSet)
        {
            if (birthInterval <= Time.time - timeStamp)
            {
                SpawnObject();
            }
        }
    }

    private void SpawnObject()
    {
        if (spawnerBirthSound)
        {
            spawnerBirthSound.Play();
        }

        var newObject = Instantiate(prefabToSpawn);

        if (targetParent)
        {
            newObject.transform.SetParent(targetParent);
        }
        if (birthEffect)
        {
            var effect = Instantiate(birthEffect, transform);
        }

        newObject.transform.position = transform.position;

        if(null != OnShooterSpawn)
        {
            OnShooterSpawn(newObject.transform);
        }

        timeSet = false;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
