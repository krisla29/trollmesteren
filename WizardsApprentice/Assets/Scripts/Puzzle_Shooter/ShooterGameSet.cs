﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterGameSet : MonoBehaviour
{
    [Header("Description")]
    [TextArea(2, 10)]
    public string SetDescription;
    public GameObject targetPrefab;
    public GameObject hitPrefab;
    public GameObject correctHitPrefab;
    public GameObject wrongHitPrefab;
    public GameObject bonusHitPrefab;
    public Shooter.OperationType operationType;
    public Shooter.SpawnerPattern spawnerPattern;
    public int spawnerCount;
    public float spawnerRadius;
    public string questionText;
    public int correctMin;
    public int correctMax;
    public int alternativeMin;
    public int alternativeMax;
    [Range(0, 1f)] public float rightWrongRatio;
    public int numberOfCorrects; // Number of corrects to next level
    public int roundTime;
    public float responseTime; // Unused?
    public float targetBirthInterval; // Time before spawn next target
    public float randomRate; // Randomizes previous in seconds
    public int wrongHitPenalty = -2;
    [Range(0, 1f)] public float bonusChance = 0.1f; // 1 = max
    public Vector2Int bonusTimeRange = new Vector2Int(1, 4); // RandomNumber generated from range
    public Vector3 spawnersRotation;
    public float spawnersRotMult;
    public Vector3 targetsRotation;
    public float targetsRotMult;
    public Vector3Int rewards;
}
