﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Shooter : MonoBehaviour, IDifficulty, ISetDifficulty, IAchievementPanel
{
    public PlayerStats.Difficulty shooterDifficulty = PlayerStats.Difficulty.Normal;
    private int correctStreakCounter;
    private int wrongStreakCounter;
    public int numberOfCorrects = 3;
    public int numberOfWrongs = 3;
    public AchievementPanel achievementPanel;
    public enum InteractionTypes { Direct, PointRelease}
    public InteractionTypes interactionType;
    public enum OperationType { AddTwo, SubtractTwo, AddThree, SubtractThree, FindXAdd, FindXSubtract }
    public enum SpawnerPattern { Circle, Random, Grid }
    public enum TargetType { Normal, TimeBonus}
    public GameManager gameManager;
    
    [Header("GAME SETS")]
    public List<ShooterGameSet> shooterGameSets = new List<ShooterGameSet>();
    public List<ShooterGameSet> shooterGameSetsEasy = new List<ShooterGameSet>();
    public List<ShooterGameSet> shooterGameSetsHard = new List<ShooterGameSet>();
    public List<ShooterGameSet> shooterCompletedGameSets = new List<ShooterGameSet>();
    public List<int> completedSetsAchievements = new List<int>();
    public ShooterGameSet currentGameSet;
    [Header("Spawner Settings")]
    public GameObject spawnerPrefab;
    public List<GameObject> currentSpawners = new List<GameObject>();
    [Header("Global Game Settings")]
    public int gameSetCounter = 0;
    public int correctCounter = 0;

    public EasyRotate spawnersRotate;
    public EasyRotate targetsRotate;
    public ShooterCanvas shooterCanvas;
    public ShooterProjectile shooterProjectilePrefab;
    public GameObject gemPhysicalPrefab;
    //public Trophy trophy;
    public GameObject victoryEffectPrefab;
    public GameObject rewardsEffectPrefab;
    public GameObject textInfoPrefab;
    public RectTransform winGamePanel;
    public PlayerStats.Trophy trophy = PlayerStats.Trophy.ShooterGame;
    public RectTransform trophyPanel;
    public RectTransform trophyGraphic;
    public Mesh bonusTargetMesh;
    public Material bonusTargetMaterial;
    public Material bonusTargetMaterial2;
    public Material bonusTargetMaterial3;
    public float projectileSpeed = 18f;
    public AudioSource correctHitSound;
    public AudioSource bonusHitSound;
    public AudioSource wrongHitSound;
    public AudioSource bowDraw;
    public AudioSource bowCancel;
    public AudioSource arrowLoad;
    public AudioSource arrowRelease;
    public AudioSource arrowReleaseOverdrive;
    public AudioSource winRoundSound;
    public AudioSource winGameSound;
    public AudioSource winGameSound2;
    public AudioSource nextRoundSound;
    public AudioSource gameOverSound;
    public AudioSource stopWatchSound;
    public AudioSource counterRewindSound;
    public List<AudioSource> bonusPlings = new List<AudioSource>();
    private ShooterProjectile currentProjectile;
    private float projectileRestPosZ;
    private Ray projectileRestRay;
    private bool hasProjectile;
    private bool isProjectileVisible;
    public Camera sceneCamera;
    public Transform gameTransform;
    public Transform spawnersParent;
    public Transform targetsParent;
    public Transform projectilesParent;

    public Transform shooterDemo;

    private GameObject currenTargetPrefab;
    private GameObject currentHitPrefab;
    private GameObject currentCorrectHitPrefab;
    private GameObject currentWrongHitPrefab;
    private OperationType currentOperationType;
    private SpawnerPattern currentSpawnerPattern;
    private int currentSpawnerCount;
    private float currentSpawnerRadius;
    private string currentQuestionText;
    private int currentCorrectMin;
    private int currentCorrectMax;
    private int currentAlternativeMin;
    private int currentAlternativeMax;
    private int currentNumberOfCorrects;
    private float currentRightWrongRatio;
    private float currentResponseTime;
    private float currentTargetBirthInterval;
    private float currentrandomRate;
    private Vector3 currentSpawnersRotation;
    private Vector3 currentTargetsRotation;
    private float currentSpawnersRotMult;
    private float currentTargetsRotMult;

    private int currentRoundTime = 100;
    public int currentTimer;
    private int startTime;
    private bool isAddingBonus;
    private bool audioLoopIsPlaying;

    private bool gamePaused = true;
    private bool hasWonRound;
    private bool hasLostGame;
    //public int roundsToWin = 6;
    private bool hasWonGame;
    private bool hasCompleted;
    private int completedSets = 0;

    public bool currentTaskComplete;
    public string currentTaskText;
    public string currentTaskTextFull;
    public int currentCorrectValue;
    public int currentA;
    public int currentB;
    public int currentC;    
    public string currentCorrectText;

    public List<Transform> wakeUpOnComplete = new List<Transform>();
    public List<Transform> sleepOnComplete = new List<Transform>();
    public CharacterSelector.PlayerCharacter unlockCharacter;

    public delegate void ShooterEvent(Transform instance, int value, string text, Shooter thisShooterScript, TargetType targetType); //Sends to ShooterTarget
    public static event ShooterEvent OnAssignValue;
    public static event ShooterEvent OnAssignBonus;
    public delegate void ShooterCameraEvent(); // Sends to Shooter Camera
    public static event ShooterCameraEvent OnWinRoundCamera;
    public delegate void ShooterMainEvent(); //Sends to PlayerStats
    public static event ShooterMainEvent OnShooterEnable;
    public static event ShooterMainEvent OnShooterDisable;

    private void Update()
    {
        if (!gamePaused)
        {
            if (interactionType == InteractionTypes.PointRelease)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    if (!shooterCanvas.isUiInteracting)
                    {
                        if (hasProjectile)
                        {
                            FireProjectile();

                            var randNr = Random.Range(0f, 1f);

                            if (randNr > 0.7f && shooterCanvas.power > 0.8f)
                                arrowReleaseOverdrive.Play();
                            else
                                arrowRelease.Play();
                        }
                    }
                }

                ProjectileRestPos();

                if (Input.GetMouseButtonDown(0))
                {
                    if (!shooterCanvas.isUiInteracting)
                    {
                        if (hasProjectile)
                        {
                            bowDraw.Play();
                        }
                    }
                }
            }

            if (!hasLostGame && !hasWonRound && !isAddingBonus)
            {
                if (!audioLoopIsPlaying)
                    stopWatchSound.Play();

                UpdateTimer();

                audioLoopIsPlaying = true;
            }            
        }
        else
        {
            if (audioLoopIsPlaying)
                stopWatchSound.Stop();

            audioLoopIsPlaying = false;
        }
    }

    private void OnEnable()
    {
        TransferPlayerStats();
        //SetPlayerStatsCurrentCompletedSets(); //---FiX 03.03.21---

        if (gameSetCounter > 0)
            StartCoroutine(StartSameRound());
        else
            StartCoroutine(StartFirstRound());

        ShooterTarget.OnTargetHit += TargetHit;
        ShooterTarget.OnTargetDestroy += TargetDestroy;
        ShooterSpawner.OnShooterSpawn += AssignValue;
        DifficultyPanel.OnSetDifficulty += SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel += SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel += StartSelectedGameSet;

        if (OnShooterEnable != null)
            OnShooterEnable();

        OpenDifficultyPanelStart();

        if(shooterDemo)
            shooterDemo.gameObject.SetActive(true);
    }

    private void OnDisable()
    {       
        ShooterTarget.OnTargetHit -= TargetHit;
        ShooterTarget.OnTargetDestroy -= TargetDestroy;
        ShooterSpawner.OnShooterSpawn -= AssignValue;
        DifficultyPanel.OnSetDifficulty -= SetCurrentDifficulty;
        DifficultyPanel.OnSetDifficultyAndRestartLevel -= SetCurrentDifficultyStartSet;
        DifficultyPanel.OnSetDifficultyAndLevel -= StartSelectedGameSet;

        if (shooterCanvas.setCountPanel.gameObject.activeSelf)
            shooterCanvas.setCountPanel.gameObject.SetActive(false);

        if (projectilesParent)
        {
            foreach (Transform child in projectilesParent)
                Destroy(child.gameObject);
        }

        if (counterRewindSound)
        {
            if (counterRewindSound.isPlaying)
                counterRewindSound.Stop();
        }

        if (stopWatchSound)
        {
            if (stopWatchSound.isPlaying)
                stopWatchSound.Stop();
        }

        hasWonGame = false;

        StopAllCoroutines();
        CancelInvoke();

        if (OnShooterDisable != null)
            OnShooterDisable();
    }

    void UpdateTimer()
    {
        int lastTime = currentTimer;
        currentTimer = currentRoundTime - Mathf.FloorToInt(Time.time - startTime);

        if (lastTime != currentTimer)
            shooterCanvas.timer.text = currentTimer.ToString();

        if (currentTimer <= 0)
        {
            StartCoroutine(LoseGame());
            print("LostGame");
            hasLostGame = true;
        }        
    }

    private IEnumerator LoseGame()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);

        gamePaused = true;
        hasLostGame = true;        

        if (OnWinRoundCamera != null)
            OnWinRoundCamera();

        DestroyTargets();
        ClearSpawners();

        //gameSetCounter = 0;

        yield return new WaitForSeconds(0.5f);

        gameOverSound.Play();

        float t = 0;
        var color= shooterCanvas.fillerColor;
        var colorTransparent = new Vector4(color.r, color.g, color.b, 0f);

        while(t < 1)
        {
            t += Time.deltaTime / 2.5f;
            shooterCanvas.filler.color = Vector4.Lerp(colorTransparent, color, t);
            yield return new WaitForEndOfFrame();
        }

        WrongTicker();

        yield return new WaitForSeconds(1);

        StartCoroutine(StartSameRound());
        //StartCoroutine(StartFirstRound());
    }
    void TargetHit(Transform passedTarget, int passedValue, string passedText, bool isNormal)
    {
        if (isNormal)
        {
            if (passedValue == currentCorrectValue)
            {
                Instantiate(currentCorrectHitPrefab, passedTarget.position, Quaternion.identity);
                correctCounter += 1;
                CheckSuccess();
                correctHitSound.Play();
            }
            else
            {
                Instantiate(currentWrongHitPrefab, passedTarget.position, Quaternion.identity);
                var newInfoText = Instantiate(textInfoPrefab, passedTarget.position, Quaternion.identity);
                var infoText = newInfoText.GetComponent<TextMeshPro>();
                infoText.text = currentGameSet.wrongHitPenalty.ToString();
                infoText.color = Color.red;
                infoText.transform.GetChild(0).localPosition += -Vector3.right * 0.3f;
                Failure();
                isAddingBonus = true;
                currentRoundTime += currentGameSet.wrongHitPenalty;
                shooterCanvas.timer.text = (currentTimer + currentGameSet.wrongHitPenalty).ToString();
                isAddingBonus = false;
                wrongHitSound.Play();
            }
        }
        else
        {
            isAddingBonus = true;
            Instantiate(currentGameSet.bonusHitPrefab, passedTarget.position, Quaternion.identity);
            var newInfoText = Instantiate(textInfoPrefab, passedTarget.position, Quaternion.identity);
            var infoText = newInfoText.GetComponent<TextMeshPro>();
            infoText.color = Color.yellow;
            infoText.text = "+" + passedValue.ToString();
            var icon = infoText.transform.GetChild(0);

            if (infoText.text.Length == 2)
                icon.localPosition += -Vector3.right * 0.25f;
            else
                icon.localPosition += Vector3.right * 0.05f;

            bonusHitSound.Play();
            currentRoundTime += passedValue;
            shooterCanvas.timer.text = (currentTimer + passedValue).ToString();
            isAddingBonus = false;
        }
    }

    void TargetDestroy(Transform passedTarget, int passedValue, string passedText, bool isNormal)
    {

    }

    void InitializeRound(int gameSet)
    {
        Resources.UnloadUnusedAssets();
        shooterCanvas.timer.transform.parent.gameObject.SetActive(true);
        var color = shooterCanvas.fillerColor;
        shooterCanvas.filler.color = new Vector4(color.r, color.g, color.b, 0);
        StartCoroutine(CloseRoundText());
        startTime = Mathf.FloorToInt(Time.time);
        ClearSpawners();

        SetGameSetWithDifficulty(gameSet);
        gameManager.playerStats.ShooterSetCount = gameSetCounter;
        //currentGameSet = shooterGameSets[gameSet];
        correctCounter = 0;
        AssignCurrentSetMembers();
        PlaceSpawners(currentSpawnerPattern);
        AssignSpawnerObjects(currenTargetPrefab);
        AssignSpawnerSettings(currentTargetBirthInterval, currentrandomRate);
        GenerateTask(currentOperationType, currentCorrectMin, currentCorrectMax, currentAlternativeMin, currentAlternativeMax);
        AssignGroupRotations();
        shooterCanvas.task.text = currentTaskTextFull;
        hasLostGame = false;
        hasWonRound = false;
        gamePaused = false;
        isAddingBonus = false;
        startTime = Mathf.FloorToInt(Time.time);
        shooterCanvas.ShowUIElements();

        gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
        gameManager.playerStats.SaveGame();
    }

    void StopGame(bool resetGameSetCounter)
    {
        gamePaused = true;

        if (resetGameSetCounter)
        {
            gameSetCounter = 0;
            //gameManager.playerStats.ShooterSetCount = gameSetCounter;
        }

        foreach (var item in shooterCanvas.bonusTexts)
        {
            item.text = "";
            item.transform.parent.gameObject.SetActive(false);
        }

        if (currentProjectile)
            Destroy(currentProjectile.gameObject);

        if (counterRewindSound.isPlaying)
            counterRewindSound.Stop();

        shooterCanvas.HideUIElements();
        shooterCanvas.startRoundText.text = "";
        shooterCanvas.startRoundText.transform.parent.gameObject.SetActive(false);
        startTime = 0;
        shooterCanvas.timer.text = "00";
        ClearSpawners();
        DestroyTargets();

        if (resetGameSetCounter)
            currentGameSet = shooterGameSets[0];

        correctCounter = 0;
        hasLostGame = false;
        hasWonRound = false;
        isAddingBonus = false;
    }

    void AssignValue(Transform spawnedObject)
    {
        float bonusTest = Random.Range(0, 1f);

        if (bonusTest <= currentGameSet.bonusChance)
        {
            AssignBonus(spawnedObject);
        }
        else
        {
            int valueToSend = SetValue(currentRightWrongRatio);
            string textToSend = valueToSend.ToString();

            if (null != OnAssignValue)
            {
                OnAssignValue(spawnedObject, valueToSend, textToSend, this, TargetType.Normal);
            }
        }
    }

    void AssignBonus(Transform passedTarget)
    {
        int bonus = Random.Range(1, 4);
        string bonusText = bonus.ToString();

        if (null != OnAssignBonus)
        {
            OnAssignBonus(passedTarget, bonus, bonusText, this, TargetType.TimeBonus);
        }
    }
    
    void PlaceSpawners(SpawnerPattern pattern)
    {
        print("PlacingSpawners");

        switch (pattern)
        {
            case (SpawnerPattern.Circle):
                PlaceSpawnersCircle(currentSpawnerCount, currentSpawnerRadius);
                break;
            case (SpawnerPattern.Grid):
                PlaceSpawnersGrid(currentSpawnerCount, currentSpawnerRadius);
                break;
            case (SpawnerPattern.Random):
                PlaceSpawnersRandom(currentSpawnerCount, currentSpawnerRadius);
                break;
            default:
                break;
        }
    }

    void PlaceSpawnersCircle(int spawnerNum, float radius)
    {
        for (int i = 0; i < spawnerNum; i++)
        {
            float angle = i * Mathf.PI * 2f / spawnerNum;
            Vector3 newPos = new Vector3(Mathf.Cos(angle) * radius, gameTransform.position.y, Mathf.Sin(angle) * radius);
            newPos += new Vector3(gameTransform.position.x, 0, gameTransform.position.z);
            var instance = Instantiate(spawnerPrefab, newPos, Quaternion.identity);
            instance.transform.SetParent(spawnersParent);
            currentSpawners.Add(instance);
        }
    }

    void PlaceSpawnersGrid(int spawnerNum, float radius)
    {
        var row = Mathf.Sqrt(spawnerNum);
        Vector3 point = gameTransform.position;

        for (int i = 0; i < row; i++)
        {
            point += new Vector3(radius / row, 0, 0);
            var instance = Instantiate(spawnerPrefab, point, Quaternion.identity);
            instance.transform.SetParent(spawnersParent);
            currentSpawners.Add(instance);
            var nPoint = point;

            for (int e = 0; e < row; e++)
            {
                nPoint += new Vector3(0, 0, radius / row);
                var instance2 = Instantiate(spawnerPrefab, nPoint, Quaternion.identity);
                instance2.transform.SetParent(spawnersParent);
                currentSpawners.Add(instance2);
            }
        }

        foreach (var item in currentSpawners)
        {
            item.transform.position -= new Vector3(radius / 2, 0, radius / 2);
        }
    }

    void PlaceSpawnersRandom(int spawnerNum, float radius)
    {
        for (int i = 0; i < spawnerNum; i++)
        {
            Vector3 randomPoint = new Vector3(Random.Range(-radius, radius), 0, Random.Range(-radius, radius));
            randomPoint += gameTransform.position;
            var instance = Instantiate(spawnerPrefab, randomPoint, Quaternion.identity);
            instance.transform.SetParent(spawnersParent);
            currentSpawners.Add(instance);
        }
    }

    void ClearSpawners()
    {
        if (currentSpawners.Count > 0)
        {
            foreach (var item in currentSpawners)
            {
                Destroy(item.gameObject);
            }

            currentSpawners.Clear();
        }
    }

    void AssignCurrentSetMembers()
    {
        currenTargetPrefab = currentGameSet.targetPrefab;
        currentHitPrefab = currentGameSet.hitPrefab;
        currentCorrectHitPrefab = currentGameSet.correctHitPrefab;
        currentWrongHitPrefab = currentGameSet.wrongHitPrefab;
        currentOperationType = currentGameSet.operationType;
        currentSpawnerPattern = currentGameSet.spawnerPattern;
        currentSpawnerCount = currentGameSet.spawnerCount;
        currentSpawnerRadius = currentGameSet.spawnerRadius;
        currentQuestionText = currentGameSet.questionText;
        currentCorrectMin = currentGameSet.correctMin;
        currentCorrectMax = currentGameSet.correctMax;
        currentAlternativeMin = currentGameSet.alternativeMin;
        currentAlternativeMax = currentGameSet.alternativeMax;
        currentNumberOfCorrects = currentGameSet.numberOfCorrects;
        currentRoundTime = currentGameSet.roundTime;
        currentResponseTime = currentGameSet.responseTime;
        currentTargetBirthInterval = currentGameSet.targetBirthInterval;
        currentrandomRate = currentGameSet.randomRate;
        currentRightWrongRatio = currentGameSet.rightWrongRatio;
        currentSpawnersRotation = currentGameSet.spawnersRotation;
        currentTargetsRotation = currentGameSet.targetsRotation;
        currentSpawnersRotMult = currentGameSet.spawnersRotMult;
        currentTargetsRotMult = currentGameSet.targetsRotMult;
}

    void AssignSpawnerObjects(GameObject target)
    {
        for (int i = 0; i<currentSpawners.Count; i++)
        {
            var spawner = currentSpawners[i].GetComponent<ShooterSpawner>();
            spawner.prefabToSpawn = target;
        }
    }

    void AssignSpawnerSettings(float interval, float random)
    {
        for (int i = 0; i < currentSpawners.Count; i++)
        {
            var spawner = currentSpawners[i].GetComponent<ShooterSpawner>();
            spawner.birthInterval = interval;
            spawner.randomValue = random;
            spawner.targetParent = targetsParent;
        }
    }

    int SetValue(float ratio)
    {
        float rand = Random.Range(0, 1f);

        if (ratio > rand)
        {
            return currentCorrectValue;
        }
        else
        {
            int alt = Random.Range(currentAlternativeMin, currentAlternativeMax + 1);

            while(alt == currentCorrectValue)
            {
                alt = Random.Range(currentAlternativeMin, currentAlternativeMax + 1);
            }

            return alt;
        }       
    }
    
    string GenerateTask(OperationType operation, int minCorrect, int maxCorrect, int minAlternative, int maxAlternative)
    {
        currentCorrectValue = Random.Range(minCorrect, maxCorrect + 1);

        switch (operation)
        {            
            case (OperationType.AddTwo):
                currentA = FindAlternativeAddition(currentCorrectValue);
                currentB = currentCorrectValue - currentA;
                currentTaskText = " " + currentA.ToString() + " + " + currentB.ToString() + " =  ? ";
                break;
            case (OperationType.SubtractTwo):
                currentA = FindAlternativeSubtraction(currentCorrectValue);
                currentB = currentCorrectValue + currentA;
                currentTaskText = " " + currentB.ToString() + " - " + currentA.ToString() + " =  ? ";
                break;
            case (OperationType.AddThree):
                currentA = FindAlternativeAddition(currentCorrectValue);
                currentB = FindNextAlternativeAddition(currentCorrectValue, currentA);
                currentC = currentCorrectValue - currentA - currentB;
                currentTaskText = " " + currentA.ToString() + " + " + currentB.ToString() + " + " + currentC.ToString() + " = ? ";               
                break;
            case (OperationType.SubtractThree):
                currentA = FindAlternativeSubtraction(currentCorrectValue);
                currentB = FindNextAlternativeSubtraction(currentCorrectValue, currentA);
                currentC = currentCorrectValue + currentA + currentB;
                currentTaskText = " " + currentC.ToString() + " - " + currentB.ToString() + " - " + currentA.ToString() + " = ? ";
                break;
            case (OperationType.FindXAdd):
                currentA = FindAlternativeSubtraction(currentCorrectValue);
                currentB = currentCorrectValue + currentA;
                var randomizer = Random.Range(0f, 1f);
                if(randomizer < 0.5f)
                    currentTaskText = " " + currentA.ToString() + " + " + " ? " + " = " + currentB.ToString();
                else
                    currentTaskText = " " + " ? " + " + " + currentA.ToString() + " = " + currentB.ToString();
                break;
            case (OperationType.FindXSubtract):
                currentA = FindAlternativeAddition(currentCorrectValue);                
                var randomizer2 = Random.Range(0f, 1f);
                if (randomizer2 < 0.5f)
                {
                    currentB = currentCorrectValue - currentA;
                    currentTaskText = " " + " ? " + "- " + currentB.ToString() + " = " + currentA.ToString();
                }
                else
                {
                    currentB = currentA + currentCorrectValue;
                    currentTaskText = " " + currentB.ToString() + " -" + " ? " + " = " + currentA.ToString();
                }
                break;
            default:
                break;
        }

        currentTaskTextFull = currentQuestionText + " " + currentTaskText;

        return currentTaskTextFull;
    }

    int FindAlternativeAddition(int answer)
    {
        var termA = Random.Range(currentAlternativeMin, answer + 1);
        return termA;
    }

    int FindAlternativeSubtraction(int answer)
    {
        var termA = Random.Range(answer, (currentAlternativeMax - answer) + 1);
        return termA;
    }

    int FindNextAlternativeAddition(int answer, int A)
    {
        var termB = Random.Range(currentAlternativeMin, answer - A + 1);
        return termB;
    }

    int FindNextAlternativeSubtraction(int answer, int A)
    {
        var termB = Random.Range(answer, (currentAlternativeMax - A) + 1);
        return termB;
    }

    void AssignGroupRotations()
    {
        spawnersRotate.rotationAxis = currentSpawnersRotation;
        spawnersRotate.rotationMultiplier = currentSpawnersRotMult;
        targetsRotate.rotationAxis = currentTargetsRotation;
        targetsRotate.rotationMultiplier = currentTargetsRotMult;
    }

    void CheckSuccess()
    {        
        //Some response code...
        if(correctCounter < currentNumberOfCorrects)
        {
            GenerateTask(currentOperationType, currentCorrectMin, currentCorrectMax, currentAlternativeMin, currentAlternativeMax);
            shooterCanvas.task.text = currentTaskTextFull;
        }
        else
        {
            hasWonRound = true;
            gamePaused = true;
            DestroyTargets();
            ClearSpawners();
            shooterCanvas.HideUIElements();            

            if (OnWinRoundCamera != null)
                OnWinRoundCamera();

            StartCoroutine(WinRound());               
        }
    }

    private void DestroyTargets()
    {
        foreach (Transform child in targetsParent)
        {
            Destroy(child.gameObject);
        }
    }

    private IEnumerator WinRound()
    {
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);

        yield return new WaitForSeconds(1f);

        winRoundSound.Play();
        var winRoundEffect = Instantiate(victoryEffectPrefab, sceneCamera.transform.position + (sceneCamera.transform.forward * 2), Quaternion.identity, sceneCamera.transform);
        
        yield return new WaitForSeconds(1f);

        var achievement = CheckAchievement();
        var pastAchievement = 0;

        if (!shooterCompletedGameSets.Contains(currentGameSet))
        {
            shooterCompletedGameSets.Add(currentGameSet);
            completedSetsAchievements.Add(achievement);
            print("Won new Round!");
        }
        else
        {
            int achivementIndex = shooterCompletedGameSets.IndexOf(currentGameSet);
            pastAchievement = completedSetsAchievements[shooterCompletedGameSets.IndexOf(currentGameSet)];
            print("past achievement was " + pastAchievement);
        }

        SetCurrentCompletedSet(achievement);

        int oldAchievement = pastAchievement;
        int levelIndex = CheckLevelIndex(shooterDifficulty);
        int currentAchievement = achievement;

        OpenAchievementPanel(achievementPanel, shooterDifficulty, levelIndex, shooterGameSets.Count, oldAchievement, currentAchievement);

        yield return new WaitForSeconds(2.5f);

        StartCoroutine(RewindTimer());
        var position = gameTransform.position + sceneCamera.transform.forward * 4.5f + (Vector3.up * 2f);
        var rewardsEffect = Instantiate(rewardsEffectPrefab, position, Quaternion.identity, gameTransform);
        var reward = (achievement - pastAchievement) * 5;

        if (reward <= 0)
            reward = 1;

        for (int i = 0; i < reward; i++) // = 5 gems per star
        {
            var randTime = Random.Range(0, 0.25f);
            float t = 0;

            while(t < 1)
            {
                t += Time.deltaTime / randTime;
                yield return new WaitForEndOfFrame();
            }

            SpawnGem();
        }

        yield return new WaitForSeconds(4.5f);
        /*
        for (int i = 0; i < shooterCanvas.bonusTexts.Count; i++)
        {
            float t = 0;

            while (t < 1)
            {
                t += Time.deltaTime / 0.35f;
                float h = t;
                h = t * t * t * (t * (6f * t - 15f) + 10f);
                shooterCanvas.bonusTexts[i].transform.parent.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
                yield return new WaitForEndOfFrame();
            }

            shooterCanvas.bonusTexts[i].text = "";
            shooterCanvas.bonusTexts[i].transform.parent.gameObject.SetActive(false);
            print("hid bonus text " + i.ToString());
        }
        */

        CorrectTicker();

        if (gameSetCounter == shooterGameSets.Count - 1)
        {
            StartCoroutine(WinGame());            
        }
        else
        {
            StartCoroutine(StartNextRound());
        }       
    }

    private IEnumerator WinGame()
    {
        if (gameSetCounter < shooterGameSets.Count - 1)
        {
            gameSetCounter += 1;
            //gameManager.playerStats.ShooterSetCount = gameSetCounter;
        }

        shooterCanvas.timer.transform.parent.gameObject.SetActive(false);

        if (!hasWonGame)
        {
            trophyPanel.gameObject.SetActive(true);
            yield return new WaitForSeconds(2);
            if (winGameSound)
                winGameSound.Play();
            trophyGraphic.gameObject.SetActive(true);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(false);
            yield return new WaitForSeconds(2);
            gameManager.hudCanvas.trophyPanel.GainTrophy(trophy);
            trophyGraphic.gameObject.SetActive(false);
            trophyPanel.gameObject.SetActive(false);
            yield return new WaitForSeconds(3);
            gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
            gameManager.hudCanvas.trophyPanel.closePanelButton.gameObject.SetActive(true);
            gameManager.playerStats.trophies.Add(trophy);
            gameManager.playerStats.hasWonShooterGame = true;
        }
        /*
        var position2 = gameTransform.position + sceneCamera.transform.forward * 4.5f + (Vector3.up * 2f);
        var rewardsEffect = Instantiate(rewardsEffectPrefab, position2, Quaternion.identity, gameTransform);

        for (int i = 0; i < 50; i++)
        {
            var randTime = Random.Range(0, 0.15f);
            float tim = 0;

            while (tim < 1)
            {
                tim += Time.deltaTime / randTime;
                yield return new WaitForEndOfFrame();
            }

            SpawnGem();
        }
        */

        float time = 0;
        var color = shooterCanvas.fillerColor;
        var colorTransparent = new Vector4(color.r, color.g, color.b, 0f);
        shooterCanvas.filler.color = colorTransparent;

        while (time < 1)
        {
            time += Time.deltaTime / 2.5f;
            shooterCanvas.filler.color = Vector4.Lerp(colorTransparent, color, time);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1f);

        hasWonGame = true;
        gameManager.playerStats.hasWonShooterGame = true;

        if(!hasCompleted)
        {
            foreach (var item in wakeUpOnComplete)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in sleepOnComplete)
            {
                item.gameObject.SetActive(false);
            }
        }

        hasCompleted = true;
        gameManager.playerStats.SaveGame();
        gameManager.hudCanvas.exitButton.GetComponent<ExitButton>().MovePlayerToSceneExit();
    }

    private IEnumerator ActivateWinGamePanel()
    {
        winGamePanel.gameObject.SetActive(true);
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            winGamePanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }
    private IEnumerator CloseWinGamePanel()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            winGamePanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        winGamePanel.gameObject.SetActive(false);
    }

    private IEnumerator StartNextRound()
    {
        if (gameSetCounter < shooterGameSets.Count - 1)
        {
            gameSetCounter += 1;
            //gameManager.playerStats.ShooterSetCount = gameSetCounter;

            if (!hasCompleted && gameSetCounter > completedSets)
                completedSets = gameSetCounter;
        }

        StartCoroutine(SetRoundText());

        yield return new WaitForSeconds(2f);

        InitializeRound(gameSetCounter);

        yield return null;
    }
    
    private IEnumerator StartFirstRound()
    {
        StopGame(true);

        float t = 0;
        var color = shooterCanvas.fillerColor;
        var colorTransparent = new Vector4(color.r, color.g, color.b, 0f);

        while (t < 1)
        {
            t += Time.deltaTime / 2.5f;
            shooterCanvas.filler.color = Vector4.Lerp(color, colorTransparent, t);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(SetRoundText());

        yield return new WaitForSeconds(2f);

        StartCoroutine(LoadProjectile());
        InitializeRound(gameSetCounter);
    }

    private IEnumerator StartSameRound()
    {
        StopGame(false);

        float t = 0;
        var color = shooterCanvas.fillerColor;
        var colorTransparent = new Vector4(color.r, color.g, color.b, 0f);

        while (t < 1)
        {
            t += Time.deltaTime / 2.5f;
            shooterCanvas.filler.color = Vector4.Lerp(color, colorTransparent, t);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(SetRoundText());

        yield return new WaitForSeconds(2f);

        StartCoroutine(LoadProjectile());
        InitializeRound(gameSetCounter);
    }

    private IEnumerator SetRoundText()
    {
        nextRoundSound.Play();

        if (shooterDifficulty == PlayerStats.Difficulty.Easy)
            shooterCanvas.setCountImage.color = gameManager.colorEasy;
        else if (shooterDifficulty == PlayerStats.Difficulty.Normal)
            shooterCanvas.setCountImage.color = gameManager.colorNormal;
        else if (shooterDifficulty == PlayerStats.Difficulty.Hard)
            shooterCanvas.setCountImage.color = gameManager.colorHard;
        else
            shooterCanvas.setCountImage.color = Color.grey;

        shooterCanvas.setCountPanelText.text = (gameSetCounter + 1).ToString() + "/" + shooterGameSets.Count.ToString();

        if (shooterCanvas.setCountPanelText.text.Length < 4)
            shooterCanvas.setCountPanelText.fontSize = 220;
        else if (shooterCanvas.setCountPanelText.text.Length == 4)
            shooterCanvas.setCountPanelText.fontSize = 200;
        else if (shooterCanvas.setCountPanelText.text.Length > 4)
            shooterCanvas.setCountPanelText.fontSize = 160;

        shooterCanvas.setCountPanel.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            shooterCanvas.setCountPanel.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1);

        float ti = 0;

        while (ti < 1)
        {
            ti += Time.deltaTime / 1;
            float h = ti;
            h = ti * ti * ti * (ti * (6f * ti - 15f) + 10f);
            shooterCanvas.setCountPanel.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        shooterCanvas.setCountPanel.gameObject.SetActive(false);
        /*
        shooterCanvas.startRoundText.transform.parent.localScale = Vector3.zero;

        shooterCanvas.startRoundText.text = (gameSetCounter + 1).ToString() + "/" + shooterGameSets.Count.ToString();

        shooterCanvas.startRoundText.transform.parent.gameObject.SetActive(true);

        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 0.5f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            shooterCanvas.startRoundText.transform.parent.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }    
        */
    }

    private IEnumerator SetWinText()
    {
        shooterCanvas.startRoundText.transform.parent.localScale = Vector3.zero;
        shooterCanvas.startRoundText.text = "^O^^O^^O^";
        shooterCanvas.startRoundText.transform.parent.gameObject.SetActive(true);

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 0.5f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            shooterCanvas.startRoundText.transform.parent.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator CloseRoundText()
    {        
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 0.5f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            shooterCanvas.startRoundText.transform.parent.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        shooterCanvas.startRoundText.transform.parent.gameObject.SetActive(true);
    }

    private IEnumerator RewindTimer()
    {
        float t = 0;
        int tempTimer = currentTimer;
        int startTime = currentTimer;
        counterRewindSound.Play();


        while(t < 1)
        {
            t += Time.deltaTime / 3f;

            //if (t >= ((currentTimer/100) * ((currentTimer - tempTimer) * 3)) && tempTimer > 0)
            if (tempTimer > 0 && t >= (((startTime / tempTimer) / startTime) / 3)) 
            {
                tempTimer--;
                shooterCanvas.timer.text = tempTimer.ToString();
            }

            if(tempTimer == 0)
            {
                if (counterRewindSound.isPlaying)
                    counterRewindSound.Stop();
            }

            yield return new WaitForEndOfFrame();
        }

        if(tempTimer > 0)
        {
            tempTimer = 0;
            shooterCanvas.timer.text = tempTimer.ToString();
        }

        if (counterRewindSound.isPlaying)
            counterRewindSound.Stop();
    }

    void SpawnGem()
    {
        Vector3 position = Vector3.zero;

        int randNum = Random.Range(0, 2);

        if(randNum == 0)
            position = Random.insideUnitSphere + gameTransform.position + (sceneCamera.transform.forward * 4.5f) + (Vector3.up * 2f) + (sceneCamera.transform.right * 2);
        else
            position = Random.insideUnitSphere + gameTransform.position + (sceneCamera.transform.forward * 4.5f) + (Vector3.up * 2f) + (-sceneCamera.transform.right * 2);

        var newGem = Instantiate(gemPhysicalPrefab, position, Quaternion.identity, gameTransform);
    }

    void Failure()
    {
        //Some response code...
    }

    int CheckAchievement()
    {
        int achievement = 0;

        if (currentTimer >= 30f)
        {            
            achievement = 3;
            //StartCoroutine(ShowBonus(shooterCanvas.bonusTexts[0], 5, 1f));
            //StartCoroutine(ShowBonus(shooterCanvas.bonusTexts[1], 10, 2f));
            //StartCoroutine(ShowBonus(shooterCanvas.bonusTexts[2], 15, 3f));
        }
        else if (currentTimer < 30f && currentTimer >= 20f)
        {            
            achievement = 2;
            //StartCoroutine(ShowBonus(shooterCanvas.bonusTexts[0], 5, 1f));
            //StartCoroutine(ShowBonus(shooterCanvas.bonusTexts[1], 10, 2f));
        }
        else if (currentTimer < 20f && currentTimer >= 10f)
        {            
            achievement = 1;
            //StartCoroutine(ShowBonus(shooterCanvas.bonusTexts[0], 5, 1f));
        }

        return achievement;
    }

    private IEnumerator ShowBonus(TextMeshProUGUI bonusTextObject, int bonusValue, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        if (waitTime == 1f)
            bonusPlings[0].Play();
        else if (waitTime == 2f)
            bonusPlings[1].Play();
        else if (waitTime == 3f)
            bonusPlings[2].Play();

        float t = 0;
        bonusTextObject.transform.parent.gameObject.SetActive(true);
        bonusTextObject.text = "+" + bonusValue.ToString();

        while(t < 1)
        {
            t += Time.deltaTime / 0.5f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            bonusTextObject.transform.parent.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, h);
            yield return new WaitForEndOfFrame();
        }
    }   

    private IEnumerator LoadProjectile()
    {
        arrowLoad.Play();
        yield return new WaitForSeconds(0.5f);
        isProjectileVisible = false;
        currentProjectile = Instantiate(shooterProjectilePrefab, sceneCamera.transform.position + (-Vector3.up * 0.7f) + (sceneCamera.transform.forward * 1.5f), sceneCamera.transform.parent.rotation);
        currentProjectile.transform.SetParent(sceneCamera.transform);
        projectileRestPosZ = currentProjectile.transform.localPosition.z;       
        hasProjectile = true;
    }

    void ProjectileRestPos()
    {
        if(currentProjectile && hasProjectile)
        {
            projectileRestRay = sceneCamera.ScreenPointToRay(Input.mousePosition);
            Debug.DrawLine(projectileRestRay.origin, projectileRestRay.origin + projectileRestRay.direction * 100f, Color.black);
            currentProjectile.transform.rotation = Quaternion.LookRotation(projectileRestRay.GetPoint(100) - currentProjectile.transform.position, Vector3.up);

            Vector3 localPos = currentProjectile.transform.localPosition;
            currentProjectile.transform.localPosition = new Vector3(localPos.x, localPos.y, Mathf.Lerp(projectileRestPosZ, projectileRestPosZ - 0.5f, shooterCanvas.power));  
            
            if(!isProjectileVisible)
            {
                currentProjectile.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    void FireProjectile()
    {
        if (currentProjectile)
        {
            var currentRot = currentProjectile.transform.rotation;
            currentProjectile.transform.SetParent(projectilesParent);
            currentProjectile.transform.rotation = currentRot;
            var rb = currentProjectile.GetComponent<Rigidbody>();
            rb.rotation = currentRot;
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.AddForce(projectileRestRay.direction * projectileSpeed * shooterCanvas.power, ForceMode.Impulse);
            currentProjectile.GetComponent<TrailRenderer>().enabled = true;
            currentProjectile.StartCountDown();
            hasProjectile = false;
            StartCoroutine(LoadProjectile());
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    public void CheckDifficulty()
    {
        if (!hasWonGame)
        {
            if ((correctStreakCounter >= numberOfCorrects && shooterDifficulty != PlayerStats.Difficulty.Hard) || (wrongStreakCounter >= numberOfWrongs && shooterDifficulty != PlayerStats.Difficulty.Easy))
            {
                if (correctStreakCounter >= numberOfCorrects && shooterDifficulty == PlayerStats.Difficulty.Easy)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;
                else if (correctStreakCounter >= numberOfCorrects && shooterDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Hard;
                else if (wrongStreakCounter >= numberOfWrongs && shooterDifficulty == PlayerStats.Difficulty.Normal)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Easy;
                else if (wrongStreakCounter >= numberOfWrongs && shooterDifficulty == PlayerStats.Difficulty.Hard)
                    gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.Normal;

                gameManager.hudCanvas.difficultyPanel.OpenPanel();
                correctStreakCounter = 0;
                wrongStreakCounter = 0;
            }
        }
    }

    public void SetCurrentDifficulty(PlayerStats.Difficulty requestedDifficulty)
    {
        shooterDifficulty = requestedDifficulty;
        //gameSetCounter = gameManager.playerStats.OrderGameSetCount;
        //InitializeRound(gameSetCounter);
    }

    public void SetCurrentDifficultyStartSet(PlayerStats.Difficulty requestedDifficulty)
    {
        shooterDifficulty = requestedDifficulty;
        StopAllCoroutines();
        CancelInvoke();
        StartCoroutine(StartSameRound());
    }

    public void SetGameSetWithDifficulty(int index)
    {
        switch (shooterDifficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                currentGameSet = shooterGameSetsEasy[index];
                break;
            case (PlayerStats.Difficulty.Normal):
                currentGameSet = shooterGameSets[index];
                break;
            case (PlayerStats.Difficulty.Hard):
                currentGameSet = shooterGameSetsHard[index];
                break;
            default:
                currentGameSet = shooterGameSets[index];
                break;
        }
    }

    public void WrongTicker()
    {
        wrongStreakCounter++;
        correctStreakCounter = 0;
        CheckDifficulty();
    }

    public void CorrectTicker()
    {
        wrongStreakCounter = 0;
        correctStreakCounter++;
        CheckDifficulty();
    }

    public void OpenDifficultyPanelStart()
    {
        gameManager.hudCanvas.difficultyPanel.suggestedDifficulty = PlayerStats.Difficulty.None;
        gameManager.hudCanvas.difficultyPanel.startNewLevel = true;

        if (gameManager.playerStats.hasWonShooterGame)
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = true;
            gameManager.hudCanvas.difficultyPanel.currentLevelCount = shooterGameSets.Count;
        }
        else
        {
            gameManager.hudCanvas.difficultyPanel.hasTrophy = false;
        }

        gameManager.hudCanvas.difficultyPanel.gameObject.SetActive(true);
    }

    public void SetCurrentCompletedSet(int passedAchievement)
    {
        int setIndex = 0;

        if (shooterGameSets.Contains(currentGameSet))
            setIndex = shooterGameSets.IndexOf(currentGameSet) + shooterGameSets.Count;
        else if (shooterGameSetsEasy.Contains(currentGameSet))
            setIndex = shooterGameSetsEasy.IndexOf(currentGameSet);
        else if (shooterGameSetsHard.Contains(currentGameSet))
            setIndex = shooterGameSetsHard.IndexOf(currentGameSet) + (shooterGameSets.Count * 2);

        if (!gameManager.playerStats.shooterGameCompletedSets.Contains(setIndex))
        {
            gameManager.playerStats.shooterGameCompletedSets.Add(setIndex);
            gameManager.playerStats.shooterGameSetAchievements.Add(setIndex, passedAchievement);
        }

        //SetPlayerStatsCurrentCompletedSets();
    }

    public void SetPlayerStatsCurrentCompletedSets()
    {
        gameManager.playerStats.currentCompletedSets.Clear();
        gameManager.playerStats.currentAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.shooterGameCompletedSets.Count; i++)
        {
            var index = gameManager.playerStats.shooterGameCompletedSets[i];
            gameManager.playerStats.currentCompletedSets.Add(index);
            gameManager.playerStats.currentAchievements.Add(index, gameManager.playerStats.shooterGameSetAchievements[index]);
        }
    }

    public void TransferPlayerStats()
    {
        hasWonGame = gameManager.playerStats.hasWonShooterGame;
        gameSetCounter = gameManager.playerStats.ShooterSetCount;

        shooterCompletedGameSets.Clear();
        completedSetsAchievements.Clear();

        for (int i = 0; i < gameManager.playerStats.shooterGameCompletedSets.Count; i++)
        {
            var setIndex = gameManager.playerStats.shooterGameCompletedSets[i];

            if (setIndex < shooterGameSets.Count)
            {
                shooterCompletedGameSets.Add(shooterGameSetsEasy[setIndex]);
            }
            else if (setIndex >= shooterGameSets.Count && setIndex < (shooterGameSets.Count * 2))
            {
                shooterCompletedGameSets.Add(shooterGameSets[setIndex - shooterGameSets.Count]);
            }
            else if (setIndex >= (shooterGameSets.Count * 2) && setIndex < (shooterGameSets.Count * 3))
            {
                shooterCompletedGameSets.Add(shooterGameSetsHard[setIndex - (shooterGameSets.Count * 2)]);
            }

            completedSetsAchievements.Add(gameManager.playerStats.shooterGameSetAchievements[setIndex]);
        }
    }

    public void StartSelectedGameSet(PlayerStats.Difficulty requestedDifficulty, int requestedLevel)
    {
        shooterDifficulty = requestedDifficulty;

        if (requestedLevel < shooterGameSets.Count)
            gameSetCounter = requestedLevel;
        else if (requestedLevel >= shooterGameSets.Count && requestedLevel < (shooterGameSets.Count * 2))
            gameSetCounter = requestedLevel - shooterGameSets.Count;
        else if (requestedLevel >= shooterGameSets.Count * 2)
            gameSetCounter = requestedLevel - (shooterGameSets.Count * 2);

        shooterDifficulty = requestedDifficulty;
        StopAllCoroutines();
        CancelInvoke();
        StartCoroutine(StartSameRound());
    }

    //IAchievementPanel:------------------------------------

    public void OpenAchievementPanel(AchievementPanel achievementPanel, PlayerStats.Difficulty difficulty, int levelIndex, int totalLevels, int prevAchievement, int achievement)
    {
        achievementPanel.difficulty = difficulty;
        achievementPanel.levelIndex = levelIndex;
        achievementPanel.prevAchievment = prevAchievement;
        achievementPanel.achievement = achievement;
        achievementPanel.totalLevels = totalLevels;

        achievementPanel.gameObject.SetActive(true);
    }

    public int CheckLevelIndex(PlayerStats.Difficulty difficulty)
    {
        switch (difficulty)
        {
            case (PlayerStats.Difficulty.Easy):
                return shooterGameSetsEasy.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Normal):
                return shooterGameSets.IndexOf(currentGameSet) + 1;
            case (PlayerStats.Difficulty.Hard):
                return shooterGameSetsHard.IndexOf(currentGameSet) + 1;
            default:
                return 0;
        }
    }

    public int CheckCurrentAchievement()
    {
        int testAchievement;

        if (currentTimer >= 20f)
        {
            testAchievement = 3;
        }
        else if (currentTimer < 30f && currentTimer >= 20f)
        {
            testAchievement = 2;
        }
        else if (currentTimer < 20f && currentTimer >= 10f)
        {
            testAchievement = 1;
        }
        else
        {
            testAchievement = 0;
        }

        return testAchievement;
    }

    public int CheckPrevAchievement()
    {
        int prevAchievement = 0;
        return prevAchievement;
    }    
}
