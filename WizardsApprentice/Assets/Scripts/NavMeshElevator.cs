﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshElevator : MonoBehaviour
{
    public GameObject elevatorPlatform;
    public GameObject elevatorStart;
    public NavMeshObstacle elevatorStartCarver;
    public GameObject elevatorEnd;
    public NavMeshObstacle elevatorEndCarver;
    public Transform interactable;
    public bool useColliders;
    public Transform manualStartCollider;
    public Transform manualEndCollider;
    public Transform playerTarget;
    public Transform targetTransform;

    public enum ElevatorTypes { Linear, Curved, Path, Manual}
    public ElevatorTypes elevatorType;

    public List<Transform> checkPoints = new List<Transform>();
    
    public float travelTime = 10f;
    public float waitTime = 5f;
    [Header("Read Only")]
    public Transform currentCheckPoint; //ReadOnly
    private int checkPointCounter; //ReadOnly
    public Transform targetPlayer; //ReadOnly
    public PlayerMovementAgent playerMovementAgent; //Readonly
    public bool playerInPosition; //Readonly
    public bool isPlayerLocked; //ReadOnly
    public bool isAtCheckPoint; //ReadOnly

    public bool isAtEnd = false; //ReadOnly
    public bool isAtStart = true; //ReadOnly

    public delegate void NavMeshElevatorEvent(Transform elevator); // EVENTS SENT TO PLAYERTARGET SCRIPT
    public static event NavMeshElevatorEvent OnNavMeshElevatorReachStart;
    public static event NavMeshElevatorEvent OnNavMeshElevatorReachEnd;
    public static event NavMeshElevatorEvent OnNavMeshElevatorStartMoving;

    private void Start()
    {
        if(!isAtStart && !isAtEnd)
        {
            interactable.gameObject.SetActive(false);
        }

        if (elevatorType != ElevatorTypes.Manual)
        {
            StartCoroutine("ElevatorStop");
        }
    }

    private IEnumerator ElevatorStop()
    {
        interactable.gameObject.SetActive(true);

        yield return new WaitForSeconds(waitTime);

        if (OnNavMeshElevatorStartMoving != null)
        {
            OnNavMeshElevatorStartMoving(transform);
        }

        if(targetPlayer && playerInPosition)
        {
            playerMovementAgent = targetPlayer.GetComponent<PlayerMovementAgent>();

            while (playerMovementAgent.enabled)
            {
                print("wait for player to lock");
                yield return new WaitForEndOfFrame();
            }
        }
                
        if (elevatorType != ElevatorTypes.Path)
        {
            if (isAtEnd)
            {
                StartCoroutine("ElevatorTravelToStart");
            }

            if (isAtStart)
            {
                StartCoroutine("ElevatorTravelToEnd");
            }
        }
        else
        {
            for (int i = 0; i < checkPoints.Count; i++)
            {
                if(checkPoints[i] == currentCheckPoint)
                {
                    if(isAtStart)
                    {
                        checkPointCounter = i + 1;
                    }
                    if(isAtEnd)
                    {
                        checkPointCounter = i - 1;
                    }
                }
            }  
            
            StartCoroutine("ElevatorTravelToCheckPoint");
        }

        if (elevatorStart.gameObject.activeSelf)
        {
            elevatorStart.gameObject.SetActive(false);
            elevatorStartCarver.gameObject.SetActive(true);
        }
        if (elevatorEnd.gameObject.activeSelf)
        {
            elevatorEnd.gameObject.SetActive(false);
            elevatorEndCarver.gameObject.SetActive(true);
        }

        interactable.gameObject.SetActive(false);
    }

    private IEnumerator ElevatorTravelToEnd()
    {
        isAtStart = false;

        float time = 0;

        if (elevatorType == ElevatorTypes.Linear)
        {
            while (time < 1)
            {
                time += Time.deltaTime / travelTime;
                elevatorPlatform.transform.position = Vector3.Lerp(elevatorStart.transform.position, elevatorEnd.transform.position, time);
                yield return new WaitForEndOfFrame();
            }
        }

        if (elevatorType == ElevatorTypes.Curved)
        {
            while (time < 1)
            {
                time += Time.deltaTime / travelTime;
                elevatorPlatform.transform.position = Vector3.Slerp(elevatorStart.transform.position, elevatorEnd.transform.position, time);
                yield return new WaitForEndOfFrame();
            }
        }

        isAtEnd = true;
        
        if (!elevatorEnd.activeSelf)
        {
            elevatorEnd.gameObject.SetActive(true);
            elevatorEndCarver.gameObject.SetActive(false);
        }

        if (OnNavMeshElevatorReachEnd != null)
        {
            OnNavMeshElevatorReachEnd(transform);
        }

        StartCoroutine("ElevatorStop");
    }
    private IEnumerator ElevatorTravelToStart()
    {
        isAtEnd = false;

        float time = 0;

        if (elevatorType == ElevatorTypes.Linear)
        {
            while (time < 1)
            {
                time += Time.deltaTime / travelTime;
                elevatorPlatform.transform.position = Vector3.Lerp(elevatorEnd.transform.position, elevatorStart.transform.position, time);
                yield return new WaitForEndOfFrame();
            }

        }

        if (elevatorType == ElevatorTypes.Curved)
        {
            while (time < 1)
            {
                time += Time.deltaTime / travelTime;
                elevatorPlatform.transform.position = Vector3.Slerp(elevatorEnd.transform.position, elevatorStart.transform.position, time);
                yield return new WaitForEndOfFrame();
            }
        }

        isAtStart = true;
        
        if (!elevatorStart.activeSelf)
        {
            elevatorStart.gameObject.SetActive(true);
            elevatorStartCarver.gameObject.SetActive(false);
        }

        if (OnNavMeshElevatorReachStart != null)
        {
            OnNavMeshElevatorReachStart(transform);
        }

        StartCoroutine("ElevatorStop");
    }

    private IEnumerator ElevatorTravelToCheckPoint()
    {
        float time = 0;

        while (time < 1)
        {
            time += Time.deltaTime / travelTime;
            elevatorPlatform.transform.position = Vector3.Lerp(currentCheckPoint.position, checkPoints[checkPointCounter].position, time);
            yield return new WaitForEndOfFrame();
        }

        currentCheckPoint = checkPoints[checkPointCounter];
        
        if(checkPointCounter == checkPoints.Count - 1)
        {
            isAtStart = false;
            isAtEnd = true;
            
            if (!elevatorEnd.activeSelf)
            {
                elevatorEnd.gameObject.SetActive(true);
                elevatorEndCarver.gameObject.SetActive(false);
            }

            if (OnNavMeshElevatorReachEnd != null)
            {
                OnNavMeshElevatorReachEnd(transform);
            }
        }

        if(checkPointCounter == 0)
        {
            isAtStart = true;
            isAtEnd = false;
            
            if (!elevatorStart.activeSelf)
            {
                elevatorStart.gameObject.SetActive(true);
                elevatorStartCarver.gameObject.SetActive(false);
            }

            if (OnNavMeshElevatorReachStart != null)
            {
                OnNavMeshElevatorReachStart(transform);
            }
        }

        StartCoroutine("ElevatorStop");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (useColliders)
        {
            if (elevatorType == ElevatorTypes.Manual)
            {
                if (other.transform == manualStartCollider)
                {
                    ReachStart(transform);
                }

                if (other.transform == manualEndCollider)
                {
                    ReachEnd(transform);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (useColliders)
        {
            if (elevatorType == ElevatorTypes.Manual)
            {
                if (other.transform == manualStartCollider)
                {
                    StartMoving(transform);
                }

                if (other.transform == manualEndCollider)
                {
                    StartMoving(transform);
                }
            }
        }
    }

    private void OnEnable()
    {
        PlayerTarget.OnPlayerEnter += PlayerEnters;
        PlayerTarget.OnPlayerExit += PlayerExits;
        PlayerTarget.OnPlayerLock += PlayerLocked;
        PlayerTarget.OnPlayerUnLock += PlayerUnLocked;
        InteractableBehaviourSlidePath2.OnReachStart += ElevatorEventReachStart;
        InteractableBehaviourSlidePath2.OnReachEnd += ElevatorEventReachEnd;
        InteractableBehaviourSlidePath2.OnLeaveStart += ElevatorEventStartMoving;
        InteractableBehaviourSlidePath2.OnLeaveEnd += ElevatorEventStartMoving;
        PlayerController.OnPlayerDeath += PlayerDies;
    }

    private void OnDisable()
    {
        PlayerTarget.OnPlayerEnter -= PlayerEnters;
        PlayerTarget.OnPlayerExit -= PlayerExits;
        PlayerTarget.OnPlayerLock -= PlayerLocked;
        PlayerTarget.OnPlayerUnLock -= PlayerUnLocked;
        InteractableBehaviourSlidePath2.OnReachStart -= ElevatorEventReachStart;
        InteractableBehaviourSlidePath2.OnReachEnd -= ElevatorEventReachEnd;
        InteractableBehaviourSlidePath2.OnLeaveStart -= ElevatorEventStartMoving;
        InteractableBehaviourSlidePath2.OnLeaveEnd -= ElevatorEventStartMoving;
        PlayerController.OnPlayerDeath -= PlayerDies;
        StopAllCoroutines();
    }

    private void PlayerEnters(Transform playerTargetObject, Transform playerTransform)
    {
        if (playerTargetObject == playerTarget)
        {
            targetPlayer = playerTransform;
            playerInPosition = true;
        }
    }
    private void PlayerExits(Transform playerTargetObject, Transform playerTransform)
    {
        if (playerTargetObject == playerTarget)
        {
            playerInPosition = false;
        }
    }

    private void PlayerLocked(Transform playerTargetObject, Transform playerTransform)
    {
        if (playerTargetObject == playerTarget)
        {
            if(targetPlayer)
            {
                isPlayerLocked = true;
            }
        }
    }

    private void PlayerUnLocked(Transform playerTargetObject, Transform playerTransform)
    {
        if (playerTargetObject == playerTarget)
        {
            if (targetPlayer)
            {
                isPlayerLocked = false;
            }
        }
    }

    private void PlayerDies()
    {
        targetPlayer = null;
        isPlayerLocked = false;
        playerInPosition = false;
    }

    private void ElevatorEventReachStart(Transform interactableTransform)
    {
        if(interactableTransform == elevatorPlatform.transform)
        {
            ReachStart(transform);
        }
    }

    private void ElevatorEventReachEnd(Transform interactableTransform)
    {
        if (interactableTransform == elevatorPlatform.transform)
        {
            ReachEnd(transform);
        }
    }

    private void ElevatorEventStartMoving(Transform interactableTransform)
    {
        if (interactableTransform == elevatorPlatform.transform)
        {
            StartMoving(transform);
        }
    }

    private void ReachStart(Transform thisTransform)
    {
        interactable.gameObject.SetActive(true);
        elevatorStartCarver.gameObject.SetActive(false);

        if (OnNavMeshElevatorReachStart != null)
        {
            OnNavMeshElevatorReachStart(transform);
        }

        isAtStart = true;
        isAtEnd = false;
    }

    private void ReachEnd(Transform thisTransform)
    {
        interactable.gameObject.SetActive(true);
        elevatorEndCarver.gameObject.SetActive(false);

        if (OnNavMeshElevatorReachEnd != null)
        {
            OnNavMeshElevatorReachEnd(transform);
        }

        isAtStart = false;
        isAtEnd = true;
    }

    private void StartMoving(Transform thisTransform)
    {
        interactable.gameObject.SetActive(false);
        elevatorEndCarver.gameObject.SetActive(true);
        elevatorStartCarver.gameObject.SetActive(true);

        if (OnNavMeshElevatorStartMoving != null)
        {
            OnNavMeshElevatorStartMoving(transform);
        }

        isAtStart = false;
        isAtEnd = false;
    }

    private void Tester(Transform test)
    {

    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
