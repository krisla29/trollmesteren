﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchObject : MonoBehaviour
{
    public delegate void MatchObjectEvent(Transform thisTransform, Transform otherTransform); // Sends to MatchObjectGame
    public static event MatchObjectEvent OnMatchObjectEnter;
    public static event MatchObjectEvent OnMatchObjectExit;

    private void OnTriggerEnter(Collider other)
    {
        if(OnMatchObjectEnter != null)
        {
            OnMatchObjectEnter(transform, other.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (OnMatchObjectExit != null)
        {
            OnMatchObjectExit(transform, other.transform);
        }
    }
}
