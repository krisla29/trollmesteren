﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorShowGame : MonoBehaviour
{
    public MOG_BossDoor bossDoorGame;

    private void OnEnable()
    {
        if (!bossDoorGame.gameComplete)
        {
            if(bossDoorGame)
                bossDoorGame.gameObject.SetActive(true);
        }
    }

    private void OnDisable()
    {
        if (bossDoorGame)
            bossDoorGame.gameObject.SetActive(false);
    }
}
