﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cage : MonoBehaviour, ICharacter
{
    public GameManager gameManager;
    public GameEvent connectedEvent;
    public FriendlyCharacterController capturedCharacter;
    public Transform sceneCam;
    public Transform barsParent;
    public Transform top;
    public Transform hat;
    public SpriteRenderer hatBlob;
    public Transform hatBtnTransform;
    public Image hatBtnImage;
    public RectTransform pointerRect;
    public Image pointerImage;
    public RectTransform pointerParent;
    public AudioSource barsMove;
    public AudioSource topPop;
    public AudioSource hatMagic;
    public AudioSource hatMagic2;
    public AudioSource uiHatTic;
    float moveYdist = 3f;
    float moveXdist = -2.5f;
    private bool isComplete;

    private Vector3 startPosBars;
    private Vector3 startPosTop;
    private Quaternion startRotTop;
    private Quaternion endRotTop;
    
    private void OnEnable()
    {
        GameManager.OnAfterSwitchOffEvent += CheckEvent;
        isComplete = gameManager.playerStats.hasUnlockedHat;

        if(!isComplete)
            OpenCage();
    }

    private void OnDisable()
    {
        GameManager.OnAfterSwitchOffEvent -= CheckEvent;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void CheckEvent(GameEvent passedEvent)
    {
        if(passedEvent == connectedEvent)
        {
            print("EventStopped!");
            gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
            gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
            gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);
        }
    }

    public void OpenCage()
    {
        StartCoroutine(MoveBars());
        StartCoroutine(MoveTop());
        StartCoroutine(MoveHat());
        gameManager.player.playerMovementController.playerMovementAgent.lockAgent = true;
        gameManager.hudCanvas.exitButton.gameObject.SetActive(false);
        gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(true);
    }

    private IEnumerator MoveBars()
    {
        float t = 0;
        float animTime = 4f;
        
        startPosBars = barsParent.localPosition;

        if (barsMove)
            barsMove.Play();

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            barsParent.localPosition = Vector3.Lerp(startPosBars, startPosBars + new Vector3(0, -moveYdist, 0), t);
            yield return new WaitForEndOfFrame();
        }

        barsParent.gameObject.SetActive(false);
    }

    private IEnumerator MoveTop()
    {
        float t = 0;
        float animTime = 1f;
        
        startPosTop = top.localPosition;
        startRotTop = top.rotation;
        endRotTop = top.rotation * Quaternion.AngleAxis(180, top.forward) * Quaternion.AngleAxis(20, top.right);

        if (topPop)
            topPop.Play();

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var e = LerpUtils.EaseIn(t);
            var s = LerpUtils.SmootherStep(e);
            var moveY = Mathf.Sin(s * (Mathf.PI * 1.5f));
            moveY *= 2.3f;
            top.localPosition = Vector3.Lerp(startPosTop, new Vector3(startPosTop.x + moveXdist, startPosTop.y + moveY, startPosTop.z), e);
            top.rotation = Quaternion.Slerp(startRotTop, endRotTop, e);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator MoveHat()
    {
        yield return new WaitForSeconds(6);

        capturedCharacter.animatorTrue.SetTrigger("isThrowing");

        yield return new WaitForSeconds(1f);

        hat.gameObject.SetActive(true);

        float t = 0;
        float animTime = 6f;
        Vector3 startPos = hat.position;
        Quaternion startRot = hat.localRotation;
        Quaternion endRot = Quaternion.AngleAxis(1080f, hat.up) * Quaternion.AngleAxis(-30f, hat.right);

        if (hatMagic)
            hatMagic.Play();

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var e = LerpUtils.SmootherStep(t);
            var moveY = Mathf.Sin(t * (Mathf.PI));
            //var moveY = Mathf.Sin(t * (Mathf.PI * 2));
            hat.position = Vector3.Lerp(startPos, new Vector3(gameManager.player.transform.position.x, gameManager.player.transform.position.y + 1.7f + moveY, gameManager.player.transform.position.z) + (-gameManager.player.transform.forward * 0.375f), e);
            //hat.position = Vector3.Lerp(startPos, new Vector3(cam.transform.position.x, cam.transform.position.y + moveY, cam.transform.position.z + (cam.transform.forward.z * 2)), e);
            hat.localRotation = Quaternion.Slerp(startRot, endRot, e);
            yield return new WaitForEndOfFrame();
        }

        if (hatMagic2)
            hatMagic2.Play();

        float x = 0;
        float xTime = 1f;
        Vector3 curScale = hat.localScale;
        Vector3 headPos = hat.position;
        Color hatBlobColor = hatBlob.color;
        float alpha = hatBlobColor.a;

        while(x < 1)
        {
            x += Time.deltaTime / xTime;
            var h = LerpUtils.SmootherStep(x);
            var s = Mathf.Sin(x * Mathf.PI);
            hat.position = headPos + Vector3.up * (s * 0.25f);
            hat.localScale = curScale + Vector3.one * (s * 0.3f);
            hatBlob.color = new Color(hatBlobColor.r, hatBlobColor.g, hatBlobColor.b, alpha + (s * 0.25f));
            yield return new WaitForEndOfFrame();
        }

        float ti = 0;
        float animTime2 = 2f;
        Vector3 curPos = hat.position;
        Quaternion curRot = hat.localRotation;
        Vector3 startScale = hat.localScale;

        while(ti < 1)
        {
            ti += Time.deltaTime / animTime2;
            var h = LerpUtils.SmootherStep(ti);
            //hat.position = Vector3.Lerp(curPos, curPos + (Vector3.up * 3), h);
            //hat.position = Vector3.Lerp(curPos, sceneCam.transform.position + (sceneCam.transform.forward * 2) + (sceneCam.transform.right * 1.7f) + (sceneCam.transform.up * 0.1f), h);
            hat.position = Vector3.Lerp(curPos, hatBtnTransform.position, h);
            hat.localRotation = Quaternion.Slerp(curRot, Quaternion.AngleAxis(360, hat.right), h);
            hat.localScale = Vector3.Lerp(startScale, startScale * 0.5f, h);
            yield return new WaitForEndOfFrame();
        }
        
        float tim = 0;
        float animTime3 = 2;
        Vector3 ea = hat.localEulerAngles;
        float Yangles = 360;
        hatBtnImage.gameObject.SetActive(true);        
        Vector3 scale = hat.localScale;

        while(tim < 1)
        {
            tim += Time.deltaTime / animTime3;
            var h = LerpUtils.SmootherStep(tim);
            hat.localEulerAngles = Vector3.Lerp(ea, new Vector3(ea.x, ea.y + Yangles, ea.z), h);
            hatBtnImage.color = Color.Lerp(new Color(hatBtnImage.color.r, hatBtnImage.color.g, hatBtnImage.color.b, 0), new Color(hatBtnImage.color.r, hatBtnImage.color.g, hatBtnImage.color.b, 1), h);
            //pointerImage.color = Color.Lerp(new Color(Color.white.r, Color.white.g, Color.white.b, 0), Color.white, h);
            hat.localScale = Vector3.Lerp(scale, Vector3.zero, h);
            yield return new WaitForEndOfFrame();
        }

        if (uiHatTic)
            uiHatTic.Play();
        //gameManager.playerStats.hasUnlockedHat = true;
        pointerRect.SetParent(pointerParent);
        pointerRect.anchoredPosition = new Vector2(gameManager.hudCanvas.uiPointerBtnXpos, 0);
        pointerRect.gameObject.SetActive(true);        
        hat.gameObject.SetActive(false);
        //gameManager.playerStats.SaveGame();
    }

    public void HatDemoComplete()
    {
        if (!isComplete)
        {
            gameManager.player.playerMovementController.playerMovementAgent.lockAgent = false;
            gameManager.hudCanvas.exitButton.gameObject.SetActive(true);
            gameManager.hudCanvas.pauseInteractionIcon.gameObject.SetActive(false);
            gameManager.hudCanvas.toggleCharacterButton.interactable = true;
            pointerRect.gameObject.SetActive(false);
            hat.gameObject.SetActive(false);

            UnlockCharacter(capturedCharacter.character);
            gameManager.playerStats.hasUnlockedHat = true;           
            gameManager.playerStats.SaveGame();
        }

        isComplete = true;
        transform.gameObject.SetActive(false);  
    }

    public void SetToComplete() // Used by PlayerStats in Awake
    {
        barsParent.gameObject.SetActive(false);

        startPosTop = top.localPosition;
        startRotTop = top.rotation;
        endRotTop = top.rotation * Quaternion.AngleAxis(180, top.forward) * Quaternion.AngleAxis(20, top.right);
        var moveY = Mathf.Sin(Mathf.PI * 1.5f) * 2.3f;

        top.localPosition = new Vector3(startPosTop.x + moveXdist, startPosTop.y + moveY, startPosTop.z);
        top.rotation = endRotTop;
        hatBtnImage.gameObject.SetActive(true);
        hat.gameObject.SetActive(false);
        transform.gameObject.SetActive(false);
    }

    //---------------------------------------------------------ICHARACTER-----------------------------------------------

    public void UnlockCharacter(CharacterSelector.PlayerCharacter character)
    {
        var characterIndex = gameManager.characterSelector.GetCharacterIndex(character);

        if (!gameManager.playerStats.unlockedCharacters.Contains(characterIndex))
        {
            gameManager.playerStats.unlockedCharacters.Add(characterIndex);
            gameManager.playerStats.SaveGame();
        }
    }
}
