﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorBlocked : MonoBehaviour
{
    public MOG_BossDoor bossDoorScript;
    public AudioSource blockSound;

    private void OnMouseDown()
    {
        if (!bossDoorScript.gameManager.hudCanvas.trophyPanel.gameObject.activeSelf)
        {
            if (blockSound)
                blockSound.Play();

            bossDoorScript.StartGame();
        }
    }    
}
