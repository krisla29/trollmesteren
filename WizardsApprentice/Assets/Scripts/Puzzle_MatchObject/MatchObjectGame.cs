﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchObjectGame : MonoBehaviour
{
    public enum MatchEffects { DeactivateActive, Nothing }
    [System.Serializable]
    public class ObjectPair
    {
        public Transform activeObject;
        public Interactable activeObjectInteractable;
        public Transform passiveObject;
        public float matchRadius;
        public bool isMatching;
        public MatchEffects matchEffect;
    }

    public List<ObjectPair> objectPairs = new List<ObjectPair>();
    public bool gameComplete;

    public void CheckForMatchEnter(Transform active, Transform passive)
    {
        bool isComplete = true;

        for (int i = 0; i < objectPairs.Count; i++)
        {
            if(active == objectPairs[i].activeObject && passive == objectPairs[i].passiveObject)
            {
                objectPairs[i].isMatching = true;
                MatchEffect(objectPairs[i]);
            }
        }

        passive.gameObject.SetActive(false);

        if(isComplete)
        {
            gameComplete = true;
            GameCompleted();
        }
        else
        {
            gameComplete = false;
        }
    }

    public void CheckForMatchExit(Transform active, Transform passive)
    {
        for (int i = 0; i < objectPairs.Count; i++)
        {
            if (active == objectPairs[i].activeObject && passive == objectPairs[i].passiveObject)
            {
                objectPairs[i].isMatching = false;
            }
        }
    }

    public void SetSlotActive(Transform active, Transform passive)
    {
        passive.gameObject.SetActive(true);
    }

    public void OnEnable()
    {
        InteractableBehaviourDragToPoints.OnArriveCurrentPoint += CheckForMatchEnter;
        InteractableBehaviourDragToPoints.OnLeaveCurrentPoint += SetSlotActive;
        MatchObject.OnMatchObjectEnter += CheckForMatchEnter;
        MatchObject.OnMatchObjectExit += CheckForMatchExit;
    }

    public void OnDisable()
    {
        InteractableBehaviourDragToPoints.OnArriveCurrentPoint -= CheckForMatchEnter;
        InteractableBehaviourDragToPoints.OnLeaveCurrentPoint -= SetSlotActive;
        MatchObject.OnMatchObjectEnter -= CheckForMatchEnter;
        MatchObject.OnMatchObjectExit -= CheckForMatchExit;
    }

    public void MatchEffect(ObjectPair passedObjectPair)
    {
        switch(passedObjectPair.matchEffect)
        {
            case (MatchEffects.DeactivateActive):
                DeactivateActive(passedObjectPair);
                break;
            case (MatchEffects.Nothing):
                break;
            default:
                break;
        }
    }

    public void DeactivateActive(ObjectPair passedObjectPair)
    {
        if(passedObjectPair.activeObjectInteractable)
        {
            passedObjectPair.activeObjectInteractable.gameObject.SetActive(false);
        }        
    }

    public void GameCompleted()
    {
        
    }
       
}
