﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MOG_BossDoor : MonoBehaviour
{   
    public GameManager gameManager;
    public PlayerStats playerStats;
    public SceneController connectedScene;
    public BossDoorOpen bossDoorOpenScript;
    public Transform caveInsideLocation;
    public List<Trophy> trophys = new List<Trophy>();
    public Vector3 startPos;
    public Vector3 endPos;
    public AudioSource winGameSound;
    public AudioSource infoWhistleSound;
    public Transform blockObject;
    public SpriteRenderer keyGraphic;
    public SpriteRenderer keyGraphicGlow;
    public bool isUnlocked;

    public enum MatchEffects { DeactivateActive, Nothing }
    [System.Serializable]
    public class ObjectPair
    {
        public Transform activeObject;
        public Interactable activeObjectInteractable;
        public List<Transform> passiveObjects;
        public float matchRadius;
        public bool isMatching;
        public MatchEffects matchEffect;
    }

    public List<ObjectPair> objectPairs = new List<ObjectPair>();
    public bool gameComplete;
    public List<Transform> wakeUpOnComplete = new List<Transform>();
    //public List<Transform> caveStartEvents = new List<Transform>();
    //public List<Transform> caveCompleteEvents = new List<Transform>();
    public List<Transform> sleepOnComplete = new List<Transform>();

    //Temp:
    public Transform gameFinishText;

    public delegate void BossDoorEvent(MOG_BossDoor thisScript);
    public static event BossDoorEvent OnWinGame;

    public delegate void BossDoorMainEvent(); //Sends to PlayerStats
    public static event BossDoorMainEvent OnBossDoorEnable;
    public static event BossDoorMainEvent OnBossDoorDisable;
   
    public void StartGame()
    {
        bool hasAll = true;
        bool hasShowedTrophyPanel = false;

        for (int i = 0; i < trophys.Count; i++)
        {
            if(playerStats.trophies.Contains(trophys[i].trophy))
            {
                trophys[i].transform.parent.gameObject.SetActive(true);                    
            }
            else
            {
                trophys[i].transform.parent.gameObject.SetActive(false);

                if (!hasShowedTrophyPanel)
                {
                    blockObject.gameObject.SetActive(true);
                    //gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);
                    StartCoroutine(ShowTrophyPanel());
                    hasAll = false;
                }
            }
        }

        if (hasAll)
        {
            if (!isUnlocked)
            {
                StartCoroutine(UnlockSequence());
                isUnlocked = true;
            }
            else
            {
                blockObject.gameObject.SetActive(false);
            }
        }
    }

    void CheckForMatchEnter(Transform active, Transform passive)
    {
        bool isComplete = true;

        for (int i = 0; i < objectPairs.Count; i++)
        {
            for (int e = 0; e < objectPairs[i].passiveObjects.Count; e++)
            {
                if (active == objectPairs[i].activeObject && passive == objectPairs[i].passiveObjects[e])
                {
                    objectPairs[i].isMatching = true;
                    MatchEffect(objectPairs[i]);
                }
            }
        }

        passive.gameObject.SetActive(false);

        foreach (var item in objectPairs)
        {
            if(!item.isMatching)
            {
                isComplete = false;
            }
        }

        if(isComplete)
        {
            gameComplete = true;
            GameCompleted();
        }
        else
        {
            gameComplete = false;
        }
    }

    void CheckForMatchExit(Transform active, Transform passive)
    {
        for (int i = 0; i < objectPairs.Count; i++)
        {
            for (int e = 0; e < objectPairs[i].passiveObjects.Count; e++)
            {
                if (active == objectPairs[i].activeObject && passive == objectPairs[i].passiveObjects[e])
                {
                    objectPairs[i].isMatching = false;
                }
            }
        }
    }

    public void SetSlotActive(Transform active, Transform passive)
    {
        for (int i = 0; i < objectPairs.Count; i++)
        {
            for (int e = 0; e < objectPairs[i].passiveObjects.Count; e++)
            {
                if (active == objectPairs[i].activeObject && passive == objectPairs[i].passiveObjects[e])
                {
                    objectPairs[i].isMatching = false;
                    passive.gameObject.SetActive(true);
                }
            }
        }
    }

    private void OnEnable()
    {
        InteractableBehaviourDragToPoints.OnArriveCurrentPoint += CheckForMatchEnter;
        InteractableBehaviourDragToPoints.OnLeaveCurrentPoint += SetSlotActive;
        MatchObject.OnMatchObjectEnter += CheckForMatchEnter;
        MatchObject.OnMatchObjectExit += CheckForMatchExit;
        //CameraSceneManager.OnChangeToSceneCam += StartGame;
        StartGame();

        if (OnBossDoorEnable != null)
            OnBossDoorEnable();
    }

    private void OnDisable()
    {
        InteractableBehaviourDragToPoints.OnArriveCurrentPoint -= CheckForMatchEnter;
        InteractableBehaviourDragToPoints.OnLeaveCurrentPoint -= SetSlotActive;
        MatchObject.OnMatchObjectEnter -= CheckForMatchEnter;
        MatchObject.OnMatchObjectExit -= CheckForMatchExit;
        //CameraSceneManager.OnChangeToSceneCam -= StartGame;
        StopAllCoroutines();

        if (OnBossDoorDisable != null)
            OnBossDoorDisable();
    }

    void MatchEffect(ObjectPair passedObjectPair)
    {
        switch(passedObjectPair.matchEffect)
        {
            case (MatchEffects.DeactivateActive):
                DeactivateActive(passedObjectPair);
                break;
            case (MatchEffects.Nothing):
                break;
            default:
                break;
        }
    }

    void DeactivateActive(ObjectPair passedObjectPair)
    {
        if(passedObjectPair.activeObjectInteractable)
        {
            passedObjectPair.activeObjectInteractable.gameObject.SetActive(false);
        }

        print("Boss Door Game Complete!");
    }

    void GameCompleted()
    {
        print("Boss Door Game Complete!");
        winGameSound.Play();
        //DisplayDebugText();
        gameManager.player.anim.SetTrigger("Success");

        if (OnWinGame != null)
            OnWinGame(this);

        foreach (var item in wakeUpOnComplete)
        {
            item.gameObject.SetActive(true);
        }

        foreach (var item in sleepOnComplete)
        {
            item.gameObject.SetActive(false);
        }
        /*
        if(!playerStats.hasWonRacingGame)
        {
            foreach (var item in caveStartEvents)
            {
                item.gameObject.SetActive(true);
            }
            foreach (var item in caveCompleteEvents)
            {
                item.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (var item in caveCompleteEvents)
            {
                item.gameObject.SetActive(true);
            }
            foreach (var item in caveStartEvents)
            {
                item.gameObject.SetActive(false);
            }
        }
        */
        StartCoroutine(WinGameRoutine());
    }

    public void CheckEvents()
    {
        if (gameManager.playerStats.unlockedLocations.Contains(gameManager.worldMapController.worldMapCanvas.locations.IndexOf(caveInsideLocation)) ||
            gameManager.playerStats.hasOpenedCaveDoor)
        {
            foreach (var item in wakeUpOnComplete)
            {
                item.gameObject.SetActive(true);
            }

            foreach (var item in sleepOnComplete)
            {
                item.gameObject.SetActive(false);
            }

            gameComplete = true;
        }
        /*
        if (!playerStats.hasWonRacingGame)
        {
            foreach (var item in caveStartEvents)
            {
                item.gameObject.SetActive(true);
            }
            foreach (var item in caveCompleteEvents)
            {
                item.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (var item in caveCompleteEvents)
            {
                item.gameObject.SetActive(true);
            }
            foreach (var item in caveStartEvents)
            {
                item.gameObject.SetActive(false);
            }
        }
        */
    }

    private IEnumerator ShowTrophyPanel()
    {
        yield return new WaitForSeconds(2);

        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(true);

        if (infoWhistleSound)
        {
            if (infoWhistleSound.isPlaying)
                infoWhistleSound.Stop();

            infoWhistleSound.Play();
        } 

        yield return new WaitForSeconds(4);

        gameManager.hudCanvas.trophyPanel.gameObject.SetActive(false);
    }

    private IEnumerator UnlockSequence()
    {
        yield return new WaitForSeconds(1);

        if (winGameSound)
            winGameSound.Play();

        float t = 0;
        Color startColor = keyGraphic.color;
        Color endColor = new Vector4(startColor.r, startColor.g, startColor.b, 0);
        Color startColorGlow = new Vector4(startColor.r, startColor.g, startColor.b, 0.05f);

        while (t < 1)
        {
            t += Time.deltaTime / 1;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            keyGraphic.color = Vector4.Lerp(startColor, endColor, h);
            keyGraphicGlow.color = Vector4.Lerp(startColorGlow, endColor, h);
            yield return new WaitForEndOfFrame();
        }

        blockObject.gameObject.SetActive(false);
    }

    private IEnumerator WinGameRoutine()
    {
        yield return new WaitForSeconds(10);

        transform.gameObject.SetActive(false);
    }

    void DisplayDebugText()
    {
        gameFinishText.gameObject.SetActive(true);
    }
   
    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void CheckTrophies()
    {

    }

}
