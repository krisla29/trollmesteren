﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDoorOpen : MonoBehaviour
{
    public GameManager gameManager;
    public MOG_BossDoor mog_BossDoorScript;
    public WorldMapCanvas worldMapCanvasScript;
    public Transform caveInsideLocation;
    public Vector3 startPos;
    public Vector3 endPos;
    public bool isOpen;
    public MOG_BossDoor bossDoorScript;
    public AudioSource openDoorSound;

    private void OnEnable()
    {
        MOG_BossDoor.OnWinGame += CheckScript;

        if (!isOpen)
            CheckPlayerStats();
    }

    private void OnDisable()
    {
        MOG_BossDoor.OnWinGame -= CheckScript;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    void CheckScript(MOG_BossDoor passedScript)
    {
        if(passedScript == bossDoorScript)
        {
            StartCoroutine(OpenDoor());
        }
    }

    void CheckPlayerStats()
    {
        if (gameManager.playerStats.unlockedLocations.Contains(gameManager.worldMapController.worldMapCanvas.locations.IndexOf(caveInsideLocation)))
        {
            mog_BossDoorScript.gameComplete = true;
            //mog_BossDoorScript.SetToCompleted();
            StartCoroutine(OpenDoor());
        }
    }

    private IEnumerator OpenDoor()
    {
        yield return new WaitForSeconds(2);

        openDoorSound.Play();

        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime / 3f;
            float h = t;
            h = t * t * t * (t * (6f * t - 15f) + 10f);
            transform.position = Vector3.Lerp(startPos, endPos, h);
            yield return new WaitForEndOfFrame();
        }

        isOpen = true;
        gameManager.playerStats.hasOpenedCaveDoor = true;
        gameManager.playerStats.SaveGame();
    }
}