﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerByTag : MonoBehaviour
{
    public string tagName;
    public bool isActive;
    public delegate void TriggerByTagEvent(Transform thisTransform, Transform otherTransform);
    public static TriggerByTagEvent OnTriggerByTagEnter;
    public static TriggerByTagEvent OnTriggerByTagExit;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag(tagName))
        {
            isActive = true;

            if(OnTriggerByTagEnter != null)
            {
                OnTriggerByTagEnter(transform, other.transform);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag(tagName))
        {
            isActive = false;

            if(OnTriggerByTagExit != null)
            {
                OnTriggerByTagExit(transform, other.transform);
            }
        }
    }
}
