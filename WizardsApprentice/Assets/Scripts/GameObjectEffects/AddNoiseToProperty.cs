﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddNoiseToProperty : MonoBehaviour
{
    public enum NoiseProperties { Position, Rotation, LocalPosition, LocalRotation, LocalScale }
    public enum NoiseTypes { Add, Multiply}
    public bool updateNoise = true;
    [System.Serializable]
    public class Property
    {
        public NoiseProperties noiseProperty;
        public NoiseTypes noiseType;
        public float multiplier = 1f;
        public Vector2 noiseSpeed = new Vector2(1f, 1f);
        public Vector2 seed = Vector2.zero;
        public bool x;
        public bool y;
        public bool z;
        public bool randomSeed;
        public bool randomSpeed;
        public Vector2 randomSpeedRangeX = new Vector2(0, 2);
        public Vector2 randomSpeedRangeY = new Vector2(0, 2);
    }
    public List<Property> properties = new List<Property>();

    private float origPosX;
    private float origPosY;
    private float origPosZ;
    private float origRotX;
    private float origRotY;
    private float origRotZ;
    private float origLocPosX;
    private float origLocPosY;
    private float origLocPosZ;
    private float origLocRotX;
    private float origLocRotY;
    private float origLocRotZ;
    private float origLocScaleX;
    private float origLocScaleY;
    private float origLocScaleZ;


    void AddNoise(Property property)
    {
        switch(property.noiseProperty)
        {
            case (NoiseProperties.Position):
                var pos = transform.position;
                if (property.noiseType == NoiseTypes.Add)
                {
                    if (property.x)
                        pos.x = origPosX + GenerateNoise(pos.x, property);
                    if (property.y)
                        pos.y = origPosY + GenerateNoise(pos.y, property);
                    if (property.z)
                        pos.z = origPosZ + GenerateNoise(pos.z, property);
                }
                else
                {
                    if (property.x)
                        pos.x = origPosX * GenerateNoise(pos.x, property);
                    if (property.y)
                        pos.y = origPosY * GenerateNoise(pos.y, property);
                    if (property.z)
                        pos.z = origPosZ * GenerateNoise(pos.z, property);
                }
                transform.position = pos;
                break;
            case (NoiseProperties.Rotation):
                var rot = transform.eulerAngles;
                if (property.noiseType == NoiseTypes.Add)
                {
                    if (property.x)
                        rot.x = origRotX + GenerateNoise(rot.x, property);
                    if (property.y)
                        rot.y = origRotY + GenerateNoise(rot.y, property);
                    if (property.z)
                        rot.z = origRotZ + GenerateNoise(rot.z, property);
                }
                else
                {
                    if (property.x)
                        rot.x = origRotX * GenerateNoise(rot.x, property);
                    if (property.y)
                        rot.y = origRotY * GenerateNoise(rot.y, property);
                    if (property.z)
                        rot.z = origRotZ * GenerateNoise(rot.z, property);
                }
                transform.eulerAngles = rot;
                break;
            case (NoiseProperties.LocalPosition):
                var locPos = transform.localPosition;
                if (property.noiseType == NoiseTypes.Add)
                {
                    if (property.x)
                        locPos.x = origLocPosX + GenerateNoise(locPos.x, property);
                    if (property.y)
                        locPos.y = origLocPosY + GenerateNoise(locPos.y, property);
                    if (property.z)
                        locPos.z = origLocPosZ + GenerateNoise(locPos.z, property);
                }
                else
                {
                    if (property.x)
                        locPos.x = origLocPosX * GenerateNoise(locPos.x, property);
                    if (property.y)
                        locPos.y = origLocPosY * GenerateNoise(locPos.y, property);
                    if (property.z)
                        locPos.z = origLocPosZ * GenerateNoise(locPos.z, property);
                }
                transform.localPosition = locPos;
                break;
            case (NoiseProperties.LocalRotation):
                var locRot = transform.localEulerAngles;
                if (property.noiseType == NoiseTypes.Add)
                {
                    if (property.x)
                        locRot.x = origLocRotX + GenerateNoise(locRot.x, property);
                    if (property.y)
                        locRot.y = origLocRotY + GenerateNoise(locRot.y, property);
                    if (property.z)
                        locRot.z = origLocRotZ + GenerateNoise(locRot.z, property);
                }
                else
                {
                    if (property.x)
                        locRot.x = origLocRotX * GenerateNoise(locRot.x, property);
                    if (property.y)
                        locRot.y = origLocRotY * GenerateNoise(locRot.y, property);
                    if (property.z)
                        locRot.z = origLocRotZ * GenerateNoise(locRot.z, property);
                }
                transform.localEulerAngles = locRot;
                break;
            case (NoiseProperties.LocalScale):
                var locScale = transform.localScale;
                if (property.noiseType == NoiseTypes.Add)
                {
                    if (property.x)
                        locScale.x = origLocScaleX + GenerateNoise(locScale.x, property);
                    if (property.y)
                        locScale.y = origLocScaleY + GenerateNoise(locScale.y, property);
                    if (property.z)
                        locScale.z = origLocScaleZ + GenerateNoise(locScale.z, property);
                }
                else
                {
                    if (property.x)
                        locScale.x = origLocScaleX * GenerateNoise(locScale.x, property);
                    if (property.y)
                        locScale.y = origLocScaleY * GenerateNoise(locScale.y, property);
                    if (property.z)
                        locScale.z = origLocScaleZ * GenerateNoise(locScale.z, property);
                }
                transform.localScale = locScale;
                break;
            default:
                break;
        }
    }

    float GenerateNoise(float value, Property cProperty)
    {
        if (cProperty.seed == Vector2.zero)
        {
            return Noise(cProperty.multiplier, cProperty.noiseSpeed);                
        }
        else
        {
            return Noise(cProperty.multiplier, cProperty.noiseSpeed, cProperty.seed);
        }       
    }

    private void Start()
    {
        origPosX = transform.position.x;
        origPosY = transform.position.y;
        origPosZ = transform.position.z;
        origRotX = transform.eulerAngles.x;
        origRotY = transform.eulerAngles.y;
        origRotZ = transform.eulerAngles.z;
        origLocPosX = transform.localPosition.x;
        origLocPosY = transform.localPosition.y;
        origLocPosZ = transform.localPosition.z;
        origLocRotX = transform.localEulerAngles.x;
        origLocRotY = transform.localEulerAngles.y;
        origLocRotZ = transform.localEulerAngles.z;
        origLocScaleX = transform.localScale.x;
        origLocScaleY = transform.localScale.y;
        origLocScaleZ = transform.localScale.z;

        for (int i = 0; i < properties.Count; i++)
        {
            if(properties[i].randomSeed)
            {
                float randX = Random.Range(-100, 100);
                float randY = Random.Range(-100, 100);

                properties[i].seed = new Vector2(randX, randY);
            }

            if(properties[i].randomSpeed)
            {
                float speedX = Random.Range(properties[i].randomSpeedRangeX.x, properties[i].randomSpeedRangeX.y);
                float speedY = Random.Range(properties[i].randomSpeedRangeY.x, properties[i].randomSpeedRangeY.y);
                properties[i].noiseSpeed = new Vector2(speedX, speedY);
            }
        }

        if (!updateNoise)
        {
            for (int i = 0; i < properties.Count; i++)
            {
                AddNoise(properties[i]);
            }
        }
    }

    private void Update()
    {
        if(updateNoise)
        {
            for (int i = 0; i < properties.Count; i++)
            {
                AddNoise(properties[i]);
            }
        }
    }

    public float Noise(float multiplier, Vector2 noiseSpeed)//used in Update for noise over time
    {
        var noise = Mathf.PerlinNoise(Time.time * noiseSpeed.x/100f, Time.time * noiseSpeed.y/100f);
        noise = Mathf.Sin(noise*180) * multiplier;
        return noise;
    }
    public float Noise(float multiplier, Vector2 noiseSpeed, Vector2 seed)//used in Update for noise over time
    {
        var noise = Mathf.PerlinNoise(seed.x +(Time.time * noiseSpeed.x/100f), seed.y + (Time.time * noiseSpeed.y/100f));
        noise = Mathf.Sin(noise*180) * multiplier;
        return noise;
    }    
}
