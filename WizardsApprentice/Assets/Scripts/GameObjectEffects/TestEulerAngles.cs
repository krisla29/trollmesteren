﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestEulerAngles : MonoBehaviour
{
    public bool useLocal;

    private void Update()
    {
        if (!useLocal)
            print(transform.eulerAngles);
        else
            print(transform.localEulerAngles);
    }
}
