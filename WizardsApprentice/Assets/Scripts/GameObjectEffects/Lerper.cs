﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lerper : MonoBehaviour
{    
    public enum ObjectTypes
    {
        CanvasGroupAlpha,
        SpriteRendererColor,
        ImageColor,
        RawImageColor,
        TransformLocalPosition,
        TransformGlobalPosition,
        TransformLocalRotation,
        TransformGlobalRotation,
        TransformLocalScale,
        LightIntensity,
        RectTransform,
        SpriteRendererSize
    }
    [Header("General Settings")]
    public ObjectTypes objectType;
    public enum LerpModes { Linear, Smooth, Stepped, EaseIn, EaseOut, SmoothStep }
    public LerpModes lerpMode;
    public enum LoopModes { Repeat, PingPong, Random}
    public LoopModes loopMode;  
    public enum RepeatTypes { Infinite, FiniteCycles, FiniteDuration }
    public RepeatTypes repeatType;
    public float totalDuration = 4;
    public float lerpDuration = 2;
    public float intervalDelay = 0;
    public float startDelay = 0;
    public float stopDelay = 0;    
    public int cycles = 1;
    public float progression = 0;
    public bool play = false;
    public bool isReverse = false;
    public bool startOnActivation = true;
    public bool deactivateOnComplete = false;
    public bool resetOnDeactivated;
    [Header("Type Specific Settings")]
    public int startInteger = 0;
    public int endInteger = 1;
    public float startValue = 0;
    public float endValue = 1;
    public Vector3 startVector = Vector3.zero;
    public Vector3 endVector = Vector3.one;
    public Vector2 startXY = Vector2.zero;
    public Vector2 endXY = Vector2.one;
    public Color startColor = Color.clear;
    public Color endColor = Color.white;
    public Rect startRect = new Rect(0, 0, 1, 1);
    public Rect endRect = new Rect(0, 0, 0, 0);
    public int cycleCounter;
    private float currentProgression;
    private bool totalDurationReached;
    private enum DataTypes { Int, Float, Vector2, Vector3, Color, Rect}
    private DataTypes datatype;

    public bool useUnscaledTime;
    private float mainDeltaTime;

    //private Coroutine currentRoutine;
    private CanvasGroup canvasGroup;
    private SpriteRenderer spriteRenderer;
    private Image image;
    private RawImage rawImage;
    private new Light light;
    private RectTransform rectTransform;

    private void Awake()
    {
        CheckDataType(isReverse);
    }
    void Start()
    {

    }
    private void Update()
    {
        if (useUnscaledTime)
            mainDeltaTime = Time.unscaledDeltaTime;
        else
            mainDeltaTime = Time.deltaTime;
    }

    private void OnEnable()
    {
        if (startOnActivation)
        {
            PlayLerp();
        }
    }

    private void OnDisable()
    {
        if (resetOnDeactivated)
        {
            totalDurationReached = false;
            currentProgression = 0;
            cycleCounter = 0;
            progression = 0;
            play = false;
        }
        //{
        CancelInvoke();
        StopAllCoroutines();
        //}        
    }

    private IEnumerator Lerp()
    {
        if (cycleCounter > 0)
        {
            yield return new WaitForSeconds(intervalDelay);
        }

        float t = 0;

        while(t < 1)
        {
            var nt = LerpFunctions(t);
            UpdateLerp(nt);
            t += mainDeltaTime / lerpDuration;
            yield return new WaitForEndOfFrame();
        }

        cycleCounter += 1;
        PlaybackSwitch();
    }

    private IEnumerator LerpBack()
    {
        if (cycleCounter > 0)
        {
            yield return new WaitForSeconds(intervalDelay);
        }

        float t = 1;

        while(t > 0)
        {
            var nt = LerpFunctions(t);
            UpdateLerp(nt);
            t -= mainDeltaTime / lerpDuration;
            yield return new WaitForEndOfFrame();
        }

        cycleCounter += 1;
        PlaybackSwitch();
    }

    private void UpdateLerp(float lerpType)
    {
        switch(objectType)
        {
            case ObjectTypes.CanvasGroupAlpha:
                canvasGroup.alpha = Mathf.Lerp(startValue, endValue, lerpType);
                break;
            case ObjectTypes.SpriteRendererColor:
                spriteRenderer.color = Color.Lerp(startColor, endColor, lerpType);
                break;
            case ObjectTypes.ImageColor:
                image.color = Color.Lerp(startColor, endColor, lerpType);
                break;
            case ObjectTypes.RawImageColor:
                rawImage.color = Color.Lerp(startColor, endColor, lerpType);
                break;
            case ObjectTypes.LightIntensity:
                light.intensity = Mathf.Lerp(startValue, endValue, lerpType);
                break;
            case ObjectTypes.TransformLocalPosition:
                transform.localPosition = Vector3.Lerp(startVector, endVector, lerpType);
                break;
            case ObjectTypes.TransformGlobalPosition:
                transform.position = Vector3.Lerp(startVector, endVector, lerpType);
                break;
            case ObjectTypes.TransformLocalRotation:
                transform.localRotation = Quaternion.Slerp(Quaternion.Euler(startVector), Quaternion.Euler(endVector), lerpType);
                break;
            case ObjectTypes.TransformGlobalRotation:
                transform.rotation = Quaternion.Slerp(Quaternion.Euler(startVector), Quaternion.Euler(endVector), lerpType);
                break;
            case ObjectTypes.TransformLocalScale:
                transform.localScale = Vector3.Lerp(startVector, endVector, lerpType);
                break;
            case ObjectTypes.RectTransform:
                rectTransform.anchoredPosition = Vector2.Lerp(startRect.position, endRect.position, lerpType);
                rectTransform.sizeDelta = Vector2.Lerp(startRect.size, endRect.size, lerpType);
                break;
            case ObjectTypes.SpriteRendererSize:
                spriteRenderer.size = Vector2.Lerp(startXY, endXY, lerpType);                
                break;          
            default:
                break;
        }
    }

    private void StartLerp(bool isReverse)
    {
        switch (repeatType)
        {
            case RepeatTypes.FiniteCycles:
                if (cycleCounter <= cycles)
                {
                    if (!isReverse)
                        StartCoroutine(Lerp());
                    else
                        StartCoroutine(LerpBack());
                }
                else
                { 
                    Invoke("StopLerp", stopDelay);
                }
            break;
            case RepeatTypes.FiniteDuration:
                if (!totalDurationReached)
                {
                    if (!isReverse)
                        StartCoroutine(Lerp());
                    else
                        StartCoroutine(LerpBack());
                }
                else
                {
                    Invoke("StopLerp", stopDelay);
                }
            break;
            case RepeatTypes.Infinite:
                    if (!isReverse)
                        StartCoroutine(Lerp());
                    else
                        StartCoroutine(LerpBack());
            break;
            default:
            break;
        }
    }

    private void CheckDataType(bool reverse)
    {
        switch (objectType)
        {
            case ObjectTypes.CanvasGroupAlpha:
                canvasGroup = GetComponent<CanvasGroup>();
                canvasGroup.alpha = reverse ? endValue : startValue;
                datatype = DataTypes.Float;
                break;
            case ObjectTypes.SpriteRendererColor:
                spriteRenderer = GetComponent<SpriteRenderer>();
                spriteRenderer.color = reverse ? endColor : startColor;
                datatype = DataTypes.Color;
                break;
            case ObjectTypes.ImageColor:
                image = GetComponent<Image>();
                image.color = reverse ? endColor : startColor;
                datatype = DataTypes.Color;
                break;
            case ObjectTypes.RawImageColor:
                rawImage = GetComponent<RawImage>();
                rawImage.color = reverse ? endColor : startColor;
                datatype = DataTypes.Color;
                break;
            case ObjectTypes.LightIntensity:
                light = GetComponent<Light>();
                light.intensity = reverse ? endValue : startValue;
                datatype = DataTypes.Float;
                break;
            case ObjectTypes.TransformLocalPosition:
                transform.localPosition = reverse ? endVector : startVector;
                datatype = DataTypes.Vector3;
                break;
            case ObjectTypes.TransformGlobalPosition:
                transform.position = reverse ? endVector : startVector;
                datatype = DataTypes.Vector3;
                break;
            case ObjectTypes.TransformLocalRotation:
                transform.localRotation = Quaternion.Euler(reverse ? endVector : startVector);
                datatype = DataTypes.Vector3;
                break;
            case ObjectTypes.TransformGlobalRotation:
                transform.rotation = Quaternion.Euler(reverse ? endVector : startVector);
                datatype = DataTypes.Vector3;
                break;
            case ObjectTypes.TransformLocalScale:
                transform.localScale = reverse ? endVector : startVector;
                datatype = DataTypes.Vector3;
                break;
            case ObjectTypes.RectTransform:
                rectTransform = GetComponent<RectTransform>();
                rectTransform.anchoredPosition = reverse ? endRect.position : startRect.position;
                rectTransform.sizeDelta = reverse ? endRect.size : startRect.size;
                datatype = DataTypes.Rect;
                break;
            case ObjectTypes.SpriteRendererSize:
                spriteRenderer = GetComponent<SpriteRenderer>();
                spriteRenderer.size = reverse ? endXY : startXY;
                datatype = DataTypes.Vector2;
                break;
            default:
                break;
        }
    }

    private float LerpFunctions(float time)
    {
        switch (lerpMode)
        {
            case LerpModes.Linear:
                return time;
            case LerpModes.Smooth:
                var lerp = Mathf.Lerp(0.0f, 1.0f, time);
                var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
                return smoothLerp;
            case LerpModes.EaseIn:
                float f = time;
                f = 1 - Mathf.Cos(f * Mathf.PI * 0.5f);
                return f;
            case LerpModes.EaseOut:
                float g = time;
                g = Mathf.Sin(g * Mathf.PI * 0.5f);
                return g;
            case LerpModes.SmoothStep:
                float h = time;
                h = time * time * time * (time * (6f * time - 15f) + 10f);
                return h;
            case LerpModes.Stepped:
                float i = time;
                i = Mathf.Round(time * 10) / 10;
                return i;
            default:
                return time;
        }
    }

    void PlaybackSwitch()
    {
        switch (loopMode)
        {
            case LoopModes.Repeat:
                StartLerp(false);
                break;
            case LoopModes.PingPong:
                if (cycleCounter % 2 == 0)
                {
                    StartLerp(false);
                }
                else
                    StartLerp(true);
                break;
            case LoopModes.Random:
                int rand = Random.Range(0, 2);
                if (rand == 0)
                {
                    StartLerp(false);
                }
                else
                    StartLerp(true);
                break;           
            default:
                break;
        }
    }

    void CheckDeactivation()
    {
        if (deactivateOnComplete)
            transform.gameObject.SetActive(false);
    }
    
    private void TimerReached()
    {
        totalDurationReached = true;
        CheckDeactivation();
    }

    private void PlayLerp()
    {
        Invoke("TimerReached", totalDuration);
        currentProgression = progression;
        cycleCounter = 0;
        play = true;
        Invoke("PlaybackSwitch", startDelay);
    }

    private bool StopLerp()
    {
        cycleCounter = 0;
        currentProgression = progression;
        play = false;
        return true;
    }
    
    private void OnDestroy()
    {
        //if (StopLerp())
        //{
        CancelInvoke();
        StopAllCoroutines();
        //}
    }
}
