﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatAfterSeconds : MonoBehaviour
{
    public bool deActivate;
    public float time;
    public List<Transform> objectsToActivate = new List<Transform>();

    private void Start()
    {
        StartCoroutine(CountDown());
    }
    
    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator CountDown()
    {
        yield return new WaitForSeconds(time);

        Activation();
    }

    void Activation()
    {
        if (!deActivate)
        {
            foreach (var item in objectsToActivate)
            {
                item.gameObject.SetActive(true);
            }
        }
        else
        {
            foreach (var item in objectsToActivate)
            {
                item.gameObject.SetActive(false);
            }
        }
    }
}
