﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicMove : MonoBehaviour
{
    public float moveSpeed = 5;
    public Vector3 direction = Vector3.up;
    public float randomOffsetSpeed = 0;
    public Vector3 randomOffsetDirection = Vector3.zero;
    public Vector2 noiseSpeed = Vector2.zero;
    public Vector2 noiseDirX = Vector2.zero;
    public Vector2 noiseDirY = Vector2.zero;
    public Vector2 noiseDirZ = Vector2.zero;

    private float randomSpeed;
    private float randomX;
    private float randomY;
    private float randomZ;
    private float tempSpeed;
    private Vector3 tempDirection;

    private void Start()
    {
        tempSpeed = moveSpeed + Random.Range(-randomOffsetSpeed, randomOffsetSpeed);

        if (noiseSpeed != Vector2.zero)
        {
            randomSpeed = Random.Range(noiseSpeed.x, noiseSpeed.y);
        }

        var rand = randomOffsetDirection;
        direction += new Vector3(Random.Range(-rand.x, rand.x), Random.Range(-rand.y, rand.y), Random.Range(-rand.z, rand.z));

        if(noiseDirX != Vector2.zero)
        {
            randomX = Random.Range(noiseDirX.x, noiseDirX.y);
        }
        if (noiseDirY != Vector2.zero)
        {
            randomY = Random.Range(noiseDirY.x, noiseDirY.y);
        }
        if (noiseDirZ != Vector2.zero)
        {
            randomZ = Random.Range(noiseDirZ.x, noiseDirZ.y);
        }
    }

    void Update()
    {       
        tempDirection = direction;

        if(noiseSpeed != Vector2.zero)
        {            
            tempSpeed = Mathf.PerlinNoise(randomSpeed, Time.time);
        }
        if(noiseDirX != Vector2.zero)
        {
            tempDirection.x = Mathf.PerlinNoise(randomX, Time.time);
        }
        else
        {
            tempDirection.x = direction.x;
        }
        if (noiseDirY != Vector2.zero)
        {
            tempDirection.y = Mathf.PerlinNoise(randomY, Time.time);
        }
        else
        {
            tempDirection.y = direction.y;
        }
        if (noiseDirZ != Vector2.zero)
        {
            tempDirection.z = Mathf.PerlinNoise(randomZ, Time.time);
        }
        else
        {
            tempDirection.z = direction.z;
        }

        direction = tempDirection;
        var nSpeed = tempSpeed * moveSpeed;

        transform.Translate(direction * Time.deltaTime * nSpeed, Space.World);
    }
}
