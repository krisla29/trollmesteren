﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicLineRenderer2Points : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public Transform pointA;
    public Transform pointB;
    private Vector3 pointAB;
    
    private void FixedUpdate()
    {
        pointAB = pointA.position + (pointB.position - pointA.position) / 2;

        Vector3[] positions = new Vector3[3];

        positions[0] = pointA.position;
        positions[1] = pointAB;
        positions[2] = pointB.position;

        lineRenderer.SetPositions(positions);
    }    
}
