﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject : MonoBehaviour
{
    public Transform target;
    void Update()
    {
        if (target)
        {
            transform.LookAt(target, Vector3.up);
        }
    }
}
