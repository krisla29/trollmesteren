﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyBlendShapeAnimation : MonoBehaviour
{
    public List<SkinnedMeshRenderer> skinnedMeshesFrom = new List<SkinnedMeshRenderer>();
    public List<SkinnedMeshRenderer> skinnedMeshesTo = new List<SkinnedMeshRenderer>();

    public bool copyAtStart;
    public bool copyAtUpdate;

    private void Start()
    {
        if (copyAtStart)
            CopyWeights();
    }

    private void Update()
    {
        if (copyAtUpdate)
            CopyWeights();
    }

    void CopyWeights()
    {
        for (int i = 0; i < skinnedMeshesFrom.Count; i++)
        {
            for (int e = 0; e < skinnedMeshesFrom[i].sharedMesh.blendShapeCount; e++)
            {
                if(skinnedMeshesTo.Count > i)
                {
                    if(skinnedMeshesTo[i].sharedMesh.blendShapeCount > e)
                    {
                        skinnedMeshesTo[i].SetBlendShapeWeight(e, skinnedMeshesFrom[i].GetBlendShapeWeight(e));
                    }
                }
            }
        }
    }    
}
