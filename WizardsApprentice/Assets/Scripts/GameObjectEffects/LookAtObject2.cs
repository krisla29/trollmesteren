﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtObject2 : MonoBehaviour
{
    public GameManager gameManager;
    public Transform objectToLookAt;
    public bool lookAtPlayer;
    public bool localRotation;
    public bool lockX;
    public bool lockY;
    public bool lockZ;
    public bool invert;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        if(lookAtPlayer)
        {
            if (gameManager.player)
                objectToLookAt = gameManager.player.transform;
        }

        if (objectToLookAt)
        {
            var oldRot = transform.eulerAngles;
            var dir = objectToLookAt.transform.position - transform.position;
            var lookRot = Quaternion.LookRotation(dir, Vector3.up);

            var vectorRot = lookRot.eulerAngles;

            if (localRotation)
                oldRot = transform.localEulerAngles;

            if (lockX)
                vectorRot.x = oldRot.x;
            if (lockY)
                vectorRot.y = oldRot.y;
            if (lockZ)
                vectorRot.z = oldRot.z;
            
            if (localRotation)
            {
                transform.localEulerAngles = vectorRot;

                if (invert)
                    transform.localRotation = Quaternion.Inverse(transform.localRotation);
            }
            else
            {
                transform.eulerAngles = vectorRot;

                if (invert)
                    transform.rotation = Quaternion.Inverse(transform.rotation);
            }
        }
    }
}
