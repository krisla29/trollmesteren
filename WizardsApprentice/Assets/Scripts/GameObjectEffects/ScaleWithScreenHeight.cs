﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleWithScreenHeight : MonoBehaviour
{
    public GameManager gameManager;
    public float modifier = 1f;
    public bool useCustomStartScale;
    public Vector3 startScale;
    public bool disableScaling;
    
    private void Awake()
    {
        if(!useCustomStartScale)
            startScale = transform.localScale;
    }
    
    private void LateUpdate()
    {
        if (!disableScaling)
            transform.localScale = startScale * ((gameManager.currentScreenHeight / gameManager.targetScreenHeight) * modifier);
        else
        {
            if(transform.localScale != startScale)
                transform.localScale = startScale;
        }
    }
}
