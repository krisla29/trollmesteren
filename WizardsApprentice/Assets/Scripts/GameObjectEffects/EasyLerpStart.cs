﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using MEC;
//using Sirenix.OdinInspector;

public class EasyLerpStart : MonoBehaviour
{
    public enum SimpleLerpTarget
    {
        CanvasGroupAlpha,
        SpriteRendererColor,
        ImageColor,
        RawImageColor,
        TransformLocalPosition,
        TransformGlobalPosition,
        TransformLocalRotation,
        TransformGlobalRotation,
        TransformLocalScale,
        LightIntensity,
        RectTransform,
    }

    [Header("General")]
    public SimpleLerpTarget lerpTargetType;
    public LerpUtils.LerpType lerpTimeType = LerpUtils.LerpType.Original;
    public float preDelay = 0f;
    public float lerpDuration = 0.25f;
    //public bool useIsActiveOnStart = true;
    //public bool isActiveOnStart = false;
    //public bool toggleConstantly = false;

    [Header("Float value")]
    [SerializeField] public float onValue = 1;
    [SerializeField] public float offValue = 0;

    [Header("Color value")]
    [SerializeField] public Color onColor = Color.white;
    [SerializeField] public Color offColor = Color.clear;

    [Header("Vector3 value")]
    [SerializeField] public Vector3 onVector = Vector3.one;
    [SerializeField] public Vector3 offVector = Vector3.zero;

    [Header("RectTransfom values")]
    [SerializeField] public Rect onRect = new Rect(0, 0, 1, 1);
    [SerializeField] public Rect offRect = new Rect(0, 0, 0, 0);

    public bool IsOn { get; private set; }

    private Coroutine currentRoutine;
    private CanvasGroup canvasGroup;
    private SpriteRenderer spriteRenderer;
    private Image image;
    private RawImage rawImage;
    private new Light light;
    private RectTransform rectTransform;

    public Color GetOnColor => onColor;
    public Color GetOffColor => offColor;
    public Color SetOnColor(Color color) => onColor = color;
    public Color SetOffColor(Color color) => offColor = color;

    /*
    [Button] public void TurnOn() => LerpToState(true);
    [Button] public void TurnOff() => LerpToState(false);
    [Button] public void Toggle() => LerpToState(!IsOn);
    */
    private void Awake()
    {
        //SetStateInstantly(isActiveOnStart);
    }

    private void Start()
    {
        //SetStateInstantly(true);
        currentRoutine = StartCoroutine(DoLerpToState(false));
        //if (currentRoutine != null) return;
        //if (!toggleConstantly) return;
        //LerpToState(!IsOn);
    }

    public void SetStateInstantly(bool isOn)
    {
        IsOn = isOn;

        switch (lerpTargetType)
        {
            case SimpleLerpTarget.CanvasGroupAlpha:
                canvasGroup = GetComponent<CanvasGroup>();
                canvasGroup.alpha = IsOn ? onValue : offValue;
                break;
            case SimpleLerpTarget.SpriteRendererColor:
                spriteRenderer = GetComponent<SpriteRenderer>();
                spriteRenderer.color = IsOn ? onColor : offColor;
                break;
            case SimpleLerpTarget.ImageColor:
                image = GetComponent<Image>();
                image.color = IsOn ? onColor : offColor;
                break;
            case SimpleLerpTarget.RawImageColor:
                rawImage = GetComponent<RawImage>();
                rawImage.color = IsOn ? onColor : offColor;
                break;
            case SimpleLerpTarget.LightIntensity:
                light = GetComponent<Light>();
                light.intensity = IsOn ? onValue : offValue;
                break;
            case SimpleLerpTarget.TransformLocalPosition:
                transform.localPosition = IsOn ? onVector : offVector;
                break;
            case SimpleLerpTarget.TransformGlobalPosition:
                transform.position = IsOn ? onVector : offVector;
                break;
            case SimpleLerpTarget.TransformLocalRotation:
                transform.localRotation = Quaternion.Euler(IsOn ? onVector : offVector);
                break;
            case SimpleLerpTarget.TransformGlobalRotation:
                transform.rotation = Quaternion.Euler(IsOn ? onVector : offVector);
                break;
            case SimpleLerpTarget.TransformLocalScale:
                transform.localScale = IsOn ? onVector : offVector;
                break;
            case SimpleLerpTarget.RectTransform:
                rectTransform.anchoredPosition = IsOn ? onRect.position : offRect.position;
                rectTransform.sizeDelta = IsOn ? onRect.size : offRect.size;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void LerpToState(bool isOn)
    {
        //if (currentRoutine != null) StopCoroutine(currentRoutine);
        currentRoutine = StartCoroutine(DoLerpToState(isOn));
    }

    private IEnumerator DoLerpToState(bool isOn)
    {
        IsOn = isOn;

        switch (lerpTargetType)
        {
            case SimpleLerpTarget.CanvasGroupAlpha:
                yield return DelayedUpdate(UpdateCanvasGroupAlpha());
                break;
            case SimpleLerpTarget.SpriteRendererColor:
                yield return DelayedUpdate(UpdateSpriteRendererColor());
                break;
            case SimpleLerpTarget.ImageColor:
                yield return DelayedUpdate(UpdateImageColor());
                break;
            case SimpleLerpTarget.RawImageColor:
                yield return DelayedUpdate(UpdateRawImageColor());
                break;
            case SimpleLerpTarget.LightIntensity:
                yield return DelayedUpdate(UpdateLightIntensity());
                break;
            case SimpleLerpTarget.TransformLocalPosition:
            case SimpleLerpTarget.TransformGlobalPosition:
            case SimpleLerpTarget.TransformLocalRotation:
            case SimpleLerpTarget.TransformGlobalRotation:
            case SimpleLerpTarget.TransformLocalScale:
                yield return DelayedUpdate(UpdateTransform());
                break;
            case SimpleLerpTarget.RectTransform:
                yield return DelayedUpdate(UpdateRectTransform());
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        currentRoutine = null;
    }

    private IEnumerator DelayedUpdate(IEnumerator updateMethod)
    {
        yield return new WaitForSeconds(preDelay);
        yield return updateMethod;
    }

    private IEnumerator UpdateCanvasGroupAlpha()
    {
        if (canvasGroup == null)
            canvasGroup = GetComponent<CanvasGroup>();

        var t = 0f;
        var start = canvasGroup.alpha;
        var target = IsOn ? onValue : offValue;

        while (t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            canvasGroup.alpha = Mathf.Lerp(start, target, perc);
            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        canvasGroup.alpha = target;
    }

    private IEnumerator UpdateSpriteRendererColor()
    {
        if (spriteRenderer == null)
            spriteRenderer = GetComponent<SpriteRenderer>();

        var t = 0f;
        var start = spriteRenderer.color;
        var target = IsOn ? onColor : offColor;

        while (t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            spriteRenderer.color = Color.Lerp(start, target, perc);
            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        spriteRenderer.color = target;
    }

    private IEnumerator UpdateImageColor()
    {
        if (image == null)
            image = GetComponent<Image>();

        var t = 0f;
        var start = image.color;
        var target = IsOn ? onColor : offColor;

        while (t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            image.color = Color.Lerp(start, target, perc);
            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        image.color = target;
    }

    private IEnumerator UpdateRawImageColor()
    {
        if (rawImage == null)
            rawImage = GetComponent<RawImage>();

        var t = 0f;
        var start = rawImage.color;
        var target = IsOn ? onColor : offColor;

        while (t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            rawImage.color = Color.Lerp(start, target, perc);
            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        rawImage.color = target;
    }

    private IEnumerator UpdateLightIntensity()
    {
        if (light == null)
            light = GetComponent<Light>();

        var t = 0f;
        var start = light.intensity;
        var target = IsOn ? onValue : offValue;

        while (t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            light.intensity = Mathf.Lerp(start, target, perc);
            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        light.intensity = target;
    }

    private IEnumerator UpdateRectTransform()
    {
        if (rectTransform == null)
            rectTransform = GetComponent<RectTransform>();

        var t = 0f;
        var start = new Rect(rectTransform.anchoredPosition, rectTransform.sizeDelta); //rectTransform.rect;
        var target = IsOn ? onRect : offRect;

        while (t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            rectTransform.anchoredPosition = Vector2.Lerp(start.position, target.position, perc);
            rectTransform.sizeDelta = Vector2.Lerp(start.size, target.size, perc);
            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        rectTransform.anchoredPosition = target.position;
        rectTransform.sizeDelta = target.size;
    }


    private IEnumerator UpdateTransform()
    {
        var isRot = lerpTargetType == SimpleLerpTarget.TransformGlobalRotation ||
                    lerpTargetType == SimpleLerpTarget.TransformLocalRotation;

        var t = 0f;
        var start = IsOn ? offVector : onVector;
        var target = IsOn ? onVector : offVector;

        while (gameObject.activeInHierarchy && t <= 1)
        {
            var perc = LerpUtils.GetLerpType(lerpTimeType, t);
            var value = Vector3.zero;
            var qValue = Quaternion.identity;

            if (isRot)
                qValue = Quaternion.Slerp(Quaternion.Euler(start), Quaternion.Euler(target), perc);
            else
                value = Vector3.Lerp(start, target, perc);

            switch (lerpTargetType)
            {
                case SimpleLerpTarget.TransformLocalPosition:
                    transform.localPosition = value; break;
                case SimpleLerpTarget.TransformGlobalPosition:
                    transform.position = value; break;
                case SimpleLerpTarget.TransformLocalRotation:
                    transform.localRotation = qValue; break;
                case SimpleLerpTarget.TransformGlobalRotation:
                    transform.rotation = qValue; break;
                case SimpleLerpTarget.TransformLocalScale:
                    transform.localScale = value; break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            t += lerpDuration <= 0 ? 1 : Time.deltaTime / lerpDuration;
            yield return null;
        }

        switch (lerpTargetType)
        {
            case SimpleLerpTarget.TransformLocalPosition:
                transform.localPosition = target; break;
            case SimpleLerpTarget.TransformGlobalPosition:
                transform.position = target; break;
            case SimpleLerpTarget.TransformLocalRotation:
                transform.localRotation = Quaternion.Euler(target); break;
            case SimpleLerpTarget.TransformGlobalRotation:
                transform.rotation = Quaternion.Euler(target); break;
            case SimpleLerpTarget.TransformLocalScale:
                transform.localScale = target; break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        //Timing.KillCoroutines(currentRoutine);
    }
}
