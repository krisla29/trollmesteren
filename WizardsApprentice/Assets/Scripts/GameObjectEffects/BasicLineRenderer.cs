﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicLineRenderer : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public List<Transform> points = new List<Transform>();

    private void FixedUpdate()
    {
        SetPoints();
    }

    private void SetPoints()
    {        
        for (int i = 0; i < points.Count; i++)
        {
            lineRenderer.SetPosition(i, points[i].position);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;

        for (int i = 0; i < points.Count; i++)
        {
            if (i < points.Count - 2)
                Gizmos.DrawLine(points[i].position, points[i + 1].position);
        }
    }
}
