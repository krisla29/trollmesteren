﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableChildrenAtStart : MonoBehaviour
{    
    void Start()
    {
        foreach (Transform child in transform)
        {
            transform.gameObject.SetActive(false);
        }
    }    
}
