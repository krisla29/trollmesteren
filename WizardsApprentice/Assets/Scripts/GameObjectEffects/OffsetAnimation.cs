﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetAnimation : MonoBehaviour
{
    public Animator anim;

    private void Start()
    {
        float randNum = Random.Range(0.0f, 1.0f);
        print(randNum);

        anim.Play(0, 0, randNum);
    }
}
