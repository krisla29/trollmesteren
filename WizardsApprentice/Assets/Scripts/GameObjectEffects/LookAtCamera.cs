﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    void Update()
    {
        if (Camera.main)
        {
            Transform cam = Camera.main.transform;
            transform.LookAt(cam, Vector3.up);
            transform.forward = -transform.forward;
        }
    }
}
