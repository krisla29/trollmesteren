﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdoptiveChild : MonoBehaviour
{
    public GameObject parent;
    public GameObject child;

    public Vector3 localPosition = Vector3.one;
    public Vector3 locPosOffset = Vector3.zero;
    public Vector3 localRotation = Vector3.one;
    public Vector3 locRotOffset = Vector3.zero;
    public Vector3 globalPosition = Vector3.one;
    public Vector3 globPosOffset = Vector3.zero;
    public Vector3 globalRotation = Vector3.one;
    public Vector3 globRotOffset = Vector3.zero;
    public Vector3 localScale = Vector3.one;
    public Vector3 locScaleOffset = Vector3.zero;

    private Vector3 tempLocPos;
    private Vector3 tempLocRot;
    private Vector3 tempGlobPos;
    private Vector3 tempGlobRot;
    private Vector3 tempLocScale;

    private void Update()
    {       
        if (localPosition != Vector3.zero)
        {            
            child.transform.localPosition = ParentVectorMembers(localPosition, child.transform.localPosition, parent.transform.localPosition, tempLocPos, locPosOffset);
            //print(child.transform.localPosition);
        }
        if (localRotation != Vector3.zero)
        {
            child.transform.localEulerAngles = ParentVectorMembers(localRotation, child.transform.localEulerAngles, parent.transform.localEulerAngles, tempLocRot, locRotOffset);
        }

        if (globalPosition != Vector3.zero)
        {
            child.transform.position = ParentVectorMembers(globalPosition, child.transform.position, parent.transform.position, tempGlobPos, globPosOffset);
        }
        if (globalRotation != Vector3.zero)
        {
            child.transform.eulerAngles = ParentVectorMembers(globalRotation, child.transform.eulerAngles, parent.transform.eulerAngles, tempGlobRot, globRotOffset);
        }
        if (localScale != Vector3.zero)
        {
            child.transform.localScale = ParentVectorMembers(localScale, child.transform.localScale, parent.transform.localScale, tempLocScale, locScaleOffset);
        }
    }

    Vector3 ParentVectorMembers(Vector3 testVector, Vector3 childVector, Vector3 parentVector, Vector3 tempVector, Vector3 offset)
    {
        for (int i = 0; i < 3; i++)
        {
            if (testVector[i] == 1)
            {               
                tempVector[i] = parentVector[i];                
            }
            else
            {
                tempVector[i] = childVector[i];
            }
        }

        return tempVector + offset;
        //print(childVector);
    }
}
