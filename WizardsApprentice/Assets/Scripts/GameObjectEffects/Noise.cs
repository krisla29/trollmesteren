﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{

    public static float AddNoise(float value, float multiplier, Vector2 noiseSpeed)//used in Update for noise over time
    {
        var noise = Mathf.PerlinNoise(Time.time * noiseSpeed.x, Time.time * noiseSpeed.y);
        return value + (noise * multiplier);
    }
    public static float AddNoise(float value, float multiplier, Vector2 noiseSpeed, Vector2 seed)//used in Update for noise over time
    {
        var noise = Mathf.PerlinNoise(seed.x + (Time.time * noiseSpeed.x), seed.y + (Time.time * noiseSpeed.y));
        return value + (noise * multiplier);
    }
    public static float MultiplyNoise(float value, float multiplier, Vector2 noiseSpeed)//used in Update for noise over time
    {
        var noise = Mathf.PerlinNoise(Time.time * noiseSpeed.x, Time.time * noiseSpeed.y);
        return value * (noise * multiplier);
    }
    public static float MultiplyNoise(float value, float multiplier, Vector2 noiseSpeed, Vector2 seed)//used in Update for noise over time
    {
        var noise = Mathf.PerlinNoise(seed.x + (Time.time * noiseSpeed.x), seed.y + (Time.time * noiseSpeed.y));
        return value * (noise * multiplier);
    }
}
