﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpToMaterialColor : MonoBehaviour
{
    public Color newColor;
    public LerpUtils.LerpType lerpType;
    public float lerpTime;
    public float lerpDelay;
    private Color oldColor;
    private Material material;

    void Start()
    {
        material = transform.GetComponent<MeshRenderer>().material;
        oldColor = material.color;
        StartCoroutine("Delay", lerpDelay);
    }
    private IEnumerator ChangeColor()
    {
        float t = 0;

        while (t <= lerpTime)
        {
            var lerp = LerpUtils.GetLerpType(lerpType, t);
            material.color = Color.Lerp(oldColor, newColor, lerp);
            t += lerpTime <= 0 ? 1 : Time.deltaTime / lerpTime;

            yield return new WaitForEndOfFrame();
        }
    }
    private IEnumerator Delay(float time)
    {
        yield return new WaitForSeconds(time);
        StartCoroutine("ChangeColor");
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }

}
