﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyScalePulse : MonoBehaviour
{
    public float amplitude;
    public float speed;
    public float waitForSeconds;
    public Vector3 scaleAxis = Vector3.one;

    private Vector3 scale;
    private Vector3 startScale;
    private float t;

    public bool useUnscaledTime;
    private float time;

    private void Start()
    {
        startScale = transform.localScale;
        t = Time.time;
    }
    void Update()   
    {
        if (useUnscaledTime)
            time = Time.unscaledTime;
        else
            time = Time.time;

        if (time - t > waitForSeconds)
        {
            for (int i = 0; i < 3; ++i)
            {
                //scale[i] = amplitude * Mathf.Sin(Time.time*speed) + 1;
                if (scaleAxis[i] == 1)
                {
                    scale[i] = amplitude * Mathf.Sin((time - waitForSeconds) * speed) + startScale[i];
                    transform.localScale = scale;
                }
                else
                {
                    scale[i] = startScale[i];
                }
            }
        }
    }
}
