﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateAfterTime : MonoBehaviour
{
    public float time = 5;

    private void OnEnable()
    {
        StartCoroutine(DeactiveSelf(time));
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private IEnumerator DeactiveSelf(float inputTime)
    {
        yield return new WaitForSeconds(inputTime);

        transform.gameObject.SetActive(false);
    }
}
