﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyRotate: MonoBehaviour
{
    public bool useLocalRotation;
    public Vector3 rotationAxis;
    public float rotationMultiplier = 1;

    private void Update()
    {
        transform.Rotate(rotationAxis * rotationMultiplier * Time.deltaTime, useLocalRotation ? Space.Self : Space.World);
    }
}
