﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class FixedScale : MonoBehaviour
{
    public float FixeScale = 1;
    private GameObject parent;

    void Start()
    {
        parent = transform.parent.gameObject;
        transform.localScale = new Vector3(FixeScale / parent.transform.localScale.x, FixeScale / parent.transform.localScale.y, FixeScale / parent.transform.localScale.z);
    }
}