﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookaAtCamera2 : MonoBehaviour
{
    public bool localRotation;
    public bool lockX;
    public bool lockY;
    public bool lockZ;
    public bool invert;

    void Update()
    {
        if(Camera.main)
        {
            var oldRot = transform.eulerAngles;
            var lookRot = Quaternion.LookRotation(-Camera.main.transform.forward, Vector3.up);
            
            var vectorRot = lookRot.eulerAngles;

            if (localRotation)
                oldRot = transform.localEulerAngles;

            if (lockX)
                vectorRot.x = oldRot.x;
            if (lockY)
                vectorRot.y = oldRot.y;
            if (lockZ)
                vectorRot.z = oldRot.z;
            /*
            for (int i = 0; i < 3; i++)
            {
                if (vectorRot[i] < -179)
                    vectorRot[i] += 180;
                if (vectorRot[i] > 179)
                    vectorRot[i] -= 180;
            }
            */
            if (localRotation)
            {
                transform.localEulerAngles = vectorRot;

                if (invert)
                    transform.localRotation = Quaternion.Inverse(transform.localRotation);
            }
            else
            {
                transform.eulerAngles = vectorRot;

                if (invert)
                    transform.rotation = Quaternion.Inverse(transform.rotation);
            }
        }
    }
}
