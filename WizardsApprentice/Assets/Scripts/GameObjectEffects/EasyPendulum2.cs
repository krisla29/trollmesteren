﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyPendulum2: MonoBehaviour
{
    public Vector3 direction;
    public float amplitude;
    public float speed;
    public float delay;
    public bool useLocalPosition;

    private Vector3 startPos;
    private Vector3 pendulumPos;
    private float t;

    private void Start()
    {
        startPos = transform.localPosition;
        t = Time.time;
    }
    void Update()
    {
        if (Time.time - t > delay)
        {
            if (!useLocalPosition)
            {
                for (int i = 0; i < 3; ++i)
                {
                    //scale[i] = amplitude * Mathf.Sin(Time.time*speed) + 1;
                    pendulumPos[i] = direction[i] * amplitude * Mathf.Sin((Time.time - delay) * speed);
                    transform.localPosition = startPos + pendulumPos;
                }
            }
            else
            {
                for (int i = 0; i < 3; ++i)
                {
                    //scale[i] = amplitude * Mathf.Sin(Time.time*speed) + 1;
                    pendulumPos[i] = transform.TransformDirection(direction)[i] * amplitude * Mathf.Sin((Time.time - delay) * speed);
                    transform.localPosition = startPos + pendulumPos;
                }
            }
        }
    }
}
