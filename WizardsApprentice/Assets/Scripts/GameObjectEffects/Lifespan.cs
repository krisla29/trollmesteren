﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lifespan : MonoBehaviour
{
    public float lifeSpan;
    public bool useUnscaledTime;
    private Coroutine routine;

    private void Start()
    {
        routine = StartCoroutine(WaitAndKill());
    }

    private IEnumerator WaitAndKill()
    {
        if (!useUnscaledTime)
            yield return new WaitForSeconds(lifeSpan);
        else
            yield return new WaitForSecondsRealtime(lifeSpan);

        Destroy(transform.gameObject);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        routine = null;
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
        routine = null;
    }
}
