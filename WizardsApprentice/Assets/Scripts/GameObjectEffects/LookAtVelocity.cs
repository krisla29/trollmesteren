﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtVelocity : MonoBehaviour
{
    public Rigidbody rigidBody;

    private void Update()
    {
        transform.rotation = Quaternion.LookRotation(rigidBody.velocity);
    }
}
