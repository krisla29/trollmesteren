﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAnimation : MonoBehaviour
{
    public enum PlaybackModes { Linear, LinearReverse, PingPong, PingPongReverse}
    public PlaybackModes playbackMode;
    public Animator basicAnimator;
    public string animatorState;
    public float animationTime = 1f;
    public float pauseTime = 0;
    public bool infiniteLoops;
    public float loops;
    public float startDelay;


    public bool play;
    public float startProgress;

    public float currentProgress;
    public float currentCounter;

    private void OnEnable()
    {
        currentProgress = startProgress;
        currentCounter = 0;
        Invoke("PlayAnimation", startDelay);
    }

    void SwitchPlayback()
    {
        switch(playbackMode)
        {
            case PlaybackModes.Linear:
                PlayAnimation();
                break;
            case PlaybackModes.LinearReverse:
                PlayAnimationReverse();
                break;
            case PlaybackModes.PingPong:
                if (currentCounter % 2 == 0)
                    PlayAnimation();
                else
                    PlayAnimationReverse();
                break;
            case PlaybackModes.PingPongReverse:
                if (currentCounter % 2 == 0)
                    PlayAnimationReverse();
                else
                    PlayAnimation();
                break;
            default:
                break;
        }
    }

    private void PlayAnimation()
    {
        if (!infiniteLoops)
        {
            if (currentCounter <= loops)
            {
                currentProgress = 0;
                basicAnimator.SetFloat("normalizedTime", 0);
                basicAnimator.Play(animatorState, -1, 0);
                StartCoroutine(Animation());
            }
            else
            {
                StopAnimation();
            }
        }
        else
        {
            currentProgress = 0;
            basicAnimator.SetFloat("normalizedTime", 0);
            basicAnimator.Play(animatorState, -1, 0);
            StartCoroutine(Animation());
        }
    }

    private void PlayAnimationReverse()
    {
        if (!infiniteLoops)
        {
            if (currentCounter <= loops)
            {
                currentProgress = 1;
                basicAnimator.SetFloat("normalizedTime", 1);
                basicAnimator.Play(animatorState, -1, 1);
                StartCoroutine(AnimationReverse());
            }
            else
            {
                StopAnimation();
            }
        }
        else
        {
            currentProgress = 1;
            basicAnimator.SetFloat("normalizedTime", 1);
            basicAnimator.Play(animatorState, -1, 1);
            StartCoroutine(AnimationReverse());
        }
    }

    private IEnumerator Animation()
    {
        play = true;

        float t = 0;

        while (t < 1)
        {
            basicAnimator.SetFloat("normalizedTime", 0);
            t += Time.deltaTime / pauseTime;
            yield return new WaitForEndOfFrame();
        }
        
        while (currentProgress < 1)
        {            
            basicAnimator.SetFloat("normalizedTime", currentProgress);            
            currentProgress += Mathf.Clamp(Time.deltaTime / animationTime,0 ,1);
            basicAnimator.enabled = true;
            yield return new WaitForEndOfFrame();
        }

        basicAnimator.enabled = false;

        basicAnimator.SetFloat("normalizedTime", 1);

        currentCounter += 1;

        SwitchPlayback();
    }

    private IEnumerator AnimationReverse()
    {
        play = true;

        float t = 0;

        while (t < 1)
        {
            basicAnimator.SetFloat("normalizedTime", 1);
            t += Time.deltaTime / pauseTime;
            yield return new WaitForEndOfFrame();
        }

        while (currentProgress > 0)
        {            
            basicAnimator.SetFloat("normalizedTime", currentProgress);           
            currentProgress -= Mathf.Clamp(Time.deltaTime / animationTime,0 ,1);
            basicAnimator.enabled = true;
            yield return new WaitForEndOfFrame();
        }

        basicAnimator.enabled = false;

        basicAnimator.SetFloat("normalizedTime", 0);

        currentCounter += 1;

        SwitchPlayback();
    }
    
    private void StopAnimation()
    {
        play = false;
        currentProgress = 0;
        currentCounter = 0;
    }

    private void OnDisable()
    {
        StopAnimation();
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAnimation();
        StopAllCoroutines();
    }
}
