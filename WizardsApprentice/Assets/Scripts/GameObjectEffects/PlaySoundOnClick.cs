﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnClick : MonoBehaviour
{
    public AudioSource sound;

    private void OnMouseDown()
    {
        if (sound)
            sound.Play();
    }
}
