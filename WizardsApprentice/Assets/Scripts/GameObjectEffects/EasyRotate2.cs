﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyRotate2: MonoBehaviour
{
    public bool useLocalRotation;
    public Vector3 rotationAxis;
    public float rotationMultiplier = 1;
    public float stepSize = 1;
    public float flipAngle = 30;
    public enum RotationTypes { Linear, Stepped, PingPongLinear, PingPongSmooth, PingPongStepped, PingPongSmoothStepped}
    public RotationTypes rotationType;
    public bool flip;
    private Vector3 oldRot;

    private void Update()
    {
        switch(rotationType)
        {
            case (RotationTypes.Linear):
                transform.Rotate(rotationAxis * rotationMultiplier * Time.deltaTime, useLocalRotation ? Space.Self : Space.World);
                break;
            case (RotationTypes.Stepped):
                transform.Rotate(rotationAxis * rotationMultiplier * Time.deltaTime, useLocalRotation ? Space.Self : Space.World);
                Stepper();
                break;
            case (RotationTypes.PingPongLinear):
                transform.Rotate(rotationAxis * rotationMultiplier * Time.deltaTime, useLocalRotation ? Space.Self : Space.World);
                Flipper();
                break;
            case (RotationTypes.PingPongSmooth):                
                SmoothRot();
                Flipper();
                break;
            case (RotationTypes.PingPongStepped):
                transform.Rotate(rotationAxis * rotationMultiplier * Time.deltaTime, useLocalRotation ? Space.Self : Space.World);
                Stepper();
                Flipper();
                break;
            case (RotationTypes.PingPongSmoothStepped):
                SmoothRot();
                Stepper();
                Flipper();
                break;
            default:
                transform.Rotate(rotationAxis * rotationMultiplier * Time.deltaTime, useLocalRotation ? Space.Self : Space.World);
                break;
        }
    }

    void Stepper()
    {
        if (!useLocalRotation)
            transform.eulerAngles = (transform.eulerAngles / stepSize) * stepSize;
        else
            transform.localEulerAngles = (transform.eulerAngles / stepSize) * stepSize;
    }

    void Flipper()
    {
        var rot = transform.eulerAngles;

        if (useLocalRotation)
            rot = transform.localEulerAngles;

        bool lastCheck = flip;

        for (int i = 0; i < 3; i++)
        {
            if (rot[i] > flipAngle || rot[i] < -flipAngle)
            {
                flip = !flip;
            }
        }
        
        if (lastCheck != flip)
            rotationMultiplier = rotationMultiplier * -1;            
    }

    void SmoothRot()
    {
        var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * 100f);
        var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

        var rot = Vector3.Lerp(oldRot, oldRot + (rotationAxis * rotationMultiplier * Time.deltaTime), smoothLerp);       

        if (!useLocalRotation)
        {
            transform.eulerAngles = rot;
            oldRot = rot;
        }
        else
        {
            transform.localEulerAngles = rot;
            oldRot = rot;
        }
    }
}
