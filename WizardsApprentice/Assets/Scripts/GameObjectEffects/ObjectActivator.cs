﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActivator : MonoBehaviour
{
    public bool DeactivateOnComplete;
    public bool deactivateWithActivator;
    public bool activateWhenActivatorDisable;

    [System.Serializable]
    public struct ObjectsActivation
    {
        public Transform objectToActivate;
        public float activationTime;
    }

    public List<ObjectsActivation> objectsToActivate = new List<ObjectsActivation>();

    [System.Serializable]
    public struct ObjectsDectivation
    {
        public Transform objectToDeactivate;
        public float deactivationTime;
    }

    public List<ObjectsDectivation> objectsToDeactivate = new List<ObjectsDectivation>();

    private int totalCounter;

    private void OnEnable()
    {
        if(objectsToActivate.Count > 0)
            ActivateObjects();
        if (objectsToDeactivate.Count > 0)
            DeactivateObjects();
    }

    private void ActivateObjects()
    {
        if (objectsToActivate.Count > 0)
        {
            for (int i = 0; i < objectsToActivate.Count; i++)
            {
                if (objectsToActivate[i].objectToActivate)
                    StartCoroutine(TimedUpdate(objectsToActivate[i].objectToActivate, objectsToActivate[i].activationTime, true));
            }
        }
    }

    private void DeactivateObjects()
    {
        if (objectsToDeactivate.Count > 0)
        {
            for (int i = 0; i < objectsToDeactivate.Count; i++)
            {
                if(objectsToDeactivate[i].objectToDeactivate)
                    StartCoroutine(TimedUpdate(objectsToDeactivate[i].objectToDeactivate, objectsToDeactivate[i].deactivationTime, false));
            }
        }
    }

    private IEnumerator TimedUpdate(Transform passedObject, float time, bool activation)
    {
        yield return new WaitForSeconds(time);
        print("activation " + activation);
        totalCounter += 1;

        if(passedObject)
        {
            if(passedObject.gameObject)
                passedObject.gameObject.SetActive(activation);
        }


        if(totalCounter >= objectsToActivate.Count + objectsToDeactivate.Count)
        {
            if(DeactivateOnComplete)
            {
                transform.gameObject.SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();

        if (deactivateWithActivator)
        {
            foreach(var item in objectsToActivate)
            {
                if(null != item.objectToActivate)
                    item.objectToActivate.gameObject.SetActive(false);
            }
        }
        if (activateWhenActivatorDisable)
        {
            foreach (var item in objectsToDeactivate)
            {
                if (null != item.objectToDeactivate)
                    item.objectToDeactivate.gameObject.SetActive(true);
            }
        }       
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
