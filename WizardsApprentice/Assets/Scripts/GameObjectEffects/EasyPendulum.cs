﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyPendulum: MonoBehaviour
{
    public Vector3 direction;
    public float amplitude;
    public float speed;
    public float delay;

    private Vector3 startPos;
    private Vector3 pendulumPos;
    private float t;

    private void Start()
    {
        startPos = transform.localPosition;
        t = Time.time;
    }
    void Update()
    {
        if (Time.time - t > delay)
        {
            for (int i = 0; i < 3; ++i)
            {
                //scale[i] = amplitude * Mathf.Sin(Time.time*speed) + 1;
                pendulumPos[i] = direction[i] * amplitude * Mathf.Sin((Time.time - delay) * speed);
                transform.localPosition = startPos + pendulumPos;
            }
        }
    }
}
