﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{
    public Transform target;
    public Transform follower;
    public bool targetIsPlayer;
    
    public enum FollowProperties { LocalPosition, Position, LocalRotation, Rotation, LocalScale, LookAt, None}
    [System.Serializable]
    public class FollowProperty
    {
        public FollowProperties followProperty;
        public float speed;
        public bool smooth;
        public bool maintainOffset;
    }

    public FollowProperty followProperty01;
    public FollowProperty followProperty02;
    public FollowProperty followProperty03;
  
    private Vector3 followerStartLocalPosition;
    private Vector3 followerStartPosition;
    private Quaternion followerStartLocalRotation;
    private Quaternion followerStartRotation;
    private Vector3 followerStartLocalScale;

    private Vector3 targetStartLocalPosition;
    private Vector3 targetStartPosition;
    private Quaternion targetStartLocalRotation;
    private Quaternion targetStartRotation;
    private Vector3 targetStartLocalScale;
    /*
    private void Start()
    {
        followerStartLocalPosition = follower.localPosition;
        followerStartPosition = follower.position;
        followerStartLocalRotation = follower.localRotation;
        followerStartRotation = follower.rotation;
        followerStartLocalScale = follower.localScale;
    }
    */
    private void OnEnable()
    {
        if (follower)
        {
            followerStartLocalPosition = follower.localPosition;
            followerStartPosition = follower.position;
            followerStartLocalRotation = follower.localRotation;
            followerStartRotation = follower.rotation;
            followerStartLocalScale = follower.localScale;
        }

        if (target)
        {
            targetStartLocalPosition = target.localPosition;
            targetStartPosition = target.position;
            targetStartLocalRotation = target.localRotation;
            targetStartRotation = target.rotation;
            targetStartLocalScale = target.localScale;
        }

    SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;
    }

    private void OnDisable()
    {
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
    }

    private void Update()
    {
        if (target)
        {
            Switcher(followProperty01);
            Switcher(followProperty02);
            Switcher(followProperty03);
        }
        else
        {
            Initializer();
        }
    }

    void Switcher(FollowProperty followProperty)
    {
        switch (followProperty.followProperty)
        {
            case FollowProperties.LocalPosition:
                follower.localPosition = FollowVector(followProperty, follower.localPosition, target.localPosition, followerStartLocalPosition, targetStartLocalPosition);
                break;
            case FollowProperties.Position:
                follower.position = FollowVector(followProperty, follower.position, target.position, followerStartPosition, targetStartPosition);
                break;
            case FollowProperties.LocalRotation:
                follower.localRotation = FollowQuaternion(followProperty, follower.localRotation, target.localRotation, followerStartLocalRotation, targetStartLocalRotation);
                break;
            case FollowProperties.Rotation:
                follower.rotation = FollowQuaternion(followProperty, follower.rotation, target.rotation, followerStartRotation, targetStartRotation);
                break;
            case FollowProperties.LocalScale:
                follower.localScale = FollowVector(followProperty, follower.localScale, target.localScale, followerStartLocalScale, targetStartLocalScale);
                break;
            case FollowProperties.LookAt:
                FollowLooker(followProperty);
                break;
            case FollowProperties.None:
                break;
            default:
                break;
        }
    }

    Vector3 FollowVector(FollowProperty followP, Vector3 followerProperty, Vector3 targetProperty, Vector3 followerOffset, Vector3 targetOffset)
    {
        if (followP.smooth)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * followP.speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            if(!followP.maintainOffset)             
                return Vector3.Lerp(followerProperty, targetProperty, smoothLerp);
            else
                return Vector3.Lerp(followerProperty, targetProperty - (targetOffset - followerOffset), smoothLerp);
        }
        else
        {
            if (!followP.maintainOffset)
                return Vector3.Lerp(followerProperty, targetProperty, Time.deltaTime * followP.speed);
            else
                return Vector3.Lerp(followerProperty, targetProperty - (targetOffset - followerOffset), Time.deltaTime * followP.speed);
        }
        
    }

    Quaternion FollowQuaternion(FollowProperty followP, Quaternion followerProperty, Quaternion targetProperty, Quaternion followerOffset, Quaternion targetOffset)
    {
        if (followP.smooth)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * followP.speed);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);
            
            if (!followP.maintainOffset)
                return Quaternion.Slerp(followerProperty, targetProperty, smoothLerp);
            else
                return Quaternion.Slerp(followerProperty, targetProperty * followerOffset, smoothLerp);
        }
        else
        {            
            if (!followP.maintainOffset)
                return Quaternion.Slerp(followerProperty, targetProperty, Time.deltaTime * followP.speed);
            else
                return Quaternion.Slerp(followerProperty, targetProperty * followerOffset, Time.deltaTime * followP.speed);
        }

        
    }

    void FollowLooker(FollowProperty followP)
    {
        var lerp = Mathf.Lerp(0.0f, 1.0f, Time.deltaTime * followP.speed);
        var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

        if (followP.smooth)
        {
            //var rot = Quaternion.FromToRotation(follower.forward, (target.position - follower.position));
            //var newRot = Quaternion.RotateTowards(follower.rotation, rot, smoothLerp);
            var newRot = Quaternion.Slerp(follower.rotation, Quaternion.LookRotation(target.position - follower.position), smoothLerp);

            follower.rotation = newRot;
        }
        else
        {
            //var rot = Quaternion.FromToRotation(follower.forward, (target.position - follower.position));
            //var newRot = Quaternion.RotateTowards(follower.rotation, rot, lerp);
            var newRot = Quaternion.Slerp(follower.rotation, Quaternion.LookRotation(target.position - follower.position), lerp);

            follower.rotation = newRot;
        }
    }

    private void ReferenceNewPlayer(Transform playerTransform)
    {
        if (targetIsPlayer)
        {
            target = playerTransform;
            targetStartLocalPosition = target.localPosition;
            targetStartPosition = target.position;
            targetStartLocalRotation = target.localRotation;
            targetStartRotation = target.rotation;
            targetStartLocalScale = target.localScale;
        }
    }

    private void Initializer()
    {
        if (targetIsPlayer)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }       
        
        if (target)
        {
            targetStartLocalPosition = target.localPosition;
            targetStartPosition = target.position;
            targetStartLocalRotation = target.localRotation;
            targetStartRotation = target.rotation;
            targetStartLocalScale = target.localScale;
        }       
    }
}
