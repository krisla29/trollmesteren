﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleActivator : MonoBehaviour
{
    public List<Transform> objectsToActivate = new List<Transform>();
    public bool activateOthers;

    private void OnEnable()
    {
        foreach (var item in objectsToActivate)
        {
            item.gameObject.SetActive(activateOthers);
        }
    }

    private void OnDisable()
    {
        foreach (var item in objectsToActivate)
        {
            item.gameObject.SetActive(!activateOthers);
        }
    }
}
