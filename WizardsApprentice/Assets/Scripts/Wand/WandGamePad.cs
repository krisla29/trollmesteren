﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandGamePad : Wand
{
    void Start()
    {
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        magicPointer.SetActive(false);
        lineRenderer.gameObject.SetActive(false);
        wandMode = WandModes.Mode.Normal;
        magicPointer.transform.parent = null;
        lineRenderer.transform.parent = null;
    }
    void Update()
    {
        RunMode();
    }
    void RunMode()
    {
        if (isActive && player)
        {
            if (player.toggleAim)
            {
                if (!isPickingUp)
                {
                    CastRay();

                    if (player.isFiring)
                    {
                        FireRay();
                    }
                    else
                    {
                        ReleaseFire();
                    }
                }
                else
                {
                    ReleasePickup();
                }
            }
            else
            {
                if (!isReset)
                {
                    ResetVariables();
                }
            }
        }
        if (isActive && !player)
        {
            wandMode = WandModes.Mode.Normal;
            player = transform.root.GetComponent<PlayerController>();
            playerCam = player.playerCam.GetComponent<CameraMovementController>();
        }

        if (isReset)
        {
            isReset = false;
        }
    }

    void CastRay()
    {
        if (Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out playerCam.hit, 100f))  //IS CASTING RAY AND HIT SOMETHING
        {
            isDetecting = true;
        }
        else
        {
            isDetecting = false;
        }
    }

    void FireRay()
    {
        lineRenderer.gameObject.SetActive(true);

        lineRenderer.positionCount = 2;

        magicPointer.SetActive(true);
        magicPointer.transform.position = playerCam.hit.point;

        if (isDetecting)
        {
            if (playerCam.hit.transform.gameObject.CompareTag("WandInteractable"))
            {
                if (!currentInteractable)
                {
                    currentInteractable = playerCam.hit.transform.GetComponent<WandInteractable>();
                }

                if (currentInteractable.wandInteractionMode == WandModes.Mode.Normal)
                {
                    wandNormal.NormalHit();
                    lineRenderer.material = normalMaterial;
                    magicPointerMesh.material = pointerNormalMaterial;
                    pSystemRenderer.material = pointerNormalMaterial;
                }
                if (currentInteractable.wandInteractionMode == WandModes.Mode.Pickup)
                {
                    if (pickupStartTime == 0 && !isPickingUp)
                    {
                        pickupStartTime = Time.time;
                    }

                    wandPickup.PickupHit();

                    if (Time.time - pickupStartTime >= pickupCoolDown)
                    {
                        pickUpClickCounter = 0;
                        isPickingUp = true;
                        pickupStartTime = 0;
                        print("Resetting pickup counter, counter is " + pickUpClickCounter);
                    }
                }
            }
        }

        if (isDetecting)
        {
            positions = new Vector3[] { wandHotSpot.position, playerCam.hit.point };
            lineRenderer.SetPositions(positions);

            Debug.DrawLine(playerCam.transform.position, playerCam.hit.point, Color.red);
        }
        else
        {
            positions = new Vector3[] { wandHotSpot.position, (playerCam.transform.position + (playerCam.transform.forward * 50)),
                                                            (playerCam.transform.position + (playerCam.transform.forward * 100)) };

            lineRenderer.SetPositions(positions);
        }
    }

    void ReleaseFire()
    {
        if (!isReset && !isPickingUp)
        {
            //print("Release Fire");
            ResetVariables();
        }
    }

    void ReleasePickup()
    {
        if (isPickingUp)
        {
            //int currentPickUpCounter
            if (pickUpClickCounter == 0)
            {
                int newNumber = inputManager.actionClickCounter;
                pickUpClickCounter = newNumber;
                print("Initializing pickupCounter, counter is " + newNumber);
            }

            if (!inputManager.isTouch)
            {
                if (inputManager.ActionDown())
                {
                    isPickingUp = false;
                }
            }
            else
            {
                if (pickUpClickCounter <= 1)
                {
                    if (inputManager.actionClickCounter >= pickUpClickCounter + 1)
                    {
                        isPickingUp = false;
                        print(pickUpClickCounter);
                        pickUpClickCounter = 0;
                    }
                }
                else
                {
                    if (inputManager.actionClickCounter > pickUpClickCounter + 1)
                    {
                        isPickingUp = false;
                        print(pickUpClickCounter);
                        pickUpClickCounter = 0;
                    }
                }
            }

            wandPickup.PickupHold();
            lineRenderer.material = pickupMaterial;
            magicPointerMesh.material = pointerPickupMaterial;
            pSystemRenderer.material = pointerPickupMaterial;
            lineRenderer.gameObject.SetActive(true);
        }
    }

    void ResetVariables()
    {
        isHitting = false;
        isPickingUp = false;
        pickupStartTime = 0;
        pickUpClickCounter = 0;

        if (wandAudioBeam)
        {
            wandAudioBeam.pitch = 1;
        }

        if (lineRenderer.gameObject.activeSelf)
        {
            lineRenderer.gameObject.SetActive(false);
        }
        if (magicPointer.activeSelf)
        {
            magicPointer.SetActive(false);
        }

        if (currentInteractable)
        {
            if (oldInteractableParent)
            {
                currentInteractable.transform.parent = oldInteractableParent;
            }
            else
            {
                currentInteractable.transform.parent = null;
            }

            if (currentInteractable.rigidBody)
            {
                currentInteractable.rigidBody.isKinematic = currentInteractable.isKinematic;
                currentInteractable.rigidBody.useGravity = currentInteractable.useGravity;
            }

            currentInteractable.isInteracting = false;
            currentInteractable = null;
        }

        wandMode = WandModes.Mode.Normal;
        lineRenderer.material = normalMaterial;
        magicPointerMesh.material = pointerNormalMaterial;
        pSystemRenderer.material = pointerNormalMaterial;

        //print("isReset");

        isReset = true;
    }
}
