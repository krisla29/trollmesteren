﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wand : MonoBehaviour
{
    public bool canDestroy = true;
    public bool canPickup = true;
    public bool isDetecting;
    public bool isPickingUp;
    //public InteractablesManager interactablesManager;
    public Transform closestInteractable;
    public Transform closestEnemy;
    //public GameObject aimPrefab;
    public Transform aimSpriteObject;
    //public float searchDistance = 30;

    public Transform wandHotSpot;
    public LineRenderer lineRenderer;
    public Vector3[] positions;

    public InputManager inputManager;
    public GameObject magicPointer;
    public MeshRenderer magicPointerMesh;
    public ParticleSystemRenderer pSystemRenderer;
    public bool isHitting;
    public bool isActive;
    public PlayerController player;
    public CameraMovementController playerCam;

    public WandInteractable currentInteractable;
    public Transform oldInteractableParent;
    public Rigidbody oldInteractableRigidbody;
    //[HideInInspector]
    public bool oldRbIsKinematic;
    //[HideInInspector]
    public bool oldRbUseGravity;
    //[HideInInspector]
    public bool isReset;

    public WandModes.Mode wandMode;
    public WandBehaviourNormal wandNormal;
    public WandBehaviourPickup wandPickup;
    public WandBehaviourNormalTPA wandNormalTPA;
    public WandBehaviourPickupTPA wandPickupTPA;
    public Material normalMaterial;
    public Material pickupMaterial;
    public Material pointerNormalMaterial;
    public Material pointerPickupMaterial;
    public Color normalColor;
    public Color pickupColor;
    public AudioSource wandAudioBeam;

    public float pickupStartTime;
    public float pickupCoolDown = 1f;
    public int pickUpClickCounter;

    public delegate void WandEvents(Transform transfom);
    public static event WandEvents OnWandInteractableStart;
    public static event WandEvents OnWandInteractableStop;

    public void Start()
    {
        inputManager = GameObject.FindGameObjectWithTag("InputManager").GetComponent<InputManager>();
        //interactablesManager = GameObject.FindGameObjectWithTag("InteractablesManager").GetComponent<InteractablesManager>();
        magicPointer.SetActive(false);
        lineRenderer.gameObject.SetActive(false);
        wandMode = WandModes.Mode.Normal;
        magicPointer.transform.parent = null;
        lineRenderer.transform.parent = null;
    }
    public void Update()
    {
        if (isActive && player)
        {
            if (player.toggleAim)
            {
                RunMode();
            }

            if(player.toggleAimThirdPerson)
            {
                RunModeTPA();
            }
            else
            {
                ResetAimObject();
            }
        }

        if (isActive && !player)
        {
            wandMode = WandModes.Mode.Normal;
            player = transform.root.GetComponent<PlayerController>();
            playerCam = player.playerCam.GetComponent<CameraMovementController>();
        }
    }
    public void RunMode()
    {
        if (player.toggleAim)
        {               
            if (!isPickingUp)
            {
                CastRay();

                if (player.isFiring)
                {
                    FireRay();
                }
                else
                {
                    ReleaseFire();
                }
            }
            else
            {
                ReleasePickup();
            }
        }
        else
        {
            if (!isReset)
            {
                ResetVariables();
            }               
        }
               
        if(isReset)
        {
            isReset = false;
        }       
    }

    public void RunModeTPA()
    {
        if (player.toggleAimThirdPerson)
        {           
            if (!isPickingUp)
            {
                CheckTargets();
                ShowTargets();

                if (player.isFiring)
                {
                    FireAtClosest();
                }
                else
                {
                    ReleaseFire();
                }
            }
            else
            {
                ReleasePickup();
            }
        }
        else
        {
            if (!isReset)
            {
                ResetVariables();
                ResetAimObject();
            }
        }

        if (isReset)
        {
            isReset = false;
        }
    }

    public void CastRay()
    {
        if (Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out playerCam.hit, 100f))  //IS CASTING RAY AND HIT SOMETHING
        {           
            isDetecting = true;
        }
        else
        {
            isDetecting = false;
        }             
    }

    public void ShowTargets()
    {
        if(!aimSpriteObject)
        {

        }

        if (closestEnemy != null)
        {
            aimSpriteObject.gameObject.SetActive(true);
            aimSpriteObject.SetParent(null);
            aimSpriteObject.position = Vector3.Lerp(aimSpriteObject.position, closestEnemy.position, Time.deltaTime * 10f);
        }
        else if (closestEnemy == null && closestInteractable != null)
        {
            aimSpriteObject.gameObject.SetActive(true);
            aimSpriteObject.SetParent(null);
            aimSpriteObject.position = Vector3.Lerp(aimSpriteObject.position, closestInteractable.position, Time.deltaTime * 10f);
        }

        else if (closestEnemy == null && closestInteractable == null)
        {
            ResetAimObject();
        }
    }

    public void CheckTargets()
    {
        closestEnemy = SearchForWandInteractables(player.wandInteractableDetector.enemies);
        closestInteractable = SearchForWandInteractables(player.wandInteractableDetector.wandInteractables);
              
        if(closestEnemy != null || closestInteractable != null)
        {
            isDetecting = true;
        }
        else
        {
            isDetecting = false;
        }
    }

    public void FireRay()
    {
        lineRenderer.gameObject.SetActive(true);

        lineRenderer.positionCount = 2;

        magicPointer.SetActive(true);
        magicPointer.transform.position = playerCam.hit.point;

        if (isDetecting)
        {
            Transform tempWandInteractable = null;

            foreach (Transform item in playerCam.hit.transform)
            {
                if(item.CompareTag("WandInteractable"))
                {
                    tempWandInteractable = item;
                }
            }

            if (tempWandInteractable != null)
            {
                if (!currentInteractable)
                {
                    //currentInteractable = playerCam.hit.transform.GetComponent<WandInteractable>();
                    currentInteractable = tempWandInteractable.GetComponent<WandInteractable>();
                    print(currentInteractable.transform);

                    if (OnWandInteractableStart != null)
                    {
                        OnWandInteractableStart(currentInteractable.transform);                        
                    }
                    
                }

                if (currentInteractable.wandInteractionMode == WandModes.Mode.Normal)
                {
                    wandNormal.NormalHit();
                    lineRenderer.material = normalMaterial;
                    magicPointerMesh.material = pointerNormalMaterial;
                    pSystemRenderer.material = pointerNormalMaterial;
                }
                if (currentInteractable.wandInteractionMode == WandModes.Mode.Pickup)
                {
                    if (pickupStartTime == 0 && !isPickingUp)
                    {
                        pickupStartTime = Time.time;
                    }

                    wandPickup.PickupHit();

                    if (Time.time - pickupStartTime >= pickupCoolDown)
                    {
                        pickUpClickCounter = 0;
                        isPickingUp = true;
                        pickupStartTime = 0;
                        print("Resetting pickup counter, counter is " + pickUpClickCounter);
                    }
                }
            }
        }

        if (isDetecting)
        {
            positions = new Vector3[] { wandHotSpot.position, playerCam.hit.point };
            lineRenderer.SetPositions(positions);

            Debug.DrawLine(playerCam.transform.position, playerCam.hit.point, Color.red);
        }
        else
        {
            positions = new Vector3[] { wandHotSpot.position, (playerCam.transform.position + (playerCam.transform.forward * 50)),
                                                            (playerCam.transform.position + (playerCam.transform.forward * 100)) };

            lineRenderer.SetPositions(positions);
        }
    }

    public void FireAtClosest()
    {
        lineRenderer.gameObject.SetActive(true);

        lineRenderer.positionCount = 2;

        magicPointer.SetActive(true);

        if (isDetecting)
        {
            if (closestEnemy != null)
            {
                magicPointer.transform.position = closestEnemy.position;

                CheckMode(closestEnemy);
            }
            else if (closestEnemy == null && closestInteractable != null)
            {
                magicPointer.transform.position = closestInteractable.position;

                CheckMode(closestInteractable);
            }
            else if (closestEnemy == null && closestInteractable == null)
            {
                magicPointer.transform.position = player.transform.position + (player.transform.forward * 50f);
            }
        }
        if (isDetecting)
        {
            positions = new Vector3[] { wandHotSpot.position, currentInteractable.transform.position };
            lineRenderer.SetPositions(positions);

            Debug.DrawLine(wandHotSpot.position, currentInteractable.transform.position, Color.red);
        }
        else
        {
            positions = new Vector3[] { wandHotSpot.position, (wandHotSpot.position + (player.transform.forward * 25f)),
                                                            (wandHotSpot.position + (playerCam.transform.forward * 50)) };

            lineRenderer.SetPositions(positions);
        }

    }

    public void CheckMode(Transform wandInteractable)
    {
        if (!currentInteractable)
        {
            currentInteractable = wandInteractable.GetComponent<WandInteractable>();
            print(currentInteractable.transform);

            if (OnWandInteractableStart != null)
            {
                OnWandInteractableStart(currentInteractable.transform);
            }
            
        }
        else
        {
            if (currentInteractable.wandInteractionMode == WandModes.Mode.Normal)
            {
                wandNormalTPA.NormalHit();
                lineRenderer.material = normalMaterial;
                magicPointerMesh.material = pointerNormalMaterial;
                pSystemRenderer.material = pointerNormalMaterial;
            }
            if (currentInteractable.wandInteractionMode == WandModes.Mode.Pickup)
            {
                if (pickupStartTime == 0 && !isPickingUp)
                {
                    pickupStartTime = Time.time;
                }

                wandPickupTPA.PickupHit();

                if (Time.time - pickupStartTime >= pickupCoolDown)
                {
                    pickUpClickCounter = 0;
                    isPickingUp = true;
                    pickupStartTime = 0;
                    print("Resetting pickup counter, counter is " + pickUpClickCounter);
                }
            }
        }
    }

    public void ReleaseFire()
    {        
        if (!isReset && !isPickingUp)
        {
            ResetVariables();
        }
    }

    public void ReleasePickup()
    {
        if (isPickingUp)
        {
            //int currentPickUpCounter
            if(pickUpClickCounter == 0)
            {
                int newNumber = inputManager.actionClickCounter;                
                pickUpClickCounter = newNumber;
                print("Initializing pickupCounter, counter is " + newNumber);
            }

            if(!inputManager.isTouch)
            {
                if (inputManager.ActionDown())
                {
                    isPickingUp = false;
                }
            }
            else
            {
                if(pickUpClickCounter <= 1)
                {
                    if (inputManager.actionClickCounter >= pickUpClickCounter + 1)
                    {
                        isPickingUp = false;
                        print(pickUpClickCounter);
                        pickUpClickCounter = 0;
                    }
                }
                else
                {
                    if (inputManager.actionClickCounter > pickUpClickCounter + 1)
                    {
                        isPickingUp = false;
                        print(pickUpClickCounter);
                        pickUpClickCounter = 0;
                    }
                }                
            }

            if(player.toggleAim)
            {
                wandPickup.PickupHold();
            }

            if(player.toggleAimThirdPerson)
            {
                wandPickupTPA.PickupHold();
                ResetAimObject();
            }

            lineRenderer.material = pickupMaterial;
            magicPointerMesh.material = pointerPickupMaterial;
            pSystemRenderer.material = pointerPickupMaterial;
            lineRenderer.gameObject.SetActive(true);
        }
    }

    public void ResetAimObject()
    {
        aimSpriteObject.SetParent(transform);
        aimSpriteObject.gameObject.SetActive(false);
        aimSpriteObject.position = Vector3.zero;
    }

    public void ResetVariables()
    {
        isHitting = false;
        isPickingUp = false;
        pickupStartTime = 0;
        pickUpClickCounter = 0;
        /*
        if(aimSpriteObject.gameObject.activeSelf)
        {
            aimSpriteObject.gameObject.SetActive(false);
        }
        */
        if (wandAudioBeam)
        {
            wandAudioBeam.pitch = 1;
        }

        if (lineRenderer.gameObject.activeSelf)
        {
            lineRenderer.gameObject.SetActive(false);
        }
        if (magicPointer.activeSelf)
        {
            magicPointer.SetActive(false);
        }

        if (currentInteractable)
        {           
            if(oldInteractableParent)
            {
                currentInteractable.mainObject.transform.parent = oldInteractableParent;
            }
            else
            {
                currentInteractable.mainObject.transform.parent = null;
            }

            if(currentInteractable.rigidBody)
            {
                currentInteractable.rigidBody.isKinematic = currentInteractable.isKinematic;
                currentInteractable.rigidBody.useGravity = currentInteractable.useGravity;
            }

            currentInteractable.isInteracting = false;

            if (OnWandInteractableStop != null)
            {
                OnWandInteractableStop(currentInteractable.transform);
                print(currentInteractable.transform);
            }

            currentInteractable = null;
            oldInteractableRigidbody = null;
            oldInteractableParent = null;
            oldInteractableParent = null;
        }

        wandMode = WandModes.Mode.Normal;
        lineRenderer.material = normalMaterial;
        magicPointerMesh.material = pointerNormalMaterial;
        pSystemRenderer.material = pointerNormalMaterial;

        //print("isReset");

        isReset = true;
    }

    public Transform SearchForWandInteractables(List<Transform> objectList)
    {
        Transform closest = null;
        //float distance = Mathf.Infinity;
        float distance = player.detectionDistance * 10f;
        Vector3 position = player.transform.position;

        foreach (var item in objectList)
        {
            if (item != null)
            {
                Vector3 diff = item.position - position;
                float curDistance = diff.sqrMagnitude;
                if (curDistance < distance)
                {
                    closest = item;
                    distance = curDistance;
                }
            }
        }

        return closest;
    }

    private void OnDestroy()
    {
        if (lineRenderer)
        {
            Destroy(lineRenderer.gameObject);
        }
        if (magicPointer)
        {
            Destroy(magicPointer);
        }
    }

    //////////////////////////// NOT IN USE ////////////////////////////////////


    /*
    public GameObject FindClosestInteractable()                 /// Do not use, too heavy!!
    {
        GameObject[] gos;
        gos = GameObject.FindGameObjectsWithTag("WandInteractable");
        GameObject closest = null;
        float distance = Mathf.Infinity;
        Vector3 position = transform.position;
        foreach (GameObject go in gos)
        {
            Vector3 diff = go.transform.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        return closest;
    }

    public Transform SearchForEnemies()
    {
        Transform closest = null;
        float distance = searchDistance;
        Vector3 position = transform.position;

        foreach (var item in interactablesManager.enemies)
        {
            Vector3 diff = item.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = item;
                distance = curDistance;
            }
        }

        return closest;
    }

    public Transform SearchForInteractables()
    {
        Transform closest = null;
        float distance = searchDistance;
        Vector3 position = transform.position;

        foreach (var item in interactablesManager.wandInteractables)
        {
            Vector3 diff = item.position - position;
            float curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = item;
                distance = curDistance;
            }
        }

        return closest;
    }
    */
}
