﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandInteractable : MonoBehaviour
{
    public bool isEnemy;
    public bool isInteracting;
    public Transform wandPointer;
    public WandModes.Mode wandInteractionMode;
    public GameObject mainObject;
    public Collider mainCollider;
    public Rigidbody rigidBody;
    public bool isKinematic;
    public bool useGravity;
    public Transform aimObject;

    public delegate void WandInteractableState(Transform wandInteractable); //Sends to InteractablesManager
    public static event WandInteractableState OnWandInteractableActive;
    public static event WandInteractableState OnWandInteractableDeactive;
    public static event WandInteractableState OnWandInteractableActiveEnemy;
    public static event WandInteractableState OnWandInteractableDeactiveEnemy;
    public static event WandInteractableState OnWandInteractableBirth;
    public static event WandInteractableState OnWandInteractableDeath;
    public static event WandInteractableState OnWandInteractableEnemyBirth;
    public static event WandInteractableState OnWandInteractableEnemyDeath;

    private void Start()
    {
        if(rigidBody)
        {
            isKinematic = rigidBody.isKinematic;
            useGravity = rigidBody.useGravity;
        }

        if (!isEnemy)
        {
            if (OnWandInteractableBirth != null)
            {
                OnWandInteractableBirth(transform);
            }
        }
        else
        {
            if (OnWandInteractableEnemyBirth != null)
            {
                OnWandInteractableEnemyBirth(transform);
            }
        }

        if (!isEnemy)
        {
            if (OnWandInteractableActive != null)
            {
                OnWandInteractableActive(transform);
            }
        }
        else
        {
            if (OnWandInteractableActiveEnemy != null)
            {
                OnWandInteractableActiveEnemy(transform);
            }
        }
    }
    private void OnDestroy()
    {
        if (!isEnemy)
        {
            if (OnWandInteractableDeath != null)
            {
                OnWandInteractableDeath(transform);
            }
        }
        else
        {
            if (OnWandInteractableEnemyDeath != null)
            {
                OnWandInteractableEnemyDeath(transform);
            }
        }

        if (!isEnemy)
        {
            if (OnWandInteractableDeactive != null)
            {
                OnWandInteractableDeactive(transform);
            }
        }
        else
        {
            if (OnWandInteractableDeactiveEnemy != null)
            {
                OnWandInteractableDeactiveEnemy(transform);
            }
        }
    }
    private void OnEnable()
    {
        if (!isEnemy)
        {
            if (OnWandInteractableActive != null)
            {
                OnWandInteractableActive(transform);
            }
        }
        else
        {
            if (OnWandInteractableActiveEnemy != null)
            {
                OnWandInteractableActiveEnemy(transform);
            }
        } 
    }
    private void OnDisable()
    {
        if (!isEnemy)
        {
            if (OnWandInteractableDeactive != null)
            {
                OnWandInteractableDeactive(transform);
            }
        }
        else
        {
            if (OnWandInteractableDeactiveEnemy != null)
            {
                OnWandInteractableDeactiveEnemy(transform);
            }
        }
    }

}
