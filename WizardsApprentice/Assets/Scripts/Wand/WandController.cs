﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandController : MonoBehaviour
{
    public GameManager gameManager;
    public GameManager.ControlsMode controlsMode;
    public WandPc wandPc;
    public WandMobile wandMobile;
    public WandGamePad wandGamePad;
    public Wand currentWand;


    void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        controlsMode = gameManager.controlsMode;
        if(controlsMode == GameManager.ControlsMode.GamePad)
        {
            currentWand = wandGamePad;
        }
        else if(controlsMode == GameManager.ControlsMode.Mobile)
        {
            currentWand = wandMobile;
        }
        else if(controlsMode == GameManager.ControlsMode.PC)
        {
            currentWand = wandPc;
        }
    }
}
