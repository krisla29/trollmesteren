﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandBehaviourNormalTPA : WandBehaviours
{
    // Start is called before the first frame update
    public void NormalHit()
    {
        if (wand.wandAudioBeam)
        {
            wand.wandAudioBeam.pitch = 1;
        }

        wand.isHitting = true;

        wand.currentInteractable.isInteracting = true;

        wand.currentInteractable.wandPointer = wand.magicPointer.transform;
    }
}
