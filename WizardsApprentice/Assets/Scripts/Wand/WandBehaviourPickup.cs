﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandBehaviourPickup : WandBehaviours
{
    //public Wand wand;
    private bool isInitialized;

    public void PickupHit()
    {
        if (wand.wandAudioBeam)
        {
            wand.wandAudioBeam.pitch = 0.5f;
        }

        if (!wand.isPickingUp)
        {

            wand.currentInteractable.isInteracting = true;
            wand.currentInteractable.wandPointer = wand.magicPointer.transform;
            //IF COOLDOWN IS COOL, INITIALIZE PICKUP MODE:
            if (!isInitialized)
            {
                if (wand.currentInteractable.rigidBody)
                {
                    //wand.oldInteractableRigidbody = wand.playerCam.hit.transform.GetComponent<Rigidbody>();
                    wand.oldInteractableRigidbody = wand.currentInteractable.rigidBody;
                    wand.oldRbIsKinematic = wand.currentInteractable.isKinematic;
                    wand.oldRbUseGravity =  wand.currentInteractable.useGravity;
                }

                if (wand.currentInteractable.mainObject.transform.parent)
                {
                    if (wand.currentInteractable.mainObject.transform.parent != wand.playerCam.transform)
                    {
                        //wand.oldInteractableParent = wand.playerCam.hit.transform.parent;
                        wand.oldInteractableParent = wand.currentInteractable.mainObject.transform.parent;
                    }
                }
                else
                {
                    wand.oldInteractableParent = null;
                }

                isInitialized = true;

                //print("Initializing pickup object");
            }
        }
    }
    public void PickupHold()
    {
        if (wand.isPickingUp)
        {
            if (wand.currentInteractable)
            {
                wand.currentInteractable.rigidBody.isKinematic = true;
                wand.currentInteractable.rigidBody.useGravity = false;

                wand.currentInteractable.mainObject.transform.parent = wand.playerCam.transform;

                var pos = wand.currentInteractable.mainObject.transform.localPosition;

                var dir = wand.playerCam.transform.forward;
                var dist = Vector3.Distance(pos, wand.playerCam.transform.localPosition);

                wand.magicPointer.SetActive(true);
                wand.magicPointer.transform.position = wand.currentInteractable.mainObject.transform.position;
                Vector3[] positions = { wand.wandHotSpot.position, wand.magicPointer.transform.position };
                wand.lineRenderer.SetPositions(positions);

                isInitialized = false;
            }
        }
    }
}

