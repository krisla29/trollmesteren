﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandBehaviourPickupTPA : WandBehaviours
{
    private bool isInitialized;
    private float t;

    public void PickupHit()
    {
        if (wand.wandAudioBeam)
        {
            wand.wandAudioBeam.pitch = 0.5f;
        }

        if (!wand.isPickingUp)
        {
            wand.currentInteractable.isInteracting = true;
            wand.currentInteractable.wandPointer = wand.magicPointer.transform;
            //IF COOLDOWN IS COOL, INITIALIZE PICKUP MODE:
            if (!isInitialized)
            {
                if (wand.currentInteractable.rigidBody)
                {
                    if(wand.closestEnemy)
                    {
                        wand.oldInteractableRigidbody = wand.closestEnemy.GetComponent<WandInteractable>().rigidBody;
                    }
                    if(wand.closestInteractable)
                    {
                        wand.oldInteractableRigidbody = wand.closestInteractable.GetComponent<WandInteractable>().rigidBody;
                    }
                    if (wand.oldInteractableRigidbody)
                    {
                        wand.oldRbIsKinematic = wand.oldInteractableRigidbody.isKinematic;
                        wand.oldRbUseGravity = wand.oldInteractableRigidbody.useGravity;
                    }
                }

                if (wand.currentInteractable.mainObject.transform.parent != null)
                {
                    if (wand.closestEnemy)
                    {
                        wand.oldInteractableParent = wand.closestEnemy.transform.parent;
                    }
                    if (wand.closestInteractable)
                    {
                        wand.oldInteractableParent = wand.closestInteractable.transform.parent;
                    }
                }
                else
                {
                    wand.oldInteractableParent = null;
                }

                t = Time.time;

                isInitialized = true;
            }
        }
    }
    public void PickupHold()
    {
        if (wand.isPickingUp)
        {
            if (wand.currentInteractable)
            {
                wand.currentInteractable.rigidBody.isKinematic = true;
                wand.currentInteractable.rigidBody.useGravity = false;

                var newPos = wand.player.transform.position + (Vector3.up * 2) + (wand.player.transform.forward * 3f) + wand.player.transform.right;
                wand.currentInteractable.mainObject.transform.position = Vector3.Lerp(wand.currentInteractable.mainObject.transform.position, newPos, Time.deltaTime * 10f);

                var localPos = wand.currentInteractable.mainObject.transform.localPosition;
                var locY = localPos.y;

                wand.currentInteractable.mainObject.transform.localPosition = new Vector3(localPos.x,locY + (Mathf.Sin(Time.time * 2f) * 0.05f), localPos.z);

                wand.magicPointer.SetActive(true);
                wand.magicPointer.transform.position = wand.currentInteractable.mainObject.transform.position;
                Vector3[] positions = { wand.wandHotSpot.position, wand.magicPointer.transform.position };
                wand.lineRenderer.SetPositions(positions);

                isInitialized = false;
            }
        }
    }
}
