﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WandInteractableDetector : MonoBehaviour
{
    public PlayerController player;
    public List<Transform> wandInteractables = new List<Transform>();
    public List<Transform> enemies = new List<Transform>();

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("WandInteractable"))
        {
            if (CheckIfNew(other.transform, wandInteractables))
            {
                wandInteractables.Add(other.transform);
            }
            
        }
        if (other.transform.CompareTag("Enemy"))
        {
            if (CheckIfNew(other.transform, enemies))
            {
                enemies.Add(other.transform);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("WandInteractable"))
        {
            if(!CheckIfNew(other.transform, wandInteractables))
            {
                wandInteractables.Remove(other.transform);
            }            
        }
        if (other.transform.CompareTag("Enemy"))
        {
            if(!CheckIfNew(other.transform, enemies))
            {
                enemies.Remove(other.transform);
            }            
        }
    }
    
    private void OnEnable()
    {
        WandInteractable.OnWandInteractableActive += AddWandInteractable;
        WandInteractable.OnWandInteractableDeactive += RemoveWandInteractable;
        WandInteractable.OnWandInteractableActiveEnemy += AddEnemy;
        WandInteractable.OnWandInteractableDeactiveEnemy += RemoveEnemy;
    }

    private void OnDisable()
    {
        WandInteractable.OnWandInteractableActive -= AddWandInteractable;
        WandInteractable.OnWandInteractableDeactive -= RemoveWandInteractable;
        WandInteractable.OnWandInteractableActiveEnemy -= AddEnemy;
        WandInteractable.OnWandInteractableDeactiveEnemy -= RemoveEnemy;
    }
    
    private void AddWandInteractable(Transform wandInteractable)
    {
        if (CheckIfNew(wandInteractable, wandInteractables))
        {
            if (CheckDistance(wandInteractable))
            {
                wandInteractables.Add(wandInteractable);
            }
        }        
    }

    private void RemoveWandInteractable(Transform wandInteractable)
    {
        if (!CheckIfNew(wandInteractable, wandInteractables))
        {
            wandInteractables.Remove(wandInteractable);
        }       
    }

    private void AddEnemy(Transform wandInteractable)
    {
        if (CheckIfNew(wandInteractable, enemies))
        {
            if (CheckDistance(wandInteractable))
            {
                enemies.Add(wandInteractable);
            }
        }       
    }

    private void RemoveEnemy(Transform wandInteractable)
    {
        if (!CheckIfNew(wandInteractable, enemies))
        { 
            enemies.Remove(wandInteractable);
        }
    }

    private bool CheckIfNew(Transform transformToCheck, List<Transform> listToCompare)
    {
        bool isNew = true;

        foreach (var item in listToCompare)
        {
            if (item == transformToCheck)
            {
                isNew = false;
            }
        }

        if (isNew)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckDistance(Transform objectToCheck) // Same distance check as in Wand
    {
        float distance = player.detectionDistance * 10f;
        Vector3 position = player.transform.position;

        if (objectToCheck != null)
        {
            Vector3 diff = objectToCheck.position - position;
            float curDistance = diff.sqrMagnitude;

            if (curDistance < distance)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }
        
    }
    
}
