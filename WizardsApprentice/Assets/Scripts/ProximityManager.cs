﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximityManager : MonoBehaviour
{
    public GameManager gameManager;
    public Transform player;
    //public float timeInterval;
    public bool isScene; //Controlled by gameManager
    private Coroutine check;

    [System.Serializable]
    public class ProximityObjects
    {
        public string label;
        public List<Transform> objects;
        public float distance;
        public bool activeInScene;
    }

    public List<ProximityObjects> proximityObjects = new List<ProximityObjects>();
    
    private void OnEnable()
    {
        SpawnPlayer.OnSpawnNewPlayer += ReferencePlayer;
    }

    private void OnDisable()
    {
        SpawnPlayer.OnSpawnNewPlayer -= ReferencePlayer;
        StopAllCoroutines();
        check = null;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        check = null;
    }

    void Start()
    {
        check = StartCoroutine(CheckPulse());
    }

    public IEnumerator CheckPulse()
    { 
        if (player)
        {
            var playerPos = player.position;

            for (int i = 0; i < proximityObjects.Count; i++)
            {
                var x = proximityObjects[i];

                for (int j = 0; j < proximityObjects[i].objects.Count; j++)
                {

                    Check(x, x.objects[j], x.distance, playerPos);
                    print(Time.time);
                    yield return new WaitForEndOfFrame();
                }

                yield return new WaitForEndOfFrame();
            }            
        }

        yield return new WaitForEndOfFrame();

        check = StartCoroutine(CheckPulse());
    }

    void Check(ProximityObjects poObj, Transform obj, float distance, Vector3 playerCurrentPos)
    {
        if ((obj.position - playerCurrentPos).sqrMagnitude / 100f <= distance)
        {
            if(!isScene)
                obj.gameObject.SetActive(true);
            else
            {
                if(poObj.activeInScene)
                    obj.gameObject.SetActive(true);
                else
                    obj.gameObject.SetActive(false);
            }
        }
        else
        {
            obj.gameObject.SetActive(false);
        }               
    }

    void ReferencePlayer(Transform playerTransform)
    {
        player = playerTransform;
    }    
}
