﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillByField : MonoBehaviour
{
    [SerializeField] private bool isInverse;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("KillField") && !isInverse)
        {
            Destroy(transform.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("KillField") && isInverse)
        {
            Destroy(transform.gameObject);
        }
    }
}
