﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerScanner : MonoBehaviour
{
    public GameManager gameManager;
    public List<TowerScannerObj> scannerObjects = new List<TowerScannerObj>();
    public TowerScannerDoor towerScannerDoor;
    public Transform bossGameLocation;
    public Collider sceneCollider;

    public Transform rayBlocker;
    public Transform enterPoint;
    private Coroutine checkT;
    private Coroutine success;
    public bool doorUnlocked;

    public AudioSource soundAccessDenied;
    public AudioSource soundAccessGranted;
    public AudioSource soundSuccess;
    public AudioSource soundHasTrophy;
    public AudioSource soundTrophyMissing;
    /*
    public GameEventTrigger meetWizardTrigger;
    public GameEvent meetWizardEvent;

    public GameEventTrigger completeWizardTrigger;
    public GameEvent completeWizardEvent;
    */
    /*
    private void Awake()
    {
        ResetAll();
    }
    */
    private void OnEnable()
    {
        doorUnlocked = gameManager.playerStats.hasOpenedTowerScanner;

        ResetAll();

        if (checkT != null)
            StopCoroutine(checkT);

        checkT = StartCoroutine(CheckTrophys());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        checkT = null;
        ResetAll();
        /*
        if(gameManager.playerStats.hasMetWizard && !gameManager.playerStats.hasCompletedWizard)
        {
            completeWizardTrigger.gameObject.SetActive(true);
            completeWizardEvent.gameObject.SetActive(true);
        }
        */
    }

    private IEnumerator CheckTrophys()
    {
        rayBlocker.gameObject.SetActive(true);

        bool hasAll = true;

        for (int i = 0; i < scannerObjects.Count; i++)
        {
            yield return new WaitForSeconds(0.25f);

            scannerObjects[i].gameObject.SetActive(true);

            bool check = false;

            if (gameManager.playerStats.trophies.Contains(scannerObjects[i].trophy))
                check = true;

            if(check)
            {
                if (soundHasTrophy)
                    soundHasTrophy.Play();

                scannerObjects[i].gameObject.SetActive(true);
                StartCoroutine(ScaleObject(scannerObjects[i], true));
            }
            else
            {
                if (soundTrophyMissing)
                    soundTrophyMissing.Play();

                StartCoroutine(ErrorBlink(scannerObjects[i].glowError));

                hasAll = false;
            }
        }

        if (!doorUnlocked)
        {
            if (hasAll)
            {
                if (soundAccessGranted)
                    soundAccessGranted.Play();

                if (success != null)
                    StopCoroutine(success);

                success = StartCoroutine(Success());

                doorUnlocked = true;
            }
            else
            {
                if (soundAccessDenied)
                    soundAccessDenied.Play();
            }
        }
    }

    private IEnumerator AnimateTrophyIn()
    {
        float t = 0;
        float checkTime = 2;

        while (t < 1)
        {
            t += Time.deltaTime / checkTime;
            yield return new WaitForEndOfFrame();
        }
    }

    void ResetAll()
    {
        for (int i = 0; i < scannerObjects.Count; i++)
        {
            var color = scannerObjects[i].glowCorrect.color;
            scannerObjects[i].glowCorrect.color = new Color(color.r, color.g, color.b, 0);
            var color2 = scannerObjects[i].glowError.color;
            scannerObjects[i].glowError.color = new Color(color2.r, color2.g, color2.b, 0);
            scannerObjects[i].symbol.transform.localScale = Vector3.zero;
            scannerObjects[i].symbol.gameObject.SetActive(false);
            scannerObjects[i].gameObject.SetActive(false);
        }        

        if(doorUnlocked)
        {
            for (int i = 0; i < towerScannerDoor.sprites.Count; i++)
            {
                towerScannerDoor.sprites[i].color = towerScannerDoor.colorOpen;
            }

            towerScannerDoor.lockSymbol.transform.localScale = Vector3.zero;
            var lockColor = towerScannerDoor.lockSymbol.color;
            var lockGlowColor = towerScannerDoor.lockGlow.color;
            towerScannerDoor.lockSymbol.color = new Color(lockColor.r, lockColor.g, lockColor.b, 0);
            towerScannerDoor.lockGlow.color = new Color(lockGlowColor.r, lockGlowColor.g, lockGlowColor.b, 0);
            towerScannerDoor.lockSymbol.gameObject.SetActive(false);
            towerScannerDoor.navMeshObstacle.enabled = false;
            sceneCollider.gameObject.SetActive(false);
            /*
            if (!gameManager.playerStats.hasMetWizard)
            {
                meetWizardTrigger.gameObject.SetActive(true);
                meetWizardEvent.gameObject.SetActive(true);
            }
            */
            gameManager.playerStats.CheckWizardEvents();
        }
        else
        {
            for (int i = 0; i < towerScannerDoor.sprites.Count; i++)
            {
                towerScannerDoor.sprites[i].color = towerScannerDoor.colorLock;
            }

            towerScannerDoor.lockSymbol.transform.localScale = Vector3.one * 0.5f;
            var lockColor = towerScannerDoor.lockSymbol.color;
            var lockGlowColor = towerScannerDoor.lockGlow.color;
            towerScannerDoor.lockSymbol.color = new Color(lockColor.r, lockColor.g, lockColor.b, 0.15f);
            towerScannerDoor.lockGlow.color = new Color(lockGlowColor.r, lockGlowColor.g, lockGlowColor.b, 0.1f);
            towerScannerDoor.lockSymbol.gameObject.SetActive(true);
            towerScannerDoor.navMeshObstacle.enabled = true;
            sceneCollider.gameObject.SetActive(true);
        }

        rayBlocker.gameObject.SetActive(false);
    }

    private IEnumerator FadeGlow(bool fadeIn, SpriteRenderer sprite)
    {
        float t = 0;
        float FadeTime = 1;
        Color transparent = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
        Color opaque = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);

        while (t < 1)
        {
            t += Time.deltaTime / FadeTime;
            var h = LerpUtils.SmootherStep(t);
            if (fadeIn)
                sprite.color = Color.Lerp(transparent, opaque, h);
            else
                sprite.color = Color.Lerp(opaque, transparent, h);

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ScaleObject(TowerScannerObj obj, bool scaleUp)
    {
        StartCoroutine(FadeGlow(true, obj.glowCorrect));

        float t = 0;
        float scaleTime = 1;
        obj.symbol.gameObject.SetActive(true);

        while(t < 1)
        {
            t += Time.deltaTime / scaleTime;
            var h = LerpUtils.SmootherStep(t);
            if (scaleUp)
                obj.symbol.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * 0.35f, h);
            else
                obj.symbol.transform.localScale = Vector3.Lerp(Vector3.one * 0.35f, Vector3.zero, h);

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ErrorBlink(SpriteRenderer sprite)
    {
        float t = 0;
        float blinkTime = 1;
        Color transparent = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
        Color opaque = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);

        while (t < 1)
        {
            t += Time.deltaTime / blinkTime;
            var h = LerpUtils.SmootherStep(t);
            var s = Mathf.Sin(h * Mathf.PI);
            sprite.color = Color.Lerp(transparent, opaque, s);

            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator Success()
    {
        yield return new WaitForSeconds(2);

        if (soundSuccess)
            soundSuccess.Play();

        float t1 = 0;
        float time1 = 2;
        var lockColor = towerScannerDoor.lockSymbol.color;
        Color transparent = new Color(lockColor.r, lockColor.g, lockColor.b, 0);
        Color opaque = new Color(lockColor.r, lockColor.g, lockColor.b, 0.15f);
        var lockGlowColor = towerScannerDoor.lockGlow.color;
        Color transparentG = new Color(lockGlowColor.r, lockGlowColor.g, lockGlowColor.b, 0);
        Color opaqueG = new Color(lockGlowColor.r, lockGlowColor.g, lockGlowColor.b, 0.1f);

        while (t1 < 1)
        {
            t1 += Time.deltaTime / time1;
            var h = LerpUtils.SmootherStep(t1);
            foreach (var sprite in towerScannerDoor.sprites)
            {
                sprite.color = Color.Lerp(towerScannerDoor.colorLock, towerScannerDoor.colorOpen, h);
            }
            towerScannerDoor.lockSymbol.color = Color.Lerp(opaque, transparent, h);
            towerScannerDoor.lockSymbol.transform.localScale = Vector3.Lerp(Vector3.one * 0.5f, Vector3.zero, h);
            towerScannerDoor.lockGlow.color = Color.Lerp(opaqueG, transparentG, h);
            yield return new WaitForEndOfFrame();
        }

        gameManager.playerStats.hasOpenedTowerScanner = true;
        gameManager.playerStats.SaveGame();

        towerScannerDoor.navMeshObstacle.enabled = false;
        yield return new WaitForEndOfFrame();
        gameManager.player.navMeshAgent.SetDestination(enterPoint.position);

        rayBlocker.gameObject.SetActive(false);
    }

    public void CheckIfUnlocked()
    {
        //if (gameManager.playerStats.unlockedLocations.Contains(gameManager.worldMapController.worldMapCanvas.locations.IndexOf(bossGameLocation)) ||
        //gameManager.playerStats.hasOpenedTowerScanner)
        if (gameManager.playerStats.hasOpenedTowerScanner)
        {
            doorUnlocked = true;
            towerScannerDoor.navMeshObstacle.enabled = false;
            rayBlocker.gameObject.SetActive(false);
            ResetAll();
        }
    }
}
