﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TowerScannerDoor : MonoBehaviour
{
    public SpriteRenderer lockSymbol;
    public SpriteRenderer lockGlow;
    public List<SpriteRenderer> sprites = new List<SpriteRenderer>();
    public Color colorLock = Color.magenta;
    public Color colorOpen = Color.cyan;
    public NavMeshObstacle navMeshObstacle;
}
