﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TowerScannerObj : MonoBehaviour
{
    public PlayerStats.Trophy trophy;
    public SpriteRenderer glowCorrect;
    public SpriteRenderer glowError;
    public SpriteRenderer symbol;
}
