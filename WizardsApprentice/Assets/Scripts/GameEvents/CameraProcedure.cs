﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CameraProcedure : MonoBehaviour
{
    public GameEvent gameEvent;
    
    public void RunCamProcedure()
    {
        StartCoroutine("Procedure");
    }
    
    public virtual IEnumerator Procedure()
    {       
        if (gameEvent.CameraEventDeactivation())
        {
            yield return null;
        }
    }
    
    public virtual void StartProcedure()
    {

    }
    
    public virtual void UpdateProcedure(float time)
    {

    }

    public virtual void EndProcedure()
    {

    }
    /*
    public void OnDisable()
    {
        StopAllCoroutines();
    }

    public void OnDestroy()
    {
        StopAllCoroutines();
    }
    */
}
