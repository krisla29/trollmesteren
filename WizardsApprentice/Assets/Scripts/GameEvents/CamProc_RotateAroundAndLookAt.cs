﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamProc_RotateAroundAndLookAt : CameraProcedure
{
    public float camDistanceToTarget = 5f;
    public float height = 1f;
    public float startRotation;
    public float endRotation;
    [TextArea]
    public string procedureDescription = "camera parent rotates around target on Y axis while camera is looking at target. Requires target to work.";
    /*
    public override IEnumerator Procedure()
    {
        CheckForPlayerTarget();

        cameraEvent.eventCamParent.transform.rotation = cameraEvent.cameraTarget.rotation;
        cameraEvent.eventCamera.transform.localPosition = -cameraEvent.eventCamera.transform.forward * camDistanceToTarget;
        
        float startRotation = cameraEvent.eventCamParent.transform.eulerAngles.y;
        float endRotation = startRotation + 360.0f;
        float t = 0;

        while(t < 1)
        {
            print("in camEventProcedure");
            CheckForPlayerTarget();

            cameraEvent.eventCamParent.transform.position = cameraEvent.cameraTarget.position + (Vector3.up * height);
            cameraEvent.eventCamera.transform.LookAt(cameraEvent.cameraTarget, Vector3.up);

            float yRotation = Mathf.Lerp(startRotation, endRotation, t) % 360.0f;
            cameraEvent.eventCamParent.transform.eulerAngles = new Vector3(transform.eulerAngles.x, yRotation, transform.eulerAngles.z);

            t += Time.deltaTime / cameraEvent.duration;
        }

        //return base.Procedure();
    }
    */
    public void CheckForPlayerTarget()
    {
        if (gameEvent.targetIsPlayer)
        {
            if (gameEvent.player)
            {
                gameEvent.cameraTarget = gameEvent.player.transform;
            }
        }
    }

    public override void StartProcedure()
    {
        base.StartProcedure();

        CheckForPlayerTarget();

        gameEvent.eventCamParent.transform.rotation = gameEvent.cameraTarget.rotation;
        gameEvent.eventCamera.transform.localPosition = -gameEvent.eventCamera.transform.forward * camDistanceToTarget;
        startRotation = gameEvent.eventCamParent.transform.eulerAngles.y;
        endRotation = startRotation + 360.0f;
    }

    public override void UpdateProcedure(float time)
    {
        //base.UpdateProcedure();

        print("in camEventProcedure");
        CheckForPlayerTarget();

        gameEvent.eventCamParent.transform.position = gameEvent.cameraTarget.position + (Vector3.up * height);
        gameEvent.eventCamera.transform.LookAt(gameEvent.cameraTarget, Vector3.up);

        float yRotation = Mathf.Lerp(startRotation, endRotation, time) % 360.0f;
        gameEvent.eventCamParent.transform.eulerAngles = new Vector3(transform.eulerAngles.x, yRotation, transform.eulerAngles.z);
    }
}
