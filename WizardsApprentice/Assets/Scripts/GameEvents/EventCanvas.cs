﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventCanvas : MonoBehaviour
{
    public Canvas canvas;
    public SpriterController spriter;
    public ParticleSystem spriterPS;
    public ParticleSystem.EmissionModule spriterPSemission;
    private float startEmission;
    public Transform spriterTransform;    
    public float spriterEnterTime = 1f;

    public bool useUnscaledTime;
    private float time;

    [System.Serializable]
    public struct SpriterKeys
    {
        public Vector3 spriterKeyPos;
        public Vector3 spriterKeyRot;
        public float spriterKeyTime;
    }
    public List<SpriterKeys> spriterKeyframes = new List<SpriterKeys>(1);
    private int spriterKeyFrameCounter;

    [System.Serializable]
    public struct GraphicClips
    {
        public CanvasGroup graphicClip;
        public Mask graphiClipAnim;
        public float graphicClipTime;
        public float clipLerpsTime;
    }
    public List<GraphicClips> graphicClips = new List<GraphicClips>(1);
    public float graphicClipsStartTime = 1;
    private int graphicClipsCounter;
    private Vector3 clipStartScale;
    
    public CanvasGroup bg;
    public float bgStartTime;
    public float bgEndTime;
    public float bgLerpInTime = 1f;
    public float bgLerpOutTime = 2f;
    private float bgStartAlpha;

    private void Awake()
    {
        canvas = transform.GetComponent<Canvas>();
    }
    private void Start()
    {
        spriterPSemission = spriterPS.emission;
        startEmission = spriterPSemission.rateOverTime.constant;
    }

    private void Update()
    {
        if (useUnscaledTime)
            time = Time.unscaledDeltaTime;
        else
            time = Time.deltaTime;
    }

    void OnEnable()
    {
        if (spriter)
        {
            spriterTransform.localPosition = spriterKeyframes[0].spriterKeyPos;
            spriterTransform.localEulerAngles = spriterKeyframes[0].spriterKeyRot;

            if (!useUnscaledTime)
                Invoke("SpriterStart", spriterEnterTime);
            else
                StartCoroutine(StartSpriterStart());
        } 
        if (graphicClips.Count > 0)
        {
            if (!useUnscaledTime)
                Invoke("ClipStart", graphicClipsStartTime);
            else
                StartCoroutine(StartClipStart());
        }
    
        if(bg)
        {
            if (!useUnscaledTime)
                Invoke("BgStart", bgStartTime);
            else
                StartCoroutine(StartBgStart());
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
        CancelInvoke();
    }

    private void SpriterStart()
    {
        if (spriter)
        {
            if (gameObject.activeSelf)
            {
                spriterTransform.gameObject.SetActive(true);
                StartCoroutine(SpriterAppear());

                if(!useUnscaledTime)
                    Invoke("SpriterEnd", spriterKeyframes[spriterKeyFrameCounter].spriterKeyTime);
                else
                    StartCoroutine(StartSpriterEnd());
            }
        }
    }

    private IEnumerator StartSpriterEnd()
    {
        yield return new WaitForSecondsRealtime(spriterKeyframes[spriterKeyFrameCounter].spriterKeyTime);
        SpriterEnd();
    }

    private void SpriterEnd()
    {
        if (spriter)
        {
            if (gameObject.activeSelf)
                StartCoroutine(SpriterVanish());
        }
    }

    private IEnumerator SpriterVanish()
    {
        //print("Spriter vanishes");
        spriterPSemission.rateOverTime = 0;

        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            spriterTransform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, smoothLerp);           

            t += time / spriter.enterExitTime;
            yield return new WaitForEndOfFrame();
        }

        spriterTransform.gameObject.SetActive(false);

        if(spriterKeyFrameCounter < spriterKeyframes.Count - 1)
        {
            if (gameObject.activeSelf)
            {
                spriterKeyFrameCounter += 1;
                /*
                if (!useUnscaledTime)
                    Invoke("SpriterStart", 0);
                else
                    StartCoroutine(StartSpriterStart());
                */
                SpriterStart();
            }
        }
    }

    private IEnumerator StartSpriterStart()
    {
        yield return new WaitForSecondsRealtime(spriterEnterTime);
        SpriterStart();
    }

    private IEnumerator SpriterAppear()
    {
        spriterPSemission.rateOverTime = startEmission;

        if (spriterKeyFrameCounter < spriterKeyframes.Count)
        {
            spriterTransform.transform.localPosition = spriterKeyframes[spriterKeyFrameCounter].spriterKeyPos;
            spriterTransform.transform.localEulerAngles = spriterKeyframes[spriterKeyFrameCounter].spriterKeyRot;
        }

        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            spriterTransform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, smoothLerp);

            t += time / spriter.enterExitTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ClipAppear()
    {
        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            graphicClips[graphicClipsCounter].graphicClip.transform.localScale = Vector3.Lerp(Vector3.zero, clipStartScale, smoothLerp);

            t += time / graphicClips[graphicClipsCounter].clipLerpsTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ClipVanish()
    {
        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            graphicClips[graphicClipsCounter].graphicClip.transform.localScale = Vector3.Lerp(clipStartScale, Vector3.zero, smoothLerp);

            t += time / graphicClips[graphicClipsCounter].clipLerpsTime;
            yield return new WaitForEndOfFrame();
        }

        graphicClips[graphicClipsCounter].graphicClip.gameObject.SetActive(false);

        if (graphicClipsCounter < graphicClips.Count - 1)
        {
            if (gameObject.activeSelf)
            {
                graphicClipsCounter += 1;
                /*
                if (!useUnscaledTime)
                    Invoke("ClipStart", 0);
                else
                    StartCoroutine(StartClipStart());
                */
                ClipStart();
            }
        }
    }

    private IEnumerator StartClipStart()
    {
        yield return new WaitForSecondsRealtime(graphicClipsStartTime);
        ClipStart();
    }

    private void ClipStart()
    {
        if (gameObject.activeSelf)
        {
            clipStartScale = graphicClips[graphicClipsCounter].graphicClip.transform.localScale;
            graphicClips[graphicClipsCounter].graphicClip.transform.localScale = Vector3.zero;
            graphicClips[graphicClipsCounter].graphicClip.gameObject.SetActive(true);
            StartCoroutine(ClipAppear());

            if (!useUnscaledTime)
                Invoke("ClipEnd", graphicClips[graphicClipsCounter].graphicClipTime);
            else
                StartCoroutine(StartClipEnd());
        }
    }

    private IEnumerator StartClipEnd()
    {
        yield return new WaitForSecondsRealtime(graphicClips[graphicClipsCounter].graphicClipTime);
        ClipEnd();
    }

    private void ClipEnd()
    {
        if (gameObject.activeSelf)
            StartCoroutine(ClipVanish());
    }

    private IEnumerator BgAppear()
    {
        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            bg.alpha = Mathf.Lerp(0, bgStartAlpha, smoothLerp);

            t += time / bgLerpInTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator BgVanish()
    {
        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            bg.alpha = Mathf.Lerp(bgStartAlpha, 0, smoothLerp);

            t += time / bgLerpOutTime;
            yield return new WaitForEndOfFrame();
        }

        if (gameObject.activeSelf)
            bg.gameObject.SetActive(false);
    }

    private IEnumerator StartBgStart()
    {
        yield return new WaitForSecondsRealtime(bgStartTime);
        BgStart();
    }

    void BgStart()
    {
        if (gameObject.activeSelf)
        {
            bgStartAlpha = bg.alpha;
            bg.alpha = 0;
            bg.gameObject.SetActive(true);
            StartCoroutine(BgAppear());

            if (!useUnscaledTime)
                Invoke("BgEnd", bgEndTime);
            else
                StartCoroutine(StartBGEnd());
        }
    }

    private IEnumerator StartBGEnd()
    {
        yield return new WaitForSecondsRealtime(bgEndTime);
        BgEnd();
    }

    void BgEnd()
    {
        if(gameObject.activeSelf)
            StartCoroutine(BgVanish());
    }


    void TransformSpriter()
    {

    }

}
