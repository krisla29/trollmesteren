﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriterController : MonoBehaviour
{
    public Animator animator;
    public bool isTalking;
    public float enterExitTime = 0.5f;

    public bool useUnscaledTime;
    private GameObject worldSpriter;
    /*
    public AudioSource spriterVoice01;
    public AudioSource spriterVoice02;
    public AudioSource spriterVoice03;
    public AudioSource spriterVoice04;
    public AudioSource spriterVoice05;
    */
    void Start()
    {
        if (isTalking)
        {
            var randTime = Random.Range(0, 2f);

            if (!useUnscaledTime)
                Invoke("StartTalkAnim", randTime);
            else
                StartCoroutine(InvokeStartTalkAnim(randTime));
        }
    }

    private void OnEnable()
    {
        //SpawnPlayer.OnSpawnNewPlayer += ReferencePlayer;
        worldSpriter = GameObject.FindGameObjectWithTag("Spriter");

        if (worldSpriter)
        {
            if (worldSpriter.transform.root != null)
                worldSpriter = worldSpriter.transform.root.gameObject;

                worldSpriter.SetActive(false);
        }
    }

    private void OnDisable()
    {
        //SpawnPlayer.OnSpawnNewPlayer -= ReferencePlayer;
        if (worldSpriter)
            worldSpriter.SetActive(true);

        CancelInvoke();
        StopAllCoroutines();
    }

    private IEnumerator InvokeStartTalkAnim(float time)
    {
        yield return new WaitForSecondsRealtime(time);

        StartTalkAnim();
    }

    void StartTalkAnim()
    {
        animator.SetBool("isTalking", true);
        var randTime = Random.Range(2f, 4f);

        if (!useUnscaledTime)
            Invoke("StopTalkAnim", randTime);
        else
            StartCoroutine(InvokeStopTalkAnim(randTime));
        //waveAudio.Play();
    }

    private IEnumerator InvokeStopTalkAnim(float time)
    {
        yield return new WaitForSecondsRealtime(time);

        StopTalkAnim();
    }

    void StopTalkAnim()
    {
        animator.SetBool("isTalking", false);
        var randTime = Random.Range(0.5f, 2f);

        if(!useUnscaledTime)
            Invoke("StartTalkAnim", randTime);
        else
            StartCoroutine(InvokeStartTalkAnim(randTime));
    }

    private void OnDestroy()
    {
        CancelInvoke();
        StopAllCoroutines();
    }

    /*
    void ReferencePlayer(Transform playerTransform)
    {
        player = playerTransform.GetComponent<PlayerController>();
    }
    */
}
