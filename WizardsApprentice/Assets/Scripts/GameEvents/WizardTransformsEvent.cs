﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WizardTransformsEvent : MonoBehaviour
{
    public GameManager gameManager;
    public Camera eventCam;
    public WandWorld wandObject;
    public Transform wizardHead;
    private Vector3 origPos;

    public bool playerLooksAtWizard;
    public bool playerLooksAtWand;

    public GameObject magicConjureEffect;
    public GameObject magicPoofEffect;
    public GameObject magicAquireEffect;

    public AudioSource boom;
    public AudioSource magicConjure;
    public AudioSource magicPoof;
    public AudioSource victorySound;
    public AudioSource magicAquire;

    void Start()
    {
        Invoke("SetPlayerStartled", 3);
        Invoke("SetPlayerLooksAtWizard", 5);
        Invoke("PlayMagicEffect01", 15f);
        Invoke("RevealWand", 16f);
        Invoke("SetPlayerSuccess", 17f);
        //Invoke("HideWand", 22f);
    }

    void SetPlayerLooksAtWizard()
    {
        playerLooksAtWizard = true;
    }

    void SetPlayerStartled()
    {
        if (boom)
            boom.Play();

        gameManager.player.anim.SetTrigger("Startled");
        origPos = gameManager.player.anim.gameObject.transform.localPosition;
        StartCoroutine(MovePlayerCharacter());
    }

    void SetPlayerSuccess()
    {
        gameManager.player.anim.SetTrigger("Success");
    }

    void RevealWand()
    {
        StartCoroutine(WandHandOver());
    }

    void HideWand()
    {

    }

    void PlayMagicEffect01()
    {
        if (magicConjure)
            magicConjure.Play();

        var effect = Instantiate(magicConjureEffect, transform.position, Quaternion.identity, transform);
    }

    private void OnDisable()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        CancelInvoke();
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        CancelInvoke();
        StopAllCoroutines();
    }

    private IEnumerator MovePlayerCharacter()
    {
        float t = 0;
        float animTime1 = 1;

        while(t < 1)
        {
            t += Time.deltaTime / animTime1;
            var h = LerpUtils.SmootherStep(t);
            gameManager.player.anim.gameObject.transform.localPosition = Vector3.Lerp(origPos, origPos + (-gameManager.player.anim.gameObject.transform.right * 1.5f), h);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.5f);

        float x = 0;
        float animTime2 = 1.5f;
        Vector3 newPos = gameManager.player.anim.gameObject.transform.localPosition;

        while (x < 1)
        {
            x += Time.deltaTime / animTime2;
            var h = LerpUtils.SmootherStep(x);
            gameManager.player.anim.gameObject.transform.localPosition = Vector3.Lerp(newPos, origPos, h);
            yield return new WaitForEndOfFrame();
        }

        gameManager.player.anim.gameObject.transform.localPosition = origPos;
    }

    private IEnumerator WandHandOver()
    {
        playerLooksAtWizard = false;
        playerLooksAtWand = true;
        float t = 0;
        float animTime = 5;
        float yMult = 0.25f;
        wandObject.gameObject.SetActive(true);
        Vector3 startPos = wandObject.transform.localPosition;
        StartCoroutine(ScaleWandUp());

        while(t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            var s = Mathf.Sin(t * (Mathf.PI * 2));
            var sm = s * yMult;
            wandObject.transform.localPosition = startPos + new Vector3(0, sm, 0);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ScaleWandUp()
    {
        float t = 0;
        float animTime = 1;

        if (magicPoof)
            magicPoof.Play();

        var effect = Instantiate(magicPoofEffect, wandObject.transform.position, Quaternion.identity, transform);

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            wandObject.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one * 1.5f, h);
            yield return new WaitForEndOfFrame();
        }

        if (victorySound)
            victorySound.Play();
       
        yield return new WaitForSeconds(4);
        StartCoroutine(ScaleWandDown());
        //StartCoroutine(MoveWandTowardsCamera());
    }

    private IEnumerator MoveWandTowardsCamera()
    {
        float t = 0;
        float animTime = 3;
        Vector3 startPos = wandObject.transform.position;

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            wandObject.transform.position = Vector3.Lerp(startPos, eventCam.transform.position, h);
            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(ScaleWandDown());
    }

    private IEnumerator ScaleWandDown()
    {
        float t = 0;
        float animTime = 0.5f;
        Vector3 startPos = wandObject.transform.position;
        playerLooksAtWand = false;

        if (magicAquire)
            magicAquire.Play();

        var effect = Instantiate(magicAquireEffect, wandObject.star.position, Quaternion.identity, transform);
        Vector3 effectStartPos = effect.transform.position;

        while (t < 1)
        {
            t += Time.deltaTime / animTime;
            var h = LerpUtils.SmootherStep(t);
            wandObject.transform.localScale = Vector3.Lerp(Vector3.one * 1.5f, Vector3.zero, h);
            wandObject.transform.position = Vector3.Lerp(startPos, gameManager.player.transform.position + Vector3.up, h);
            effect.transform.position = Vector3.Lerp(effectStartPos, gameManager.player.transform.position + Vector3.up, h);
            yield return new WaitForEndOfFrame();
        }

        wandObject.gameObject.SetActive(false);
    }

    private void LateUpdate()
    {
        if (playerLooksAtWizard)
            gameManager.player.head.LookAt(wizardHead);

        if (playerLooksAtWand)
            gameManager.player.head.LookAt(wandObject.transform);
    }
}
