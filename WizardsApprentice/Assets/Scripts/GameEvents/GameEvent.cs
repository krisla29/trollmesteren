﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public GameEventsManager gameEventsManager;
    public bool useCamera = true;
    public CameraSceneManager camSceneManager;
    public bool targetIsPlayer;
    public Transform cameraTarget;
    public bool camEventStartAtPlayer;   
    public Transform camEventStartTransform;
    public Vector3 posOffset = Vector3.zero;
    public Vector3 rotOffset = Vector3.zero;
    public bool camStartAtPlayerCam;
    public bool camEndsAtPlayerCam;
    public float duration;
    public bool deactivateOnCompletion = true;
    public List<Transform> wakeUpOnComplete = new List<Transform>();
    public List<Transform> sleepOnComplete = new List<Transform>();
    public enum CameraEventTypes { Keys, Procedural, Animated}
    public CameraEventTypes cameraEventType;

    [System.Serializable]
    public struct CamKeys
    {
        public float duration;
        public Transform parentTransform;
        public Transform camTransform;
        public EventCamera.CameraMoves move;
    }
    
    public List<CamKeys> cameraKeys = new List<CamKeys>();
    public int cameraKeysCounter;
    public bool cameraKeysAutoLook;
    public EventCamera.CameraEnds cameraEnd;
    //public bool isAnimated;
    public CameraProcedure cameraProcedure;
    public Animator camAnimator;
    public string animatorState;
    public float animationSpeed = 1f;

    private float currentDuration;
    private Transform curParTransform;
    private Transform curCamTransform;
    private EventCamera.CameraMoves curMove;

    public Camera eventCamera;
    public Transform eventCamParent;
    public EventCanvas eventCanvas;
    public bool lockPlayer = true;
    public bool teleportPlayer = true;
    public bool hideSpriter;
    public Transform playerStartPos;
    public Transform playerEndPos;
    public AudioSource TriggerSound;
    public bool hasSound = true;
    public bool hideUI;

    [Header("READONLY")]
    public PlayerController player; //Readonly
    public Camera playerCam; //Readonly
    public Transform playerCamParent; //Readonly
    public delegate void CamEvent(GameEvent thisEvent); // Sends to CameraSceneManager
    public static event CamEvent OnCameraEventStart;
    public static event CamEvent OnCameraEventStop;
    public delegate void GameEvents(GameEvent thisEvent); // General, fetched by GameEventTrigger
    public static event GameEvents OnGameEventStart;
    public static event GameEvents OnGameEventStop;

    public bool useUnscaledTime;

    public bool GameEventActivation()
    {
        if (hasSound)
            TriggerSound.Play();

        if (OnGameEventStart != null)
        {            
            OnGameEventStart(this);
            print("Sent GameEventStart Message");
        }

        if (!useCamera)
        {
            eventCanvas.gameObject.SetActive(true);
            eventCanvas.canvas.worldCamera = Camera.main;

            float endTime = eventCanvas.bgStartTime + eventCanvas.bgLerpInTime + eventCanvas.bgEndTime + eventCanvas.bgLerpOutTime;

            if (!useUnscaledTime)
                Invoke("SetDeactivation", endTime);
            else
                StartCoroutine(InvokeSetDeactivation(endTime));
        }

        return true;
    }

    private IEnumerator InvokeSetDeactivation(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        SetDeactivation();
    }

    public void SetDeactivation()
    {
        GameEventDeactivation();
      
        if (deactivateOnCompletion)
            transform.gameObject.SetActive(false);
    }

    public bool GameEventDeactivation()
    {
        if (OnGameEventStop != null)
        {
            OnGameEventStop(this);
            print("Sent GameEventStop Message");
        }

        return true;
    }

    public bool CameraEventActivation()
    {
        if (OnCameraEventStart != null)
        {   
            //if(hasSound)
                //TriggerSound.Play();
            OnCameraEventStart(this);
            print("Sent CamEventStart Message");
        }

        return true;
    }
    
    public bool CameraEventDeactivation()
    {
        if (OnCameraEventStop != null)
        {
            OnCameraEventStop(this);
            print("Sent CamEventStop Message");
        }

        return true;
    }
   
    public void InitializeCamEvent(GameEvent passedEvent)
    {
        if (this == passedEvent)
        {
            camSceneManager = GameObject.FindGameObjectWithTag("CamSceneManager").GetComponent<CameraSceneManager>();

            ResetCamera();
           
            if (camSceneManager.playerController)
            {
                player = camSceneManager.playerController;
                playerCam = camSceneManager.playerCamera;
                playerCamParent = playerCam.transform.parent;
                curParTransform = playerCamParent;
                curCamTransform = playerCam.transform;
            }

            if(targetIsPlayer)
            {
                if(player)
                {                    
                    cameraTarget = player.transform;
                }
            }

            if(camEventStartAtPlayer)
            {
                if(player)
                {
                    transform.position = player.transform.position;
                    transform.rotation = player.transform.rotation;
                }
            }
            else if(!camEventStartAtPlayer && camEventStartTransform)
            {
                transform.position = camEventStartTransform.position;
                transform.rotation = camEventStartTransform.rotation;
            }

            transform.position += posOffset;
            transform.localEulerAngles += rotOffset;
            
            if(camStartAtPlayerCam && player)
            {
                eventCamParent.position = playerCamParent.position;
                eventCamParent.rotation = playerCamParent.rotation;
                eventCamera.transform.localPosition = playerCam.transform.localPosition;
                eventCamera.transform.localRotation = playerCam.transform.localRotation;
            }
            
            curMove = EventCamera.CameraMoves.Stepped;
            currentDuration = 0;
            cameraKeysCounter = 0;

            if(eventCanvas)
            {
                eventCanvas.gameObject.SetActive(true);
                if (cameraEventType == CameraEventTypes.Keys)
                {
                    float totalTime = 0;

                    for (int i = 0; i < cameraKeys.Count; i++)
                    {
                        totalTime += cameraKeys[i].duration;
                    }

                    eventCanvas.bgEndTime = totalTime - eventCanvas.bgLerpOutTime;
                }

                if (cameraEventType == CameraEventTypes.Procedural)
                {
                    eventCanvas.bgEndTime = duration - eventCanvas.bgLerpOutTime;
                }

                if (cameraEventType == CameraEventTypes.Animated)
                {
                    eventCanvas.bgEndTime = eventCanvas.bgLerpOutTime;
                }               
            }

            if (cameraEventType == CameraEventTypes.Keys)
            {
                CamKeysBehaviour();
            }

            if(cameraEventType == CameraEventTypes.Procedural)
            {
                CamProceduralBehaviour();
            }

            if (cameraEventType == CameraEventTypes.Animated)
            {
                CamAnimatedBehaviour();
            }

            if(player)
            {
                if (player.navMeshAgent.enabled)
                {
                    if (playerStartPos != null)
                    {
                        player.navMeshAgent.SetDestination(playerStartPos.position);
                        StartCoroutine(RotatePlayer(playerStartPos.rotation, true));
                    }
                }

                if (hideSpriter)
                {
                    if (player.spriter)
                        player.spriter.gameObject.SetActive(false);
                }

                if (lockPlayer)
                {
                    if (playerStartPos != null)
                    {
                        if (teleportPlayer)
                        {
                            player.transform.position = playerStartPos.position;
                            StartCoroutine(RotatePlayer(playerStartPos.rotation, true));
                        }                        
                    }
                }
            }            
        }
    }

    public void TerminateCamEvent(GameEvent passedEvent)
    {
        if (this == passedEvent)
        {
            eventCanvas.gameObject.SetActive(false);
            eventCamera.gameObject.SetActive(false);

            if (player)
            {
                if (player.navMeshAgent.enabled && !lockPlayer)
                {
                    if (playerEndPos != null)
                    {
                        player.navMeshAgent.SetDestination(playerEndPos.position);
                        StartCoroutine(RotatePlayer(playerEndPos.rotation, false));
                    }
                }

                if (hideSpriter)
                {
                    if (player.spriter)
                        player.spriter.gameObject.SetActive(true);
                }

                if(lockPlayer)
                {
                    if (playerEndPos != null)
                    {
                        player.transform.position = playerEndPos.position;
                        StartCoroutine(RotatePlayer(playerEndPos.rotation, false));
                    }
                }
            }
        }       
    }

    private IEnumerator RotatePlayer(Quaternion rotation, bool isStart)
    {
        //
        yield return new WaitForSeconds(0.5f);

        if (isStart) // Hack to ensure player always gets in position...
        {
            if (player.navMeshAgent.enabled)
            {
                while (player.navMeshAgent.velocity.magnitude > 0.1f)
                    yield return new WaitForEndOfFrame();

                if (playerStartPos)
                {
                    if (Vector3.Distance(playerStartPos.position, player.transform.position) > 0.1f)
                        player.navMeshAgent.Warp(playerStartPos.position);
                }

            }            
        }

        Quaternion curRot = player.transform.rotation;
        float t = 0;

        while(t < 1)
        {
            t += Time.deltaTime / 0.5f;
            player.transform.rotation = Quaternion.Slerp(curRot, rotation, t);
            yield return new WaitForEndOfFrame();
        }
    }

    public void OnEnable()
    {
        CameraSceneManager.OnChangeToEventCam += InitializeCamEvent;
        CameraSceneManager.OnChangeFromEventCam += TerminateCamEvent;
        SpawnPlayer.OnSpawnNewPlayer += ReferenceNewPlayer;
    }

    public void OnDisable()
    {
        CameraSceneManager.OnChangeToEventCam -= InitializeCamEvent;
        CameraSceneManager.OnChangeFromEventCam -= TerminateCamEvent;
        SpawnPlayer.OnSpawnNewPlayer -= ReferenceNewPlayer;
        StopAllCoroutines();

        for (int i = 0; i < wakeUpOnComplete.Count; i++)
        {
            if(wakeUpOnComplete[i])
                wakeUpOnComplete[i].gameObject.SetActive(true);
        }

        for (int i = 0; i < sleepOnComplete.Count; i++)
        {
            if(sleepOnComplete[i])
                sleepOnComplete[i].gameObject.SetActive(false);
        }
    }

    void ResetCamera()
    {
        eventCamParent.position = Vector3.zero;
        eventCamParent.rotation = Quaternion.identity;
        eventCamera.transform.position = Vector3.zero;
        eventCamera.transform.rotation = Quaternion.identity;
    }

    public void CamAnimatedBehaviour()
    {
        camAnimator.Play(animatorState, -1, 0f);
        StartCoroutine(Animation());
    }

    public void CamProceduralBehaviour()
    {
        //cameraProcedure.RunCamProcedure();
        StartCoroutine(Procedure());
    }

    public void CamKeysBehaviour()
    {
        //print("cameraKeysCounter = " + cameraKeysCounter);
        StartCoroutine(Tween(cameraKeysCounter));
    }

    private IEnumerator Animation()
    {        
        float t = 0;

        while(t < 1)
        {
            if (cameraTarget)
            {
                eventCamera.transform.LookAt(cameraTarget, Vector3.up);
            }

            camAnimator.SetFloat("normalizedTime", t);

            t += Time.deltaTime / duration;
            
            yield return new WaitForEndOfFrame();
        }

        camAnimator.SetFloat("normalizedTime", 0);

        CameraEventDeactivation();
    }

    private IEnumerator Procedure()
    {
        cameraProcedure.StartProcedure();

        float t = 0;

        while(t < 1)
        {
            cameraProcedure.UpdateProcedure(t);
            t += Time.deltaTime / duration;
            yield return new WaitForEndOfFrame();
        }

        cameraProcedure.EndProcedure();

        CameraEventDeactivation();
    }
    
    private IEnumerator Tween(int counter)
    {
        Vector3 posTarget = Vector3.zero;
        Quaternion rotTarget = Quaternion.identity;
        Vector3 currentPos = Vector3.zero;
        Quaternion currentRot = Quaternion.identity;
        Vector3 camStartPos = playerCam.transform.position;
        Quaternion camStartRot = playerCam.transform.rotation;

        if (counter < cameraKeys.Count - 1)
        {
            posTarget = cameraKeys[counter + 1].parentTransform.position;
            rotTarget = cameraKeys[counter + 1].parentTransform.rotation;
            currentPos = cameraKeys[counter].parentTransform.position;
            currentRot = cameraKeys[counter].parentTransform.rotation;
        }

        if (counter == 0)
        {
            if (camStartAtPlayerCam)
            {
                if (player)
                {
                    currentPos = playerCamParent.position;
                    currentRot = playerCamParent.rotation;
                }
            }
        }
        if (counter == cameraKeys.Count - 1) //// 2 ?
        {
            if(camEndsAtPlayerCam)
            {
                if(player)
                {
                    posTarget = playerCamParent.position;
                    rotTarget = playerCamParent.rotation;
                }
            }
        }

        float t = 0;

        while(t < 1)
        {            
            if (counter < cameraKeys.Count - 1) /// ???
            {
                if (cameraKeys[counter].move == EventCamera.CameraMoves.Linear)
                {
                    eventCamParent.position = Vector3.Lerp(currentPos, posTarget, t);
                    eventCamParent.rotation = Quaternion.Lerp(currentRot, rotTarget, t);
                }
                
                if (cameraKeys[counter].move == EventCamera.CameraMoves.Smooth)
                {
                    var lerp = Mathf.Lerp(0.0f, 1.0f, t);
                    var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

                    eventCamParent.position = Vector3.Lerp(currentPos, posTarget, smoothLerp);
                    eventCamParent.rotation = Quaternion.Slerp(currentRot, rotTarget, t);
                }

                if(cameraKeys[counter].move == EventCamera.CameraMoves.EaseOut)
                {
                    float f = t;
                    f = Mathf.Sin(f * Mathf.PI * 0.5f);

                    eventCamParent.position = Vector3.Lerp(currentPos, posTarget, f);
                    eventCamParent.rotation = Quaternion.Slerp(currentRot, rotTarget, f);
                }

                if(cameraKeys[counter].move == EventCamera.CameraMoves.EaseIn)
                {
                    float f = t;
                    f = 1 - Mathf.Cos(f * Mathf.PI * 0.5f);

                    eventCamParent.position = Vector3.Lerp(currentPos, posTarget, f);
                    eventCamParent.rotation = Quaternion.Slerp(currentRot, rotTarget, f);
                }
            }

            if(counter == 0)
            {
                if(camStartAtPlayerCam)
                {
                    var lerp = Mathf.Lerp(0.0f, 1.0f, t);
                    var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

                    if (cameraKeys.Count > 1)
                    {
                        eventCamera.transform.position = Vector3.Lerp(camStartPos, cameraKeys[1].camTransform.position, smoothLerp);

                        if (!cameraTarget && !targetIsPlayer)
                            eventCamera.transform.rotation = Quaternion.Slerp(camStartRot, cameraKeys[1].camTransform.rotation, smoothLerp);
                    }
                    else
                    {
                        eventCamera.transform.position = Vector3.Lerp(camStartPos, cameraKeys[0].camTransform.position, smoothLerp);

                        if (!cameraTarget && !targetIsPlayer)
                            eventCamera.transform.rotation = Quaternion.Slerp(camStartRot, cameraKeys[0].camTransform.rotation, smoothLerp);
                    }
                }
            }

            if (counter < cameraKeys.Count - 1)
            {
                if (cameraTarget)
                {
                    if (counter == 0)
                    {
                        Vector3 direction = cameraTarget.position - eventCamera.transform.position;
                        Quaternion toRotation = Quaternion.LookRotation(direction, Vector3.up);

                        eventCamera.transform.rotation = Quaternion.Slerp(eventCamera.transform.rotation, toRotation, t);
                    }                    
                }
            }
            if (counter >= cameraKeys.Count - 1)
            {
                if (camEndsAtPlayerCam)
                {
                    float f = t;
                    f = Mathf.Sin(f * Mathf.PI * 0.5f);

                    eventCamera.transform.position = Vector3.Lerp(eventCamera.transform.position, playerCam.transform.position, f);
                    eventCamera.transform.rotation = Quaternion.Slerp(eventCamera.transform.rotation, playerCam.transform.rotation, f);
                }
            }

            t += Time.deltaTime / cameraKeys[counter].duration;

            yield return new WaitForEndOfFrame();
        }

        if (counter < cameraKeys.Count - 1)
        {
            if (cameraKeys[counter].move == EventCamera.CameraMoves.Stepped)
            {
                eventCamParent.position = posTarget;
                eventCamParent.rotation = rotTarget;
            }
        }

        cameraKeysCounter += 1;

        if (cameraKeysCounter < cameraKeys.Count)
        {
            Invoke("CamKeysBehaviour", 0);
        }
        else
        {
            if (OnCameraEventStop != null)
            {
                OnCameraEventStop(this);
                print("Sent CamEventStop Message");
            }
        }

        gameEventsManager.AddCompletedEvent(this);
    }
  
    public void OnDestroy()
    {    
        StopAllCoroutines();
    }

    private void ReferenceNewPlayer(Transform playerTransform)
    {
        player = playerTransform.GetComponent<PlayerController>();
        playerCam = player.playerCam.GetComponent<Camera>();
        playerCamParent = playerCam.transform.parent;
        curParTransform = playerCamParent;
        curCamTransform = playerCam.transform;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;

        for (int i = 0; i < cameraKeys.Count; i++)
        {
            if (i <= cameraKeys.Count - 2)
            {
                Gizmos.DrawLine(cameraKeys[i].camTransform.position, cameraKeys[i + 1].camTransform.position);

                if (cameraKeysAutoLook)
                {
                    cameraKeys[i].camTransform.transform.LookAt(cameraKeys[i + 1].camTransform, Vector3.up);
                }
            }
        }
    }
}
