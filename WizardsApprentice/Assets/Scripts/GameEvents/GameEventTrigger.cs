﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventTrigger : MonoBehaviour
{
    public GameEventsManager gameEventsManager;
    public bool overrideAutoDisableEvent;
    public GameEvent gameEvent;
    public Transform graphicParent;
    public enum EventTriggerType { Achievement, Collision, Time }
    public bool fireAtFixedTime;
    public float timeToFireEvent;
    public bool isConstantRepeating;
    public float repeatTimeInterval;
    public EventTriggerType eventTriggerType;
    public bool deactivateAfterTrigger;
    public float deactivationDelay;
    public List<GameObject> objectsToWakeup = new List<GameObject>();
    public float wakeUpDelay;
    public List<GameObject> objectsToSleep = new List<GameObject>();
    public float sleepDelay;
    public float startTime;
    public int count = 2;
    private int currentCount;
    private int wakeUpCounter;
    private int sleepCounter;
    private bool checkIfEventHasCamera;
    //public AudioSource triggerAudio;
    public delegate void GameEventTriggerEvent(GameEvent gameEvent); //Sends to many; CaveInside, 
    public static event GameEventTriggerEvent OnTriggerEvent;
    /*
    private void Start()
    {
        if (!overrideAutoDisableEvent)
        {
            if (gameEventsManager.CheckEvent(gameEvent))
            {
                transform.gameObject.SetActive(false);
            }
            else
                InitializeTrigger();
        }
        else
        {
            InitializeTrigger();
        }
    }
    */
    public void OnEnable()
    {
        if (!overrideAutoDisableEvent)
        {
            if (gameEventsManager.CheckEvent(gameEvent))
                transform.gameObject.SetActive(false);
            else
                InitializeTrigger();
        }
        else
            InitializeTrigger();

        //Debug.Log(transform.gameObject);
    }

    void InitializeTrigger()
    {
        currentCount = 0;
        startTime = Time.time;        
        StartCoroutine(ScaleTriggerUp());

        if (eventTriggerType == EventTriggerType.Time)
        {
            if (fireAtFixedTime)
            {
                Invoke("TimedUpdate", timeToFireEvent);
            }
            if (isConstantRepeating)
            {
                InvokeRepeating("TimedUpdate", 0f, repeatTimeInterval);
            }
        }
    }

    private void TriggerLinkedEvent()
    {
        CheckEnableObjects();

        if (gameEvent.GameEventActivation())
        {
        //gameEvent.GameEventActivation();

            if (gameEvent.useCamera)
            {
                if (gameEvent.CameraEventActivation())
                {
                    checkIfEventHasCamera = true;
                }
                else
                {
                    checkIfEventHasCamera = false;
                }
            }

            if (OnTriggerEvent != null)
                OnTriggerEvent(gameEvent);

            StartCoroutine(ScaleTriggerDown());

            if (deactivateAfterTrigger)
            {
                Invoke("RunTriggerDeactivation", deactivationDelay);
            }
        }
    }

    private void CheckEnableObjects()
    {
        if (wakeUpDelay == 0)
            WakeUpObjects();
        else
            Invoke("WakeUpObjects", wakeUpDelay);

        if (sleepDelay == 0)
            SleepObjects();
        else
            Invoke("SleepObjects", sleepDelay);
    }

    private void RunTriggerDeactivation()
    {
        transform.gameObject.SetActive(false);
    }

    private void WakeUpObjects()
    {
        foreach (var item in objectsToWakeup)
        {
            item.SetActive(true);
        }        
    }

    private void SleepObjects()
    {
        foreach (var item in objectsToSleep)
        {
            item.SetActive(false);
        }
    }

    private void TriggerAfterTime()
    {
        if (Time.time - startTime >= repeatTimeInterval && currentCount < count)
        {
            //cameraEvent.InitializeCamEvent();
            CheckEnableObjects();

            if (gameEvent.CameraEventActivation())
            {
                currentCount += 1;
                startTime = Time.time;
            }            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (eventTriggerType == EventTriggerType.Collision)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                StartCoroutine(WaitForTimer());
            }
        }
    }

    private IEnumerator WaitForTimer()
    {
        yield return new WaitForSeconds(timeToFireEvent);

        TriggerLinkedEvent();
    }

    private IEnumerator ScaleTriggerUp()
    {
        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            graphicParent.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, smoothLerp);

            t += Time.deltaTime / 1f;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ScaleTriggerDown()
    {
        float t = 0;

        while (t < 1)
        {
            var lerp = Mathf.Lerp(0.0f, 1.0f, t);
            var smoothLerp = Mathf.SmoothStep(0.0f, 1.0f, lerp);

            graphicParent.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, smoothLerp);

            t += Time.deltaTime / 1f;
            yield return new WaitForEndOfFrame();
        }
    }

    private void TimedUpdate()
    {
        TriggerLinkedEvent();
    }
    
    public void OnDisable()
    {
        //Debug.Log(transform.gameObject);
    }
    private void OnDestroy()
    {
        CancelInvoke();
    }
}
