﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNavigationDemo : MonoBehaviour
{
    public GameManager gameManager;
    public SpriteRenderer UIworldPointer;
    public SpriteRenderer UIworldPointerArrow;
    public NavMeshPointer navMeshPointer;
    public Transform pointerDestination;
    public Animator demoNavigationAnim;

    public float wait0 = 1;
    public float wait1 = 1;
    public float lerp1 = 1;
    public float wait2 = 1;
    public float lerp2 = 1;
    public float wait3 = 1;
    public float lerp3 = 1;
    public float wait4 = 1;
    public float lerp4 = 1;

    private void Start()
    {
        StartCoroutine(PlayerNavigationDemoRoutine());
    }

    private IEnumerator PlayerNavigationDemoRoutine()
    {
        yield return new WaitForSeconds(wait0);

        float a = 0;

        while(a < 1)
        {
            a += Time.deltaTime / lerp1;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(wait1);

        float b = 0;

        while (b < 1)
        {
            b += Time.deltaTime / lerp2;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(wait2);

        float c = 0;

        while (c < 1)
        {
            c += Time.deltaTime / lerp3;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(wait3);

        float d = 0;

        while (d < 1)
        {
            d += Time.deltaTime / lerp4;
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(wait4);

        yield return null;
    }
}
