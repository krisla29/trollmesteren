﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventsManager : MonoBehaviour
{
    public GameManager gameManager;
    public Transform eventsParent;

    public List<Transform> gameEvents = new List<Transform>();

    public GameEvent completeWizardEvent;

    public void GetAllEvents()
    {
        for (int i = 0; i < eventsParent.childCount; i++)
        {
            gameEvents.Add(eventsParent.GetChild(i));
        }
    }

    public bool CheckEvent(GameEvent passedEvent) //used by GameEventTrigger
    {
        bool check = false;

        for (int i = 0; i < gameManager.playerStats.playedEvents.Count; i++)
        {
            if (gameManager.playerStats.playedEvents.Contains(gameEvents.IndexOf(passedEvent.transform)))
                check = true;
        }

        return check;
    }

    public void AddCompletedEvent(GameEvent passedEvent) //used by GameEvent
    {
        int index = gameEvents.IndexOf(passedEvent.transform);

        if (!gameManager.playerStats.playedEvents.Contains(index))
        {
            gameManager.playerStats.playedEvents.Add(index);

            if(passedEvent == completeWizardEvent)
            {
                int wizIndex = gameManager.characterSelector.GetCharacterIndex(CharacterSelector.PlayerCharacter.Wizard);

                if (!gameManager.playerStats.unlockedCharacters.Contains(wizIndex))
                    gameManager.playerStats.unlockedCharacters.Add(wizIndex);
            }
        }
    }
}
