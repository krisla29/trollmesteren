using MEC;
using Megapop.Logging;
//using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class RestManager2 : MonoSingleton<RestManager2>
{
    private ILog Log { get; } = Logs.Get(LogCategory.Server);

    private const string saveGameName = "wizardSaveGame";

    public bool IsInitializing { get; private set; } = true;
    public bool Initialized { get; private set; }
    private string UgcUri { get; set; }
    private string UserWizardUri { get; set; }
    private string CreateItemUri { get; set; }
    private string SaveGameUri { get; set; }
    private bool IsSaveGameOk { get; set; }

    public static event Action<Dictionary<string, object>> OnReceiveData;

    //Kristian Edit:
    public delegate void LoadingEvent(bool hasSaveGame); //Sends To MainMenuController
    public static event LoadingEvent OnPreloadDone;

    public delegate void WebEvent(); //Sends To playerstats
    public static event WebEvent OnSaveDone;

    public void GetStaticUris()
    {
        SaveGameUri = GameProperties.gp_saveGameUri;
        UgcUri = GameProperties.gp_ugcUri;
        UserWizardUri = UgcUri;
        CreateItemUri = GameProperties.gp_createItemUri;
        Initialized = GameProperties.gp_initialized;

        Debug.Log("Getting uris from other previous scene...");
        Debug.Log("SaveGameUri = " + SaveGameUri);
        Debug.Log("UgcUri = " + UgcUri);
        Debug.Log("UserWizardUri = " + UserWizardUri);
        Debug.Log("CreateItemUri = " + CreateItemUri);
        Debug.Log("Initialized = " + Initialized);
    }

    public void SetStaticUris()
    {
        GameProperties.gp_saveGameUri = SaveGameUri;
        GameProperties.gp_ugcUri = UgcUri;
        GameProperties.gp_createItemUri = CreateItemUri;
        GameProperties.gp_initialized = Initialized;

        Debug.Log("Setting for current session...");
        Debug.Log("SaveGameUri = " + SaveGameUri);
        Debug.Log("UgcUri = " + UgcUri);
        Debug.Log("UserWizardUri = " + UserWizardUri);
        Debug.Log("CreateItemUri = " + CreateItemUri);
        Debug.Log("Initialized = " + Initialized);
    }
    //
    public IEnumerator<float> Initialize()
    {       
        IsInitializing = true;
        Log.Info("Initialize");

        Initialized = false;
        UgcUri = null;
        UserWizardUri = null;
        CreateItemUri = null;
        SaveGameUri = null;
        IsSaveGameOk = false;
        
        yield return Timing.WaitUntilDone(GetUgcUri());

        UserWizardUri = UgcUri;

        if (string.IsNullOrEmpty(UserWizardUri))
        {
            Log.Error("UserWizardUri is null");
            IsInitializing = false;
            yield break;
        }

        Log.Info($"UserWizardUri={UserWizardUri}");

        yield return Timing.WaitUntilDone(LoadUserData());

        Log.Info($"SaveGameUri={SaveGameUri}");
        if (string.IsNullOrEmpty(SaveGameUri)) Log.Error("SaveGameUri is null");

        Initialized = IsSaveGameOk;

        SetStaticUris(); //Kristian edit

        IsInitializing = false;
        
        Log.Info($"Initialize done, Initialized={Initialized}");
    }

    private IEnumerator<float> GetUgcUri()
    {
        OnReceiveUgcUriCallback += HandleUgcUriCallback;

        Log.Info("GetUgcUri");

        RequestUgcUri();

        var startTime = Time.time;
        var waitTime = 20f;
#if UNITY_EDITOR
        waitTime = 1f;
#endif

        while (UgcUri == null)
        {
            if (Time.time - startTime > waitTime)
            {
                Log.Error("Timeout obtaining ugcUri");
                UgcUri = "";
                break;
            }

            yield return Timing.WaitForOneFrame;
        }

        OnReceiveUgcUriCallback -= HandleUgcUriCallback;
    }

#pragma warning disable 618
    private void RequestUgcUri() => UnityEngine.Application.ExternalCall("RequestUgcUri");
#pragma warning restore 618
    private event Action<string> OnReceiveUgcUriCallback;
    private void HandleUgcUriCallback(string ugcUri)
    {
        if (string.IsNullOrEmpty(ugcUri)) Log.Error("HandleUgcUriCallback ugcUri is null");
        UgcUri = ugcUri;
    }

    // This is not used for now, we get the composer url directly from ugcUri   
    private IEnumerator<float> GetUserWizardUri()
    {
        string json = null;

        void Callback(bool ok, int code, string data)
        {
            Log.Info($"UserWizardUri {UgcUri} ok={ok}, code={code}");
            if (!ok)
            {
                Log.Error($"UserWizardUri response code from server is {code}");
                return;
            }
            json = data;
        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(UgcUri, Callback));

        /*
         * Expected result:
         * {"zfiles":"http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634?siteId=adminSite&contentId=GNF169634&userId=admin&signature=rZ3fqrQe9oxYUL1%2fh4z6iwbpcHY%3d"}
         */

        const string wizardKey = "wizard";

        var result = RestUtils.DeserializeJson(json);

        if (result == null || !result.ContainsKey(wizardKey))
        {
            Log.Error("UserComposerUri  result from server is null");
                                                                          //result = RestUtils.DeserializeJson(TestRestData.UserLogonResult);
        }

        var userUri = result[wizardKey] as string;
        UserWizardUri = userUri;
        Log.Info($"UserWizardUri  UserWizardUri ={UserWizardUri}");
    }

    private IEnumerator<float> LoadUserData()
    {
        Log.Info($"LoadUserData");
        string json = null;

        void Callback(bool ok, int code, string data)
        {
            Log.Info($"LoadUserData callback ok={ok}, code={code}");
            if (!ok)
            {
                Log.Error($"LoadUserData response code from server is {code}");
                return;
            }
            json = data;
        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(UserWizardUri, Callback));

        /*
         * Expected result:
         *  {
         *      "createItemUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items?siteId=adminSite&contentId=GNF169634&userId=admin&signature=aVGu38KXV%2fCv1x7a6itRSC2L0ps%3d&expires=637080300646633246",
         *      "selfUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634?siteId=adminSite&contentId=GNF169634&userId=admin&signature=dBYKXabDW2LqlhkVjrWDBWNQ2kE%3d&expires=637080300646633246",
         *      "items": [
         *          {
         *              "uri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items/1?siteId=adminSite&contentId=GNF169634&userId=admin&signature=yipxvMhLAaM17AztBa1QMf23WTk%3d&expires=637080300646633246",
         *              "title": null,
         *              "updated": "10/28/2019 11:32:42",
         *              "siteId": "adminSite",
         *              "contentId": "GNF169634",
         *              "metaData": null
         *          }
         *      ]
         *  }
         *
         *  First time items can be null, or multiple items in the array.
         */

        const string createItemUriKey = "createItemUri";
        const string itemsKey = "items";
        const string titleKey = "title";
        const string uriKey = "uri";

        var result = RestUtils.DeserializeJson(json);
        //var result = json;

        if (result == null || !result.ContainsKey(createItemUriKey))
        {
            Log.Error("LoadUserData result from server is null");
            yield break;
        }

        CreateItemUri = result[createItemUriKey] as string;

        Log.Info("LoadUserData checking for existing save game");
        var hasSaveFile = false;

        if (result.ContainsKey(itemsKey) && result[itemsKey] != null)
        {
            if (result[itemsKey] is List<object> items)
            {
                // TODO what if there exists multiple save games? choose newest? right now first is used
                foreach (var item in items)
                {
                    if (!(item is Dictionary<string, object> dict)) continue;

                    if (!dict.ContainsKey(titleKey) || !(dict[titleKey] is string title) || title != saveGameName) continue;

                    var saveGameUri = dict[uriKey] as string;

                    SaveGameUri = saveGameUri;

                    Log.Info($"LoadUserData found existing save game");

                    // load data
                    yield return Timing.WaitUntilDone(LoadSaveGame());
                    // verify loaded data
                    hasSaveFile = IsSaveGameOk;
                    Log.Info($"LoadUserData hasSaveFile={hasSaveFile}");
                }
            }
        }

        if (OnPreloadDone != null)
            OnPreloadDone(hasSaveFile);

        if (!hasSaveFile)
        {
            Log.Info("LoadUserData creating new save game");
            // create item
            yield return Timing.WaitUntilDone(CreateSaveGame());
            yield return Timing.WaitUntilDone(SaveGame(""));
            //yield return Timing.WaitUntilDone(LoadSaveGame());
        }        
    }

    public IEnumerator<float> LoadSaveGame()
    {
        Log.Info($"LoadSaveGame");
        string json = null;

        void Callback(bool ok, int code, string data)
        {
            Log.Info($"LoadSaveGame callback ok={ok}, code={code}");

            if (!ok)
            {
                Log.Error($"LoadSaveGame response code from server is {code}");
                return;
            }
            json = data;
        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(SaveGameUri, Callback));

        /*
         * Expected result:
         *  {
         *      "selfUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items/8?siteId=adminSite&contentId=GNF169634&userId=admin&signature=4qYzsXR%2b7N7Udn%2fktIsmX3RFJgg%3d&expires=637080311789600202",
         *      "created": {
         *          "time": "10/29/2019 09:57:28",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "updated": {
         *          "time": "10/29/2019 16:07:35",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "contentUri": null,
         *      "title": "zfilesSaveGame",
         *      "data": "{\"_bool\":{\"Downtown_HasSeenWorldIntro\":true,\"Downtown_HasSeenPolicemanIntro\":true,\"Downtown_HasSeenParkRuinsIntro\":true,\"Downtown_HasCheckedPyramidCodeFirstTime\":true,\"Downtown_HasSeenOnReturnFromPyramid\":true},\"_integers\":{},\"_items\":{\"Downtown_IdCard\":\"Destination\"},\"_strings\":{\"Downtown_LastOrbitLocationGroupDesc\":\"DowntownGroup_Crimescene_Pano.asset\"}}",
         *      "key": "zfilesSaveGame",
         *      "metaData": null
         *  }
         */

        const string dataKey = "data";

        var result = RestUtils.DeserializeJson(json);

        //Log.Info("data recieved after Deserialization 1 is " + result[dataKey]);

        if (result == null)
        {
            //Log.Error("LoadSaveGame result from server is null");
            yield break;
        }

        var gameDataJson = result.ContainsKey(dataKey) ? result[dataKey] as string : null;

        if (gameDataJson != null)
        {
            string gameData = gameDataJson;
            IsSaveGameOk = gameData != null;
            SaveSystemWeb.ReturnWebGlLoad(gameData);
            //Log.Info("data recieved is " + gameData);
            //OnReceiveData?.Invoke(gameData);
        }        
    }

    private IEnumerator<float> CreateSaveGame()
    {
        Log.Info("CreateSaveGame");
        var name = $"{saveGameName}";

        var body = $"{{\"title\":\"{name}\",\"key\":\"{name}\"}}";

        string json = null;

        void Callback(bool ok, int code, string data)
        {
            Log.Info($"CreateSaveGame callback ok={ok}, code={code}");

            if (!ok)
            {
                Log.Error($"CreateSaveGame response code from server is {code}");
                return;
            }
            json = data;
        }

        /*
         * Expected result:
         *  {
         *      "selfUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items/9?siteId=adminSite&contentId=GNF169634&userId=admin&signature=zAcsoXckgBFnFw89motjk%2bS9BiA%3d&expires=637080313071127996",
         *      "created": {
         *          "time": "10/30/2019 10:21:47",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "updated": {
         *          "time": "10/30/2019 10:21:47",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "contentUri": null,
         *      "title": "zfiles_store_item",
         *      "data": null,
         *      "key": "zfiles_store_item",
         *      "metaData": null
         *  }
         */

        const string selfUriKey = "selfUri";

        yield return Timing.WaitUntilDone(RestUtils.PostRequest(CreateItemUri, body, Callback));

        var result = RestUtils.DeserializeJson(json);

        if (result == null || !result.ContainsKey(selfUriKey))
        {
            Log.Error("CreateSaveGame result from server is null");
            yield break;
        }

        SaveGameUri = result[selfUriKey] as string;
    }

    public IEnumerator<float> SaveGame(string saveString)
    {
        Log.Info("SaveGame");
        Log.Info(saveString);
        
        if (string.IsNullOrEmpty(SaveGameUri))
        {
            Log.Error($"Cannot save game because SaveGameUri is not set.");
            yield break;
        }
        
        yield return Timing.WaitUntilDone(UploadSaveGame(saveString));

        if (OnSaveDone != null)
            OnSaveDone();
    }

    private IEnumerator<float> UploadSaveGame(string saveString)
    {      
        Log.Info("UploadSaveGame");
        var saveGameAsJson = MiniJSON.Json.Serialize(saveString); //allready done in SaveHandler
        // -----------Server stores this as a string so we need to properly escape all apostrophes.
        saveGameAsJson = saveString.Replace("\"", "\\\"");
        //Debug.Log(saveString);

        var name = $"{saveGameName}";
        var body = $"{{\"title\":\"{name}\",\"key\":\"{name}\",\"data\":\"{saveGameAsJson}\"}}";

        //Debug.Log(body);

        string json = null;
        var isOk = false;

        void Callback(bool ok, int code, string data)
        {
            Log.Info($"UploadSaveGame callback ok={ok}, code={code}");
            if (!ok)
            {
                Log.Error($"UploadSaveGame response code from server is {code}");
                return;
            }
            json = data;
            isOk = ok;
        }

        //Debug.Log(body);

        yield return Timing.WaitUntilDone(RestUtils.PutRequest(SaveGameUri, body, Callback));

        Log.Info($"Upload of save game was {(isOk ? "" : "not ")}ok");
    }

    private IEnumerator<float> SanityRestTest()
    {
        var url = "https://httpbin.org/get";
        Log.Info($"SanityRestTest url={url}");

        void Callback(bool ok, int code, string data)
        {
            Log.Info($"SanityRestTest {UgcUri} ok={ok}, code={code}");
            Log.Info($"SanityRestTest {UgcUri} data={data}");
            if (!ok)
            {
                Log.Error($"GetUserZFilesUri response code from server is {code}");
                return;
            }

        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(url, Callback));
    }
}
