using MEC;
//using Megapop.Logging;
//using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class RestManager : MonoSingleton<RestManager>
{
    //private ILog Log { get; } = Logs.Get(LogCategory.Server);

    private const string saveGameName = "composerSaveGame";

    public bool IsInitializing { get; private set; } = true;
    public bool Initialized { get; private set; }
    private string UgcUri { get; set; }
    private string UserComposerUri { get; set; }
    private string CreateItemUri { get; set; }
    private string SaveGameUri { get; set; }
    private bool IsSaveGameOk { get; set; }

    public static event Action<Dictionary<string, object>> OnReceiveData;
    
    public IEnumerator<float> Initialize()
    {       
        IsInitializing = true;
        //Log.Info("Initialize");

        Initialized = false;
        UgcUri = null;
        UserComposerUri = null;
        CreateItemUri = null;
        SaveGameUri = null;
        IsSaveGameOk = false;
        
        yield return Timing.WaitUntilDone(GetUgcUri());
        
        UserComposerUri = UgcUri;

        if (string.IsNullOrEmpty(UserComposerUri))
        {
            //Log.Error("UserZFilesUri is null");
            IsInitializing = false;
            yield break;
        }

        //Log.Info($"UserZFilesUri={UserZFilesUri}");

        yield return Timing.WaitUntilDone(LoadUserData());

        //Log.Info($"SaveGameUri={SaveGameUri}");
        //if (string.IsNullOrEmpty(SaveGameUri)) Log.Error("SaveGameUri is null");

        Initialized = IsSaveGameOk;
        IsInitializing = false;

        //Log.Info($"Initialize done, Initialized={Initialized}");
    }

    private IEnumerator<float> GetUgcUri()
    {
        OnReceiveUgcUriCallback += HandleUgcUriCallback;

        //Log.Info("GetUgcUri");

        RequestUgcUri();

        var startTime = Time.time;
        var waitTime = 20f;
#if UNITY_EDITOR
        waitTime = 1f;
#endif

        while (UgcUri == null)
        {
            if (Time.time - startTime > waitTime)
            {
                //Log.Error("Timeout obtaining ugcUri");
                UgcUri = "";
                break;
            }

            yield return Timing.WaitForOneFrame;
        }

        OnReceiveUgcUriCallback -= HandleUgcUriCallback;
    }

#pragma warning disable 618
    private void RequestUgcUri() => UnityEngine.Application.ExternalCall("RequestUgcUri");
#pragma warning restore 618
    private event Action<string> OnReceiveUgcUriCallback;
    private void HandleUgcUriCallback(string ugcUri)
    {
        //if (string.IsNullOrEmpty(ugcUri)) Log.Error("HandleUgcUriCallback ugcUri is null");
        UgcUri = ugcUri;
    }

    // This is not used for now, we get the composer url directly from ugcUri   
    private IEnumerator<float> GetUserCompositionUri()
    {
        string json = null;

        void Callback(bool ok, int code, string data)
        {
            //Log.Info($"GetUserZFilesUri {UgcUri} ok={ok}, code={code}");
            if (!ok)
            {
                //Log.Error($"GetUserZFilesUri response code from server is {code}");
                return;
            }
            json = data;
        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(UgcUri, Callback));

        /*
         * Expected result:
         * {"zfiles":"http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634?siteId=adminSite&contentId=GNF169634&userId=admin&signature=rZ3fqrQe9oxYUL1%2fh4z6iwbpcHY%3d"}
         */

        const string composerKey = "composer";

        var result = RestUtils.DeserializeJson(json);

        if (result == null || !result.ContainsKey(composerKey))
        {
            //Log.Error("GetUserZFilesUri result from server is null");
                                                                          //result = RestUtils.DeserializeJson(TestRestData.UserLogonResult);
        }

        var userUri = result[composerKey] as string;
        UserComposerUri = userUri;
        //Log.Info($"GetUserZFilesUri UserZFilesUri={UserZFilesUri}");
    }

    private IEnumerator<float> LoadUserData()
    {
        //Log.Info($"LoadUserData");
        string json = null;

        void Callback(bool ok, int code, string data)
        {
            //Log.Info($"LoadUserData callback ok={ok}, code={code}");
            if (!ok)
            {
                //Log.Error($"LoadUserData response code from server is {code}");
                return;
            }
            json = data;
        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(UserComposerUri, Callback));

        /*
         * Expected result:
         *  {
         *      "createItemUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items?siteId=adminSite&contentId=GNF169634&userId=admin&signature=aVGu38KXV%2fCv1x7a6itRSC2L0ps%3d&expires=637080300646633246",
         *      "selfUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634?siteId=adminSite&contentId=GNF169634&userId=admin&signature=dBYKXabDW2LqlhkVjrWDBWNQ2kE%3d&expires=637080300646633246",
         *      "items": [
         *          {
         *              "uri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items/1?siteId=adminSite&contentId=GNF169634&userId=admin&signature=yipxvMhLAaM17AztBa1QMf23WTk%3d&expires=637080300646633246",
         *              "title": null,
         *              "updated": "10/28/2019 11:32:42",
         *              "siteId": "adminSite",
         *              "contentId": "GNF169634",
         *              "metaData": null
         *          }
         *      ]
         *  }
         *
         *  First time items can be null, or multiple items in the array.
         */

        const string createItemUriKey = "createItemUri";
        const string itemsKey = "items";
        const string titleKey = "title";
        const string uriKey = "uri";

        var result = RestUtils.DeserializeJson(json);

        if (result == null || !result.ContainsKey(createItemUriKey))
        {
            //Log.Error("LoadUserData result from server is null");
            yield break;
        }

        CreateItemUri = result[createItemUriKey] as string;

        //Log.Info("LoadUserData checking for existing save game");
        var hasSaveFile = false;
        if (result.ContainsKey(itemsKey) && result[itemsKey] != null)
        {
            if (result[itemsKey] is List<object> items)
            {
                // TODO what if there exists multiple save games? choose newest? right now first is used
                foreach (var item in items)
                {
                    if (!(item is Dictionary<string, object> dict)) continue;

                    if (!dict.ContainsKey(titleKey) || !(dict[titleKey] is string title) || title != saveGameName) continue;

                    var saveGameUri = dict[uriKey] as string;

                    SaveGameUri = saveGameUri;

                    //Log.Info($"LoadUserData found existing save game");

                    // load data
                    yield return Timing.WaitUntilDone(LoadSaveGame());
                    // verify loaded data
                    hasSaveFile = IsSaveGameOk;
                    //Log.Info($"LoadUserData hasSaveFile={hasSaveFile}");
                }
            }
        }

        if (!hasSaveFile)
        {
            //Log.Info("LoadUserData creating new save game");
            // create item
            yield return Timing.WaitUntilDone(CreateSaveGame());
            yield return Timing.WaitUntilDone(SaveGame(new GameProgressData()));
            yield return Timing.WaitUntilDone(LoadSaveGame());
        }
    }

    private IEnumerator<float> LoadSaveGame()
    {
        //Log.Info($"LoadSaveGame");
        string json = null;

        void Callback(bool ok, int code, string data)
        {
            //Log.Info($"LoadSaveGame callback ok={ok}, code={code}");

            if (!ok)
            {
                //Log.Error($"LoadSaveGame response code from server is {code}");
                return;
            }
            json = data;
        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(SaveGameUri, Callback));

        /*
         * Expected result:
         *  {
         *      "selfUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items/8?siteId=adminSite&contentId=GNF169634&userId=admin&signature=4qYzsXR%2b7N7Udn%2fktIsmX3RFJgg%3d&expires=637080311789600202",
         *      "created": {
         *          "time": "10/29/2019 09:57:28",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "updated": {
         *          "time": "10/29/2019 16:07:35",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "contentUri": null,
         *      "title": "zfilesSaveGame",
         *      "data": "{\"_bool\":{\"Downtown_HasSeenWorldIntro\":true,\"Downtown_HasSeenPolicemanIntro\":true,\"Downtown_HasSeenParkRuinsIntro\":true,\"Downtown_HasCheckedPyramidCodeFirstTime\":true,\"Downtown_HasSeenOnReturnFromPyramid\":true},\"_integers\":{},\"_items\":{\"Downtown_IdCard\":\"Destination\"},\"_strings\":{\"Downtown_LastOrbitLocationGroupDesc\":\"DowntownGroup_Crimescene_Pano.asset\"}}",
         *      "key": "zfilesSaveGame",
         *      "metaData": null
         *  }
         */

        const string dataKey = "data";

        var result = RestUtils.DeserializeJson(json);

        if (result == null)
        {
            //Log.Error("LoadSaveGame result from server is null");
            yield break;
        }

        var gameDataJson = result.ContainsKey(dataKey) ? result[dataKey] as string : null;
        if (gameDataJson != null)
        {
            var gameData = RestUtils.DeserializeJson(gameDataJson);
            IsSaveGameOk = gameData != null;
            OnReceiveData?.Invoke(gameData);
        }
    }

    private IEnumerator<float> CreateSaveGame()
    {
        //Log.Info("CreateSaveGame");
        var name = $"{saveGameName}";

        var body = $"{{\"title\":\"{name}\",\"key\":\"{name}\"}}";

        string json = null;

        void Callback(bool ok, int code, string data)
        {
            //Log.Info($"CreateSaveGame callback ok={ok}, code={code}");

            if (!ok)
            {
                //Log.Error($"CreateSaveGame response code from server is {code}");
                return;
            }
            json = data;
        }

        /*
         * Expected result:
         *  {
         *      "selfUri": "http://api.gyldendal.no/PodiumUGC/GNF-preview/Private/GNF169634/items/9?siteId=adminSite&contentId=GNF169634&userId=admin&signature=zAcsoXckgBFnFw89motjk%2bS9BiA%3d&expires=637080313071127996",
         *      "created": {
         *          "time": "10/30/2019 10:21:47",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "updated": {
         *          "time": "10/30/2019 10:21:47",
         *          "siteId": "adminSite",
         *          "contentId": "GNF169634",
         *          "contentUri": null
         *      },
         *      "contentUri": null,
         *      "title": "zfiles_store_item",
         *      "data": null,
         *      "key": "zfiles_store_item",
         *      "metaData": null
         *  }
         */

        const string selfUriKey = "selfUri";

        yield return Timing.WaitUntilDone(RestUtils.PostRequest(CreateItemUri, body, Callback));

        var result = RestUtils.DeserializeJson(json);

        if (result == null || !result.ContainsKey(selfUriKey))
        {
            //Log.Error("CreateSaveGame result from server is null");
            yield break;
        }

        SaveGameUri = result[selfUriKey] as string;
    }

    public IEnumerator<float> SaveGame(GameProgressData gameProgressData)
    {
        //Log.Info("SaveGame");
        if (string.IsNullOrEmpty(SaveGameUri))
        {
            //Log.Error($"Cannot save game because SaveGameUri is not set.");
            yield break;
        }

        yield return Timing.WaitUntilDone(UploadSaveGame(gameProgressData.ToExportDictionary()));
    }

    private IEnumerator<float> UploadSaveGame(Dictionary<string, object> dataDictionary)
    {
        //Log.Info("UploadSaveGame");
        var saveGameAsJson = MiniJSON.Json.Serialize(dataDictionary);
        // Server stores this as a string so we need to properly escape all apostrophes.
        saveGameAsJson = saveGameAsJson.Replace("\"", "\\\"");

        var name = $"{saveGameName}";
        var body = $"{{\"title\":\"{name}\",\"key\":\"{name}\",\"data\":\"{saveGameAsJson}\"}}";

        string json = null;
        var isOk = false;

        void Callback(bool ok, int code, string data)
        {
            //Log.Info($"UploadSaveGame callback ok={ok}, code={code}");
            if (!ok)
            {
                //Log.Error($"UploadSaveGame response code from server is {code}");
                return;
            }
            json = data;
            isOk = ok;
        }

        yield return Timing.WaitUntilDone(RestUtils.PutRequest(SaveGameUri, body, Callback));

        //Log.Info($"Upload of save game was {(isOk ? "" : "not ")}ok");
    }

    private IEnumerator<float> SanityRestTest()
    {
        var url = "https://httpbin.org/get";
        //Log.Info($"SanityRestTest url={url}");

        void Callback(bool ok, int code, string data)
        {
            //Log.Info($"SanityRestTest {UgcUri} ok={ok}, code={code}");
            //Log.Info($"SanityRestTest {UgcUri} data={data}");
            if (!ok)
            {
                //Log.Error($"GetUserZFilesUri response code from server is {code}");
                return;
            }

        }

        yield return Timing.WaitUntilDone(RestUtils.GetRequest(url, Callback));
    }
    /*
    #region DEBUG
#if UNITY_EDITOR
    [Button("DebugSetInitialized")]
    private void SetInitialized()
    {
        Initialized = true;
    }

    [Button("Initialize")]
    private void DoInitialize()
    {
        Timing.RunCoroutine(Initialize());
    }

    [Button("TestUploadSaveGame")]
    private void TestUploadSave()
    {
        if (string.IsNullOrEmpty(SaveGameUri))
        {
            Log.Error("Cannot upload save because SaveGameUri is not set");
            return;
        }

        Timing.RunCoroutine(UploadSaveGame(GenerateTestSaveData()));
    }

    [Button("TestClearSaveGame")]
    private void TestClearSave()
    {
        if (string.IsNullOrEmpty(SaveGameUri))
        {
            Log.Error("Cannot upload save because SaveGameUri is not set");
            return;
        }

        Timing.RunCoroutine(UploadSaveGame(new GameProgressData().ToExportDictionary()));
    }

    [Button("SerializeDeserializeTest")]
    private void SerializeTest()
    {
        var gameData = GenerateTestSaveData();
        var json = MiniJSON.Json.Serialize(gameData);
        Log.Info($"json=\n{json}");
        PrintGameProgressDataJson(json);
    }
    
    private Dictionary<string, object> GenerateTestSaveData()
    {
        var gameProgressData = new GameProgressData();

        foreach (var boolId in Enum.GetValues(typeof(BoolSaveGameIds)).Cast<BoolSaveGameIds>().ToArray())
        {
            gameProgressData.Booleans.Add(boolId.ToString(), Random.value >= 0.5f);
        }

        foreach (var intId in Enum.GetValues(typeof(IntegerSaveGameIds)).Cast<IntegerSaveGameIds>().ToArray())
        {
            gameProgressData.Integers.Add(intId.ToString(), (int)(Random.value * 100));
        }

        gameProgressData.Items.Add("SomeItem", ItemLocation.Inventory);
        gameProgressData.Items.Add("AnotherItem", ItemLocation.Destination);
        gameProgressData.Items.Add("UnrelatedItem", ItemLocation.Source);

        gameProgressData.Strings.Add("SomeStringValue", "TheValue");
        gameProgressData.Strings.Add("AnotherStringValue", "TheOtherValue");
        gameProgressData.Strings.Add("UnrelatedStringValue", "CompletelyUnrelatedValue");

        return gameProgressData.ToExportDictionary();

        //var dict = new Dictionary<string, object>();
        //dict.Add("_bool", gameProgressData.Booleans);
        //dict.Add("_integers", gameProgressData.Integers);
        //dict.Add("_items", gameProgressData.Items);
        //dict.Add("_strings", gameProgressData.Strings);
        //return dict;
    }
    
    private void PrintGameProgressDataJson(string json)
    {
        var data = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
        if (data == null)
        {
            //Log.Error($"data is null");
            return;
        }

        var bools = data[GameProgressData.BoolsKey] as Dictionary<string, object>;
        //Log.Info($"Printing bools ({bools.Count} elements)");
        foreach (var kvp in bools)
        {
            var boolValue = Convert.ToBoolean(kvp.Value);
            //Log.Info($"{kvp.Key}:{boolValue}");
        }

        var ints = data[GameProgressData.IntsKey] as Dictionary<string, object>;
        //Log.Info($"Printing ints ({ints.Count} elements)");
        foreach (var kvp in ints)
        {
            var boolValue = Convert.ToInt32(kvp.Value);
            //Log.Info($"{kvp.Key}:{boolValue}");
        }

        var items = data[GameProgressData.ItemsKey] as Dictionary<string, object>;
        //Log.Info($"Printing items ({items.Count} elements)");
        foreach (var kvp in items)
        {
                                                                  Enum.TryParse(kvp.Value as string, out ItemLocation itemLocationValue);
            //Log.Info($"{kvp.Key}:{itemLocationValue}");
        }

        var strings = data[GameProgressData.StringsKey] as Dictionary<string, object>;
        //Log.Info($"Printing strings ({strings.Count} elements)");
        foreach (var kvp in items)
        {
            //Log.Info($"{kvp.Key}:{kvp.Value as string}");
        }
    }
    
    private void _TestSerialization()
    {
        var dict = new Dictionary<string, object>();
        dict.Add("stringKey", "thisIsAStringValue");
        dict.Add("intKey", 666);
        dict.Add("boolKey", true);
        //dict.Add("itemDataKey", new GameProgressData.ItemData() { Location = ItemLocation.Inventory });
        dict.Add("itemLocationKey", ItemLocation.Destination);

        var json = MiniJSON.Json.Serialize(dict);
        Log.Info($"json={json}");

        var obj = MiniJSON.Json.Deserialize(json);
        Log.Info($"deserialized obj={obj}");

        var _dict = obj as Dictionary<string, object>;
        if (_dict == null)
        {
            Log.Error("Error deserializing json as dictionary");
            return;
        }

        var stringValue = _dict["stringKey"] as string;
        Log.Info($"stringValue={stringValue}");
        var intValue = Convert.ToInt32(_dict["intKey"]);
        Log.Info($"intValue={intValue}");
        var boolValue = Convert.ToBoolean(_dict["boolKey"]);
        Log.Info($"boolValue={boolValue}");
        var itemDataValue = _dict["itemDataKey"] as GameProgressData.ItemData;
        //Log.Info($"itemDataValue={itemDataValue}");
        Enum.TryParse(_dict["itemLocationKey"] as string, out ItemLocation itemLocationValue);
        Log.Info($"itemLocationValue={itemLocationValue}");
    }
    */
//#endif
    //#endregion
}
