using MEC;
using Megapop.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public static class RestUtils
{
    private static ILog Log { get; } = Logs.Get(LogCategory.Server);

    public delegate void RestRequestCallback(bool isOk, int responseCode, string data);

    public static IEnumerator<float> GetRequest(string url, RestRequestCallback callback)
    {
        var request = UnityWebRequest.Get(url);
        request.timeout = 20;

        yield return Timing.WaitUntilDone(request.SendWebRequest());

        var responseCode = (int)request.responseCode;
        var isOk = !(request.isNetworkError || request.isHttpError);

        Log.Info("got data from server");

        string data = null;
        if (isOk)
        {
            try
            {
                data = Encoding.UTF8.GetString(request.downloadHandler.data, 0, request.downloadHandler.data.Length);               
            }
            catch (Exception e)
            {
                Log.Error($"RestUtils.Get: Problem decoding data from server: {e}");
            }
        }

        //Log.Info("data recieved is " + data);

        callback?.Invoke(isOk, responseCode, data);
    }

    public static IEnumerator<float> PutRequest(string url, string data, RestRequestCallback callback)
    {
        yield return Timing.WaitUntilDone(SendRequest(url, data, RestMethod.PUT, callback));
    }

    public static IEnumerator<float> PostRequest(string url, string data, RestRequestCallback callback)
    {
        yield return Timing.WaitUntilDone(SendRequest(url, data, RestMethod.POST, callback));
    }

    private enum RestMethod { POST, PUT }

    private static IEnumerator<float> SendRequest(string url, string dataToSend, RestMethod method, RestRequestCallback callback)
    {
        // Due to a bug in UnityWebRequest we have to initializing request with Put first, and then change method to POST
        //K log:
        Log.Info("url to send is " + url);
        //Log.Info("data sent is " + dataToSend);
        //
        var request = UnityWebRequest.Put(url, dataToSend);
        if (method == RestMethod.POST) request.method = UnityWebRequest.kHttpVerbPOST;
        //----edit:
        /*
        var request = new UnityWebRequest();

        if (method == RestMethod.POST)
        {
            request = UnityWebRequest.Post(url, dataToSend);
        }
        else
            request = UnityWebRequest.Put(url, dataToSend);
        */
        //
        request.timeout = 20;
        request.SetRequestHeader("Content-Type", "application/json");

        yield return Timing.WaitUntilDone(request.SendWebRequest());
        var responseCode = (int)request.responseCode;
        var isOk = !(request.isNetworkError || request.isHttpError);

        if (!isOk)
        {
            Log.Error($"RestUtils.{method}: Problem receiving sending data to server. ResponseCode={responseCode}");
            callback?.Invoke(isOk, responseCode, "");
            yield break;
        }

        var data = request.downloadHandler.data;

        string tokenJson;
        try
        {
            tokenJson = Encoding.UTF8.GetString(data, 0, data.Length);
        }
        catch (Exception e)
        {
            Log.Error($"RestUtils.{method}: Problem decoding reply data from server. e={e}");
            yield break;
        }

        callback?.Invoke(isOk, responseCode, tokenJson);
    }

    public static Dictionary<string, object> DeserializeJson(string json)
    {
        
        Dictionary<string, object> objectDict;

        try
        {
            objectDict = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
        }
        catch (Exception)
        {
            objectDict = null;
        }
        
        return objectDict;
    }

    public static Dictionary<string, object> DeserializeJsonX(string json)
    {
        Dictionary<string, object> objectDict;

        try
        {
            //objectDict = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
            objectDict = JsonUtility.FromJson<Dictionary<string, object>>(json);
        }
        catch (Exception)
        {
            objectDict = null;
        }

        return objectDict;
    }

    public static Dictionary<string, T> DeserializeJson<T>(string json)
    {
        var objectDict = DeserializeJson(json);
        var dict = new Dictionary<string, T>();
        if (objectDict != null)
        {
            foreach (var obj in objectDict)
            {
                if (obj.Value is T value)
                    dict.Add(obj.Key, value);
            }

            return dict;
        }

        return null;
    }

    public static string DeserializeJson2(string json)
    {
        //var objectDict = MiniJSON.Json.Deserialize(json) as string;
        var objectDict = JsonUtility.FromJson<string>(json);

        return objectDict;
    }

    public static SaveObject DeserializeJson3(string json)
    {
        var sObject = MiniJSON.Json.Deserialize(json) as SaveObject;

        return sObject;
    }
    /*
    public static Dictionary<string, T> DeserializeJson2<T>(string json)
    {
        var objectDict = DeserializeJson(json);
        var dict = new Dictionary<string, T>();
        if (objectDict != null)
        {
            foreach (var obj in objectDict)
            {
                if (obj.Value is T value)
                    dict.Add(obj.Key, value);
            }

            return dict;
        }

        return null;
    }
    */
}
