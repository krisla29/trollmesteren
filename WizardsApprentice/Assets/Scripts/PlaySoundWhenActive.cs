﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundWhenActive : MonoBehaviour
{
    public AudioSource audioSource;

    private void OnEnable()
    {
        audioSource.Play();
    }
    private void OnDisable()
    {
        audioSource.Stop();
    }
}
